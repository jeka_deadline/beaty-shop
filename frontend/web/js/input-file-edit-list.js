var inputFilesStore = {};

(function($) {
    $.fn.serializeEditFiles = function() {
        var obj = $(this);
        var formData = new FormData();
        $.each($(obj).find("input[type='file']"), function(i, tag) {
            var id = $(tag).attr('id');
            if (!id) return;
            $.each(inputFilesStore[id], function(i, file) {
                formData.append(tag.name, file);
            });
        });
        var params = $(obj).serializeArray();
        $.each(params, function (i, val) {
            formData.append(val.name, val.value);
        });
        return formData;
    };
})(jQuery);

$(function() {
    function unique(arr) {
        var obj = {};
        for (var i = 0; i < arr.length; i++) {
            var str = arr[i].name;
            obj[str] = arr[i];
        }
        return Object.keys(obj).map(function (index) {
            return obj[index];
        });
    }

    function showEditFiles(id) {
        var files = '';
        $.each(inputFilesStore[id], function (i, v) {
            files += '<p class="files-added">' + ' <a href="javascript:void(0)" class="load-file-edit-remove" data-index="' + i + '"><svg class="close-added-file svg"><use xlink:href="#close"></use></svg></a>' + v.name + '</p>';
        });
        $('[data-for="'+id+'"]').html(files);
    }

    $(document).on("click", ".load-file-edit-remove", function (e) {
        var id = $(this).parents('[data-for]').attr('data-for');
        if (!id) return;
        var index = $(this).attr('data-index')

        inputFilesStore[id].splice(index, 1);
        showEditFiles(id);
    });

    $('.load-file-edit-list').on('change', function () {
        console.log(this);
        console.log(this.files);
        console.log(this.files.length);
        if (!this.files.length) return;
        var that = this;

        var id = $(this).attr('id');
        if (!id) return;

        var listFiles = Object.keys(this.files).map(function (index) {
            return that.files[index];
        });

        if (id in inputFilesStore) {
            inputFilesStore[id] = inputFilesStore[id].concat(listFiles);
        } else {
            inputFilesStore[id] = listFiles;
        }
        inputFilesStore[id] = unique(inputFilesStore[id]);

        this.value = '';
        showEditFiles(id);
    });
});