<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class MaintainceAsset extends AssetBundle
{
    public $sourcePath = '@frontend/web/maintaince';

    public $css = [
        'css/bootstrap.css',
        'css/bootstrap-theme.css',
        'css/font-awesome.css',
        'css/style.css',
    ];

    public $js = [
        'js/bootstrap.min.js',
        'js/jquery.countdown.min.js',
        'js/contactform/contactform.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
    ];

    public $publishOptions = [
        'forceCopy' => (YII_DEBUG) ? true : false,
    ];
}
