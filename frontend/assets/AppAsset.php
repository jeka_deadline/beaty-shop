<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/libs.min.css',
        'css/bootstrap.min.css',
        'css/main.min.css',
    ];

    public $js = [
        'js/popper.min.js',
        'js/bootstrap.min.js',
        'js/owl.carousel.min.js',
        'js/html5shiv/html5shiv.min.js',
        'js/main.min.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'frontend\widgets\typeahead\assets\TypeaheadAsset',
        'frontend\widgets\typeahead\assets\WidgetAsset'
    ];

    public $publishOptions = [
        'forceCopy' => (YII_DEBUG) ? true : false,
    ];
}
