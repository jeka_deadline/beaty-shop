<?php

namespace frontend\modules\geo\models;

use common\models\geo\City as BaseCity;
use yii\helpers\ArrayHelper;

/**
 * Frontend city model extends common city model.
 */
class City extends BaseCity
{
    /**
     * Get array list cities by country id for select.
     *
     * @param int $countryId Country id
     * @return array
     */
    public static function getCitiesByCountryIdForDepDrop($countryId)
    {
        $cities = self::getCitiesByCountryId($countryId);

        $list = [];

        foreach ($cities as $city) {
            $list[] = [
                'id' => $city->id,
                'name' => $city->name,
            ];
        }

        return $list;
    }

    /**
     * Get list cities for some country.
     *
     * @param int $countryId Country id
     * @return static[]
     */
    public static function getCitiesByCountryId($countryId)
    {
        return static::find()
            ->where(['country_id' => $countryId])
            ->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }
}
