<?php

namespace frontend\modules\geo\models;

use common\models\geo\Country as BaseCountry;

/**
 * Frontend country model extends common country model.
 */
class Country extends BaseCountry
{
    /**
     * Germany country code.
     *
     * @var string
     */
    const GERMANY_COUNTRY_CODE = 'de';

    /**
     * Switzerland country code.
     *
     * @var string
     */
    const SWITZERLAND_COUNTRY_CODE = 'ch';

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::className(), ['country_id' => 'id']);
    }

    /**
     * Find country by id with default German.
     *
     * @static
     * @param int $id Country id.
     * @return self
     */
    public static function findByIdWithDefault($id)
    {
        $country = self::findOne($id);

        if (!$country) {
            $country = self::find()
                ->where(['like', 'code', self::GERMANY_COUNTRY_CODE])
                ->one();
        }

        return $country;
    }
}
