<?php

namespace frontend\modules\geo\controllers;

use Yii;
use yii\web\Controller;
use frontend\modules\geo\models\City;
use yii\web\Response;

/**
 * Country frontend controller.
 */
class CountryController extends Controller
{
    /**
     * Get list cities by country id for widget \kartik\depdrop\DepDrop.
     *
     * @return JSON list cities
     */
    public function actionGetCitiesFromCountry()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $response = [
            'output' => '',
            'selected' => '',
        ];

        if (isset($_POST[ 'depdrop_parents' ])) {
            $parents = $_POST[ 'depdrop_parents' ];
            if ($parents !== null) {
                $countryId = $parents[ 0 ];

                $response[ 'output' ] = City::getCitiesByCountryIdForDepDrop($countryId);;

                return $response;
            }
        }

        return $response;
    }
}
