<?php

namespace frontend\modules\blog;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\blog\controllers';

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function init()
    {
        return parent::init();
    }
}
