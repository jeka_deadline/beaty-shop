<?php

namespace frontend\modules\blog\models;


use frontend\modules\core\behaviors\LangBehavior;
use frontend\queries\ActiveQuery;
use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

class Article extends \common\models\blog\Article
{
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                [
                    'class' => LangBehavior::className(),
                    'langModel' => ArticleLangField::className(),
                    'modelForeignKey' => 'article_id',
                    'attributes' => [
                        'name',
                        'meta_title',
                        'meta_description',
                        'meta_keywords',
                        'content',
                    ],
                ]
            ]
        );
    }

    public static function find()
    {
        return new ActiveQuery(get_called_class());
    }

    public static function getRecommended($limit = 0)
    {
        $query = static::find();
        if (($slug = Yii::$app->request->get('slug'))) {
            $query->where(['<>','slug', $slug]);
        }
        $query->andWhere(['<', 'date', date('Y-m-d H:i:s')]);
        return $query->active()
            ->orderBy(new Expression('rand()'))
            ->limit($limit)
            ->all();
    }

    public static function getLatest($limit = 0)
    {
        return static::find()
            ->active()
            ->orderBy(['date' => SORT_DESC])
            ->limit($limit)
            ->all();
    }

    public function getNextArticle() {
        return Article::find()
            ->where([
                'AND',
                [
                    'OR',
                    ['<','date',$this->date],
                    [
                        'AND',
                        ['=','date',$this->date],
                        ['>','id',$this->id],
                    ]
                ],
                ['!=','id',$this->id],
            ])
            ->andWhere(['<', 'date', date('Y-m-d H:i:s')])
            ->active()
            ->orderBy(['date' => SORT_DESC])
            ->limit(1)
            ->one();
    }

    public function getPrevArticle() {
        return Article::find()
            ->where([
                'AND',
                [
                    'OR',
                    ['>','date',$this->date],
                    [
                        'AND',
                        ['=','date',$this->date],
                        ['<','id',$this->id],
                    ]
                ],
                ['!=','id',$this->id],
            ])
            ->andWhere(['<', 'date', date('Y-m-d H:i:s')])
            ->active()
            ->orderBy(['date' => SORT_ASC])
            ->limit(1)
            ->one();
    }
}
