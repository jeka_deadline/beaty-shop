<?php

use frontend\modules\core\widgets\LoadNextPage;
use yii\widgets\LinkPager;
use yii\widgets\ListView;

$this->bodyClass = 'blog-page';

?>
<div class="img-and-text__head container-fluid">
    <div class="img-and-text__main-img row">
        <h1 class="img-and-text__main-text">infinity lashes blog</h1>
        <div class="img-and-text__text"><?= Yii::t('blog', 'News, articles and everything you wanted to know'); ?></div>
    </div>
</div>
<div class="container">
    <div class="latest-article__items row list-block">
        <?= ListView::widget([
            'dataProvider' => $dataProviderArticles,
            'itemView' => '_article_item',
            'layout' => '{items}',
            'summary' => false,
            'itemOptions' => [
                'tag' => false
            ],
            'options' => [
                'tag' => false
            ],
        ]); ?>
    </div>

    <?= LoadNextPage::widget([
        'dataProvider' => $dataProviderArticles
    ]) ?>
    <?= LinkPager::widget([
        'pagination' => $dataProviderArticles->pagination,
        'disableCurrentPageButton' => true,
        'options' => [
            'class' => 'pagination d-none'
        ]
    ]) ?>
</div>
