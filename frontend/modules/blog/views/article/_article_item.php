<?php

use frontend\modules\core\models\Setting;
use yii\helpers\Url;

?>
<a class="latest-article__item col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12" href="<?= Url::to('/blog/'.$model->slug) ?>" title="Read more">
    <div class="latest-article__img">
        <img src="<?= $model->urlSmallImage ?>" alt="main-article-1">
    </div>
    <div class="latest-article__descr">
        <div class="latest-article__date-block">
            <div class="latest-article__date">
                <?= $model->date?date("M j, Y", strtotime($model->date)):'' ?>
            </div>
            <div class="latest-article__line"></div>
        </div>
        <p class="latest-article__head">
            <?= $model->name ?>
        </p>
    </div>
</a>