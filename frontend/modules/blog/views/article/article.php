<?php

use frontend\modules\blog\models\Article;
use frontend\modules\blog\widgets\SmallBlockArticles;
use yii\helpers\Url;

$this->title = $modelArticle->name;

$this->bodyClass = 'blog-article-page';

?>

<div class="blog-article__header-buttons container">
    <?php if ($modelPrevArticle): ?>
        <a class="blog-article__back-btn" href="<?= Url::to('/blog/'.$modelPrevArticle->slug) ?>">
            <?= Yii::t('blog', 'Previous Article'); ?>
            <svg class="blog-article__back-svg blog-article__back-next-svg svg">
                <use xlink:href="#arrow-new"></use>
            </svg>
        </a>
    <?php else: ?>
        <a class="blog-article__back-btn" href="<?= Url::to('/blog') ?>">
            <?= Yii::t('blog', 'Back to Blog'); ?>
            <svg class="blog-article__back-svg blog-article__back-next-svg svg">
                <use xlink:href="#arrow-new"></use>
            </svg>
        </a>
    <?php endif; ?>
    <?php if ($modelNextArticle): ?>
        <a class="blog-article__next-btn" href="<?= Url::to('/blog/'.$modelNextArticle->slug) ?>"><?= Yii::t('blog', 'Next Article'); ?>
            <svg class="blog-article__next-svg blog-article__back-next-svg svg">
                <use xlink:href="#arrow-new"></use>
            </svg>
        </a>
    <?php else: ?>
        <a class="blog-article__back-btn" href="#"></a>
    <?php endif; ?>
</div>

<div class="main container">
    <div class="row">
        <h1 class="offset-xl-1 col-xl-10"><?= $modelArticle->name ?></h1>
    </div>
    <div class="row">
        <p class="blog-article__date offset-xl-1 col-xl-10"><?= $modelArticle->date?date("M j, Y", strtotime($modelArticle->date)):'' ?></p>
    </div>
    <div class="blog-article__content">
        <?php if ($modelArticle->image): ?>
            <img src="<?= $modelArticle->urlImage ?>" alt="blog-article-img">
        <?php endif; ?>
        <?= Yii::$app->shortcodes->parse($modelArticle->content) ?>
    </div>
</div>

<?= SmallBlockArticles::widget([
    'items' => Article::getRecommended(3),
]); ?>