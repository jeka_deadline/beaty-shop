<?php

use yii\widgets\ListView;

?>

<?= ListView::widget([
    'dataProvider' => $dataProviderArticles,
    'itemView' => '_article_item',
    'layout' => '{items}',
    'summary' => false,
    'itemOptions' => [
        'tag' => false
    ],
    'options' => [
        'tag' => false
    ],
]); ?>