<?php

namespace frontend\modules\blog\controllers;

use frontend\modules\academy\models\Academy;
use frontend\modules\academy\models\forms\CourseSortForm;
use frontend\modules\academy\models\forms\RegisterForm;
use frontend\modules\blog\models\Article;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use frontend\modules\core\controllers\FrontController;
use yii\web\Response;
use frontend\modules\core\models\CorePage;
use yii\helpers\Url;

class ArticleController extends FrontController
{

    public function actionIndex()
    {
        $dataProviderArticles = new ActiveDataProvider([
            'query' => Article::find()
                ->where(['<', 'date', date('Y-m-d H:i:s')])
                ->active()
                ->orderBy(['date' => SORT_DESC]),
            'pagination' => [
                'pageSize' => 9,
                'validatePage' => false
            ],
        ]);
        if (Yii::$app->request->isAjax) {
            return $this->renderPartial('index-ajax', [
                'dataProviderArticles' => $dataProviderArticles,
            ]);
        }

        if ($page = CorePage::findBlogPage()) {
            $this->setMetaTitle($page->meta_title);
            $this->setMetaDescription($page->meta_description);
            $this->setMetaKeywords($page->meta_keywords);
        }

        return $this->render('index', [
            'dataProviderArticles' => $dataProviderArticles,
        ]);
    }

    public function actionArticle($slug)
    {
        $modelArticle = Article::find()
            ->where(['slug' => $slug])
            ->active()
            ->one();

        if (!$modelArticle) {
            throw new NotFoundHttpException('Not Course.');
        }

        $this->setMetaTitle($modelArticle->meta_title);
        $this->setMetaKeywords($modelArticle->meta_keywords);
        $this->setMetaDescription($modelArticle->meta_description);
        $this->setSocialTags($modelArticle);

        return $this->render('article', [
            'modelArticle' => $modelArticle,
            'modelNextArticle' => $modelArticle->nextArticle,
            'modelPrevArticle' => $modelArticle->prevArticle,
        ]);
    }

    /**
     * Register social tags.
     *
     * @access protected
     * @return void
     */
    protected function setSocialTags($article)
    {
        if ($article->image) {
            $this->view->registerMetaTag(['name' => 'og:image', 'content' => Url::toRoute([$article->getUrlImage()], true)]);
        } else if ($article->small_image) {
            $this->view->registerMetaTag(['name' => 'og:image', 'content' => Url::toRoute([$article->getUrlSmallImage()], true)]);
        }
    }
}
