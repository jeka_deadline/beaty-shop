<?php

namespace frontend\modules\blog\widgets;

use frontend\modules\blog\models\SliderImage;
use frontend\modules\core\models\Setting;
use Yii;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

class ShortCodeBlogSlider extends Widget
{
    public $template = 'short-code-blog-slider';

    public $id = null;
    public $name = null;

    public function run()
    {
        if (!$this->id) return '';

        $modelSlider = SliderImage::findOne($this->id);

        if (!$modelSlider) return '';

        return $this->render($this->template, [
            'modelSlider' => $modelSlider
        ]);
    }
}