<?php

use frontend\modules\core\models\Setting;
use yii\helpers\Url;

?>

<section class="container">
    <h3><?= Yii::t('blog', 'Related articles'); ?></h3>
    <div class="latest-article__items row">
        <?php foreach ($items as $item): ?>
            <a class="latest-article__item col-xl-4 col-lg-4" href="<?= Url::to(['/blog/'.$item->slug]) ?>" title="Read more">
                <div class="latest-article__img">
                    <img src="<?= $item->urlSmallImage ?>" alt="<?= $item->name ?>">
                </div>
                <div class="latest-article__descr">
                    <div class="latest-article__date-block">
                        <div class="latest-article__date"><?= $item->date?date("M j, Y", strtotime($item->date)):'' ?></div>
                        <div class="latest-article__line"></div>
                    </div>
                    <p class="latest-article__head"><?= $item->name ?></p>
                </div>
            </a>
        <?php endforeach; ?>
    </div>
    <div class="owl-carousel-latest-article__items owl-carousel owl-theme">
        <?php foreach ($items as $item): ?>
            <div class="item">
                <a href="<?= Url::to(['/blog/'.$item->slug]) ?>" title="Read more">
                    <div class="latest-article__item">
                        <div class="latest-article__img">
                            <img src="<?= $item->urlSmallImage ?>" alt="<?= $item->name ?>">
                        </div>
                        <div class="latest-article__descr">
                            <div class="latest-article__date-block">
                                <div class="latest-article__date"><?= $item->date?date("M j, Y", strtotime($item->date)):'' ?></div>
                                <div class="latest-article__line"></div>
                            </div>
                            <p class="latest-article__head"><?= $item->name ?></p>
                        </div>
                    </div>
                </a>
            </div>
        <?php endforeach; ?>
    </div>
</section>