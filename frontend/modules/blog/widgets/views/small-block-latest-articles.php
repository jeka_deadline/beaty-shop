<?php

use yii\helpers\Url;

?>

<div class="latest-article container-fluid">
    <div class="container">
        <header>
            <div class="latest-article__name">
                <?= Yii::t('core', 'latest article') ?>
            </div>
            <div class="latest-article__link">
                <a href="<?= Url::toRoute(['/blog']); ?>" title="<?= Yii::t('core', 'All Articles') ?>">
                    <?= Yii::t('core', 'All Articles') ?>
                </a>
            </div>
        </header>
        <div class="latest-article__items row">
            <?php foreach ($items as $item): ?>
                <a class="latest-article__item col-xl-4 col-lg-4" href="<?= Url::to(['/blog/'.$item->slug]) ?>" title="Read more">
                    <div class="latest-article__img">
                        <img src="<?= $item->urlSmallImage ?>" alt="<?= $item->name ?>">
                    </div>
                    <div class="latest-article__descr">
                        <div class="latest-article__date-block">
                            <div class="latest-article__date"><?= $item->date?date("M j, Y", strtotime($item->date)):'' ?></div>
                            <div class="latest-article__line"></div>
                        </div>
                        <p class="latest-article__head"><?= $item->name ?></p>
                    </div>
                </a>
            <?php endforeach; ?>
        </div>
        <div class="owl-carousel-latest-article__items owl-carousel owl-theme">
            <?php foreach ($items as $item): ?>
                <a class="item" href="<?= Url::to(['/blog/'.$item->slug]) ?>" title="Read more">
                    <div class="latest-article__item">
                        <div class="latest-article__img">
                            <img src="<?= $item->urlSmallImage ?>" alt="<?= $item->name ?>">
                        </div>
                        <div class="latest-article__descr">
                            <div class="latest-article__date-block">
                                <div class="latest-article__date"><?= $item->date?date("M j, Y", strtotime($item->date)):'' ?></div>
                                <div class="latest-article__line"></div>
                            </div>
                            <p class="latest-article__head"><?= $item->name ?></p>
                        </div>
                    </div>
                </a>
            <?php endforeach; ?>
        </div>
    </div>
</div>