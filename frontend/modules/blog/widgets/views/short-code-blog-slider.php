<?php

use frontend\modules\blog\models\SliderImage;

?>
<?php if ($modelSlider->data): ?>
    <div class="carousel slide" id="carouselBlogArticle" data-ride="carousel" data-interval="false">
        <div class="carousel-inner">
            <?php foreach ($modelSlider->data as $key => $image): ?>
                <div class="carousel-item<?= $key?'':' active'?>">
                    <img class="d-block" src="/<?= $modelSlider->pathUrl.$image ?>" alt="<?= $image ?>">
                </div>
            <?php endforeach; ?>
            <a class="carousel-control-prev" href="#carouselBlogArticle" role="button" data-slide="prev">
                <svg class="slider-arrow-left slider-arrows svg">
                    <use xlink:href="#arrow"></use>
                </svg>
                <span class="sr-only">
                    <?= Yii::t('blog', 'Previous'); ?>
                </span>
            </a>
            <a class="carousel-control-next" href="#carouselBlogArticle" role="button" data-slide="next">
                <svg class="slider-arrow-right slider-arrows svg">
                    <use xlink:href="#arrow"></use>
                </svg>
                <span class="sr-only">
                    <?= Yii::t('blog', 'Next'); ?>
                </span>
            </a>
            <ol class="carousel-indicators">
                <?php foreach ($modelSlider->data as $key => $image): ?>
                    <li<?= $key?'':' class="active"'?> data-target="#carouselBlogArticle" data-slide-to="<?= $key ?>"></li>
                <?php endforeach; ?>
            </ol>
        </div>
    </div>
<?php endif; ?>
