<?php

namespace frontend\modules\pages;

/**
 * Define module pages.
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     *
     * @var string $controllerNamespace
     */
    public $controllerNamespace = 'frontend\modules\pages\controllers';

    /**
     * {@inheritdoc}
     *
     * @var string $defaultRoute
     */
    public $defaultRoute = 'index';

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function init()
    {
        parent::init();
    }
}
