<?php

namespace frontend\modules\pages\controllers;

use Yii;
use frontend\modules\core\controllers\FrontController;
use frontend\modules\pages\models\Page;
use yii\web\NotFoundHttpException;

class IndexController extends FrontController
{
    /**
     * Display static page content.
     *
     * @param string $uri Page uri
     * @return mixed
     */
    public function actionPage($uri)
    {
        $page = Page::find()
            ->where(['active' => 1, 'uri' => $uri])
            ->one();

        if (!$page) {
            throw new NotFoundHttpException("Page not found");
        }

        // установка meta-данных для страницы
        $this->setMetaTitle($page->meta_title);
        $this->setMetaDescription($page->meta_description);
        $this->setMetaKeywords($page->meta_keywords);

        return $this->render('page', compact('page'));
    }
}
