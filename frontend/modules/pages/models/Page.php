<?php

namespace frontend\modules\pages\models;

use common\models\pages\Page as BasePage;
use DateTime;
use yii\helpers\Url;

/**
 * Frontend model page extends common model Page.
 */
class Page extends BasePage
{
    /**
     * Generate list url for sitemap
     *
     * @param string $frequently Value frequently
     * @param string $priority Value priority
     * @return array
     */
    public static function getListItemsForSitemap($frequently, $priority = '0.5')
    {
        $items = [];
        $pages = static::find()
            ->where(['active' => 1])
            ->all();

        foreach ($pages as $page) {
            $dateUpdatePage = new DateTime($page->updated_at);

            $items[] = [
                'loc'         => Url::toRoute(['/pages/index/page', 'uri' => $page->uri], true),
                'lastmod'     => $dateUpdatePage->format(DateTime::W3C),
                'changefreq'  => $frequently,
                'priority'    => $priority,
            ];
        }

        return $items;
    }
}