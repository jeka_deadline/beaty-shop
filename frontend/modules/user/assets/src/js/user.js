$(function() {
    $(document).on('click', '.account-login-page__forgot-pass', function( e ) {
        e.preventDefault();

        $.ajax({
            url: $(this).data('url'),
            method: 'get',
        }).done(function(data) {
            if (data) {
                showModal(data);
            }
        })
    });

    let flagSubmitResetPasswordForm = false;
    $(document).on('submit', '#reset-password-form', function( e ) {
        e.preventDefault();
        if (flagSubmitResetPasswordForm) return false;
        flagSubmitResetPasswordForm = true;

        let form = $(this);

        var button = form.find('button[type=submit]');

        button.attr('disabled', 'disabled');

        $.ajax({
            url: $(form).attr('action'),
            method: $(form).attr('method'),
            data: $(form).serialize(),
        }).done(function(data) {
            showModal(data);
            $(button).removeAttr('disabled', 'disabled')
            flagSubmitResetPasswordForm = false;
        })
    });

    $(document).on('submit', '#save-new-password-form', function( e ) {
        e.preventDefault();
        let form = $(this);

        $.ajax({
            url: $(form).attr('action'),
            method: $(form).attr('method'),
            data: $(form).serialize(),
            dataType: 'json'
        }).done(function(data) {
            if (data.status) {
                let modalBody = $(document).find('#core-modal .modal-body');

                if ($(modalBody).length) {
                    $(modalBody).html(data.html);

                    $(document).find('#core-modal').modal();

                    setTimeout(function() {location.href = data.redirectUrl}, 5000);
                } else {
                    $(form).replaceWith(data.html);
                }
            }
        })
    });

    $(document).on('change', '#signupform-rules', function( e ) {
        ajaxAgreeRules(this);
    });

    $(document).on('change', '#loginform-agreed', function( e ) {
        ajaxAgreeRules(this);
    });
});

function ajaxAgreeRules(checkbox)
{
    var formId = $(checkbox).closest('form').attr('id');

    $.ajax({
        url: $(checkbox).data('url'),
        method: 'get',
        data: {formId: formId}
    });
}