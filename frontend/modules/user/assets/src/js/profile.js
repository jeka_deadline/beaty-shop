var adaptiveMenuWidth = 978;

if (parseInt($(window).innerWidth()) < parseInt(adaptiveMenuWidth)) {
    showUserProfileAdaptiveMenu();
}

$(function() {

    $(window).resize(function() {
        if ($(document).find('a.back-to-menu').css('display') == 'none') {
            $(document).find('main.container').children('h1').css('display', 'block');
        } else {
            $(document).find('main.container').children('h1').css('display', 'none');
        }

        if (parseInt($(window).innerWidth()) > parseInt(adaptiveMenuWidth)) {

            showUserProfileForm();
            $(document).find('main.container').children('h1').css('display', 'block');

            if ($(document).find('.profile-and-settings').length) {
                $($(document).find('.account-page__two-parts-wrapper .list-group-item-action')[0]).addClass('active');
            }
            if ($(document).find('#list-payment').length) {
                $($(document).find('.account-page__two-parts-wrapper .list-group-item-action')[1]).addClass('active');
            }
            if ($(document).find('#list-shipping-adress').length) {
                $($(document).find('.account-page__two-parts-wrapper .list-group-item-action')[2]).addClass('active');
            }
            if ($(document).find('#list-orders').length) {
                $($(document).find('.account-page__two-parts-wrapper .list-group-item-action')[3]).addClass('active');
            }
        }
    });

    // ============================================ payment tab ===============================================

    // delete user payment card
    $(document).on('click', '.account-page__card-items .action-delete', function( e ) {
        e.preventDefault();
        var self = $(this);
        var id = $(this).data('id');

        $.ajax({
            url: $(document).find('.account-page__card-items').data('delete-url'),
            method: 'post',
            data: {id: $(this).data('id')},
            dataType: 'json',
            headers: {
                'X-CSRF-Token': yii.getCsrfToken(),
            },
        }).done(function(data) {
            if (data.status) {
                $(self).closest('.account-page__card-item').remove();
                if ($(document).find('.account-page__card-item').length == '0') {
                    $(document).find('.account-page__right-part').html(data.emptyCard);
                }
                $(document).find('#account-page__select-card option[value=' + id + ']').remove();
            }
        });
    });

    // show form create payment card
    $(document).on('click', '.account-page__payment-add-new', function( e ) {
        e.preventDefault();

        $.ajax({
            url: $(this).data('create-url'),
            method: 'get',
            dataType: 'html',
        }).done(function(data) {
            $(document).find('.account-page__right-part').html(data);
        });
    });

    // submit payment card form
    $(document).on('submit', 'form#add-new-card', function( e ) {
        e.preventDefault();

        var checkResult = check();
        if (checkResult === false) {
            return false;
        }
    });

    // submit payment card form
    $(document).on('submit', 'form#update-card', function( e ) {
        e.preventDefault();

        $.ajax({
            url: $(this).attr('action'),
            method: $(this).attr('method'),
            data: $(this).serialize(),
            dataType: 'html',
        }).done(function(data) {
            $(document).find('.account-page__right-part').html(data);
        });
    });

    // show form update payment card
    $(document).on('click', '.account-page__card-items .action-edit', function( e ) {
        e.preventDefault();
        var self = $(this);

        $.ajax({
            url: $(document).find('.account-page__card-items').data('edit-url'),
            method: 'get',
            data: {id: $(this).data('id')},
            dataType: 'html',
        }).done(function(data) {
            if (data) {
                $(document).find('.account-page__right-part').html(data);
            }
        });
    });

    // change default payment card
    $(document).on('change', '#account-page__select-card', function( e ) {
        var data = {};
        data[ $(this).attr('name') ] = $(this).val();

        $.ajax({
            url: $(this).data('change-url'),
            data: data,
            method: 'post',
            headers: {
                'X-CSRF-Token': yii.getCsrfToken(),
            },
        }).done(function(data) {
            console.log(data);
        });
    });

    // ================================================= shipping tab ====================================

    // show form create shipping information
    $(document).on('click', '.account-page__shipping-adress-new button, .account-page__shipping-address-ready button', function( e ) {
        e.preventDefault();

        $.ajax({
            url: $(this).data('create-url'),
            method: 'get',
            dataType: 'html',
        }).done(function(data) {
            $(document).find('.account-page__right-part').html(data);
        });
    });

    // submit shipping information form
    $(document).on('submit', 'form#add-new-adress', function( e ) {
        e.preventDefault();

        $.ajax({
            url: $(this).attr('action'),
            method: $(this).attr('method'),
            data: $(this).serialize(),
            dataType: 'html',
        }).done(function(data) {
            if (data) {
                $(document).find('.account-page__right-part').html(data);
            }
        })
    });

    // show form update shipping information
    $(document).on('click', '.account-page__shipping-address-items .action-edit', function( e ) {
        e.preventDefault();
        var self = $(this);

        $.ajax({
            url: $(document).find('.account-page__shipping-address-items').data('edit-url'),
            method: 'get',
            data: {id: $(this).data('id')},
            dataType: 'html',
        }).done(function(data) {
            if (data) {
                $(document).find('.account-page__right-part').html(data);
            }
        });
    });

    // delete user shipping information
    $(document).on('click', '.account-page__shipping-address-item .action-delete', function( e ) {
        e.preventDefault();
        var self = $(this);
        var id = $(this).data('id');

        $.ajax({
            url: $(document).find('.account-page__shipping-address-items').data('delete-url'),
            method: 'post',
            data: {id: $(this).data('id')},
            dataType: 'json',
            headers: {
                'X-CSRF-Token': yii.getCsrfToken(),
            },
        }).done(function(data) {
            if (data.status) {
                $(self).closest('.account-page__shipping-address-item').remove();
                if ($(document).find('.account-page__shipping-address-item').length == '0') {
                    $(document).find('.account-page__right-part').html(data.emptyCard);
                }
                $(document).find('#account-page__select-home option[value=' + id + ']').remove();
            }
        });
    });

    // change default shipping information
    $(document).on('change', '#account-page__select-home', function( e ) {
        var data = {};
        data[ $(this).attr('name') ] = $(this).val();

        $.ajax({
            url: $(this).data('change-url'),
            data: data,
            method: 'post',
            headers: {
                'X-CSRF-Token': yii.getCsrfToken(),
            },
        }).done(function(data) {
            console.log(data);
        });
    });

    // ==================================================== Tabs ==========================

    // changes tabs in page profile and settings
    $(document).on('click', '.account-page__two-parts-wrapper a.list-group-item-action', function ( e ) {
        if ($(this).attr('id') == 'list-logout-list') {
            return null;
        }

        let link = $(this);

        e.preventDefault();

        if ($(this).hasClass('active')) {
            return null;
        }

        $.ajax({
            url: $(this).data('url'),
            method: 'get',
            dataType: 'html',
        }).done(function(data) {
            $(document).find('.account-page__two-parts-wrapper a.list-group-item-action.active').removeClass('active');
            $(document).find('.account-page__right-part').html(data);
            $(link).addClass('active');

            showUserProfileForm();
        })
    });

    $(document).on('click', 'a.back-to-menu', function( e ) {
        e.preventDefault();

        $(this).css('display', 'none');
        $(document).find('main.container').children('h1').css('display', 'block');
        showUserProfileAdaptiveMenu();
    });

    $(document).on('click', '.account-page__unsubscribe-link', function( e ) {
        e.preventDefault();

        $.ajax({
            url: $(this).attr('href'),
            method: 'post',
            dataType: 'json',
            headers: {
                'X-CSRF-Token': yii.getCsrfToken(),
            },
        }).done(function(data) {
            if (data.status) {
                $('.unsubscribed-icon').css('visibility', 'visible');
                $('.unsubscribed-icon').css('opacity', '1');
            }
        });
    });

    // changes tabs in page profile and settings
    $(document).on('click', 'a.btn-cancel', function ( e ) {
        e.preventDefault();

        $.ajax({
            url: $(this).data('url'),
            method: 'get',
            dataType: 'html',
        }).done(function(data) {
            $(document).find('.account-page__right-part').html(data);
        });
    });
});

function showUserProfileForm()
{
    $(document).find('.account-page__left-part').removeClass('col-sm-12');
    $(document).find('.account-page__left-part > div').attr('id', 'account-page__list-tab');
    $(document).find('.account-page__right-part').css('display', 'block');
    $(document).find('a.back-to-menu').css('display', 'inline');

    if (parseInt($(window).innerWidth()) > parseInt(adaptiveMenuWidth)) {
        $(document).find('main.container').children('h1').css('display', 'block');
    } else {
        $(document).find('main.container').children('h1').css('display', 'none');
    }
}

function showUserProfileAdaptiveMenu()
{
    $(document).find('.account-page__left-part').addClass('col-sm-12');
    $(document).find('.account-page__left-part > div').attr('id', 'account-page__list-tab-show');
    $(document).find('.account-page__right-part').css('display', 'none');
    $(document).find('a.back-to-menu').css('display', 'none');
    $(document).find('main.container').children('h1').css('display', 'block');
    $(document).find('.account-page__two-parts-wrapper a.list-group-item-action.active').removeClass('active');
}

function check() { // Function called by submitting PAY-button
    if (window.iframes.isComplete()) {
        window.iframes.creditCardCheck('checkCallback'); // Perform "CreditCardCheck" to create and get a
        // PseudoCardPan; then call your function
        "checkCallback"
    } else {
        return false;
    }
}

function checkCallback(response) {
    console.debug(response);
    if (response.status === "VALID") {
        document.getElementById("pseudocardpan").value = response.pseudocardpan;
        document.getElementById("truncatedcardpan").value = response.truncatedcardpan;
        sendCardForm();
    }
}

function sendCardForm()
{
    var form = $(document).find('#add-new-card');

    $.ajax({
        url: $(form).attr('action'),
        method: $(form).attr('method'),
        data: $(form).serialize(),
        dataType: 'html',
    }).done(function(data) {
        if (data) {
            $(document).find('.account-page__right-part').html(data);
        }
    })
}