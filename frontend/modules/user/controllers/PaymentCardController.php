<?php

namespace frontend\modules\user\controllers;

use Yii;
use yii\web\Controller;
use frontend\modules\user\models\User;
use frontend\modules\user\models\UserPaymentCard;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use frontend\modules\user\models\forms\PaymentCardForm;
use yii\web\Response;
use yii\helpers\ArrayHelper;
use frontend\modules\user\models\forms\DefaultUserPaymentCardForm;

/**
 * Frontend controller for user payment cards.
 */
class PaymentCardController extends Controller
{
    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
               'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
        ];
    }

    /**
     * Get payment tab in account.
     *
     * @return mixed
     */
    public function actionGetIndexPaymentPage()
    {
        $userIdentityId = Yii::$app->user->identity->id;
        $user = User::find()
            ->with('profile')
            ->where(['id' => $userIdentityId])
            ->one();

        $paymentCards = UserPaymentCard::findAllPaymentCardsByUserId($userIdentityId);
        $defaultUserPaymentCardForm = new DefaultUserPaymentCardForm($userIdentityId, $paymentCards);

        if (!$paymentCards) {
            return $this->renderPartial('empty-payment-cards');
        }

        return $this->renderAjax('list-payment-cards', compact('paymentCards', 'defaultUserPaymentCardForm'));
    }

    /**
     * Show user payment card create form..
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $paymentCardForm = new PaymentCardForm(Yii::$app->user->identity->id);

        return $this->renderAjax('create-payment-card', compact('paymentCardForm'));
    }

    /**
     * Create new user payment card.
     *
     * @return mixed
     */
    public function actionStore()
    {
        $userIdentityId = Yii::$app->user->identity->id;
        $paymentCardForm = new PaymentCardForm($userIdentityId);

        if ($paymentCardForm->load(Yii::$app->request->post()) && $paymentCardForm->createPaymentCard()) {

            $paymentCards = UserPaymentCard::findAllPaymentCardsByUserId($userIdentityId);
            $defaultUserPaymentCardForm = new DefaultUserPaymentCardForm($userIdentityId, $paymentCards);

            return $this->renderAjax('list-payment-cards', compact('paymentCards', 'defaultUserPaymentCardForm'));
        }

        return $this->renderAjax('create-payment-card', compact('paymentCardForm'));
    }

    /**
     * Delete payment card for auth user.
     *
     * @return string
     */
    public function actionDelete()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        try {
            $card = $this->findModel(Yii::$app->request->post('id'));
        } catch (NotFoundHttpException $e) {
            return ['status' => false];
        }

        $card->delete();

        return [
            'status' => true,
            'emptyCard' => $this->renderPartial('empty-payment-cards'),
        ];
    }

    /**
     * Show user payment card form edit
     *
     * @param int $id User payment card id
     * @return mixed
     */
    public function actionEdit($id)
    {
        try {
            $card = $this->findModel($id);
        } catch (NotFoundHttpException $e) {
            return null;
        }

        $paymentCardForm = new PaymentCardForm(Yii::$app->user->identity->id, $card);

        return $this->renderAjax('update-payment-card', compact('paymentCardForm'));
    }

    /**
     * Update user payment card.
     *
     * @param int $id User payment card id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        try {
            $card = $this->findModel($id);
        } catch (NotFoundHttpException $e) {
            return $this->renderAjax('update-payment-card', compact('paymentCardForm'));
        }

        $userIdentityId = Yii::$app->user->identity->id;

        $paymentCardForm = new PaymentCardForm($userIdentityId, $card);

        if ($paymentCardForm->load(Yii::$app->request->post()) && $paymentCardForm->updatePaymentCard()) {

            $paymentCards = UserPaymentCard::findAllPaymentCardsByUserId($userIdentityId);
            $defaultUserPaymentCardForm = new DefaultUserPaymentCardForm($userIdentityId, $paymentCards);

            return $this->renderAjax('list-payment-cards', compact('paymentCards', 'defaultUserPaymentCardForm'));
        }

        return $this->renderAjax('update-payment-card', compact('paymentCardForm'));
    }

    /**
     * Change user default payment card for auth user.
     *
     * @return string
     */
    public function actionChangeUserDefaultCard()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $userIdentityId = Yii::$app->user->identity->id;
        $paymentCards = UserPaymentCard::findAllPaymentCardsByUserId($userIdentityId);
        $defaultUserPaymentCardForm = new DefaultUserPaymentCardForm($userIdentityId, $paymentCards);

        if ($defaultUserPaymentCardForm->load(Yii::$app->request->post())) {
            $defaultUserPaymentCardForm->setDefaultCard();

            return [
                'status' => true,
            ];
        }

        return [
            'status' => false,
        ];
    }

    /**
     * Ajax validation payment card form.
     *
     * @return JSON response validation result
     */
    public function actionAjaxValidationPaymentCard()
    {
        $paymentCardForm = new PaymentCardForm(Yii::$app->user->identity->id);

        if (Yii::$app->request->isAjax && $paymentCardForm->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return \yii\widgets\ActiveForm::validate($paymentCardForm);
        }
    }

    /**
     * Find payment card identity user by id card.
     *
     * @throws \yii\web\NotFoundHttpException If user payment card not found
     * @param int $id User payment card id
     * @return \frontend\modules\user\models\UserPaymentCard
     */
    private function findModel($id)
    {
        $model = UserPaymentCard::findOne(['id' => $id, 'user_id' => Yii::$app->user->identity->id]);

        if (!$model) {
            throw new NotFoundHttpException('Payment card not found');
        }

        return $model;
    }
}