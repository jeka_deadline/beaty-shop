<?php

namespace frontend\modules\user\controllers;

use StdClass;
use Yii;
use yii\filters\AccessControl;
use frontend\modules\core\controllers\FrontController;
use yii\filters\VerbFilter;
use frontend\modules\user\models\forms\LoginForm;
use frontend\modules\user\models\forms\SignupForm;
use frontend\modules\user\models\Social;
use frontend\modules\user\models\forms\UpdateEmailTokenForm;
use frontend\modules\user\models\forms\PasswordResetRequestForm;
use frontend\modules\user\models\forms\ResetPasswordForm;
use frontend\modules\user\models\UserEmailToken;
use yii\helpers\Url;
use frontend\modules\user\models\User;
use yii\web\Response;
use frontend\widgets\bootstrap4\ActiveForm;
use frontend\modules\core\models\Setting;
use frontend\modules\core\models\Subscriber;
use frontend\modules\core\models\CorePage;

/**
 * Frontend controller for user securities.
 */
class SecurityController extends FrontController
{

    public static $distributorSessionKey = 'distributor-key';
    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'login',
                            'auth',
                            'signup',
                            'confirm-email',
                            'update-user-confirm-email-token',
                            'request-password-reset',
                            'reset-password',
                            'generate-new-reset-password-token',
                            'validate-signup-form',
                            'set-social-agree',
                        ],
                        'allow'   => true,
                    ],
                    [
                        'actions' => ['logout'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function actions()
    {
        return [
            'auth' => [
                'class'           => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'authenticate'],
                'successUrl'      => Url::to(['/']),
            ],
        ];
    }

    /**
     * Callback to register and auth in social.
     *
     * @param
     * @return boolean
     */
    public function authenticate($client)
    {
        if (Yii::$app->user->isGuest) {

            $attributes = $client->getUserAttributes();

            if ($client instanceof \yii\authclient\clients\Google) {
                $objectUser = \frontend\modules\user\models\authclient\GoogleClient::getUserAttributes($attributes);
            }
            if ($client instanceof \yii\authclient\clients\Facebook) {
                $objectUser = \frontend\modules\user\models\authclient\FacebookClient::getUserAttributes($attributes);
            }

            $objectUser->source = $client->getId();

            if (!empty($objectUser->email)) {
                $user = User::findByEmail($objectUser->email);
            } else {
               return false;
            }

            if (!$user) {

                $response = $this->checkSocialAgreeRules();

                if (!$response->status) {
                    Yii::$app->session->set('error-social', $response);

                    return $this->redirect(['login']);
                }

                $distributorCode = (Yii::$app->session->has(self::$distributorSessionKey)) ? Yii::$app->session->get(self::$distributorSessionKey) : null;

                $user = User::createSocial($objectUser, $distributorCode);

                if (!$user) {
                    return false;
                }
            }

            if ($user->blocked_at) {
                $this->setFlash('danger', 'You is blocked');
                return false;
            }

            if (Yii::$app->getUser()->login($user)) {

                if (Yii::$app->session->has(self::$distributorSessionKey)) {
                    Yii::$app->session->remove(self::$distributorSessionKey);
                }
                return true;
            } else {
                return false;
            }
        }

        if (Yii::$app->session->has(self::$distributorSessionKey)) {
            Yii::$app->session->remove(self::$distributorSessionKey);
        }

        return true;
    }

    /**
     * Login user in shop.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        $session = Yii::$app->session;

        if ($session->has('agree-rules-' . LoginForm::FORM_ID)) {
            $session->remove('agree-rules-' . LoginForm::FORM_ID);
        }

        if ($session->has('agree-rules-' . SignupForm::FORM_ID)) {
            $session->remove('agree-rules-' . SignupForm::FORM_ID);
        }

        if ($session->has('social-action')) {
            $session->remove('social-action');
        }

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        };

        $loginForm = new LoginForm();
        $signupForm = new SignupForm();

        if ($session->has('error-social')) {
            $responseObject = $session->get('error-social');

            if ($responseObject->form === LoginForm::FORM_ID) {
                $loginForm->addError('agreed', $responseObject->error);
            }

            if ($responseObject->form === SignupForm::FORM_ID) {
                $signupForm->addError('rules', $responseObject->error);
            }

            $session->remove('error-social');
        }

        Yii::$app->session->set(self::$distributorSessionKey, Yii::$app->request->get('distributor_code', null));

        if ($loginForm->load(Yii::$app->request->post()) && $loginForm->login()) {

            return $this->goHome();
        }

        $loginForm->resetPasswordField();

        if ($page = CorePage::findLoginPage()) {
            $this->setMetaTitle($page->meta_title);
            $this->setMetaDescription($page->meta_description);
            $this->setMetaKeywords($page->meta_keywords);
        }

        return $this->render('login', compact('loginForm', 'signupForm'));
    }

    /**
     * {@inheritdoc}
     *
     * @param yii\base\Action $action The action just executed.
     * @param mixed $result The action return result.
     * @return mixed
     */
    public function afterAction($action, $result)
    {
        if ($action->id === 'auth') {
            $session = Yii::$app->session;

            $session->set('social-action', Yii::$app->request->get('action', null));
        }

        return parent::afterAction($action, $result);
    }

    /**
     * Action agree rules for social.
     *
     * @param string $formId Form id.
     * @return void
     */
    public function actionSetSocialAgree($formId)
    {
        $session = Yii::$app->session;

        if ($session->has('agree-rules-' . $formId)) {
            $session->remove('agree-rules-' . $formId);
        } else {
            $session->set('agree-rules-' . $formId, true);
        }
    }

    /**
     * Logout user.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Sign up user in shop.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $response = [
            'status' => false,
            'text' => null,
        ];

        Yii::$app->response->format = Response::FORMAT_JSON;

        $signupForm = new SignupForm();

        $signupForm->distributorCode = (Yii::$app->session->has(self::$distributorSessionKey)) ? Yii::$app->session->get(self::$distributorSessionKey) : null;

        if ($signupForm->load(Yii::$app->request->post())) {
            $response[ 'status' ] = $signupForm->signup();

            if ($response[ 'status' ]) {
                $response[ 'text' ] = 'Sie sind erfolgreich angemeldet! Bitte prüfen Sie Ihr E-Mail Postfach.';
                if (Yii::$app->session->has(self::$distributorSessionKey)) {
                    Yii::$app->session->remove(self::$distributorSessionKey);
                }
            } else {
                $response[ 'text' ] = Yii::t('core', 'Something went wrong, try again later');
            }
        }

        return $response;
    }

    /**
     * Ajax validation signup form.
     *
     * @return JSON response validation result
     */
    public function actionValidateSignupForm()
    {
        $signupForm = new SignupForm();

        if (Yii::$app->request->isAjax && $signupForm->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($signupForm);
        }
    }

    /**
     * Confirm user email.
     *
     * @param string $token Code for confirm email.
     * @return string
     */
    public function actionConfirmEmail($token)
    {
        $token = UserEmailToken::findOne(['token' => $token]);

        if (!$token) {
            throw new \Exception("Not found token");
        }

        if ($token->isTokenExpired()) {
            return $this->redirect(['update-user-confirm-email-token', 'token' => $token->token]);
        }

        $token->user->confirmEmail();
        $token->user->save();

        if (Subscriber::hasEmail($token->user->email)) {
            Subscriber::assignUser($token->user->email, $token->user->id);
        }

        Yii::$app->session->set(Setting::MODAL_SESSION_KEY, 'Email bestätigt. Loggen Sie sich ein und geniessen Sie alle Vorteile für registrierte Kunden in unserem Shop. Vielen Dank und viel Spass beim Einkaufen!');

        return $this->goHome();
    }

    /**
     * Display page for user if user confirm email token expired or if user in new.
     *
     * @return mixed
     */
    public function actionUpdateUserConfirmEmailToken($token)
    {
        $model = new UpdateEmailTokenForm($token);

        if (Yii::$app->request->isPost && Yii::$app->request->isAjax) {

            $response = [
                'status' => false,
                'text' => Yii::t('core', 'An error occurred, try again later'),
            ];

            if ($model->createResetEmailToken()) {
                $response[ 'status' ] = true;
                $response[ 'text' ] = Yii::t('core', 'Your token is re-created, check your email');
            }

            Yii::$app->response->format = Response::FORMAT_JSON;

            return $response;
        }

        return $this->render('update-user-email-token', compact('token'));
    }

    /**
     * Display page request reset user password.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();

        if ($model->load(Yii::$app->request->post()) && $model->createResetPasswordToken()) {
            return $this->renderPartial('success-request-reset-password');
        }

        return $this->renderAjax('request-password-reset-token', [
            'model' => $model,
        ]);
    }

    /**
     * Display page reset password and enter new password.
     *
     * @return mixed
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $response = [
                'status' => false,
                'html' => null,
            ];

            if ($model->validate() && $model->resetPassword()) {
                $response[ 'status' ] = true;
                $response[ 'html' ] = $this->renderPartial('success-save-new-password');
                $response[ 'redirectUrl' ] = Url::toRoute(['/user/security/login']);
            } else {
                $response[ 'html' ] = $this->renderAjax('reset-password-form', compact('model'));
            }

            return $response;
        }

        return $this->render('reset-password', [
            'model' => $model,
        ]);
    }

    /**
     * Generate user new reset password token.
     *
     * @return string
     */
    public function actionGenerateNewResetPasswordToken($token)
    {
        try {
            $model = new ResetPasswordForm($token);

        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if (Yii::$app->request->isPost && Yii::$app->request->isAjax) {

            $response = [
                'status' => false,
                'text' => Yii::t('core', 'An error occurred, try again later'),
            ];

            if ($model->regenerateResetToken()) {
                $response[ 'status' ] = true;
                $response[ 'text' ] = Yii::t('core', 'Your token is re-created, check your email');
            }

            Yii::$app->response->format = Response::FORMAT_JSON;

            return $response;
        }

        return $this->redirect(['login']);
    }

    /**
     * Set flash message.
     *
     * @param string $type Type message
     * @param string $message Text message
     * @return void
     */
    private function setFlash($type, $message)
    {
        //Yii::$app->session->setFlash($type, $message);
    }

    /**
     * Check social agree rules.
     *
     * @access private
     * @return object
     */
    private function checkSocialAgreeRules()
    {
        $object = new StdClass();
        $object->form = null;
        $object->status = false;
        $object->error = null;

        $session = Yii::$app->session;

        $action = $session->get('social-action');

        if (!$action) {
            return $object;
        }

        if ($action === LoginForm::FORM_SOCIAL_ACTION && !$session->get('agree-rules-' . LoginForm::FORM_ID)) {
            $object->form = LoginForm::FORM_ID;
            $object->error = Yii::t('core', 'The checkbox must be checked');

            return $object;
        }

        if ($action === SignupForm::FORM_SOCIAL_ACTION && !$session->get('agree-rules-' . SignupForm::FORM_ID)) {
            $object->form = SignupForm::FORM_ID;
            $object->error = Yii::t('core', 'The checkbox must be checked');

            return $object;
        }

        $object->status = true;

        return $object;
    }
}
