<?php

namespace frontend\modules\user\controllers;

use Yii;
use frontend\modules\core\controllers\FrontController;
use frontend\modules\user\models\User;
use frontend\modules\user\models\forms\ProfileForm;
use frontend\modules\user\models\forms\ChangeProfilePasswordForm;
use yii\filters\AccessControl;
use frontend\modules\user\models\UserPaymentCard;
use yii\helpers\ArrayHelper;
use frontend\modules\user\models\forms\DefaultUserPaymentCardForm;
use frontend\modules\user\models\forms\DefaultUserShippingForm;
use frontend\modules\user\models\UserShippingAddress;
use frontend\modules\product\models\Order;
use yii\web\Response;
use yii\web\MethodNotAllowedHttpException;
use frontend\modules\core\models\CorePage;

/**
 * Frontend controller for user.
 */
class UserController extends FrontController
{
    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
               'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
        ];
    }

    /**
     * Display user account page.
     *
     * @return mixed
     */
    public function actionAccount()
    {
        $userIdentityId = Yii::$app->user->identity->id;
        $user = User::find()
            ->with('profile')
            ->where(['id' => $userIdentityId])
            ->one();

        $profileForm = new ProfileForm($user);
        $changePasswordForm = new ChangeProfilePasswordForm($user);

        if ($page = CorePage::findUserAccountPage()) {
            $this->setMetaTitle($page->meta_title);
            $this->setMetaDescription($page->meta_description);
            $this->setMetaKeywords($page->meta_keywords);
        }

        return $this->render('account', compact('user', 'profileForm', 'changePasswordForm'));
    }

    /**
     * Get profile tab in account.
     *
     * @return mixed
     */
    public function actionGetIndexProfilePage()
    {
        $userIdentityId = Yii::$app->user->identity->id;
        $user = User::find()
            ->with('profile')
            ->where(['id' => $userIdentityId])
            ->one();

        $profileForm = new ProfileForm($user);
        $changePasswordForm = new ChangeProfilePasswordForm($user);

        return $this->renderAjax('profile-form-tab', compact('user', 'profileForm', 'changePasswordForm'));
    }

    /**
     * Update user profile.
     *
     * @return mixed
     */
    public function actionUpdateProfile()
    {
        $userIdentityId = Yii::$app->user->identity->id;
        $user = User::find()
            ->with('profile')
            ->where(['id' => $userIdentityId])
            ->one();

        $profileForm = new ProfileForm($user);
        $changePasswordForm = new ChangeProfilePasswordForm($user);

        if ($profileForm->load(Yii::$app->request->post()) && $changePasswordForm->load(Yii::$app->request->post())) {
            $valid = $profileForm->validate();
            $valid = $changePasswordForm->validate() && $valid;

            if ($valid && $profileForm->updateProfile($changePasswordForm)) {
                return $this->redirect('account');
            }
        }

        $changePasswordForm->resetInputPasswords();

        return $this->render('account', compact('user', 'profileForm', 'changePasswordForm'));
    }

    /**
     * Unsubscribe identity user.
     *
     * @throws MethodNotAllowedHttpException If method not equal post
     * @return void
     */
    public function actionUnsubscribe()
    {
        if (Yii::$app->request->isPost) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $user = $this->findIdentityUserModel();

            $user->unSubscribe();

            return [
                'status' => true,
            ];
        }

        if (Yii::$app->request->isAjax) {
            return [
              'status' => false,
            ];
        }

        throw new MethodNotAllowedHttpException("Method not allowew");
    }

    /**
     * Find model identity user.
     *
     * @return \frontend\modules\user\models\User
     */
    private function findIdentityUserModel()
    {
        $userIdentityId = Yii::$app->user->identity->id;
        $user = User::find()
            ->where(['id' => $userIdentityId])
            ->one();

        return $user;
    }
}
