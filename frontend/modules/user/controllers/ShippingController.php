<?php

namespace frontend\modules\user\controllers;

use Yii;
use frontend\modules\user\models\User;
use yii\web\Controller;
use frontend\modules\user\models\UserShippingAddress;
use frontend\modules\user\models\forms\UserShippingInformationForm;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\helpers\ArrayHelper;
use frontend\modules\user\models\forms\DefaultUserShippingForm;

/**
 * Frontend controller for shipping.
 */
class ShippingController extends Controller
{
    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
               'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
        ];
    }

    /**
     * Get shipping tab in account.
     *
     * @return mixed
     */
    public function actionGetIndexShippingPage()
    {
        $userIdentityId = Yii::$app->user->identity->id;
        $user = User::find()
            ->with('profile')
            ->where(['id' => $userIdentityId])
            ->one();

        $shippingAddresses = UserShippingAddress::findAllAddressesForUser($userIdentityId);
        $defaultUserShippingForm = new DefaultUserShippingForm($userIdentityId, $shippingAddresses);

        if (!$shippingAddresses) {
            return $this->renderPartial('empty-shipping-addresses');
        }


        return $this->renderAjax('list-shipping-addresses', compact('shippingAddresses', 'defaultUserShippingForm'));
    }

    /**
     * Show user shipping information create form..
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $shippingForm = new UserShippingInformationForm(Yii::$app->user->identity->id);

        return $this->renderAjax('create-shipping-address', compact('shippingForm'));
    }

    /**
     * Create new user shipping information.
     *
     * @return mixed
     */
    public function actionStore()
    {
        $userIdentityId = Yii::$app->user->identity->id;
        $shippingForm = new UserShippingInformationForm($userIdentityId);

        if ($shippingForm->load(Yii::$app->request->post()) && $shippingForm->createNewShippingAddress()) {

            $shippingAddresses = UserShippingAddress::findAllAddressesForUser($userIdentityId);
            $defaultUserShippingForm = new DefaultUserShippingForm($userIdentityId, $shippingAddresses);

            return $this->renderAjax('list-shipping-addresses', compact('shippingAddresses', 'defaultUserShippingForm'));
        }

        return $this->renderAjax('create-shipping-address', compact('shippingForm'));
    }

    /**
     * Delete shipping informationfor auth user.
     *
     * @return string
     */
    public function actionDelete()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        try {
            $shipping = $this->findModel(Yii::$app->request->post('id'));
        } catch (NotFoundHttpException $e) {
            return ['status' => false];
        }

        $shipping->delete();

        return [
            'status' => true,
            'emptyCard' => $this->renderPartial('empty-shipping-addresses'),
        ];
    }

    /**
     * Show user shipping information form edit
     *
     * @param int $id User shipping information id
     * @return mixed
     */
    public function actionEdit($id)
    {
        try {
            $shipping = $this->findModel($id);
        } catch (NotFoundHttpException $e) {
            return null;
        }

        $shippingForm = new UserShippingInformationForm(Yii::$app->user->identity->id, $shipping);

        return $this->renderAjax('update-shipping-address', compact('shippingForm'));
    }

    /**
     * Update user shipping information.
     *
     * @param int $id User shipping information id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        try {
            $shipping = $this->findModel($id);
        } catch (NotFoundHttpException $e) {
            return $this->renderAjax('update-shipping-address', compact('shippingForm'));
        }

        $userIdentityId = Yii::$app->user->identity->id;

        $newShippingForm = new UserShippingInformationForm($userIdentityId);
        $shippingForm = new UserShippingInformationForm($userIdentityId, $shipping);

        if ($shippingForm->load(Yii::$app->request->post())) {

            $newShippingForm->load(Yii::$app->request->post());
            $shippingForm->userAddress2 = $newShippingForm->userAddress2;

            if ($shippingForm->updateShippingAddress()) {
                $shippingAddresses = UserShippingAddress::findAllAddressesForUser($userIdentityId);
                $defaultUserShippingForm = new DefaultUserShippingForm($userIdentityId, $shippingAddresses);

                return $this->renderAjax('list-shipping-addresses', compact('shippingAddresses', 'defaultUserShippingForm'));
            }
        }

        return $this->renderAjax('update-shipping-address', compact('shippingForm'));
    }

    /**
     * Change user default shipping information for auth user.
     *
     * @return string
     */
    public function actionChangeUserDefaultShipping()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $userIdentityId = Yii::$app->user->identity->id;
        $shippingAddresses = UserShippingAddress::findAllAddressesForUser($userIdentityId);
        $defaultUserShippingForm = new DefaultUserShippingForm($userIdentityId, $shippingAddresses);

        if ($defaultUserShippingForm->load(Yii::$app->request->post())) {
            $defaultUserShippingForm->setDefaultShippingAddress();

            return [
                'status' => true,
            ];
        }

        return [
            'status' => false,
        ];
    }

    /**
     * Ajax validation shipping information form.
     *
     * @return JSON response validation result
     */
    public function actionAjaxValidationShipping()
    {
        $shippingForm = new UserShippingInformationForm(Yii::$app->user->identity->id);

        if (Yii::$app->request->isAjax && $shippingForm->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return \yii\widgets\ActiveForm::validate($shippingForm);
        }
    }

    /**
     * Find user shipping information identity user by id shipping information.
     *
     * @throws \yii\web\NotFoundHttpException If user shipping information not found
     * @param int $id User shipping information id
     * @return \frontend\modules\user\models\UserShippingAddress
     */
    private function findModel($id)
    {
        $model = UserShippingAddress::findOne(['id' => $id, 'user_id' => Yii::$app->user->identity->id]);

        if (!$model) {
            throw new NotFoundHttpException('Shipping information not found');
        }

        return $model;
    }
}
