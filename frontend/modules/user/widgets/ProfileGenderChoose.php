<?php

namespace frontend\modules\user\widgets;

use yii\widgets\InputWidget;
use frontend\modules\user\models\User;
use yii\helpers\Html;

class ProfileGenderChoose extends InputWidget
{
    public function run()
    {
        $listSex = array_reverse(User::getListSex());

        return Html::activeRadioList($this->model, $this->attribute, $listSex, ['item' => function ($index, $label, $name, $checked, $value) {
            $beginWrapper = '<div class="custom-control custom-radio custom-control-inline">';
            $endWrapper = '</div>';
            $radioOptions = [
                'class' => 'custom-control-input',
                'value' => $value,
            ];
            $labelOptions = ['class' => 'custom-control-label'];
            $labelFor = '';

            if ($index == 0) {
                $radioOptions[ 'id' ] = 'account-page__radio-female';
                $labelFor = 'account-page__radio-female';
            } else {
                $radioOptions[ 'id' ] = 'account-page__radio-male';
                $labelFor = 'account-page__radio-male';
            }

            return $beginWrapper . Html::radio($name, $checked, $radioOptions) . Html::label($label, $labelFor, $labelOptions) . $endWrapper;

        }]);
    }
}