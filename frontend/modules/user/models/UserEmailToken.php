<?php

namespace frontend\modules\user\models;

use Yii;
use common\models\user\UserEmailToken as BaseUserEmailToken;

class UserEmailToken extends BaseUserEmailToken
{
    const MINUNES_VALID_TOKEN = 30;

    public function isTokenExpired()
    {
        $tokenTime = strtotime($this->updated_at);
        $now       = time();

        $diff = $now - $tokenTime;

        if ($diff < self::MINUNES_VALID_TOKEN * 60) {
            return false;
        }

        return true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
