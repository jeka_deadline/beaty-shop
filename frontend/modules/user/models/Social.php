<?php

namespace frontend\modules\user\models;

use Yii;
use common\models\user\Social as BaseSocial;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class Social extends BaseSocial
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
