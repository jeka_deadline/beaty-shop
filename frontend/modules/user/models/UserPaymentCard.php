<?php

namespace frontend\modules\user\models;

use common\models\user\UserPaymentCard as BaseUserPaymentCard;
use frontend\modules\product\models\forms\CheckoutPaymentForm;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * Front user payment class extends common user payment class.
 */
class UserPaymentCard extends BaseUserPaymentCard
{
    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * Find all payment cards for some user.
     *
     * @param int $userId User id
     * @return \frontend\modules\user\models\UserPaymentCard[]
     */
    public static function findAllPaymentCardsByUserId($userId)
    {
        return self::find()
            ->where(['user_id' => $userId])
            ->all();
    }

    /**
     * Save user payment card from checkout form.
     *
     * @param \frontend\modules\product\models\forms\CheckoutPaymentForm $checkoutPaymentForm Checkout payment form
     * @param int $userId User auth id
     * @return bool
     */
    public static function savePaymentCardFromCheckout(CheckoutPaymentForm $checkoutPaymentForm, $userId)
    {
        $model = new static();
        $model->number = $checkoutPaymentForm->number;
        $model->pseudocardpan = $checkoutPaymentForm->pseudocardpan;
        $model->user_id = $userId;

        $model->name = self::generateSavedPaymentCardName();

        if ($model->validate() && $model->save()) {
            return true;
        }

        return false;
    }

    /**
     * Get type card by number card.
     *
     * @return string
     */
    public function getTypeCard()
    {
        if (preg_match('#^4[0-9]{6,}$#', $this->number)) {
            return 'VISA';
        }

        if (preg_match('#^5[1-5][0-9]{5,}|222[1-9][0-9]{3,}|22[3-9][0-9]{4,}|2[3-6][0-9]{5,}|27[01][0-9]{4,}|2720[0-9]{3,}$#', $this->number)) {
            return 'MasterCard';
        }

        return '';
    }

    /**
     * Find default user payment card.
     *
     * @param int $userId User id
     * @return static
     */
    public static function getUserDefaultPaymentCard($userId)
    {
        return static::find()
            ->where(['user_id' => $userId, 'default' => 1])
            ->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Generate name for saved payment card
     *
     * @return string
     */
    private static function generateSavedPaymentCardName()
    {
        return 'Payment card (' . date('Y-m-d H:i:s') . ')';
    }
}
