<?php

namespace frontend\modules\user\models\authclient;

class FacebookClient extends AbstractClient
{
    /**
     * Generate user information from google social account
     *
     * @param array $socialAttributes User socila attributes
     * @return \frontend\modules\user\models\authclient\Client
     */
    public static function getUserAttributes($socialAttributes)
    {
        $facebookFullName = isset($socialAttributes[ 'name' ]) ? $socialAttributes[ 'name' ] : null;
        $facebookFullNameData = explode(' ', $facebookFullName);

        if (isset($facebookFullNameData[ 0 ])) {
            $name = $facebookFullNameData[ 0 ];
        } else {
            $name = null;
        }

        if (isset($facebookFullNameData[ 1 ])) {
            $surname = $facebookFullNameData[ 1 ];
        } else {
            $surname = null;
        }

        $object = new Client();
        $object->sourceId = $socialAttributes[ 'id' ];
        $object->email = isset($socialAttributes[ 'email' ]) ? $socialAttributes[ 'email' ] : null;
        $object->surname = $surname;
        $object->name = $name;
        $object->sex = null;

        return $object;
    }
}
