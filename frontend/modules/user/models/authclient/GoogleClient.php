<?php

namespace frontend\modules\user\models\authclient;

class GoogleClient extends AbstractClient
{
    /**
     * Generate user information from google social account
     *
     * @param array $socialAttributes User socila attributes
     * @return \frontend\modules\user\models\authclient\Client
     */
    public static function getUserAttributes($socialAttributes)
    {
        $object = new Client();
        $object->sourceId = $socialAttributes[ 'id' ];
        $object->email = isset($socialAttributes[ 'emails' ][ 0 ]) ? $socialAttributes[ 'emails' ][ 0 ][ 'value' ] : null;
        $object->surname = $socialAttributes[ 'name' ][ 'familyName' ];
        $object->name = $socialAttributes[ 'name' ][ 'givenName' ];
        $object->sex = (isset($socialAttributes[ 'gender' ])) ? $socialAttributes[ 'gender' ] : null;

        return $object;
    }
}
