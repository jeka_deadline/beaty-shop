<?php

namespace frontend\modules\user\models\authclient;

abstract class AbstractClient
{
    /**
     * Abstract method which must be realization in extends class.
     *
     * @param array $socialAttributes User socila attributes
     */
    abstract public static function getUserAttributes($socialAttributes);
}
