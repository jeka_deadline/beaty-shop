<?php

namespace frontend\modules\user\models\authclient;

/**
 * User social information class.
 */
class Client
{
    /**
     * User social client id.
     *
     * @var string $sourceId
     */
    public $sourceId;

    /**
     * User social suename.
     *
     * @var string $surname
     */
    public $surname;

    /**
     * User social name.
     *
     * @var string $name
     */
    public $name;

    /**
     * User social email.
     *
     * @var string $email
     */
    public $email;

    /**
     * User social sex.
     *
     * @var string $sex
     */
    public $sex;

    /**
     * User social name source.
     *
     * @var string $source
     */
    public $source;
}
