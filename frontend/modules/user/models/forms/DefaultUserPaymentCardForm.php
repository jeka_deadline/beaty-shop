<?php

namespace frontend\modules\user\models\forms;

use yii\base\Model;
use frontend\modules\user\models\UserPaymentCard;
use yii\helpers\ArrayHelper;

/**
 * Form for change user default payment card.
 */
class DefaultUserPaymentCardForm extends Model
{
    /**
     * User payment card id.
     *
     * @var int $defaultPaymentCardId
     */
    public $defaultPaymentCardId;

    /**
     * Current user id.
     *
     * @var int $userId
     */
    private $userId;

    /**
     * Array list user payment cards.
     *
     * @var \frontend\modules\user\models\UserPaymentCard[] $paymentCards
     */
    private $paymentCards;

    /**
     * {@inheritdoc}
     *
     * @param int $userId User id.
     * @param \frontend\modules\user\models\UserPaymentCard[] $paymentCards List user payment cards.
     * @param array $config {inheritdoc}
     * @return void
     */
    public function __construct($userId, $paymentCards, $config = [])
    {
        $this->userId = $userId;
        $this->paymentCards = $paymentCards;

        $userPaymentCard = UserPaymentCard::getUserDefaultPaymentCard($this->userId);

        if ($userPaymentCard) {
            $this->defaultPaymentCardId = $userPaymentCard->id;
        }

        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return [
            ['defaultPaymentCardId', 'exist', 'targetClass' => UserPaymentCard::className(), 'targetAttribute' => 'id', 'filter' => function($query) {
                return $query->andWhere(['user_id' => $this->userId]);
            }],
        ];
    }

    /**
     * Set default payment card.
     *
     * @return void
     */
    public function setDefaultCard()
    {
        if (!$this->validate()) {
            return false;
        }

        foreach ($this->paymentCards as $paymentCard) {
            if ($this->defaultPaymentCardId == $paymentCard->id) {
                $paymentCard->default = 1;
                $paymentCard->save(false);
            } else {
                if (!$paymentCard->default) {
                    continue;
                } else {
                    $paymentCard->default = 0;
                    $paymentCard->save(false);
                }
            }
        }
    }

    /**
     * List user payment card for hash map.
     *
     * @return array
     */
    public function getListPaymentCardsHashMap()
    {
        return ArrayHelper::map($this->paymentCards, 'id', 'name');
    }
}
