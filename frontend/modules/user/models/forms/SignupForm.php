<?php

namespace frontend\modules\user\models\forms;

use Exception;
use Yii;
use yii\base\Model;
use frontend\modules\user\models\User;
use frontend\modules\user\models\Profile;
use frontend\modules\core\models\EmailTemplate;
use yii\helpers\Url;
use frontend\modules\core\models\Subscriber;
use frontend\modules\core\models\TrainerDistributor;

/**
 * Class register user.
 */
class SignupForm extends Model
{
    /**
     * User email.
     *
     * @var string $email
     */
    public $email;

    /**
     * User password.
     *
     * @var string $password
     */
    public $password;

    /**
     * Repeat password.
     *
     * @var string $repeatPassword
     */
    public $repeatPassword;

    /**
     * User surname.
     *
     * @var string $surname
     */
    public $surname;

    /**
     * User company.
     *
     * @var string $company
     */
    public $company;

    /**
     * User title.
     *
     * @var string $title
     */
    public $title;

    /**
     * User name.
     *
     * @var string $name
     */
    public $name;

    /**
     * User agree rules.
     *
     * @var bool $rules
     */
    public $rules;

    /**
     * User agree confirm subscribe email delivery.
     *
     * @var bool $emailDelivery
     */
    public $emailDelivery;

    /**
     * Distributor code.
     *
     * @var string $distributorCode
     */
    public $distributorCode;

    const FORM_ID = 'signup-form';

    const FORM_SOCIAL_ACTION = 'signup';

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email', 'message' =>'Не корректный Email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => User::className()],

            ['password', 'string', 'min' => 6,],
            ['password', 'required'],


            ['repeatPassword', 'required'],
            ['repeatPassword', 'compare', 'compareAttribute' => 'password', 'operator' => '==='],
            [['surname', 'name', 'company'], 'trim'],
            [['surname', 'name'], 'string', 'max' => 50],
            ['company', 'string', 'max' => 100],
            ['title', 'string', 'max' => 10],
            [['surname', 'name', 'title'], 'required'],
            [['rules', 'emailDelivery'], 'boolean'],
            ['rules', 'compare', 'compareValue' => 1, 'operator' => '===', 'type' => 'number', 'message' => Yii::t('core', 'The checkbox must be checked')],
            ['distributorCode', 'safe'],
        ];

    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'title'          => Yii::t('product', 'Title'),
            'email'          => 'E-mail',
            'password'       => Yii::t('user', 'Password'),
            'repeatPassword' => Yii::t('user', 'Confirm password'),
            'surname'        => Yii::t('core', 'Last Name'),
            'name'           => Yii::t('core', 'First Name'),
            'company'        => Yii::t('user', 'Company'),
            'rules'          => Yii::t('user', 'I agree to the Terms & Conditions.'),
            'emailDelivery'  => 'Sign up to hear about exclusive updates and receive 10% off your next online purchase.',
        ];
    }

    /**
     * Reset password fields.
     *
     * @return void
     */
    public function resetPasswordFields()
    {
        $this->password       = null;
        $this->repeatPassword = null;
    }

    /**
     * Register new user.
     *
     * @return bool|\frontend\modules\user\models\User
     */
    public function signup()
    {
        if (!$this->validate()) {

            return false;
        }

        $user              = new User();
        $user->email       = $this->email;
        $user->register_ip = Yii::$app->request->userIP;

        $user->setPassword($this->password);
        $user->generateAuthKey();

        if (!empty($this->distributorCode)) {
            $distributor = TrainerDistributor::find()
                ->where(['type' => TrainerDistributor::TYPE_DISTRUBUTOR, 'register_code' => $this->distributorCode])
                ->one();

            if ($distributor) {
                $user->distributor_id = $distributor->id;
            }
        }

        $transaction = Yii::$app->db->beginTransaction();

        if ($user->validate() && $user->save() && $tokenModel = $user->generateEmailToken()) {

            $profile = new Profile();
            $profile->surname = $this->surname;
            $profile->name = $this->name;
            $profile->company = $this->company;
            $profile->title = $this->title;
            $profile->user_id = $user->id;

            if ($profile->validate() && $profile->save()) {

                if ($this->emailDelivery) {
                    $this->subscribeUser();
                }

                // for error send letter
                try {
                    self::sendConfirmEmailLetter($user, $tokenModel->token);
                } catch (Exception $e) {
                    $transaction->rollback();

                    return false;
                }

                $transaction->commit();

                return true;
            }
        }

        $transaction->rollback();

        return false;
    }

    /**
     * Get user titles.
     *
     * @return array
     */
    public function getListUserTitles()
    {
        return User::getListUserTitles();
    }

    /**
     * Send user confirm email letter.
     *
     * @static
     * @param \frontend\modules\user\models\User $user User model.
     * @param string $token Confirm email token
     * @return bool
     */
    public static function sendConfirmEmailLetter(User $user, $token)
    {
        $template = EmailTemplate::findTemplateConfirmEmail();

        if (!$template) {
            return false;
        }

        $text = $template->getTextWithReplacedPlaceholdersConfirmEmail($user, Url::toRoute(['/user/security/confirm-email', 'token' => $token], true));

        $mailer = Yii::$app->mailer;

        return $mailer->compose()
            ->setFrom([$mailer->transport->getUsername() => $template->from])
            ->setTo($user->email)
            ->setHtmlBody($text)
            ->setSubject($template->subject)
            ->send();
    }

    /**
     * Subscribe user.
     *
     * @access private
     * @return void
     */
    private function subscribeUser()
    {
        $subscriber = Subscriber::find()
            ->where(['email' => $this->email])
            ->one();

        if (!$subscriber) {
            $subscriber = new Subscriber();
            $subscriber->email = $this->email;

            $subscriber->save(false);
        }
    }
}
