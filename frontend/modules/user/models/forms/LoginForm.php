<?php

namespace frontend\modules\user\models\forms;

use Yii;
use yii\base\Model;
use frontend\modules\user\models\User;
use yii\helpers\Url;

/**
 * Class for login user.
 */
class LoginForm extends Model
{
    /**
     * User email.
     *
     * @var string
     */
    public $email;

    /**
     * User password.
     *
     * @var string
     */
    public $password;

    /**
     * Remember user auth.
     *
     * @var bool
     */
    public $rememberMe;

    public $agreed;

    /**
     * Google recaptcha.
     *
     * @var string
     */
    public $reCaptcha;

    /**
     * User model if user find by email.
     *
     * @var \frontend\modules\user\models\User
     */
    private $user = false;

    const FORM_ID = 'login-form';

    const FORM_SOCIAL_ACTION = 'login';

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['email', 'password'], 'required'],
            [['password'], 'validatePassword'],
            [['rememberMe', 'agreed'], 'boolean'],
            [['agreed'], 'compare', 'compareValue' => 1, 'operator' => '===', 'type' => 'number', 'message' => Yii::t('core', 'The checkbox must be checked')],
            [['email'], 'filter', 'filter' => 'trim'],
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'email'      => Yii::t('user', 'Email address'),
            'password'   => Yii::t('user', 'Password'),
            'rememberMe' => Yii::t('user', 'Keep me logged in'),
            'agreed'     => 'agreed',
        ];
    }

    /**
     * Reset password field.
     *
     * @return void
     */
    public function resetPasswordField()
    {
        $this->password = null;
    }

    /**
     * Validate user password.
     *
     * @return void
     */
    public function validatePassword()
    {
        $user = $this->getUser();

        if ($user) {
            if (!$user->confirm_email_at) {
                $this->addError('email', 'Email not confirm');

                return false;
            }

            if ($user->blocked_at) {
                $this->addError('email', 'You blocked');

                return false;
            }

            if (!Yii::$app->getSecurity()->validatePassword($this->password, $user->password_hash)) {
                $this->addError('email', 'Email or password incorrect');
            }
        } else {
            $this->addError('email', 'Email or password incorrect');
        }
    }

    /**
     * Attempt auth user.
     *
     * @return bool
     */
    public function login()
    {
        if ($this->validate()) {
            $user = User::findByEmail($this->email);
            Yii::$app->user->login($user, $this->rememberMe ? 3600 * 24 * 30 : 0);

            return true;
        }

        return false;
    }

    /**
     * Find user by email.
     *
     * @return \common\models\user\User|null
     */
    private function getUser()
    {
        if ($this->user === false) {
            $this->user = User::findByEmail($this->email);
        }

        return $this->user;
    }
}
