<?php

namespace frontend\modules\user\models\forms;

use Yii;
use yii\base\Model;
use frontend\modules\user\models\User;
use frontend\modules\user\models\UserResetPasswordToken;
use frontend\modules\core\models\EmailTemplate;
use yii\helpers\Url;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    /**
     * User email.
     *
     * @var string
     */
    public $email;

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => User::className(),
                'message' => Yii::t('user', 'Email or password incorrect'),
            ],
        ];
    }

    /**
     * Create user reset password token.
     *
     * @return bool whether the email was send
     */
    public function createResetPasswordToken()
    {
        if (!$this->validate()) {
            return false;
        }

        /* @var $user User */
        $user = User::findOne([
            'email' => $this->email,
        ]);

        if (!$user) {
            return false;
        }

        $token = UserResetPasswordToken::findOne(['user_id' => $user->id]);

        if (!$token) {
            $token = new UserResetPasswordToken();
            $token->user_id = $user->id;
        }

        $token->token = Yii::$app->security->generateRandomString();

        if (!$token->validate() || !$token->save()) {
            return false;
        }

        self::sendUserResetPasswordLetter($token->token, $this->email);

        return true;
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'email' => 'E-mail',
        ];
    }

    /**
     * Send user reset password letter.
     *
     * @param string $token Reset password token
     * @return bool
     */
    public static function sendUserResetPasswordLetter($token, $userEmail)
    {
        $template = EmailTemplate::findTemplateRessetEmail();

        if (!$template) {
            return false;
        }

        $user = User::find()
            ->where(['email' => $userEmail])
            ->with('profile')
            ->one();

        $text = $template->getTextWithReplacedPlaceholdersResetPassword(Url::toRoute(['/user/security/reset-password', 'token' => $token], true), $user);

        $mailer = Yii::$app->mailer;

        return $mailer->compose()
            ->setFrom([$mailer->transport->getUsername() => $template->from])
            ->setTo($userEmail)
            ->setHtmlBody($text)
            ->setSubject($template->subject)
            ->send();
    }
}
