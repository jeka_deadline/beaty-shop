<?php

namespace frontend\modules\user\models\forms;

use Yii;
use yii\base\Model;
use yii\base\InvalidParamException;
use frontend\modules\user\models\User;
use frontend\modules\user\models\UserResetPasswordToken;

/**
 * Password reset form
 */
class ResetPasswordForm extends Model
{
    /**
     * User new password
     *
     * @var string
     */
    public $password;

    /**
     * User confirm password
     *
     * @var string
     */
    public $repeatPassword;

    /**
     * @var \frontend\modules\user\models\UserResetPasswordToken
     */
    private $tokenModel;


    /**
     * Creates a form model given a token.
     *
     * @param string $token
     * @param array $config name-value pairs that will be used to initialize the object properties
     * @throws \yii\base\InvalidParamException if token is empty or not valid
     */
    public function __construct($token, $config = [])
    {
        if (empty($token) || !is_string($token)) {
            throw new InvalidParamException('Password reset token cannot be blank.');
        }

        $tokenModel = UserResetPasswordToken::findOne(['token' => $token]);

        if (!$tokenModel) {
            throw new InvalidParamException('Wrong password reset token.');
        }

        $this->tokenModel = $tokenModel;

        parent::__construct($config);
    }

    public function isTokenExpired()
    {
        return $this->tokenModel->isTokenExpired();
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['password', 'repeatPassword'], 'required'],
            ['password', 'string', 'min' => 6],
            ['repeatPassword', 'compare', 'compareAttribute' => 'password', 'operator' => '==='],
        ];
    }

    /**
     * Resets password.
     *
     * @return bool if password was reset.
     */
    public function resetPassword()
    {
        $user = $this->tokenModel->user;

        $user->setPassword($this->password);

        UserResetPasswordToken::deleteAll(['user_id' => $user->id]);

        return $user->save(false);
    }

    public function attributeLabels()
    {
        return [
            'password' => Yii::t('user', 'Password'),
            'repeatPassword' => Yii::t('user', 'Repeat password'),
        ];
    }

    /**
     * Regenerate user password reset token.
     *
     * @return bool
     */
    public function regenerateResetToken()
    {
        $this->tokenModel->token = Yii::$app->security->generateRandomString();

        $this->tokenModel->save(false);

        return PasswordResetRequestForm::sendUserResetPasswordLetter($this->tokenModel->token, $this->tokenModel->user->email);
    }

    /**
     * Get token string.
     *
     * @return string
     */
    public function getTokenValue()
    {
        return $this->tokenModel->token;
    }
}
