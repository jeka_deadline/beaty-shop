<?php

namespace frontend\modules\user\models\forms;

use Yii;
use yii\base\Model;

/**
 * Form for change user password.
 */
class ChangeProfilePasswordForm extends Model
{
    /**
     * Scenario change password when user login not social.
     *
     * @var string
     */
    const SCENARION_LOGIN_WITH_NOT_SOCIAL = 'not-social';

    /**
     * User old password.
     *
     * @var string $oldPassword
     */
    public $oldPassword;

    /**
     * User new password.
     *
     * @var string $newPassword
     */
    public $newPassword;

    /**
     * User repeat new password.
     *
     * @var string $repeatNewPassword
     */
    public $repeatNewPassword;

    /**
     * User model.
     *
     * @var \frontend\modules\user\models\User $user
     */
    private $user;

    /**
     * {@inheritdoc}
     *
     * @param \frontend\modules\user\models\User $user User model.
     * @param array $config {@inheritdoc}
     * @return void
     */
    public function __construct($user, $config = [])
    {
        $this->user = $user;

        if (!$user->login_with_social) {
            $this->scenario = self::SCENARION_LOGIN_WITH_NOT_SOCIAL;
        }

        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return [
            ['newPassword', 'string', 'min' => 6],
            ['newPassword', 'compare', 'compareAttribute' => 'repeatNewPassword'],
            ['newPassword', 'required',
                'when' => function($model) {
                    return !empty($model->oldPassword);
                }
            ],
            ['repeatNewPassword', 'safe'],
            ['oldPassword', 'required', 'on' => self::SCENARION_LOGIN_WITH_NOT_SOCIAL,
                'when' => function($model) {
                    return !empty($model->newPassword);
                }
            ],
            ['oldPassword', 'validateOldPassword', 'on' => self::SCENARION_LOGIN_WITH_NOT_SOCIAL],
        ];
    }

    /**
     * Validate old password.
     *
     * @return void
     */
    public function validateOldPassword()
    {
        if (!Yii::$app->security->validatePassword($this->oldPassword, $this->user->password_hash)) {
            $this->addError('oldPassword', 'You old password incorrect');
        }
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'oldPassword' => Yii::t('user', 'Old password'),
            'newPassword' => Yii::t('user', 'New password'),
            'repeatNewPassword' => Yii::t('user', 'Confirm password'),
        ];
    }

    /**
     * Reset password inputs.
     *
     * @return void
     */
    public function resetInputPasswords()
    {
        $this->oldPassword = null;
        $this->newPassword = null;
        $this->repeatNewPassword = null;
    }

    /**
     * Check if user login with non social.
     *
     * @return bool
     */
    public function loginWithNotSocial()
    {
        return ($this->scenario === self::SCENARION_LOGIN_WITH_NOT_SOCIAL) ? true : false;
    }
}
