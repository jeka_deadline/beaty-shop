<?php

namespace frontend\modules\user\models\forms;

use Yii;
use frontend\modules\user\models\User;
use frontend\modules\user\models\UserEmailToken;
use frontend\modules\user\models\forms\SignupForm;
use yii\base\Model;

/**
 * Class for create user confirm email token
 */
class UpdateEmailTokenForm extends Model
{
    /**
     * User token.
     *
     * @access private
     * @var \frontend\modules\user\models\UserEmailToken $token
     */
    private $token;

    public function __construct($token, $config = [])
    {
        $token = UserEmailToken::find()
            ->where(['token' => $token])
            ->with('user')
            ->one();

        if (!$token || !$token->isTokenExpired()) {
            throw new \Exception("Error Processing Request");
        }

        $this->token = $token;

        parent::__construct($config);
    }

    /**
     * Create user reset email token
     *
     * @return bool
     */
    public function createResetEmailToken()
    {
        $this->token->token = Yii::$app->security->generateRandomString();

        $transaction = Yii::$app->db->beginTransaction();

        if (!$this->token->validate() || !$this->token->save()) {
            $transaction->rollback();

            return false;
        }

        if (!SignupForm::sendConfirmEmailLetter($this->token->user, $this->token->token)) {
            $transaction->rollback();

            return false;
        }

        $transaction->commit();

        return true;
    }
}
