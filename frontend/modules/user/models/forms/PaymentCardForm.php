<?php

namespace frontend\modules\user\models\forms;

use yii\base\Model;
use frontend\modules\user\models\UserPaymentCard;

/**
 * Form for user payment card.
 */
class PaymentCardForm extends Model
{
    /**
     * Card name.
     *
     * @var string $name
     */
    public $name;

    /**
     * Card number.
     * @var string $number
     */
    public $number;

    /**
     * Card pseudocardpan.
     *
     * @var string $pseudocardpan
     */
    public $pseudocardpan;

    /**
     * User identity id.
     *
     * @var string $userId
     */
    private $userId;

    /**
     * User payment card if it exist.
     *
     * @access private
     * @var \frontend\modules\user\models\UserPaymentCard $paymentCard
     */
    private $paymentCard;

    /**
     * {@inheritdoc}. Set user identtity id.
     *
     * @param int $userId User identity id
     * @param \frontend\modules\user\models\UserPaymentCard $card User payment card.
     * @param array $config Configuration for model
     * @return void
     */
    public function __construct($userId, UserPaymentCard $card = null, $config = [])
    {
        $this->userId = $userId;

        if ($card) {
            $this->paymentCard = $card;
            $this->name = $card->name;
        }

        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            ['name', 'trim'],
            [['number', 'pseudocardpan'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name' => 'name',
        ];
    }

    /**
     * Get user payment card id.
     *
     * @return int|null
     */
    public function getPaymentCardId()
    {
        if (!$this->paymentCard) {
            return null;
        }

        return $this->paymentCard->id;
    }

    /**
     * Create new payment card for user.
     *
     * @return bool|\frontend\modules\user\models\UserPaymentCard
     */
    public function createPaymentCard()
    {
        if (!$this->validate()) {
            return false;
        }

        $model          = new UserPaymentCard();
        $model->user_id = $this->userId;
        $model->name    = $this->name;
        $model->number  = $this->number;
        $model->pseudocardpan = $this->pseudocardpan;

        if ($model->validate() && $model->save()) {
            return $model;
        }

        return false;
    }

    /**
     * Update payment card for user.
     *
     * @return bool
     */
    public function updatePaymentCard()
    {
        if (!$this->validate()) {
            return false;
        }

        $card       = $this->paymentCard;
        $card->name = $this->name;

        if ($card->validate() && $card->save()) {
            return true;
        }

        return false;
    }
}
