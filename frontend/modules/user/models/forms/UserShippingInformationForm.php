<?php

namespace frontend\modules\user\models\forms;

use Yii;
use yii\base\Model;
use frontend\modules\geo\models\Country;
use frontend\modules\geo\models\City;
use frontend\modules\user\models\UserShippingAddress;
use frontend\modules\core\validators\PhoneValidator;

/**
 * Form for add new shipping address.
 */
class UserShippingInformationForm extends Model
{
    /**
     * Name address.
     *
     * @var string $name
     */
    public $name;

    /**
     * User surname.
     *
     * @var string $userSurname
     */
    public $userSurname;

    /**
     * Title address.
     *
     * @var string $name
     */
    public $title;

    /**
     * User name.
     *
     * @var string $userName
     */
    public $userName;

    /**
     * Country id.
     *
     * @var int $userCountryId
     */
    public $userCountryId;

    /**
     * City name.
     *
     * @var int $userCity
     */
    public $userCity;

    /**
     * User address 1.
     *
     * @var string $userAddress1
     */
    public $userAddress1;

    /**
     * User address 2.
     *
     * @var string $userAddress2
     */
    public $userAddress2;

    /**
     * User address zip code.
     *
     * @var string $zip
     */
    public $zip;

    /**
     * User phone.
     *
     * @var string $userPhone
     */
    public $userPhone;

    /**
     * User email.
     *
     * @var string $userEmail
     */
    public $userEmail;

    /**
     * User auth id.
     *
     * @var int $userId
     */
    private $userId;

    /**
     * User shipping address for update.
     *
     * @var \frontend\modules\user\models\UserShippingAddress $shippingAddress
     */
    private $shippingAddress;


    /**
     * {@inheritdoc}. Set user identtity id.
     *
     * @param int $userId User identity id
     * @param \frontend\modules\user\models\UserShippingAddress $shippingAddress User shipping information address.
     * @param array $config Configuration for model
     * @return void
     */
    public function __construct($userId, UserShippingAddress $shippingAddress = null, $config = [])
    {
        $this->userId = $userId;

        if ($shippingAddress) {
            $this->shippingAddress = $shippingAddress;
            $this->name            = $shippingAddress->name_address;
            $this->title            = $shippingAddress->title;
            $this->userSurname     = $shippingAddress->surname;
            $this->userName        = $shippingAddress->name;
            $this->userCountryId   = $shippingAddress->country_id;
            $this->userCity        = $shippingAddress->city;
            $this->userAddress1    = $shippingAddress->address1;
            $this->userAddress2    = $shippingAddress->address2;
            $this->zip             = $shippingAddress->zip;
            $this->userPhone       = $shippingAddress->phone;
            $this->userEmail       = $shippingAddress->email;
        }

        parent::__construct($config);
    }

    /**
     * Get user shipping information id.
     *
     * @return int
     */
    public function getShippingId()
    {
        return $this->shippingAddress->id;
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['name', 'userSurname', 'userName', 'userCountryId', 'userCity', 'userAddress1', 'zip', 'userEmail', 'title'], 'required'],
            [['name', 'userSurname', 'userName'], 'string', 'max' => 50],
            [['title'], 'string', 'max' => 10],
            [['userAddress1', 'userAddress2'], 'string'],
            [['zip'], 'match', 'pattern' => '#^\d{5}$#'],
            ['userEmail', 'email'],
            ['userCountryId', 'exist', 'targetClass' => Country::className(), 'targetAttribute' => 'id'],
            ['userCity', 'string', 'max' => 255],
            ['userPhone', PhoneValidator::className()],
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name' => 'address name',
            'title' => Yii::t('product', 'Title'),
            'userName' => Yii::t('core', 'First Name'),
            'userSurname' => Yii::t('core', 'Last Name'),
            'userAddress1' => Yii::t('product', 'Address'),
            'userCity' => Yii::t('product', 'City'),
            'zip' => Yii::t('product', 'Zip Code'),
            'userCountryId' => Yii::t('product', 'Country'),
            'userEmail' => 'e-mail',
            'userPhone' => Yii::t('product', 'Phone'),
        ];
    }

    /**
     * Get hash list countries.
     *
     * @return array
     */
    public function getListCountriesForDropDown()
    {
        return Country::getHashMap('id', 'name');
    }

    /**
     * Add new shipping address for user
     *
     * @return bool
     */
    public function createNewShippingAddress()
    {
        if (!$this->validate()) {
            return false;
        }

        $model = new UserShippingAddress();

        $model->user_id      = $this->userId;
        $model->name_address = $this->name;
        $model->title        = $this->title;
        $model->surname      = $this->userSurname;
        $model->name         = $this->userName;
        $model->country_id   = $this->userCountryId;
        $model->city         = $this->userCity;
        $model->address1     = $this->userAddress1;
        $model->address2     = $this->userAddress2;
        $model->zip          = $this->zip;
        $model->phone        = str_replace(' ', '', $this->userPhone);
        $model->email        = $this->userEmail;

        if ($model->validate() && $model->save()) {
            return true;
        }

        return false;
    }

    /**
     * Update shipping address for user
     *
     * @return bool
     */
    public function updateShippingAddress()
    {
        if (!$this->validate()) {
            return false;
        }

        $model = $this->shippingAddress;

        $model->name_address = $this->name;
        $model->title        = $this->title;
        $model->surname      = $this->userSurname;
        $model->name         = $this->userName;
        $model->country_id   = $this->userCountryId;
        $model->city         = $this->userCity;
        $model->address1     = $this->userAddress1;
        $model->address2     = $this->userAddress2;
        $model->zip          = $this->zip;
        $model->phone        = str_replace(' ', '', $this->userPhone);
        $model->email        = $this->userEmail;

        if ($model->validate() && $model->save()) {
            return true;
        }

        return false;
    }

    /**
     * Get phone mask.
     *
     * @return string
     */
    public function getPhoneMask()
    {
        return PhoneValidator::getGermanPhoneMask();
    }
}
