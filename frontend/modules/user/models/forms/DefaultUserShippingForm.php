<?php

namespace frontend\modules\user\models\forms;

use yii\base\Model;
use frontend\modules\user\models\UserShippingAddress;
use yii\helpers\ArrayHelper;

/**
 * Form for change user default shipping address.
 */
class DefaultUserShippingForm extends Model
{
    /**
     * User shipping address id.
     *
     * @var int $defaultShippingAddressId
     */
    public $defaultShippingAddressId;

    /**
     * Current user id.
     *
     * @var int $userId
     */
    private $userId;

    /**
     * Array list user shipping addresses.
     *
     * @var \frontend\modules\user\models\UserShippingAddress[] $shippingAddresses
     */
    private $shippingAddresses;

    /**
     * {@inheritdoc}
     *
     * @param int $userId User id.
     * @param \frontend\modules\user\models\UserShippingAddress[] $shippingAddresses List user shipping addresses.
     * @param array $config {inheritdoc}
     * @return void
     */
    public function __construct($userId, $shippingAddresses, $config = [])
    {
        $this->userId = $userId;
        $this->shippingAddresses = $shippingAddresses;

        $shippingAddress = UserShippingAddress::getUserDefaultShippingAddress($this->userId);

        if ($shippingAddress) {
            $this->defaultShippingAddressId = $shippingAddress->id;
        }

        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return [
            ['defaultShippingAddressId', 'exist', 'targetClass' => UserShippingAddress::className(), 'targetAttribute' => 'id', 'filter' => function($query) {
                return $query->andWhere(['user_id' => $this->userId]);
            }],
        ];
    }

    /**
     * Set default shipping address.
     *
     * @return void
     */
    public function setDefaultShippingAddress()
    {
        if (!$this->validate()) {
            return false;
        }

        foreach ($this->shippingAddresses as $shippingAddress) {
            if ($this->defaultShippingAddressId == $shippingAddress->id) {
                $shippingAddress->default = 1;
                $shippingAddress->save(false);
            } else {
                if (!$shippingAddress->default) {
                    continue;
                } else {
                    $shippingAddress->default = 0;
                    $shippingAddress->save(false);
                }
            }
        }
    }

    /**
     * List user shipping addresses for hash map.
     *
     * @return array
     */
    public function getListShippingAddressHashMap()
    {
        return ArrayHelper::map($this->shippingAddresses, 'id', 'name_address');
    }
}
