<?php

namespace frontend\modules\user\models\forms;

use Yii;
use DateTime;
use yii\base\Model;
use frontend\modules\user\models\User;
use frontend\modules\core\validators\PhoneValidator;

/**
 * Form for update user profile.
 */
class ProfileForm extends Model
{
    /**
     * User email.
     *
     * @var string $email
     */
    public $email;

    /**
     * User title.
     *
     * @var string $title
     */
    public $title;

    /**
     * User surname.
     *
     * @var string $surname
     */
    public $surname;

    /**
     * User name.
     *
     * @var string $name
     */
    public $name;

    /**
     * User company
     *
     * @var string $company
     */
    public $company;

    /**
     * User phone.
     *
     * @var string $phone
     */
    public $phone;

    /**
     * User day birth.
     *
     * @var int $dayBirth
     */
    public $dayBirth;

    /**
     * User month birth.
     *
     * @var int $monthBirth
     */
    public $monthBirth;

    /**
     * User year birth.
     *
     * @var int $yearBirth
     */
    public $yearBirth;

    /**
     * User model
     *
     * @var \frontend\modules\user\models\User $user
     */
    private $user;

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function __construct(User $user, $config = [])
    {
        parent::__construct($config);

        $this->user    = $user;
        $this->email   = $user->email;
        $this->surname = $user->profile->surname;
        $this->name    = $user->profile->name;
        $this->phone   = $user->profile->phone;
        $this->company = $user->profile->company;
        $this->title   = $user->profile->title;

        $birthday = $user->profile->date_birth;

        if ($birthday) {
            $birthday = new DateTime($birthday);
            $this->dayBirth = $birthday->format('j');
            $this->monthBirth = $birthday->format('n');
            $this->yearBirth = $birthday->format('Y');
        }
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return [
            // email
            [['email'], 'required'],

            ['email', 'email'],
            ['email', 'unique', 'targetClass' => User::className(), 'filter' => ['<>', 'id', $this->user->id]],

            // string max length 50
            [['surname', 'name'], 'string', 'max' => '50'],

            // filter trim
            [['surname', 'name', 'email', 'company'], 'trim'],

            // company
            ['company', 'string', 'max' => 100],

            // phone
            ['phone', PhoneValidator::className()],

            [['surname', 'name', 'title'], 'required'],

            // user title
            ['title', 'in', 'range' => [User::USER_TITLE_MR, User::USER_TITLE_MRS]],

            [['dayBirth', 'monthBirth', 'yearBirth'], 'validateDateBirth'],

            ['dayBirth', 'required',
                'when' => function($model) {
                    return (!empty($model->monthBirth) || !empty($model->yearBirth));
                }
            ],

            ['monthBirth', 'required',
                'when' => function($model) {
                    return (!empty($model->dayBirth) || !empty($model->yearBirth));
                }
            ],

            ['yearBirth', 'required',
                'when' => function($model) {
                    return (!empty($model->monthBirth) || !empty($model->dayBirth));
                }
            ],
        ];
    }

    /**
     * Validate date selects.
     *
     * @param array $attributes List validate attributes.
     * @return void
     */
    public function validateDateBirth($attributes)
    {
        if (!empty($this->dayBirth) && !empty($this->monthBirth) && !empty($this->yearBirth)) {
            if (!checkdate($this->monthBirth, $this->dayBirth, $this->yearBirth)) {
                $this->addError($attributes, 'Non correct date birth');
            }
        }
    }

    /**
     * Update current user profile.
     *
     * @param \frontend\modules\user\models\forms\ChangeProfilePasswordForm $changePasswordForm Form for change password
     * @return bool
     */
    public function updateProfile(ChangeProfilePasswordForm $changePasswordForm)
    {
        $user        = $this->user;
        $profile     = $this->user->profile;
        $user->email = $this->email;

        if ($changePasswordForm->newPassword) {
            $user->setPassword($changePasswordForm->newPassword);
            $user->login_with_social = 0;
        }

        $profile->surname    = $this->surname;
        $profile->name       = $this->name;
        $profile->phone      = str_replace(' ', '', $this->phone);
        $profile->company    = $this->company;
        $profile->title      = $this->title;

        if (!empty($this->dayBirth) && !empty($this->yearBirth) && !empty($this->monthBirth)) {
            $profile->date_birth = sprintf('%d-%02d-%02d', $this->yearBirth, $this->monthBirth, $this->dayBirth);
        } else {
            $profile->date_birth = null;
        }

        $transaction = Yii::$app->db->beginTransaction();

        if ($user->validate() && $profile->validate() && $user->save() && $profile->save()) {
            $transaction->commit();

            return true;
        }

        $transaction->rollback();

        return false;
    }

    /**
     * Get user list titles.
     *
     * @return array
     */
    public function getListUserTitles()
    {
        return User::getListUserTitles();
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'title'   => Yii::t('product', 'Title'),
            'surname' => Yii::t('core', 'Last Name'),
            'name'    => Yii::t('core', 'First Name'),
            'company' => Yii::t('product', 'Company'),
            'email'   => 'e-mail / login',
            'phone'   => Yii::t('user', 'Phone'),
        ];
    }

    /**
     * Generate list days.
     *
     * @return array
     */
    public function getListDays()
    {
        $range = range(1, 31);

        return array_combine($range, $range);
    }

    /**
     * Generate list years.
     *
     * @return array
     */
    public function getListYears()
    {
        $currentYear = date('Y');

        $range = array_reverse(range($currentYear - 99, $currentYear));

        return array_combine($range, $range);
    }

    /**
     * Generate list months.
     *
     * @return array
     */
    public function getListMonths()
    {
        return [
            1 => 'January',
            2 => 'February',
            3 => 'March',
            4 => 'April',
            5 => 'May',
            6 => 'June',
            7 => 'July',
            8 => 'August',
            9 => 'September',
            10 => 'October',
            11 => 'November',
            12 => 'December',
        ];
    }

    /**
     * Get phone mask.
     *
     * @return string
     */
    public function getPhoneMask()
    {
        return PhoneValidator::getGermanPhoneMask();
    }

    /**
     * Check if user exist in subscriber list.
     *
     * @return bool
     */
    public function isUserExistInSubscriberList()
    {
        return $this->user->isSubsciber();
    }
}
