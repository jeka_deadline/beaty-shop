<?php

namespace frontend\modules\user\models;

use Yii;
use DateTime;
use frontend\modules\user\models\authclient\Client;
use frontend\modules\core\models\Subscriber;
use frontend\modules\core\models\TrainerDistributor;
use frontend\modules\core\models\Setting;
use common\models\product\Coupon;
use frontend\modules\core\models\EmailTemplate;

/**
 * User class extends base user class
 */
class User extends \common\models\user\User
{
    /**
     * Confirm user email.
     *
     * @return void
     */
    public function confirmEmail()
    {
        UserEmailToken::deleteAll(['user_id' => $this->id]);

        $this->confirm_email_at = date('Y-m-d H:i:s');

        $this->sendNewUserLetters();
    }

    /**
     * Create new user by social account.
     *
     * @param \frontend\modules\user\models\authclient\Client user social account information
     * @return null|\frontend\modules\user\models\User
     */
    public static function createSocial(Client $client, $distributorCode = null)
    {
        $user = new self();

        $currentTime = date('Y-m-d H:i:s');

        $user->email = $client->email;
        $user->login_with_social = 1;
        $user->confirm_email_at = $currentTime;
        $user->register_ip = Yii::$app->request->userIP;
        $user->generateAuthKey();
        $user->setPassword(Yii::$app->security->generateRandomString());

        if (!empty($distributorCode)) {
            $distributor = TrainerDistributor::find()
                ->where(['type' => TrainerDistributor::TYPE_DISTRUBUTOR, 'register_code' => $distributorCode])
                ->one();

            if ($distributor) {
                $user->distributor_id = $distributor->id;
            }
        }

        $transaction = Yii::$app->db->beginTransaction();

        if ($user->validate() && $user->save()) {
            $profile = new Profile();
            $profile->user_id = $user->id;
            $profile->surname = $client->surname;
            $profile->name = $client->name;

            $social = new Social();
            $social->user_id = $user->id;
            $social->provider = $client->source;
            $social->client_id = $client->sourceId;
            $social->created_at = $currentTime;

            $valid = $profile->validate() && $profile->save();
            $valid = $valid && $social->validate() && $social->save();

            if ($valid) {
                $transaction->commit();

                try {
                    $user->sendNewUserLetters();
                } catch (Exception $e) {

                }

                return $user;
            }

            $transaction->rollback();
        }

        return null;
    }

    /**
     * Send user register coupon code if date less 30.09.2018.
     *
     * @static
     * @param User $user User model.
     * @return bool
     */
    public static function sendRegisterCouponCode(User $user)
    {
        $date = Setting::getDateRegisterForNewUserWithDiscount();

        if (!$date) {
            return false;
        }

        $template = EmailTemplate::findTemplateNewUserCoupon();

        if (!$template) {
            return false;
        }

        $date = new DateTime($date);
        $now = new DateTime();

        if ($now > $date) {
            return false;
        }

        $coupon = new Coupon();
        $coupon->type = Coupon::PERCENT_TYPE;
        $coupon->code = Coupon::generateGUID();
        $coupon->value = '19';
        $coupon->finish_date = '2018-09-30';

        $coupon->save();

        $text = $template->getTextWithReplacedPlaceholdersForNewUserCoupon($coupon->code, $user->profile);

        $mailer = Yii::$app->mailer;

        return $mailer->compose()
            ->setFrom([$mailer->transport->getUsername() => $template->from])
            ->setTo($user->email)
            ->setSubject($template->subject)
            ->setHtmlBody($text)
            ->send();
    }

    /**
     * Remove user from subscribers list.
     *
     * @return bool
     */
    public function unSubscribe()
    {
        $subscriber = Subscriber::find()
            ->where(['email' => $this->email])
            ->one();

        if ($subscriber) {
            $subscriber->delete();
        }

        return true;
    }

    /**
     * Check if user exits in subscriber list.
     *
     * @return bool
     */
    public function isSubsciber()
    {
        if ($this->subscriber) {
            return true;
        }

        $subscriber = Subscriber::find()
            ->where(['email' => $this->email])
            ->one();

        if ($subscriber) {
            return true;
        }

        return false;
    }

    /**
     * Get user full name.
     *
     * @return string
     */
    public function getFullName()
    {
        if ($this->profile) {
            return $this->profile->name . ' ' . $this->profile->surname;
        }

        return '';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriber()
    {
        return $this->hasOne(Subscriber::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSocial()
    {
        return $this->hasMany(Social::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmailToken()
    {
        return $this->hasOne(UserEmailToken::className(), ['user_id' => 'id']);
    }

    public static function getListUserTitles()
    {
        return [
            self::USER_TITLE_MRS => Yii::t('core', 'Mrs.'),
            self::USER_TITLE_MR  => Yii::t('core', 'Mr.'),
        ];
    }

    /**
     * Sent user email letters from confirm email
     *
     * @return void
     */
    public function sendNewUserLetters()
    {
        try {
            $this->sendRegisterEmailLetter();
            self::sendRegisterCouponCode($this);
        } catch (Exception $e) {
        }
    }

    /**
     * Send user register email.
     *
     * @access private
     * @return bool
     */
    private function sendRegisterEmailLetter()
    {
        $template = EmailTemplate::findOne([
            'code' => EmailTemplate::USER_REGISTER_TEMPLATE_CODE,
        ]);

        if (!$template) {
            return false;
        }

        $text = $template->getTextWithReplacedPlaceholders();

        $mailer = Yii::$app->mailer;

        return $mailer->compose()
            ->setFrom([$mailer->transport->getUsername() => $template->from])
            ->setTo($this->email)
            ->setSubject($template->subject)
            ->setHtmlBody($text)
            ->send();
    }
}
