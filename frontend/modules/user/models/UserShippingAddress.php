<?php

namespace frontend\modules\user\models;

use Yii;
use common\models\user\UserShippingAddress as BaseUserShippingAddress;
use frontend\modules\geo\models\Country;
use frontend\modules\geo\models\City;
use frontend\modules\product\models\forms\CheckoutShippingForm;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the frontend model class UserShippingAddress for table "{{%user_shipping_addresses}}" extends common model UserShippingAddress.
 *
 * @property City $city
 * @property Country $country
 * @property User $user
 */
class UserShippingAddress extends BaseUserShippingAddress
{
    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * Find all saved shipping addresses for some user.
     *
     * @param int $userId User id
     * @return static
     */
    public static function findAllAddressesForUser($userId)
    {
        return static::find()
            ->where(['user_id' => $userId])
            ->all();
    }

    /**
     * Save user shipping address from checkout form.
     *
     * @param \frontend\modules\product\models\forms\CheckoutShippingForm $checkoutShippingForm Checkout shipping form
     * @param int $userId User auth id
     * @return bool
     */
    public static function saveAddressFromCheckout(CheckoutShippingForm $checkoutShippingForm, $userId)
    {
        $model = new static();

        $model->user_id    = $userId;
        $model->title      = $checkoutShippingForm->title;
        $model->surname    = $checkoutShippingForm->surname;
        $model->name       = $checkoutShippingForm->name;
        $model->country_id = $checkoutShippingForm->countryId;
        $model->city       = $checkoutShippingForm->city;
        $model->address1   = $checkoutShippingForm->address1;
        $model->address2   = $checkoutShippingForm->address2;
        $model->zip        = $checkoutShippingForm->zip;
        $model->phone      = str_replace(' ', '', $checkoutShippingForm->phone);
        $model->email      = $checkoutShippingForm->email;

        $model->name_address = $model->generateSavedAddressName();

        if ($model->validate() && $model->save()) {
            return true;
        }

        return false;
    }

    public function getUserFullName()
    {
        return $this->surname . ' ' . $this->name;
    }

    public function getLocation()
    {
        return sprintf('%s %s, %s', $this->city, $this->zip, $this->country->name);
    }

    /**
     * Find default user payment card.
     *
     * @param int $userId User id
     * @return static
     */
    public static function getUserDefaultShippingAddress($userId)
    {
        return static::find()
            ->where(['user_id' => $userId, 'default' => 1])
            ->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Generate name for saved address
     *
     * @return string
     */
    public function generateSavedAddressName()
    {
        return $this->zip . ' ' . $this->address1;
    }
}
