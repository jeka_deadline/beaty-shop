<?php

namespace frontend\modules\user\models;

use common\models\user\UserResetPasswordToken as BaseUserResetPasswordToken;

/**
 * Frontend model user reset password token extends common model user reset password token.
 */
class UserResetPasswordToken extends BaseUserResetPasswordToken
{
    /**
     * Count minutes valid token for reset password.
     *
     * @var int
     */
    const MINUNES_VALID_TOKEN = 30;

    /**
     * Check if token reset password expired.
     *
     * @return bool
     */
    public function isTokenExpired()
    {
        $tokenTime = strtotime($this->updated_at);
        $now       = time();

        $diff = $now - $tokenTime;

        if ($diff < self::MINUNES_VALID_TOKEN * 60) {
            return false;
        }

        return true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
