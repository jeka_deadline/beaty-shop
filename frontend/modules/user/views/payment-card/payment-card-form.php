<?php

use yii\widgets\MaskedInput;

/** @var \frontend\modules\core\components\View $this */
/** @var \frontend\widgets\bootstrap4\ActiveForm $form */
/** @var \frontend\modules\user\models\forms\PaymentCardForm $paymentCardForm */

?>

<?php if (!$paymentCardForm->getPaymentCardId()) : ?>

    <?= $form->field($paymentCardForm, 'pseudocardpan', ['template' => '{input}'])->hiddenInput(['id' => 'pseudocardpan',])->label(false); ?>

    <?= $form->field($paymentCardForm, 'number', ['template' => '{input}'])->hiddenInput(['id' => 'truncatedcardpan'])->label(false); ?>

<?php endif; ?>

<?= $form->field($paymentCardForm, 'name', [
    'inputOptions' => [
        'required' => true,
        'id' => 'account-page__payment-name-add',
    ],
    'labelOptions' => [
        'class' => '',
    ],
]); ?>

<?php if (!$paymentCardForm->getPaymentCardId()) : ?>

    <div class="account-page__number-expiration">
        <div class="block-payment-credit-card">

            <?= $this->render('@app/modules/product/views/checkout/checkout-credit-card-form'); ?>

        </div>

    </div>

<?php endif; ?>