<?php

use yii\helpers\Url;
use yii\helpers\Html;

/** @var \frontend\modules\core\components\View $this */
/** @var \frontend\modules\user\models\UserPaymentCard[] $paymentCards */
/** @var \frontend\modules\user\models\UserPaymentCard $paymentCard */
/** @var \frontend\modules\user\models\forms\DefaultUserPaymentCardForm $defaultUserPaymentCardForm */

?>
<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade active show" id="list-payment" role="tabpanel" aria-labelledby="list-payment-list">
        <div class="account-page__payment-ready">
            <h4><?= Yii::t('user', 'Payment Methods') ?></h4>
            <p class="account-page__right-head-p"><?= Yii::t('payment', 'You can add or update your Infiniti Lashes Payment Method at any time.'); ?></p>
            <button class="btn btn-primary account-page__payment-add-new auto-width" data-create-url="<?= Url::toRoute(['/user/payment-card/create']); ?>"><?= Yii::t('payment', 'Add new'); ?></button>
            <h6><?= Yii::t('payment', 'My cards'); ?></h6>
            <div class="account-page__select-card">
                <div class="grey-form">

                    <?= Html::activeDropDownList($defaultUserPaymentCardForm, 'defaultPaymentCardId', $defaultUserPaymentCardForm->getListPaymentCardsHashMap(), [
                        'class' => 'form-control',
                        'id' => 'account-page__select-card',
                        'data' => [
                            'change-url' => Url::toRoute(['/user/payment-card/change-user-default-card'])
                        ],
                    ]); ?>

                </div>
                <p><?= Yii::t('payment', 'Select the primary'); ?></p>
            </div>
            <div class="account-page__card-items" data-delete-url="<?= Url::toRoute(['/user/payment-card/delete']); ?>" data-edit-url="<?= Url::toRoute(['/user/payment-card/edit']); ?>">

                <?php foreach ($paymentCards as $paymentCard) : ?>

                    <div class="account-page__card-item">
                        <p class="btn-primary"><?= $paymentCard->name; ?></p>
                        <p class="account-page__item-text"><?= $paymentCard->getTypeCard(); ?></p>
                        <p class="account-page__item-text"><?= $paymentCard->number; ?></p>
                        <div class="account-page__item-edit">
                            <a href="#" class="action-edit" data-id="<?= $paymentCard->id; ?>"><?= Yii::t('payment', 'Edit'); ?></a>
                            <a href="#" data-id="<?= $paymentCard->id; ?>" class="action-delete"><?= Yii::t('payment', 'Delete'); ?></a>
                        </div>
                    </div>

                <?php endforeach; ?>

            </div>
        </div>
    </div>
</div>
