<?php

use yii\helpers\Url;

/** @var \frontend\modules\core\components\View $this */
?>

<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade active show" id="list-payment" role="tabpanel" aria-labelledby="list-payment-list">
        <div class="account-page__payment-new">
            <h4><?= Yii::t('user', 'Payment Methods') ?></h4>
            <p class="account-page__right-head-p"><?= Yii::t('payment', 'You can add or update your Infiniti Lashes Payment Method at any time.'); ?></p>
            <button data-create-url="<?= Url::toRoute(['/user/payment-card/create']); ?>" class="btn btn-primary account-page__payment-add-new first-add auto-width"><?= Yii::t('payment', 'Add new'); ?></button>
        </div>
    </div>
</div>
