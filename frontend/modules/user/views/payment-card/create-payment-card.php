<?php

use yii\helpers\Url;
use frontend\widgets\bootstrap4\ActiveForm;

/** @var \frontend\modules\core\components\View $this */
/** @var \frontend\widgets\bootstrap4\ActiveForm $form */
/** @var \frontend\modules\user\models\forms\PaymentCardForm $paymentCardForm */

?>

<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade active show" id="list-payment" role="tabpanel" aria-labelledby="list-payment-list">
        <div class="account-page__payment-add">
            <h4><?= Yii::t('user', 'Payment Methods') ?></h4>
            <p class="account-page__right-head-p"><?= Yii::t('payment', 'You can add or update your Infiniti Lashes Payment Method at any time.'); ?></p>
            <h6><?= Yii::t('payment', 'Add card'); ?></h6>

            <?php $form = ActiveForm::begin([
                'id' => 'add-new-card',
                'options' => [
                    'class' => 'grey-form',
                ],
                'enableClientValidation' => false,
                'enableAjaxValidation' => true,
                'validationUrl' => ['/user/payment-card/ajax-validation-payment-card'],
                'action' => ['/user/payment-card/store'],
            ]); ?>

                <?= $this->render('payment-card-form', compact('form', 'paymentCardForm')); ?>

                <button class="btn btn-primary" type="submit" form="add-new-card"><?= Yii::t('core', 'Save'); ?></button>
                <a href="#" class="btn btn-primary btn-cancel" data-url="<?= Url::toRoute(['/user/payment-card/get-index-payment-page']); ?>">abbrechen</a>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>

<div id="paymentform"></div>