<?php

use yii\helpers\Url;

/** @var \frontend\modules\core\components\View $this */

?>

<div class="account-page__unsubscribe-link-wrapper">
    <?= Yii::t('user', 'Unsubcribe&nbsp;<a class="account-page__unsubscribe-link" href="{0}">newsletter</a>',[
        Url::toRoute(['/user/user/unsubscribe'])
    ]) ?>
    <svg class="unsubscribed-icon svg">
        <use xlink:href="#unsubscribed"></use>
    </svg>
</div>