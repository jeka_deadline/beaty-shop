<?php

use frontend\widgets\bootstrap4\ActiveForm;
use frontend\modules\user\widgets\ProfileGenderChoose;
use yii\widgets\MaskedInput;
use yii\helpers\Url;

/** @var \frontend\modules\core\components\View $this */
/** @var \frontend\modules\user\models\forms\ProfileForm $profileForm */
/** @var \frontend\modules\user\models\forms\ChangeProfilePasswordForm $changePasswordForm */
/** @var \frontend\widgets\bootstrap4\ActiveForm $form */

?>
<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade active show profile-and-settings" id="list-profile" role="tabpanel" aria-labelledby="list-profile-list">
        <h4><?= Yii::t('user', 'Profile and settings') ?></h4>
        <p class="account-page__right-head-p"><?= Yii::t('user', 'You can update your Infiniti Lashes account at any time and make any changes to the information below.') ?></p>
        <div class="grey-form__with-left-line">

            <?php $form = ActiveForm::begin([
                'options' => [
                    'class' => 'grey-form',
                ],
                'action' => ['/user/user/update-profile'],
                'fieldConfig' => [
                    'errorOptions' => [
                        'tag' => 'div',
                        'class' => 'invalid-feedback',
                    ],
                ],
                'enableClientValidation' => false,
            ]); ?>

                <?= $form->field($profileForm, 'title', [
                    'inputOptions' => [
                        'required' => true,
                        'class' => ($profileForm->getErrors('title')) ? 'is-invalid form-control' : 'form-control',
                    ],
                ])->dropDownList($profileForm->getListUserTitles(), [
                    'id' => 'account-page__select-who',
                ])->label($profileForm->getAttributeLabel('title').'*'); ?>

                <?= $form->field($profileForm, 'name', [
                    'inputOptions' => [
                        'required' => true,
                        'class' => ($profileForm->getErrors('name')) ? 'is-invalid form-control' : 'form-control',
                        'id' => 'account-page__first-name',
                    ],
                ])->label($profileForm->getAttributeLabel('name').'*'); ?>

                <?= $form->field($profileForm, 'surname', [
                    'inputOptions' => [
                        'required' => true,
                        'class' => ($profileForm->getErrors('surname')) ? 'is-invalid form-control' : 'form-control',
                        'id' => 'account-page__last-name',
                    ],
                ])->label($profileForm->getAttributeLabel('surname').'*'); ?>

                <?= $form->field($profileForm, 'company', [
                    'inputOptions' => [
                        'class' => ($profileForm->getErrors('company')) ? 'is-invalid form-control' : 'form-control',
                        'id' => 'account-page__company-name',
                    ],
                ]); ?>

                <?= $form->field($profileForm, 'email', [
                    'inputOptions' => [
                        'required' => true,
                        'aria-describedby' => 'emailHelp',
                        'class' => ($profileForm->getErrors('email')) ? 'is-invalid form-control' : 'form-control',
                        'id' => 'account-page__email-input',
                    ],
                ])->label($profileForm->getAttributeLabel('email').'*'); ?>

                <?= $form->field($profileForm, 'phone', [
                    'inputOptions' => [
                        'required' => true,
                        'class' => ($profileForm->getErrors('phone')) ? 'is-invalid form-control' : 'form-control',
                    ],
                ])->widget(MaskedInput::className(), [
                    'mask' => $profileForm->getPhoneMask(),
                    'options' => [
                        'id' => 'checkout-page__phone-add',
                        'class' => ($profileForm->getErrors('phone')) ? 'is-invalid form-control' : 'form-control',
                        'required' => true,
                    ],
                ]); ?>

                <label for="account-page__select-day"><?= Yii::t('user', 'Birthday') ?></label>
                <div class="grey-form__date-pick">

                    <?= $form->field($profileForm, 'dayBirth', [
                        'inputOptions' => [
                            'class' => ($profileForm->getErrors('dayBirth')) ? 'is-invalid form-control account-page__select-day' : 'form-control account-page__select-day',
                            'id' => 'account-page__select-day',
                        ],
                    ])
                    ->dropDownList($profileForm->getListDays(), ['prompt' => ''])
                    ->label(false); ?>

                    <?= $form->field($profileForm, 'monthBirth', [
                        'inputOptions' => [
                            'class' => ($profileForm->getErrors('monthBirth')) ? 'is-invalid form-control account-page__select-month' : 'form-control account-page__select-month',
                            'id' => 'account-page__select-month',
                        ],
                    ])
                    ->dropDownList($profileForm->getListMonths(), ['prompt' => ''])
                    ->label(false); ?>

                    <?= $form->field($profileForm, 'yearBirth', [
                        'inputOptions' => [
                            'class' => ($profileForm->getErrors('yearBirth')) ? 'is-invalid form-control account-page__select-year' : 'form-control account-page__select-year',
                            'id' => 'account-page__select-year',
                        ],
                    ])
                    ->dropDownList($profileForm->getListYears(), ['prompt' => ''])
                    ->label(false); ?>

                </div>

                <hr>

                <?php if ($changePasswordForm->loginWithNotSocial()) : ?>

                    <?= $form->field($changePasswordForm, 'oldPassword', [
                        'inputOptions' => [
                            'class' => ($changePasswordForm->getErrors('oldPassword')) ? 'is-invalid form-control' : 'form-control',
                            'id' => 'account-page__old-pass-input',
                        ],
                    ])->passwordInput(); ?>

                <?php endif; ?>

                <?= $form->field($changePasswordForm, 'newPassword', [
                    'inputOptions' => [
                        'class' => ($changePasswordForm->getErrors('newPassword')) ? 'is-invalid form-control' : 'form-control',
                        'id' => 'account-page__new-pass-input',
                    ],
                ])->passwordInput(); ?>

                <?= $form->field($changePasswordForm, 'repeatNewPassword', [
                    'inputOptions' => [
                        'class' => ($changePasswordForm->getErrors('repeatNewPassword')) ? 'is-invalid form-control' : 'form-control',
                        'id' => 'account-page__new-pass-check',
                    ],
                ])->passwordInput(); ?>

                <hr class="account-page__unsubscribe">

                <?php if ($profileForm->isUserExistInSubscriberList()) : ?>

                    <?= $this->render('unsubscribe-link'); ?>

                <?php else : ?>

                    <?= $this->render('subscribe-link'); ?>

                <?php endif; ?>

                <button class="btn btn-primary" type="submit"><?= Yii::t('user', 'Save changes') ?></button>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
<div class="svgHide">
    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100" height="100" style="position:absolute">
        <symbol id="unsubscribed" viewBox="0 0 20 20">
            <defs>
                <style>.cls-1-uns-ok{fill:none;stroke:#fff;stroke-miterlimit:10}</style>
            </defs>
            <title>ok_round</title>
            <g id="Layer_2" data-name="Layer 2">
                <g id="ok_check">
                    <circle cx="10" cy="10" r="10"></circle>
                    <path class="cls-1-uns-ok" d="M15.04 5l-7.22 9.49-2.87-3.48"></path>
                </g>
            </g>
        </symbol>
    </svg>
</div>