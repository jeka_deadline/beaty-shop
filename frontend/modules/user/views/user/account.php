<?php

use frontend\modules\user\assets\AccountAsset;
use yii\helpers\Url;

$this->bodyClass = 'account-page';

AccountAsset::register($this);

/** @var \frontend\modules\core\components\View $this */
/** @var \frontend\modules\user\models\User $user */
/** @var \frontend\modules\user\models\forms\ProfileForm $profileForm */
/** @var \frontend\modules\user\models\forms\ChangeProfilePasswordForm $changePasswordForm */
?>

<main class="container">
    <h1><?= Yii::t('user', 'My account') ?></h1>
    <a class="back-to-menu" href="#">
        <h2>Back to menu
      <svg class="back-adaptive-menu svg">
        <use xlink:href="#totop"></use>
      </svg>
    </h2></a>
    <div class="account-page__two-parts-wrapper row">
        <div class="account-page__left-part col-xl-5 offset-xl-1 col-lg-6 offset-lg-0">
            <p class="account-page__hello"><?= Yii::t('user', 'Hello') ?>, <span><?= $user->fullName; ?></span></p>
            <div class="list-group" id="account-page__list-tab">
                <a class="list-group-item list-group-item-action active" data-url="<?= Url::toRoute(['/user/user/get-index-profile-page']); ?>" id="list-profile-list"><?= Yii::t('user', 'Profile and settings') ?></a>
                <a class="list-group-item list-group-item-action" href="#" data-url="<?= Url::toRoute(['/user/payment-card/get-index-payment-page']); ?>" id="list-payment-list"><?= Yii::t('user', 'Payment Methods') ?></a>
                <a class="list-group-item list-group-item-action" href="#" data-url="<?= Url::toRoute(['/user/shipping/get-index-shipping-page']); ?>" id="list-shipping-adress-list"><?= Yii::t('user', 'Shipping Address') ?></a>
                <a class="list-group-item list-group-item-action" href="#" data-url="<?= Url::toRoute(['/product/order/get-index-order-page']); ?>" id="list-orders-list"><?= Yii::t('user', 'My orders') ?></a>
                <a class="list-group-item list-group-item-action" href="<?= Url::toRoute(['/user/security/logout']); ?>" data-method="post" id="list-logout-list"><?= Yii::t('user', 'Logout') ?></a>
            </div>
        </div>
        <div class="account-page__right-part col-xl-6 col-lg-6">

            <?= $this->render('profile-form-tab', compact('profileForm', 'changePasswordForm')); ?>

        </div>

    </div>
</main>

<script src="https://secure.pay1.de/client-api/js/v1/payone_hosted_min.js"></script>