<?php

use yii\helpers\Url;

/** @var \frontend\modules\core\components\View $this */
?>

<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade active show" id="list-shipping-adress" role="tabpanel" aria-labelledby="list-shipping-adress-list">
        <div class="account-page__shipping-adress-new">
            <h4><?= Yii::t('user', 'Shipping Address') ?></h4>
            <p class="account-page__right-head-p"><?= Yii::t('core', 'You can add or update your Infiniti Lashes Payment Method at any time.'); ?></p>
            <button class="btn btn-primary auto-width" data-create-url="<?= Url::toRoute(['/user/shipping/create']); ?>"><?= Yii::t('shipping', 'Add new'); ?></button>
        </div>
    </div>
</div>
