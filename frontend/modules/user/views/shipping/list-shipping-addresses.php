<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\StringHelper;

/** @var \ii\web\View $this */
/** @var \frontend\modules\user\models\UserShippingAddress[] $shippingAddresses */
/** @var \frontend\modules\user\models\forms\DefaultUserShippingForm $defaultUserShippingForm */
/** @var \frontend\modules\user\models\UserShippingAddress $address */

?>

<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade active show" id="list-shipping-adress" role="tabpanel" aria-labelledby="list-shipping-adress-list">
        <div class="account-page__shipping-address-ready">
            <h4><?= Yii::t('user', 'Shipping Address') ?></h4>
            <p class="account-page__right-head-p"><?= Yii::t('core', 'You can add or update your Infiniti Lashes Payment Method at any time.'); ?></p>
            <button class="btn btn-primary account-page__shipping-address-add-new auto-width" data-create-url="<?= Url::toRoute(['/user/shipping/create']); ?>"><?= Yii::t('shipping', 'Add new'); ?></button>
            <h6><?= Yii::t('shipping', 'My adresses'); ?></h6>
            <div class="account-page__select-home">
                <div class="grey-form">

                    <?= Html::activeDropDownList($defaultUserShippingForm, 'defaultShippingAddressId', $defaultUserShippingForm->getListShippingAddressHashMap(), [
                        'class' => 'form-control',
                        'id' => 'account-page__select-home',
                        'data' => [
                            'change-url' => Url::toRoute(['/user/shipping/change-user-default-shipping'])
                        ],
                    ]); ?>

                    <p><?= Yii::t('shipping', 'Select the primary'); ?></p>
                </div>
            </div>
            <div class="account-page__shipping-address-items" data-delete-url="<?= Url::toRoute(['/user/shipping/delete']); ?>" data-edit-url="<?= Url::toRoute(['/user/shipping/edit']); ?>">

                <?php foreach ($shippingAddresses as $address) : ?>

                    <div class="account-page__shipping-address-item">
                        <p class="btn-primary"><?= $address->name_address; ?></p>
                        <p class="account-page__item-text"><?= $address->getUserFullName(); ?></p>
                        <p class="account-page__item-text"><?= $address->address1; ?></p>
                        <p class="account-page__item-text"><?= $address->getLocation(); ?></p>
                        <p class="account-page__item-text"><?= StringHelper::truncate($address->email, 27, '...', 'utf-8'); ?></p>
                        <div class="account-page__item-edit">
                            <a href="#" class="action-edit" data-id="<?= $address->id; ?>"><?= Yii::t('shipping', 'Edit'); ?></a>
                            <a href="#" class="action-delete" data-id="<?= $address->id; ?>"><?= Yii::t('shipping', 'Delete'); ?></a>
                        </div>
                    </div>

                <?php endforeach; ?>

            </div>
        </div>
    </div>
</div>
