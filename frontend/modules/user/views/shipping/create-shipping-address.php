<?php

use yii\helpers\Url;
use frontend\widgets\bootstrap4\ActiveForm;

/** @var \yii\widgets\ActiveForm $form */
/** @var \frontend\modules\user\models\forms\UserShippingInformationForm $shippingForm */
/** @var \frontend\modules\core\components\View $this */

?>

<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade active show" id="list-shipping-adress" role="tabpanel" aria-labelledby="list-shipping-adress-list">
        <div class="account-page__shipping-address-add">
            <h4><?= Yii::t('user', 'Shipping Address') ?></h4>
            <p class="account-page__right-head-p"><?= Yii::t('core', 'You can add or update your Infiniti Lashes Payment Method at any time.'); ?></p>
            <h6><?= Yii::t('shipping', 'Add shipping address'); ?></h6>

            <?php $form = ActiveForm::begin([
                'id' => 'add-new-adress',
                'options' => [
                    'class' => 'grey-form',
                ],
                'enableClientValidation' => false,
                'enableAjaxValidation' => true,
                'validationUrl' => ['/user/shipping/ajax-validation-shipping'],
                'action' => ['/user/shipping/store'],
            ]); ?>

                <?= $this->render('shipping-address-form', compact('form', 'shippingForm')); ?>

                <button class="btn btn-primary" type="submit" form="add-new-adress"><?= Yii::t('core', 'Save'); ?></button>
                <a href="#" class="btn btn-primary btn-cancel" data-url="<?= Url::toRoute(['/user/shipping/get-index-shipping-page']); ?>">abbrechen</a>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>