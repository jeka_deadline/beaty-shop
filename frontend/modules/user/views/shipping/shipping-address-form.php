<?php

use frontend\modules\user\models\User;
use yii\widgets\MaskedInput;
use yii\helpers\Html;

/** @var \yii\widgets\ActiveForm $form */
/** @var \frontend\modules\user\models\forms\UserShippingInformationForm $shippingForm */
/** @var \frontend\modules\core\components\View $this */

?>

<?= $form->field($shippingForm, 'name', [
    'labelOptions' => [
        'class' => false,
    ],
    'inputOptions' => [
        'placeholder' => 'Address Name',
        'id' => 'account-page__adress-name-add',
        'class' => 'form-control',
        'required' => true,
    ],
]); ?>

<?= $form->field($shippingForm, 'title', [
    'labelOptions' => [
        'class' => false,
    ],
    'inputOptions' => [
        'id' => 'account-page__title-add',
        'class' => 'form-control',
        'required' => true,
    ],
])->dropDownList(User::getListUserTitles()); ?>

<?= $form->field($shippingForm, 'userName', [
    'labelOptions' => [
        'class' => false,
    ],
    'inputOptions' => [
        'placeholder' => Yii::t('core', 'First Name'),
        'id' => 'account-page__first-name-add',
        'class' => 'form-control',
        'required' => true,
    ],
])->label($shippingForm->getAttributeLabel('userName') . '*'); ?>

<?= $form->field($shippingForm, 'userSurname', [
    'labelOptions' => [
        'class' => false,
    ],
    'inputOptions' => [
        'placeholder' => Yii::t('core', 'Last Name'),
        'id' => 'account-page__last-name-add',
        'class' => 'form-control',
        'required' => true,
    ],
])->label($shippingForm->getAttributeLabel('userSurname') . '*'); ?>

<div class="form-group account-page__address-input">
    <label for="account-page__address-add-1"><?= $shippingForm->getAttributeLabel('userAddress1'); ?>*</label>
</div>
<div class="account-page__add-remove-js">

  <?= $form->field($shippingForm, 'userAddress1', [
      'labelOptions' => [
          'class' => false,
      ],
      'inputOptions' => [
          'placeholder' => 'Kaiserstr. 59',
          'id' => 'account-page__address-add-1',
          'class' => 'form-control',
          'required' => true,
      ],
      'options' => [
          'tag' => false,
      ],
  ])->label(false); ?>

  <?php if ($shippingForm->userAddress2) : ?>

      <?= $form->field($shippingForm, 'userAddress2', [
          'inputOptions' => [
              'placeholder' => 'Kaiserstr. 59',
              'id' => 'account-page__address-add-2',
              'class' => 'form-control',
              'required' => true,
          ],
          'options' => [
              'tag' => false,
          ],
      ])->label(false); ?>

  <?php endif; ?>

  <div class="account-page__address-input-add-remove">
      <a class="account-page__add-address-line" data-model-name="<?= Html::getInputName($shippingForm, 'userAddress2'); ?>" href="#" style="<?= ($shippingForm->userAddress2) ? 'visibility: hidden' : 'visibility: visible'; ?>">+ <?= Yii::t('shipping', 'Add address line'); ?></a>
      <a style="<?= ($shippingForm->userAddress2) ? 'visibility: visible' : 'visibility: hidden'; ?>" class="account-page__remove-address-line" href="#">- <?= Yii::t('shipping', 'Remove address line'); ?></a>
  </div>
</div>

<div class="account-page__city-zip">

    <?= $form->field($shippingForm, 'userCity', [
        'labelOptions' => [
            'class' => false,
        ],
        'inputOptions' => [
            'placeholder' => 'E. g. Dortmund',
            'id' => 'account-page__city-add',
            'class' => 'form-control',
            'required' => true,
        ],
    ])->label($shippingForm->getAttributeLabel('userCity') . '*'); ?>

    <?= $form->field($shippingForm, 'zip', [
        'labelOptions' => [
            'class' => false,
        ],
        'options' => [
            'class' => 'from-group account-page__zip-code',
        ]
    ])
    ->widget(MaskedInput::className(), [
        'mask' => '99999',
        'options' => [
            'placeholder' => '44135',
            'id' => 'account-page__zip-add',
            'class' => 'form-control',
            'required' => true,
        ],
    ])
    ->label($shippingForm->getAttributeLabel('zip') . '*'); ?>

</div>

<?= $form->field($shippingForm, 'userCountryId', [
    'labelOptions' => [
        'class' => false,
    ],
    'inputOptions' => [
        'id' => 'account-page__country-add',
        'class' => 'form-control',
        'required' => true,
    ],
])->dropDownList($shippingForm->getListCountriesForDropDown()); ?>

<?= $form->field($shippingForm, 'userEmail', [
    'labelOptions' => [
        'class' => false,
    ],
    'inputOptions' => [
        'id' => 'account-page__email-add',
        'class' => 'form-control',
        'required' => true,
        'aria-describedby' => 'emailHelp',
    ],
])->label($shippingForm->getAttributeLabel('userEmail') . '*'); ?>

<?= $form->field($shippingForm, 'userPhone', [
    'labelOptions' => [
        'class' => false,
    ],
])->widget(MaskedInput::className(), [
    'mask' => $shippingForm->getPhoneMask(),
    'options' => [
        'id' => 'account-page__phone-add',
        'class' => 'form-control',
        'required' => true,
    ],
])->label($shippingForm->getAttributeLabel('userPhone') . '*'); ?>