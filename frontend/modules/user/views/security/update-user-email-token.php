<?php

use yii\helpers\Url;

?>

<main>
    <div class="container">
        <h1 class="simple-header"><?= Yii::t('core', 'Information message'); ?></h1>
        <hr>

        <div class="container">
            <article class="col-xl-8 offset-xl-2 col-lg-10 offset-lg-1" style="text-align: center">
                <p><?= Yii::t('user', 'The token\'s lifetime has been exhausted. To re-create it, click <a class="regenerate-token" data-url="{0}" data-method="{1}" href="javascript:void(0);"><b>this</b></a>', [
                    Url::toRoute(['/user/security/update-user-confirm-email-token', 'token' => $token]),
                    'post',
                ]); ?></p>
                <div id="preloader" style="display: none;">
                    <img src="/img/preloader.gif" alt="">
                </div>
            </article>
        </div>

    </div>
</main>