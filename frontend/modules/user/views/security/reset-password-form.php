<?php

use frontend\widgets\bootstrap4\ActiveForm;

/** @var \frontend\modules\core\components\View $this */
/** @var \frontend\widgets\bootstrap4\ActiveForm $form */
?>

<?php $form = ActiveForm::begin([
    'options' => [
        'class' => 'grey-form col-xl-4 offset-xl-4 col-lg-6 offset-lg-3 col-md-8 offset-md-2 col-sm-12',
    ],
    'id' => 'save-new-password-form',
]); ?>

    <?= $form->field($model, 'password')->passwordInput(['id' => 'change-pass-page__new-member-pass-input']); ?>

    <?= $form->field($model, 'repeatPassword')->passwordInput(['id' => 'change-pass-page__new-member-pass-check']); ?>

    <button class="btn btn-primary" type="submit"><?= Yii::t('user', 'change password'); ?></button>

<?php ActiveForm::end(); ?>
