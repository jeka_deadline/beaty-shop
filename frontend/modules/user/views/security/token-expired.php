<?php

use yii\helpers\Html;

?>

You token is expired. Go to <?= Html::a('Reset token', ['/user/security/update-user-confirm-email-token']);