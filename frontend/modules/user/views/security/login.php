<?php

use frontend\widgets\bootstrap4\Html;
use yii\helpers\Url;
use frontend\widgets\bootstrap4\ActiveForm;
use frontend\modules\user\assets\UserAsset;

$this->bodyClass = 'account-login-page';

UserAsset::register($this);

/** @var \frontend\modules\core\components\View $this */
/** @var \yii\widgets\ActiveForm $form */
/** @var \frontend\modules\user\models\forms\LoginForm $loginForm */
/** @var \frontend\modules\user\models\forms\SignupForm $signupForm */

?>
<section class="container">
    <div class="row account-login-page__wrapper">
        <div class="account-login-page__left-part offset-xl-1 col-xl-5 col-lg-6 offset-lg-0 col-md-8 offset-md-2 col-sm-12">
            <h2><?= Yii::t('user', 'Member Sign In') ?></h2>

            <?php $form = ActiveForm::begin([
                'action' => ['/user/security/login'],
                'options' => [
                    'class' => 'grey-form',
                    'id' => $loginForm::FORM_ID,
                ],
                'fieldConfig' => [
                    'errorOptions' => [
                        'tag' => 'div',
                    ],
                ],
            ]); ?>

                <?= $form->field($loginForm, 'email', [
                    'labelOptions' => [
                        'class' => '',
                    ],
                    'inputOptions' => [
                        'aria-describedb' => 'emailHelp',
                        'id' => 'account-login-page__member-email-input',
                    ],
                ])->label($loginForm->getAttributeLabel('email').'*'); ?>

                <?= $form->field($loginForm, 'password', [
                    'labelOptions' => [
                        'class' => '',
                    ],
                ])->passwordInput([
                    'id' => 'account-login-page__member-pass-input',
                ])->label($loginForm->getAttributeLabel('password').'*'); ?>

                <?= $form->field($loginForm, 'rememberMe', [
                    'options' => [
                        'class' => 'form-check',
                    ],
                    'checkboxTemplate' => '<div class="grey-form__forgot-pass custom-control custom-checkbox">{input}{label}<a class="account-login-page__forgot-pass" href="#" data-url=' . Url::toRoute(['/user/security/request-password-reset']) . '>'.Yii::t('user', 'Forgot password').'?</a></div>',
                ])->checkbox(
                    ['class' => 'custom-control-input account-login-page__member-check',],
                    ['class' => 'custom-control-label',]
                ); ?>

                <?= $form->field($loginForm, 'agreed', [
                    'options' => [
                        'class' => 'form-check',
                    ],
                    'checkboxTemplate' => '<div class="grey-form__forgot-pass custom-control custom-checkbox">{input}{label}</div>{error}<br>',
                    'labelOptions' => [
                         'encode' => false,
                    ]
                ])->checkbox(
                    ['class' => 'custom-control-input account-login-page__member-check', 'data-url' => Url::toRoute(['/user/security/set-social-agree'])],
                    ['class' => 'custom-control-label',]
                )->label(
                    Yii::t('user', 'I confirm that I have read and agree to the <a href="{0}">terms and conditions</a> and <a href="{1}">privacy policy</a> of Infinity Lashes', [
                        Url::toRoute(['/pages/index/page', 'uri' => 'terms']),
                        Url::toRoute(['/pages/index/page', 'uri' => 'cookie'])
                    ])
                ); ?>

                <button class="btn btn-primary" type="submit"><?= Yii::t('user', 'Sign in') ?></button>
                <div class="with-lines-wrapper">
                    <div class="grey-form__line"></div>
                    <p class="grey-form__or-with-lines"><?= Yii::t('user', 'or') ?></p>
                    <div class="grey-form__line"></div>
                </div>
                <a class="btn btn-primary facebook-btn" href="<?= Url::to(['/user/security/auth', 'authclient' => 'facebook', 'action' => $loginForm::FORM_SOCIAL_ACTION]); ?>">
                    <svg class="fb_svg" enable-background="new 0 0 512 512" id="Layer_1" version="1.1" viewBox="0 0 512 512" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g><path d="M308.3,508.5c-2.5,0.1-4.1,0.3-5.7,0.3c-34.2,0-68.3-0.1-102.5,0.1c-4.8,0-6.1-1.3-6.1-6.1c0.1-79.6,0.1-159.3,0.1-238.9   c0-2.1,0-4.2,0-6.9c-18.6,0-36.7,0-55.1,0c0-28.4,0-56.3,0-85c1.9,0,3.7,0,5.4,0c15,0,30-0.1,45,0.1c3.8,0,4.8-1.1,4.8-4.8   c-0.2-22.3-0.2-44.7,0-67c0.1-15.6,2.6-30.8,9.8-44.9c10.3-19.9,26.6-32.8,47.2-40.8c16.8-6.6,34.5-9,52.3-9.3   c29-0.4,58-0.2,87-0.3c2.7,0,4.9-0.1,4.9,3.7c-0.1,27.5-0.1,55-0.1,82.5c0,0.3-0.1,0.6-0.5,1.9c-1.7,0-3.6,0-5.5,0   c-18,0-36-0.1-54,0c-10.4,0-18.8,4.2-24.1,13.3c-1.6,2.7-2.6,6.2-2.6,9.4c-0.3,17,0,34-0.2,51c0,4,1.2,5.1,5.1,5.1   c25-0.2,50-0.1,75-0.1c2,0,3.9,0,7.3,0c-3.5,28.6-6.9,56.6-10.4,84.9c-26,0-51.3,0-77.1,0C308.3,340.8,308.3,424.4,308.3,508.5z"/></g></svg>
                    <span>
                        <?= Yii::t('user', 'sign in with facebook') ?>
                    </span>
                </a>
                <a class="btn btn-primary google-btn" href="<?= Url::to(['/user/security/auth', 'authclient' => 'google', 'action' => $loginForm::FORM_SOCIAL_ACTION]); ?>">
                    <svg class="g_pl" id="Layer_2" version="1.1" viewBox="0 0 56.6934 56.6934" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g><path d="M19.6671,25.7867c-0.0075,1.7935,0,3.5869,0.0076,5.3803c3.0067,0.098,6.0208,0.0527,9.0275,0.098   c-1.3262,6.6689-10.3989,8.8315-15.199,4.4761C8.5674,31.9206,8.801,23.5412,13.9327,19.992   c3.5869-2.8635,8.6884-2.1552,12.2752,0.324c1.4092-1.3036,2.7278-2.6977,4.0013-4.1445   c-2.984-2.3812-6.6462-4.0767-10.5421-3.8958c-8.1307-0.2713-15.6059,6.8497-15.7415,14.9805   c-0.52,6.6462,3.8506,13.1644,10.0222,15.5155c6.1489,2.3661,14.031,0.7535,17.957-4.77c2.5922-3.4889,3.1498-7.98,2.8484-12.1999   C29.7194,25.7641,24.6933,25.7716,19.6671,25.7867z"/><path d="M49.0704,25.7641c-0.0151-1.4996-0.0226-3.0067-0.0301-4.5062c-1.4996,0-2.9916,0-4.4836,0   c-0.0151,1.4996-0.0301,2.9991-0.0377,4.5062c-1.5071,0.0075-3.0067,0.0151-4.5062,0.0302c0,1.4995,0,2.9915,0,4.4836   c1.4995,0.0151,3.0066,0.0302,4.5062,0.0452c0.0151,1.4996,0.0151,2.9991,0.0302,4.4987c1.4996,0,2.9916,0,4.4911,0   c0.0075-1.4996,0.015-2.9991,0.0301-4.5062c1.5071-0.0151,3.0067-0.0226,4.5062-0.0377c0-1.4921,0-2.9916,0-4.4836   C52.0771,25.7792,50.57,25.7792,49.0704,25.7641z"/></g></svg>
                    <span>
                        <?= Yii::t('user', 'sign in with google +') ?>
                    </span>
                </a>

            <?php ActiveForm::end(); ?>

        </div>
        <div class="account-login-page__right-part col-xl-5 col-lg-6 offset-lg-0 col-md-8 offset-md-2 col-sm-12">
            <h2><?= Yii::t('user', 'Create an Account') ?></h2>
            <div class="account-login-page__with-left-line">

                <?php $form = ActiveForm::begin([
                    'action' => ['/user/security/signup'],
                    'options' => [
                        'class' => 'grey-form',
                    ],
                    'id' => $signupForm::FORM_ID,
                    'enableAjaxValidation' => true,
                    'validationUrl' => Url::toRoute(['/user/security/validate-signup-form']),
                ]); ?>

                    <?= $form->field($signupForm, 'title', [
                        'labelOptions' => [
                            'class' => '',
                        ],
                    ])->dropDownList($signupForm->getListUserTitles(), [
                        'prompt' => '',
                        'id' => 'account-login-page__select-who',
                    ])->label($signupForm->getAttributeLabel('title').'*'); ?>

                    <?= $form->field($signupForm, 'name', [
                        'inputOptions' => [
                            'aria-describedb' => 'emailHelp',
                            'id' => 'account-login-page__new-member-first-name',
                        ],
                        'labelOptions' => [
                            'class' => '',
                        ],
                    ])->label($signupForm->getAttributeLabel('name').'*'); ?>

                    <?= $form->field($signupForm, 'surname', [
                        'inputOptions' => [
                            'aria-describedb' => 'emailHelp',
                            'id' => 'account-login-page__new-member-last-name',
                        ],
                        'labelOptions' => [
                            'class' => '',
                        ],
                    ])->label($signupForm->getAttributeLabel('surname').'*'); ?>

                    <?= $form->field($signupForm, 'company', [
                        'inputOptions' => [
                            'id' => 'account-login-page__company-name',
                        ],
                        'labelOptions' => [
                            'class' => '',
                        ],
                    ]); ?>

                    <?= $form->field($signupForm, 'email', [
                        'inputOptions' => [
                            'aria-describedb' => 'emailHelp',
                            'id' => 'account-login-page__new-member-email-input',
                        ],
                        'labelOptions' => [
                            'class' => '',
                        ],
                    ])->label($signupForm->getAttributeLabel('email').'*'); ?>

                    <?= $form->field($signupForm, 'password', [
                        'labelOptions' => [
                            'class' => '',
                        ],
                    ])->passwordInput([
                        'id' => 'account-login-page__new-member-pass-input',
                    ])->label($signupForm->getAttributeLabel('password').'*'); ?>

                    <?= $form->field($signupForm, 'repeatPassword', [
                        'labelOptions' => [
                            'class' => '',
                        ],
                    ])->passwordInput([
                        'id' => 'account-login-page__new-member-pass-check',
                    ])->label($signupForm->getAttributeLabel('repeatPassword').'*'); ?>

                    <?= $form->field($signupForm, 'rules', [
                        'options' => [
                            'class' => 'form-check',
                        ],
                        'checkboxTemplate' => '<div class="grey-form__forgot-pass custom-control custom-checkbox">{input}{label}</div>{error}',
                    ])->checkbox(
                        ['class' => 'custom-control-input', 'data-url' => Url::toRoute(['/user/security/set-social-agree'])],
                        ['class' => 'custom-control-label',]
                    )->label(
                        Yii::t('user', 'I confirm that I have read and agree to the <a href="{0}">terms and conditions</a> and <a href="{1}">privacy policy</a> of Infinity Lashes', [
                            Url::toRoute(['/pages/index/page', 'uri' => 'terms']),
                            Url::toRoute(['/pages/index/page', 'uri' => 'cookie'])
                        ])
                    ); ?>

                    <button class="btn btn-primary" type="submit"><?= Yii::t('user', 'Sign in') ?></button>
                    <div class="with-lines-wrapper">
                        <div class="grey-form__line"></div>
                        <p class="grey-form__or-with-lines"><?= Yii::t('user', 'or') ?></p>
                        <div class="grey-form__line"></div>
                    </div>
                    <a class="btn btn-primary facebook-btn" href="<?= Url::to(['/user/security/auth', 'authclient' => 'facebook', 'action' => $signupForm::FORM_SOCIAL_ACTION]); ?>">
                        <svg class="fb_svg" enable-background="new 0 0 512 512" id="Layer_1" version="1.1" viewBox="0 0 512 512" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g><path d="M308.3,508.5c-2.5,0.1-4.1,0.3-5.7,0.3c-34.2,0-68.3-0.1-102.5,0.1c-4.8,0-6.1-1.3-6.1-6.1c0.1-79.6,0.1-159.3,0.1-238.9   c0-2.1,0-4.2,0-6.9c-18.6,0-36.7,0-55.1,0c0-28.4,0-56.3,0-85c1.9,0,3.7,0,5.4,0c15,0,30-0.1,45,0.1c3.8,0,4.8-1.1,4.8-4.8   c-0.2-22.3-0.2-44.7,0-67c0.1-15.6,2.6-30.8,9.8-44.9c10.3-19.9,26.6-32.8,47.2-40.8c16.8-6.6,34.5-9,52.3-9.3   c29-0.4,58-0.2,87-0.3c2.7,0,4.9-0.1,4.9,3.7c-0.1,27.5-0.1,55-0.1,82.5c0,0.3-0.1,0.6-0.5,1.9c-1.7,0-3.6,0-5.5,0   c-18,0-36-0.1-54,0c-10.4,0-18.8,4.2-24.1,13.3c-1.6,2.7-2.6,6.2-2.6,9.4c-0.3,17,0,34-0.2,51c0,4,1.2,5.1,5.1,5.1   c25-0.2,50-0.1,75-0.1c2,0,3.9,0,7.3,0c-3.5,28.6-6.9,56.6-10.4,84.9c-26,0-51.3,0-77.1,0C308.3,340.8,308.3,424.4,308.3,508.5z"/></g></svg>
                        <span>
                            <?= Yii::t('user', 'sign in with facebook') ?>
                        </span>
                    </a>
                    <a class="btn btn-primary google-btn" href="<?= Url::to(['/user/security/auth', 'authclient' => 'google', 'action' => $signupForm::FORM_SOCIAL_ACTION]); ?>">
                        <svg class="g_pl" id="Layer_2" version="1.1" viewBox="0 0 56.6934 56.6934" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g><path d="M19.6671,25.7867c-0.0075,1.7935,0,3.5869,0.0076,5.3803c3.0067,0.098,6.0208,0.0527,9.0275,0.098   c-1.3262,6.6689-10.3989,8.8315-15.199,4.4761C8.5674,31.9206,8.801,23.5412,13.9327,19.992   c3.5869-2.8635,8.6884-2.1552,12.2752,0.324c1.4092-1.3036,2.7278-2.6977,4.0013-4.1445   c-2.984-2.3812-6.6462-4.0767-10.5421-3.8958c-8.1307-0.2713-15.6059,6.8497-15.7415,14.9805   c-0.52,6.6462,3.8506,13.1644,10.0222,15.5155c6.1489,2.3661,14.031,0.7535,17.957-4.77c2.5922-3.4889,3.1498-7.98,2.8484-12.1999   C29.7194,25.7641,24.6933,25.7716,19.6671,25.7867z"/><path d="M49.0704,25.7641c-0.0151-1.4996-0.0226-3.0067-0.0301-4.5062c-1.4996,0-2.9916,0-4.4836,0   c-0.0151,1.4996-0.0301,2.9991-0.0377,4.5062c-1.5071,0.0075-3.0067,0.0151-4.5062,0.0302c0,1.4995,0,2.9915,0,4.4836   c1.4995,0.0151,3.0066,0.0302,4.5062,0.0452c0.0151,1.4996,0.0151,2.9991,0.0302,4.4987c1.4996,0,2.9916,0,4.4911,0   c0.0075-1.4996,0.015-2.9991,0.0301-4.5062c1.5071-0.0151,3.0067-0.0226,4.5062-0.0377c0-1.4921,0-2.9916,0-4.4836   C52.0771,25.7792,50.57,25.7792,49.0704,25.7641z"/></g></svg>
                        <span>
                            <?= Yii::t('user', 'sign in with google +') ?>
                        </span>
                    </a>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</section>
