<?php

/** @var $this yii\web\View */
/** @var $form frontend\widgets\bootstrap4\ActiveForm */
/** @var $model \frontend\modules\user\models\forms\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\widgets\bootstrap4\ActiveForm;

$this->title = 'Request password reset';
$this->params['breadcrumbs'][] = $this->title;
?>

<h5><?= Yii::t('user', 'Request to Reset Your Password'); ?></h5>
<p><?= Yii::t('user', 'Provide your account email address to receive an email to reset your password.'); ?></p>

<?php $form = ActiveForm::begin([
    'options' => [
        'class' => 'grey-form',
    ],
    'id' => 'reset-password-form',
    'action' => Url::toRoute(['/user/security/request-password-reset']),
]); ?>

    <?= $form->field($model, 'email', [
        'labelOptions' => [
            'class' => '',
        ],
        'inputOptions' => [
            'id' => 'account-login-page__request-email-input',
            'placeholder' => 'E-mail',
            'aria-describedby' => 'emailHelp',
        ],
    ])->label($model->getAttributeLabel('email') . '*'); ?>

    <button class="btn btn-primary" type="submit"><?= Yii::t('core', 'Request'); ?></button>

<?php ActiveForm::end(); ?>