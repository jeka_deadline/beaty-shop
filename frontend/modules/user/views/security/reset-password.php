<?php

use yii\helpers\Html;
use frontend\widgets\bootstrap4\ActiveForm;
use frontend\modules\user\assets\UserAsset;
use yii\helpers\Url;

$this->title = 'Reset password';
$this->bodyClass = 'change-pass-page';

/** @var \frontend\modules\core\components\View $this */
/** @var \frontend\widgets\bootstrap4\ActiveForm $form */
/** @var \frontend\modules\user\models\fomrs\ResetPasswordForm $model */

UserAsset::register($this);
?>

<h1><?= Yii::t('user', 'Change password'); ?></h1>
<div class="container">
    <div class="row">

        <?php if ($model->isTokenExpired()) : ?>

             <article class="col-xl-8 offset-xl-2 col-lg-10 offset-lg-1" style="text-align: center">
                  <p><?= Yii::t('user', 'The token\'s lifetime has been exhausted. To re-create it, click <a class="regenerate-token" data-url="{0}" data-method="{1}" href="javascript:void(0);"><b>this</b></a>', [
                      Url::toRoute(['/user/security/generate-new-reset-password-token', 'token' => $model->getTokenValue()]),
                      'post',
                  ]); ?></p>
                  <div id="preloader" style="display: none;">
                      <img src="/img/preloader.gif" alt="">
                  </div>
              </article>

        <?php else : ?>

            <?= $this->render('reset-password-form', compact('model')); ?>

        <?php endif; ?>

    </div>
</div>