<h5><?= Yii::t('user', 'Request to Reset Your Password Received'); ?></h5>
<p><?= Yii::t('user', 'Thanks for submitting your email address. We\'ve sent you an email with the information needed to reset your password.'); ?></p>
<p><?= Yii::t('user', 'The email might take a couple of minutes to reach your account. Please check your junk mail to ensure you receive it.'); ?></p>
