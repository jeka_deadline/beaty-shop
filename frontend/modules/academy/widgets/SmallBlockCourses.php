<?php

namespace frontend\modules\academy\widgets;

use frontend\modules\core\models\Setting;
use Yii;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

class SmallBlockCourses extends Widget
{
    public $template = 'small-block-courses';

    public $items;


    public function run()
    {
        if (empty($this->items) || !is_array($this->items)) {
            return false;
        }

        return $this->render($this->template, [
            'items' => $this->items
        ]);
    }

    private function renderItems($wrapperTag, $additionalWrapperTag, $itemTag)
    {
        $content = '';

        foreach ($this->items as $item) {
            $content .= $this->renderItem($item, $itemTag);
        }

        if ($wrapperTag !== false) {
            return Html::tag(
                $wrapperTag,
                ($additionalWrapperTag?Html::tag($additionalWrapperTag, $content, $this->additionalWrapperOptions):$content),
                $this->wrapperOptions
            );
        }

        return $content;
    }

    private function renderItem($item, $itemTag)
    {
        $replacedBlocks = [
            '{image}' => $item->urlSmallImage,
            '{name}' => $item->name,
            '{date}' => $item->date?date("d F",strtotime($item->date)):'',
            '{description}' => $item->description,
            '{price}' => $item->price,
            '{link}' => Url::to(['/course/'.$item->slug]),
            '{currency}' => Setting::value('product.shop.currency'),
        ];

        $block = strtr($this->itemTemplate, $replacedBlocks);

        if ($itemTag !== false) {
            return Html::tag($itemTag, $block, $this->itemOptions);
        }

        return $block;
    }

    private function renderHeader($headerTitle, $headerTag)
    {
        if ($headerTag !== false) {
            return Html::tag($headerTag, $headerTitle, $this->headerOptions);
        }

        return $headerTitle;
    }

    private function renderShopNowLink()
    {
        if (isset($this->shopNowLink[ 'template' ])) {
            $link = ArrayHelper::remove($this->shopNowLink, 'link', '#');
            $text = ArrayHelper::remove($this->shopNowLink, 'text', Yii::t('core', 'Shop now'));

            return strtr($this->shopNowLink[ 'template' ], [
                '{link}' => $link,
                '{text}' => $text,
            ]);
        }

        return '';
    }
}