<?php

use frontend\modules\core\models\Setting;
use yii\helpers\Url;

?>

<div class="course-page-courses container">
    <h3 class="row justify-content-center align-items-center">Other Courses</h3>
    <div class="items-without-slider row items">
        <?php foreach ($items as $item): ?>
            <div class="card-row-margin col-xl-4 col-lg-4 col-md-6 col-sm-12">
                <a class="item" href="<?= Url::to(['/course/'.$item->slug]) ?>" title="<?= Yii::t('academy', 'learn more') ?>">
                    <div class="card-img-top card-img-top"><img src="<?= $item->urlSmallImage ?>" alt="item"/></div>
                    <div class="card-body">
                        <div class="when-price">
                            <svg class="date-card svg">
                                <use xlink:href="#date"></use>
                            </svg>
                            <p><?= $item->date?date("<\span>d</\span> <\span>F</\span>",strtotime($item->date)):'' ?></p><span><?= $item->price ?> <?= Setting::value('product.shop.currency') ?></span>
                        </div>
                        <div class="card-body__main justify-content-center">
                            <div class="adaptive-wrapper">
                                <h5 class="card-title"><?= $item->name ?></h5>
                                <div class="card-text"><?= $item->description ?></div>
                            </div>
                            <a class="btn btn-primary" href="<?= Url::to(['/course/'.$item->slug]) ?>" title="<?= Yii::t('academy', 'learn more') ?>">
                                <?= Yii::t('academy', 'learn more') ?>
                            </a>
                        </div>
                    </div>
                </a>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="items-in-slider">
        <div class="items-in-slider owl-carousel-main-cards owl-carousel">
            <?php foreach ($items as $item): ?>
            <div class="item">
                <a href="<?= Url::to(['/course/'.$item->slug]) ?>" title="<?= Yii::t('academy', 'learn more') ?>">
                    <div class="card-img-top card-img-top"><img src="<?= $item->urlSmallImage ?>" alt="item"></div>
                    <div class="card-body">
                        <div class="when-price">
                            <svg class="date-card svg">
                                <use xlink:href="#date"></use>
                            </svg><span>April</span><span><?= $item->price ?> $</span>
                        </div>
                        <div class="card-body__main justify-content-center">
                            <div class="adaptive-wrapper">
                                <h5 class="card-title"><?= $item->name ?></h5>
                                <div class="card-text"><?= $item->description ?></div>
                            </div>
                            <a class="btn btn-primary" href="<?= Url::to(['/course/'.$item->slug]) ?>" title="<?= Yii::t('academy', 'learn more') ?>">
                                <?= Yii::t('academy', 'learn more') ?>
                            </a>
                        </div>
                    </div>
                </a>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>