<?php

namespace frontend\modules\academy\models;

use frontend\modules\core\behaviors\LangBehavior;
use frontend\queries\ActiveQuery;
use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

class Academy extends \common\models\academy\Academy
{
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                [
                    'class' => LangBehavior::className(),
                    'langModel' => AcademysLangField::className(),
                    'modelForeignKey' => 'academy_id',
                    'attributes' => [
                        'name',
                        'description',
                        'note',
                    ],
                ]
            ]
        );
    }

    public static function find()
    {
        return new ActiveQuery(get_called_class());
    }

    public static function getRecommended($limit = 0)
    {
        $query = static::find();
        if (($slug = Yii::$app->request->get('slug'))) {
            $query->where(['<>','slug', $slug]);
        }
        $query->andWhere(['>=', 'date', date('Y-m-d H:i:s')]);
        return $query->active()
            ->orderBy(new Expression('rand()'))
            ->limit($limit)
            ->all();
    }

    public function getInfoBlocks()
    {
        return $this->hasMany(InfoBlock::className(), ['academy_id' => 'id'])->orderBy('display_order');
    }

    public function getPrices()
    {
        return $this->hasMany(Price::className(), ['academy_id' => 'id'])->active();
    }
}
