<?php

namespace frontend\modules\academy\models\forms;

use frontend\modules\academy\models\Academy;
use frontend\modules\academy\models\Register;
use frontend\modules\core\validators\PhoneValidator;
use Yii;

class RegisterForm extends Register
{

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['academy_id', 'sex', 'first_name', 'last_name', 'phone', 'email', 'seats'], 'required'],
            ['email', 'email'],
            ['phone', PhoneValidator::className()],
            [['academy_id', 'price_id'], 'integer'],
            [['sex', 'phone'], 'string', 'max' => 20],
            [['company', 'first_name', 'last_name'], 'string', 'max' => 50],
            [['email'], 'string', 'max' => 100],
            [['academy_id'], 'exist', 'skipOnError' => true, 'targetClass' => Academy::className(), 'targetAttribute' => ['academy_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'sex' => Yii::t('product', 'Title').'*',
            'first_name' => Yii::t('core', 'First Name').'*',
            'last_name' => Yii::t('core', 'Last Name').'*',
            'phone' => Yii::t('product', 'Phone').'*',
            'email' => 'E-mail*',
            'company' => Yii::t('product', 'Company'),
            'seats' => 'Number of seats*',
        ];
    }

    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);
        if ($insert) {
            $this->sendMailAdmin();
        }
    }

    private function sendMailAdmin()
    {
        $mailer = Yii::$app->mailer;

        $message = $mailer->compose()
            ->setFrom([$mailer->transport->getUsername() => 'info'])
            ->setTo(Yii::$app->params[ 'adminEmail' ])
            ->setSubject('New Registration for the Academy course')
            ->setTextBody("Registered\n\n"
                .Yii::t('product', 'Title').": {$this->sex}\n"
                .Yii::t('core', 'First Name').": {$this->first_name}\n"
                .Yii::t('core', 'Last Name').": {$this->last_name}\n"
                .Yii::t('product', 'Company').": {$this->company}\n"
                .Yii::t('product', 'Phone').": {$this->phone}\n"
                ."E-mail: {$this->email}\n"
                ."Number of seats: {$this->seats}\n"
            );

        $message->send();
    }

    public function getGermanPhoneMask() {
        return PhoneValidator::getGermanPhoneMask();
    }
}
