<?php

namespace frontend\modules\academy\models\forms;

use Yii;
use yii\base\Model;

class CourseSortForm extends Model
{
    /**
     * Types of sorts.
     */
    const TYPE_SORT_NONE = 'group_none';
    const TYPE_SORT_WEEK = 'group_week';
    const TYPE_SORT_MONTH = 'group_month';
    const TYPE_SORT_YEAR = 'group_year';

    public $sort;

    /**
     * Sort by default.
     * @var string
     */
    private $typeSortDefault = self::TYPE_SORT_YEAR;

    public function init()
    {
        parent::init();
        $this->sort = $this->typeSortDefault;
    }

    public function rules()
    {
        return [
            [['sort'], 'string', 'max' => 20],
        ];
    }

    public function formName()
    {
        return '';
    }

    public static function getSortListLabel() {
        return [
            self::TYPE_SORT_WEEK => Yii::t('academy', 'this week'),
            self::TYPE_SORT_MONTH => Yii::t('academy', 'this month'),
            self::TYPE_SORT_YEAR => Yii::t('academy', 'this year'),
        ];
    }

    public static function sort($query) {

        $formSort = new CourseSortForm();
        $formSort->load(Yii::$app->request->get());

        if (!$formSort->validate()) {
            return $query;
        }

        $query = self::setSortQuery($query, $formSort);

        return $query;
    }

    private static function setSortQuery($query, $formFilter) {
        switch ($formFilter->sort) {
            case self::TYPE_SORT_WEEK:
                $values=self::getBetweenThisWeek();
                $query->andWhere(['between', 'date', $values[0], $values[1]]);
                break;
            case self::TYPE_SORT_MONTH:
                $values=self::getBetweenThisMonth();
                $query->andWhere(['between', 'date', $values[0], $values[1]]);
                break;
            case self::TYPE_SORT_YEAR:
                $values=self::getBetweenThisYear();
                $query->andWhere(['between', 'date', $values[0], $values[1]]);
                break;
        }
        return $query;
    }

    private static function getBetweenThisWeek() {
        $day = time();
        $end_week = strtotime("next monday",$day);
        return [date('Y-m-d H:i:s', $day), date('Y-m-d 00:00:00', $end_week)];
    }

    private static function getBetweenThisMonth() {
        $day = time();
        $end_week = strtotime("first day of next month",$day);
        return [date('Y-m-d H:i:s', $day), date('Y-m-d 00:00:00', $end_week)];
    }

    private static function getBetweenThisYear() {
        $day = time();
        $end_week = strtotime("1st January Next Year",$day);
        return [date('Y-m-d H:i:s', $day), date('Y-m-d 00:00:00', $end_week)];
    }
}
