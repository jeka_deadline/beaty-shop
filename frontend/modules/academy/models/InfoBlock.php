<?php

namespace frontend\modules\academy\models;

use frontend\modules\core\behaviors\LangBehavior;
use yii\helpers\ArrayHelper;

class InfoBlock extends \common\models\academy\InfoBlock
{
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                [
                    'class' => LangBehavior::className(),
                    'langModel' => InfoBlockLangField::className(),
                    'modelForeignKey' => 'info_block_id',
                    'attributes' => [
                        'name',
                        'data',
                    ],
                ]
            ]
        );
    }
}
