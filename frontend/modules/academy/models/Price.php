<?php

namespace frontend\modules\academy\models;

use frontend\modules\core\behaviors\LangBehavior;
use frontend\queries\ActiveQuery;
use yii\helpers\ArrayHelper;

class Price extends \common\models\academy\Price
{
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                [
                    'class' => LangBehavior::className(),
                    'langModel' => PriceLangField::className(),
                    'modelForeignKey' => 'academy_price_id',
                    'attributes' => [
                        'name',
                        'description',
                    ],
                ]
            ]
        );
    }

    public static function find()
    {
        return new ActiveQuery(get_called_class());
    }
}
