<?php

namespace frontend\modules\academy\assets;

use yii\web\AssetBundle;

/**
 * Asset for cart shop.
 */
class CourseAsset extends AssetBundle
{
    /**
     * {@inheritdoc}
     *
     * @var array $js
     */
    public $js = [
        'js/jquery.waypoints.min.js',
    ];

    /**
     * {@inheritdoc}
     *
     * @var array $depends
     */
    public $depends = [
        'yii\web\JqueryAsset',
    ];

    /**
     * {@inheritdoc}
     *
     * @var array $publishOptions
     */
    public $publishOptions = [
        'forceCopy' => (YII_DEBUG) ? true : false,
    ];

    /**
     * {@inheritdoc}. Set source path for this asset.
     *
     * @return void
     */
    public function init()
    {
        parent::init();

        $this->sourcePath = __DIR__ . DIRECTORY_SEPARATOR . 'src';
    }
}
