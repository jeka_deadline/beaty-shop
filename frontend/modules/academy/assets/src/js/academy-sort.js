$(function() {
    /**
     * Specify sorting
     */
    $(document).on('click', '#dropdownMonth .option', function (e) {
        var value = $(e.target).attr('data-value');
        if (value == null) return;
        $('input[name="sort"]').val(value);
        var form = $(this).parents('form');
        window.location.href = form.attr('action') + '?' + form.serialize();
        e.preventDefault();
    });
});