$(function() {
    function sendAjax(form, modal_id) {
        var formData = form.serializeArray();
        $.ajax({
            url: form.attr("action"),
            type: form.attr("method"),
            data: formData,
            success: function (response) {
                $('#thanksForRegistrationForTheCourse .modal-body p').text(response.message);
                $(modal_id).modal('hide');
                $('#thanksForRegistrationForTheCourse').modal('show');
                $(form).find('input:text, input:password, input:file, select, textarea').val('');
                $(form).find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
            },
            error: function () {
            }
        });
    }

    $(document).on("beforeSubmit", "#form-register", function (e) {
        var form = $(this);
        sendAjax(form, '#registrationForTheCourse');
    }).on('submit', "#form-register",  function(e){
        e.preventDefault();
    });

    $('[data-target="#registrationForTheCourse"]').click(function () {
        var price_id = $(this).attr('data-course-id');
        $('#registrationForTheCourse #registerform-price_id').val(price_id);
        $('#registrationForTheCourse .data-price-info').html($('[data-form-info="'+price_id+'"]').html());
    });
});