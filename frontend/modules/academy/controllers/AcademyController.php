<?php

namespace frontend\modules\academy\controllers;

use frontend\modules\academy\models\Academy;
use frontend\modules\academy\models\forms\CourseSortForm;
use frontend\modules\academy\models\forms\RegisterForm;
use frontend\modules\core\models\Setting;
use frontend\modules\core\models\TextBlock;
use Yii;
use yii\web\NotFoundHttpException;
use frontend\modules\core\controllers\FrontController;
use yii\web\Response;
use frontend\modules\core\models\CorePage;

/**
 * Frontend controller for Product.
 */
class AcademyController extends FrontController
{
    /**
     * Display page academy.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        if ($this->isDisabled()) {
            $blockText = TextBlock::find()->where(['code' => 'text_academy_disable'])->active()->one();

            if ($page = CorePage::findAcademyPage()) {
                $this->setMetaTitle($page->meta_title);
                $this->setMetaDescription($page->meta_description);
                $this->setMetaKeywords($page->meta_keywords);
            }

            return $this->render('index-disabled', compact('blockText'));
        }

        $formSort = new CourseSortForm();
        $formSort->load(Yii::$app->request->get());

        $blocks = [];

        $modelAcademys = CourseSortForm::sort(Academy::find()
            ->where(['>=', 'date', date('Y-m-d H:i:s')])
            ->active()
            ->orderBy(['date' => SORT_ASC]))
            ->all();

        foreach ($modelAcademys as $modelAcademy) {
            $blocks[$modelAcademy->date?date("F-Y", strtotime($modelAcademy->date)):'none'][] = $modelAcademy;
        }

        if ($page = CorePage::findAcademyPage()) {
            $this->setMetaTitle($page->meta_title);
            $this->setMetaDescription($page->meta_description);
            $this->setMetaKeywords($page->meta_keywords);
        }

        return $this->render('index', compact('blocks', 'formSort'));
    }

    public function actionCourse($slug)
    {
        if ($this->isDisabled()) {
            throw new NotFoundHttpException('Not Course.');
        }

        $modelCourse = Academy::find()
            ->with('infoBlocks', 'prices')
            ->where(['slug' => $slug])
            ->active()
            ->one();

        if (!$modelCourse) {
            throw new NotFoundHttpException('Not Course.');
        }

        return $this->render('course', [
            'modelCourse' => $modelCourse,
            'formRegister' => new RegisterForm()
        ]);
    }

    public function actionSendRegister()
    {
        if ($this->isDisabled()) {
            throw new NotFoundHttpException('Not Course.');
        }

        if (!Yii::$app->request->isAjax) throw new NotFoundHttpException("Not found");

        Yii::$app->response->format = Response::FORMAT_JSON;

        $result = [
            'status' => 'error',
            'message' => 'Error sending.'
        ];

        $formRegister = new RegisterForm();

        if ($formRegister->load(Yii::$app->request->post()) && $formRegister->save()) {
            $result = [
                'status' => 'ok',
                'message' => Yii::t('core', 'Thank you so much! Your letter has been successfully sent, we will contact you as soon as possible.')
            ];
            return $result;
        }
        $result['message'] = 'Error sending.';
        return $result;
    }

    protected function findAcademySlug($slug)
    {
        if (($model = Academy::findOne(['slug' => $slug])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Not Course.');
    }

    private function isDisabled() {
        return Setting::value('academy.enable')==0;
    }
}
