<?php

/** @var \frontend\modules\core\components\View $this */
/** @var \frontend\modules\academy\models\forms\CourseSortForm $formSort */
/** @var array $blocks */
/** @var \yii\widgets\ActiveForm $form */

use frontend\modules\academy\assets\SortAsset;
use frontend\modules\academy\models\forms\CourseSortForm;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

SortAsset::register($this);

$this->bodyClass = 'academy-page';

?>
<div class="img-and-text__head container-fluid">
    <div class="img-and-text__main-img row">
        <h1 class="img-and-text__main-text"><?= Yii::t('academy', 'Infinity Lashes Academy'); ?></h1>
        <div class="img-and-text__text"><?= Yii::t('academy', 'Courses, Events, Education'); ?></div>
    </div>
</div>
<main class="container">
    <div class="academy-page__main-head">
        <div class="academy-page__thismonth"><?= CourseSortForm::getSortListLabel()[$formSort->sort]; ?></div>
        <div class="academy-page__sort">

            <?php $form = ActiveForm::begin([
                'action' => Url::to('/academy'),
                'method' => 'get',
            ]); ?>

            <?= $form->field($formSort, 'sort')->hiddenInput()->label(false); ?>

            <div class="dropdown" id="dropdownMonth">
                <span class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                    <?= Yii::t('academy', 'Sort by'); ?>

                </span>
                <p class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">+</p>
                <button class="btn btn-secondary btn-lg dropdown-toggle" id="dropdownMenuButton" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                    <?= CourseSortForm::getSortListLabel()[$formSort->sort]; ?>

                    <svg class="dropme svg">
                        <use xlink:href="#totop"></use>
                    </svg>
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">

                    <?php foreach (CourseSortForm::getSortListLabel() as $code => $value): ?>

                        <a class="dropdown-item option" href="#" data-value="<?= $code ?>">
                            <?= $value ?>
                        </a>

                    <?php endforeach; ?>

                </div>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

    <div class="academy-page__courses-items container">

        <?php foreach ($blocks as $key => $block): ?>

            <div class="academy-page__month-wrapper">
                <div class="academy-page__month-header">
                    <h6><?= date('F', strtotime($key)) ?></h6>
                    <hr>
                </div>
                <div class="row items">

                    <?php foreach ($block as $academy): ?>

                        <?= $this->render('_academy_item',[
                            'model' => $academy
                        ]) ?>

                    <?php endforeach; ?>

                </div>
            </div>

        <?php endforeach; ?>

    </div>
</main>
