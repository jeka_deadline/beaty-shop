<h2><?= $model->name ?></h2>
<?php if ($model->decodeSchedule): ?>
    <?php foreach ($model->decodeSchedule as $list): ?>
        <h4 class="offset-xl-1 col-xl-7"><?= $list->title ?>:</h4>
        <?php if (!empty($list->items)): ?>
            <ul class="offset-xl-1 col-xl-7 offset-lg-1 col-lg-7 offset-md-1 col-md-11">
                <?php foreach ($list->items as $item): ?>
                    <li><span><?= $item->title ?></span></li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>
    <?php endforeach; ?>
<?php endif; ?>