<?php

use frontend\modules\core\models\Setting;

?>
<?php if ($modelCourse->prices): ?>
    <h2>Prices</h2>
    <div class="container">
        <div class="row">
            <?php foreach ($modelCourse->prices as $price): ?>
                <div class="col">
                    <div class="course-page__ticket-one-part">
                        <p class="course-page__ticket-flow-with-row"><?= $price->name ?><?php if ($price->best_offer):?><span class="badge badge-secondary">Best Offer</span><?php endif; ?></p>
                        <p class="course-page__ticket-flow-with-row"><?= $price->description ?></p>
                    </div>
                    <div class="course-page__ticket-two-part">
                        <p class="course-page__ticket-flow-price"><?= $price->price ?> <?= Setting::value('product.shop.currency') ?></p>
                        <a
                            class="btn btn-primary justify-content-center align-items-center"
                            type="button"
                            data-toggle="modal"
                            data-target="#registrationForTheCourse"
                            data-course-id="<?= $price->id ?>"
                        >
                            <?= Yii::t('academy', 'Register') ?>
                        </a>
                    </div>
                    <div class="d-none" data-form-info="<?= $price->id ?>">
                        <p><?= $price->name ?></p>
                        <p><?= $price->description ?></p>
                        <p><?= $price->price ?> $</p>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>