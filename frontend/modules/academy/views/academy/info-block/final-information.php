<?php

use frontend\modules\core\models\Setting;

?>
<h2><?= $model->name ?></h2>
<div class="course-page__ticket">
    <div class="row course-page__ticket-down" id="course-page__ticket-down">
        <div class="course-page__ticket-img col-xl-3 col-lg-4"<?php if ($model->image): ?> style="background-image: url(<?= $model->urlImage ?>);"<?php endif; ?>></div>
        <div class="course-page__ticket-txt col-xl-6 col-lg-5">
            <div class="left-wrapper">
                <p><span><?= Yii::t('academy', 'Date') ?>:</span> <?= $model->date ?></p>
                <p><span><?= Yii::t('academy', 'Location') ?>:</span> <?= $model->location ?></p>
            </div>
            <div class="right-wrapper">
                <p><span><?= Yii::t('academy', 'Trainer') ?>:</span> <?= $model->trainer ?></p>
                <p><span><?= Yii::t('academy', 'Free Seats') ?>:</span> <?= $model->seats ?></p>
            </div>
        </div>
        <div class="course-page__ticket-flow-wrapper">
            <div class="course-page__ticket-flow">
                <div class="course-page__ticket-one-part">
                    <p class="course-page__ticket-flow-with-row"><?= Yii::t('academy', 'Duration') ?>: <?= $modelCourse->duration ?> days</p>
                    <p class="course-page__ticket-flow-with-row"><?= $modelCourse->note ?></p>
                </div>
                <div class="course-page__ticket-two-part">
                    <p class="course-page__ticket-flow-price"><?= $modelCourse->price ?> <?= Setting::value('product.shop.currency') ?></p>
                    <a class="btn btn-primary row justify-content-center align-items-center" type="button" data-toggle="modal" data-target="#registrationForTheCourse"><?= Yii::t('academy', 'Register') ?></a>
                </div>
            </div>
        </div>
    </div>
</div>