<?php

use common\models\academy\infoBlockModels\ImageSlider;

?>

<?php if ($model->images): ?>
<div class="course-page__article-slider">
    <div class="carousel slide" id="carouselCourse" data-ride="carousel">
        <div class="carousel-inner">
            <?php foreach ($model->images as $key => $image): ?>
                <div class="carousel-item<?= $key?'':' active'?>"><img class="d-block w-100" src="/<?= ImageSlider::$path.$image ?>" alt="<?= $image ?>"></div>
            <?php endforeach; ?>
            <a class="carousel-control-prev" href="#carouselCourse" role="button" data-slide="prev">
                <svg class="slider-arrow-left slider-arrows svg">
                    <use xlink:href="#arrow"></use>
                </svg>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselCourse" role="button" data-slide="next">
                <svg class="slider-arrow-right slider-arrows svg">
                    <use xlink:href="#arrow"></use>
                </svg><span class="sr-only">Next</span>
            </a>
            <ol class="carousel-indicators">
                <?php foreach ($model->images as $key => $image): ?>
                    <li<?= $key?'':' class="active"'?> data-target="#carouselCourse" data-slide-to="<?= $key ?>"></li>
                <?php endforeach; ?>
            </ol>
        </div>
    </div>
</div>
<?php endif; ?>