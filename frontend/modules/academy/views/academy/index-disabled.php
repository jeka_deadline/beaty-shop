<?php

use frontend\modules\academy\assets\SortAsset;
use frontend\modules\core\models\Setting;

$this->bodyClass = 'academy-page';

?>
<div class="img-and-text__head container-fluid">
    <div class="img-and-text__main-img row">
        <h1 class="img-and-text__main-text"><?= Yii::t('academy', 'Infinity Lashes Academy'); ?></h1>
        <div class="img-and-text__text"><?= Yii::t('academy', 'Courses, Events, Education'); ?></div>
    </div>
</div>
<main class="container">
    <div class="academy-page">
        <div class="academy-page__content">
            <?php if ($blockText):?>
                <h1 id="academy-page__header"><?= $blockText->name ?></h1>
                <div class="row">
                    <div class="academy-page__adaptive col-xl-8 offset-xl-2 col-lg-10 offset-lg-1 col-12">
                        <?= $blockText->content ?>
                    </div>
                </div>
                <div class="academy-page__button-wrapper container">
                    <a class="button" href="#academy-page__header"><?= Yii::t('academy', 'read completely') ?>
                        <svg class="button-svg svg">
                            <use xlink:href="#arrow-new"></use>
                        </svg>
                    </a>
                </div>
                <div class="row">
                    <div class="col-xl-8 offset-xl-2 col-lg-10 offset-lg-1 col-12">
                        <?= $blockText->description ?>
                        <div class="academy-page__tel">
                            <a href="tel:<?= Setting::value('academy.contact-phone') ?>">
                                <?= Setting::value('academy.contact-phone') ?>
                            </a>
                            <a href="mailto:<?= Setting::value('academy.contact-email') ?>">
                                <?= Setting::value('academy.contact-email') ?>
                            </a>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
    </div>
</main>
