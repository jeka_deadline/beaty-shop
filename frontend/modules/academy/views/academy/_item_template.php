<div class="item">
    <div class="card-img-top card-img-top"><img src="{image}" alt="item"/></div>
    <div class="card-body">
        <div class="when-price">
            <svg class="date-card svg">
                <use xlink:href="#date"></use>
            </svg><span>{date}</span><span>{price} {currency}</span>
        </div>
        <div class="card-body__main justify-content-center">
            <h5 class="card-title">{name}</h5>
            <p class="card-text">{description}</p>
            <a class="btn btn-primary" href="{link}"><?= Yii::t('academy', 'learn more') ?></a>
        </div>
    </div>
</div>