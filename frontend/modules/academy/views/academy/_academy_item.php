<?php

use frontend\modules\core\models\Setting;

?>
<div class="card-row-margin col-xl-4 col-lg-4 col-md-6 col-sm-12" title="<?= Yii::t('academy', 'learn more') ?>">
    <div class="item">
        <div class="card-img-top card-img-top"><img src="<?= $model->urlSmallImage ?>" alt="<?= $model->name ?>"/></div>
        <div class="card-body">
            <div class="when-price">
                <svg class="date-card svg">
                    <use xlink:href="#date"></use>
                </svg><span><?= $model->date?date("d F", strtotime($model->date)):'' ?></span><span><?= $model->price ?> <?= Setting::value('product.shop.currency') ?></span>
            </div>
            <div class="card-body__main justify-content-center">
                <div class="adaptive-wrapper">
                    <h5 class="card-title"><?= $model->name ?></h5>
                    <p class="card-text"><?= $model->description ?></p>
                </div>
                <a class="btn btn-primary" href="<?= \yii\helpers\Url::to('/course/'.$model->slug) ?>" title="<?= Yii::t('academy', 'learn more') ?>"><?= Yii::t('academy', 'learn more') ?></a>
            </div>
        </div>
    </div>
</div>