<?php

use frontend\modules\academy\assets\CourseAsset;
use frontend\modules\academy\models\Academy;
use frontend\modules\academy\widgets\SmallBlockCourses;

CourseAsset::register($this);

$this->title = $modelCourse->name;

$this->bodyClass = 'course-page';

?>

<div class="img-and-text__head container-fluid">
    <div class="img-and-text__main-img row"<?php if ($modelCourse->image): ?> style="background-image: url(<?= $modelCourse->urlImage ?>);"<?php endif; ?>>
        <h1 class="img-and-text__main-text"><?= $modelCourse->name ?></h1>
        <div class="img-and-text__text"><?= $modelCourse->description ?></div>
    </div>
</div>
<main class="container">
    <div class="article">
        <?php if ($modelCourse->infoBlocks) :?>
            <?php foreach ($modelCourse->infoBlocks as $block): ?>
                <?php if ($block->block) :?>
                    <?= $this->render('info-block/'.$block->block->template, [
                        'model' => $block->block,
                        'modelCourse' => $modelCourse
                    ]) ?>
                <?php endif; ?>
            <?php endforeach; ?>
        <?php endif; ?>

        <?= $this->render('info-block/prices', compact('modelCourse')); ?>

    </div>
    <hr>
    <?= $this->render('forms/_register_form', compact(
        'formRegister',
        'modelCourse'
    )); ?>
    <div class="svgHide"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100" height="100" style="position:absolute"><symbol id="arrow" viewBox="0 0 22.71 64"><defs><style>.cls-1-arrow{fill:none;stroke-miterlimit:10;stroke-width:2px}</style></defs><title>arrow</title><g id="Layer_2" data-name="Layer 2"><path class="cls-1-arrow" d="M.84.55l20.67 31.71L.84 63.45" id="arrow"></path></g></symbol></svg></div>
</main>

<?= SmallBlockCourses::widget([
    'items' => Academy::getRecommended(3),
]); ?>

