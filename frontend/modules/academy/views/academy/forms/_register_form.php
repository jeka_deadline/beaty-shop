<?php

use frontend\modules\academy\assets\RegisterAsset;
use frontend\modules\user\models\User;
use frontend\widgets\bootstrap4\ActiveForm;
use frontend\widgets\bootstrap4\Html;
use yii\helpers\Url;
use yii\widgets\MaskedInput;

RegisterAsset::register($this);

?>

<div class="course-page__registration-for-the-course-wrapper">
    <div class="modal fade" id="registrationForTheCourse" tabindex="-1" role="dialog" aria-labelledby="registrationForTheCourseTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="registrationForTheCourseLongTitle">Registration for the course</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="modal-body__form">
                        <?php $form = ActiveForm::begin([
                            'id' => 'form-register',
                            'action' => Url::toRoute('send-register'),
                            'enableClientValidation' => true,
                            'enableAjaxValidation' => false,
                            'method' => 'post',
                            'options' => [
                                'class' => 'grey-form'
                            ]
                        ]); ?>
                            <?= $form->field($formRegister, 'academy_id')->hiddenInput([
                                'value' => $modelCourse->id
                            ])->label(false) ?>
                            <?= $form->field($formRegister, 'price_id')->hiddenInput()->label(false) ?>

                            <?= $form->field($formRegister, 'sex')->dropDownList(User::getListUserTitles(), ['prompt'=>'']) ?>

                            <?= $form->field($formRegister, 'first_name')->textInput(['maxlength' => true]) ?>

                            <?= $form->field($formRegister, 'last_name')->textInput(['maxlength' => true]) ?>

                            <?= $form->field($formRegister, 'company')->textInput(['maxlength' => true]) ?>

                            <?= $form->field($formRegister, 'phone')->widget(MaskedInput::className(), [
                                'mask' => $formRegister->getGermanPhoneMask(),
                                'options' => [
                                    'class' => 'form-control',
                                    'required' => true,
                                ],
                            ]) ?>

                            <?= $form->field($formRegister, 'email')->textInput(['maxlength' => true]) ?>

                            <?= $form->field($formRegister, 'seats')->dropDownList([
                                1 => 1,
                                2 => 2,
                                3 => 3,
                                4 => 4,
                                5 => 5,
                                6 => 6,
                                7 => 7,
                                8 => 8,
                                9 => 9,
                                10 => 10,
                            ]) ?>

                            <?= Html::submitButton('submit', ['class' => 'btn btn-primary']) ?>

                        <?php ActiveForm::end(); ?>
                    </div>
                    <div class="modal-body__img">
                        <div class="card-img"><img src="<?= $modelCourse->image?$modelCourse->urlSmallImage:''?>" alt="course-img"></div>
                        <div class="card-text"><?= $modelCourse->name ?></div>
                        <div class="data-price-info"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="thanksForRegistrationForTheCourse" tabindex="-1" role="dialog" aria-labelledby="thanksForRegistrationForTheCourseTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="thanksForRegistrationForTheCourseLongTitle">We are glad that you are with us!</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <p>Thank you so much! You have successfully registered, our manager will contact you in the near future</p>
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>