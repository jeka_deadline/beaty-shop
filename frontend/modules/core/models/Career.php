<?php

namespace frontend\modules\core\models;

use frontend\modules\core\behaviors\LangBehavior;
use frontend\queries\ActiveQuery;
use yii\helpers\ArrayHelper;

class Career extends \common\models\core\Career
{
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                [
                    'class' => LangBehavior::className(),
                    'langModel' => CareerLangField::className(),
                    'modelForeignKey' => 'career_id',
                    'attributes' => [
                        'name',
                        'description',
                    ],
                ]
            ]
        );
    }

    public static function find()
    {
        return new ActiveQuery(get_called_class());
    }
}
