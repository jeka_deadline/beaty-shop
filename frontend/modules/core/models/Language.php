<?php

namespace frontend\modules\core\models;

use frontend\queries\ActiveQuery;
use Yii;

class Language extends \common\models\core\Language
{

    public static function find()
    {
        return new ActiveQuery(get_called_class());
    }

    public static function setDateLocalMonth($date) {
        $listMonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November','December'];

        $listLocalMonths = [
            Yii::t('core', 'January'),
            Yii::t('core', 'February'),
            Yii::t('core', 'March'),
            Yii::t('core', 'April'),
            Yii::t('core', 'May'),
            Yii::t('core', 'June'),
            Yii::t('core', 'July'),
            Yii::t('core', 'August'),
            Yii::t('core', 'September'),
            Yii::t('core', 'October'),
            Yii::t('core', 'November'),
            Yii::t('core', 'December')
        ];

        return str_replace($listMonths, $listLocalMonths, $date);
    }
}
