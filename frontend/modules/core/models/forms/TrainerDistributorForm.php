<?php

namespace frontend\modules\core\models\forms;

use frontend\modules\core\models\TrainerDistributor;
use frontend\modules\core\validators\PhoneValidator;
use Yii;
use yii\web\UploadedFile;

class TrainerDistributorForm extends TrainerDistributor
{
    public $files;

    public function upload()
    {
        if ($this->validate()) {
            $this->files = UploadedFile::getInstances($this, 'files');
            $allPath = $this->getPath();
            if (!is_dir($allPath)) {
                mkdir($allPath, 0777, true);
            }
            foreach ($this->files as $key => $file) {
                $filename = $file->baseName.'.'.$file->extension;
                if ($file->saveAs($allPath . $filename)) {
                    chmod($allPath . $filename, 0777);
                }
            }
            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);
        if ($insert) {
            $this->upload();
            $this->sendMailAdmin();
        }
    }

    private function sendMailAdmin() {}

    public function getGermanPhoneMask() {
        return PhoneValidator::getGermanPhoneMask();
    }
}