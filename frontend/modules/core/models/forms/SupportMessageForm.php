<?php

namespace frontend\modules\core\models\forms;

use Yii;
use yii\base\Model;
use frontend\modules\user\models\User;
use frontend\modules\core\models\SupportMessage;
use frontend\modules\core\models\EmailTemplate;
use yii\web\UploadedFile;

/**
 * Support message form.
 */
class SupportMessageForm extends Model
{
    /**
     * User title.
     *
     * @var string $title
     */
    public $title;

    /**
     * User name.
     *
     * @var string $name
     */
    public $name;

    /**
     * User surname.
     *
     * @var string $surname
     */
    public $surname;

    /**
     * User company.
     *
     * @var string $company
     */
    public $company;

    /**
     * User email.
     *
     * @var string $email
     */
    public $email;

    /**
     * User support message subject.
     *
     * @var string $subject
     */
    public $subject;

    /**
     * User support message text.
     *
     * @var string $text
     */
    public $text;

    /**
     * User support message files.
     *
     * @var string $files
     */
    public $files;

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['title', 'name', 'surname', 'email', 'subject', 'text'], 'required'],
            [['title', 'name', 'surname', 'email', 'subject'], 'trim'],
            [['name', 'surname'], 'string', 'max' => 50],
            ['email', 'email'],
            ['email', 'string', 'max' => 100],
            ['subject', 'safe'],
            ['text', 'string'],
            ['title', 'in', 'range' => array_keys($this->getListUserTitles())],
            [['files'], 'file', 'skipOnEmpty' => true, 'skipOnError' => false, 'extensions' => 'pdf, doc, docx, txt, xls, png, jpg, jpeg, bmp', 'maxFiles' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'title' => Yii::t('product', 'Title'),
            'name' => Yii::t('core', 'First Name'),
            'surname' => Yii::t('core', 'Last Name'),
            'company' => Yii::t('product', 'Company'),
            'email' => Yii::t('core', 'E-mail Address'),
            'subject' => 'Subject',
            'text' => 'Text',
        ];
    }

    /**
     * Save user support message.
     *
     * @return void
     */
    public function saveSupportMessage()
    {
        if (!$this->validate()) {
            return false;
        }

        $supportMessage = new SupportMessage();

        $supportMessage->subject = $this->subject;
        $supportMessage->name    = $this->name;
        $supportMessage->surname = $this->surname;
        $supportMessage->title   = $this->title;
        $supportMessage->email   = $this->email;
        $supportMessage->company = $this->company;
        $supportMessage->text    = $this->text;

        if ($supportMessage->validate() && $supportMessage->save()) {

            $this->saveContactFormFiles($supportMessage->id);
            $this->sendUserEmail($supportMessage);

            return true;
        }

        return false;
    }

    /**
     * Get list support message subjects.
     *
     * @return array
     */
    public function getListSubjects()
    {
        return SupportMessage::getListSubjects();
    }

    /**
     * Get list user titles.
     *
     * @return array
     */
    public function getListUserTitles()
    {
        return [
            User::USER_TITLE_MRS => Yii::t('core', 'Mrs.'),
            User::USER_TITLE_MR  => Yii::t('core', 'Mr.'),
        ];
    }


    /**
     * Save user contact form files.
     *
     * @param int $supportMessageId Support message id
     * @return void
     */
    private function saveContactFormFiles($supportMessageId)
    {
        $files = UploadedFile::getInstances($this, 'files');

        $savedPath = SupportMessage::getFilePath() . '/' . $supportMessageId;
        $fullPath = Yii::getAlias('@frontend/web/') . $savedPath;

        if (!file_exists($fullPath)) {
            mkdir($fullPath, 0777, true);
        }

        foreach ($files as $file) {
            $file->saveAs($savedPath . '/' .  $file->baseName . '.' . $file->extension);
        }
    }

    /**
     * Send user support text on email
     *
     * @param \frontend\modules\core\models\SupportMessage $supportMessage Support message
     * @return bool
     */
    private function sendUserEmail(SupportMessage $supportMessage)
    {
        $template = EmailTemplate::findTemplateSupportMessageToUser();

        if (!$template) {
            return false;
        }

        $body = $template->text;

        $body = strtr($template->text, [
            '{ticketId}' => $supportMessage->id,
            '{supportMessage}' => $this->text,
        ]);

        $mailer = Yii::$app->mailer;

        return $mailer->compose()
            ->setFrom([$mailer->transport->getUsername() => $template->from])
            ->setTo($this->email)
            ->setHtmlBody($body)
            ->setSubject($template->subject)
            ->send();
    }
}