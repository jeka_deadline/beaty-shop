<?php

namespace frontend\modules\core\models\forms;

use frontend\modules\core\validators\PhoneValidator;
use Yii;
use yii\web\UploadedFile;

class DistributorForm extends TrainerDistributorForm
{
    public function rules()
    {
        return [
            [['sex', 'first_name', 'last_name', 'phone', 'email', 'country', 'city', 'address'], 'required'],
            [['first_name', 'last_name', 'phone', 'email'], 'required'],
            ['email', 'email'],
            ['phone', PhoneValidator::className()],
            [['address'], 'string'],
            [['sex', 'phone'], 'string', 'max' => 20],
            [['first_name', 'last_name'], 'string', 'max' => 50],
            [['company', 'email', 'country', 'city', 'tax_number_1', 'tax_number_2'], 'string', 'max' => 100],
            [['files'], 'file', 'skipOnEmpty' => true, 'extensions' => 'pdf, doc, docx, txt, xls, png, jpg, jpeg, bmp', 'maxFiles' => 0],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'sex' => Yii::t('product', 'Title').'*',
            'first_name' => Yii::t('core', 'First Name').'*',
            'last_name' => Yii::t('core', 'Last Name').'*',
            'company' => Yii::t('product', 'Company'),
            'phone' => Yii::t('product', 'Phone').'*',
            'email' => 'E-mail*',
            'country' => Yii::t('product', 'Country').'*',
            'tax_number_1' => Yii::t('core', 'Tax number 1'),
            'tax_number_2' => Yii::t('core', 'Tax number 2'),
            'city' => Yii::t('product', 'City').'*',
            'address' => Yii::t('product', 'Address').'*',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->type = self::TYPE_DISTRUBUTOR;
            return true;
        }
        return false;
    }

    private function sendMailAdmin()
    {
        $mailer = Yii::$app->mailer;

        $message = $mailer->compose()
            ->setFrom([$mailer->transport->getUsername() => 'info'])
            ->setTo(Yii::$app->params[ 'adminEmail' ])
            ->setSubject('New Distributor')
            ->setTextBody("Distributor\n\n"
                .Yii::t('product', 'Title').": {$this->sex}\n"
                .Yii::t('core', 'First Name').": {$this->first_name}\n"
                .Yii::t('core', 'Last Name').": {$this->last_name}\n"
                .Yii::t('product', 'Company').": {$this->company}\n"
                .Yii::t('product', 'Phone').": {$this->phone}\n"
                ."E-mail: {$this->email}\n"
                .Yii::t('product', 'Country').": {$this->country}\n"
                ."State: {$this->state}\n"
                .Yii::t('product', 'City').": {$this->city}\n"
                .Yii::t('product', 'Address').": {$this->address}\n"
                .Yii::t('core', 'Tax number 1').": {$this->tax_number_1}\n"
                .Yii::t('core', 'Tax number 2').": {$this->tax_number_2}\n"
            );

        $allPath = $this->getPath();
        foreach ($this->files as $file) {
            $message->attach($allPath . $file->baseName.'.'.$file->extension);
        }
        $message->send();
    }
}
