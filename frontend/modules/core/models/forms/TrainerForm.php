<?php

namespace frontend\modules\core\models\forms;

use frontend\modules\core\validators\PhoneValidator;
use Yii;
use yii\web\UploadedFile;

class TrainerForm extends TrainerDistributorForm
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sex', 'first_name', 'last_name', 'phone', 'email', 'country', 'city', 'address'], 'required'],
            [['address'], 'string'],
            [['sex', 'phone'], 'string', 'max' => 20],
            [['first_name', 'last_name'], 'string', 'max' => 50],
            [['company', 'email', 'country', 'city'], 'string', 'max' => 100],
            ['email', 'email'],
            ['phone', PhoneValidator::className()],
            [['files'], 'file', 'skipOnEmpty' => true, 'extensions' => 'pdf, doc, docx, txt, xls, png, jpg, jpeg, bmp', 'maxFiles' => 0],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'sex' => Yii::t('product', 'Title').'*',
            'first_name' => Yii::t('core', 'First Name').'*',
            'last_name' => Yii::t('core', 'Last Name').'*',
            'company' => Yii::t('product', 'Company'),
            'phone' => Yii::t('product', 'Phone').'*',
            'email' => 'E-mail*',
            'country' => Yii::t('product', 'Country').'*',
            'city' => Yii::t('product', 'City').'*',
            'address' => Yii::t('product', 'Address').'*'
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->type = self::TYPE_TRAINER;
            return true;
        }
        return false;
    }

    private function sendMailAdmin()
    {
        $mailer = Yii::$app->mailer;

        $message = $mailer->compose()
            ->setFrom([$mailer->transport->getUsername() => 'info'])
            ->setTo(Yii::$app->params[ 'adminEmail' ])
            ->setSubject('New Trainer')
            ->setTextBody("Trainer\n\n"
                .Yii::t('product', 'Title').": {$this->sex}\n"
                .Yii::t('core', 'First Name').": {$this->first_name}\n"
                .Yii::t('core', 'Last Name').": {$this->last_name}\n"
                .Yii::t('product', 'Company').": {$this->company}\n"
                .Yii::t('product', 'Phone').": {$this->phone}\n"
                ."E-mail: {$this->email}\n"
                .Yii::t('product', 'Country').": {$this->country}\n"
                .Yii::t('product', 'City').": {$this->city}\n"
                .Yii::t('product', 'Address').": {$this->address}\n"
            );

        $allPath = $this->getPath();
        foreach ($this->files as $file) {
            $message->attach($allPath . $file->baseName.'.'.$file->extension);
        }
        $message->send();
    }
}
