<?php

namespace frontend\modules\core\models\forms;

use frontend\modules\core\models\ContactStudio;
use Yii;
use yii\base\Model;

class FindStudioForm extends Model
{
    public $query;

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['query'], 'string'],
            [['query'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'query' => Yii::t('core', 'Search'),
        ];
    }

    public function find() {
        return ContactStudio::find()
            ->where(['like', 'description', $this->query])
            ->orWhere(['like', 'address', $this->query])
            ->all();
    }
}
