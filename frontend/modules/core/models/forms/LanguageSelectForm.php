<?php

namespace frontend\modules\core\models\forms;

use frontend\modules\core\models\Language;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class LanguageSelectForm extends Model
{
    public $language = null;

    public function init()
    {
        parent::init();
        $this->language = Yii::$app->language;
    }


    public function rules()
    {
        return [
            [['language'], 'required'],
            [['language'], 'string', 'max' => 10],
            [['language'], 'in', 'range' => array_keys($this->getListLanguage())],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'language' => Yii::t('core', 'Select your language'),
        ];
    }

    public static function getListLanguage() {
        $language = Language::find()
            ->active()
            ->all();
        if (!$language) return [];
        return ArrayHelper::map($language, 'code', 'name');
    }

    public function getInfoSelectLang() {
        return Language::find()
            ->where([
                'code' => Yii::$app->language
            ])
            ->one();
    }
}