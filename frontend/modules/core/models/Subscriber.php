<?php

namespace frontend\modules\core\models;

use Yii;
use common\models\core\Subscriber as BaseSubscriber;
use yii\helpers\ArrayHelper;
use frontend\modules\core\models\EmailTemplate;
use yii\helpers\Url;

/**
 * Front subscriber model extends common subscriber model.
 */
class Subscriber extends BaseSubscriber
{
    /**
     * Field agree with subscribe rules.
     *
     * @var bool $agreeSubscribe
     */
    public $agreeSubscribe;

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                ['agreeSubscribe', 'validateAgree'],
            ]
        );
    }

    /**
     * Validate agree subscribe rules.
     *
     * @return void
     */
    public function validateAgree()
    {
        if (!$this->agreeSubscribe) {
            $this->addError('email', Yii::t('core', 'Not set checkbox agree rules <a href="0">AGB</a> and <a href="{1}">Cookie</a>', [
                Url::toRoute(['/pages/index/page', 'uri' => 'terms']),
                Url::toRoute(['/pages/index/page', 'uri' => 'cookie']),
            ]));
        }
    }

    /**
     * Send user and admin subscribe letters.
     *
     * @return bool
     */
    public function sendLetters()
    {
        if ($this->sendUserLetter()) {

            if (Setting::getIsSendAdminLettersForNewSubscribers()) {
                $this->sendAdminLetter();
            }

            return true;
        }

        return false;
    }

    /**
     * Check if subscriber has email.
     *
     * @param string $email Email
     * @return bool
     */
    public static function hasEmail($email)
    {
        $count = self::find()
            ->where(['email' => $email])
            ->andWhere(['is', 'user_id', null])
            ->count();

        return ($count) ? true : false;
    }

    /**
     * Assign user to subscribers.
     *
     * @param string $email User email
     * @param int $userId User id
     * @return void
     */
    public static function assignUser($email, $userId)
    {
        $subscriber = self::find()
            ->where(['email' => $email])
            ->one();

        if ($subscriber) {
            $subscriber->updateAttributes(['user_id' => $userId]);
        }
    }

    /**
     * Send user subscribe information letter.
     *
     * @return bool
     */
    private function sendUserLetter()
    {
        $template = EmailTemplate::findOne([
            'code' => EmailTemplate::SUBSCRIBE_TEMPLATE_CODE,
        ]);

        if (!$template) {
            return false;
        }

        $text = $template->getTextWithReplacedPlaceholders();


        $mailer = Yii::$app->mailer;

        return $mailer->compose()
            ->setFrom([$mailer->transport->getUsername() => $template->from])
            ->setTo($this->email)
            ->setSubject($template->subject)
            ->setHtmlBody($text)
            ->send();
    }

    /**
     * Send admin subscribe information letter.
     *
     * @return void
     */
    private function sendAdminLetter()
    {
        $mailer = Yii::$app->mailer;

        $mailer->compose()
            ->setFrom([$mailer->transport->getUsername() => 'info'])
            ->setTo(Yii::$app->params[ 'adminEmail' ])
            ->setSubject('User subscriber')
            ->setTextBody(sprintf('User with email %s subscribe to news', $this->email))
            ->send();
    }
}
