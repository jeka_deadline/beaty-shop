<?php

namespace frontend\modules\core\models;

use Yii;
use common\models\core\CorePage as BaseCorePage;
use DateTime;
use yii\helpers\Url;

class CorePage extends BaseCorePage
{
    /**
     * Code for main page.
     *
     * @var string
     */
    const CODE_MAIN_PAGE = 'main';

    /**
     * Code for academy index page.
     *
     * @var string
     */
    const CODE_ACADEMY_PAGE = 'academy-index';

    /**
     * Code for shop page.
     *
     * @var string
     */
    const CODE_SHOP_PAGE = 'shop';

    /**
     * Code for blog page.
     *
     * @var string
     */
    const CODE_BLOG_PAGE = 'blog';

    /**
     * Code for contact page.
     *
     * @var string
     */
    const CODE_CONTACT_PAGE = 'contact';

    /**
     * Code for user account page.
     *
     * @var string
     */
    const CODE_USER_ACCOUNT_PAGE = 'user-account';

    /**
     * Code for cart page.
     *
     * @var string
     */
    const CODE_CART_PAGE = 'cart';

    /**
     * Code for checkout page.
     *
     * @var string
     */
    const CODE_CHECKOUT_PAGE = 'checkout';

    /**
     * Code for thanks page.
     *
     * @var string
     */
    const CODE_THANKS_PAGE = 'thanks';

    /**
     * Code for careers page.
     *
     * @var string
     */
    const CODE_CAREERS_PAGE = 'careers';

    /**
     * Code for studios page.
     *
     * @var string
     */
    const CODE_STUDIOS_PAGE = 'studois';

    /**
     * Code for shop new page.
     *
     * @var string
     */
    const CODE_PAGE_SHOP_NEW = 'shop-new';

    /**
     * Code for login page.
     *
     * @var string
     */
    const CODE_PAGE_LOGIN = 'login';

    /**
     * Code for search page.
     *
     * @var string
     */
    const CODE_PAGE_SEARCH = 'search';

    /**
     * Code for trainers/distributors page.
     *
     * @var string
     */
    const CODE_PAGE_TRAINERS_DISTRIBUTORS = 'trainers-distributors';

    /**
     * Find model main page with seo information.
     *
     * @static
     * @return frontend\modules\core\models\CorePage
     */
    public static function findMainPage()
    {
        return self::findPageByCode(self::CODE_MAIN_PAGE);
    }

    /**
     * Find model academy page with seo information.
     *
     * @static
     * @return frontend\modules\core\models\CorePage
     */
    public static function findAcademyPage()
    {
        return self::findPageByCode(self::CODE_ACADEMY_PAGE);
    }

    /**
     * Find model shop page with seo information.
     *
     * @static
     * @return frontend\modules\core\models\CorePage
     */
    public static function findShopPage()
    {
        return self::findPageByCode(self::CODE_SHOP_PAGE);
    }

    /**
     * Find model blog page with seo information.
     *
     * @static
     * @return frontend\modules\core\models\CorePage
     */
    public static function findBlogPage()
    {
        return self::findPageByCode(self::CODE_BLOG_PAGE);
    }

    /**
     * Find model contact page with seo information.
     *
     * @static
     * @return frontend\modules\core\models\CorePage
     */
    public static function findContactPage()
    {
        return self::findPageByCode(self::CODE_CONTACT_PAGE);
    }

    /**
     * Find model user account page with seo information.
     *
     * @static
     * @return frontend\modules\core\models\CorePage
     */
    public static function findUserAccountPage()
    {
        return self::findPageByCode(self::CODE_USER_ACCOUNT_PAGE);
    }

    /**
     * Find model cart page with seo information.
     *
     * @static
     * @return frontend\modules\core\models\CorePage
     */
    public static function findCartPage()
    {
        return self::findPageByCode(self::CODE_CART_PAGE);
    }

    /**
     * Find model checkout page with seo information.
     *
     * @static
     * @return frontend\modules\core\models\CorePage
     */
    public static function findCheckoutPage()
    {
        return self::findPageByCode(self::CODE_CHECKOUT_PAGE);
    }

    /**
     * Find model thanks page with seo information.
     *
     * @static
     * @return frontend\modules\core\models\CorePage
     */
    public static function findThanksPage()
    {
        return self::findPageByCode(self::CODE_THANKS_PAGE);
    }

    /**
     * Find model career page with seo information.
     *
     * @static
     * @return frontend\modules\core\models\CorePage
     */
    public static function findCareersPage()
    {
        return self::findPageByCode(self::CODE_CAREERS_PAGE);
    }

    /**
     * Find model studio page with seo information.
     *
     * @static
     * @return frontend\modules\core\models\CorePage
     */
    public static function findStudiosPage()
    {
        return self::findPageByCode(self::CODE_STUDIOS_PAGE);
    }

    /**
     * Find model shop new products page with seo information.
     *
     * @static
     * @return frontend\modules\core\models\CorePage
     */
    public static function findShopNewPage()
    {
        return self::findPageByCode(self::CODE_PAGE_SHOP_NEW);
    }

    /**
     * Find model login page with seo information.
     *
     * @static
     * @return frontend\modules\core\models\CorePage
     */
    public static function findLoginPage()
    {
        return self::findPageByCode(self::CODE_PAGE_LOGIN);
    }

    /**
     * Find model search page with seo information.
     *
     * @static
     * @return frontend\modules\core\models\CorePage
     */
    public static function findSearchPage()
    {
        return self::findPageByCode(self::CODE_PAGE_SEARCH);
    }

    /**
     * Find model trainers/distributors with seo information.
     *
     * @static
     * @return frontend\modules\core\models\CorePage
     */
    public static function findTrainersDistributorsPage()
    {
        return self::findPageByCode(self::CODE_PAGE_TRAINERS_DISTRIBUTORS);
    }

    /**
     * Generate list url for sitemap
     *
     * @param string $frequently Value frequently
     * @param string $priority Value priority
     * @return array
     */
    public static function getListItemsForSitemap($frequently, $priority = '0.5')
    {
        $items = [];
        $pages = static::find()
            ->all();

        foreach ($pages as $page) {
            $dateUpdatePage = new DateTime($page->updated_at);

            switch ($page->code) {
                case self::CODE_MAIN_PAGE:
                    $url = '/';
                    break;
                case self::CODE_PAGE_LOGIN:
                    $url = '/login';
                    break;
                case self::CODE_CART_PAGE:
                    $url = '/cart';
                    break;
                case self::CODE_CHECKOUT_PAGE:
                    $url = '/checkout';
                    break;
                case self::CODE_STUDIOS_PAGE:
                    $url = '/find-studio';
                    break;
                case self::CODE_PAGE_TRAINERS_DISTRIBUTORS:
                    $url = '/trainer-distributor';
                    break;
                case self::CODE_CONTACT_PAGE:
                    $url = '/contacts';
                    break;
                case self::CODE_SHOP_PAGE:
                    $url = '/shop';
                    break;
                case self::CODE_BLOG_PAGE:
                    $url = '/blog';
                    break;
                case self::CODE_ACADEMY_PAGE:
                    $url = '/academy';
                    break;
                case self::CODE_CAREERS_PAGE:
                    $url = '/careers';
                    break;
                case self::CODE_PAGE_SHOP_NEW:
                    $url = '/shop/news';
                    break;
                case self::CODE_THANKS_PAGE:
                    $url = '/thanks';
                    break;
                default:
                    $url = null;
                    break;
            }

            if (is_null($url)) {
                continue;
            }

            $items[] = [
                'loc'         => Url::toRoute([$url], true),
                'lastmod'     => $dateUpdatePage->format(DateTime::W3C),
                'changefreq'  => $frequently,
                'priority'    => $priority,
            ];
        }

        return $items;
    }

    /**
     * Find seo model page by code.
     *
     * @static
     * @access private
     * @param string $code Code page.
     * @return frontend\modules\core\models\CorePage
     */
    private static function findPageByCode($code)
    {
        return self::find()
            ->where(['code' => $code])
            ->one();
    }
}
