<?php

namespace frontend\modules\core\models;

use Yii;
use common\models\core\TextBlock as BaseTextBlock;
use frontend\queries\ActiveQuery;

/**
 * Frontend text block model extends common text block model.
 */
class TextBlock extends BaseTextBlock
{
    /**
     * {@inheritdoc}
     *
     * @return \frontend\queries\ActiveQuery
     */
    public static function find()
    {
        return new ActiveQuery(get_called_class());
    }
}
