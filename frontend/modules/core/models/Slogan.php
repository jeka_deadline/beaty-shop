<?php

namespace frontend\modules\core\models;

use frontend\modules\core\behaviors\LangBehavior;
use frontend\queries\ActiveQuery;
use yii\helpers\ArrayHelper;

class Slogan extends \common\models\core\Slogan
{
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                [
                    'class' => LangBehavior::className(),
                    'langModel' => SloganLangField::className(),
                    'modelForeignKey' => 'slogan_id',
                    'attributes' => [
                        'slogan',
                    ],
                ]
            ]
        );
    }

    public static function find()
    {
        return new ActiveQuery(get_called_class());
    }
}
