<?php

namespace frontend\modules\core\models;

use frontend\modules\core\behaviors\LangBehavior;
use frontend\queries\ActiveQuery;
use yii\helpers\ArrayHelper;

class ContactStudio extends \common\models\core\ContactStudio
{
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                [
                    'class' => LangBehavior::className(),
                    'langModel' => ContactStudioLangField::className(),
                    'modelForeignKey' => 'contact_studio_id',
                    'attributes' => [
                        'name',
                        'description',
                    ],
                ]
            ]
        );
    }

    public static function find()
    {
        return new ActiveQuery(get_called_class());
    }
}
