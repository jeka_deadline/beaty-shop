<?php

namespace frontend\modules\core\models;

use frontend\modules\product\models\Product;
use frontend\modules\product\models\ProductCategory;
use frontend\modules\pages\models\Page;
use frontend\modules\core\models\CorePage;
use yii\helpers\ArrayHelper;

class SitemapGenerator
{
    /**
     * @var string Always frequently for url
     */
    const CHANGEFREQ_ALWAYS = 'always';

    /**
     * @var string Hourly frequently for url
     */
    const CHANGEFREQ_HOURLY = 'hourly';

    /**
     * @var string Daily frequently for url
     */
    const CHANGEFREQ_DAILY = 'daily';

    /**
     * @var string Weekly frequently for url
     */
    const CHANGEFREQ_WEEKLY = 'weekly';

    /**
     * @var string Monthly frequently for url
     */
    const CHANGEFREQ_MONTHLY = 'monthly';

    /**
     * @var string Yearly frequently for url
     */
    const CHANGEFREQ_YEARLY = 'yearly';

    /**
     * @var string Never frequently for url
     */
    const CHANGEFREQ_NEWER = 'never';

    /**
     * Generate list url for sitemap
     *
     * @return array
     */
    public static function generate()
    {
        $urls     = [];
        $classes  = [
            [
                'class'       => Product::className(),
                'frequently'  => self::CHANGEFREQ_MONTHLY,
                'priority'    => '0.7',
            ],
            [
                'class'       => ProductCategory::className(),
                'frequently'  => self::CHANGEFREQ_MONTHLY,
                'priority'    => '0.5',
            ],
            [
                'class'       => CorePage::className(),
                'frequently'  => self::CHANGEFREQ_MONTHLY,
                'priority'    => '0.5',
            ],
            [
                'class'       => Page::className(),
                'frequently'  => self::CHANGEFREQ_MONTHLY,
                'priority'    => '0.5',
            ],
        ];

        foreach ($classes as $itemClass) {

            $urls = ArrayHelper::merge(
                $urls,
                call_user_func_array(
                    [
                        $itemClass[ 'class' ],
                        'getListItemsForSitemap'
                    ],
                    [
                        $itemClass[ 'frequently' ],
                        $itemClass[ 'priority' ]
                    ]
                )
            );
        }

        return $urls;
    }
}