<?php

namespace frontend\modules\core\models;

use Yii;

class Setting extends \common\models\core\Setting
{
    const MODAL_SESSION_KEY = 'modal';

    /**
     * Key for free shipping sum.
     *
     * @static
     * @access private
     * @var string $freeShippingSettingKey
     */
    private static $freeShippingSettingKey = 'order.free.shipping';

    /**
     * Key for free shipping other countries.
     *
     * @static
     * @access private
     * @var string $freeShippingSettingKey
     */
    private static $freeShippingOtherCountriesSettingKey = 'order.free.shipping.other.countries';

    /**
     * Key for free shipping sum Switzerlan country.
     *
     * @static
     * @access private
     * @var string $freeShippingSettingKey
     */
    private static $freeShippingSwitzerlandSettingKey = 'order.free.shipping.switzerland.country';

    /**
     * Key for text success signup.
     *
     * @static
     * @access private
     * @var string $successUserSignupTextSettingKey
     */
    private static $successUserSignupTextSettingKey = 'user.success.signup.text';

    /**
     * Key for end date register new users with discount.
     *
     * @static
     * @access private
     * @var string $successUserSignupTextSettingKey
     */
    private static $keyForDateRegisterForNewUserWithDiscount = 'finish-date.register-with-discount';

    /**
     * Key for need send admin email for new subscribers.
     *
     * @static
     * @access private
     * @var string $successUserSignupTextSettingKey
     */
    private static $keySendAdminEmailForNewSubscribers = 'send.email.toadmin.subscribers';

    /**
     * Key for need send admin email for new orders.
     *
     * @static
     * @access private
     * @var string $successUserSignupTextSettingKey
     */
    private static $keySendAdminEmailForNewOrders = 'send.email.toadmin.neworders';

    /**
     * Get value sum for free shipping
     *
     * @return int
     */
    public static function getFreeShippingSettingValue()
    {
        return static::value(self::$freeShippingSettingKey);
    }

    /**
     * Get value sum for free shipping for other countries.
     *
     * @return int
     */
    public static function getFreeShippingSettingForOtherCountriesValue()
    {
        return static::value(self::$freeShippingOtherCountriesSettingKey);
    }

    /**
     * Get value sum for free shipping for Switzerland country.
     *
     * @return int
     */
    public static function getFreeShippingSettingForSwitzerlandValue()
    {
        return static::value(self::$freeShippingSwitzerlandSettingKey);
    }

    /**
     * Get value text for success signup.
     *
     * @return text
     */
    public static function getSuccessUserSignupTextValue()
    {
        return static::value(self::$successUserSignupTextSettingKey);
    }

    public static function getDateRegisterForNewUserWithDiscount()
    {
        return static::value(self::$keyForDateRegisterForNewUserWithDiscount);
    }

    public static function getIsSendAdminLettersForNewSubscribers()
    {
        return static::value(self::$keySendAdminEmailForNewSubscribers);
    }

    public static function getIsSendAdminLettersForNewOrders()
    {
        return static::value(self::$keySendAdminEmailForNewOrders);
    }
}
