<?php

namespace frontend\modules\core\models;

use Yii;
use Datetime;
use common\models\core\EmailTemplate as BaseEmailTemplate;
use yii\helpers\Url;
use frontend\modules\user\models\User;
use frontend\modules\product\models\ProductVariation;

class EmailTemplate extends BaseEmailTemplate
{
    /**
     * Find email template for user confirm email.
     *
     * @static
     * @return \frontend\modules\core\models\EmailTemplate
     */
    public static function findTemplateConfirmEmail()
    {
        return static::findTemplateByCode(self::CONFIRM_EMAIL_TEMPLATE_CODE);
    }

    /**
     * Find email template for user support message to user.
     *
     * @static
     * @return \frontend\modules\core\models\EmailTemplate
     */
    public static function findTemplateSupportMessageToUser()
    {
        return static::findTemplateByCode(self::SUPPORT_MESSAGE_TEMPLATE_CODE);
    }

    /**
     * Find email template for user reset password.
     *
     * @static
     * @return \frontend\modules\core\models\EmailTemplate
     */
    public static function findTemplateRessetEmail()
    {
        return static::findTemplateByCode(self::RESET_PASSWORD_TEMPLATE_CODE);
    }

    /**
     * Find email template for user subscribe.
     *
     * @static
     * @return \frontend\modules\core\models\EmailTemplate
     */
    public static function findTemplateSubscribe()
    {
        return static::findTemplateByCode(self::SUBSCRIBE_TEMPLATE_CODE);
    }

    /**
     * Find email template for user register.
     *
     * @static
     * @return \frontend\modules\core\models\EmailTemplate
     */
    public static function findTemplateUserRegister()
    {
        return static::findTemplateByCode(self::USER_REGISTER_TEMPLATE_CODE);
    }

    /**
     * Find email template for user order.
     *
     * @static
     * @return \frontend\modules\core\models\EmailTemplate
     */
    public static function findTemplateUserOrder()
    {
        return static::findTemplateByCode(self::USER_ORDER_TEMPLATE_CODE);
    }

    /**
     * Find email template for new user with coupon.
     *
     * @static
     * @return \frontend\modules\core\models\EmailTemplate
     */
    public static function findTemplateNewUserCoupon()
    {
        return static::findTemplateByCode(self::NEW_USER_COUPON_CODE);
    }

    /**
     * Get text email template with replaced placeholders.
     *
     * @return string
     */
    public function getTextWithReplacedPlaceholders()
    {
        switch ($this->code) {
            case self::SUBSCRIBE_TEMPLATE_CODE:
                return self::getTextWithReplacedPlaceholdersSubscribe($this);
            case self::USER_REGISTER_TEMPLATE_CODE:
                return self::getTextWithReplacedPlaceholdersUserRegister($this);
        }
    }

    /**
     * Get text email template with replaced placeholders for new user with coupon.
     *
     * @param string $couponCode Coupon code.
     * @param frontend\modules\user\models\Profile $userProfile User profile model.
     * @return string
     */
    public function getTextWithReplacedPlaceholdersForNewUserCoupon($couponCode, $userProfile)
    {
        if ($userProfile->title === User::USER_TITLE_MRS) {
            $sex = 'geehrte';
        } else {
            $sex = 'geehrter';
        }

        $username = $userProfile->name . ' ' . $userProfile->surname;

        $placeholders = self::getBasePlaceholers() + [
            '{couponCode}' => $couponCode,
            '{sex}' => $sex,
            '{username}' => $username,
        ];

        $text = strtr($this->text, $placeholders);

        return str_replace('{linkInBrowser}', Url::toRoute(['/core/index/email-in-browser', 'code' => gzencode($text, 9)], true), $text);
    }

    /**
     * Get text email template with replaced placeholders for letter order.
     *
     * @param frontend\modules\product\models\Order $order Order model.
     * @return string
     */
    public function getTextWithReplacedPlaceholdersUserOrder($order)
    {
        $orderCreatedDate = new DateTime($order->created_at);
        if ($order->orderShippingInformation->title === User::USER_TITLE_MRS) {
            $sex = 'geehrte';
        } else {
            $sex = 'geehrter';
        }

        $username = $order->orderShippingInformation->name . ' ' . $order->orderShippingInformation->surname;
        $shippingAddress = $username . ', ' . $order->orderShippingInformation->address1 . ', ' . $order->orderShippingInformation->zip . ' ' . $order->orderShippingInformation->city;

        $orderItems = '';

        preg_match('#{templateRowItem}(.*?){/templateRowItem}#ms', $this->text, $templateRowItem);

        if (isset($templateRowItem[ 1 ]) && !empty($templateRowItem[ 1 ])) {
            $template = $templateRowItem[ 1 ];

            foreach ($order->orderProducts as $orderProduct) {
                $productPrice = ($orderProduct->promotion) ? $orderProduct->promotion : $orderProduct->price;
                $orderItems .= strtr($template, [
                    '{productVendorCode}' => $orderProduct->productVariation->vendor_code,
                    '{productName}' => $orderProduct->product_variation_name,
                    '{productCount}' => $orderProduct->count,
                    '{productSumWithCount}' => ProductVariation::viewFormatPrice($productPrice * $orderProduct->count),
                    '{productPrice}' => ProductVariation::viewFormatPrice($productPrice),
                ]);
            }
        }

        $this->text = preg_replace('#{templateRowItem}(.*?){/templateRowItem}#ms', '', $this->text);

        $placeholders = self::getBasePlaceholers() + [
            '{orderNumber}' => $order->order_number,
            '{orderCreateDate}' => $orderCreatedDate->format('d. F Y'),
            '{sex}' => $sex,
            '{username}' => $username,
            '{orderTypeShipping}' => $order->orderShippingInformation->humanShippingMethod,
            '{orderTypePayment}' => $order->humanPaymentMethod,
            '{orderShippingAddress}' => $shippingAddress,
            '{orderItems}' => $orderItems,
            '{sumAllProducts}' => ProductVariation::viewFormatPrice($order->total_sum),
            '{sumShipping}' => ProductVariation::viewFormatPrice($order->shipping_price),
            '{sumDiscount}' => ProductVariation::viewFormatPrice($order->sum_discount),
            '{sumTax}' => ProductVariation::viewFormatPrice($order->tax),
            '{brutto}' => ProductVariation::viewFormatPrice($order->full_sum - $order->shipping_price),
            '{orderFullSum}' => ProductVariation::viewFormatPrice($order->full_sum),
            '{orderShippingDate}' => $order->orderShippingInformation->getShippingTimeDeliveryByMethod(),
        ];

        $cipher = 'AES-128-CBC';
        $secret_iv = 'This is my secret iv';
        $iv = substr(hash('sha256', $secret_iv), 0, 16);

        $output = openssl_encrypt(json_encode($placeholders), $cipher, $order::$keyCrypt, 0, $iv);

        $text = strtr($this->text, $placeholders);

        return str_replace('{linkInBrowser}', Url::toRoute(['/core/index/email-in-browser-order', 'output' => gzencode($output, 9)], true), $text);
    }

    /**
     * Get text email template with replaced placeholders for confirm email letter.
     *
     * @param string $link Link confirm email.
     * @return string
     */
    public function getTextWithReplacedPlaceholdersConfirmEmail(User $user, $link)
    {
        $username = $user->profile->name . ' ' . $user->profile->surname;

        if ($user->profile->title === User::USER_TITLE_MRS) {
            $sex = 'geehrte';
        } else {
            $sex = 'geehrter';
        }

        $title = User::getHumanTitle($user->profile->title);

        $placeholders = self::getBasePlaceholers() + [
            '{confirmEmailLink}' => $link,
            '{sex}' => $sex,
            '{title}' => $title,
            '{username}' => $username,
            '{emailLogoGrey}' => Url::toRoute([self::IMG_PATH . '/img/main-logo-grey-bg.jpg'], true),
        ];

        $text = strtr($this->text, $placeholders);

        return str_replace('{linkInBrowser}', Url::toRoute(['/core/index/email-in-browser', 'code' => gzencode($text, 9)], true), $text);
    }

    /**
     * Get text email template with replaced placeholders for reset password letter.
     *
     * @param string $link Reset password link.
     * @param frontend\modules\user\models\User $user User model.
     * @return string
     */
    public function getTextWithReplacedPlaceholdersResetPassword($link, $user)
    {
        if ($user->profile->title === User::USER_TITLE_MRS) {
            $sex = 'geehrte';
        } else {
            $sex = 'geehrter';
        }

        $placeholders = self::getBasePlaceholers() + [
            '{resetPasswordLink}' => $link,
            '{username}' => $user->fullName,
            '{sex}' => $sex,
        ];

        $text = strtr($this->text, $placeholders);

        return str_replace('{linkInBrowser}', Url::toRoute(['/core/index/email-in-browser', 'code' => gzencode($text, 9)], true), $text);
    }

    /**
     * Find template by code.
     *
     * @access protected
     * @param string $code Email template code.
     * @return \frontend\modules\core\models\EmailTemplate
     */
    protected static function findTemplateByCode($code)
    {
        return static::find()
            ->where(['code' => $code])
            ->one();
    }

    /**
     * Replaced placeholders for subscribe template.
     *
     * @static
     * @access private
     * @param \frontend\modules\core\models\EmailTemplate $template Email template model.
     * @return string
     */
    private static function getTextWithReplacedPlaceholdersSubscribe($template)
    {
        $placeholders = self::getBasePlaceholers() + [
            '{mainImg}' => Url::toRoute([self::IMG_PATH . '/img/main-mail.jpg'], true),
        ];

        $text = strtr($template->text, $placeholders);

        return str_replace('{linkInBrowser}', Url::toRoute(['/core/index/email-in-browser', 'code' => gzencode($text, 9)], true), $text);
    }

    /**
     * Replaced placeholders for user register template.
     *
     * @static
     * @access private
     * @param \frontend\modules\core\models\EmailTemplate $template Email template model.
     * @return string
     */
    private static function getTextWithReplacedPlaceholdersUserRegister($template)
    {
        $placeholders = self::getBasePlaceholers() + [
            '{linkLogin}' => Url::toRoute(['/user/security/login'], true),
        ];

        $text = strtr($template->text, $placeholders);

        return str_replace('{linkInBrowser}', Url::toRoute(['/core/index/email-in-browser', 'code' => gzencode($text, 9)], true), $text);
    }

    /**
     * Get base placeholders for email templates.
     *
     * @static
     * @access private
     * @return array
     */
    private static function getBasePlaceholers()
    {
        return [
            '{emailLogo}' => Url::toRoute([self::IMG_PATH . '/img/infiniry_logo.png'], true),
            '{fbIcon}' => Url::toRoute([self::IMG_PATH . '/img/fb.png'], true),
            '{igIcon}' => Url::toRoute([self::IMG_PATH . '/img/ig.png'], true),
            '{agbUrl}' => Url::toRoute(['/pages/index/page', 'uri' => 'terms'], true),
            '{datenschutzUrl}' => Url::toRoute(['/pages/index/page', 'uri' => 'cookie'], true),
            '{impressumUrl}' => Url::toRoute(['/pages/index/page', 'uri' => 'impressum'], true),
            '{igUrl}' => 'https://www.instagram.com/infinitylashesshop',
            '{fbUrl}' => 'https://www.facebook.com/Infinity-Lashes-Academy-199428187583919/',
        ];
    }
}
