<?php

namespace frontend\modules\core\controllers;

use frontend\modules\core\models\forms\LanguageSelectForm;
use Yii;
use frontend\modules\core\controllers\FrontController;
use yii\helpers\ArrayHelper;
use frontend\modules\academy\models\Academy;
use frontend\modules\core\models\forms\SupportMessageForm;
use yii\web\Cookie;
use yii\web\Response;
use frontend\widgets\bootstrap4\ActiveForm;
use frontend\modules\core\models\Setting;
use yii\web\NotFoundHttpException;
use frontend\modules\core\models\EmailTemplate;
use frontend\modules\product\models\Order;
use kartik\mpdf\Pdf;
use frontend\modules\core\models\CorePage;
use frontend\modules\core\models\SitemapGenerator;

/**
 * Class core controller.
 */
class IndexController extends FrontController
{
    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Display main page.
     *
     * @return string
     */
    public function actionIndex()
    {
        $courses = Academy::getRecommended(3);

        $modalText = Yii::$app->session->get(Setting::MODAL_SESSION_KEY, null);

        if (Yii::$app->session->has(Setting::MODAL_SESSION_KEY)) {
            Yii::$app->session->remove(Setting::MODAL_SESSION_KEY);
        }

        if ($page = CorePage::findMainPage()) {
            $this->setMetaTitle($page->meta_title);
            $this->setMetaDescription($page->meta_description);
            $this->setMetaKeywords($page->meta_keywords);
        }

        return $this->render('index', [
            'courses' => $courses,
            'modalText' => $modalText,
        ]);
    }

    public function actionContactUs()
    {
        $model = new SupportMessageForm();

        if ($model->load(Yii::$app->request->post()) && $model->saveSupportMessage()) {

            Yii::$app->response->format = Response::FORMAT_JSON;

            return [
                'status' => true,
                'text' => Yii::t('core', 'Your application is accepted, we will contact you as soon as possible'),
            ];
        }

        if (Yii::$app->request->isAjax) {
            return [
                'status' => false,
                'text' => Yii::t('core', 'Something went wrong, try again later'),
            ];
        }

        if ($page = CorePage::findContactPage()) {
            $this->setMetaTitle($page->meta_title);
            $this->setMetaDescription($page->meta_description);
            $this->setMetaKeywords($page->meta_keywords);
        }

        return $this->render('contact-us', compact('model'));
    }

    /**
     * Ajax validation contact form.
     *
     * @return JSON response validation result
     */
    public function actionAjaxValidationContactForm()
    {
        $form = new SupportMessageForm();

        if (Yii::$app->request->isAjax && $form->load(Yii::$app->request->post())) {

            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($form);
        }

        return [
            'sdasdas' => 'dasdasd',
        ];
    }

    public function actionMaintaince()
    {
        $this->layout = '/maintaince';

        return $this->render('maintaince');
    }

    /**
     * {@inheritdoc}
     *
     * @param \yii\base\Action $action {@inheritdoc}
     * @param mixed $result {@inheritdoc}
     * @return mixed
     */
    public function actionCookiesPolicy()
    {
        $cookies = Yii::$app->getResponse()->getCookies();

        if (!Yii::$app->request->cookies->has('cookies-policy')) {

            $cookies->add(new \yii\web\Cookie([
                'name' => 'cookies-policy',
                'value' => true,
            ]));

        }
        return true;
    }

    public function actionLanguage()
    {
        $languageForm = new LanguageSelectForm();

        if ($languageForm->load(Yii::$app->request->post()) && $languageForm->validate()) {
            $languageCookie = new Cookie([
                'name' => 'language',
                'value' => $languageForm->language,
                'expire' => time() + 2592000,
            ]);
            Yii::$app->response->cookies->add($languageCookie);
        }

        return $this->redirect(['/']);
    }

    /**
     * View email template in browser.
     *
     * @param string $code Browser email code.
     * @return string
     */
    public function actionEmailInBrowser($code)
    {
        $text = gzdecode($code);

        echo str_replace('Im Browser anzeigen', '', $text);

        exit;
    }

    /**
     * View email template order email in browser.
     *
     * @param string $code Browser email code.
     * @return string
     */
    public function actionEmailInBrowserOrder($output)
    {
        $template = EmailTemplate::findOne([
            'code' => EmailTemplate::USER_ORDER_TEMPLATE_CODE,
        ]);

        if (!$template) {
            throw new NotFoundException("Error Processing Request", 1);
        }

        $cipher = 'AES-128-CBC';
        $secret_iv = 'This is my secret iv';
        $iv = substr(hash('sha256', $secret_iv), 0, 16);

        $output = json_decode(openssl_decrypt(gzdecode($output), $cipher, Order::$keyCrypt, 0, $iv), true);

        if (!is_array($output)) {
            throw new NotFoundException("Error Processing Request", 1);
        }

        $text =  strtr($template->text, $output);
        $text = preg_replace('#{templateRowItem}(.*?){/templateRowItem}#ms', '', $text);
        echo str_replace('Im Browser anzeigen', '', $text);
        exit;
    }

    /**
     * Generate sitemap
     *
     * @return string XML format
     */
    public function actionSitemap()
    {
        $urls = SitemapGenerator::generate();

        Yii::$app->response->format = Response::FORMAT_RAW;
        Yii::$app->response->headers->add('Content-Type', 'text/xml');

        return $this->renderPartial('sitemap', compact('urls'));
    }
}
