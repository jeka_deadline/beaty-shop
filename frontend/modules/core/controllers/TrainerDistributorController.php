<?php

namespace frontend\modules\core\controllers;

use frontend\modules\core\models\forms\DistributorForm;
use frontend\modules\core\models\forms\TrainerForm;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use frontend\modules\core\models\CorePage;

/**
 * TrainerDistributorController.
 */
class TrainerDistributorController extends FrontController
{

    /**
     *
     * @return string
     */
    public function actionIndex()
    {
        $formTrainer = new TrainerForm();
        $formDistributor = new DistributorForm();

        if ($page = CorePage::findTrainersDistributorsPage()) {
            $this->setMetaTitle($page->meta_title);
            $this->setMetaDescription($page->meta_description);
            $this->setMetaKeywords($page->meta_keywords);
        }

        return $this->render('index', [
            'formTrainer' => $formTrainer,
            'formDistributor' => $formDistributor
        ]);
    }

    public function actionSendTrainer()
    {
        if (!Yii::$app->request->isAjax) throw new NotFoundHttpException("Not found");

        Yii::$app->response->format = Response::FORMAT_JSON;

        $result = [
            'status' => 'error',
            'message' => 'Error sending.'
        ];

        $formTrainer = new TrainerForm();

        if ($formTrainer->load(Yii::$app->request->post()) && $formTrainer->save()) {
            $result = [
                'status' => 'ok',
                'message' => Yii::t('core', 'Thank you so much! Your letter has been successfully sent, we will contact you as soon as possible.')
            ];
            return $result;
        }
        $result['message'] = 'Error sending.';
        return $result;
    }

    public function actionSendDistributor()
    {
        if (!Yii::$app->request->isAjax) throw new NotFoundHttpException("Not found");

        Yii::$app->response->format = Response::FORMAT_JSON;

        $result = [
            'status' => 'error',
            'message' => 'Error sending.'
        ];

        $formDistributor = new DistributorForm();

        if ($formDistributor->load(Yii::$app->request->post()) && $formDistributor->save()) {
            $result = [
                'status' => 'ok',
                'message' => Yii::t('core', 'Thank you so much! Your letter has been successfully sent, we will contact you as soon as possible.')
            ];
            return $result;
        }

        return $result;
    }
}