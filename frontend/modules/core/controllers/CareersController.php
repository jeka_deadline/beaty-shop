<?php

namespace frontend\modules\core\controllers;

use frontend\modules\core\models\Career;
use frontend\modules\core\models\CorePage;
use yii\helpers\Url;

class CareersController extends FrontController
{
    /**
     *
     * @return string
     */
    public function actionIndex()
    {
        $modelCareers = Career::find()
            ->active()
            ->orderBy(['display_order' => SORT_ASC])
            ->all();

        if ($page = CorePage::findCareersPage()) {
            $this->setMetaTitle($page->meta_title);
            $this->setMetaDescription($page->meta_description);
            $this->setMetaKeywords($page->meta_keywords);
        }

        $this->view->registerMetaTag(['name' => 'og:image', 'content' => Url::toRoute(['/img/careers-main-img.jpg'], true)]);

        return $this->render('index', [
            'modelCareers' => $modelCareers
        ]);
    }

}