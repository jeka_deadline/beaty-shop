<?php

namespace frontend\modules\core\controllers;

use Yii;
use yii\web\Controller;

class FrontController extends Controller
{
    /**
     * Set meta title.
     *
     * @param string $metaTitle Meta title for page
     * @return void
     */
    public function setMetaTitle($metaTitle = '')
    {
        if ($metaTitle) {
            $this->view->title = $metaTitle;
        }
    }

    /**
     * Set meta description.
     *
     * @param string $metaDescription Meta description for page
     * @return void
     */
    public function setMetaDescription($metaDescription = '')
    {
        if ($metaDescription) {
            $this->view->registerMetaTag(['name' => 'description', 'content' => $metaDescription]);
        }
    }

    /**
     * Set meta keywords.
     *
     * @param string $metaKeywords Meta keywords for page
     * @return void
     */
    public function setMetaKeywords($metaKeywords = '')
    {
        if ($metaKeywords) {
            $this->view->registerMetaTag(['name' => 'keywords', 'content' => $metaKeywords]);
        }
    }

    /**
     * {@inheritdoc}
     *
     * @param \yii\base\Action $action {@inheritdoc}
     * @return bool
     */
    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return false;
        }

        if (Yii::$app->request->cookies->has('cookies-policy')) {
            Yii::$app->params[ 'cookies-policy' ] = true;
        }

        return true;
    }
}
