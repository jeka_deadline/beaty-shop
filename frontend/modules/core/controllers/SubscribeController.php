<?php

namespace frontend\modules\core\controllers;

use Exception;
use Yii;
use frontend\modules\core\models\Subscriber;
use yii\web\Controller;
use yii\web\Response;
use frontend\widgets\bootstrap4\ActiveForm;

/**
 * SubscribeController.
 */
class SubscribeController extends Controller
{
    /**
     * Subscribe user.
     *
     * @return string
     */
    public function actionSubscribe()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new Subscriber();
        $response = [
            'status' => false,
            'text' => null,
            'form' => null,
        ];

        if (!Yii::$app->user->isGuest) {
            $model->user_id = Yii::$app->user->identity->id;
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            try {
                $model->sendLetters();
            } catch (Exception $e) {
            }

            $model->save(false);
            $response[ 'status' ] = true;
            $response[ 'text' ] = Yii::t('core', 'Thank You! You are now signed up for the Infinity Lashes newsletter as <span>{0}</span>',[
                $model->email
            ]);

            return $response;
        }

        $response[ 'form' ] = $this->renderAjax('@app/modules/core/widgets/views/subscribe-form', compact('model'));

        return $response;
    }

    /**
     * Ajax validate subscriber model.
     *
     * @return array
     */
    public function actionAjaxValidateSubscribeModel()
    {
        $model = new Subscriber();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($model);
        }
    }
}