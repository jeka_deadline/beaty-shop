<?php

namespace frontend\modules\core\controllers;

use DOMDocument;
use frontend\modules\core\models\ContactStudio;
use frontend\modules\core\models\forms\FindStudioForm;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use frontend\modules\core\models\CorePage;

class FindStudioController extends FrontController
{

    /**
     *
     * @return string
     */
    public function actionIndex()
    {
        $findStudioForm = new FindStudioForm();
        $modelContactStudios = ContactStudio::find()
            ->active()
            ->orderBy(['display_order' => SORT_ASC])
            ->all();

        if ($page = CorePage::findStudiosPage()) {
            $this->setMetaTitle($page->meta_title);
            $this->setMetaDescription($page->meta_description);
            $this->setMetaKeywords($page->meta_keywords);
        }

        return $this->render('index', [
            'modelContactStudios' => $modelContactStudios,
            'findStudioForm' => $findStudioForm
        ]);
    }

    public function actionSearchStudio()
    {
        if (!Yii::$app->request->isAjax) throw new NotFoundHttpException("Not found");

        Yii::$app->response->format = Response::FORMAT_JSON;

        $result = [
            'status' => 'error',
            'message' => ''
        ];

        $formFindStudio = new FindStudioForm();

        if ($formFindStudio->load(Yii::$app->request->post()) && $formFindStudio->validate()) {
            $result = [
                'status' => 'ok',
                'html' => $this->renderPartial('search-studio', [
                    'modelContactStudios' => $formFindStudio->find(),
                ])
            ];
            return $result;
        }
        $result['message'] = 'Error find studio.';
        return $result;
    }

    public function actionMapMarkers()
    {
        Yii::$app->response->format = Response::FORMAT_RAW;
        Yii::$app->response->headers->add('Content-Type', 'text/xml');

        $dom = new DOMDocument("1.0");
        $node = $dom->createElement("markers");
        $parnode = $dom->appendChild($node);

        $formFindStudio = new FindStudioForm();
        $formFindStudio->query = Yii::$app->request->get('query');

        if ($formFindStudio->validate()) {
            $modelContactStudios = $formFindStudio->find();
        } else {
            $modelContactStudios = ContactStudio::find()
                ->active()
                ->orderBy(['display_order' => SORT_ASC])
                ->all();
        }

        if (!$modelContactStudios) return $dom->saveXML();

        foreach ($modelContactStudios as $contactStudio) {
            $node = $dom->createElement("marker");
            $newnode = $parnode->appendChild($node);
            $newnode->setAttribute("id",$contactStudio->id);
            $newnode->setAttribute("name", $contactStudio->name);
            $newnode->setAttribute("description", $contactStudio->description);
            $newnode->setAttribute("address", $contactStudio->address);
            $newnode->setAttribute("lat", $contactStudio->lat);
            $newnode->setAttribute("lng", $contactStudio->lng);
            $newnode->setAttribute("type", $contactStudio->type);
        }

        return $dom->saveXML();
    }
}