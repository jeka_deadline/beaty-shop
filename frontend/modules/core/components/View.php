<?php

namespace frontend\modules\core\components;

use yii\web\View as YiiView;

/**
 * App main view class.
 */
class View extends YiiView
{
    /**
     * Body html tag class.
     *
     * @var string $bodyClass
     */
    public $bodyClass = '';
}
