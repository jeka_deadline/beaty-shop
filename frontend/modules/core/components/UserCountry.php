<?php

namespace frontend\modules\core\components;

use Yii;
use yii\base\Component;
use frontend\modules\geo\models\Country;
use frontend\modules\core\models\Setting;
use frontend\modules\product\models\OrderShippingInformation;

/**
 * User country component.
 */
class UserCountry extends Component
{
    /**
     * Country model
     *
     * @var frontend\modules\geo\models\Country $country
     */
    private static $country = null;

    /**
     * Get country by ip.
     *
     * @throws Exception If country German not found.
     * @return frontend\modules\geo\models\Country
     */
    public function getCountry()
    {
        if (!is_null(self::$country)) {
            return self::$country;
        }

        $information = json_decode(file_get_contents('http://getcitydetails.geobytes.com/GetCityDetails?fqcn=' . $this->getIP()));

        if (empty($information->geobytesinternet)) {
            $code = Country::GERMANY_COUNTRY_CODE;
        } else {
            $code = strtolower($information->geobytesinternet);
        }

        $country = Country::find()
            ->where(['like', 'code', $code])
            ->one();

        if (!$country) {
            $country = Country::find()
                ->where(['like', 'code', Country::GERMANY_COUNTRY_CODE])
                ->one();

            if (!$country) {
                throw new \Exception("Error Processing Request", 1);

            }
        }

        self::$country = $country;

        return $country;
    }

    /**
     * Get sum free shipping by country.
     *
     * @return null|float
     */
    public function getSumFreeShippingByCountry()
    {
        $country = $this->getCountry();

        if (!$country || strtolower($country->code) === Country::GERMANY_COUNTRY_CODE) {
            return Setting::getFreeShippingSettingValue();
        }

        if (strtolower($country->code) === Country::SWITZERLAND_COUNTRY_CODE) {
            return Setting::getFreeShippingSettingForSwitzerlandValue();
        }

        return Setting::getFreeShippingSettingForOtherCountriesValue();
    }

    /**
     * Get standart price for countries.
     *
     * @return float
     */
    public function getSumStandartShippingByCountry()
    {
        $country = $this->getCountry();

        if (!$country || strtolower($country->code) === Country::GERMANY_COUNTRY_CODE) {
            return OrderShippingInformation::SHIPPING_PRICE_STANDART_METHOD;
        }

        if (strtolower($country->code) === Country::SWITZERLAND_COUNTRY_CODE) {
            return OrderShippingInformation::SHIPPING_PRICE_SWITZERLAND_STANDART_METHOD;
        }

        return OrderShippingInformation::SHIPPING_PRICE_EU_COUNTRIES_STANDART_METHOD;
    }

    /**
     * Get user ip address.
     *
     * @access private
     * @return string
     */
    private function getIP()
    {
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key) {
            if (array_key_exists($key, $_SERVER) === true) {
                foreach (explode(',', $_SERVER[$key]) as $ip) {
                    if (filter_var($ip, FILTER_VALIDATE_IP) !== false) {
                        return $ip;
                    }
                }
            }
        }
    }
}
