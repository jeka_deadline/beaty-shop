<?php

namespace frontend\modules\core\assets;

use yii\web\AssetBundle;

/**
 * Asset for cart shop.
 */
class FindStudioAsset extends AssetBundle
{
    /**
     * {@inheritdoc}
     *
     * @var array $js
     */
    public $js = [
        'js/init-map.js',
        [
            'https://maps.googleapis.com/maps/api/js?key=AIzaSyAioiyPpDqMuUnz7g7mKiqX4BCtg6bqXSU&callback=initMap',
            'async' => 'async',
            'defer' => 'defer'
        ],
        'js/find-studio.js',
    ];

    /**
     * {@inheritdoc}
     *
     * @var array $depends
     */
    public $depends = [
        'yii\web\JqueryAsset',
    ];

    /**
     * {@inheritdoc}
     *
     * @var array $publishOptions
     */
    public $publishOptions = [
        'forceCopy' => (YII_DEBUG) ? true : false,
    ];

    /**
     * {@inheritdoc}. Set source path for this asset.
     *
     * @return void
     */
    public function init()
    {
        parent::init();

        $this->sourcePath = __DIR__ . DIRECTORY_SEPARATOR . 'src';
    }
}
