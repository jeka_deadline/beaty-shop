function initMap() {
    var customLabel = {
        restaurant: {
            label: 'R'
        },
        bar: {
            label: 'B'
        }
    };

    var latlngList = [];

    var map = new google.maps.Map(document.getElementById('map'), {});

    var infoWindow = new google.maps.InfoWindow;

    var query = '';
    if ($('#findstudioform-query').val() != '') query = '?query='+$('#findstudioform-query').val();

    // Change this depending on the name of your PHP or XML file
    downloadUrl('/core/find-studio/map-markers'+query, function(data) {
        var xml = data.responseXML;
        var markers = xml.documentElement.getElementsByTagName('marker');
        Array.prototype.forEach.call(markers, function(markerElem) {
            var id = markerElem.getAttribute('id');
            var name = markerElem.getAttribute('name');
            var address = markerElem.getAttribute('address');
            var type = markerElem.getAttribute('type');
            var description = markerElem.getAttribute('description');
            var point = new google.maps.LatLng(
                parseFloat(markerElem.getAttribute('lat')),
                parseFloat(markerElem.getAttribute('lng')));
            latlngList.push(point);

            var infowincontent = document.createElement('div');
            var strong = document.createElement('strong');
            strong.textContent = name;
            infowincontent.appendChild(strong);
            infowincontent.appendChild(document.createElement('br'));

            var text = document.createElement('div');
            text.innerHTML = '<div>'+description+'</div>';
            infowincontent.appendChild(text);
            var icon = customLabel[type] || {};
            var marker = new google.maps.Marker({
                map: map,
                position: point,
                label: icon.label,
                animation: google.maps.Animation.DROP,
                icon: '../img/map-pin.png',
            });
            marker.addListener('click', function() {
                infoWindow.setContent(infowincontent);
                infoWindow.open(map, marker);
            });
            $('.city-search-items-wrapper .item'+id).on("click", function () {
                map.setCenter(marker.getPosition());
                map.setZoom(16);
            });
        });
        var bounds = new google.maps.LatLngBounds();
        Array.prototype.forEach.call(latlngList, function(n){
            bounds.extend(n);
        });
        map.setCenter(bounds.getCenter());
        map.fitBounds(bounds);
        if (latlngList.length == 1) map.setZoom(16);
    });
}

function downloadUrl(url, callback) {
    var request = window.ActiveXObject ?
        new ActiveXObject('Microsoft.XMLHTTP') :
        new XMLHttpRequest;

    request.onreadystatechange = function() {
        if (request.readyState == 4) {
            request.onreadystatechange = doNothing;
            callback(request, request.status);
        }
    };

    request.open('GET', url, true);
    request.send(null);
}

function doNothing() {}