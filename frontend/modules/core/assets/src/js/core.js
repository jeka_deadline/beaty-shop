(function($) {
    $.fn.serializefiles = function() {
        var obj = $(this);
        var formData = new FormData();
        $.each($(obj).find("input[type='file']"), function(i, tag) {
            $.each($(tag)[0].files, function(i, file) {
                formData.append(tag.name, file);
            });
        });
        var params = $(obj).serializeArray();
        $.each(params, function (i, val) {
            formData.append(val.name, val.value);
        });
        return formData;
    };
})(jQuery);

$(function() {

    let flagSubmitFormSubscribe = false;
    $(document).on('submit', '#subscribe-form', function( e ) {
        e.preventDefault();
        if (flagSubmitFormSubscribe) return false;
        flagSubmitFormSubscribe = true;

        var form = $(this);

        var button = form.find('button[type=submit]');

        button.attr('disabled', 'disabled');

        $.ajax({
            url: $(form).attr('action'),
            method: $(form).attr('method'),
            data: $(form).serialize(),
            dataType: 'json'
        }).done(function(data) {
            if (data.status) {
                $('#footer-popup .subscribe-text').html(data.text);
                $('#footer-popup').css('bottom', '0');
                $(form)[0].reset();
                setTimeout(
                    function () {
                      $('#footer-popup').css('bottom', '-100%');
                    },
                    5000
                );
            } else {
                $(form).replaceWith(data.form);
            }
            flagSubmitFormSubscribe = false;
            $(button).removeAttr('disabled', 'disabled');
        });
    });

    // submit signup form
    let flagSubmitSignupForm = false;
    $(document).on('submit', '#signup-form', function( e ) {
        e.preventDefault();
        if (flagSubmitSignupForm) return false;
        flagSubmitSignupForm = true;

        var form = $(this);
        var button = $(this).find('button');
        $(button).attr('disabled', 'disabled');

        $.ajax({
            url: $(this).attr('action'),
            method: $(this).attr('method'),
            data: $(this).serialize(),
            dataType: 'json',
        }).done(function(data) {

            showModal(data.text);

            if (data.status) {
                setTimeout(function(){ location.href='/'; }, 2000);
            } else {
                $(button).removeAttr('disabled', 'disabled');
            }
            flagSubmitSignupForm = false;
        })
    });

    // submit contact us form
    let flagSubmitContactUsForm = false;
    $(document).on('submit', '#contact-us-form', function( e ) {
        e.preventDefault();
        if (flagSubmitContactUsForm) return false;
        flagSubmitContactUsForm = true;

        var form = $(this);
        var button = $(this).find('button');
        $(button).attr('disabled', 'disabled');
        var formData = form.serializeEditFiles();

        $.ajax({
            url: form.attr("action"),
            type: form.attr("method"),
            processData: false,
            contentType: false,
            data: formData,
        }).done(function(data) {

            showModal(data.text);

            if (data.status) {
                setTimeout(function(){ location.reload(); }, 2000);
            } else {
                $(button).removeAttr('disabled', 'disabled');
            }
            flagSubmitContactUsForm = false;
        });
    });

    $('body').on('click', '.close-cookies', function (event) {
        $.get('/core/index/cookies-policy', function(data) {

        });
    });

    $('body').on('click', '.regenerate-token', function(e) {
        e.preventDefault();

        var parent = $(this).parent();

        if ($(document).find('#preloader').length) {
            $(parent).css({display: 'none'});
            $(document).find('#preloader').css({display: 'block'});
        }
        $.ajax({
            url: $(this).data('url'),
            method: $(this).data('method'),
            dataType: 'json',
            headers: {
                'X-CSRF-Token': yii.getCsrfToken(),
            },
        }).done(function(data) {
            $(document).find('#core-modal .modal-body').html(data.text);

            $(document).find('#core-modal').modal();

            if (data.status) {
                setTimeout(function() {
                    location.href = '/';
                }, 3000);
            } else {
                if ($(document).find('#preloader').length) {
                    $(parent).css({display: 'block'});
                    $(document).find('#preloader').css({display: 'none'});
                }
            }
        });
    });
});

function showModal(modalText) {
    let modalBody = $(document).find('#core-modal .modal-body');

    if ($(modalBody).length) {
        $(modalBody).html(modalText);

        $(document).find('#core-modal').modal();
    }
}