$(function() {
    function sendAjax(form, modal_id) {
        $(form).find('button[type=submit]').attr('disabled', 'disabled');

        var formData = form.serializeEditFiles();
        $.ajax({
            url: form.attr("action"),
            type: form.attr("method"),
            processData: false,
            contentType: false,
            data: formData,
            success: function (response) {
                $(modal_id).modal('hide');
                $(form).find('input:text, input:password, input:file, select, textarea').val('');
                $(form).find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
                $(form).find('div[data-for]').each(function( index ) {
                    var id = $(this).attr('data-for');
                    if (!id) return;
                    delete inputFilesStore[id];
                });
                $(form).find('div[data-for]').empty();

                $(form).find('button[type=submit]').removeAttr('disabled', 'disabled');

                showModal(response.message);
                
                switch (modal_id) {
                    case '#trainer-modal':
                        flagSubmitFormTrainer = false;
                        break;
                    case '#distributor-modal':
                        flagSubmitFormDistributor = false;
                        break;
                }
            },
            error: function () {
            }
        });
    }

    let flagSubmitFormTrainer = false;
    $(document).on("beforeSubmit", "#form-trainer", function (e) {
        if (flagSubmitFormTrainer) return false;
        flagSubmitFormTrainer = true;

        var form = $(this);

        sendAjax(form, '#trainer-modal');
    }).on('submit', function(e){
        e.preventDefault();
    });

    let flagSubmitFormDistributor = false;
    $(document).on("beforeSubmit", "#form-distributor", function (e) {
        if (flagSubmitFormDistributor) return false;
        flagSubmitFormDistributor = true;

        var form = $(this);

        sendAjax(form, '#distributor-modal');
    }).on('submit', function(e){
        e.preventDefault();
    });
});