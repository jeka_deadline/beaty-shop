$(function() {
    var getAjaxDataFindStudio = function () {
        var form = $('#form-search-studio');
        $.ajax({
            url: $(form).attr('action'),
            method: $(form).attr('method'),
            data: $(form).serialize(),
            dataType: 'json'
        }).done(function(data) {
            if (data.status == 'ok') {
                $('.find-studio-page .city-search-items-wrapper').html(data.html);
                initMap();
            } else {
                $('.find-studio-page .city-search-items-wrapper').empty();
            }
        });

    };

    $(document).on('keyup', '#findstudioform-query', function(e) {
        if ($('#findstudioform-query').val() != '') getAjaxDataFindStudio();
    });
    $(document).on('submit', '#form-search-studio', function(e) {
        e.preventDefault();
        if ($('#findstudioform-query').val() != '') getAjaxDataFindStudio();
    });
});

