<?php

namespace frontend\modules\core\behaviors;

use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;

class LangBehavior extends Behavior
{
    /**
     * Name field in table which relation model and lang fields
     *
     * @var string
     */
    public $modelForeignKey;

    /**
     * Name field in table which relation lang fields and lang
     *
     * @var string
     */
    public $langForeingKey = 'lang_id';

    /**
     * Language model for model
     *
     * @var \yii\db\ActiveRecord
     */
    public $langModel;

    /**
     * List attributes which translated
     *
     * @var array
     */
    public $attributes;

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_FIND => 'afterFind',
        ];
    }

    /**
     * Method which run after find ActiveRecord and set lang attributes
     *
     * @param $event
     * @return void
     */
    public function afterFind($event)
    {
        $session = Yii::$app->session;

        $language = $session['language'];

        $currentModel = $this->owner;
        $langModelQuery = call_user_func([$this->langModel, 'find']);

        $langModel = $langModelQuery->where([
            $this->modelForeignKey => $currentModel->id,
            $this->langForeingKey => $language['id']
        ])->one();

        foreach ($this->attributes as $attributeName) {
            if (!empty($langModel->{$attributeName})) {
                $this->owner->setAttribute($attributeName, $langModel->{$attributeName});
            }
        }
    }
}
