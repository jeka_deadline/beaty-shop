<?php

use yii\helpers\Url;

/** @var \frontend\modules\core\components\View $this */
/** @var \frontend\modules\core\models\Subscriber $model */
?>

<div class="footer__one-for-subscribe-wrapper row">
    <div class="footer__one-text-wrapper col-xl-4 offset-xl-2 col-lg-5 offset-lg-1 col-md-10 offset-md-1 col-sm-12 col-12">
        <div class="footer__one-header"><?= Yii::t('core', 'Let’s keep the conversation going'); ?></div>
        <div class="footer__one-text"><?= Yii::t('core', 'Receive our newsletter and discover our products and surprises.'); ?></div>
    </div>
    <div class="col-xl-4 offset-xl-0 col-lg-5 offset-lg-0 col-md-10 offset-md-1 col-sm-12 col-12">

        <?= $this->render('subscribe-form', compact('model')); ?>

    </div>
</div>
<div class="row">
    <div class="footer__one-footer col-xl-8 offset-xl-2 col-lg-10 offset-lg-1 col-md-10 offset-md-1 col-sm-12 col-12"><?= Yii::t('core', 'By subscribing to our newsletter, you agree that your data will be processed in compliance with our <a href="{0}">General Terms and Conditions of Use</a> and our <a href="{1}">Privacy Policy</a>.', [
        Url::toRoute(['/pages/index/page', 'uri' => 'terms']),
        Url::toRoute(['/pages/index/page', 'uri' => 'cookie'])
      ]); ?></div>
</div>
