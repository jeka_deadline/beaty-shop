<?php

?>

<?php foreach ($categories as $category) : ?>
    <?php if (in_array($category->display_order, $range)): ?>
        <?php if ($category instanceof stdClass): ?>
            <li>
                <a href="<?= $category->url ?>"><?= $category->name ?></a>
            </li>
        <?php else: ?>
            <li>
                <a href="<?= $category->getUrl() ?>"><?= $category->name ?></a>
            </li>
        <?php endif; ?>
    <?php endif; ?>
<?php endforeach; ?>