<?php foreach ($categories as $category) : ?>

    <div class="<?= isset($class)?$class:''?>">
        <?php if (is_array($category)): ?>

            <h5><a href="<?= $category['url'] ?>"><?= $category['name'] ?></a></h5>

        <?php else: ?>

            <h5><a href="<?= $category->getUrl() ?>"><?= $category->name ?></a></h5>

        <?php endif; ?>

    </div>

<?php endforeach; ?>
