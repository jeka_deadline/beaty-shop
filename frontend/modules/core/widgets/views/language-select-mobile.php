<?php

use frontend\widgets\bootstrap4\ActiveForm;
use frontend\modules\core\models\forms\LanguageSelectForm;
use frontend\widgets\bootstrap4\Html;

?>

<?php $form = ActiveForm::begin([
    'action' => ['/core/index/language'],
    'enableAjaxValidation' => false,
    'enableClientValidation' => false,
]); ?>
    <p><?= $languageForm->getAttributeLabel('language') ?></p>
    <?= $form->field($languageForm, 'language', [
        'template'=>'{input}',
        'options' => [
            'tag' => false,
        ],
    ])->dropDownList(LanguageSelectForm::getListLanguage(),[
        'class' => 'languageSelect',
        'id' => false,
    ])->label(false) ?>

    <?= Html::submitButton('save', ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>