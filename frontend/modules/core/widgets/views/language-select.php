<?php

use frontend\widgets\bootstrap4\ActiveForm;
use frontend\modules\core\models\forms\LanguageSelectForm;
use frontend\widgets\bootstrap4\Html;

?>

<div class="header_right-block_language" id="dropdownMenuLanguage"><a class="" href="#" title="Language"><span><?= $infoSelectLang?$infoSelectLang->name:'' ?></span>
        <svg class="lang svg">
            <use xlink:href="#lang"></use>
        </svg></a>
    <div class="dropdown-menu-change">
        <?php $form = ActiveForm::begin([
            'action' => ['/core/index/language'],
            'enableAjaxValidation' => false,
            'enableClientValidation' => false,
        ]); ?>
            <p><?= $languageForm->getAttributeLabel('language') ?></p>
            <?= $form->field($languageForm, 'language', [
                'template'=>'{input}',
                'options' => [
                    'tag' => false,
                ],
            ])->dropDownList(LanguageSelectForm::getListLanguage(), [
                    'class' => 'languageSelect',
            ])->label(false) ?>

            <?= Html::submitButton(Yii::t('core', 'Save'), ['class' => 'btn btn-primary']) ?>
        <?php ActiveForm::end(); ?>
    </div>
</div>