<?php

?>

<?php if ($slogans): ?>

    <div class="heaeder_cener" data-position="0">
        <?php if (isset($slogans[0])): ?>
            <?= $slogans[0]->slogan ?>
        <?php endif; ?>
    </div>

    <div class="d-none model-list-slogan">
        <?php foreach ($slogans as $slogan): ?>
            <div><?= $slogan->slogan ?></div>
        <?php endforeach; ?>
    </div>

<?php endif; ?>