<?php

use frontend\modules\core\models\Setting;
use yii\helpers\Url;

?>
<div class="header__hover-menu container-fluid">
    <div class="container">
        <div class="row shop-hover header-hover-hide header__shop header-hover-show">

            <div class="header__hover-items-wrapper col-xl-5 col-lg-4">
                <div class="header__hover-items">
                    <ul>
                        <?= $this->render('header_menu/_category_items', [
                            'categories' => $categories,
                            'range' => range(10, 19)
                        ]); ?>
                    </ul>
                </div>
                <div class="header__hover-items">
                    <ul>
                        <?= $this->render('header_menu/_category_items', [
                            'categories' => $categories,
                            'range' => range(20, 29)
                        ]); ?>
                    </ul>
                </div>
            </div>
            <div class="header__hover-items-wrapper col-xl-5 col-lg-4">
                <div class="header__hover-items">
                    <ul>
                        <?= $this->render('header_menu/_category_items', [
                            'categories' => $categories,
                            'range' => range(30, 39)
                        ]); ?>
                    </ul>
                </div>
                <div class="header__hover-items">
                    <ul>
                        <?= $this->render('header_menu/_category_items', [
                            'categories' => $categories,
                            'range' => range(40, 49)
                        ]); ?>
                    </ul>
                </div>
            </div>

            <a href="<?= Setting::value('top.menu.shop.link') ?>" class="col-xl-3 header__hover-img"><img src="<?= Setting::value('top.menu.shop.image') ?>" alt="hover-menu-image"></a>
        </div>

        <?php if (Setting::value('academy.enable')!=0): ?>
            <div class="row academy-hover header-hover-hide header__academy">
                <?php if ($courses): ?>
                    <div class="header__hover-items col-xl-3 offset-xl-3">
                            <ul>
                                <?php foreach ($courses as $index => $course): ?>
                                    <li><a class="course-<?= $index+1 ?>" href="<?= Url::toRoute(['/course/'.$course->slug]); ?>"><?= $course->name ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        <a class="btn btn-primary" href="<?= Url::toRoute(['/academy']); ?>">all courses</a>
                    </div>
                    <div class="col-xl-3 header__hover-img">
                        <?php foreach ($courses as $index => $course): ?>
                            <img class="course-<?= $index+1 ?> header__hover-img-hide<?php if ($index==0): ?> header__hover-img-active<?php endif;?>" src="<?= $course->urlSmallImage ?>" alt="hover-menu-image">
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    </div>
</div>