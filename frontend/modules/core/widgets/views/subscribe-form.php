<?php

use frontend\widgets\bootstrap4\ActiveForm;

/** @var \frontend\modules\core\components\View $this */
/** @var \frontend\modules\core\models\Subscriber $model */
/** @var \yii\widgets\ActiveForm $form */
?>

<?php $form = ActiveForm::begin([
    'action' => ['/core/subscribe/subscribe'],
    'id' => 'subscribe-form',
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'validationUrl' => ['/core/subscribe/ajax-validate-subscribe-model']
]); ?>

    <?= $form->field($model, 'email', [
        'errorOptions' => ['encode' => false],
    ])->textInput(['placeholder' => 'E-mail'])
        ->label(false);
    ?>

    <?= $form->field($model, 'agreeSubscribe', [
        'options' => [
            'class' => 'form-check grey-form',
        ],
        'checkboxTemplate' => '<div class="custom-control custom-checkbox">{input}{label}</div>',
        'errorOptions' => ['encode' => false],
    ])->checkbox(
        ['class' => 'custom-control-input footerSubscribeAgree', 'id' => 'footerSubscribeAgree'],
        ['class' => 'custom-control-label',]
    )->label(''); ?>

    <button class="btn btn-primary footer-subscribe" form="subscribe-form" type="submit"><?= Yii::t('core', 'Subscribe') ?></button>

<?php ActiveForm::end(); ?>
