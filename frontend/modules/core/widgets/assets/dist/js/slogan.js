$(function() {
    var listSlogan = [];

    $('.model-list-slogan div').each(function (index, el) {
        listSlogan.push($(el).text());
    });

    setInterval(function() {
        var heaederCener = $('.heaeder_cener');
        if (!heaederCener.length) return;

        var position = parseInt(heaederCener.attr('data-position'));
        position++;
        if (position>=listSlogan.length) position = 0;

        heaederCener.text(listSlogan[position]);
        heaederCener.attr('data-position', position);
    }, 20000);
});