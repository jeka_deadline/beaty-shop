$(function() {
    /**
     * AJAX Load the next page in the directory.
     */
    $(document).on('click', '.load-next-page', function(e) {
        var currentPage = parseInt($('.pagination .active:last').text());
        var nextPage = $('.pagination li:not(.next) a[data-page='+currentPage+']');
        var target = e.target;

        if (!nextPage.length) return;

        var nextPageHref = nextPage.attr('href');
        if (nextPageHref == null) return;

        $.get(nextPageHref, function(data) {
            window.history.pushState(null, null, nextPageHref);
            $('.list-block').append(data);

            var li = nextPage.parent('li');
            li.addClass('active disabled');
            li.html('<span>'+li.text()+'</span>');

            var pageNext = li.next().find('a');
            var aNext = $('.pagination li.next a');

            if (!pageNext.length) return;

            if (currentPage == pageNext.attr('data-page')) {
                $(target).hide(); // hidden buton more
                li.next()
                    .addClass('active disabled')
                    .html('<span>»</span>')
            } else {
                aNext.attr('href', pageNext.attr('href'));
                aNext.attr('data-page', pageNext.attr('data-page'));
            }
        });
    });

});