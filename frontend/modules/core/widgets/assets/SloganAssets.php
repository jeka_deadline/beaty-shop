<?php

namespace frontend\modules\core\widgets\assets;

use yii\web\AssetBundle;

class SloganAssets extends AssetBundle
{
    public $sourcePath = '@frontend/modules/core/widgets/assets/dist';

    public $css = [
    ];

    public $js = [
        'js/slogan.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
        'frontend\widgets\bootstrap4\BootstrapAsset',
        'frontend\widgets\bootstrap4\BootstrapPluginAsset',
    ];

    public $publishOptions = [
        'forceCopy' => (YII_DEBUG) ? true : false,
    ];

}