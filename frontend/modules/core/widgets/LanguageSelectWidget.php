<?php

namespace frontend\modules\core\widgets;

use frontend\modules\core\models\forms\LanguageSelectForm;
use yii\base\Widget;

class LanguageSelectWidget extends Widget
{
    public $template = 'language-select';

    /**
     * {@inheritdoc}
     *
     * @return mixed
     */
    public function run()
    {
        $languageForm = new LanguageSelectForm();

        $infoSelectLang = $languageForm->infoSelectLang;

        return $this->render($this->template, [
            'languageForm' => $languageForm,
            'infoSelectLang' => $infoSelectLang
        ]);
    }
}
