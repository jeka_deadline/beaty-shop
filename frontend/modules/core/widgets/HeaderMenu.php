<?php

namespace frontend\modules\core\widgets;

use frontend\modules\academy\models\Academy;
use Yii;
use yii\base\Widget;
use frontend\modules\product\models\ProductCategory;
use yii\helpers\Url;

class HeaderMenu extends Widget
{
    public static $categories = null;

    public static function getAdditionalItems() {
        return [
            [
                'name' => Yii::t('product', 'New Arrivals'),
                'url' => Url::toRoute(['/product/product-category/news']),
                'display_order' => 10
            ],
        ];
    }

    public function run()
    {
        $categories = self::getBlockCategories();

        $courses = Academy::find()
            ->andWhere(['>=', 'date', date('Y-m-d H:i:s')])
            ->active()
            ->orderBy('display_order')
            ->limit(4)
            ->all();

        return $this->render('header-menu', compact('categories', 'courses'));
    }

    public static function getBlockCategories() {
        if (is_null(self::$categories)) {
            $categories = ProductCategory::find()
                ->orderBy('display_order_hmenu') // TO-DO After making changes, this field is not used.
                ->with('children')
                ->where(['parent_id' => 0])
                ->active()
                ->orderBy('display_order')
                ->all();

            self::$categories = self::glueMenu($categories);
        }
        return self::$categories;
    }

    public static function glueMenu($categories) {
        $additionalItems = self::getAdditionalItems();
        foreach ($additionalItems as $item) {
            array_push($categories, self::arrayToObject($item));
        }
        usort($categories, function ($a, $b) {
            if ($a->display_order == $b->display_order) {
                return 0;
            }
            return ($a->display_order < $b->display_order) ? -1 : 1;
        });
        return $categories;
    }

    public static function arrayToObject($el) {
        $class = new \stdClass();
        foreach ($el as $key => $value) {
            $class->$key = $value;
        }
        return $class;
    }
}