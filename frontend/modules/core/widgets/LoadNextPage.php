<?php

namespace frontend\modules\core\widgets;

use frontend\modules\core\widgets\assets\LoadNextPageAssets;
use yii\base\Widget;


class LoadNextPage extends Widget
{
    public $dataProvider = null;
    public $template = 'load-next-page';

    public function init() {
        parent::init();
        $this->registerAssets();
    }

    public function run()
    {
        $visible = false;
        if ($this->dataProvider) {
            $this->dataProvider->prepare();
            $pageCount = $this->dataProvider->pagination->pageCount;
            $currentPage = $this->dataProvider->pagination->page;
            if ($pageCount > 1 && $pageCount > ($currentPage+1)) $visible = true;
        }
        return $this->render($this->template, [
            'visible' => $visible
        ]);
    }

    public function registerAssets() {
        $view=$this->getView();
        LoadNextPageAssets::register($view);
    }
}