<?php

namespace frontend\modules\core\widgets;

use frontend\modules\core\widgets\assets\SloganAssets;
use yii\base\Widget;
use frontend\modules\core\models\Slogan as SloganModel;

class Slogan extends Widget
{
    public $template = 'slogan';

    public function init() {
        parent::init();
        $this->registerAssets();
    }

    /**
     * {@inheritdoc}
     *
     * @return mixed
     */
    public function run()
    {
        $modelSlogans = SloganModel::find()
            ->active()
            ->orderBy(['display_order' => SORT_ASC])
            ->all();

        return $this->render($this->template, [
            'slogans' => $modelSlogans
        ]);
    }

    public function registerAssets() {
        $view=$this->getView();
        SloganAssets::register($view);
    }
}
