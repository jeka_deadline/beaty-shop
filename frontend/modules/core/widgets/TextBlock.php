<?php

namespace frontend\modules\core\widgets;

use frontend\modules\core\models\TextBlock as TextBlockModel;
use yii\base\Widget;
use yii\helpers\HtmlPurifier;

/**
 * Widget which return string content for active text block.
 */
class TextBlock extends Widget
{
    /**
     * Code identity text block.
     *
     * @var string
     */
    public $code;

    /**
     * {@inheritdoc}
     *
     * @return null|string
     */
    public function run()
    {
        $textBlock = TextBlockModel::find()
            ->where(['code' => $this->code])
            ->active()
            ->one();

        if (!$textBlock) {
            return null;
        }

        return HtmlPurifier::process($textBlock->content);
    }
}
