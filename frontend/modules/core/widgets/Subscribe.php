<?php

namespace frontend\modules\core\widgets;

use yii\base\Widget;
use frontend\modules\core\models\Subscriber;

/**
 * Widget for show subscribe form.
 */
class Subscribe extends Widget
{
    /**
     * {@inheritdoc}
     *
     * @return mixed
     */
    public function run()
    {
        $model = new Subscriber();

        return $this->render('subscribe', compact('model'));
    }
}
