<?php

use frontend\modules\core\assets\TrainerDistributorAsset;

TrainerDistributorAsset::register($this);

$this->bodyClass = 'tr-dist-page';

?>
<div class="img-and-text__head container-fluid">
    <div class="img-and-text__main-img row" style="background: #000; background-image: none;">
        <h1 class="img-and-text__main-text"><?= Yii::t('user', 'trainer / distrubutor'); ?></h1>
        <div class="img-and-text__text"><?= Yii::t('user', 'We need reliable partners'); ?></div>
    </div>
</div>
<div class="container">
    <section>
        <div class="row">
            <div class="tr-dist-page__main col-xl-8 offset-xl-2 col-lg-10 offset-lg-1">
                <div class="tr-dist-page__info-par">
                    <p><?= Yii::t('user', 'Infinity Lashes is growing fast. With increasing interest in our courses and products we have now expanded into Europe. If you are interested in cooperation with us fill out the form below.'); ?></p>
                </div>
            </div>
        </div>
        <div class="tr-dist-page__blocks-wrapper row">
            <div class="tr-dist-page__trainer-block-wrapper col-xl-5 offset-xl-1 offset-lg-0 col-lg-6 col-md-8 offset-md-2">
                <div class="tr-dist-page__trainer-block">
                    <svg class="trainer svg">
                        <use xlink:href="#trainer"></use>
                    </svg>
                    <p><?= Yii::t('user', 'Trainer'); ?></p>
                    <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#trainer-modal"><?= Yii::t('core', 'Become a trainer') ?></button>
                </div>
            </div>
            <div class="tr-dist-page__distributor-block-wrapper offset-xl-0 col-xl-5 col-lg-6 offset-lg-0 col-md-8 offset-md-2">
                <div class="tr-dist-page__distributor-block">
                    <svg class="distribution svg">
                        <use xlink:href="#distribution"></use>
                    </svg>
                    <p><?= Yii::t('user', 'Distributor'); ?></p>
                    <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#distributor-modal"><?= Yii::t('core', 'Become a distributor') ?></button>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="svgHide"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="0" height="0" style="position:absolute"><symbol id="trainer" viewBox="0 0 19 32"><title>trainer</title><g data-name="Layer 2"><g data-name="Layer 1"><g id="Page-1"><g id="icon-137-document-certificate"><path d="M13 0H2a2 2 0 0 0-2 2v23a2 2 0 0 0 2 2h8v5l2.5-2.5L15 32v-5h2a2 2 0 0 0 2-2V7l-6-7zM2 26a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h10v5a2 2 0 0 0 2 2h4v17a1 1 0 0 1-1 1h-2v-1.05a3.5 3.5 0 1 0-5 0V26zM14 7a1 1 0 0 1-1-1V1.5L17.7 7zm-1.5 18a2.5 2.5 0 1 1 2.5-2.5 2.5 2.5 0 0 1-2.5 2.5zm0 1a3.49 3.49 0 0 0 1.5-.34v3.94l-1.5-1.5-1.5 1.5v-3.94a3.49 3.49 0 0 0 1.5.34zM10 4H3v1h7V4zm0 3H3v1h7V7zm6 3H3v1h13v-1zm0 3H3v1h13v-1zm0 3H3v1h13v-1z" id="document-certificate"></path></g></g></g></g></symbol><symbol id="distribution" viewBox="0 0 490.1 490.2"><title>Distribution</title><g data-name="Layer 2"><path d="M416.2 344.2a74.76 74.76 0 0 0-41.2 12.2l-37.3-36.9a116.12 116.12 0 0 0-1.4-147.9l38.4-38a74.76 74.76 0 0 0 41.4 12.4c40 0 73.8-32.3 73.8-73S457.2 0 416.1 0s-73.8 32.3-73.8 73a71.86 71.86 0 0 0 16.7 46.5l-37.7 37.3a116 116 0 0 0-164 162.5L118 358.6a74.66 74.66 0 0 0-44.2-14.4c-41.1 0-73.8 32.4-73.8 73s32.7 73 73.8 73c40 0 73.8-32.3 73.8-73a72.38 72.38 0 0 0-14.6-43.9l39.2-38.8a115.95 115.95 0 0 0 151 .1l36.2 35.8a72.3 72.3 0 0 0-16.9 46.8c0 40.7 32.7 73 73.8 73 40 0 73.8-32.3 73.8-73s-32.8-73-73.9-73zm0-323.1c28.5 0 52.7 22.9 52.7 52.1s-23.2 52.1-52.7 52.1-52.7-22.9-52.7-52.1 23.2-52.1 52.7-52.1zM73.8 469.3c-29.5 0-52.7-22.9-52.7-52.1s23.2-52.1 52.7-52.1c28.5 0 52.7 22.9 52.7 52.1s-23.2 52.1-52.7 52.1zm78.8-222.8a95.1 95.1 0 1 1 95.1 95.1 95.13 95.13 0 0 1-95.1-95.1zm263.6 222.8c-29.5 0-52.7-22.9-52.7-52.1s23.2-52.1 52.7-52.1c28.5 0 52.7 22.9 52.7 52.1s-23.2 52.1-52.7 52.1z" data-name="Layer 1"></path></g></symbol></svg></div>

<?= $this->render('forms/_trainer_form', compact(
    'formTrainer'
)); ?>

<?= $this->render('forms/_distributor_form', compact(
    'formDistributor'
)); ?>