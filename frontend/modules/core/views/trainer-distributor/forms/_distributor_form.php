<?php

use frontend\modules\user\models\User;
use frontend\widgets\bootstrap4\ActiveForm;
use frontend\widgets\bootstrap4\Html;
use yii\helpers\Url;
use yii\widgets\MaskedInput;

?>

<div class="modal fade" id="distributor-modal" tabindex="-1" role="dialog" aria-labelledby="distributor-modal-window">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <svg class="distribution-black svg">
                    <use xlink:href="#distribution"></use>
                </svg>
                <h5 class="modal-title" id="distributor-modal-window"><?= Yii::t('core', 'Become a distributor') ?></h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <?php $form = ActiveForm::begin([
                    'id' => 'form-distributor',
                    'action' => Url::toRoute('send-distributor'),
                    'enableClientValidation' => true,
                    'enableAjaxValidation' => false,
                    'method' => 'post',
                    'options' => [
                        'enctype' => 'multipart/form-data',
                        'class' => 'grey-form'
                    ]
                ]); ?>

                    <?= $form->field($formDistributor, 'sex')->dropDownList(User::getListUserTitles(), ['prompt'=>'']) ?>

                    <?= $form->field($formDistributor, 'first_name')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($formDistributor, 'last_name')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($formDistributor, 'company')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($formDistributor, 'email')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($formDistributor, 'phone')->widget(MaskedInput::className(), [
                        'mask' => $formDistributor->getGermanPhoneMask(),
                        'options' => [
                            'class' => 'form-control',
                            'required' => true,
                        ],
                    ]) ?>

                    <?= $form->field($formDistributor, 'address')->textInput() ?>

                    <?= $form->field($formDistributor, 'city')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($formDistributor, 'country')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($formDistributor, 'tax_number_1')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($formDistributor, 'tax_number_2')->textInput(['maxlength' => true]) ?>

                    <p><?= Yii::t('core', 'You can attach your resume or other documents. The file size should not be more than 5MB') ?></p>
                    <div data-for="<?= Html::getInputId($formDistributor, 'files[]')?>"></div>
                    <div class="file__attach">
                        <?= $form->field($formDistributor, 'files[]')->fileInput([
                            'multiple' => true,
                            'class' => 'd-none load-file-edit-list'
                        ])->label(false) ?>
                        <svg class="attach svg">
                            <use xlink:href="#attach"></use>
                        </svg>
                        <label for="distributorform-files"><?= Yii::t('core', 'Attach file') ?></label>
                    </div>
                    <div class="modal-footer">
                        <?= Html::submitButton(Yii::t('core', 'Submit'), ['class' => 'btn btn-primary']) ?>
                    </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>