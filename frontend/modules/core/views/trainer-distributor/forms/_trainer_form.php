<?php

use frontend\modules\user\models\User;
use frontend\widgets\bootstrap4\ActiveForm;
use frontend\widgets\bootstrap4\Html;
use yii\helpers\Url;
use yii\widgets\MaskedInput;

?>
<div class="modal fade" id="trainer-modal" tabindex="-1" role="dialog" aria-labelledby="trainer-modal-window">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <svg class="trainer-black svg">
                    <use xlink:href="#trainer"></use>
                </svg>
                <h5 class="modal-title" id="trainer-modal-window"><?= Yii::t('core', 'Become a trainer') ?></h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <?php $form = ActiveForm::begin([
                    'id' => 'form-trainer',
                    'action' => Url::toRoute('send-trainer'),
                    'enableClientValidation' => true,
                    'enableAjaxValidation' => false,
                    'method' => 'post',
                    'options' => [
                        'enctype' => 'multipart/form-data',
                        'class' => 'grey-form'
                    ]
                ]); ?>

                    <?= $form->field($formTrainer, 'sex')->dropDownList(User::getListUserTitles(), ['prompt'=>'']) ?>

                    <?= $form->field($formTrainer, 'first_name')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($formTrainer, 'last_name')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($formTrainer, 'phone')->widget(MaskedInput::className(), [
                        'mask' => $formTrainer->getGermanPhoneMask(),
                        'options' => [
                            'class' => 'form-control',
                            'required' => true,
                        ],
                    ]) ?>

                    <?= $form->field($formTrainer, 'company')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($formTrainer, 'email')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($formTrainer, 'country')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($formTrainer, 'city')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($formTrainer, 'address')->textInput() ?>

                    <p><?= Yii::t('core', 'You can attach your resume or other documents. The file size should not be more than 5MB') ?></p>
                    <div data-for="<?= Html::getInputId($formTrainer, 'files[]')?>"></div>
                    <div class="file__attach">
                        <?= $form->field($formTrainer, 'files[]')->fileInput([
                            'multiple' => true,
                            'class' => 'd-none load-file-hide load-file-edit-list'
                        ])->label(false) ?>
                        <svg class="attach svg">
                            <use xlink:href="#attach"></use>
                        </svg>
                        <label for="trainerform-files"><?= Yii::t('core', 'Attach file') ?></label>
                    </div>
                    <div class="modal-footer">
                        <?= Html::submitButton(Yii::t('core', 'Submit'), ['class' => 'btn btn-primary']) ?>
                    </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>