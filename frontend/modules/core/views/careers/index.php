<?php

$this->bodyClass = 'careers-page';

?>

<div class="added-to-cart-wrapper">Item has been added to your bag</div>
<div class="img-and-text__head container-fluid">
    <div class="img-and-text__main-img row">
        <h1 class="img-and-text__main-text">jobs</h1>
        <div class="img-and-text__text">Wir suchen Mitarbeiter zur Verstärkung unseres Teams</div>
    </div>
</div>
<h2>Werde Teil der Infinity Lashes Familie!</h2>
<div class="container">
    <div class="row">
        <div class="careers-main-header-description offset-xl-2 col-xl-8 offset-lg-1 col-lg-10" style="text-align: left;">
            <ul style="list-style-type:circle">
                <li>Du hast eine gro&szlig;e Leidenschaft f&uuml;r Kosmetik und &Auml;sthetik.</li>
                <li>Du bist offen und kontaktfreudig.</li>
                <li>Dir macht es Spa&szlig; im Team zu arbeiten und du kannst fachgerecht die Kunden beraten.</li>
                <li>Du m&ouml;chtest das Gesicht und Herz unserer Marke sein.</li>
            </ul>

            <p style="margin-left:21.3pt">Dann bist du genau die Person, die wir suchen f&uuml;r die Verst&auml;rkung unseres jungen und motivierten Teams in unserem sch&ouml;nen modernen Studio in Dortmund-<a name="_GoBack"></a>City.</p>

            <p style="margin-left:21.3pt">Wir suchen:</p>

            <ul style="list-style-type:circle">
                <li>Kosmetiker/innen</li>
                <li>Wimpernstylist/innen</li>
                <li>Nageldesigner/innen</li>
            </ul>

            <p>&nbsp;</p>

            <h3><strong>Wir bieten an: </strong></h3>

            <ul>
                <li>Interne Aus- und Weiterbildungen</li>
                <li>Flexible Arbeitszeiten</li>
                <li>&Uuml;berdurchschnittliche Bezahlung</li>
                <li>Bonuszahlungen</li>
                <li>Harmonisches und famili&auml;res Team</li>
                <li>Aufstiegs- bzw. Karrierem&ouml;glichkeiten</li>
            </ul>

            <p>&nbsp;</p>

            <h3><strong>Die Anforderungen:</strong></h3>

            <ul>
                <li>Abgeschlossene Ausbildung oder Quereinsteiger mit Fachkenntnissen</li>
                <li>Freundliches Auftreten</li>
                <li>Lernbereitschaft</li>
                <li>Agilit&auml;t &amp; Belastbarkeit</li>
                <li>Gepflegtes &Auml;u&szlig;eres</li>
            </ul>

            <p>&nbsp;</p>

            <h3><strong>Bewerbung an die Adresse: </strong></h3>

            <p style="margin-left:21.3pt">Infinity Lashes<br />
                Kaiserstr. 59<br />
                44135 Dortmund</p>

            <p>&nbsp;</p>

            <p style="margin-left:21.3pt">Oder per Email: <a href="mailto:info@infinity-lashes.de">bewerbung@infinity-lashes.de</a></p>

            <p>&nbsp;</p>

            <p>Wir freuen uns auf euch!</p>

            <p><em>Euer Infinity Lashes Team!</em></p>
        </div>
    </div>
    <div class="accordion" id="accordionCareers">
        <?php if ($modelCareers):?>
            <?php foreach ($modelCareers as $key => $career):?>
                <div class="card">
                    <div class="card-header" id="heading-<?= $key ?>">
                        <h5 class="mb-0">
                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse-<?= $key ?>" aria-expanded="true" aria-controls="collapse-<?= $key ?>"><?= $career->name ?></button>
                            <button class="btn btn-link arrow-btn" type="button" data-toggle="collapse" data-target="#collapse-<?= $key ?>" aria-expanded="true" aria-controls="collapse-<?= $key ?>">
                                <svg class="faq-arrow-new svg">
                                    <use xlink:href="#arrow-new"></use>
                                </svg>
                            </button>
                        </h5>
                    </div>
                    <div class="collapse" id="collapse-<?= $key ?>" aria-labelledby="heading-<?= $key ?>" data-parent="#accordionCareers">
                        <div class="card-body">
                            <div class="card-body-block-wrapper">
                                <?= $career->description ?>
                            </div><a class="careers-send-mail-button btn btn-primary" href="mailto:<?= Yii::$app->params[ 'adminEmail' ] ?>">Apply</a>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>