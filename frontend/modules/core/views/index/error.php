<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $name;
$this->bodyClass = 'wrong-page';
?>

<div class="img-and-text__head container-fluid">
    <div class="img-and-text__main-img">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 offset-xl-0 col-lg-7 offset-lg-0 col-md-10 offset-md-1">
                    <div class="fzf-info-wrapper">
                        <h2><?= Yii::t('core', 'Page not found') ?></h2>
                        <p>
                            <?= Yii::t('core', 'We apologise, we cannot find the page you are looking for. It has either been moved, deleted or no longer exists. Please contact our Client Services or navigate to another page. Thank you.') ?>
                        </p>
                        <div class="fzf-info-a-wrapper">
                            <a class="btn btn-primary" href="/">
                                <?= Yii::t('core', 'homepage') ?>
                            </a>
                            <a class="client-services" href="<?= Url::toRoute(['/core/index/contact-us', '#' => 'customerServices']); ?>">
                                <?= Yii::t('core', 'client services') ?>
                                <svg class="client-services-stroke svg">
                                  <use xlink:href="#totop"></use>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
