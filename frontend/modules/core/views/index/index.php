<?php

use frontend\modules\academy\models\Academy;
use frontend\modules\blog\models\Article;
use frontend\modules\blog\widgets\SmallBlockArticles;
use frontend\modules\core\models\Setting;
use yii\helpers\Url;
use frontend\modules\core\widgets\Subscribe;
use frontend\modules\product\widgets\SmallSliderWithProducts;
use frontend\modules\product\models\ProductCategory;

$this->bodyClass = 'main-page';

/** @var \frontend\modules\core\components\View $this */

if ($modalText) {
    $js = '$(function() {
        $(document).find("#core-modal .modal-body").html("' . $modalText . '");
        $(document).find("#core-modal").modal();
    })';

    $this->registerJs($js);
}
?>


<div class="owl-carousel owl-carousel__first-on-index-page owl-theme">
    <div class="item item-2" data-small="linear-gradient(to top, rgba(0, 0, 0, 0.51) 0%, rgba(0, 0, 0, 0) 31%, rgba(0, 0, 0, 0) 100%), url('/img/main-page-slider/2/small/2small.png') center center no-repeat" data-big="linear-gradient(to top, rgba(0, 0, 0, 0.51) 0%, rgba(0, 0, 0, 0) 31%, rgba(0, 0, 0, 0) 100%), url('/img/main-page-slider/2/big/2big.png') center center no-repeat">
        <p>Black Pearl Wimpernkleber</p>
        <p class="text">Die neue Perle unter den Wimprnklebern für Profis</p><a class="btn btn-primary" href="/glue" title="Shop now">Shop now</a>
    </div>
    <div class="item item-3" data-small="linear-gradient(to top, rgba(0, 0, 0, 0.51) 0%, rgba(0, 0, 0, 0) 31%, rgba(0, 0, 0, 0) 100%), url('/img/main-page-slider/3/small/3small.png') center center no-repeat" data-big="linear-gradient(to top, rgba(0, 0, 0, 0.51) 0%, rgba(0, 0, 0, 0) 31%, rgba(0, 0, 0, 0) 100%), url('/img/main-page-slider/3/big/3big.jpg') center center no-repeat">
        <p>PREMIERE!</p>
        <p class="text">Pflanzlicher Keratin Booster der neuen Generation</p><a class="btn btn-primary" href="/linfting-limination" title="Shop now">Shop now</a>
    </div>
</div>

<!-- <div class="owl-carousel owl-carousel__first-on-index-page owl-theme">
    <div class="item item-1">
        <p>Wimpernbürstchen mit Kristalen</p>
        <p class="text">The New Intensity</p>
        <a href="/<?= ProductCategory::getLinkShopNow(); ?>" class="btn btn-primary" title="<?= Yii::t('core', 'Shop now') ?>">
            <?= Yii::t('core', 'Shop now') ?>
        </a>
    </div>
    <div class="item item-2">
        <p>Image: The Legend of Our Time</p>
        <p class="text">Dream becomes reality with new eyelashes from Infiniti Lashes</p>
        <a href="/<?= ProductCategory::getLinkShopNow(); ?>" class="btn btn-primary" title="<?= Yii::t('core', 'Shop now') ?>">
            <?= Yii::t('core', 'Shop now') ?>
        </a>
    </div>
    <div class="item item-3">
        <p>Wimpernbürstchen mit Kristalen</p>
        <p class="text">The New Intensity</p>
        <a href="/<?= ProductCategory::getLinkShopNow(); ?>" class="btn btn-primary" title="<?= Yii::t('core', 'Shop now') ?>">
            <?= Yii::t('core', 'Shop now') ?>
        </a>
    </div>
</div> -->

<?php if (Setting::value('academy.enable')!=0): ?>

    <div class="courses container">
        <h3 class="row justify-content-center align-items-center"><?= Yii::t('core', 'New courses from infinity lashes') ?></h3>
        <div class="items">
            <div class="row">

                <?php foreach ($courses as $course) : ?>

                    <div class="card-row-margin col-xl-4 col-lg-4 col-md-6 col-sm-12">
                        <a class="item" href="<?= Url::toRoute(['/academy/academy/course', 'slug' => $course->slug]); ?>" title="<?= Yii::t('core', 'see more') ?>">
                            <div class="card-img-top card-img-top"><img src="<?= $course->urlSmallImage; ?>" alt="item"/></div>
                            <div class="card-body">
                                <div class="when-price">
                                    <svg class="date-card svg">
                                        <use xlink:href="#date"></use>
                                    </svg>
                                    <p>
                                        <span><?= $course->date?date("d F", strtotime($course->date)):'' ?></span>
                                    </p>
                                    <span><?= $course->price; ?> <?= Setting::value('product.shop.currency') ?></span>
                                </div>
                                <div class="card-body__main justify-content-center">
                                    <div class="adaptive-wrapper">
                                        <h5 class="card-title"><?= $course->name; ?></h5>
                                        <div class="card-text"><?= $course->description; ?></div>
                                    </div>
                                    <a class="btn btn-primary" href="<?= Url::toRoute(['/academy/academy/course', 'slug' => $course->slug]); ?>" title="<?= Yii::t('core', 'see more') ?>">
                                        <?= Yii::t('core', 'see more') ?>
                                    </a>
                                </div>
                            </div>
                        </a>
                    </div>

                <?php endforeach; ?>

            </div>
        </div>
        <div class="items-in-slider owl-carousel-main-cards owl-carousel">

            <?php foreach ($courses as $course) : ?>

                <div class="item">
                    <a href="<?= Url::toRoute(['/academy/academy/course', 'slug' => $course->slug]); ?>" title="<?= Yii::t('core', 'see more') ?>">
                        <div class="card-img-top card-img-top"><img src="<?= $course->urlSmallImage; ?>" alt="<?= $course->name; ?>"></div>
                        <div class="card-body">
                            <div class="when-price">
                                <svg class="date-card svg">
                                    <use xlink:href="#date"></use>
                                </svg>
                                <span><?= $course->date?date("d F", strtotime($course->date)):'' ?></span>
                                <span><?= $course->price; ?> <?= Setting::value('product.shop.currency') ?></span>
                            </div>
                            <div class="card-body__main justify-content-center">
                                <div class="adaptive-wrapper">
                                    <h5 class="card-title"><?= $course->name; ?></h5>
                                    <div class="card-text"><?= $course->description; ?></div>
                                </div>
                                <a class="btn btn-primary" href="<?= Url::toRoute(['/academy/academy/course', 'slug' => $course->slug]); ?>" title="<?= Yii::t('core', 'see more') ?>">
                                    <?= Yii::t('core', 'see more') ?>
                                </a>
                            </div>
                        </div>
                    </a>
                </div>

          <?php endforeach; ?>

        </div>
    </div>

<?php endif; ?>

<?= SmallSliderWithProducts::widget([
    'mainWrapperOptions' => [
        'class' => 'best-sellers container',
    ],
    'headerOptions' => [
        'title' => Yii::t('core', 'best-sellers'),
        'class' => 'row justify-content-center align-items-center',
    ],
    'wrapperOptions' => [
        'class' => 'owl-carousel owl-theme all-pages-carousel',
    ],
    'itemOptions' => [
        'class' => 'item',
    ],
    'typeWidget' => SmallSliderWithProducts::TYPE_BEST_SELLERS,
]); ?>

<?= SmallBlockArticles::widget([
    'template' => 'small-block-latest-articles',
    'items' => Article::getLatest(3),
]); ?>

<?= SmallSliderWithProducts::widget([
    'mainWrapperOptions' => [
        'class' => 'recommened container',
    ],
    'headerOptions' => [
        'title' => Yii::t('core', 'Recommended for you'),
        'class' => 'row justify-content-center align-items-center'
    ],
    'wrapperOptions' => [
        'class' => 'owl-carousel owl-theme all-pages-carousel',
    ],
    'itemOptions' => [
        'class' => 'item',
    ],
    'typeWidget' => SmallSliderWithProducts::TYPE_ALL_RECOMMENDS,
]); ?>