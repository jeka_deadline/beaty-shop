<?php

use frontend\assets\EditInputFileAsset;
use frontend\widgets\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\modules\core\models\Setting;

$this->bodyClass = 'contact-us-page';

EditInputFileAsset::register($this);

/** @var \frontend\modules\core\components\View $this */
/** @var \frontend\widgets\bootstrap4\ActiveForm $form */
/** @var \frontend\modules\core\models\forms\SupportMessageForm $model */
?>

<div class="img-and-text__head container-fluid">
    <div class="img-and-text__main-img row">
        <h1 class="img-and-text__main-text"><?= Yii::t('core', 'Contact us') ?></h1>
        <div class="img-and-text__text"><?= Yii::t('core', 'Customer support') ?></div>
    </div>
</div>
<main class="container">
    <h3 id="customerServices"><?= Yii::t('core', 'Customer service') ?></h3>
    <p><?= Yii::t('core', 'Our team of staff is available Monday to Saturday from 9:00 am to 8:00 pm (Dortmund time) to answer your questions'); ?></p>
    <div class="contact-us-page__contacts">
        <div class="contact-us-page__phone contact-us-page__contact">
            <div>
                <a href="tel:<?= Setting::value('core.contact.phone') ?>">
                    <svg class="phone svg">
                        <use xlink:href="#phone"></use>
                    </svg>
                </a>
            </div>
            <div><a href="tel:<?= Setting::value('core.contact.phone') ?>"><?= Setting::value('core.contact.phone') ?></a></div>
        </div>
        <div class="contact-us-page__mail contact-us-page__contact">
            <div>
                <a href="mailto:<?= Setting::value('core.contact.email') ?>">
                    <svg class="e-mail svg">
                        <use xlink:href="#e-mail"></use>
                    </svg>
                </a>
            </div>
            <div><a href="mailto:<?= Setting::value('core.contact.email') ?>"><?= Setting::value('core.contact.email') ?></a></div>
        </div>
        <div class="contact-us-page__map contact-us-page__contact">
            <div>
                <a href="<?= Setting::value('core.contact.address.maplink') ?>" target="_blank">
                    <svg class="address svg">
                        <use xlink:href="#address"></use>
                    </svg>
                </a>
            </div>
            <div><a href="<?= Setting::value('core.contact.address.maplink') ?>" target="_blank"><?= Setting::value('core.contact.address') ?></a></div>
        </div>
    </div>
    <h3><?= Yii::t('core', 'General Enquiries') ?></h3>
    <p><?= Yii::t('core', 'You can also write to us using the form below. Please complete all fields marked with an *.'); ?></p>
    <div class="contact-us-page__write-us">

        <?php $form = ActiveForm::begin([
            'options' => [
                'class' => 'row',
                'enctype' => 'multipart/form-data',
            ],
            'id' => 'contact-us-form',
            'enableAjaxValidation' => true,
            'validationUrl' => Url::toRoute(['/core/index/ajax-validation-contact-form']),
            'enableClientValidation' => false,
        ]); ?>

            <div class="contact-us-page__fields offset-xl-1 col-xl-5 col-lg-6">

                <?= $form->field($model, 'title', [
                    'template' => '<div class="input-effect">{input}</div>{hint}{error}',
                    'errorOptions' => [
                        'tag' => 'div',
                    ]
                ])
                ->dropDownList($model->getListUserTitles(), [
                    'prompt'=>Yii::t('product', 'Title').'*',
                    'class' => 'form-control effect-16',
                    'id' => 'contact-us-page__select-title',
                ]); ?>

                <?= $form->field($model, 'name', [
                    'template' => '<div class="input-effect">{input}{label}<span class="focus-border"></span></div>{error}',
                    'errorOptions' => [
                        'tag' => 'div',
                    ],
                    'labelOptions' => [
                        'class' => '',
                    ],
                    'inputOptions' => [
                        'class' => 'effect-16',
                        'id' => 'contact-us-page__input-first-name',
                        'placeholder' => '',
                    ]
                ])->label($model->getAttributeLabel('name') . '*'); ?>

                <?= $form->field($model, 'surname', [
                    'template' => '<div class="input-effect">{input}{label}<span class="focus-border"></span></div>{error}',
                    'errorOptions' => [
                        'tag' => 'div',
                    ],
                    'labelOptions' => [
                        'class' => '',
                    ],
                    'inputOptions' => [
                        'class' => 'effect-16',
                        'id' => 'contact-us-page__input-last-name',
                        'placeholder' => '',
                    ]
                ])->label($model->getAttributeLabel('surname') . '*'); ?>

                <?= $form->field($model, 'company', [
                    'template' => '<div class="input-effect">{input}{label}<span class="focus-border"></span></div>{error}',
                    'errorOptions' => [
                        'tag' => 'div',
                    ],
                    'labelOptions' => [
                        'class' => '',
                    ],
                    'inputOptions' => [
                        'class' => 'effect-16',
                        'id' => 'contact-us-page__company-name',
                        'placeholder' => '',
                    ]
                ])->label($model->getAttributeLabel('company')); ?>

                <?= $form->field($model, 'email', [
                    'template' => '<div class="input-effect">{input}{label}<span class="focus-border"></span></div>{error}',
                    'errorOptions' => [
                        'tag' => 'div',
                    ],
                    'labelOptions' => [
                        'class' => '',
                    ],
                    'inputOptions' => [
                        'class' => 'effect-16',
                        'id' => 'contact-us-page__input-emai',
                        'placeholder' => '',
                    ]
                ])->label($model->getAttributeLabel('email') . '*'); ?>

                <?= $form->field($model, 'subject', [
                    'template' => '<div class="input-effect select-subject">{input}<span class="focus-border"></span></div>{hint}{error}',
                    'errorOptions' => [
                        'tag' => 'div',
                    ]
                ])
                ->dropDownList($model->getListSubjects(), [
                    'class' => 'form-control effect-16',
                    'id' => 'contact-us-page__select-subject',
                ]); ?>

            </div>
            <div class="contact-us-page__fields-send col-xl-5 col-lg-6">

                <?= $form->field($model, 'text', [
                    'errorOptions' => [
                        'tag' => 'div',
                    ],
                    'labelOptions' => [
                        'class' => '',
                    ],
                ])->label('*')
                ->textarea([
                    'class' => 'form-control textarea',
                    'id' => 'contact-us-page__textarea',
                    'placeholder' => Yii::t('core', 'Your message'),
                ]); ?>
                <div data-for="<?= Html::getInputId($model, 'files[]')?>" style="margin-bottom: 20px;"></div>
                <div class="contact-us-page__send-attach-wrapper">
                    <div class="file__attach">

                        <?= $form->field($model, 'files[]', [
                            'options' => [
                                'tag' => false,
                            ],
                            'template' => '{input}',
                        ])->fileInput([
                            'class' => 'load-file-hide load-file-edit-list',
                            'multiple' => 'multiple',
                            'accept' => 'image/*,.pdf,.txt,.xlsx,.xls,.doc,.docx',
                        ]); ?>

                        <svg class="attach svg">
                            <use xlink:href="#attach"></use>
                        </svg>
                        <label for="<?= Html::getInputId($model, 'files[]')?>"><?= Yii::t('core', 'Attach file') ?></label>
                    </div>
                    <button class="btn btn-primary col" type="submit"><?= Yii::t('core', 'Send message') ?></button>
                </div>
            </div>

        <?php ActiveForm::end(); ?>

    </div>
    <div class="svgHide">
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="0" height="0" style="position:absolute">
            <symbol id="phone" viewBox="0 0 64 126">
                <defs>
                    <style>
                    .cls-1-pho {
                        fill: #0a0a0a
                    }
                    </style>
                </defs>
                <title>phone</title>
                <g id="Layer_2" data-name="Layer 2">
                    <g id="Layer_1-2" data-name="Layer 1">
                        <path class="cls-1-pho" d="M59 0H5a5 5 0 0 0-5 5v116a5 5 0 0 0 5 5h54a5 5 0 0 0 5-5V5a5 5 0 0 0-5-5zm3 121a3 3 0 0 1-3 3H5a3 3 0 0 1-3-3V5a3 3 0 0 1 3-3h54a3 3 0 0 1 3 3z"></path>
                        <path class="cls-1-pho" d="M41 9H23a.94.94 0 0 0-1 1 .94.94 0 0 0 1 1h18a.94.94 0 0 0 1-1 .94.94 0 0 0-1-1zM32 110a6 6 0 1 0 6 6 6 6 0 0 0-6-6zm0 10a4 4 0 1 1 4-4 4 4 0 0 1-4 4z"></path>
                    </g>
                </g>
            </symbol>
            <symbol id="e-mail" viewBox="0 0 126 70">
                <defs>
                    <style>
                    .cls-1-mail {
                        fill: #0a0a0a
                    }
                    </style>
                </defs>
                <title>e-mail</title>
                <g id="Layer_2" data-name="Layer 2">
                    <path class="cls-1-mail" d="M123 0H3a3 3 0 0 0-3 3v63.8A3.16 3.16 0 0 0 3.2 70h119.6a3.16 3.16 0 0 0 3.2-3.2V3a3 3 0 0 0-3-3zm.9 67.3L78.1 42.1 124 3.2v63.6a.9.9 0 0 1-.1.5zM2 66.8V3.2l45.9 38.9L2.1 67.2a.6.6 0 0 1-.1-.4zm61.6-15.1a1 1 0 0 1-1.3 0L3.7 2h118.6zm-14-8.2l11.5 9.8a3.7 3.7 0 0 0 1.9.7 2.64 2.64 0 0 0 1.9-.7l11.5-9.7L121.1 68H4.8z" id="Layer_1-2" data-name="Layer 1"></path>
                </g>
            </symbol>
            <symbol id="address" viewBox="0 0 90 126">
                <defs>
                    <style>
                    .cls-1-addr {
                        fill: #0a0a0a
                    }

                    .cls-2 {
                        fill: none;
                        stroke: #0a0a0a;
                        stroke-miterlimit: 10;
                        stroke-width: 2px
                    }
                    </style>
                </defs>
                <title>address</title>
                <g id="Layer_2" data-name="Layer 2">
                    <g id="Layer_1-2" data-name="Layer 1">
                        <path class="cls-1-addr" d="M45 0C20.2 0 0 20 0 44.6c0 10.7 7.1 29.3 19.1 49.7C28 109.6 39.9 126 45 126s17-16.4 25.9-31.7C82.8 73.9 90 55.3 90 44.6 90 20 69.8 0 45 0zm24.2 93.3C58.2 112.3 48 124 45 124s-13.2-11.7-24.2-30.7C9 73.2 2 55 2 44.6 2 21.1 21.3 2 45 2s43 19.1 43 42.6c0 10.4-7 28.6-18.8 48.7z"></path>
                        <path class="cls-2" d="M45 17a26 26 0 1 0 26 26 26.08 26.08 0 0 0-26-26z"></path>
                        <circle class="cls-2" cx="45" cy="43" r="7"></circle>
                    </g>
                </g>
            </symbol>
        </svg>
    </div>
</main>
