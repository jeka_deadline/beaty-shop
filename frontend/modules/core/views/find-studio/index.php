<?php

use frontend\modules\core\assets\FindStudioAsset;
use frontend\widgets\bootstrap4\ActiveForm;
use frontend\widgets\bootstrap4\Html;
use yii\helpers\Url;

$this->bodyClass = 'find-studio-page';

FindStudioAsset::register($this);

?>

<main class="container-fluid">
    <div class="find-studio__adaptive-find"></div>
    <div class="find-studio__map">
        <div id="map"></div>
    </div>
    <div class="find-studio__find-list">
        <?php $form = ActiveForm::begin([
            'id' => 'form-search-studio',
            'action' => Url::toRoute('search-studio'),
            'enableClientValidation' => false,
            'enableAjaxValidation' => false,
            'method' => 'post',
        ]); ?>
            <h5><?= Yii::t('core', 'City, country'); ?></h5>
            <div id="city-search">
                <div class="city-search input-group">
                    <?= $form->field($findStudioForm, 'query', ['options' => [
                        'tag' => false,
                    ]])->textInput(['maxlength' => true])->label(false) ?>
                    <div class="input-group-append">
                        <?= Html::submitButton('<svg class="search svg"><use xlink:href="#search"></use></svg>') ?>
                    </div>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
        <div class="city-search-items-wrapper">
            <?php foreach ($modelContactStudios as $contactStudio): ?>
                <div class="city-search-item item<?= $contactStudio->id ?>">
                    <div class="city-search-item-header"><?= $contactStudio->name ?></div>
                    <div class="city-search-item-body">
                        <?= $contactStudio->description ?>
                    </div>
                </div>
            <?php endforeach; ?>
            <button class="btn btn-primary"><?= Yii::t('core', 'load more'); ?></button>
        </div>
    </div>
</main>