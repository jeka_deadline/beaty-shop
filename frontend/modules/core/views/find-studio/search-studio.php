<?php

?>
<?php foreach ($modelContactStudios as $contactStudio): ?>
    <div class="city-search-item item<?= $contactStudio->id ?>">
        <div class="city-search-item-header"><?= $contactStudio->name ?></div>
        <div class="city-search-item-body">
            <?= $contactStudio->description ?>
        </div>
    </div>
<?php endforeach; ?>
