<?php

use yii\helpers\Html;
use frontend\assets\AppAsset;
use frontend\modules\core\assets\CoreAsset;
use frontend\modules\product\widgets\CartPopup;
use frontend\modules\core\widgets\HeaderMenu;

AppAsset::register($this);
CoreAsset::register($this);

/** @var \frontend\modules\core\components\View $this */

?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language; ?>">
    <head>

        <?php if ($_SERVER[ 'HTTP_HOST' ] === 'dverst.belka-z.com' || $_SERVER[ 'HTTP_HOST' ] === 'infinity-lashes.de') : ?>

            <!-- Google Tag Manager -->
            <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-TR8SQRX');</script>
        <!-- End Google Tag Manager -->

        <?php endif; ?>

        <title><?= Html::encode($this->title); ?></title>
        <meta charset="<?= Yii::$app->charset; ?>">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="theme-color" content="#000">
        <meta name="msapplication-navbutton-color" content="#000">
        <meta name="apple-mobile-web-app-status-bar-style" content="#000">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="msapplication-config" content="/img/icons/favicons/browserconfig.xml">
        <meta name="theme-color" content="#ffffff">
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
        <link rel="manifest" href="/site.webmanifest">
        <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">

        <?= Html::csrfMetaTags(); ?>

        <?php $this->head(); ?>
    </head>
    <body class="<?= $this->bodyClass; ?>">

        <?php if ($_SERVER[ 'HTTP_HOST' ] === 'dverst.belka-z.com' || $_SERVER[ 'HTTP_HOST' ] === 'infinity-lashes.de') : ?>

            <!-- Google Tag Manager (noscript) -->
            <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TR8SQRX" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
            <!-- End Google Tag Manager (noscript) -->

        <?php endif; ?>

        <?php $this->beginBody(); ?>

        <noscript>
            <h2>Your browser does not support JavaScript!</h2>
        </noscript>

        <?= $this->render('parts/header', [
                'categories' => HeaderMenu::getBlockCategories()
        ]); ?>
        <?= HeaderMenu::widget(); ?>

        <?= CartPopup::widget(); ?>

        <?= $content; ?>

        <?= $this->render('parts/footer'); ?>

        <?php if (!isset(Yii::$app->params[ 'cookies-policy' ])) : ?>

            <?= $this->render('parts/cookie-policy'); ?>

        <?php endif; ?>

        <?= $this->render('parts/popups'); ?>
        <?php $this->endBody(); ?>

    </body>
</html>
<?php $this->endPage(); ?>