<div class="alert <?= (isset($alertClass)) ? $alertClass : 'alert-info'; ?> alert-dismissible" role="alert">
    <?php if (isset($closeButton)) : ?>

        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

    <?php endif; ?>
    <?php if (isset($strongText)) : ?>

        <strong><?= $strongText; ?></strong>

    <?php endif; ?>

    <?= (isset($text) ? $text : ''); ?>

</div>