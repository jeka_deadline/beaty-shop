<?php

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\modules\product\helpers\CartHelper;
use frontend\assets\CheckoutAsset;

CheckoutAsset::register($this);

/** @var \frontend\modules\core\components\View $this */
?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language; ?>">
    <head>

        <?php if ($_SERVER[ 'HTTP_HOST' ] === 'dverst.belka-z.com' || $_SERVER[ 'HTTP_HOST' ] === 'infinity-lashes.de') : ?>

            <!-- Google Tag Manager -->
            <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-TR8SQRX');</script>
        <!-- End Google Tag Manager -->

        <?php endif; ?>

        <title><?= Html::encode($this->title); ?></title>
        <meta charset="<?= Yii::$app->charset; ?>">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="theme-color" content="#000">
        <meta name="msapplication-navbutton-color" content="#000">
        <meta name="apple-mobile-web-app-status-bar-style" content="#000">
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
        <link rel="manifest" href="/site.webmanifest">
        <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">

        <?= Html::csrfMetaTags(); ?>

        <?php $this->head(); ?>
    </head>
    <body class="<?= $this->bodyClass; ?>">

        <?php if ($_SERVER[ 'HTTP_HOST' ] === 'dverst.belka-z.com' || $_SERVER[ 'HTTP_HOST' ] === 'infinity-lashes.de') : ?>

            <!-- Google Tag Manager (noscript) -->
            <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TR8SQRX" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
            <!-- End Google Tag Manager (noscript) -->

        <?php endif; ?>

        <?php $this->beginBody(); ?>
        <noscript>
            <h2>Your browser does not support JavaScript!</h2>
        </noscript>
        <header class="container-fluid header-shadow" id="top-checkout">
            <div class="main row justify-content-between align-items-center">
                <div class="main-logo"><a href="/"><img src="/img/icons/logo.svg"  alt="Infinity Lashes" title="Infinity Lashes"></a></div>
                <div class="main-enter">

                    <div class="main-enter__singin">
                        <a href="<?= (Yii::$app->user->isGuest) ? Url::toRoute(['/user/security/login']) : Url::toRoute(['/user/user/account']); ?>">
                            <svg class="account svg">
                                <use xlink:href="#account"></use>
                            </svg>
                        </a>

                        <?php if (Yii::$app->user->isGuest) : ?>

                            <a class="checkout-header-sign" href="<?= Url::toRoute(['/user/security/login']); ?>" title="<?= Yii::t('core', 'Sign in') ?> / <?= Yii::t('core', 'Join') ?>">
                                <?= Yii::t('core', 'Sign in') ?> / <?= Yii::t('core', 'Join') ?>
                            </a>

                        <?php else : ?>

                            <a class="checkout-header-sign" href="<?= Url::toRoute(['/user/user/account']); ?>"><?= Yii::$app->user->identity->fullName; ?></a>

                        <?php endif; ?>

                    </div>

                    <div class="main-enter__basket">
                        <a href="<?= Url::toRoute(['/product/cart/index']); ?>" title="Cart">
                            <svg class="cart1 svg">
                                <use xlink:href="#Cart"></use>
                            </svg>

                            <?php if (!CartHelper::isEmptyCart()) : ?>

                                <div class="header__numbers-of-orders">
                                    <p><?= CartHelper::getCountProductsInCart(); ?></p>
                                </div>

                            <?php endif; ?>
                        </a>
                    </div>

                </div>
            </div>
        </header>

        <?= $content; ?>

        <?= $this->render('parts/popups'); ?>
        <?= $this->render('parts/footer'); ?>
        <?php if (!isset(Yii::$app->params[ 'cookies-policy' ])) : ?>

            <?= $this->render('parts/cookie-policy'); ?>

        <?php endif; ?>

        <?php $this->endBody(); ?>
    </body>
</html>
<?php $this->endPage(); ?>