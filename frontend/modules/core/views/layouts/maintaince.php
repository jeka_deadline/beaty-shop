<?php
use yii\helpers\Html;
use frontend\assets\MaintainceAsset;

MaintainceAsset::register($this);
?>
<?php $this->beginPage(); ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language; ?>">
    <head>
        <meta charset="<?= Yii::$app->charset; ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Free coming soon template with jQuery countdown">
        <title>Maintaince mode</title>
        <?php $this->head(); ?>
    </head>
    <body>
    <?php $this->beginBody(); ?>

        <?= $content; ?>


    <?php $this->endBody(); ?>
    </body>
</html>
<?php $this->endPage(); ?>