<?php

use frontend\modules\core\widgets\Slogan;
use yii\helpers\Url;
use frontend\modules\product\widgets\ProductSearch;
use frontend\modules\product\helpers\CartHelper;
use frontend\modules\core\widgets\LanguageSelectWidget;

?>

<header class="container-fluid header-shadow" id="top">
    <div class="header__burger-menu-wrapper">
        <div class="container">
            <div class="row burger-main-logo-wrapper">
                <div class="burger-back-wrapper">
                    <svg class="totop-back svg">
                        <use xlink:href="#totop"></use>
                    </svg>
                    <div class="burger-main-back">back</div>
                </div>
                <div class="burger-main-logo col-lg-12 col-md-12">
                    <a class="big-logo" href="/"><img src="/img/new_logo.svg" alt="Infinity Lashes" title="Infinity Lashes"></a>
                    <a class="small-logo" href="/"><img src="/img/svg/logo_for_small.svg" alt="Infinity Lashes" title="Infinity Lashes"></a>
                </div>
            </div>
            <div class="burger-menu__windows-wrapper">
                <div class="burger-menu__first-window">
                    <div class="row burger-menu-wrapper">
                        <div class="burger-menu-punkts col-12">
                            <ul>
                                <li>
                                    <a href="#" data-attr="shop"><?= Yii::t('core', 'Shop') ?></a>
                                    <a class="next-link" href="#" data-attr="shop">
                                        <svg class="totop svg">
                                            <use xlink:href="#totop"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= Url::toRoute(['/academy']); ?>" data-attr="Academy"><?= Yii::t('core', 'Academy') ?></a>
                                    <a class="next-link" href="#" data-attr="Academy">
                                        <svg class="totop svg">
                                            <use xlink:href="#totop"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= Url::toRoute(['/blog']); ?>" data-attr="blog"><?= Yii::t('core', 'Blog') ?></a>
                                    <a class="next-link" href="#" data-attr="blog">
                                        <svg class="totop svg">
                                            <use xlink:href="#totop"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= Url::toRoute(['/pages/index/page', 'uri' => 'about']); ?>" data-attr="About us"><?= Yii::t('core', 'About us') ?></a>
                                    <a class="next-link" href="#" data-attr="About us">
                                        <svg class="totop svg">
                                            <use xlink:href="#totop"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= Url::toRoute(['/core/index/contact-us']); ?>" data-attr="contact us"><?= Yii::t('core', 'Contact us') ?></a>
                                    <a class="next-link" href="#" data-attr="contact us">
                                        <svg class="totop svg">
                                            <use xlink:href="#totop"></use>
                                        </svg>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="row burger-after-menu-wrapper">
                        <div class="burger-after-menu-punkts col-12">
                            <ul>
                                <li><a href="<?= Url::to('/trainer-distributor')?>" title="trainer / distributor">trainer / distributor</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="burger-menu__second-window" data-parent-attr="shop">
                    <div class="row burger-menu-wrapper">
                        <a class="burger-menu-submenu-name" href="<?= Url::to('/shop')?>"></a>
                        <div class="burger-menu-punkts col-12">
                            <ul>

                                <?php if ($categories): ?>
                                    <?php foreach ($categories as $category): ?>

                                        <li>
                                            <a href="<?= (isset($category->children) && $category->children) ? '#' : $category->url; ?>" data-attr="<?= $category->name?>"><?= $category->name?></a>
                                            <a class="next-link" href="#" data-attr="<?= $category->name?>">
                                                <svg class="totop svg">
                                                    <use xlink:href="#totop"></use>
                                                </svg>
                                            </a>
                                        </li>

                                    <?php endforeach;?>
                                <?php endif; ?>

                            </ul>
                        </div>
                    </div>
                </div>

                <?php if ($categories): ?>
                    <?php foreach ($categories as $category): ?>
                        <?php if (is_object($category) && isset($category->children) && $category->children): ?>

                            <div class="burger-menu__third-window" data-parent-attr="<?= $category['name']?>">
                                <div class="row burger-menu-wrapper">
                                    <a class="burger-menu-submenu-name" href="<?= $category['url']?>"></a>
                                    <div class="burger-menu-punkts col-12">
                                        <ul>
                                <?php foreach ($category['children'] as $children): ?>
                                            <li><a href="<?= $children['url']?>" data-attr="<?= $children['name']?>"><?= $children['name']?></a></li>
                                <?php endforeach;?>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        <?php endif; ?>
                    <?php endforeach;?>
                <?php endif; ?>

                <div class="burger-menu__language-window">
                    <div class="burger-menu__language-menu-change grey-form">
                        <?= LanguageSelectWidget::widget([
                                'template' => 'language-select-mobile'
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="burger-footer-wrapper">
                <div class="burger-footer-item burger-menu__language-menu-trigger">
                    <a href="#" title="Language">
                        <svg class="lang svg">
                            <use xlink:href="#lang"></use>
                        </svg>
                        <p>
                            <?= (isset(Yii::$app->session['language']) && isset(Yii::$app->session['language']['name']))?Yii::$app->session['language']['name']:'' ?>
                        </p>
                    </a>
                </div>
                <div class="burger-footer-item center-item">
                    <a href="<?= Url::toRoute(['/find-studio']); ?>" title="<?= Yii::t('core', 'Find a Studio') ?>">
                        <svg class="onmap svg">
                            <use xlink:href="#onmap"></use>
                        </svg>
                        <p><?= Yii::t('core', 'Find a Studio') ?></p>
                    </a>
                </div>
                <div class="burger-footer-item">

                    <?php if (Yii::$app->user->isGuest) : ?>

                        <a href="<?= Url::toRoute(['/user/security/login']); ?>" title="<?= Yii::t('core', 'Sign in') ?> / <?= Yii::t('core', 'Join') ?>">
                            <svg class="account svg">
                                <use xlink:href="#account"></use>
                            </svg>
                            <p><?= Yii::t('core', 'Sign in') ?> / <?= Yii::t('core', 'Join') ?></p>
                        </a>

                    <?php else : ?>

                        <a href="<?= Url::toRoute(['/user/user/account']); ?>">
                            <svg class="account svg">
                                <use xlink:href="#account"></use>
                            </svg>
                            <p><?= Yii::$app->user->identity->fullName; ?></p>
                        </a>

                    <?php endif; ?>

                </div>
            </div>
        </div>
    </div>
    <div class="header row justify-content-between">
        <div class="header_left-block"><a href="<?= Url::to('/trainer-distributor')?>" title="trainer / distributor">  Trainer / Distributor</a></div>

        <?= Slogan::widget(); ?>

        <div class="header_right-block">
            <div class="header_right-block_find">
                <a href="<?= Url::toRoute(['/find-studio']); ?>" title="<?= Yii::t('core', 'Find a Studio') ?>">
                    <span><?= Yii::t('core', 'Find a Studio') ?></span>
                    <svg class="onmap svg">
                        <use xlink:href="#onmap"></use>
                    </svg>
                </a>
            </div>

            <?= LanguageSelectWidget::widget() ?>

        </div>
    </div>
    <div class="main row justify-content-between align-items-center">
        <div class="main-search-adaptive-left">
            <svg class="search-adaptive-left svg">
                <use xlink:href="#search"></use>
            </svg>
        </div>
        <div class="main-search input-group">

            <?= ProductSearch::widget([
                'groupForm' => 'search-filter',
                'options' => [
                    'placeholder' => Yii::t('product', 'Search'),
                    'class' => 'form-control',
                ],
            ]) ?>

        </div>
        <div class="header__boorger-wrapper">
            <div class="header__boorger"></div>
        </div>
        <div class="main-logo">
            <a class="big-logo" href="/"><img src="/img/new_logo.svg" alt="Infinity Lashes" title="Infinity Lashes"></a>
            <a class="small-logo" href="/"><img src="/img/svg/logo_for_small.svg" alt="Infinity Lashes" title="Infinity Lashes"></a>
        </div>
        <div class="main-enter">
            <div class="main-search-adaptive-right">
                <svg class="search-adaptive-right svg">
                    <use xlink:href="#search"></use>
                </svg>
            </div>

            <div class="main-enter__singin">
                <a href="<?= (Yii::$app->user->isGuest) ? Url::toRoute(['/user/security/login']) : Url::toRoute(['/user/user/account']); ?>">
                    <svg class="account svg">
                        <use xlink:href="#account"></use>
                    </svg>
                </a>

                <?php if (Yii::$app->user->isGuest) : ?>

                    <a href="<?= Url::toRoute(['/user/security/login']); ?>" title="<?= Yii::t('core', 'Sign in') ?> / <?= Yii::t('core', 'Join') ?>">
                        <?= Yii::t('core', 'Sign in') ?> / <?= Yii::t('core', 'Join') ?>
                    </a>

                <?php else : ?>

                    <a href="<?= Url::toRoute(['/user/user/account']); ?>"><?= Yii::$app->user->identity->fullName; ?></a>

                <?php endif; ?>

            </div>

            <div class="main-enter__basket">
                <a href="<?= Url::toRoute(['/product/cart/index']); ?>" title="Cart">
                    <p><?= (CartHelper::isEmptyCart()) ? Yii::t('core', 'Empty') : ''; ?></p>
                    <svg class="cart svg">
                        <use xlink:href="#Cart"></use>
                    </svg>

                    <?php if (!CartHelper::isEmptyCart()) : ?>

                        <div class="header__numbers-of-orders">
                            <p><?= CartHelper::getCountProductsInCart(); ?></p>
                        </div>

                    <?php endif; ?>

                </a>
            </div>

        </div>
    </div>
    <nav class="container">
        <div class="row justify-content-center align-items-center">
            <ul>
                <li>
                    <a class="header__shop" href="<?= Url::toRoute(['/shop']); ?>" title="<?= Yii::t('core', 'Shop') ?>">
                        <?= Yii::t('core', 'Shop') ?>
                    </a>
                </li>
                <li>
                    <a class="header__academy" href="<?= Url::toRoute(['/academy']); ?>" title="<?= Yii::t('core', 'Academy') ?>">
                        <?= Yii::t('core', 'Academy') ?>
                    </a>
                </li>
                <li><a href="<?= Url::toRoute(['/blog']); ?>" title="<?= Yii::t('core', 'Blog') ?>"><?= Yii::t('core', 'Blog') ?></a></li>
                <li><a href="<?= Url::toRoute(['/pages/index/page', 'uri' => 'about']); ?>" title="<?= Yii::t('core', 'About us') ?>"><?= Yii::t('core', 'About us') ?></a></li>
                <li><a href="<?= Url::toRoute(['/core/index/contact-us']); ?>" title="<?= Yii::t('core', 'Contact us') ?>"><?= Yii::t('core', 'Contact us') ?></a></li>
            </ul>
        </div>
    </nav>
    <div id="nav-line"></div>
    <div class="svgHide">
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="0" height="0" style="position:absolute">
            <symbol id="account" viewBox="0 0 25 23">
                <title>account</title>
                <path d="M12.5 0A5.5 5.5 0 1 0 18 5.5 5.5 5.5 0 0 0 12.5 0zm0 10A4.5 4.5 0 1 1 17 5.5a4.5 4.5 0 0 1-4.5 4.5zm0 2A12.56 12.56 0 0 0 0 23h25a12.56 12.56 0 0 0-12.5-11zm-11 10c.66-5.07 5.33-9 11-9s10.34 3.93 11 9z" data-name="Layer 1"></path>
            </symbol>
            <symbol id="lang" viewBox="0 0 416 416">
                <title>lang</title>
                <path d="M208 0h-.5C92.8.3 0 93.3 0 208s92.8 207.7 207.5 208h.5c114.9 0 208-93.1 208-208S322.9 0 208 0zm8.3 124.5a308.36 308.36 0 0 0 64.2-8.5c6.2 24.5 10.1 52.8 10.7 83.8h-74.9v-75.3zm0-16.7V18c22.4 6.2 45.2 36.1 59.6 82a292 292 0 0 1-59.6 7.8zm-16.6-90v90.1a289.16 289.16 0 0 1-60.1-8c14.6-46.2 37.5-76.3 60.1-82.1zm0 106.7v75.2h-75.4c.6-31 4.5-59.3 10.7-83.8a313.18 313.18 0 0 0 64.7 8.6zm-92.2 75.2H16.9a190.12 190.12 0 0 1 43.3-113 302.39 302.39 0 0 0 58.8 24.8c-6.8 26.5-10.8 56.4-11.5 88.2zm0 16.6c.6 31.7 4.6 61.7 11.4 88.2a311.4 311.4 0 0 0-58.8 24.8 190.76 190.76 0 0 1-43.3-113zm16.8 0h75.4v75.1a306.58 306.58 0 0 0-64.7 8.7c-6.2-24.5-10.1-52.8-10.7-83.8zm75.4 91.8v90.2c-22.6-5.9-45.5-35.9-60.1-82.1a282.64 282.64 0 0 1 60.1-8.1zm16.6 89.9v-90a294 294 0 0 1 59.7 7.9c-14.5 46-37.2 75.9-59.7 82.1zm0-106.6v-75.1h74.9c-.6 30.9-4.5 59.2-10.7 83.7a308.4 308.4 0 0 0-64.2-8.6zm91.6-75.1h91.2a190.37 190.37 0 0 1-43.3 113 307.5 307.5 0 0 0-59.3-25c6.8-26.5 10.8-56.3 11.4-88zm0-16.6c-.6-31.7-4.6-61.6-11.3-88.1a309.93 309.93 0 0 0 59.2-24.9 190.17 190.17 0 0 1 43.3 113.1h-91.2zm36.5-125.8a289.22 289.22 0 0 1-52.3 21.6c-9.7-31.3-23.4-56.8-39.5-73.6a192.33 192.33 0 0 1 91.8 52zM162.8 22.1c-16.1 16.7-29.7 42.2-39.3 73.3a284.1 284.1 0 0 1-51.8-21.5 190.14 190.14 0 0 1 91.1-51.8zM71.6 342a287.06 287.06 0 0 1 51.8-21.5c9.7 31.2 23.3 56.6 39.4 73.4A190.27 190.27 0 0 1 71.6 342zm181 52.1c16.2-16.8 29.8-42.3 39.6-73.7a289.22 289.22 0 0 1 52.3 21.6 192 192 0 0 1-91.9 52.1z">
                </path>
            </symbol>
            <symbol id="search" viewBox="0 0 60 60">
                <title>Search</title>
                <path d="M44.5 23.5a20.9 20.9 0 1 0-20.9 20.9 20.89 20.89 0 0 0 20.9-20.9zM60 58.2L58.2 60l-19-19A23.51 23.51 0 1 1 47 23.5a23.74 23.74 0 0 1-6 15.7z" data-name="Layer 1" title="TEST TITLE"></path>
            </symbol>
            <symbol id="onmap" viewBox="0 0 45.8 60">
                <defs></defs>
                <title>onmap</title>
                <path d="M50.2 21.1A22.9 22.9 0 0 0 27.3 44c0 12.6 22.9 37.1 22.9 37.1S73.1 56.7 73.1 44a22.83 22.83 0 0 0-22.9-22.9zm0 57c-2.1-2.3-6-6.8-9.9-12.1-7.2-9.7-11-17.3-11-22.1a20.9 20.9 0 1 1 41.8.1c0 4.7-3.8 12.4-11 22.1-3.9 5.2-7.8 9.7-9.9 12z" transform="translate(-27.3 -21.1)"></path>
                <path d="M50.2 32.1A10.8 10.8 0 1 0 61 42.9a10.76 10.76 0 0 0-10.8-10.8zm6.2 17.1A8.69 8.69 0 1 1 59 43a8.77 8.77 0 0 1-2.6 6.2z" transform="translate(-27.3 -21.1)"></path>
            </symbol>
            <symbol id="Cart" viewBox="0 0 50 50">
                <defs>
                    <style>
                    .cls-1-ct,
                    .cls-2-ct {
                        fill: none
                    }

                    .cls-1-ct {
                        stroke: #000;
                        stroke-linecap: round;
                        stroke-miterlimit: 10;
                        stroke-width: 2px
                    }
                    </style>
                </defs>
                <title>Cart</title>
                <g id="Layer_2" data-name="Layer 2">
                    <g id="Layer_1-2" data-name="Layer 1">
                        <path class="cls-1-ct" d="M8 14L4 49h42l-4-35z"></path>
                        <path class="cls-2-ct" d="M0 0h50v50H0z"></path>
                        <path class="cls-1-ct" d="M34 19v-8a9 9 0 0 0-18 0v8"></path>
                        <circle cx="34" cy="19" r="2"></circle>
                        <circle cx="16" cy="19" r="2"></circle>
                    </g>
                </g>
            </symbol>
            <symbol id="Cart-white" viewBox="0 0 50 50">
                <defs>
                    <style>
                    .cls-1-ctw,
                    .cls-2-ctw {
                        fill: none
                    }

                    .cls-1-ctw {
                        stroke: #fff;
                        stroke-linecap: round;
                        stroke-miterlimit: 10;
                        stroke-width: 2px
                    }
                    </style>
                </defs>
                <title>Cart</title>
                <g id="Layer_2" data-name="Layer 2">
                    <g id="Layer_1-2" data-name="Layer 1">
                        <path class="cls-1-ctw" d="M8 14L4 49h42l-4-35z"></path>
                        <path class="cls-2-ctw" d="M0 0h50v50H0z"></path>
                        <path class="cls-1-ctw" d="M34 19v-8a9 9 0 0 0-18 0v8"></path>
                        <circle cx="34" cy="19" r="2"></circle>
                        <circle cx="16" cy="19" r="2"></circle>
                    </g>
                </g>
            </symbol>
        </svg>
    </div>
    <div class="header__adaptive-search">
        <div class="container">
            <div class="main-search input-group">

                <?= ProductSearch::widget([
                    'groupForm' => 'search-filter',
                    'options' => [
                        'id' => 'adaptive-search-input',
                        'placeholder' => Yii::t('product', 'Search'),
                        'class' => 'form-control',
                    ],
                ]) ?>

            </div>
        </div>
        <div class="svgHide"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="0" height="0" style="position:absolute">
                <symbol id="close-search" viewBox="0 0 15 15">
                    <defs><style>.cls-1-cs{fill:#000}</style></defs><title>close</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1-cs" transform="rotate(-45 7.498 7.504)" d="M6.84-2.44h1.33v19.89H6.84z"></path><path class="cls-1-cs" transform="rotate(45 7.504 7.498)" d="M6.84-2.44h1.33v19.89H6.84z"></path></g></g>
                </symbol></svg>
        </div>
    </div>
</header>