<section class="container-fluid" id="footer-popup">
    <div class="row">
        <div class="container">
            <p class="subscribe-text col-xl-10 offset-xl-1"></p>
            <svg class="close-subscribe svg">
                <use xlink:href="#close"></use>
            </svg>
        </div>
    </div>
</section>
<div class="modal fade" id="core-modal" tabindex="-1" role="dialog" aria-labelledby="requestTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>