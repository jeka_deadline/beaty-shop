<?php
use yii\helpers\Url;
?>
<section class="cookies-wrapper container-fluid">
    <div class="row">
        <div class="container">
            <p class="cookies-text col-xl-10 offset-xl-1 col-lg-11 col-md-11 col-sm-12"><?= Yii::t('core', 'To give you the best possible experience, this  site use cookies. By continuing you are giving your consent to cookies being used. To find out more or adjust cooking settings, <a href="{0}">click here.</a>', Url::toRoute(['/pages/index/page', 'uri' => 'terms'])); ?></p>
            <a class="close-cookies" href="#">Ok</a>
        </div>
    </div>
</section>