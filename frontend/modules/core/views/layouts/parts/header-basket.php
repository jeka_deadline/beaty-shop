<?php
use yii\helpers\Url;
use frontend\modules\product\helpers\CartHelper;
?>

<div class="main-enter__basket">
    <a href="<?= Url::toRoute(['/product/cart/index']); ?>" title="Cart">
        <p><?= (CartHelper::isEmptyCart()) ? Yii::t('core', 'Empty') : ''; ?></p>
        <svg class="cart svg">
            <use xlink:href="#Cart"></use>
        </svg>

        <?php if (!CartHelper::isEmptyCart()) : ?>

            <div class="header__numbers-of-orders">
                <p><?= CartHelper::getCountProductsInCart(); ?></p>
            </div>

        <?php endif; ?>

    </a>
</div>