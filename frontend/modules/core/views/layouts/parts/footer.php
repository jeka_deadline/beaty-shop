<?php

use yii\helpers\Url;
use frontend\modules\core\widgets\Subscribe;
?>

<footer>
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <a class="scrolltop" href="#top" title="<?= Yii::t('core', 'Back to the top') ?>">
                <svg class="totop svg">
                    <use xlink:href="#arrow-new"></use>
                </svg>
            </a>
        </div>
        <a class="scrolltop" href="#top" title="<?= Yii::t('core', 'Back to the top') ?>">
            <p class="to-top row justify-content-center align-items-center"><?= Yii::t('core', 'Back to the top') ?></p>
        </a>
        <hr>

        <?= Subscribe::widget(); ?>

    </div>
    <div class="container">
        <hr>
    </div>
    <div class="container footer__two">
        <div class="footer__two-custumer footer__two-parts">
            <p><?= Yii::t('core', 'Custumer support') ?></p>
            <ul>
                <li>
                    <a href="<?= Url::toRoute(['/core/index/contact-us']); ?>" title="<?= Yii::t('core', 'Contact Us') ?>">
                        <?= Yii::t('core', 'Contact Us') ?>
                    </a>
                </li>
                <?php
                /*
                ?>
                <li>
                    <a href="<?= Url::toRoute(['/pages/index/page', 'uri' => 'faq']); ?>" title="FAQs">
                        FAQs
                    </a>
                </li>
                */
                ?>
                <?php
                /*
                <li>
                    <a href="<?= Url::toRoute(['/pages/index/page', 'uri' => 'shipping']); ?>" title="<?= Url::toRoute(['/pages/index/page', 'uri' => 'shipping']); ?>">
                        <?= Yii::t('core', 'Shipping') ?>
                    </a>
                </li>
                */
                ?>
                <li>
                    <a href="<?= Url::toRoute(['/pages/index/page', 'uri' => 'delivery-cost']); ?>" title="<?= Yii::t('core', 'Delivery and Cost') ?>">
                        <?= Yii::t('core', 'Delivery and Cost') ?>
                    </a>
                </li>
                <?php
                /*
                <li>
                    <a href="<?= Url::toRoute(['/pages/index/page', 'uri' => 'impressum']); ?>" title="<?= Url::toRoute(['/pages/index/page', 'uri' => 'impressum']); ?>">
                        <?= Yii::t('core', 'Return') ?>
                    </a>
                </li>
                */
                ?>
            </ul>
        </div>
        <div class="footer__two-about footer__two-parts">
            <p><?= Yii::t('core', 'The company') ?></p>
            <ul>
                <li>
                    <a href="<?= Url::toRoute(['/pages/index/page', 'uri' => 'about']); ?>" title="<?= Yii::t('core', 'About us') ?>">
                        <?= Yii::t('core', 'About us') ?>
                    </a>
                </li>
                <li>
                    <a href="<?= Url::toRoute(['/careers']); ?>" title="<?= Yii::t('core', 'Careers') ?>">
                        <?= Yii::t('core', 'Careers') ?>
                    </a>
                </li>
                <li>
                    <a href="<?= Url::toRoute(['/find-studio']); ?>" title="Find Studio">
                        <?= Yii::t('core', 'Find Studio') ?>
                    </a>
                </li>
            </ul>
        </div>
        <div class="footer__two-legal footer__two-parts">
            <p><?= Yii::t('core', 'Return') ?></p>
            <ul>
                <li>
                    <a href="<?= Url::toRoute(['/pages/index/page', 'uri' => 'impressum']); ?>" title="Impressum">
                        Impressum
                    </a>
                </li>
                <li>
                    <a href="<?= Url::toRoute(['/pages/index/page', 'uri' => 'cookie']); ?>" title="<?= Yii::t('core', 'Cookie policies') ?>">
                        <?= Yii::t('core', 'Cookie policies') ?>
                    </a>
                </li>
                <li>
                    <a href="<?= Url::toRoute(['/pages/index/page', 'uri' => 'terms']); ?>" title="<?= Yii::t('core', 'Terms and Conditions') ?>">
                        <?= Yii::t('core', 'Terms and Conditions') ?>
                    </a>
                </li>
            </ul>
        </div>
        <div class="footer__two-accordeon">
            <div class="accordion" id="accordionFooter">
                <div class="card">
                    <div class="card-header" id="headingFooterOne">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFooterOne" aria-expanded="true" aria-controls="collapseFooterOne"><?= Yii::t('core', 'Custumer support') ?></button>
                        </h5>
                    </div>
                    <div class="collapse" id="collapseFooterOne" aria-labelledby="headingFooterOne" data-parent="#accordionFooter">
                        <div class="card-body">
                            <ul>
                                <li>
                                    <a href="<?= Url::toRoute(['/core/index/contact-us']); ?>" title="<?= Yii::t('core', 'Contact Us') ?>">
                                        <?= Yii::t('core', 'Contact Us') ?>
                                    </a>
                                </li>
                                <?php
                                /*
                                ?>
                                <li>
                                    <a href="<?= Url::toRoute(['/pages/index/page', 'uri' => 'faq']); ?>" title="FAQs">
                                        FAQs
                                    </a>
                                </li>
                                */
                                ?>
                                <?php
                                /*
                                <li>
                                    <a href="<?= Url::toRoute(['/pages/index/page', 'uri' => 'shipping']); ?>" title="<?= Yii::t('core', 'Shipping') ?>">
                                        <?= Yii::t('core', 'Shipping') ?>
                                    </a>
                                </li>
                                */
                                ?>
                                <li>
                                    <a href="<?= Url::toRoute(['/pages/index/page', 'uri' => 'delivery-cost']); ?>" title="<?= Yii::t('core', 'Delivery and Cost') ?>">
                                        <?= Yii::t('core', 'Delivery and Cost') ?>
                                    </a>
                                </li>
                                <?php
                                /*
                                <li>
                                    <a href="<?= Url::toRoute(['/pages/index/page', 'uri' => 'return']); ?>" title="<?= Yii::t('core', 'Return') ?>">
                                        <?= Yii::t('core', 'Return') ?>
                                    </a>
                                </li>
                                */
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFooterTwo">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFooterTwo" aria-expanded="false" aria-controls="collapseFooterTwo"><?= Yii::t('core', 'The company') ?></button>
                        </h5>
                    </div>
                    <div class="collapse" id="collapseFooterTwo" aria-labelledby="headingFooterTwo" data-parent="#accordionFooter">
                        <div class="card-body">
                            <ul>
                                <li>
                                    <a href="<?= Url::toRoute(['/pages/index/page', 'uri' => 'about']); ?>" title="<?= Yii::t('core', 'About us') ?>">
                                        <?= Yii::t('core', 'About us') ?>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= Url::toRoute(['/careers']); ?>" title="<?= Yii::t('core', 'Careers') ?>">
                                        <?= Yii::t('core', 'Careers') ?>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= Url::toRoute(['/find-studio']); ?>" title="Find Studio">
                                        <?= Yii::t('core', 'Find Studio') ?>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFooterThree">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFooterThree" aria-expanded="false" aria-controls="collapseFooterThree"><?= Yii::t('core', 'Return') ?></button>
                        </h5>
                    </div>
                    <div class="collapse" id="collapseFooterThree" aria-labelledby="headingFooterThree" data-parent="#accordionFooter">
                        <div class="card-body">
                            <ul>
                                <li>
                                    <a href="<?= Url::toRoute(['/pages/index/page', 'uri' => 'impressum']); ?>" title="Impressum">
                                        Impressum
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= Url::toRoute(['/pages/index/page', 'uri' => 'cookie']); ?>" title="<?= Yii::t('core', 'Cookie policies') ?>">
                                        <?= Yii::t('core', 'Cookie policies') ?>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= Url::toRoute(['/pages/index/page', 'uri' => 'terms']); ?>" title="<?= Yii::t('core', 'Terms and Conditions') ?>">
                                        <?= Yii::t('core', 'Terms and Conditions') ?>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer__two-stay footer__two-parts">
            <div class="footer__find-us-wrapper">
                <p><?= Yii::t('core', 'Find us on') ?></p>
                <ul class="footer__two-stay-icons">
                    <li>
                        <a href="https://www.facebook.com/Infinity-Lashes-Academy-199428187583919/" title="Facebook" target="_blank">
                            <svg class="facebook svg">
                                <use xlink:href="#Facebook"></use>
                            </svg>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.instagram.com/infinitylashesshop" title="instagram" target="_blank">
                            <svg class="instagram svg">
                                <use xlink:href="#instagram"></use>
                            </svg>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="footer__two-family">
                <p><?= Yii::t('core', 'Family Brand') ?></p>
                <a target="_blank" href="http://lash-room.com/" title="<?= Yii::t('core', 'Family Brand') ?>">
                    <img src="/img/lash_room_logo_1.png" alt="<?= Yii::t('core', 'Family Brand') ?>">
                </a>
            </div>
        </div>
        <div class="footer__cards-we-accept">
            <svg class="mastercard-main svg">
                <use xlink:href="#mastercard-main"></use>
            </svg>
            <svg class="svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 40 30">
                <defs>
                    <style>
                    .cls-mc1 {
                        fill: none;
                    }

                    .cls-mc2 {
                        fill: #fff;
                    }

                    .cls-mc3 {
                        fill: #d6d6d6;
                    }

                    .cls-mc4 {
                        clip-path: url(#clip-path);
                    }

                    .cls-mc5 {
                        fill: url(#linear-gradient);
                    }
                    </style>
                    <clipPath id="clip-path">
                        <path class="cls-mc1" d="M13.84,10.33,11.32,16.7l-1-5.42a1.13,1.13,0,0,0-1.12-1H5.06L5,10.6a10.14,10.14,0,0,1,2.39.8,1,1,0,0,1,.58.82L9.9,19.7h2.56l3.93-9.37Zm3.59,0-2,9.37h2.42l2-9.37Zm13.9,2.53.73,3.52h-2ZM31,10.33a1.1,1.1,0,0,0-1,.69L26.3,19.7h2.55l.51-1.4h3.11l.29,1.4H35l-2-9.37ZM20.51,13.25c0,1.35,1.2,2.1,2.12,2.55s1.26.75,1.26,1.16c0,.63-.75.91-1.45.92A5.07,5.07,0,0,1,20,17.29l-.44,2.05a7.35,7.35,0,0,0,2.7.5c2.55,0,4.21-1.26,4.22-3.21,0-2.47-3.42-2.61-3.4-3.71,0-.34.33-.69,1-.78a4.58,4.58,0,0,1,2.39.42l.43-2a6.52,6.52,0,0,0-2.27-.42c-2.4,0-4.08,1.27-4.1,3.1" />
                    </clipPath>
                    <linearGradient id="linear-gradient" x1="-286.24" y1="411.23" x2="-286" y2="411.23" gradientTransform="matrix(126.55, 0, 0, -126.55, 36227.36, 52054.4)" gradientUnits="userSpaceOnUse">
                        <stop offset="0" stop-color="#241f5d" />
                        <stop offset="1" stop-color="#034ea1" />
                    </linearGradient>
                </defs>
                <title>Visa</title>
                <g id="Layer_2" data-name="Layer 2">
                    <g id="Layer_1-2" data-name="Layer 1">
                        <rect class="cls-mc2" x="0.35" y="0.35" width="39.29" height="29.29" rx="1.65" ry="1.65" />
                        <path class="cls-mc3" d="M38,.71A1.29,1.29,0,0,1,39.29,2V28A1.29,1.29,0,0,1,38,29.29H2A1.29,1.29,0,0,1,.71,28V2A1.29,1.29,0,0,1,2,.71H38M38,0H2A2,2,0,0,0,0,2V28a2,2,0,0,0,2,2H38a2,2,0,0,0,2-2V2a2,2,0,0,0-2-2Z" />
                        <g id="Visa">
                            <g class="cls-mc4">
                                <rect id="_Path_" data-name="&lt;Path&gt;" class="cls-mc5" x="5" y="10.16" width="30" height="9.69" />
                            </g>
                        </g>
                    </g>
                </g>
            </svg>
            <svg class="svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 40 30">
                <defs>
                    <style>
                    .cls3-1,
                    .cls3-2,
                    .cls3-3 {
                        fill: none;
                    }

                    .cls3-2 {
                        clip-rule: evenodd;
                    }

                    .cls3-3 {
                        fill-rule: evenodd;
                    }

                    .cls3-4 {
                        fill: #fff;
                    }

                    .cls3-5 {
                        fill: #d6d6d6;
                    }

                    .cls3-6 {
                        clip-path: url(#clip-path);
                    }

                    .cls3-7 {
                        fill: #253b80;
                    }

                    .cls3-8 {
                        fill: #179bd7;
                    }

                    .cls3-9 {
                        fill: #222d65;
                    }

                    .cls3-10 {
                        clip-path: url(#clip-path-4);
                    }
                    </style>
                    <clipPath id="clip-path">
                        <rect id="_Clipping_Path_" data-name="&lt;Clipping Path&gt;" class="cls3-1" x="2.5" y="10.34" width="35" height="9.31" />
                    </clipPath>
                    <clipPath id="clip-path-4">
                        <polyline id="_Clipping_Path_4" data-name="&lt;Clipping Path&gt;" class="cls3-2" points="37.5 10.34 37.5 19.66 2.5 19.66 2.5 10.34" />
                    </clipPath>
                </defs>
                <title>PayPal</title>
                <g id="Layer_2" data-name="Layer 2">
                    <g id="Layer_1-2" data-name="Layer 1">
                        <rect class="cls3-4" x="0.35" y="0.35" width="39.29" height="29.29" rx="1.65" ry="1.65" />
                        <path class="cls3-5" d="M38,.71A1.29,1.29,0,0,1,39.29,2V28A1.29,1.29,0,0,1,38,29.29H2A1.29,1.29,0,0,1,.71,28V2A1.29,1.29,0,0,1,2,.71H38M38,0H2A2,2,0,0,0,0,2V28a2,2,0,0,0,2,2H38a2,2,0,0,0,2-2V2a2,2,0,0,0-2-2Z" />
                        <g id="_Group_" data-name="&lt;Group&gt;">
                            <g id="_Group_6" data-name="&lt;Group&gt;">
                                <g id="_Group_7" data-name="&lt;Group&gt;">
                                    <g id="_Group_8" data-name="&lt;Group&gt;">
                                        <g id="_Group_9" data-name="&lt;Group&gt;">
                                            <path id="_Compound_Path_" data-name="&lt;Compound Path&gt;" class="cls3-7" d="M15.77,14.06c-.11.69-.63.69-1.15.69h-.29l.2-1.29a.16.16,0,0,1,.16-.14h.13c.35,0,.68,0,.85.2A.64.64,0,0,1,15.77,14.06Zm-.22-1.81H13.61a.27.27,0,0,0-.27.23l-.78,4.95a.16.16,0,0,0,.16.19h.92a.27.27,0,0,0,.27-.23L14.12,16a.27.27,0,0,1,.26-.23H15A2,2,0,0,0,17.2,14,1.49,1.49,0,0,0,17,12.74,1.79,1.79,0,0,0,15.54,12.25Z" />
                                            <path id="_Compound_Path_2" data-name="&lt;Compound Path&gt;" class="cls3-7" d="M20,15.83a1,1,0,0,1-1,.88.72.72,0,0,1-.77-.9,1,1,0,0,1,1-.89.78.78,0,0,1,.62.25A.8.8,0,0,1,20,15.83ZM21.31,14h-.92a.16.16,0,0,0-.16.14l0,.26-.06-.09A1.32,1.32,0,0,0,19,13.95,2.13,2.13,0,0,0,17,15.81a1.74,1.74,0,0,0,.34,1.42,1.45,1.45,0,0,0,1.16.47,1.77,1.77,0,0,0,1.28-.53l0,.26a.16.16,0,0,0,.16.19h.83a.27.27,0,0,0,.27-.23l.5-3.16A.16.16,0,0,0,21.31,14Z" />
                                            <path id="_Path_" data-name="&lt;Path&gt;" class="cls3-7" d="M26.24,14h-.93a.27.27,0,0,0-.22.12L23.8,16l-.54-1.81A.27.27,0,0,0,23,14h-.91a.16.16,0,0,0-.15.21l1,3-1,1.36a.16.16,0,0,0,.13.25h.93a.27.27,0,0,0,.22-.12l3.09-4.46A.16.16,0,0,0,26.24,14Z" />
                                            <path id="_Compound_Path_3" data-name="&lt;Compound Path&gt;" class="cls3-8" d="M29.53,14.06c-.11.69-.63.69-1.15.69H28.1l.2-1.29a.16.16,0,0,1,.16-.14h.13c.35,0,.68,0,.85.2A.64.64,0,0,1,29.53,14.06Zm-.22-1.81H27.38a.27.27,0,0,0-.26.23l-.78,4.95a.16.16,0,0,0,.16.19h1a.19.19,0,0,0,.19-.16l.22-1.4a.27.27,0,0,1,.26-.23h.61A2,2,0,0,0,31,14a1.49,1.49,0,0,0-.25-1.25A1.79,1.79,0,0,0,29.31,12.25Z" />
                                            <path id="_Compound_Path_4" data-name="&lt;Compound Path&gt;" class="cls3-8" d="M33.79,15.83a1,1,0,0,1-1,.88.72.72,0,0,1-.77-.9,1,1,0,0,1,1-.89.78.78,0,0,1,.62.25A.8.8,0,0,1,33.79,15.83ZM35.08,14h-.92a.16.16,0,0,0-.16.14l0,.26-.06-.09a1.32,1.32,0,0,0-1.09-.39,2.13,2.13,0,0,0-2.06,1.86,1.75,1.75,0,0,0,.34,1.42,1.45,1.45,0,0,0,1.16.47,1.77,1.77,0,0,0,1.28-.53l0,.26a.16.16,0,0,0,.16.19h.83a.27.27,0,0,0,.26-.23l.5-3.16A.16.16,0,0,0,35.08,14Z" />
                                            <path id="_Path_2" data-name="&lt;Path&gt;" class="cls3-8" d="M36.17,12.38l-.79,5a.16.16,0,0,0,.16.19h.8a.27.27,0,0,0,.27-.23l.78-4.95a.16.16,0,0,0-.16-.19h-.89A.16.16,0,0,0,36.17,12.38Z" />
                                            <path id="_Path_3" data-name="&lt;Path&gt;" class="cls3-7" d="M4.55,18.57l.15-.94H2.8l1.09-6.92a.09.09,0,0,1,0-.05l.06,0H6.63a2.36,2.36,0,0,1,1.8.54,1.24,1.24,0,0,1,.29.54,2,2,0,0,1,0,.75v.21l.15.08a1,1,0,0,1,.3.23,1.07,1.07,0,0,1,.24.55,2.31,2.31,0,0,1,0,.79,2.8,2.8,0,0,1-.33.9,1.85,1.85,0,0,1-.52.56,2.09,2.09,0,0,1-.69.31,3.46,3.46,0,0,1-.87.1H6.77a.62.62,0,0,0-.61.52l0,.08-.26,1.65v.06a.05.05,0,0,1,0,0H4.55Z" />
                                            <path id="_Path_4" data-name="&lt;Path&gt;" class="cls3-8" d="M9,12.51l0,.16a2.78,2.78,0,0,1-3.07,2.41H5.13a.38.38,0,0,0-.37.32l-.4,2.52-.11.71a.2.2,0,0,0,.2.23H5.82a.33.33,0,0,0,.33-.28v-.07l.26-1.65,0-.09a.33.33,0,0,1,.33-.28H7a2.43,2.43,0,0,0,2.69-2.11,1.8,1.8,0,0,0-.28-1.59A1.32,1.32,0,0,0,9,12.51Z" />
                                            <path id="_Path_5" data-name="&lt;Path&gt;" class="cls3-9" d="M8.64,12.36l-.16,0-.17,0a4.31,4.31,0,0,0-.68,0H5.54a.33.33,0,0,0-.33.28l-.44,2.8v.08a.38.38,0,0,1,.37-.32h.78A2.78,2.78,0,0,0,9,12.66l0-.16-.29-.12Z" />
                                            <path id="_Path_6" data-name="&lt;Path&gt;" class="cls3-7" d="M5.21,12.52a.33.33,0,0,1,.33-.28H7.62a4.31,4.31,0,0,1,.68,0l.17,0,.16,0,.08,0,.29.12A1.69,1.69,0,0,0,8.65,11a2.63,2.63,0,0,0-2-.64H4a.38.38,0,0,0-.37.32l-1.1,7a.23.23,0,0,0,.22.26H4.36l.41-2.6Z" />
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </svg>
            <svg class="svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 30">
                <defs>
                    <style>
                    .cls4-1 {
                        fill: #fff;
                    }

                    .cls4-2 {
                        fill: #d6d6d6;
                    }

                    .cls4-3 {
                        fill: #ee7f00;
                    }

                    .cls4-4 {
                        fill: #383a41;
                    }
                    </style>
                </defs>
                <title>Sofort</title>
                <g id="Layer_2" data-name="Layer 2">
                    <g id="Layer_1-2" data-name="Layer 1">
                        <rect class="cls4-1" x="0.35" y="0.35" width="39.29" height="29.29" rx="1.65" ry="1.65" />
                        <path class="cls4-2" d="M38,.71A1.29,1.29,0,0,1,39.29,2V28A1.29,1.29,0,0,1,38,29.29H2A1.29,1.29,0,0,1,.71,28V2A1.29,1.29,0,0,1,2,.71H38M38,0H2A2,2,0,0,0,0,2V28a2,2,0,0,0,2,2H38a2,2,0,0,0,2-2V2a2,2,0,0,0-2-2Z" />
                        <g id="layer1">
                            <path id="path66" class="cls4-3" d="M14.92,10.35a2.76,2.76,0,0,0-1.18.27,3.7,3.7,0,0,0-1.41,1.23,5.36,5.36,0,0,0-.65,1.25,2.28,2.28,0,0,0,0,2.08,1.56,1.56,0,0,0,1.37.59h0c1.45,0,2.53-.95,3.21-2.82a2.37,2.37,0,0,0,0-2,1.53,1.53,0,0,0-1.34-.57Zm8.74,0a2.76,2.76,0,0,0-1.18.27,3.69,3.69,0,0,0-1.41,1.23,5.34,5.34,0,0,0-.65,1.25,2.28,2.28,0,0,0,0,2.08,1.56,1.56,0,0,0,1.37.59h0c1.45,0,2.53-.95,3.21-2.82a2.37,2.37,0,0,0,0-2A1.53,1.53,0,0,0,23.66,10.35Zm-13.39.08a2.92,2.92,0,0,0-1.35.21,2.33,2.33,0,0,0-1.06,1.47,1.67,1.67,0,0,0-.1.47.71.71,0,0,0,.07.35,1,1,0,0,0,.29.33,1.45,1.45,0,0,0,.45.23l.2.06.31.09.14.06a.28.28,0,0,1,.11.11.25.25,0,0,1,0,.11.35.35,0,0,1,0,.13.34.34,0,0,1-.21.23,1.53,1.53,0,0,1-.52.06H5.49L5,15.7H8.33a3.63,3.63,0,0,0,1.44-.23,2.21,2.21,0,0,0,1.3-1.64A1.11,1.11,0,0,0,11,13a1.23,1.23,0,0,0-.75-.49l-.18-.05-.26-.08a.35.35,0,0,1-.21-.15.29.29,0,0,1,0-.24.35.35,0,0,1,.17-.21.7.7,0,0,1,.34-.07H12l0-.06a4.27,4.27,0,0,1,1.25-1.19Zm8.7,0a1.81,1.81,0,0,0-1.8,1.25l-1.46,4h1.6l.75-2.07h1.42l.4-1.11H18.46l.23-.64a.34.34,0,0,1,.33-.21h1.67l0-.06A4.51,4.51,0,0,1,22,10.43H19Zm7.43,0L24.49,15.7h1.63l.7-1.86c0,.08.45,1.86.45,1.86H29a16.44,16.44,0,0,0-.54-1.84,2.89,2.89,0,0,0-.2-.42,2.4,2.4,0,0,0,1.48-1.35,1.22,1.22,0,0,0,0-1.16,1.72,1.72,0,0,0-1.5-.52H26.4Zm3.34,0a1.32,1.32,0,0,1,.27.28,1.37,1.37,0,0,1,.22,1h1l-1.46,4H31.2l1.46-4h1.88L35,10.43Zm-2.27,1.09h.27c.49,0,.68.14.51.59a.94.94,0,0,1-1,.68H27l.47-1.27Zm-12.94,0h0c.5,0,.62.29.18,1.49h0c-.45,1.24-.78,1.55-1.27,1.55h0c-.48,0-.65-.3-.2-1.54.34-.92.77-1.51,1.3-1.51Zm8.74,0h0c.5,0,.62.29.18,1.49s-.78,1.55-1.27,1.55h0c-.48,0-.65-.3-.2-1.54.34-.92.77-1.51,1.3-1.51Z" />
                            <path id="path98" class="cls4-4" d="M6.58,16.25a.4.4,0,0,0-.23.06.42.42,0,0,0-.13.2c0,.09,0,.16,0,.19s.09.05.19.05a.39.39,0,0,0,.23-.06.42.42,0,0,0,.13-.2c0-.09,0-.16,0-.19S6.69,16.25,6.58,16.25Zm.88,0a.4.4,0,0,0-.23.06.42.42,0,0,0-.13.2c0,.09,0,.16,0,.19s.09.05.19.05a.39.39,0,0,0,.23-.06.42.42,0,0,0,.13-.2c0-.09,0-.16,0-.19S7.57,16.25,7.47,16.25Zm16.5.69a1.41,1.41,0,0,0-.36,0,1.21,1.21,0,0,0-.33.14,1.17,1.17,0,0,0-.27.24,1.07,1.07,0,0,0-.19.33.73.73,0,0,0-.05.28.47.47,0,0,0,.05.2.47.47,0,0,0,.12.15l.16.11.17.09.14.09a.28.28,0,0,1,.09.11.21.21,0,0,1,0,.15.48.48,0,0,1-.2.24l-.15.07-.18,0a.79.79,0,0,1-.25,0l-.17-.07L22.44,19l-.07,0h0l0,0,0,.07,0,.1a.76.76,0,0,0,0,.14.11.11,0,0,0,0,.08l.06.06.12.07.19.05.24,0a1.52,1.52,0,0,0,.4-.05,1.37,1.37,0,0,0,.36-.16,1.27,1.27,0,0,0,.3-.26,1.16,1.16,0,0,0,.21-.36.71.71,0,0,0,.05-.27.46.46,0,0,0-.05-.2.48.48,0,0,0-.13-.15l-.17-.11L23.6,18l-.15-.09a.27.27,0,0,1-.09-.11.21.21,0,0,1,0-.15.41.41,0,0,1,.06-.11.38.38,0,0,1,.1-.09l.13-.06.16,0a.62.62,0,0,1,.19,0l.14.06.1.06.06,0h0l0,0,0-.07,0-.1,0-.09v-.06a.15.15,0,0,0,0,0v0L24.43,17,24.31,17l-.16,0H24Zm8.4,0a1.88,1.88,0,0,0-.6.1,1.9,1.9,0,0,0-.53.27,2.07,2.07,0,0,0-.43.43,2.27,2.27,0,0,0-.31.58,1.61,1.61,0,0,0-.11.56.76.76,0,0,0,.11.42.69.69,0,0,0,.33.26,1.37,1.37,0,0,0,.53.09h.22l.22,0,.22,0,.18-.05a.26.26,0,0,0,.11-.07.36.36,0,0,0,.06-.11l.38-1a.24.24,0,0,0,0-.07.11.11,0,0,0,0-.05l0,0h-1l0,0,0,.06,0,.09a.44.44,0,0,0,0,.15s0,0,0,0h.5l-.22.61-.18.06-.18,0a.75.75,0,0,1-.31-.06A.41.41,0,0,1,31,19a.56.56,0,0,1-.06-.29,1.24,1.24,0,0,1,.08-.4,1.56,1.56,0,0,1,.19-.37,1.39,1.39,0,0,1,.27-.29,1.22,1.22,0,0,1,.33-.19,1.14,1.14,0,0,1,.67,0l.2.08.13.08.07,0h0l0,0,0-.07,0-.1a.78.78,0,0,0,0-.15.12.12,0,0,0,0-.08L33,17.09,32.89,17,32.67,17ZM15.83,17h-.16l-.09,0a.11.11,0,0,0,0,.06.65.65,0,0,0,0,.11l-.24,2.28a.22.22,0,0,0,0,.08.07.07,0,0,0,0,0l.1,0h.37l.12,0,.07,0,.06-.08,1-1.66h0l-.18,1.66a.2.2,0,0,0,0,.08.07.07,0,0,0,0,0l.1,0h.35l.12,0,.08,0,.06-.08L19,17.19l.06-.12a.07.07,0,0,0,0-.07A.08.08,0,0,0,19,17h-.29l-.09,0-.05,0,0,.06-1.19,2h0l.23-2a.19.19,0,0,0,0-.06.06.06,0,0,0,0,0l-.08,0h-.3l-.09,0-.06,0,0,.06-1.2,2h0l.27-2a.19.19,0,0,0,0-.06,0,0,0,0,0,0,0h-.23Zm6.31,0H22l-.09,0-.05,0,0,0-.9,2.47a0,0,0,0,0,0,0l0,0,.08,0h.26l.09,0,.05,0,0,0,.9-2.47a0,0,0,0,0,0,0l0,0-.07,0h-.13ZM6,17H5.83l-.09,0-.05,0,0,0-.58,1.59a1.24,1.24,0,0,0-.09.44.54.54,0,0,0,.09.31.53.53,0,0,0,.26.19,1.26,1.26,0,0,0,.42.06,1.69,1.69,0,0,0,.49-.07A1.4,1.4,0,0,0,7,19.06a1.7,1.7,0,0,0,.23-.43l.57-1.57a0,0,0,0,0,0,0l0,0L7.7,17H7.44l-.09,0L7.3,17l0,0-.57,1.58a1,1,0,0,1-.13.25.84.84,0,0,1-.17.18.72.72,0,0,1-.21.11.74.74,0,0,1-.24,0,.53.53,0,0,1-.21,0,.28.28,0,0,1-.13-.11.35.35,0,0,1,0-.19.88.88,0,0,1,.06-.27l.56-1.54a0,0,0,0,0,0,0l0,0-.07,0H6Zm19.62,0h-.13l-.09,0-.05,0,0,0-.58,1.59a1.23,1.23,0,0,0-.09.44.54.54,0,0,0,.09.31.53.53,0,0,0,.26.19,1.26,1.26,0,0,0,.42.06,1.69,1.69,0,0,0,.49-.07,1.4,1.4,0,0,0,.74-.52,1.71,1.71,0,0,0,.23-.43l.57-1.57a0,0,0,0,0,0,0l0,0-.07,0h-.25L27,17l-.05,0,0,0-.57,1.58a1.06,1.06,0,0,1-.13.25.85.85,0,0,1-.17.18.72.72,0,0,1-.21.11.73.73,0,0,1-.24,0,.53.53,0,0,1-.21,0,.28.28,0,0,1-.13-.11.34.34,0,0,1,0-.19.88.88,0,0,1,.06-.27l.56-1.54a0,0,0,0,0,0,0l0,0-.07,0h-.13Zm4.55,0H30l-.08,0-.05,0,0,0-.45,1.23-.1.29-.1.3h0c0-.05,0-.11,0-.16l0-.16,0-.17,0-.17-.16-1a.75.75,0,0,0,0-.13.21.21,0,0,0,0-.08l-.08,0H28.4a.28.28,0,0,0-.15,0,.28.28,0,0,0-.11.14l-.86,2.35a.06.06,0,0,0,0,0l0,0,.07,0h.24l.08,0,0,0,0,0,.5-1.37.1-.3.09-.29h0c0,.08,0,.16,0,.25s0,.16,0,.24l.21,1.22a1.45,1.45,0,0,0,0,.16.29.29,0,0,0,0,.1.15.15,0,0,0,.08.05h.42l.08,0,.07-.06a.29.29,0,0,0,0-.08l.86-2.35a.06.06,0,0,0,0,0l0,0-.07,0h-.11ZM11.25,17a.23.23,0,0,0-.13,0,.25.25,0,0,0-.09.13l-.83,2.28c0,.06,0,.1,0,.13a.12.12,0,0,0,.1,0h1.35l0,0,0-.06,0-.1,0-.1a.24.24,0,0,0,0-.06.05.05,0,0,0,0,0h-1l.27-.74h.83l0,0,0-.06,0-.1,0-.1a.21.21,0,0,0,0-.06.05.05,0,0,0,0,0h-.82l.23-.64h1l0,0,0-.06,0-.1,0-.1a.26.26,0,0,0,0-.06.05.05,0,0,0,0,0H11.25Zm2.19,0a.23.23,0,0,0-.13,0,.25.25,0,0,0-.09.13l-.86,2.38a.05.05,0,0,0,0,0l0,0,.08,0h.26l.09,0,.05,0,0,0,.36-1h.17a.32.32,0,0,1,.14,0,.2.2,0,0,1,.09.08.37.37,0,0,1,0,.13c0,.05,0,.11,0,.18l0,.58a.12.12,0,0,0,0,0l0,0,.07,0h.39l0,0,0,0a.22.22,0,0,0,0-.06s0-.06,0-.11l0-.52a1.27,1.27,0,0,0,0-.17.61.61,0,0,0,0-.13.34.34,0,0,0-.06-.1l-.09-.07a1.29,1.29,0,0,0,.25-.1,1.08,1.08,0,0,0,.21-.15,1,1,0,0,0,.17-.19,1.11,1.11,0,0,0,.12-.24.78.78,0,0,0,.05-.28.38.38,0,0,0-.22-.35.82.82,0,0,0-.26-.08h-1Zm6.39,0a.23.23,0,0,0-.13,0,.25.25,0,0,0-.09.13l-.83,2.28c0,.06,0,.1,0,.13a.12.12,0,0,0,.1,0h1.35l0,0,0-.06,0-.1h0l0-.1a.24.24,0,0,0,0-.06.05.05,0,0,0,0,0h-1l.27-.74h.83l0,0,0-.06,0-.1,0-.1a.21.21,0,0,0,0-.06.05.05,0,0,0,0,0h-.82l.23-.64h1l0,0,0-.06,0-.1,0-.1a.25.25,0,0,0,0-.06.05.05,0,0,0,0,0H19.84ZM8.74,17a.23.23,0,0,0-.13,0,.25.25,0,0,0-.09.13l-.83,2.28c0,.06,0,.1,0,.13a.12.12,0,0,0,.1,0h.7l.29,0L9,19.52l.24-.1a1.17,1.17,0,0,0,.22-.15,1.14,1.14,0,0,0,.18-.2,1.07,1.07,0,0,0,.13-.25.69.69,0,0,0,0-.24.42.42,0,0,0,0-.19.34.34,0,0,0-.12-.13.49.49,0,0,0-.18-.07l.19-.08L9.87,18a.89.89,0,0,0,.13-.15.88.88,0,0,0,.09-.18.65.65,0,0,0,0-.3.33.33,0,0,0-.1-.21A.53.53,0,0,0,9.79,17a1.57,1.57,0,0,0-.39,0Zm.22.4h.27l.19,0a.21.21,0,0,1,.1.06.17.17,0,0,1,0,.1.39.39,0,0,1,0,.14.56.56,0,0,1-.07.13.52.52,0,0,1-.11.11L9.2,18a.6.6,0,0,1-.19,0h-.3Zm4.71,0h.38l.09,0a.2.2,0,0,1,.15.13.35.35,0,0,1,0,.22.57.57,0,0,1-.08.15.53.53,0,0,1-.13.12.62.62,0,0,1-.17.08l-.21,0H13.4Zm-5.1,1.06h.32a.77.77,0,0,1,.23,0,.26.26,0,0,1,.12.07.2.2,0,0,1,0,.12.41.41,0,0,1,0,.16.52.52,0,0,1-.09.15A.55.55,0,0,1,9,19.1l-.16.07-.2,0H8.3Z" />
                        </g>
                    </g>
                </g>
            </svg>
        </div>
    </div>
    <div class="container footer__two-rights">
        <hr class="before-end">
        <p>© Infinity Lashes <?= (date('Y') == 2018) ? '2018' : '2018 - ' . date('Y'); ?>. All rights reserved.</p>
    </div>
    <div class="svgHide">
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100" height="100" style="position:absolute">
            <symbol id="totop" viewBox="0 0 39.5 60">
                <title>totop</title>
                <path d="M0 1.5L36.3 30 0 58.5 1.2 60l38.3-30L1.2 0 0 1.5z"></path>
            </symbol>
            <symbol id="Facebook" viewBox="0 0 50 50">
                <title>Facebook</title>
                <g data-name="Layer 1">
                    <path d="M26.82 19.31c0-1 .1-1.52 1.62-1.52h2V14h-3.22c-3.91 0-5.29 1.84-5.29 4.93v2.28H19.5V25h2.44v11h4.88V25h3.25l.43-3.79h-3.69z"></path>
                    <path d="M25 0a25 25 0 1 0 25 25A25 25 0 0 0 25 0zm0 47a22 22 0 1 1 22-22 22 22 0 0 1-22 22z"></path>
                </g>
            </symbol>
            <symbol id="Twitter" viewBox="0 0 50 50">
                <title>Twitter</title>
                <g data-name="Layer 1">
                    <path d="M24.64 19.91zM24.68 20.45V20zM24.68 20.55zM24.68 20.48zM24.65 20zM25 0a25 25 0 1 0 25 25A25 25 0 0 0 25 0zm0 47a22 22 0 1 1 22-22 22 22 0 0 1-22 22z"></path>
                    <path d="M35.25 18c-1.35.5-1.54.44-.87-.32a4.92 4.92 0 0 0 1.08-1.79c0-.05-.24 0-.51.18a8.64 8.64 0 0 1-1.4.57l-.86.29-.78-.55a6.37 6.37 0 0 0-1.35-.74 5.2 5.2 0 0 0-2.78.07 4.81 4.81 0 0 0-3.1 4.83v.82l-.79-.1a12.72 12.72 0 0 1-7.57-3.89l-1-1.09-.32.82a5 5 0 0 0 1 5c.64.7.49.81-.6.39-.38-.13-.72-.23-.75-.18a6.23 6.23 0 0 0 .57 2.25 5.24 5.24 0 0 0 2.18 2.15l.78.39h-.92c-.89 0-.92 0-.83.37a4.89 4.89 0 0 0 3 2.75l1 .35-.86.54a8.66 8.66 0 0 1-4.26 1.24 4.51 4.51 0 0 0-1.3.13 11.87 11.87 0 0 0 3.07 1.48 12.92 12.92 0 0 0 10.42-1.28 14.43 14.43 0 0 0 5.29-6.51 18 18 0 0 0 1.08-5c0-.77 0-.87.94-1.79a10.82 10.82 0 0 0 1.11-1.29c.16-.35.14-.35-.67-.09z"></path>
                </g>
            </symbol>
            <symbol id="Pinterest" viewBox="0 0 50 50">
                <title>Pinterest</title>
                <g data-name="Layer 1">
                    <path d="M25.58 12c-6.7 0-10.08 5.09-10.08 9.33 0 2.57.92 4.85 2.89 5.71a.48.48 0 0 0 .71-.37c.07-.26.22-.92.29-1.2a.75.75 0 0 0-.2-.83 4.47 4.47 0 0 1-.93-2.93 6.88 6.88 0 0 1 6.94-7.15c3.79 0 5.87 2.45 5.87 5.72 0 4.31-1.8 7.94-4.47 7.94a2.28 2.28 0 0 1-2.22-2.88c.42-1.89 1.24-3.93 1.24-5.3s-.62-2.24-1.9-2.24c-1.51 0-2.72 1.65-2.72 3.87a6 6 0 0 0 .43 2.33l-1.81 8.14a17.74 17.74 0 0 0 0 5.68.18.18 0 0 0 .34.09 16.2 16.2 0 0 0 2.5-4.91c.17-.66 1-4.11 1-4.11a4 4 0 0 0 3.44 1.86c4.53 0 7.61-4.38 7.61-10.24A8.64 8.64 0 0 0 25.58 12z"></path>
                    <path d="M25 0a25 25 0 1 0 25 25A25 25 0 0 0 25 0zm0 47a22 22 0 1 1 22-22 22 22 0 0 1-22 22z"></path>
                </g>
            </symbol>
            <symbol id="Youtube" viewBox="0 0 50 50">
                <title>Youtube</title>
                <g data-name="Layer 1">
                    <path d="M25 0a25 25 0 1 0 25 25A25 25 0 0 0 25 0zm0 47a22 22 0 1 1 22-22 22 22 0 0 1-22 22z"></path>
                    <path d="M34.14 16.84A81.9 81.9 0 0 0 25 16.5a81.9 81.9 0 0 0-9.13.34 3.08 3.08 0 0 0-2.64 2.4A26.16 26.16 0 0 0 12.8 25a26.14 26.14 0 0 0 .42 5.77 3.08 3.08 0 0 0 2.64 2.4 81.9 81.9 0 0 0 9.14.33 81.9 81.9 0 0 0 9.13-.34 3.08 3.08 0 0 0 2.64-2.4A26.16 26.16 0 0 0 37.2 25a26.14 26.14 0 0 0-.42-5.77 3.08 3.08 0 0 0-2.64-2.39zM22.8 28.29V20.5l6.75 3.9z"></path>
                </g>
            </symbol>
            <symbol id="instagram" viewBox="0 0 50 50">
                <title>instagram</title>
                <g data-name="Layer 1">
                    <path d="M25 0a25 25 0 1 0 25 25A25 25 0 0 0 25 0zm0 47a22 22 0 1 1 22-22 22 22 0 0 1-22 22z"></path>
                    <path d="M33.95 13.52H16.17l-.32.06A2.44 2.44 0 0 0 14 15.65v18.58c0 .12 0 .23.06.35a2.42 2.42 0 0 0 2 1.9H33.8l.34-.06A2.44 2.44 0 0 0 36 34.51V15.8v-.3a2.43 2.43 0 0 0-2.05-1.98zm-3.3 1.84h2.08a.91.91 0 0 1 .89.94v2.18a.9.9 0 0 1-.85.92h-2.09a.89.89 0 0 1-.87-.62 1.09 1.09 0 0 1-.05-.32V16.3a.91.91 0 0 1 .89-.93zm-5.65 5a4.67 4.67 0 1 1-4.46 4.59A4.57 4.57 0 0 1 25 20.34zm8.62 2.56v10.2a.91.91 0 0 1-.88.91H17.25a.91.91 0 0 1-.87-.91V22.85h2.09a7.43 7.43 0 0 0 .53 5.59 6.84 6.84 0 0 0 2.66 2.8 6.59 6.59 0 0 0 7.17-.32 7 7 0 0 0 2-2.2 7.25 7.25 0 0 0 .95-2.87 7.5 7.5 0 0 0-.27-3h2.1z"></path>
                </g>
            </symbol>
            <symbol id="close" viewBox="0 0 20 20">
                <path transform="rotate(-45 7.498 7.504)" d="M6.84-2.44h1.33v19.89H6.84z"></path>
                <path transform="rotate(45 7.504 7.498)" d="M6.84-2.44h1.33v19.89H6.84z"></path>
            </symbol>
            <symbol id="date" viewBox="0 0 20 20">
                <defs>
                    <style>
                    .cls-1 {
                        fill: #fff;
                        fill-opacity: .9
                    }
                    </style>
                </defs>
                <title>date</title>
                <g id="Layer_2" data-name="Layer 2">
                    <g id="Layer_1-2" data-name="Layer 1">
                        <g id="Page-1">
                            <g id="Icons-Device">
                                <g id="access-time">
                                    <path id="Shape" class="cls-1" d="M10 0a10 10 0 1 0 10 10A10 10 0 0 0 10 0zm0 18a8 8 0 1 1 8-8 8 8 0 0 1-8 8zm.5-13H9v6l5.2 3.2.8-1.3-4.5-2.7z"></path>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </symbol>
            <symbol id="attach" viewBox="0 0 20 18.09">
                <title>Attach</title>
                <g data-name="Layer 2">
                    <g data-name="Layer 1">
                        <g data-name="Layer 2">
                            <g data-name="Layer 1-2">
                                <g id="_9b52b8e5-067a-4267-95bf-348ae06549fd.psd" data-name="9b52b8e5-067a-4267-95bf-348ae06549fd.psd">
                                    <path d="M18.9 1.09a3.79 3.79 0 0 0-5.32 0l-10 9.85V11a2.47 2.47 0 0 0 .14 3.37 2.55 2.55 0 0 0 3.57 0l6.54-6.48s.29-.28.05-.54a2.94 2.94 0 0 0-.41-.35.42.42 0 0 0-.52.06l-6.54 6.51a1.34 1.34 0 0 1-1.88 0 1.31 1.31 0 0 1 0-1.85l9.9-9.79a2.59 2.59 0 0 1 3.63 0 2.52 2.52 0 0 1 0 3.56l-10 9.85-.47.47a3.8 3.8 0 0 1-5.33 0 3.7 3.7 0 0 1 0-5.23L8.08 4.8a.32.32 0 0 0 0-.45 5.65 5.65 0 0 0-.45-.45c-.21-.17-.43.1-.43.1L1.45 9.72a4.87 4.87 0 0 0-.05 6.89l.05.05a5 5 0 0 0 7 0L18.9 6.34a3.69 3.69 0 0 0 0-5.22z" id="icon-paperclip_copy" data-name="icon-paperclip copy"></path>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </symbol>
            <symbol id="arrow-new" viewBox="0 0 22.71 64">
                <defs>
                    <style>
                    .cls-1-an {
                        fill: none;
                        stroke-miterlimit: 10;
                        stroke-width: 2px
                    }
                    </style>
                </defs>
                <title>arrow</title>
                <g id="Layer_2" data-name="Layer 2">
                    <path class="cls-1-an" d="M.84.55l20.67 31.71L.84 63.45" id="arrow"></path>
                </g>
            </symbol>
            <symbol id="shop-plus" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
                <style>
                .ast0 {
                    fill: #010101
                }
                </style>
                <path class="ast0" d="M80.2 51.6H51.4v-29h-2.5v29h-29v2.5h29v29h2.5v-29h29v-2.5z" />
            </symbol>
            <symbol id="shop-minus" viewBox="0 0 60.5 2.5" xmlns="http://www.w3.org/2000/svg">
                <defs>
                    <style>
                    .acls-min-1 {
                        fill: #010101
                    }
                    </style>
                </defs>
                <g id="aСлой_2" data-name="Слой 2">
                    <path class="acls-min-1" d="M0 0h60.5v2.5H0z" id="aLayer_1" data-name="Layer 1" />
                </g>
            </symbol>
            <symbol id="mastercard-main" viewBox="0 0 40 30" xmlns="http://www.w3.org/2000/svg">
                <defs>
                    <style>
                    .acls-1 {
                        fill: #fff
                    }

                    .acls-2 {
                        fill: #d6d6d6
                    }

                    .acls-3 {
                        fill: #010101
                    }

                    .acls-4 {
                        fill: #f16522
                    }

                    .acls-5 {
                        fill: #e41b24
                    }

                    .acls-6 {
                        fill: #f89e1c
                    }
                    </style>
                </defs>
                <g id="aLayer_2" data-name="Layer 2">
                    <g id="aLayer_1-2" data-name="Layer 1">
                        <rect class="acls-1" x=".35" y=".35" width="39.29" height="29.29" rx="1.65" ry="1.65" />
                        <path class="acls-2" d="M38 .71A1.29 1.29 0 0 1 39.29 2v26A1.29 1.29 0 0 1 38 29.29H2A1.29 1.29 0 0 1 .71 28V2A1.29 1.29 0 0 1 2 .71h36M38 0H2a2 2 0 0 0-2 2v26a2 2 0 0 0 2 2h36a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2z" />
                        <g id="a_Group_" data-name="&lt;Group>">
                            <path id="a_Compound_Path_" data-name="&lt;Compound Path>" class="acls-3" d="M10.46 26.54V25a.92.92 0 0 0-1-1 1 1 0 0 0-.87.44.9.9 0 0 0-.78-.44.81.81 0 0 0-.72.37v-.3h-.54v2.46h.54v-1.36a.58.58 0 0 1 .6-.65c.36 0 .54.23.54.65v1.37h.54v-1.37a.58.58 0 0 1 .6-.65c.37 0 .54.23.54.65v1.37zm8-2.46h-.88v-.75H17v.75h-.5v.49h.5v1.12c0 .57.22.91.85.91a1.25 1.25 0 0 0 .67-.19l-.15-.46a1 1 0 0 1-.47.14c-.27 0-.36-.16-.36-.41v-1.12h.88zM23 24a.73.73 0 0 0-.65.36v-.3h-.53v2.46h.54v-1.36c0-.41.18-.63.53-.63a.88.88 0 0 1 .33.06l.16-.5A1.15 1.15 0 0 0 23 24m-6.91.26a1.84 1.84 0 0 0-1-.26c-.62 0-1 .3-1 .79s.3.65.85.73h.25c.29 0 .43.12.43.26s-.2.3-.56.3a1.31 1.31 0 0 1-.82-.26l-.25.42a1.78 1.78 0 0 0 1.07.32c.71 0 1.12-.33 1.12-.8s-.32-.66-.86-.74h-.25c-.23 0-.42-.08-.42-.24s.17-.29.47-.29a1.59 1.59 0 0 1 .77.21zM30.47 24a.73.73 0 0 0-.65.36v-.3h-.53v2.46h.54v-1.36c0-.41.18-.63.53-.63a.88.88 0 0 1 .33.06l.16-.5a1.15 1.15 0 0 0-.38-.07m-6.9 1.29a1.24 1.24 0 0 0 1.31 1.29 1.29 1.29 0 0 0 .89-.29l-.26-.43a1.08 1.08 0 0 1-.64.22.79.79 0 0 1 0-1.58 1.08 1.08 0 0 1 .64.22l.26-.43a1.29 1.29 0 0 0-.89-.29 1.24 1.24 0 0 0-1.31 1.29m5 0v-1.24H28v.3a.93.93 0 0 0-.78-.36 1.29 1.29 0 0 0 0 2.59.93.93 0 0 0 .78-.36v.3h.54zm-2 0a.75.75 0 1 1 .75.79.74.74 0 0 1-.75-.79M20.13 24a1.29 1.29 0 0 0 0 2.59 1.5 1.5 0 0 0 1-.35l-.26-.4a1.17 1.17 0 0 1-.72.26.68.68 0 0 1-.74-.6h1.83v-.21A1.18 1.18 0 0 0 20.13 24m0 .48a.61.61 0 0 1 .62.6h-1.29a.63.63 0 0 1 .65-.6m13.44.81v-2.2H33v1.29a.93.93 0 0 0-.78-.36 1.29 1.29 0 0 0 0 2.59.93.93 0 0 0 .78-.36v.3h.54zm-2 0a.75.75 0 1 1 .75.79.74.74 0 0 1-.75-.79m-18.09 0v-1.22h-.54v.3a.93.93 0 0 0-.78-.36 1.29 1.29 0 0 0 0 2.59.94.94 0 0 0 .78-.36v.3h.54zm-2 0a.75.75 0 1 1 .75.79.74.74 0 0 1-.75-.79m23 .87h.1l.08.05a.26.26 0 0 1 .05.08.24.24 0 0 1 0 .1.24.24 0 0 1 0 .09.25.25 0 0 1-.05.08l-.08.05h-.2l-.08-.05a.24.24 0 0 1-.05-.08.24.24 0 0 1 0-.09.24.24 0 0 1 0-.1.25.25 0 0 1 .05-.08l.08-.05h.1m0 .44h.13v-.06a.2.2 0 0 0 0-.15v-.06h-.27v.06a.2.2 0 0 0 0 .15v.06h.14m0-.31h.07a.07.07 0 0 1 0 .05h-.05l.07.09h-.05l-.07-.09v.09-.23zm-.06 0v.06h.09-.09z" />
                            <path id="a_Path_" data-name="&lt;Path>" class="acls-4" d="M15.94 5.31h8.11v14.58h-8.11z" />
                            <path id="a_Path_2" data-name="&lt;Path>" class="acls-5" d="M16.46 12.6A9.26 9.26 0 0 1 20 5.31a9.27 9.27 0 1 0 0 14.58 9.26 9.26 0 0 1-3.54-7.29" />
                            <path id="a_Compound_Path_2" data-name="&lt;Compound Path>" class="acls-6" d="M35 12.6a9.27 9.27 0 0 1-15 7.29 9.27 9.27 0 0 0 0-14.58 9.27 9.27 0 0 1 15 7.29m-.88 5.75V18h.12-.31.07v.3zm.6 0V18h-.09l-.11.25-.12-.25h-.09v.36h.07v-.27l.1.23h.07l.1-.23v.27z" />
                        </g>
                    </g>
                </g>
            </symbol>
        </svg>
    </div>
</footer>