<?php

use yii\helpers\Url;

?>

<div class="main-enter__singin">
    <a href="<?= (Yii::$app->user->isGuest) ? Url::toRoute(['/user/security/login']) : Url::toRoute(['/user/user/account']); ?>">
        <svg class="account svg">
            <use xlink:href="#account"></use>
        </svg>
    </a>

    <?php if (Yii::$app->user->isGuest) : ?>

        <a href="<?= Url::toRoute(['/user/security/login']); ?>" title="<?= Yii::t('core', 'Sign in') ?> / <?= Yii::t('core', 'Join') ?>">
            <?= Yii::t('core', 'Sign in') ?> / <?= Yii::t('core', 'Join') ?>
        </a>

    <?php else : ?>

        <a href="<?= Url::toRoute(['/user/user/account']); ?>"><?= Yii::$app->user->identity->fullName; ?></a>

    <?php endif; ?>

</div>