<?php

namespace frontend\modules\core\validators;

use yii\validators\Validator;

/**
 * Phone validator.
 */
class PhoneValidator extends Validator
{
    /**
     * German phone pattern.
     *
     * @var string $germanPatternPhone
     */
    private $germanPatternPhone = '#^\+49\s\d{3}\s\d{3}\s\d{2}\s\d{2,3}$#';

    /**
     * Validate gernam phone.
     *
     * @return void
     */
    public function validateAttribute($model, $attribute)
    {
        if (empty($model->{$attribute})) {
            $this->addError($model, $attribute, 'Phone can not be empty');
        }

        if (!preg_match($this->germanPatternPhone, $model->{$attribute})) {
            $this->addError($model, $attribute, 'Phone number not correct');
        }
    }

    /**
     * Gernam phone mask.
     *
     * @return string
     */
    public static function getGermanPhoneMask()
    {
        return '+4\9 999 999 99 99[9]';
    }
}