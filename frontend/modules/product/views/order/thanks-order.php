<?php

use frontend\modules\core\models\Setting;
use yii\helpers\Url;
use frontend\modules\product\widgets\SmallSliderWithProducts;
use frontend\modules\product\models\ProductCategory;
use frontend\modules\product\models\ProductVariation;

$this->bodyClass = 'checkout-thanks-page';

/** @var \yii\web\View */
/** @var \frontend\modules\product\models\Order $order */
/** @var \frontend\modules\product\models\ProductVariation $orderProductVariation */
/** @var \frontend\modules\product\models\ProductVariationPropertie $property */

?>
<div class="svgHide">
    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="0" height="0" style="position:absolute">
        <symbol id="account" viewBox="0 0 25 23">
            <title>account</title>
            <path d="M12.5 0A5.5 5.5 0 1 0 18 5.5 5.5 5.5 0 0 0 12.5 0zm0 10A4.5 4.5 0 1 1 17 5.5a4.5 4.5 0 0 1-4.5 4.5zm0 2A12.56 12.56 0 0 0 0 23h25a12.56 12.56 0 0 0-12.5-11zm-11 10c.66-5.07 5.33-9 11-9s10.34 3.93 11 9z" data-name="Layer 1"></path>
        </symbol>
        <symbol id="Cart" viewBox="0 0 50 50">
            <defs>
                <style>
                .cls-1-cart,
                .cls-2-cart {
                    fill: none
                }

                .cls-1-cart {
                    stroke: #000;
                    stroke-linecap: round;
                    stroke-miterlimit: 10;
                    stroke-width: 2px
                }
                </style>
            </defs>
            <title>Cart</title>
            <g id="Layer_2" data-name="Layer 2">
                <g id="Layer_1-2" data-name="Layer 1">
                    <path class="cls-1-cart" d="M8 14L4 49h42l-4-35z"></path>
                    <path class="cls-2-cart" d="M0 0h50v50H0z"></path>
                    <path class="cls-1-cart" d="M34 19v-8a9 9 0 0 0-18 0v8"></path>
                    <circle cx="34" cy="19" r="2"></circle>
                    <circle cx="16" cy="19" r="2"></circle>
                </g>
            </g>
        </symbol>
    </svg>
</div>
<main class="container">
    <h1><?= Yii::t('product', 'Thank you for your order, {0} {1}!', [$order->orderShippingInformation->humanTitle, $order->orderShippingInformation->surname]); ?></h1>
    <div class="checkout-thanks-page__first-info row">
        <div class="checkout-thanks-page__first-info-text col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
            <h3><?= Yii::t('product', 'Confirmation of an order') ?></h3>
            <p><?= sprintf(Yii::t('product', 'Your order %s was completed successfully. An e-mail reciept including the details bout your order has been sent to the e-mail address provided. Please keep it for your records.'), '<span>#'.$order->order_number.'</span>'); ?></p>
            <p><?= sprintf(Yii::t('product', 'If you have any questions about your order, please contact us by %s or by %s.'), '<a href="mailto:'.Setting::value('core.contact.email').'">Email</a>', '<a href="tel:'.Setting::value('core.contact.phone').'">Telefon</a>') ?></p>
        </div>
        <div class="checkout-thanks-page__first-info-img col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12"><img src="/img/icons/IL_box_new.svg" alt="ilbox"></div>
    </div>
    <h5 class="items-number col-xl-8 col-lg-8 col-md-12 col-sm-12 col-11"><?= Yii::t('product', 'Order') ?> #<?= $order->order_number; ?></h5>
    <section class="checkout-thanks-page__second-info">

        <?php foreach ($order->orderProducts as $orderProductVariation) : ?>

            <div class="checkout-thanks-page__second-info-block-wrapper row">
                <div class="checkout-thanks-page__second-info-img"><img src="<?= $orderProductVariation->productVariation->getImagePreview(140, 210); ?>" alt="<?= $orderProductVariation->product_variation_name; ?>"></div>
                <div class="whole-text-wrapper row">
                    <div class="part-one-wrapper">
                        <div class="checkout-thanks-page__second-info-summary-wrapper">
                            <a class="checkout-thanks-page__second-info-summary-header" href="<?= Url::toRoute(['/product/product/index', 'slug' => $orderProductVariation->productVariation->product->slug ]); ?>">
                                <?= $orderProductVariation->product_variation_name; ?>
                            </a>
                            <div class="checkout-thanks-page__second-info-summary-body product-id"><?= Yii::t('product', 'Product vendor code'); ?>: <?= $orderProductVariation->productVariation->vendor_code; ?></div>
                            <?php foreach ($orderProductVariation->productVariation->properties as $property) : ?>

                                <div class="checkout-thanks-page__second-info-summary-body"><?= $property->filter->name; ?>: <?= $property->humanValue; ?></div>

                            <?php endforeach; ?>

                        </div>
                        <div class="checkout-thanks-page__second-info-qty-wrapper">
                            <div class="checkout-thanks-page__second-info-summary-body"><?= Yii::t('product', 'QTY') ?>: <span><?= $orderProductVariation->count; ?></span></div>
                        </div>
                    </div>
                    <div class="part-two-wrapper">
                        <div class="checkout-thanks-page__second-info-shipping-wrapper">
                            <div class="checkout-thanks-page__second-info-summary-body-wrapper">
                                <div class="checkout-thanks-page__second-info-summary-body"><?= Yii::t('product', 'Einzelpreis') ?>: </div>
                                <div class="checkout-thanks-page__second-info-summary-body"><?= Setting::value('product.shop.currency') ?><?= ProductVariation::viewFormatPrice($orderProductVariation->viewFullPrice); ?></div>
                            </div>
                            <div class="checkout-thanks-page__second-info-summary-body-wrapper">
                                <div class="checkout-thanks-page__second-info-summary-body bold"><?= Yii::t('product', 'Product subtotal') ?>:</div>
                                <div class="checkout-thanks-page__second-info-summary-body bold"><?= Setting::value('product.shop.currency') ?><?= ProductVariation::viewFormatPrice($orderProductVariation->count * $orderProductVariation->viewFullPrice); ?></div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row">
                <hr>
            </div>

        <?php endforeach; ?>

        <div class="checkout-thanks-page__summ-wrapper">
            <div class="checkout-thanks-page__second-info-shipping-wrapper checkout-thanks-page__second-info-shipping-wrapper-summ">
                <div class="checkout-thanks-page__second-info-summary-body"><?= $order->orderShippingInformation ? $order->orderShippingInformation->getShippingTimeDeliveryByMethod() : '' ?></div>
                <div class="checkout-thanks-page__second-info-summary-body-wrapper">
                    <div class="checkout-thanks-page__second-info-summary-body"><?= Yii::t('product', 'Selected payment method') ?>: </div>
                    <div class="checkout-thanks-page__second-info-summary-body"><?= $order->humanPaymentMethod ?></div>
                </div>
                <div class="checkout-thanks-page__second-info-summary-body-wrapper">
                    <div class="checkout-thanks-page__second-info-summary-body"><?= Yii::t('product', 'Subtotal') ?>:</div>
                    <div class="checkout-thanks-page__second-info-summary-body"><?= Setting::value('product.shop.currency') ?><?= ProductVariation::viewFormatPrice($order->total_sum); ?></div>
                </div>
                <div class="checkout-thanks-page__second-info-summary-body-wrapper">
                    <div class="checkout-thanks-page__second-info-summary-body"><?= Yii::t('product', 'Discount') ?>: </div>
                    <div class="checkout-thanks-page__second-info-summary-body">
                        <?= $order->coupon_code?'-':'' ?><?= Setting::value('product.shop.currency') ?><?= ProductVariation::viewFormatPrice($order->sum_discount); ?>
                    </div>
                </div>
                <div class="checkout-thanks-page__second-info-summary-body-wrapper">
                    <div class="checkout-thanks-page__second-info-summary-body"><?= Yii::t('product', 'Order tax') ?>:</div>
                    <div class="checkout-thanks-page__second-info-summary-body"><?= Setting::value('product.shop.currency') ?><?= ProductVariation::viewFormatPrice($order->tax); ?></div>
                </div>
                <div class="checkout-thanks-page__second-info-summary-body-wrapper">
                    <div class="checkout-thanks-page__second-info-summary-body"><?= Yii::t('product', 'Brutto') ?>:</div>
                    <div class="checkout-thanks-page__second-info-summary-body"><?= Setting::value('product.shop.currency') ?><?= ProductVariation::viewFormatPrice($order->full_sum - $order->shipping_price); ?></div>
                </div>
                <div class="checkout-thanks-page__second-info-summary-body-wrapper">
                    <div class="checkout-thanks-page__second-info-summary-body"><?= Yii::t('product', 'Shipping') ?>:</div>
                    <div class="checkout-thanks-page__second-info-summary-body"><?= Setting::value('product.shop.currency') ?><?= ProductVariation::viewFormatPrice($order->shipping_price); ?></div>
                </div>
                <div class="checkout-thanks-page__second-info-summary-body-wrapper">
                    <div class="checkout-thanks-page__second-info-summary-body bold"><?= Yii::t('product', 'Estimated total') ?>:</div>
                    <div class="checkout-thanks-page__second-info-summary-body bold"><?= Setting::value('product.shop.currency') ?><?= ProductVariation::viewFormatPrice($order->full_sum); ?></div>
                </div>
            </div>
        </div>

    </section>
</main>

<?= SmallSliderWithProducts::widget([
    'mainWrapperOptions' => [
        'class' => 'container checkout-thanks-page__slider',
    ],
    'headerOptions' => [
        'title' => Yii::t('product', 'You may also like'),
        'class' => ''
    ],
    'wrapperOptions' => [
        'class' => 'owl-carousel owl-theme all-pages-carousel',
    ],
    'itemOptions' => [
        'class' => 'item',
    ],
    'typeWidget' => SmallSliderWithProducts::TYPE_ALL_RECOMMENDS,
    'shopNowLink' => [
        'template' => '<div class="button-wrapper"><a class="btn btn-primary" href="/shop">'.Yii::t('product', 'Continue shopping') . '</a></div>',
    ],
]); ?>
