<?php

use frontend\modules\product\models\ProductCategory;

/** @var \frontend\modules\core\components\View $this */

?>

<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade active show" id="list-orders" role="tabpanel" aria-labelledby="list-orders-list">
        <div class="account-page__orders-new">
            <h4><?= Yii::t('user', 'My orders') ?></h4>
            <p class="account-page__right-head-p"><?= Yii::t('product', 'Here you can see your order history'); ?></p>
            <p class="account-page__oops"><?= Yii::t('product', 'Oops, there\'s nothing here. It\'s time to start shopping'); ?></p>
            <a href="/shop" class="btn btn-primary account-page__orders-new"><?= Yii::t('product', 'Start shopping'); ?></a>
        </div>
    </div>
</div>
