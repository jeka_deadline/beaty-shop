<p>Small products</p>

<table>
        <tr>
            <th>Name</th>
            <th>Vendor code</th>
            <th>Count</th>
        </tr>

        <?php foreach ($variations as $variation) : ?>

            <tr>
                <td>
                    <p><?= $variation->name; ?></p>

                    <?php foreach ($variation->properties as $property) : ?>

                        <?= sprintf('<b>%s</b>: %s<br>', $property->filter->name, $property->humanValue); ?>

                    <?php endforeach; ?>

                </td>
                <td>
                    <?= $variation->vendor_code; ?>
                </td>
                <td>
                    <?= $variation->inventorie->count; ?>
                </td>
            </tr>

        <?php endforeach; ?>

</table>