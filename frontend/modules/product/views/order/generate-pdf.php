<?php

use yii\data\ActiveDataProvider;
use yii\widgets\DetailView;
use yii\widgets\ListView;
use yii\helpers\Url;
use frontend\modules\core\models\Setting;
use frontend\modules\product\models\ProductVariation;

?>

<div style="text-align: center;">
    <img style="width: 60%" src="/files/core/email/img/infiniry_logo.png" alt="">
</div>

<table style="margin: 20px 0px;">
    <tr>
        <td><i>Infinity Lashes</i></td>
    </tr>
    <tr>
        <td><i>Kaiserstraße 59</i></td>
    </tr>
    <tr>
        <td><i>44135 Dortmund</i></td>
    </tr>
</table>

<div style="width: 50%; float: left;">

    <?php if ($modelOrder->orderShippingInformation) : ?>

        <table style="float: left; width: 50%;">
            <tr>
                <td><strong>Empfänger:</strong></td>
            </tr>
            <tr>
                <td><?= $modelOrder->orderShippingInformation->company; ?></td>
            </tr>
            <tr>
                <td><?= $modelOrder->orderShippingInformation->name . ' ' . $modelOrder->orderShippingInformation->surname ;?></td>
            </tr>
            <tr>
                <td><?= $modelOrder->orderShippingInformation->address1; ?></td>
            </tr>
            <tr>
                <td><?= $modelOrder->orderShippingInformation->zip; ?> <?= $modelOrder->orderShippingInformation->city; ?></td>
            </tr>
            <tr>
                <td>
                    <?= (!empty($modelOrder->orderShippingInformation->country)) ? ' ' . $modelOrder->orderShippingInformation->country->name : ''; ?>
                </td>
            </tr>
        </table>

    <?php endif; ?>

</div>

<br>
<div style="width: 50%; float: right;  background-color: #eee">
    <table style="width: 50%; font-size: 14px">
        <tr>
            <td class="textr">Rechnungsdatum</td>
            <td style="padding-left: 20px"><?= $modelOrder->getCreatedAtFormat(); ?></td>
        </tr>
        <tr>
            <td class="textr">Rechnungsnummer</td>
            <td style="padding-left: 20px"><?= $modelOrder->pdfNumberFormat; ?></td>
        </tr>
        <tr>
            <td class="textr">Kundennummer</td>
            <td style="padding-left: 20px"><?= ($modelOrder->user_id) ? $modelOrder->pdfUserIdFormat : '--'; ?></td>
        </tr>
        <tr>
            <td class="textr">Zahlungsziel</td>
            <td style="padding-left: 20px"><?= $modelOrder->humanPaymentMethod; ?></td>
        </tr>
    </table>
</div>

<div style="clear: both; margin: 0pt; padding: 0pt; "></div>

<div style="margin: 20px 0px;">
    <span><strong>Zusätzliche Informationen</strong></span><br>
    <span><i>Die Ware bleibt bis zur vollständigen Bezahlung Eigentum von Infinity Lashes.</i></span>
</div>

<?php if ($modelOrder->orderProducts): ?>
<?php $price = 0; ?>
<?php $discount = $modelOrder->sum_discount; ?>

    <table class="bordered" style="width: 100%;" cellspacing="0" cellpadding="0">
        <thead>
            <tr style="background-color: #C5D9F1">
                <th style="text-align: center; border-left: 1px solid #000">Artikel</th>
                <th style="text-align: center; width: 20px;">Menge</th>
                <th style="text-align: center; width: 150px;">Stückpreis</th>
                <th style="text-align: center; width: 100px;">Gesamt</th>
            </tr>
        </thead>

        <tbody>
        <?php $countProducts = count($modelOrder->orderProducts); ?>

        <?php foreach ($modelOrder->orderProducts as $index => $product) : ?>
        <?php $productPrice = ($product->promotion) ? $product->promotion : $product->price; ?>

            <?php if ($countProducts == $index + 1) : ?>

                <tr>
                    <td style="border-left: 1px solid #000">
                        [ <?= $product->productVariation->vendor_code; ?> ] <?= $product->product_variation_name; ?>

                        <?php if ($product->productVariation->properties) :?>

                            <table style="border: 0px; margin-left: 10px">

                                <?php foreach ($product->productVariation->properties as $property) : ?>

                                    <tr>
                                        <td style="border: 0px;"><?= sprintf('%s: %s', $property->filter->name, $property->humanValue); ?></td>
                                    </tr>

                                <?php endforeach; ?>

                            </table>

                        <?php endif; ?>

                        <?php if (
                            $product->productVariation
                            && ($setProduct = $product->productVariation->product)
                            && ($setRelationVariations = $setProduct->productSetVariationWithData)
                        ) :?>

                            <table style="border: 1px; margin-left: 20px">

                                <?php foreach ($setRelationVariations as $setRelationVariation) : ?>
                                    <?php if (!($variation = $setRelationVariation->productVariation)) continue;?>
                                    <tr>
                                        <td style="border: 0px;">[ <?= $variation->vendor_code; ?> ] <?= $variation->name; ?></td>
                                    </tr>
                                    <tr>
                                        <td style="border: 0px;" align="center"><?= sprintf('%s: %s', 'Count', $setRelationVariation->count); ?></td>
                                    </tr>
                                    <?php foreach ($variation->properties as $property) : ?>

                                        <tr>
                                            <td style="border: 0px;"><?= sprintf('%s: %s', $property->filter->name, $property->humanValue); ?></td>
                                        </tr>

                                    <?php endforeach; ?>

                                <?php endforeach; ?>

                            </table>

                        <?php endif; ?>

                    </td>
                    <td class="textr"><?= $product->count; ?></td>
                    <td class="textr"><?= ProductVariation::viewFormatPrice($productPrice); ?> <?= Setting::value('product.shop.currency') ?></td>
                    <td class="textr"><?= ProductVariation::viewFormatPrice($product->count * $productPrice); ?> <?= Setting::value('product.shop.currency') ?></td>
                </tr>

            <?php else : ?>

                <tr>
                    <td style="border-bottom: 0px; border-left: 1px solid #000">
                        [ <?= $product->productVariation->vendor_code; ?> ] <?= $product->product_variation_name; ?>

                        <?php if ($product->productVariation->properties) :?>

                            <table style="border: 0px; margin-left: 10px">

                                <?php foreach ($product->productVariation->properties as $property) : ?>

                                    <tr>
                                        <td style="border: 0px;"><?= sprintf('%s: %s', $property->filter->name, $property->humanValue); ?></td>
                                    </tr>

                                <?php endforeach; ?>

                            </table>

                        <?php endif; ?>

                        <?php if (
                            $product->productVariation
                            && ($setProduct = $product->productVariation->product)
                            && ($setRelationVariations = $setProduct->productSetVariationWithData)
                        ) :?>

                            <table style="border: 1px; margin-left: 20px">

                                <?php foreach ($setRelationVariations as $setRelationVariation) : ?>
                                    <?php if (!($variation = $setRelationVariation->productVariation)) continue;?>
                                    <tr>
                                        <td style="border: 0px;">[ <?= $variation->vendor_code; ?> ] <?= $variation->name; ?></td>
                                    </tr>
                                    <tr>
                                        <td style="border: 0px;" align="center"><?= sprintf('%s: %s', 'Count', $setRelationVariation->count); ?></td>
                                    </tr>
                                    <?php foreach ($variation->properties as $property) : ?>

                                        <tr>
                                            <td style="border: 0px;"><?= sprintf('%s: %s', $property->filter->name, $property->humanValue); ?></td>
                                        </tr>

                                    <?php endforeach; ?>

                                <?php endforeach; ?>

                            </table>

                        <?php endif; ?>

                    </td>
                    <td class="textr" style="border-bottom: 0px;"><?= $product->count; ?></td>
                    <td class="textr" style="border-bottom: 0px;"><?= ProductVariation::viewFormatPrice($productPrice); ?> <?= Setting::value('product.shop.currency') ?></td>
                    <td class="textr" style="border-bottom: 0px;"><?= ProductVariation::viewFormatPrice($product->count * $productPrice); ?> <?= Setting::value('product.shop.currency') ?></td>
                </tr>

            <?php endif; ?>

        <?php $price += ProductVariation::viewFormatPrice($product->count * $productPrice); ?>
        <?php endforeach; ?>

        <tr>
            <td style="border: 0px"></td>
            <td colspan="3" style="padding: 0px; border: 0px">
                <table class="not-bordered-table" style="width: 100%" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="textr fs12">Gesamt netto </td>
                        <td class="textr fs12"><?= ProductVariation::viewFormatPrice($price); ?> <?= Setting::value('product.shop.currency') ?></td>
                    </tr>
                    <tr>
                        <td class="textr fs12">Rabatt </td>
                        <td class="textr fs12"><?= ProductVariation::viewFormatPrice($discount); ?> <?= Setting::value('product.shop.currency') ?></td>
                    </tr>
                    <tr>
                        <td class="textr fs12">USt. 19%</td>
                        <td class="textr fs12"><?= $modelOrder->tax; ?> <?= Setting::value('product.shop.currency') ?></td>
                    </tr>
                    <tr>
                        <td class="textr fs12">Gesamt brutto</td>
                        <td class="textr fs12"><?= ProductVariation::viewFormatPrice($price - $discount + $modelOrder->tax); ?> <?= Setting::value('product.shop.currency') ?></td>
                    </tr>
                    <tr>
                        <td class="textr fs12">Versand</td>
                        <td class="textr fs12"><?= ProductVariation::viewFormatPrice($modelOrder->shipping_price); ?> <?= Setting::value('product.shop.currency') ?></td>
                    </tr>
                    <tr style="border-top: 1px solid #000">
                        <td style="border-top: 1px solid #000; font-size: 14px;" class="textr"><strong>Rechnungsbetrag</strong></td>
                        <td style="border-top: 1px solid #000; font-size: 14px;" class="textr"><?= ProductVariation::viewFormatPrice($modelOrder->full_sum); ?> <?= Setting::value('product.shop.currency') ?></td>
                    </tr>
                </table>
            </td>
        </tr>
        </tbody>
    </table>

<?php endif; ?>
