<?php

use frontend\modules\core\models\Setting;
use yii\helpers\Url;
use frontend\modules\product\models\ProductVariation;

/** @var \frontend\modules\core\components\View $this */
/** @var \frontend\modules\product\models\Order[] $orders */
/** @var \frontend\modules\product\models\Order $order */
/** @var \frontend\modules\product\models\OrderProductVariation $orderProduct */

?>

<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade active show" id="list-orders" role="tabpanel" aria-labelledby="list-orders-list">
        <div class="account-page__orders-now">
            <h4><?= Yii::t('user', 'My orders') ?></h4>
            <p class="account-page__right-head-p"><?= Yii::t('product', 'Here you can see your order history'); ?></p>
            <div class="account-page__wrapper-orders">

                <?php foreach ($orders as $order) : ?>

                    <div class="account-page__wrapper-order">
                        <header>
                            <div class="account-page__order-main-info">
                                <div class="account-page__order-num"><?= Yii::t('product', 'Order №{0}', $order->order_number); ?></div>
                                <div class="account-page__order-date"><?= $order->getOrderDate(); ?></div>
                            </div>
                            <div class="account-page__order-status"><?= $order->humanStatus; ?></div>
                        </header>

                        <?php foreach ($order->orderProducts as $orderProduct) : ?>

                            <div class="account-page__order-body">
                                <div class="account-page__order-inside"><span class="account-page__name"><?= $orderProduct->product_variation_name; ?></span><span><?= $orderProduct->count; ?></span></div>
                                <div class="account-page__order-price"><?= Setting::value('product.shop.currency') ?><?= ProductVariation::viewFormatPrice(($orderProduct->promotion) ? $orderProduct->promotion : $orderProduct->price); ?></div>
                            </div>

                        <?php endforeach; ?>

                        <div class="account-page__order-shipping">
                            <div class="account-page__order-shipping-name"><?= Yii::t('product', 'Subtotal') ?></div>
                            <div class="account-page__order-shipping-price"><?= Setting::value('product.shop.currency') ?><?= ProductVariation::viewFormatPrice($order->total_sum); ?></div>
                        </div>
                        <div class="account-page__order-shipping">
                            <div class="account-page__order-shipping-name"><?= Yii::t('product', 'Discount') ?></div>
                            <div class="account-page__order-shipping-price"><?= Setting::value('product.shop.currency') ?><?= ProductVariation::viewFormatPrice($order->sum_discount); ?></div>
                        </div>
                        <div class="account-page__order-shipping">
                            <div class="account-page__order-shipping-name"><?= Yii::t('product', 'Order tax') ?></div>
                            <div class="account-page__order-shipping-price"><?= Setting::value('product.shop.currency') ?><?= ProductVariation::viewFormatPrice($order->tax); ?></div>
                        </div>
                        <div class="account-page__order-shipping">
                            <div class="account-page__order-shipping-name"><?= Yii::t('product', 'Brutto') ?></div>
                            <div class="account-page__order-shipping-price"><?= Setting::value('product.shop.currency') ?><?= ProductVariation::viewFormatPrice($order->full_sum - $order->shipping_price); ?></div>
                        </div>
                        <div class="account-page__order-total">
                            <div class="account-page__order-total-name"><?= Yii::t('product', 'Shipping') ?></div>
                            <div class="account-page__order-total-price"><?= Setting::value('product.shop.currency') ?><?= ProductVariation::viewFormatPrice($order->shipping_price); ?></div>
                        </div>
                        <div class="account-page__order-total">
                            <div class="account-page__order-total-name"><?= Yii::t('product', 'Estimated total') ?></div>
                            <div class="account-page__order-total-price"><?= Setting::value('product.shop.currency') ?><?= ProductVariation::viewFormatPrice($order->full_sum); ?></div>
                        </div>
                        <div class="account-page__order-pdf-wrapper">
                            <svg class="svg-pdf svg">
                                <use xlink:href="#pdf"></use>
                            </svg><a class="account-page__order-pdf" target="_blank" href="<?= Url::to(['/product/order/generate-pdf', 'id' => $order->id]) ?>">Rechnung herunterladen (PDF)</a>
                        </div>
                    </div>

                <?php endforeach; ?>

            </div>
        </div>
    </div>
</div>
