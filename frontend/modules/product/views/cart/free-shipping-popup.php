<?php

use frontend\modules\core\models\Setting;
use frontend\modules\product\models\ProductVariation;

?>
<div class="cart-hover-menu__header-1">Free shipping</div>
<div class="cart-hover-menu__header-2">Buy another <?= Setting::value('product.shop.currency') ?><?= ProductVariation::viewFormatPrice($infoFreeShipping['remainder']) ?> and get free shipping</div>
<div class="progress-wrapper">
    <div class="progress" style="height: 2px;">
        <div class="progress-bar" role="progressbar" style="width: <?= $infoFreeShipping['value'] ?>%;" aria-valuenow="<?= $infoFreeShipping['value'] ?>" aria-valuemin="0" aria-valuemax="<?= $infoFreeShipping['max'] ?>"></div>
    </div>
    <div class="progress-bar-value">55</div>
</div>