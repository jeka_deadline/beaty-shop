<?php

use frontend\modules\core\models\Setting;
use yii\helpers\Html;
use frontend\modules\product\models\ProductVariation;

?>
<div class="cart-hover-menu__item" data-id="<?= $productVariation->id; ?>">

    <?= Html::tag('div', null, [
        'class' => 'cart-hover-menu__item-img',
        'style' => sprintf('background-image: url(%s)', $productVariation->getImagePreview(100, 140)),
    ]); ?>

    <div class="cart-hover-menu__item-info">
        <div class="cart-hover-menu__item-info-title"><?= $productVariation->name; ?></div>

        <?php foreach ($productVariation->withProperties as $property) : ?>

            <div class="cart-hover-menu__item-info-parameter"><?= $property->filter->name; ?>: <?= $property->humanValue; ?></div>

        <?php endforeach; ?>

        <div class="cart-hover-menu__item-info-price"><?= Setting::value('product.shop.currency') ?><span><?= ProductVariation::viewFormatPrice($cart[ $productVariation->id ]->totalPrice); ?></span></div>
    </div>
</div>
