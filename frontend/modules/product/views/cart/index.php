<?php

use frontend\modules\core\models\Setting;
use frontend\modules\product\assets\CartAsset;
use frontend\modules\product\models\Product;use yii\helpers\Url;
use yii\helpers\Html;
use frontend\widgets\bootstrap4\ActiveForm;
use frontend\modules\product\widgets\SmallSliderWithProducts;
use frontend\modules\product\models\ProductCategory;
use frontend\modules\product\models\OrderShippingInformation;
use frontend\modules\product\models\ProductVariation;

$this->bodyClass = 'cart-page';

CartAsset::register($this);

/** @var \frontend\modules\core\components\View $this */
/** @var \frontend\modules\product\helpers\CartDTO[] $cart */
/** @var \frontend\modules\product\models\ProductVariation[] $productVariations */
/** @var float $totalPrice */
/** @var int $shippingPrice */
/** @var array $freeShipping */
/** @var \frontend\widgets\bootstrap4\ActiveForm $form */
/** @var \frontend\modules\product\models\forms\CouponForm $couponForm */
/** @var int $countItems */
/** @var \frontend\modules\product\models\ProductVariation $productVariation */

if (isset($modalText) && $modalText) {
    $js = '$(function() {
        $(document).find("#core-modal .modal-body").html("' . $modalText . '");
        $(document).find("#core-modal").modal();
    })';

    $this->registerJs($js);
}

?>

<div class="container">
</div>
<section class="container cart-page__shopping-bag">
    <h1><?= Yii::t('product', 'your shopping bag'); ?> <span>(<?= $countItems; ?> <?= Yii::t('product', 'item'); ?>)</span></h1>
    <hr>
    <div class="cart-page__item-order-wrapper">
        <div id="cart-items-wrapper" class="cart-page__items-wrapper" data-plus-count-url="<?= Url::toRoute(['/product/cart/increment-product-count']); ?>" data-minus-count-url="<?= Url::toRoute(['/product/cart/decrement-product-count']); ?>" data-remove-url="<?= Url::toRoute(['/product/cart/remove-product-from-cart']); ?>">

            <?php foreach ($productVariations as $productVariation) : ?>
                <?php if (($product = $productVariation->product)): ?>
<?php switch ($product->type): ?>
<?php case Product::TYPE_PRODUCT_SET:?>
                        <?= $this->render('_item_product_set_variation', compact('productVariation', 'cart', 'product')); ?>
                        <?php break;?>
                    <?php default: ?>
                        <?= $this->render('_item_product_variation', compact('productVariation', 'cart')); ?>
<?php endswitch; ?>
                <?php endif; ?>
            <?php endforeach; ?>

        </div>
        <div class="cart-page__order-wrapper">
            <h4><?= Yii::t('product', 'Order Summary') ?></h4>

            <?php $form = ActiveForm::begin([
                'enableClientValidation' => false,
                'id' => 'coupon-form',
                'action' => ['/product/coupon/apply-coupon'],
                'options' => [
                    'data-action-clear' => '/product/coupon/remove-coupon',
                ],
            ]); ?>

                <div id="cart-page__select-title">
                    <p><?= Yii::t('product', 'Add your promotional code here') ?></p>
                    <svg class="promotion-stroke svg promotion-stroke-move">
                        <use xlink:href="#totop"></use>
                    </svg>
                </div>

                <?= $this->render('coupon-form', compact('form', 'couponForm')); ?>

                <div class="cart-page__order-text-wrapper">
                    <div class="cart-page__order-text">
                        <p><?= Yii::t('product', 'Subtotal') ?>:</p>
                        <p><?= Setting::value('product.shop.currency') ?><span id="subtotal-price"><?= ProductVariation::viewFormatPrice($totalPriceNotDiscount); ?></span></p>
                    </div>
                    <div class="cart-page__order-text">
                        <p><?= Yii::t('product', 'Discount') ?>:</p>
                        <p><span><?= $discountPrice?'-':'' ?></span><?= Setting::value('product.shop.currency') ?><span id="discount-price"><?= ProductVariation::viewFormatPrice($discountPrice) ?></span></p>
                    </div>
                    <div class="cart-page__order-text">
                        <p><?= Yii::t('product', 'Order tax') ?>:</p>
                        <p><?= Setting::value('product.shop.currency') ?><span id="tax-price"><?= ProductVariation::viewFormatPrice($tax); ?></span></p>
                    </div>
                    <div class="cart-page__order-text">
                        <p class="cart-page__shipping"><?= Yii::t('product', 'Brutto') ?>:</p>
                        <p class="cart-page__shipping"><?= Setting::value('product.shop.currency') ?><span id="brutto-price"><?= ProductVariation::viewFormatPrice($totalPriceNotDiscount - $discountPrice + $tax); ?></span></p>
                    </div>
                    <div class="cart-page__order-text">
                        <p class="cart-page__shipping"><?= Yii::t('product', 'Shipping') ?>:</p>
                        <p class="cart-page__shipping"><?= Setting::value('product.shop.currency') ?><span id="shipping-price"><?= ProductVariation::viewFormatPrice($shippingPrice); ?></span></p>
                    </div>

                    <p><?= Setting::value('product.shop.currency') ?> <span class="js-free-shipping-remainder"><?= ProductVariation::viewFormatPrice($freeShipping['remainder']) ?></span> <?= Yii::t('product', 'to free shipping') ?></p>
                </div>
                <div class="cart-page__order-total-price">
                    <h6><?= Yii::t('product', 'Estimated total') ?>:</h6>
                    <p><?= Setting::value('product.shop.currency') ?><span id="full-sum"><?= ProductVariation::viewFormatPrice($totalPriceNotDiscount - $discountPrice + $shippingPrice + $tax); ?></span></p>
                </div><a class="btn btn-primary cart-page__proceed" href="<?= Url::toRoute(['/product/checkout/index']); ?>"><?= Yii::t('product', 'To checkout') ?></a><a class="cart-page__continue-shopping" href="/shop" target="_blank"><?= Yii::t('product', 'Continue shopping') ?></a>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</section>

<?= SmallSliderWithProducts::widget([
    'mainWrapperOptions' => [
        'class' => 'cart-page__also-like container',
        'tag' => 'section'
    ],
    'headerOptions' => [
        'title' => Yii::t('product', 'You may also like'),
        'class' => 'row justify-content-center align-items-center'
    ],
    'wrapperOptions' => [
        'class' => 'owl-carousel owl-theme all-pages-carousel',
    ],
    'itemOptions' => [
        'class' => 'item',
    ],
    'typeWidget' => SmallSliderWithProducts::TYPE_ALL_RECOMMENDS,
    'shopNowLink' => false,
]); ?>
