<?php

use frontend\modules\core\models\Setting;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\modules\product\models\ProductVariation;

?>
<div class="cart-page__item-wrapper" data-id="<?= $productVariation->id; ?>" data-price="<?= $cart[ $productVariation->id ]->getPrice(); ?>">
    <div class="cart-page__img-item">

        <?= Html::tag('div', null, [
            'style' => sprintf('background-image: url(%s)', $productVariation->getImagePreview(170, 220)),
        ]); ?>

    </div>
    <div class="cart-page__desc-item">
        <h4><a href="<?= Url::toRoute(['/product/product/index', 'slug' => $productVariation->product->slug]); ?>"><?= $productVariation->name; ?></a></h4>
        <p class="cart-page__desc-item-one-price"><?= Setting::value('product.shop.currency') ?><span><?= ProductVariation::viewFormatPrice($cart[ $productVariation->id ]->getPrice()); ?></span></p>
        <p class="zz"><?= Yii::t('product', 'plus 19% VAT.') ?></p>

        <?php foreach ($productVariation->properties as $property) : ?>

            <p><?= $property->filter->name; ?>: <?= $property->humanValue; ?></p>

        <?php endforeach; ?>

        <p class="cart-page__product-id"><?= Yii::t('product', 'Product vendor code'); ?>: <?= $productVariation->vendor_code; ?></p>
        <div class="cart-page__quantiti-of-order-edit">
            <div class="cart-page__quantiti-of-order"><span class="minus">-</span>
                <div class="label-wrapper">
                    <label for="cart-page__quantiti-of-order"><?= Yii::t('product', 'QTY') ?>:</label>
                    <input type="text" value="<?= $cart[ $productVariation->id ]->count; ?>" size="5" readonly>
                </div><span class="plus">+</span>
            </div>
            <div class="cart-page__edit">
                <a href="#" class="js-remove-product-from-cart"><?= Yii::t('product', 'Remove') ?></a>
            </div>
        </div>
    </div>
</div>