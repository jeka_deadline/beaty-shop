<div id="cart-page__promotional-input" style="display: block">
    <div class="form-group grey-form">

        <?= $form->field($couponForm, 'code', [
            'options' => [
                'tag' => false,
            ],
            'inputOptions' => [
                'class' => 'form-control',
                'placeholder' => 'хххх-хххх-хххх-хххх-ххххx',
            ],
        ])->label(false); ?>

        <svg class="close-search-code svg clear-input-coupon-code">
            <use xlink:href="#close-search"></use>
        </svg>

        <button class="btn btn-primary"><?= Yii::t('product', 'Apply') ?></button>
    </div>
</div>