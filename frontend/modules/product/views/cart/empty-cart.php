<?php

use frontend\modules\product\models\ProductCategory;

$this->bodyClass = 'cart-empty';

/** @var \frontend\modules\core\components\View $this */

if (isset($modalText) && $modalText) {
    $js = '$(function() {
        $(document).find("#core-modal .modal-body").html("' . $modalText . '");
        $(document).find("#core-modal").modal();
    })';

    $this->registerJs($js);
}

?>
<main class="container">
    <h1><?= Yii::t('product', 'Your Shopping bag is Empty'); ?></h1><a class="btn btn-primary" href="/shop"><?= Yii::t('product', 'Continue shopping') ?></a>
</main>