<?php

use frontend\modules\product\assets\CategoryAsset;
use frontend\modules\product\widgets\ProductCategoriesMenu;
use frontend\modules\product\widgets\ProductFilters;
use frontend\modules\product\widgets\ProductLoadNextPage;
use frontend\modules\product\widgets\ProductSort;
use yii\widgets\LinkPager;
use yii\widgets\ListView;

/** \yii\web\View $this */
/** \yii\data\ActiveDataProvider $dataProviderVariations */
/** \frontend\modules\product\models\ProductCategory $category */
/** string $categorySlugs */

CategoryAsset::register($this);

$this->bodyClass = 'shop-page';
?>

<div class="shop-page__main-content-wrapper">
    <div class="container">
        <div class="shop-page__adaptive-header">
            <h1><?= $category->parent?$category->parent->name:''; ?></h1>
            <div class="shop-page__category-chosen-adaptive"><?= $category->name; ?> <span>(<?=  $dataProviderVariations->totalCount ?>)</span></div>
        </div>
    </div>
    <div class="container-fluid filters-wrapper" id="filters-wrapper">
        <div class="container">
            <?= ProductFilters::widget([
                'category' => $category,
                'formAction' => $categorySlugs,
                'categoryIds' => $categoryIds
            ]) ?>
        </div>
    </div>

    <div class="container-fluid category-adaptive-wrapper" id="category-adaptive-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="row" id="accordion2">
                        <?= ProductCategoriesMenu::widget([
                                'template' => 'product-categories-menu-mobile'
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="shop-page__main-content container">
        <div class="shop-page__main-part row">
            <aside class="col-xl-4">
                <div id="accordion">
                    <?= ProductCategoriesMenu::widget(); ?>
                </div>
            </aside>
            <main class="col-xl-8">
                <div class="shop-page__main-head">
                    <div class="shop-page__category-chosen"><?= $category->name; ?> <span>(<?=  $dataProviderVariations->totalCount ?>)</span></div>
                    <div class="shop-page__sort">
                        <div class="shop-page__category-adaptive"><?= Yii::t('product', 'category'); ?></div>
                        <div class="shop-page__filter"><?= Yii::t('product', 'Filter') ?></div>
                        <?= ProductSort::widget([
                            'formAction' => $categorySlugs,
                        ]) ?>
                    </div>
                </div>
                <div class="shop-page__shop-items">
                    <div class="shop-items row list-catalog">

                        <?= ListView::widget([
                            'dataProvider' => $dataProviderVariations,
                            'itemView' => 'product_item',
                            'layout' => '{items}',
                            'summary' => false,
                            'itemOptions' => [
                                'tag' => false
                            ],
                            'options' => [
                                'tag' => false
                            ],
                            'viewParams' => compact('categorySlugs'),
                        ]); ?>

                    </div>
                </div>
                <?= ProductLoadNextPage::widget([
                    'dataProvider' => $dataProviderVariations
                ]) ?>
                <?= LinkPager::widget([
                    'pagination' => $dataProviderVariations->pagination,
                    'disableCurrentPageButton' => true,
                    'options' => [
                        'class' => 'pagination d-none'
                    ]
                ]) ?>
            </main>
        </div>
    </div>
</div>