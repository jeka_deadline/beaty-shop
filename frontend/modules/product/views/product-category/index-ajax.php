<?php

use yii\widgets\ListView;

?>


<?= ListView::widget([
    'dataProvider' => $dataProviderVariations,
    'itemView' => 'product_item',
    'layout' => '{items}',
    'summary' => false,
    'itemOptions' => [
        'tag' => false
    ],
    'options' => [
        'tag' => false
    ],
    'viewParams' => compact('categorySlugs'),
]); ?>