<?php

use yii\widgets\ListView;

?>


<?= ListView::widget([
    'dataProvider' => $dataProductProviders,
    'itemView' => 'search_item',
    'layout' => '{items}',
    'summary' => false,
    'itemOptions' => [
        'tag' => false
    ],
    'options' => [
        'tag' => false
    ],
]); ?>