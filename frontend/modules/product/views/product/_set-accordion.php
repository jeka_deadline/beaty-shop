<div class="container">
    <div class="accordion" id="accordionSets">
        <?php if (($modelSetVariations = $masterProduct->productSetVariationWithData)): ?>
            <?php foreach ($modelSetVariations as $setVariation): ?>
                <div class="card">
                    <div class="card-header" id="heading<?= $setVariation->id ?>">
                        <h5 class="mb-0">
                            <button
                                class="btn btn-link productset-onclick-load-tab"
                                type="button"
                                data-toggle="collapse"
                                data-target="#collapse<?= $setVariation->id ?>"
                                aria-expanded="true"
                                aria-controls="collapse<?= $setVariation->id ?>"
                                data-id="<?= $setVariation->product_variation_id ?>"
                            ><?php if ($setVariation->productVariation): ?><?= $setVariation->productVariation->name ?> (<?= Yii::t('product', 'QTY') ?>: <?= $setVariation->count ?>)<?php endif; ?>
                            </button>
                            <button
                                class="btn btn-link arrow-btn productset-onclick-load-tab"
                                type="button"
                                data-toggle="collapse"
                                data-target="#collapse<?= $setVariation->id ?>"
                                aria-expanded="true"
                                aria-controls="collapse<?= $setVariation->id ?>"
                                data-id="<?= $setVariation->product_variation_id ?>"
                            >
                                <svg class="faq-arrow-new svg">
                                    <use xlink:href="#arrow-new"></use>
                                </svg>
                            </button>
                        </h5>
                    </div>
                    <div class="collapse" id="collapse<?= $setVariation->id ?>" aria-labelledby="heading<?= $setVariation->id ?>" data-parent="#accordionSets">
                        <div class="card-body">
                            <div class="loader">
                                <svg id="loader-1" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="40px" height="40px" viewbox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve">
                  <path opacity="0.2" fill="#000" d="M20.201,5.169c-8.254,0-14.946,6.692-14.946,14.946c0,8.255,6.692,14.946,14.946,14.946              s14.946-6.691,14.946-14.946C35.146,11.861,28.455,5.169,20.201,5.169z M20.201,31.749c-6.425,0-11.634-5.208-11.634-11.634              c0-6.425,5.209-11.634,11.634-11.634c6.425,0,11.633,5.209,11.633,11.634C31.834,26.541,26.626,31.749,20.201,31.749z"></path>
                                    <path fill="#000" d="M26.013,10.047l1.654-2.866c-2.198-1.272-4.743-2.012-7.466-2.012h0v3.312h0              C22.32,8.481,24.301,9.057,26.013,10.047z">
                                        <animatetransform attributetype="xml" attributename="transform" type="rotate" from="0 20 20" to="360 20 20" dur="0.5s" repeatcount="indefinite"></animatetransform>
                                    </path>
                </svg>
                            </div>

                            <div class="card-body-block-wrapper shop-item-page__item">

                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>