<?php

use frontend\modules\product\widgets\ProductViewItemFilters;

?>

<div class="row">
    <div class="shop-item-page__item-slider col-xl-7 col-lg-7 second-page-slider">
        <ul class="second-page-slider" id="shop-item-page__item-slider2">
            <?php if ($modelVariation->images) : ?>
                <?php foreach ($modelVariation->images as $image): ?>

                    <li data-thumb="<?= $image->urlImage ?>"><img class="shop-item-page__zoom-two" src="<?= $image->urlImage ?>" alt="<?= $modelVariation->name ?>"></li>

                <?php endforeach; ?>
            <?php endif; ?>
        </ul>
        <div class="shop-item-page__zoom-here-two"></div>
    </div>
    <div class="shop-item-page__item-prop col-xl-5 col-lg-5">
        <h1 data-model-attr="name"><?= $modelVariation->name ?></h1>

        <?= $this->render('rating-widget', compact('masterProduct')); ?>

        <?= ProductViewItemFilters::widget([
            'variationProduct' => $modelVariation,
        ]); ?>
    </div>
</div>