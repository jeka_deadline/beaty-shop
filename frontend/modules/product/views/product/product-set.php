<?php

use frontend\modules\core\models\Setting;
use frontend\modules\product\assets\ProductAsset;
use frontend\modules\product\assets\ProductSetAsset;
use frontend\modules\product\widgets\SmallSliderWithProducts;
use frontend\modules\product\widgets\ProductReviews;
use frontend\modules\product\widgets\ProductPacks;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use frontend\modules\product\assets\CartAsset;
use frontend\modules\product\assets\ReviewAsset;
use frontend\modules\product\assets\RatingAsset;
use yii\widgets\Pjax;
use frontend\modules\product\models\ProductVariation;

ProductAsset::register($this);
CartAsset::register($this);
ReviewAsset::register($this);
RatingAsset::register($this);
ProductSetAsset::register($this);

/** @var \frontend\modules\core\components\View $this */
/** @var \frontend\modules\product\models\ProductVariation $variationProduct */
/** @var \frontend\modules\product\models\Product $masterProduct */
/** @var \frontend\modules\product\models\forms\ProductReviewForm $formReview */
/** @var \frontend\modules\product\models\forms\ProductCartForm $formAddToCart */
/** @var \yii\widgets\ActiveForm $form */
/** @var bool $isUserVoiceRating */

$this->bodyClass = 'shop-set-page';

?>

<div class="added-to-cart-wrapper"><?= $variationProduct->name; ?> wurde in den Warenkorb gelegt.</div>
<div class="shop-item-page__item container">
    <div class="row">
        <div class="shop-item-page__item-slider col-xl-7 col-lg-7 first-page-slider">
            <ul id="shop-item-page__item-slider">

                <?php if ($variationProduct->images) : ?>
                    <?php foreach ($variationProduct->images as $image): ?>

                        <li data-thumb="<?= $image->urlImage ?>"><img class="shop-item-page__zoom" src="<?= $image->urlImage ?>" alt="<?= $variationProduct->name ?>"></li>

                    <?php endforeach; ?>
                <?php endif; ?>

            </ul>
            <div class="shop-item-page__zoom-here"></div>
        </div>
        <div class="shop-item-page__item-prop col-xl-5 col-lg-5">
            <h1 data-model-attr="name"><?= $variationProduct->name ?></h1>

            <?= $this->render('rating-widget', compact('masterProduct')); ?>

            <div class="shop-item-page__price">
                <div>
                    <div class="normal-price"><span><?= Setting::value('product.shop.currency') ?></span><span data-model-attr="price"><?= $variationProduct->viewNumberFormatFullPrice ?></span></div>

                    <?php if ($variationProduct->isDiscount): ?>

                        <div class="discount-price"><span><?= Setting::value('product.shop.currency') ?></span><span data-model-attr="promotion"><?= ProductVariation::viewFormatPrice($variationProduct->priceDiscount) ?></span></div>

                    <?php else : ?>

                        <div class="discount-price"><span></span><span data-model-attr="promotion"></span></div>

                    <?php endif; ?>

                </div>
                <p><?= Yii::t('product', 'plus 19% VAT.') ?></p>
            </div>

            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['/product/cart/add-product-to-cart', 'id' => $variationProduct->id]),
                'id' => 'form-cart',
                'options' => [
                    'class' => 'shop-item-page__buy',
                ],
            ]); ?>

                <div class="shop-item-page__quantiti-of-order">
                    <span class="minus">-</span>
                    <div class="label-wrapper">
                        <label for="shop-item-page__quantiti-of-order"><?= Yii::t('product', 'QTY') ?>:</label>

                        <?= $form->field($formAddToCart, 'count',[
                            'options' => [
                                'tag' => false
                            ]
                        ])->textInput([
                            'value' => $formAddToCart->count,
                            'class' => false,
                            'readonly' => true
                        ])->label(false); ?>
                    </div>
                    <span class="plus">+</span>
                </div>

                <div class="shop-item-page__button-for-order">

                    <?= Html::hiddenInput('variation-count', $variationProduct->getMaxCountForCart(), ['data-model-attr' => 'count']); ?>

                    <?= Html::button(Yii::t('product', 'Add to bag').' <svg class="cart-white svg"><use xlink:href="#Cart-white"></use></svg>', [
                            'class' => 'btn btn-warning',
                            'disabled' => ($variationProduct->properties || ($variationProduct->inventorie && !$variationProduct->inventorie->count))?true:false
                    ]); ?>

                </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>

<?= $this->render('_set-accordion', compact('masterProduct')); ?>

<section class="container-fluid shop-item-page__description-wrapper">
    <div class="container">
        <div class="row shop-item-page__description">
            <div class="shop-item-page__product-details col-xl-10 offset-xl-2 col-lg-10 offset-lg-1">
                <h5><?= Yii::t('product', 'Product details') ?></h5>
                <div data-model-attr="description" class="shop-item-page__description-text-wrapper">

                    <?= $variationProduct->description ?>

                </div>
            </div>
        </div>
    </div>
</section>

<?= SmallSliderWithProducts::widget([
    'mainWrapperOptions' => [
        'tag' => 'section',
        'class' => 'shop-item-page__carousel1 container',
    ],
    'headerOptions' => [
        'title' => Yii::t('product', 'You may also like'),
    ],
    'wrapperOptions' => [
        'class' => 'owl-carousel owl-theme all-pages-carousel',
    ],
    'itemOptions' => [
        'class' => 'item',
    ],
    'shopNowLink' => false,
    'typeWidget' => SmallSliderWithProducts::TYPE_RECOMMENDS_FROM_PRODUCT,
    'product' => $masterProduct,
]); ?>

<div class="container-fluid">
    <hr>
</div>
<div class="shop-item-page__comments container">
    <h3><?= Yii::t('product', 'Customer reviews') ?></h3>
    <div class="shop-item-page__comments-adaptive-wrapper">
        <p class="shop-item-page__item-name"><?= $variationProduct->name ?></p>

        <?php if (!Yii::$app->user->isGuest && $formReview->isAccessSetRating()) : ?>

            <?= $this->render('form-review', compact('formReview')); ?>

        <?php endif; ?>
    </div>

    <?= ($masterProduct->hasModerateRewiews()) ? Html::tag('hr') : ''; ?>

    <div id="block-reviews">

        <?php Pjax::begin([
            'enablePushState' => false
        ]); ?>

            <?= ProductReviews::widget([
                'product' => $masterProduct,
            ]); ?>

        <?php Pjax::end(); ?>

    </div>
</div>
<div class="container-fluid">
    <hr>
</div>

<?= SmallSliderWithProducts::widget([
    'mainWrapperOptions' => [
        'tag' => 'section',
        'class' => 'shop-item-page__carousel2 container',
    ],
    'headerOptions' => [
        'title' => Yii::t('core', 'Recommended for you'),
    ],
    'wrapperOptions' => [
        'class' => 'owl-carousel owl-theme all-pages-carousel',
    ],
    'itemOptions' => [
        'class' => 'item',
    ],
    'shopNowLink' => false,
    'typeWidget' => SmallSliderWithProducts::TYPE_ALL_RECOMMENDS,
]); ?>
