<?php

/** \yii\web\View $this */

use yii\helpers\Url;

/** \frontend\modules\product\models\Product $model */
/** \frontend\modules\product\models\ProductVariation $productVariation */
/** string $slug */
/** int $key */

?>

<div class="col-lg-2">
    <?php if($model->preview): ?>
        <img src="<?= $model->preview->getUrlImage() ?>" alt="<?= $model->name ?>" style="height: 182px">
    <?php endif; ?>

    <p><a href='<?= Url::to(['/product/product/index', 'slug' => $model->product->slug, 'variation' => $model->id]) ?>'><?= $model->name; ?></a></p>
    <div><?= $model->short_description; ?></div>
</div>