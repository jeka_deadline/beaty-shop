<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

/** @var \frontend\modules\core\components\View $this */
/** @var \frontend\modules\product\models\forms\ProductReviewForm $formReview */
/** @var \yii\widgets\ActiveForm $form */
?>

<?php $form = ActiveForm::begin([
    'id' => 'form-create-review',
    'action' => ['/product/product/create-review', 'productId' => $formReview->getProductId()],
]); ?>

    <?= $form->field($formReview, 'parentId')->hiddenInput()->label(false); ?>

    <?= $form->field($formReview, 'rating')
        ->dropDownList([
            '1' => '1',
            '2' => '2',
            '3' => '3',
            '4' => '4',
            '5' => '5'
        ],[
            'prompt' => ''
        ]) ?>

    <?= $form->field($formReview, 'subject')->textInput(['maxlength' => true]) ?>

    <?= $form->field($formReview, 'text')->textarea(); ?>

    <?= Html::submitButton(Yii::t('product', 'Create review'), ['class' => 'btn btn-primary']); ?>

<?php ActiveForm::end(); ?>

<?php
$script = <<< JS
    $('#productreviewform-rating').barrating({
        theme: "css-stars",
        readonly: false,
        initialRating: 0,
    });
JS;

$this->registerJs($script, yii\web\View::POS_READY);
?>
