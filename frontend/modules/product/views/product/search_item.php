<?php

use frontend\modules\core\models\Setting;
use yii\helpers\Url;
use frontend\modules\product\models\ProductVariation;

/** @var \frontend\modules\core\components\View $this */
/** @var \frontend\modules\product\models\ProductVariation $model */
/** @var string $slugMaster */

$slugMaster = $model->product->slug;

?>

<div class="shop-row-margin col-xl-3 col-lg-3 col-md-4 col-sm-4 col-6">
    <a class="shop-item" href="<?= Url::toRoute(['/product/product/index', 'slug' => $slugMaster]); ?>" title="<?= $model->name; ?>">
        <div class="shop-item-img" style="background-image: url(<?= $model->getImagePreview(170, 170) ?>)"></div>
        <div class="shop-item-body">
            <p class="shop-item__item-title"><?= $model->name; ?></p>
            <div class="shop-item__item-price-wrapper">
                <?php if ($model->isDiscount): ?>
                    <p class="shop-item__item-share-price"><?= Setting::value('product.shop.currency') ?><?= ProductVariation::viewFormatPrice($model->priceDiscount) ?></p>
                <?php endif; ?>
                <p class="shop-item__item-price"><?= Setting::value('product.shop.currency') ?><?= $model->viewNumberFormatFullPrice ?></p>
            </div>
            <p class="shop-item__item-zz"><?= Yii::t('product', 'plus 19% VAT.') ?></p>
        </div>
    </a>
</div>