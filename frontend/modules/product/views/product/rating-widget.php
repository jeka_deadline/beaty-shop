<?php

use frontend\widgets\bootstrap4\Html;
use yii\helpers\Url;

/** @var \frontend\modules\core\components\View $this */
/** @var \frontend\modules\product\models\Product $masterProduct */
/** @var string $script */

?>

<?= Html::dropDownList('shop-item-page__item-prop-rating1',round($masterProduct->rating),[
    '1'=>'1',
    '2'=>'2',
    '3'=>'3',
    '4'=>'4',
    '5'=>'5',
],[
    'prompt' => '',
    'class' => 'star-ratings',
    'id' => 'shop-item-page__item-prop-rating1'
]) ?>
<div class="shop-item-page__number-of-reviews"><span data-model-attr="countReviews"><?= $masterProduct->countReviews ?></span> <?= Yii::t('product', 'reviews'); ?></div>

<?php
$script = <<< JS
    $('.star-ratings').barrating({
        theme: "css-stars",
        readonly: true,
        initialRating: null,
    });
JS;

$this->registerJs($script, yii\web\View::POS_READY);
?>
