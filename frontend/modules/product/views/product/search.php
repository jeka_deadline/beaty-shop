<?php

use frontend\modules\product\widgets\ProductCategoriesMenu;
use frontend\modules\product\widgets\ProductFreeRecommenders;
use frontend\modules\product\widgets\ProductLoadNextPage;
use frontend\modules\product\widgets\ProductSearch;
use frontend\modules\product\widgets\ProductSearchFilters;
use frontend\modules\product\widgets\ProductSearchMenu;
use frontend\modules\product\widgets\ProductSort;
use frontend\modules\product\widgets\SmallSliderWithProducts;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\widgets\ListView;

$this->bodyClass = 'search-page';

$count = $dataProductProviders->totalCount;

?>
<?php if ($count): ?>
    <section class="search-page__result">
        <h1><?= Yii::t('core', 'Woosh! Here are the results for'); ?> “<?= $formProductSearch->query ?>” <span>(<?= $count ?>)</span></h1>
        <div class="container">
            <hr>
        </div>
        <div class="adaptive-name-wrapper">
            <div class="container">
                <div class="shop-page__category-chosen-adaptive"><?= Yii::t('core', 'Search'); ?> <span>(<?= $count ?>)</span></div>
            </div>
            <div class="container">
                <hr>
            </div>
        </div>
    </section>
    <div class="search-filters-wrapper">
        <div class="container-fluid filters-wrapper" id="filters-wrapper">
            <div class="container">
                <?= ProductSearchFilters::widget([
                    'query' => $formProductSearch->query,
                    'formAction' => Url::to('search'),
                    'findVariationIds' => $findVariationIds,
                    'groupForm' => 'search-filter'
                ]) ?>
            </div>
        </div>
        <div class="container-fluid category-adaptive-wrapper" id="category-adaptive-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="row" id="accordion2">
                            <?= ProductCategoriesMenu::widget([
                                'template' => 'product-categories-menu-mobile'
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <aside class="col-xl-4">
                    <div id="accordion">
                        <?= ProductCategoriesMenu::widget(); ?>
                    </div>
                </aside>
                <main class="col-xl-8">
                    <div class="shop-page__main-head">
                        <div class="shop-page__category-chosen"><?= Yii::t('core', 'Search'); ?> <span>(<?= $count ?>)</span></div>
                        <div class="shop-page__sort">
                            <div class="shop-page__category-adaptive"><?= Yii::t('product', 'category'); ?></div>
                            <div class="shop-page__filter"><?= Yii::t('product', 'Filter') ?></div>
                            <?= ProductSort::widget([
                                'formAction' => Url::to('search'),
                                'groupForm' => 'search-filter'
                            ]) ?>
                        </div>
                    </div>
                    <div class="shop-page__shop-items">
                        <div class="shop-items row list-catalog">

                            <?= ListView::widget([
                                'dataProvider' => $dataProductProviders,
                                'itemView' => 'search_item',
                                'layout' => '{items}',
                                'summary' => false,
                                'itemOptions' => [
                                    'tag' => false
                                ],
                                'options' => [
                                    'tag' => false
                                ],
                            ]); ?>

                        </div>
                    </div>
                    <?= ProductLoadNextPage::widget([
                        'dataProvider' => $dataProductProviders
                    ]) ?>
                    <?= LinkPager::widget([
                        'pagination' => $dataProductProviders->pagination,
                        'disableCurrentPageButton' => true,
                        'options' => [
                            'class' => 'pagination d-none'
                        ]
                    ]) ?>
                </main>
            </div>
        </div>
    </div>
<?php else: ?>
    <section class="search-page__null-result">
        <h1><?= Yii::t('core', 'Sorry, we didn’t find any matches for', $formProductSearch->query); ?></h1>
        <div class="search-page__h1-add"><?= Yii::t('core', 'Check your spelling or use a more general term to try your search again.'); ?></div>
        <div class="container">
            <hr>
            <div class="row parent-for-adaptive-search">
                <div class="search-page__null-result-block col-xl-5 offset-xl-2 col-lg-6 offset-lg-1 col-md-7 col-sm-6 col-12">
                    <div class="main-search input-group main-search-null">
                        <?= ProductSearch::widget([
                            'groupForm' => 'search-filter',
                            'options' => [
                                'id' => 'null-search-input',
                                'placeholder' => Yii::t('product', 'Search'),
                                'class' => 'form-control',
                            ]
                        ]) ?>
                    </div>
                    <p><?= Yii::t('core', 'If you continue to have problems:'); ?></p>
                    <ul>
                        <li><a href="<?= Url::to('/core/index/contact-us')?>"><?= Yii::t('core', 'Contact us') ?></a></li>
                        <li><?= Yii::t('core', '<a href="{0}">Provide feedback</a> to help improve our search', Url::to('/core/index/contact-us')); ?></li>
                    </ul>
                </div>
                <div class="search-page__null-result-recom-wrapper col-xl-3 col-lg-4 col-md-5 col-sm-6 col-12">
                    <div class="search-page__null-result-recom">
                        <div class="search-page__null-result-recom-header"><?= Yii::t('core', 'Frequently Searched:'); ?></div>
                        <?= ProductSearchMenu::widget(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?= SmallSliderWithProducts::widget([
        'mainWrapperOptions' => [
            'class' => 'recommened container',
        ],
        'headerOptions' => [
            'title' => Yii::t('core', 'Recommended for you'),
            'class' => 'row justify-content-center align-items-center'
        ],
        'wrapperOptions' => [
            'class' => 'owl-carousel owl-theme all-pages-carousel',
        ],
        'itemOptions' => [
            'class' => 'item',
        ],
        'typeWidget' => SmallSliderWithProducts::TYPE_ALL_RECOMMENDS,
    ]); ?>

<?php endif; ?>
