<?php

use yii\helpers\Url;
use yii\helpers\Html;
use frontend\widgets\bootstrap4\ActiveForm;

/** @var \frontend\modules\core\components\View $this */
/** @var \frontend\modules\product\models\forms\CheckoutOrderReviewForm $orderReviewForm */
/** @var \frontend\widgets\bootstrap4\ActiveForm $form */

?>

<div class="checkout-page__form">
    <div class="checkout-page__form-headers"><?= Yii::t('core', 'your order information'); ?></div>
    <div class="checkout-page__payment-info"><?= Yii::t('core', 'By clicking the Place Order button, you confirm that you have read and understood, and accept our <a href="{0}">AGB</a>, <a href="{1}">Return Policy</a>, and <a href="{2}">Private Policy</a>', [
          Url::toRoute(['/pages/index/page', 'uri' => 'terms']),
          Url::toRoute(['/pages/index/page', 'uri' => 'return']),
          Url::toRoute(['/pages/index/page', 'uri' => 'terms']),
      ]); ?></div>
</div>
<div class="row justify-content-end">

    <?php $form = ActiveForm::begin([
        'action' => '/product/checkout/process',
        'options' => [
            'class' => 'grey-form checkout-page__order-review-checkboxes-wrapper',
            'id' => 'order-review-form',
        ],
    ]); ?>
        <div class="checkbox-wrapper">

            <?= $form->field($orderReviewForm, 'dataProtection', [
                'options' => [
                    'class' => 'form-check',
                ],
                'checkboxTemplate' => '<div class="custom-control custom-checkbox">{input}{label}{error}</div>',
            ])->checkbox(
                ['class' => 'custom-control-input', 'id' => 'checkout-page__Dateschutz',],
                ['class' => 'custom-control-label',]
            ); ?>

            <?= $form->field($orderReviewForm, 'agb', [
                'options' => [
                    'class' => 'form-check',
                ],
                'checkboxTemplate' => '<div class="custom-control custom-checkbox">{input}{label}{error}</div>',
            ])->checkbox(
                ['class' => 'custom-control-input', 'id' => 'checkout-page__AGB',],
                ['class' => 'custom-control-label',]
            ); ?>

            <?= $form->field($orderReviewForm, 'newsletter', [
                'options' => [
                    'class' => 'form-check',
                ],
                'checkboxTemplate' => '<div class="custom-control custom-checkbox">{input}{label}</div>',
            ])->checkbox(
                ['class' => 'custom-control-input', 'id' => 'checkout-page__Newsletter',],
                ['class' => 'custom-control-label',]
            ); ?>

        </div>
        <button class="btn btn-primary checkout-page__third-form-submit auto-width" type="submit"><?= Yii::t('core', 'place order'); ?></button>

    <?php ActiveForm::end(); ?>

</div>
