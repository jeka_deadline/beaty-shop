<?php

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\modules\core\models\Setting;
use frontend\modules\product\models\ProductVariation;

?>

<?= $form->field($checkoutShippingForm, 'shippingMethod', [
    'labelOptions' => [
        'class' => 'checkout-page__select-speed',
    ],
])->radioList($checkoutShippingForm->getListShippingMethods(), [
    'item' => function ($index, $label, $name, $checked, $value) use ($checkoutShippingForm) {
          switch ($value) {
              case $checkoutShippingForm->getShippingStandartMethod():
                  $wrapperClass = 'checkout-page__first-form-radio-wrapper-standart';
                  $shippingPrice = $checkoutShippingForm->getShippingStandartMethodPrice();
                  $shippingPriceWrapperClass = 'checkout-page__radio-standart-price';
                  $shippingDateWrapperClass = 'checkout-page__radio-standart-dates';
                  $date = Yii::t('shipping', 'Fast delivery about 1-3 working days');
                  $inputId = 'checkout-page__radio-standart';
                  break;
              case $checkoutShippingForm->getShippingPickupMethod():
                  $wrapperClass = 'checkout-page__first-form-radio-wrapper-standart';
                  $shippingPrice = $checkoutShippingForm->getShippingPickupMethodPrice();
                  $shippingPriceWrapperClass = 'checkout-page__radio-standart-price';
                  $shippingDateWrapperClass = 'checkout-page__radio-standart-dates';
                  $date = Yii::t('shipping', 'Pick up the next working day from 10am to 6pm in our studio. Kaiserstrasse 59, 44135 Dortmund');
                  $inputId = 'checkout-page__radio-pickup';
                  break;
              default:
                  $wrapperClass = 'checkout-page__first-form-radio-wrapper-express';
                  $shippingPrice = $checkoutShippingForm->getShippingExpressMethodPrice();
                  $shippingPriceWrapperClass = 'checkout-page__radio-express-price';
                  $shippingDateWrapperClass = 'checkout-page__radio-express-dates';
                  $date = Yii::t('shipping', 'Delivery the next working day when ordering until 15:00');
                  $inputId = 'checkout-page__radio-express';
                  break;
          }

          $item = Html::beginTag('div', [
              'class' => $wrapperClass,
          ]);

          $radio = Html::radio($name, $checked, [
              'value' => $value,
              'class' => 'custom-control-input',
              'id' => $inputId,
          ]);

          $label = "<label for='{$inputId}' class='custom-control-label'>{$label}</label>";

          $item .= "<div class='custom-control custom-radio custom-control-inline'>{$radio}{$label}</div>";

          $item .= "<div class='{$shippingDateWrapperClass}'>{$date}</div>";
          $item .= "<div class='{$shippingPriceWrapperClass}'>".Setting::value('product.shop.currency') . ProductVariation::viewFormatPrice($shippingPrice) . "</div>";

          $item .= Html::endTag('div');

          return $item;
    },
    'class' => 'checkout-page__first-form-radio',
    'id' => 'shipping-method-js',
    'data-url' => Url::toRoute(['/product/checkout/change-shipping-method']),
]); ?>
