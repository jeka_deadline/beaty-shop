<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>
    <?php if ($isAuthUser && $checkoutPaymentForm->isCreditCardPaymentMethod()) : ?>

        <div class="checkout-page__form-headers"><?= Yii::t('core', 'add card'); ?>
            <div class="checkout-page__select-card-wrapper">

                <?= Html::activeDropDownList($checkoutPaymentForm, 'savedPaymentCardId', $checkoutPaymentForm->getUserSavedPaymentCards(), [
                    'id' => 'checkout-page__select-card',
                    'class' => 'form-control checkout-page__select-card',
                    'prompt' => Yii::t('core', 'Choose saved payment card'),
                    'data-url' => Url::toRoute(['/product/checkout/get-payment-form-for-saved-payment-card']),
                ]); ?>

                <div class="checkout-page__select-card-text"><?= Yii::t('core', 'Select saved Credit Card'); ?></div>
            </div>
        </div>

    <?php endif; ?>

    <?php if ($checkoutPaymentForm->isCreditCardPaymentMethod()) : ?>

        <?= $this->render('checkout-credit-card-form', compact('form', 'isAuthUser', 'checkoutPaymentForm')); ?>

    <?php endif; ?>

    <?php if ($isAuthUser && $checkoutPaymentForm->isCreditCardPaymentMethod()) : ?>

        <?= $form->field($checkoutPaymentForm, 'saveCard', [
            'options' => [
                'class' => 'form-check grey-form__save-card-wrapper',
            ],
            'checkboxTemplate' => '<div class="grey-form__save-card custom-control custom-checkbox">{input}{label}</div>',
        ])->checkbox(
            ['class' => 'custom-control-input',],
            ['class' => 'custom-control-label',]
        ); ?>

    <?php endif; ?>