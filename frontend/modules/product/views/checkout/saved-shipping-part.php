<?php

use frontend\modules\core\models\Setting;
use frontend\modules\product\models\ProductVariation;

/** @var \frontend\modules\core\components\View $this */
/** @var \frontend\modules\product\models\forms\CheckoutShippingForm $checkoutShippingForm */

?>
<div class="checkout-page__form-ready">
    <div class="checkout-page__form-ready-header"><?= Yii::t('user', 'Shipping Address') ?></div>
    <div class="checkout-page__form-ready-body">
        <div class="checkout-page__form-ready-body-text name"><?= sprintf('%s %s %s', ucfirst($checkoutShippingForm->humanTitle), $checkoutShippingForm->surname, $checkoutShippingForm->name); ?></div>
        <div class="checkout-page__form-ready-body-text company"><?= $checkoutShippingForm->company; ?></div>
        <div class="checkout-page__form-ready-body-text address-1"><?= $checkoutShippingForm->address1; ?></div>

        <?php if ($checkoutShippingForm->address2) : ?>

            <div class="checkout-page__form-ready-body-text address-1"><?= $checkoutShippingForm->address2; ?></div>

        <?php endif; ?>

        <div class="checkout-page__form-ready-body-text address-2"><?= sprintf('%s, %s, %s', $checkoutShippingForm->city, $checkoutShippingForm->zip, $checkoutShippingForm->getCountryName()); ?></div>
        <div class="checkout-page__form-ready-body-text email"><?= $checkoutShippingForm->email; ?></div>
    </div>
    <div class="checkout-page__form-ready-header"><?= Yii::t('shipping', 'Shipping Speed') ?></div>
    <div class="checkout-page__form-ready-body-text price">
        <?= sprintf('%s: '.Setting::value('product.shop.currency').'%s', $checkoutShippingForm->getShippingMethodName(), ProductVariation::viewFormatPrice($checkoutShippingForm->getShippingPriceByMethod())); ?>
    </div>
    <div class="checkout-page__when-get">
        <?= $checkoutShippingForm->getShippingMethodInfo(); ?>
    </div>
</div>