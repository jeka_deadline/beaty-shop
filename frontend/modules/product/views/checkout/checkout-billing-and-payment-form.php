<?php

use yii\helpers\Html;
use yii\widgets\MaskedInput;
use frontend\widgets\bootstrap4\ActiveForm;
use yii\helpers\Url;

/** @var \frontend\modules\core\components\View $this */
/** @var \frontend\widgets\bootstrap4\ActiveForm $form */
/** @var \frontend\modules\product\models\forms\CheckoutBillingForm $checkoutBillingForm */
/** @var \frontend\modules\product\models\forms\CheckoutPaymentForm $checkoutPaymentForm */
/** @var bool $isAuthUser */
/** @var bool $isNeedBillingForm */

?>

<?php $form = ActiveForm::begin([
    'id' => 'order-payment-form',
    'action' => ['/product/checkout/save-billing-and-payment-part'],
    'options' => [
        'class' => 'grey-form',
    ],
    'enableAjaxValidation' => true,
    'validationUrl' => ['/product/checkout/ajax-validate-billing-and-payment-part'],
    'enableClientValidation' => false,
    'validateOnChange' => false,
    'validateOnBlur' => false,
]); ?>

    <input type="hidden" name="pseudocardpan" id="pseudocardpan">
    <input type="hidden" name="truncatedcardpan" id="truncatedcardpan">

    <div class="checkout-page__form-headers"><?= Yii::t('core', 'Your billing information'); ?></div>

    <?php if ($isNeedBillingForm) : ?>

        <div class="checkout-page__this-adress-unchecked checkout-page__payment-show">

            <?= $form->field($checkoutBillingForm, 'title', [
                'inputOptions' => [
                    'required' => true,
                    'id' => 'checkout-page__select-who-base',
                ],
                'labelOptions' => [
                    'class' => '',
                ],
            ])->dropDownList($checkoutBillingForm->getListUserTitles(), [
                'prompt' => '',
            ])->label(Yii::t('product', 'Title') . '*'); ?>

            <div class="checkout-page__name-group">

                <?= $form->field($checkoutBillingForm, 'name', [
                    'inputOptions' => [
                        'placeholder' => Yii::t('product', 'First Name'),
                        'required' => true,
                        'id' => 'checkout-page__first-name-base'
                    ],
                    'options' => [
                        'class' => 'form-group checkout-page__first-name',
                    ],
                    'labelOptions' => [
                        'class' => '',
                    ],
                ])->label($checkoutBillingForm->getAttributeLabel('name') . '*'); ?>

                <?= $form->field($checkoutBillingForm, 'surname', [
                    'inputOptions' => [
                        'placeholder' => Yii::t('product', 'Last Name'),
                        'required' => true,
                        'id' => 'checkout-page__last-name-base'
                    ],
                    'options' => [
                        'class' => 'form-group checkout-page__last-name',
                    ],
                    'labelOptions' => [
                        'class' => '',
                    ],
                ])->label($checkoutBillingForm->getAttributeLabel('surname') . '*'); ?>

            </div>

            <?= $form->field($checkoutBillingForm, 'company', [
                'inputOptions' => [
                    'placeholder' => 'Infinity lashes',
                    'id' => 'account-page__company-name-base',
                ],
                'labelOptions' => [
                    'class' => '',
                ],
            ]); ?>

            <div class="form-group checkout-page__address-input">

                <label for="checkout-page__address-add-base-1"><?= $checkoutBillingForm->getAttributeLabel('address1'); ?>*</label>

                <?= $form->field($checkoutBillingForm, 'address1', [
                    'inputOptions' => [
                        'placeholder' => 'Kaiserstr. 59',
                        'id' => 'checkout-page__address-add-base-1',
                        'class' => 'form-control',
                        'required' => true,
                    ],
                    'options' => [
                        'tag' => 'false',
                    ],
                ])->label(false); ?>

                <?php if ($checkoutBillingForm->address2) : ?>

                    <?= $form->field($checkoutBillingForm, 'address2', [
                        'inputOptions' => [
                            'placeholder' => 'Kaiserstr. 59',
                            'id' => 'checkout-page__address-add-base-2',
                            'class' => 'form-control',
                            'required' => true,
                        ],
                        'options' => [
                            'tag' => false,
                        ],
                    ])->label(false); ?>

                <?php endif; ?>

            </div>

            <div class="checkout-page__address-input-add-remove">
                <a class="checkout-page__add-address-line-base" data-model-name="<?= Html::getInputName($checkoutBillingForm, 'address2'); ?>" href="#">+ <?= Yii::t('shipping', 'Add address line'); ?></a>
                <a class="checkout-page__remove-address-line-base" href="#">- <?= Yii::t('shipping', 'Remove address line'); ?></a>
            </div>

            <div class="checkout-page__city-zip">

                <?= $form->field($checkoutBillingForm, 'city', [
                    'inputOptions' => [
                        'placeholder' => 'E. g. Dortmund',
                        'required' => true,
                        'id' => 'checkout-page__city-add-base',
                    ],
                    'labelOptions' => [
                        'class' => '',
                    ],
                ])->label($checkoutBillingForm->getAttributeLabel('city') . '*'); ?>

                <?= $form->field($checkoutBillingForm, 'zip', [
                    'labelOptions' => [
                        'class' => false,
                    ],
                    'options' => [
                        'class' => 'form-group checkout-page__zip-code',
                    ]
                ])
                /*->widget(MaskedInput::className(), [
                    'mask' => '99999',
                    'options' => [
                        'placeholder' => '44135',
                        'id' => 'checkout-page__zip-add-base',
                        'class' => 'form-control',
                        'required' => true,
                    ],
                ])*/
                ->label($checkoutBillingForm->getAttributeLabel('zip') . '*'); ?>

            </div>

            <?= $form->field($checkoutBillingForm, 'countryId', [
                'labelOptions' => [
                    'class' => false,
                ],
                'inputOptions' => [
                    'id' => 'checkout-page__country-add-base',
                    'class' => 'form-control',
                    'required' => true,
                ],
            ])->dropDownList($checkoutBillingForm->getListCountriesForDropDown()); ?>

        </div>

    <?php else : ?>

        <div class="checkout-page__payment-info checkout-page__this-adress-checked"><?= Yii::t('core', 'You have indicated the same shipping and billing address. If you want to change your choice, uncheck the box \'Use this address for billing\' and fill out the form.'); ?></div>

    <?php endif; ?>

    <label class="checkout-page__form-headers" for="checkout-page__payment-card-1"><?= Yii::t('core', 'select payment method'); ?></label><br>

    <?= $form->field($checkoutPaymentForm, 'type', [
        'options' => [
            'tag' => false,
        ]
    ])->radioList($checkoutPaymentForm->getPaymentMethods(), [
        'item' => function ($index, $label, $name, $checked, $value) use ($checkoutPaymentForm) {
            switch ($value) {
                case $checkoutPaymentForm->getCreditCardMethod():
                    $wrapperClass = 'custom-control custom-radio custom-control-inline checkout-page__payment-card-1';
                    $svg = '<svg class="payment-cards cardyellow svg master" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 146.8 120.41"> <defs> <style> .cls_mc-1 { fill: none; } .cls_mc-2 { fill: #231f20; } .cls_mc-3 { fill: #ff5f00; } .cls_mc-4 { fill: #eb001b; } .cls_mc-5 { fill: #f79e1b; } </style> </defs> <title>mc_vrt_rgb_pos</title> <g id="Layer_2" data-name="Layer 2"> <g id="Layer_1-2" data-name="Layer 1"> <rect class="cls_mc-1" width="146.8" height="120.41" /> <path class="cls_mc-2" d="M36.35,105.26v-6a3.56,3.56,0,0,0-3.76-3.8,3.7,3.7,0,0,0-3.36,1.7,3.51,3.51,0,0,0-3.16-1.7,3.16,3.16,0,0,0-2.8,1.42V95.7H21.19v9.56h2.1V100a2.24,2.24,0,0,1,2.34-2.54c1.38,0,2.08.9,2.08,2.52v5.32h2.1V100a2.25,2.25,0,0,1,2.34-2.54c1.42,0,2.1.9,2.1,2.52v5.32ZM67.42,95.7H64V92.8h-2.1v2.9H60v1.9h1.94V102c0,2.22.86,3.54,3.32,3.54a4.88,4.88,0,0,0,2.6-.74l-.6-1.78a3.84,3.84,0,0,1-1.84.54c-1,0-1.38-.64-1.38-1.6V97.6h3.4Zm17.74-.24a2.82,2.82,0,0,0-2.52,1.4V95.7H80.58v9.56h2.08V99.9c0-1.58.68-2.46,2-2.46a3.39,3.39,0,0,1,1.3.24l.64-2a4.45,4.45,0,0,0-1.48-.26Zm-26.82,1a7.15,7.15,0,0,0-3.9-1c-2.42,0-4,1.16-4,3.06,0,1.56,1.16,2.52,3.3,2.82l1,.14c1.14.16,1.68.46,1.68,1,0,.74-.76,1.16-2.18,1.16a5.09,5.09,0,0,1-3.18-1l-1,1.62a6.9,6.9,0,0,0,4.14,1.24c2.76,0,4.36-1.3,4.36-3.12s-1.26-2.56-3.34-2.86l-1-.14c-.9-.12-1.62-.3-1.62-.94s.68-1.12,1.82-1.12a6.16,6.16,0,0,1,3,.82Zm55.71-1a2.82,2.82,0,0,0-2.52,1.4V95.7h-2.06v9.56h2.08V99.9c0-1.58.68-2.46,2-2.46a3.39,3.39,0,0,1,1.3.24l.64-2a4.45,4.45,0,0,0-1.48-.26Zm-26.8,5a4.83,4.83,0,0,0,5.1,5,5,5,0,0,0,3.44-1.14l-1-1.68a4.2,4.2,0,0,1-2.5.86,3.07,3.07,0,0,1,0-6.12,4.2,4.2,0,0,1,2.5.86l1-1.68a5,5,0,0,0-3.44-1.14,4.83,4.83,0,0,0-5.1,5Zm19.48,0V95.7h-2.08v1.16a3.63,3.63,0,0,0-3-1.4,5,5,0,0,0,0,10,3.63,3.63,0,0,0,3-1.4v1.16h2.08Zm-7.74,0a2.89,2.89,0,1,1,2.9,3.06,2.87,2.87,0,0,1-2.9-3.06Zm-25.1-5a5,5,0,0,0,.14,10A5.81,5.81,0,0,0,78,104.16l-1-1.54a4.55,4.55,0,0,1-2.78,1,2.65,2.65,0,0,1-2.86-2.34h7.1c0-.26,0-.52,0-.8,0-3-1.86-5-4.54-5Zm0,1.86a2.37,2.37,0,0,1,2.42,2.32h-5a2.46,2.46,0,0,1,2.54-2.32ZM126,100.48V91.86H124v5a3.63,3.63,0,0,0-3-1.4,5,5,0,0,0,0,10,3.63,3.63,0,0,0,3-1.4v1.16H126Zm3.47,3.39a1,1,0,0,1,.38.07,1,1,0,0,1,.31.2,1,1,0,0,1,.21.3.93.93,0,0,1,0,.74,1,1,0,0,1-.21.3,1,1,0,0,1-.31.2.94.94,0,0,1-.38.08,1,1,0,0,1-.9-.58.94.94,0,0,1,0-.74,1,1,0,0,1,.21-.3,1,1,0,0,1,.31-.2A1,1,0,0,1,129.5,103.87Zm0,1.69a.71.71,0,0,0,.29-.06.75.75,0,0,0,.23-.16.74.74,0,0,0,0-1,.74.74,0,0,0-.23-.16.72.72,0,0,0-.29-.06.75.75,0,0,0-.29.06.73.73,0,0,0-.24.16.74.74,0,0,0,0,1,.74.74,0,0,0,.24.16A.74.74,0,0,0,129.5,105.56Zm.06-1.19a.4.4,0,0,1,.26.08.25.25,0,0,1,.09.21.24.24,0,0,1-.07.18.35.35,0,0,1-.21.09l.29.33h-.23l-.27-.33h-.09v.33h-.19v-.88Zm-.22.17v.24h.22a.21.21,0,0,0,.12,0,.1.1,0,0,0,0-.09.1.1,0,0,0,0-.09.21.21,0,0,0-.12,0Zm-11-4.06a2.89,2.89,0,1,1,2.9,3.06,2.87,2.87,0,0,1-2.9-3.06Zm-70.23,0V95.7H46v1.16a3.63,3.63,0,0,0-3-1.4,5,5,0,0,0,0,10,3.63,3.63,0,0,0,3-1.4v1.16h2.08Zm-7.74,0a2.89,2.89,0,1,1,2.9,3.06A2.87,2.87,0,0,1,40.32,100.48Z" /> <g id="_Group_" data-name="&lt;Group&gt;"> <rect class="cls_mc-3" x="57.65" y="22.85" width="31.5" height="56.61" /> <path id="_Path_" data-name="&lt;Path&gt;" class="cls_mc-4" d="M59.65,51.16A35.94,35.94,0,0,1,73.4,22.85a36,36,0,1,0,0,56.61A35.94,35.94,0,0,1,59.65,51.16Z" /> <path class="cls_mc-5" d="M131.65,51.16A36,36,0,0,1,73.4,79.46a36,36,0,0,0,0-56.61,36,36,0,0,1,58.25,28.3Z" /> <path class="cls_mc-5" d="M128.21,73.46V72.3h.47v-.24h-1.19v.24H128v1.16Zm2.31,0v-1.4h-.36l-.42,1-.42-1H129v1.4h.26V72.41l.39.91h.27l.39-.91v1.06Z" /> </g> </g> </g> </svg> <svg class="payment-cards cardyellow svg visa" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 40 30"> <defs> <style> .cls-mc_1_1 { fill: none; } .cls-mc_1_2 { fill: none; } .cls-mc_1_3 { fill: transparent; } .cls-mc_1_4 { clip-path: url(#clip-path); } .cls-mc_1_5 { fill: url(#linear-gradient); } </style> <clipPath id="clip-path"> <path class="cls-mc_1_1" d="M13.84,10.33,11.32,16.7l-1-5.42a1.13,1.13,0,0,0-1.12-1H5.06L5,10.6a10.14,10.14,0,0,1,2.39.8,1,1,0,0,1,.58.82L9.9,19.7h2.56l3.93-9.37Zm3.59,0-2,9.37h2.42l2-9.37Zm13.9,2.53.73,3.52h-2ZM31,10.33a1.1,1.1,0,0,0-1,.69L26.3,19.7h2.55l.51-1.4h3.11l.29,1.4H35l-2-9.37ZM20.51,13.25c0,1.35,1.2,2.1,2.12,2.55s1.26.75,1.26,1.16c0,.63-.75.91-1.45.92A5.07,5.07,0,0,1,20,17.29l-.44,2.05a7.35,7.35,0,0,0,2.7.5c2.55,0,4.21-1.26,4.22-3.21,0-2.47-3.42-2.61-3.4-3.71,0-.34.33-.69,1-.78a4.58,4.58,0,0,1,2.39.42l.43-2a6.52,6.52,0,0,0-2.27-.42c-2.4,0-4.08,1.27-4.1,3.1" /> </clipPath> <linearGradient id="linear-gradient" x1="-286.24" y1="411.23" x2="-286" y2="411.23" gradientTransform="matrix(126.55, 0, 0, -126.55, 36227.36, 52054.4)" gradientUnits="userSpaceOnUse"> <stop offset="0" stop-color="#241f5d" /> <stop offset="1" stop-color="#034ea1" /> </linearGradient> </defs> <title>Visa</title> <g id="Layer_2" data-name="Layer 2"> <g id="Layer_1-2" data-name="Layer 1"> <rect class="cls-mc_1_2" x="0.35" y="0.35" width="39.29" height="29.29" rx="1.65" ry="1.65" /> <path class="cls-mc_1_3" d="M38,.71A1.29,1.29,0,0,1,39.29,2V28A1.29,1.29,0,0,1,38,29.29H2A1.29,1.29,0,0,1,.71,28V2A1.29,1.29,0,0,1,2,.71H38M38,0H2A2,2,0,0,0,0,2V28a2,2,0,0,0,2,2H38a2,2,0,0,0,2-2V2a2,2,0,0,0-2-2Z" /> <g id="Visa"> <g class="cls-mc_1_4"> <rect id="_Path_" data-name="&lt;Path&gt;" class="cls-mc_1_5" x="5" y="10.16" width="30" height="9.69" /> </g> </g> </g> </g> </svg>';
                    $labelText = $checkoutPaymentForm->getAttributeLabel('credit_card');
                    $inputId = 'checkout-page__payment-card-1';
                    break;
                case $checkoutPaymentForm->getPaypalMethod():
                    $wrapperClass = 'custom-control custom-radio custom-control-inline';
                    $svg = '<svg class="payment-cards paypal svg"><use xlink:href="#paypal"></use></svg>';
                    $labelText = '';
                    $inputId = 'checkout-page__payment-card-pp';
                    break;
                case $checkoutPaymentForm->getKlarnaMethod():
                    $wrapperClass = 'custom-control custom-radio custom-control-inline';
                    $svg = '<svg class="payment-cards sofort svg"><use xlink:href="#sofort"></use></svg>';
                    $labelText = '';
                    $inputId = 'checkout-page__payment-card-su';
                    break;
                default:
                    $wrapperClass = 'custom-control custom-radio custom-control-inline checkout-page__payment-card-1';
                    $svg = '<svg class="payment-cards cardyellow svg"><use xlink:href="#cardyellow"></use></svg>';
                    $inputId = 'checkout-page__payment-card-1';
                    $labelText = $checkoutPaymentForm->getAttributeLabel('credit_card');
                    break;
            }

            $input = Html::radio($name, $checked, [
                'id' => $inputId,
                'value' => $value,
                'class' => 'custom-control-input',
            ]);

            $label = "<label class='custom-control-label' for='{$inputId}'>{$labelText}{$svg}</label>";

            return Html::tag('div', $input . $label, [
                'class' => $wrapperClass,
            ]);
        },
        'class' => 'checkout-page__second-form-radio row',
        'id' => 'select-payment-type-js',
        'data-url' => Url::toRoute(['/product/checkout/get-payment-credit-card-form'])
    ])->label(false); ?>

    <div id="block-payment-credit-card">

        <?= $this->render('block-payment-credit-card', [
            'isAuthUser' => $isAuthUser,
            'checkoutPaymentForm' => $checkoutPaymentForm,
            'form' => $form,
        ]); ?>

    </div>

    <div class="row justify-content-end">
        <button class="btn btn-primary checkout-page__second-form-submit" type="submit"><?= Yii::t('product', 'Apply') ?></button>
    </div>

<?php ActiveForm::end(); ?>