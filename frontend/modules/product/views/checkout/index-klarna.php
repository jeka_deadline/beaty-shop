<?php

/** @var \frontend\modules\core\components\View $this */
/** @var string $html_snippet */
$this->bodyClass = 'checkout-page';

?>
    <div class="svgHide">
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="0" height="0" style="position:absolute">
            <symbol id="account" viewBox="0 0 25 23">
                <title>account</title>
                <path d="M12.5 0A5.5 5.5 0 1 0 18 5.5 5.5 5.5 0 0 0 12.5 0zm0 10A4.5 4.5 0 1 1 17 5.5a4.5 4.5 0 0 1-4.5 4.5zm0 2A12.56 12.56 0 0 0 0 23h25a12.56 12.56 0 0 0-12.5-11zm-11 10c.66-5.07 5.33-9 11-9s10.34 3.93 11 9z" data-name="Layer 1"></path>
            </symbol>
            <symbol id="Cart" viewBox="0 0 50 50">
                <defs>
                    <style>
                        .cls-1-cart,
                        .cls-2-cart {
                            fill: none
                        }

                        .cls-1-cart {
                            stroke: #000;
                            stroke-linecap: round;
                            stroke-miterlimit: 10;
                            stroke-width: 2px
                        }
                    </style>
                </defs>
                <title>Cart</title>
                <g id="Layer_2" data-name="Layer 2">
                    <g id="Layer_1-2" data-name="Layer 1">
                        <path class="cls-1-cart" d="M8 14L4 49h42l-4-35z"></path>
                        <path class="cls-2-cart" d="M0 0h50v50H0z"></path>
                        <path class="cls-1-cart" d="M34 19v-8a9 9 0 0 0-18 0v8"></path>
                        <circle cx="34" cy="19" r="2"></circle>
                        <circle cx="16" cy="19" r="2"></circle>
                    </g>
                </g>
            </symbol>
        </svg>
    </div>
    <div class="main-first container">
        <h1>Checkout</h1>
        <hr>
<?= $html_snippet ?>
    </div>
