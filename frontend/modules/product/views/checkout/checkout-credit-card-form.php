<?php

use yii\widgets\MaskedInput;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var \frontend\modules\core\components\View $this */
/** @var bool $isAuthUser */
/** @var \frontend\modules\product\models\forms\CheckoutPaymentForm $checkoutPaymentForm */
/** @var \frontend\widgets\bootstrap4\ActiveForm $form */

?>
<div id="iframe">

    <style>
        .block-payment-credit-card, {
          display: flex;
          flex-direction: column;
          margin-bottom: 20px;
        }

        .block-payment-credit-card span {
          margin-bottom: 15px;
        }

        span#cardpan {
          width: 100%;
        }

        span#expireInput {
          width: 100%;
          display: flex;
        }

        span#expireInput span#cardexpiremonth iframe {
          width: 100px;
          margin-right: 20px;
        }

        span#expireInput span#cardexpireyear {
          width: 100px;
        }

    </style>

    <!-- place your input fields -->
    <div class="form-group">
        <label for="cardtypeInput"><?= Yii::t('product', 'Card type'); ?>:</label>
        <span id="cardtype" class="inputIframe"></span>
    </div>

    <div class="form-group">
        <label for="cardpanInput"><?= Yii::t('product', 'Card number'); ?>:</label>
        <span id="cardpan" class="inputIframe"></span>
    </div>

    <div class="form-group">
        <label for="cvcInput"><?= Yii::t('product', 'Security code'); ?>:</label>
        <span id="cardcvc2" class="inputIframe"></span>
    </div>

    <div class="form-group">
        <label for="expireInput"><?= Yii::t('product', 'Expiration date'); ?>:</label>
        <span id="expireInput" class="inputIframe">
            <span id="cardexpiremonth"></span>
            <span id="cardexpireyear"></span>
        </span>
    </div>
    <div id="errorOutput"></div>

    <script>
        var request, config;
        config = {
            fields: {
                cardtype: {
                    selector: "cardtype", // put name of your div-container here
                    cardtypes: ["V", "M", "A"], // define possible cardtypes in PAYONE iFrame
                    style: "height: 33px; border: none; width: 100%; padding-left: 10px; color: #495057; font-size: 14px;",
                    iframe: {
                        width: "100%",
                    }
                },
                cardpan: {
                    selector: "cardpan", // put name of your div-container here
                    type: "text", // text (default), password, tel
                    style: "width: 100%; border: none; height: 33px; background-color: #e6e6e6; padding-left: 10px; font-size: 0.9rem;",
                    iframe: {
                        width: "100%",
                    }
                },
                cardcvc2: {
                    selector: "cardcvc2", // put name of your div-container here
                    type: "password", // select(default), text, password, tel
                    style: "width: 100%; border: none; background-color: #e6e6e6; height: 33px; padding-left: 10px; font-size: 14px;",
                    size: "4",
                    maxlength: "4", // set max. length for CVC input; empty values possible
                    iframe: {
                        width: "100%",
                    },
                    length: {
                        "A": 4,
                        "V": 3,
                        "M": 3,
                        "J": 0
                    } // set required CVC length per cardtype
                    // if set exact length required; 0=CVC input disabled
                },
                cardexpiremonth: {
                    selector: "cardexpiremonth", // put name of your div-container here
                    type: "select", // select(default), text, password, tel
                    size: "2",
                    maxlength: "2",
                    style: "width: 100%; border: none; height: 33px; padding-left: 10px; font-size: 14px;",
                    iframe: {
                        width: "100px",
                    }
                },
                cardexpireyear: {
                    selector: "cardexpireyear", // put name of your div-container here
                    type: "select", // select(default), text, password, tel
                    style: "width: 100%; border: none; height: 33px; padding-left: 10px; font-size: 14px;",
                    iframe: {
                        width: "80px"
                    }
                }
            },
            defaultStyle: {
                input: "font-size: 0.9rem; border: 1px solid #000; width: 175px;",
                select: "font-size: 14px; border: 1px solid #000;",
                iframe: {
                    height: "33px",
                    width: "180px"
                }
            },
            error: "errorOutput", // area to display error-messages (optional)
            language: Payone.ClientApi.Language.ru // Language to display error-messages
            // (default: Payone.ClientApi.Language.en)
        };
        request = {
            request: 'creditcardcheck', // fixed value
            responsetype: 'JSON', // fixed value
            mode: 'live', // desired mode
            mid: '40937', // your MID
            aid: '41289', // your AID
            portalid: '2029517', // your PortalId
            encoding: 'UTF-8', // desired encoding
            storecarddata: 'yes', // fixed value
            // hash calculated over your request-parameter-values (alphabetical request-order) plus PMI portal key
            // hash:
            //'1cf456bf692453613ebb992a3fb859cc347ddc7e94e2ca764efbe8b0089de6964ab1266df0831e59de89dc5291070fe7'
            // test hash - 5bf34395c22d3c3848014be3c8d241ba
            // live hash - 6e6c234f6ead907ef90ef55d371e896d
            hash: '6e6c234f6ead907ef90ef55d371e896d' // see Chapter 3.1.5.3
        };
        window,iframes = new Payone.ClientApi.HostedIFrames(config, request);
    </script>

</div>
