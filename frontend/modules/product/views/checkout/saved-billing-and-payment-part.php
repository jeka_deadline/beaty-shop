<?php

/** @var \frontend\modules\core\components\View $this */
/** @var \frontend\modules\product\models\forms\CheckoutPaymentForm $checkoutPaymentForm */
/** @var \frontend\modules\product\models\forms\CheckoutShippingForm $checkoutShippingForm */
/** @var \frontend\modules\product\models\forms\CheckoutBillingForm $checkoutBillingForm */

?>

<div class="checkout-page__form-ready">
    <div class="checkout-page__form-ready-header"><?= Yii::t('core', 'Payment Method'); ?></div>
    <div class="checkout-page__form-ready-body">
        <div class="checkout-page__form-ready-body-svg-change">

<?php switch ($checkoutPaymentForm->type) : ?>
<?php case $checkoutPaymentForm->getCreditCardMethod() : ?>

    <svg class="payment-method-card cardyellow svg"><use xlink:href="#cardyellow"></use></svg>

<?php break; ?>
<?php case $checkoutPaymentForm->getPaypalMethod() : ?>

    <svg class="payment-method-card payment-method-card-paypal paypal svg"><use xlink:href="#paypal"></use></svg>

<?php break; ?>
<?php case $checkoutPaymentForm->getKlarnaMethod() : ?>

    <svg class="payment-method-card payment-method-card-sofort sofort svg"><use xlink:href="#sofort"></use></svg>

<?php break; ?>
<?php default: ?>

    <svg class="payment-method-card cardyellow svg"><use xlink:href="#cardyellow"></use></svg>

<?php break; ?>

<?php endswitch; ?>

        </div>

        <?php if ($checkoutPaymentForm->type == $checkoutPaymentForm->getCreditCardMethod()) : ?>

            <div class="checkout-page__form-ready-body-text card-num"></div>

        <?php else : ?>

            <div class="checkout-page__form-ready-body-text card-num"><span><?= $checkoutPaymentForm->getNamePaymentMethod(); ?></span></div>

        <?php endif; ?>

    </div>
    <div class="checkout-page__form-ready-header"><?= Yii::t('core', 'Billing Details'); ?></div>

    <?php if ($checkoutShippingForm->useShippingAddressForBilling) : ?>

        <div class="checkout-page__form-ready-body">
            <div class="checkout-page__form-ready-body-text name"><?= sprintf('%s %s %s', ucfirst($checkoutShippingForm->humanTitle), $checkoutShippingForm->surname, $checkoutShippingForm->name); ?></div>
            <div class="checkout-page__form-ready-body-text company"><?= $checkoutShippingForm->company; ?></div>
            <div class="checkout-page__form-ready-body-text address-3"><?= $checkoutShippingForm->address1; ?></div>

            <?php if ($checkoutShippingForm->address2) : ?>

                <div class="checkout-page__form-ready-body-text address-3"><?= $checkoutShippingForm->address2; ?></div>

            <?php endif; ?>

            <div class="checkout-page__form-ready-body-text address-4"><?= sprintf('%s, %s, %s', $checkoutShippingForm->city, $checkoutShippingForm->zip, $checkoutShippingForm->getCountryName()); ?></div>
        </div>

    <?php else : ?>

        <div class="checkout-page__form-ready-body">
            <div class="checkout-page__form-ready-body-text name"><?= sprintf('%s %s %s', ucfirst($checkoutBillingForm->humanTitle), $checkoutBillingForm->surname, $checkoutBillingForm->name); ?></div>
            <div class="checkout-page__form-ready-body-text company"><?= $checkoutBillingForm->company; ?></div>
            <div class="checkout-page__form-ready-body-text address-3"><?= $checkoutBillingForm->address1; ?></div>

            <?php if ($checkoutBillingForm->address2) : ?>

                <div class="checkout-page__form-ready-body-text address-3"><?= $checkoutBillingForm->address2; ?></div>

            <?php endif; ?>

            <div class="checkout-page__form-ready-body-text address-4"><?= sprintf('%s, %s, %s', $checkoutBillingForm->city, $checkoutBillingForm->zip, $checkoutBillingForm->getCountryName()); ?></div>
        </div>

    <?php endif; ?>
</div>
