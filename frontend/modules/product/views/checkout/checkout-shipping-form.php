<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\MaskedInput;
use frontend\widgets\bootstrap4\ActiveForm;

/** @var \frontend\modules\core\components\View $this */
/** @var \frontend\widgets\bootstrap4\ActiveForm $form */
/** @var \frontend\modules\product\models\forms\CheckoutShippingForm $checkoutShippingForm */
/** @var bool $isAuthUser */

?>

<?php $form = ActiveForm::begin([
    'id' => 'order-shipping-form',
    'action' => ['/product/checkout/save-shipping-part'],
    'options' => [
        'class' => 'grey-form',
        'name' => 'paymentform',
    ],
    'enableAjaxValidation' => true,
    'validationUrl' => ['/product/checkout/ajax-validate-shipping-part'],
    'enableClientValidation' => false,
]); ?>

    <?php if ($isAuthUser) : ?>

        <div class="form-group checkout-page__address-input">
            <div class="checkout-page__select-adress-wrapper">

                <?= Html::activeDropDownList($checkoutShippingForm, 'savedShippingAddressId', $checkoutShippingForm->getUserSavedAddresses(), [
                    'id' => 'checkout-page__select-home',
                    'class' => 'form-control checkout-page__select-home',
                    'prompt' => 'Adresse auswählen',
                    'data-url' => Url::toRoute(['/product/checkout/get-shipping-form-for-saved-address'])
                ]); ?>

                <div class="checkout-page__select-adress"><?= Yii::t('core', 'Select saved address'); ?></div>
            </div>
        </div>

    <?php endif; ?>

    <?= $form->field($checkoutShippingForm, 'title', [
        'inputOptions' => [
            'required' => true,
            'id' => 'checkout-page__select-who',
        ],
        'labelOptions' => [
            'class' => '',
        ],
    ])->dropDownList($checkoutShippingForm->getListUserTitles(), [
        'prompt' => '',
    ])->label(Yii::t('product', 'Title') . '*'); ?>

    <div class="checkout-page__name-group">

        <?= $form->field($checkoutShippingForm, 'name', [
            'inputOptions' => [
                'placeholder' => Yii::t('product', 'First Name'),
                'required' => true,
                'id' => 'checkout-page__first-name'
            ],
            'options' => [
                'class' => 'form-group checkout-page__first-name',
            ],
            'labelOptions' => [
                'class' => '',
            ],
        ])->label($checkoutShippingForm->getAttributeLabel('name') . '*'); ?>

        <?= $form->field($checkoutShippingForm, 'surname', [
            'inputOptions' => [
                'placeholder' => Yii::t('product', 'Last Name'),
                'required' => true,
                'id' => 'checkout-page__last-name'
            ],
            'options' => [
                'class' => 'form-group checkout-page__last-name',
            ],
            'labelOptions' => [
                'class' => '',
            ],
        ])->label($checkoutShippingForm->getAttributeLabel('surname') . '*'); ?>

    </div>

    <?= $form->field($checkoutShippingForm, 'company', [
        'inputOptions' => [
            'placeholder' => 'Infinity lashes',
        ],
        'labelOptions' => [
            'class' => '',
        ],
    ])->label(Yii::t('product', 'Company')); ?>

    <label for=""><?= $checkoutShippingForm->getAttributeLabel('savedShippingAddressId'); ?>*</label>

    <div class="checkout-page__add-remove-js">

        <?= $form->field($checkoutShippingForm, 'address1', [
            'labelOptions' => [
                'class' => false,
            ],
            'inputOptions' => [
                'placeholder' => 'Kaiserstr. 59',
                'id' => 'checkout-page__address-add-1',
                'class' => 'form-control',
                'required' => true,
            ],
            'options' => [
                'tag' => false,
            ],
        ])->label(false); ?>

        <?php if ($checkoutShippingForm->address2) : ?>

            <?= $form->field($checkoutShippingForm, 'address2', [
                'inputOptions' => [
                    'placeholder' => 'Kaiserstr. 59',
                    'id' => 'checkout-page__address-add-2',
                    'class' => 'form-control',
                    'required' => true,
                ],
                'options' => [
                    'tag' => false,
                ],
            ])->label(false); ?>

        <?php endif; ?>

        <div class="checkout-page__address-input-add-remove">
            <a class="checkout-page__add-address-line" data-model-name="<?= Html::getInputName($checkoutShippingForm, 'address2'); ?>" href="#" style="<?= ($checkoutShippingForm->address2) ? 'visibility: hidden' : 'visibility: visible'; ?>">+ <?= Yii::t('shipping', 'Add address line'); ?></a>
            <a style="<?= ($checkoutShippingForm->address2) ? 'visibility: visible' : 'visibility: hidden'; ?>" class="checkout-page__remove-address-line" href="#">- <?= Yii::t('shipping', 'Remove address line'); ?></a>
        </div>

    </div>

    <div class="checkout-page__city-zip">

        <?= $form->field($checkoutShippingForm, 'city', [
            'inputOptions' => [
                'placeholder' => 'E. g. Dortmund',
                'required' => true,
            ],
            'labelOptions' => [
                'class' => '',
            ],
        ])->label($checkoutShippingForm->getAttributeLabel('city') . '*'); ?>

        <?= $form->field($checkoutShippingForm, 'zip', [
            'labelOptions' => [
                'class' => false,
            ],
            'options' => [
                'class' => 'form-group checkout-page__zip-code',
            ]
        ])
/*        ->widget(MaskedInput::className(), [
            'mask' => '99999',
            'options' => [
                'placeholder' => '44135',
                'id' => 'checkout-page__zip-add',
                'class' => 'form-control',
                'required' => true,
            ],
        ])*/
        ->label($checkoutShippingForm->getAttributeLabel('zip') . '*'); ?>

    </div>

    <?= $form->field($checkoutShippingForm, 'countryId', [
        'labelOptions' => [
            'class' => false,
        ],
        'inputOptions' => [
            'id' => 'checkout-page__country-add',
            'class' => 'form-control',
            'required' => true,
            'data-url' => Url::toRoute(['change-country']),
        ],
    ])->dropDownList($checkoutShippingForm->getListCountriesForDropDown()); ?>

    <div class="checkout-page__e-phone">

        <?= $form->field($checkoutShippingForm, 'email', [
            'inputOptions' => [
                'placeholder' => 'E. g. name@mail.com',
                'required' => true,
                'id' => 'checkout-page__email-add',
            ],
            'labelOptions' => [
                'class' => '',
            ],
        ])->label($checkoutShippingForm->getAttributeLabel('email') . '*'); ?>

        <?= $form->field($checkoutShippingForm, 'phone', [

            'labelOptions' => [
                'class' => '',
            ],
        ])/*->widget(MaskedInput::className(), [
            'mask' => $checkoutShippingForm->getPhoneMask(),
            'options' => [
                'placeholder' => Yii::t('product', 'Phone'),
                'id' => 'checkout-page__phone-add',
                'class' => 'form-control',
                'required' => true,
            ],
        ])*/
        ->label($checkoutShippingForm->getAttributeLabel('phone') . '*'); ?>

    </div>
    <div class="form-check">
        <div class="grey-form__forgot-pass">

            <?= $form->field($checkoutShippingForm, 'useShippingAddressForBilling', [
                'options' => [
                    'class' => 'use-this-address custom-control custom-checkbox',
                ],
                'checkboxTemplate' => '{input}{label}',
            ])->checkbox(
                ['class' => 'custom-control-input',],
                ['class' => 'custom-control-label',]
            ); ?>

            <?php if ($isAuthUser) : ?>

                <?= $form->field($checkoutShippingForm, 'saveAddress', [
                    'options' => [
                        'class' => 'save-address custom-control custom-checkbox',
                    ],
                    'checkboxTemplate' => '{input}{label}',
                ])->checkbox(
                    ['class' => 'custom-control-input',],
                    ['class' => 'custom-control-label',]
                ); ?>

            <?php endif; ?>

        </div>
    </div>

    <div id="shipping-methods">

        <?php if ($checkoutShippingForm->isCountryGerman()) : ?>

            <?= $this->render('checkout-shipping-methods-block-germany', compact('form', 'checkoutShippingForm')); ?>

        <?php else : ?>

            <?= $this->render('checkout-shipping-methods-block-other-countries', compact('form', 'checkoutShippingForm')); ?>

        <?php endif; ?>

    </div>

    <div class="row justify-content-end">
        <button class="btn btn-primary checkout-page__first-form-submit" type="submit"><?= Yii::t('product', 'Apply') ?></button>
    </div>


<?php ActiveForm::end(); ?>

<div id="paymentform"></div>

<script src="https://secure.pay1.de/client-api/js/v1/payone_hosted_min.js"></script>