<?php

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\modules\core\models\Setting;
use frontend\modules\product\models\ProductVariation;

?>

<?= $form->field($checkoutShippingForm, 'shippingMethod', [
    'labelOptions' => [
        'class' => 'checkout-page__select-speed',
    ],
])->radioList($checkoutShippingForm->getListShippingMethodsOtherCountries(), [
    'item' => function ($index, $label, $name, $checked, $value) use ($checkoutShippingForm) {
          $wrapperClass = 'checkout-page__first-form-radio-wrapper-standart';
          $shippingPriceWrapperClass = 'checkout-page__radio-standart-price';
          $shippingDateWrapperClass = 'checkout-page__radio-standart-dates';
          $date = Yii::t('shipping', 'Fast delivery about 1-3 working days');
          $inputId = 'checkout-page__radio-standart';

          $item = Html::beginTag('div', [
              'class' => $wrapperClass,
          ]);

          $radio = Html::radio($name, $checked, [
              'value' => $value,
              'class' => 'custom-control-input',
              'id' => $inputId,
          ]);

          $label = "<label for='{$inputId}' class='custom-control-label'>{$label}</label>";

          $item .= "<div class='custom-control custom-radio custom-control-inline'>{$radio}{$label}</div>";

          $item .= "<div class='{$shippingDateWrapperClass}'>{$date}</div>";
          $item .= "<div class='{$shippingPriceWrapperClass}'>".Setting::value('product.shop.currency') . ProductVariation::viewFormatPrice($checkoutShippingForm->getShippingStandartMethodPrice()) . "</div>";

          $item .= Html::endTag('div');

          return $item;
    },
    'class' => 'checkout-page__first-form-radio',
    'id' => 'shipping-method-js',
    'data-url' => Url::toRoute(['/product/checkout/change-shipping-method']),
]); ?>
