<?php

namespace frontend\modules\product\controllers;

use frontend\modules\product\models\Order;
use kartik\mpdf\Pdf;
use Yii;
use frontend\modules\user\models\User;
use frontend\modules\core\controllers\FrontController;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

class OrderController extends FrontController
{
    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
               'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
        ];
    }

    /**
     * Get orders tab in account.
     *
     * @return mixed
     */
    public function actionGetIndexOrderPage()
    {
        $userIdentityId = Yii::$app->user->identity->id;
        $user = User::find()
            ->with('profile')
            ->where(['id' => $userIdentityId])
            ->one();

        $orders = Order::getUserOrders($userIdentityId);

        if (!$orders) {
            return $this->renderPartial('empty-user-orders');
        }

        return $this->renderPartial('user-orders', compact('orders'));
    }

    /**
     * Display user account page.
     *
     * @return mixed
     */
    public function actionGeneratePdf($id)
    {
        $userIdentityId = Yii::$app->user->identity->id;

        $modelOrder = Order::find()
            ->with('orderProducts', 'orderProducts.productVariation', 'orderShippingInformation', 'orderBillingInformation')
            ->where([
                'id' => (int) $id,
                'user_id' => $userIdentityId,
            ])
            ->one();

        if ($modelOrder === null) {
            throw new NotFoundHttpException("Not order found", 1);
        }

        $content = $this->renderPartial('generate-pdf',compact('modelOrder'));

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'format' => Pdf::FORMAT_A4,
            'cssFile' => Yii::getAlias('@frontend/web/css/pdf.css'),
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,
            'options' => ['title' => 'Order Report'],
            'methods' => [
                'SetFooter' => $this->renderPartial('pdf-footer'),
            ],
        ]);

        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;

        return $pdf->render();
    }

}
