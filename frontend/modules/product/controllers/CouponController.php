<?php

namespace frontend\modules\product\controllers;

use Yii;
use yii\web\Controller;
use frontend\modules\product\models\forms\CouponForm;
use frontend\widgets\bootstrap4\ActiveForm;
use yii\web\Response;
use frontend\modules\product\helpers\CartHelper;
use frontend\modules\product\models\OrderShippingInformation;
use frontend\modules\core\models\Setting;

/**
 * Coupon controller.
 */
class CouponController extends Controller
{
    /**
     * Apply code coupon to total price.
     *
     * @return JSON response
     */
    public function actionApplyCoupon()
    {
        $couponForm = new CouponForm();

        Yii::$app->response->format = Response::FORMAT_JSON;

        $response = [
            'status' => false,
            'totalPrice' => 0,
            'html' => '',
        ];

        $totalPrice = CartHelper::getTotalPriceWithCoupon();

        $cookies = Yii::$app->request->cookies;

        if (!CartHelper::isEmptyCart()) {

            if ($couponForm->load(Yii::$app->request->post()) && $couponForm->validate()) {
                $response[ 'status' ] = true;

                $totalPrice = CartHelper::addCoupon($couponForm->getCoupon());
            } else {
                CartHelper::removeCoupon();

                $totalPrice = CartHelper::getTotalPrice();
            }
        }

        $freeShippingSum = Setting::getFreeShippingSettingValue();
        $shippingPrice = (is_null($freeShippingSum) || $freeShippingSum > $totalPrice) ? OrderShippingInformation::SHIPPING_PRICE_STANDART_METHOD : 0;

        $form = new ActiveForm();

        $response[ 'totalPrice' ] = $totalPrice;
        $response[ 'totalPriceNotDiscount'] = CartHelper::getTotalPrice();
        $response[ 'html' ] = $this->renderPartial('/cart/coupon-form', compact('couponForm', 'form'));
        $response[ 'shippingPrice' ] = $shippingPrice;
        $response[ 'tax' ] = (float)$response[ 'totalPrice' ] / 100 * OrderShippingInformation::TAX_PERCENT;
        $response[ 'discountPrice' ] =  $response[ 'totalPriceNotDiscount'] - $response[ 'totalPrice' ];

        return $response;
    }

    public function actionRemoveCoupon()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $response = [
            'status' => false,
            'totalPrice' => 0,
            'html' => '',
        ];

        $totalPrice = CartHelper::getTotalPriceWithCoupon();

        $cookies = Yii::$app->request->cookies;

        if (!CartHelper::isEmptyCart()) {
            CartHelper::removeCoupon();
            $totalPrice = CartHelper::getTotalPrice();
        }

        $freeShippingSum = Setting::getFreeShippingSettingValue();
        $shippingPrice = (is_null($freeShippingSum) || $freeShippingSum > $totalPrice) ? OrderShippingInformation::SHIPPING_PRICE_STANDART_METHOD : 0;

        $response[ 'totalPrice' ] = $totalPrice;
        $response[ 'totalPriceNotDiscount'] = CartHelper::getTotalPrice();
        $response[ 'shippingPrice' ] = $shippingPrice;
        $response[ 'tax' ] = (float)$response[ 'totalPrice' ] / 100 * OrderShippingInformation::TAX_PERCENT;
        $response[ 'discountPrice' ] =  $response[ 'totalPriceNotDiscount'] - $response[ 'totalPrice' ];

        return $response;
    }
}
