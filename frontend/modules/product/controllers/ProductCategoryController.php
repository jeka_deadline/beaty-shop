<?php

namespace frontend\modules\product\controllers;

use backend\modules\product\models\Product;
use frontend\modules\core\controllers\FrontController;
use frontend\modules\product\models\forms\ProductFilterForm;
use frontend\modules\product\models\ProductCategory;
use Yii;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;
use frontend\modules\product\models\ProductVariation;
use frontend\modules\core\models\CorePage;

/**
 * Frontend product category controller.
 */
class ProductCategoryController extends FrontController
{
    /**
     * Display products in category.
     *
     * @throws NotFoundHttpException If nesting slugs violated
     * @param string $slug Slug category
     * @return mixed
     */
    public function actionIndex($slug)
    {
        if (!ProductCategory::checkNestingSlugs($slug)) {
            throw new NotFoundHttpException('Product category not found');
        }

        $category = $this->findCategoryBySlug(ProductCategory::getLastCategorySlug($slug));

        $this->setMetaTitle($category->meta_title);
        $this->setMetaKeywords($category->meta_keywords);
        $this->setMetaDescription($category->meta_description);

        $categoryIds = ProductCategory::getChildrenIds($category);

        $dataProviderVariations = new ActiveDataProvider([
            'query' => ProductFilterForm::filter(
                  ProductVariation::find()
                      ->alias('default_variations')
                      ->with('preview', 'price', 'promotion')
                      ->leftJoin(Product::tableName().' product', 'product.default_variation_id = default_variations.id')
                      ->leftJoin(ProductVariation::tableName().' variations', 'variations.product_id = product.id')
                      ->joinWith('product.productRelationCategories prodRelCat')
                      ->where(['variations.active' => 1, 'product.active' => 1, 'prodRelCat.product_category_id' => $categoryIds])
                      ->groupBy('default_variations.product_id'),
                  $categoryIds
            ),
            'pagination' => [
                'pageSize' => 28,
                'validatePage' => false
            ],
        ]);
        if (Yii::$app->request->isAjax) {
            return $this->renderPartial('index-ajax', [
                'category' => $category,
                'dataProviderVariations' => $dataProviderVariations,
                'categorySlugs' => $slug,
            ]);
        } else {
            return $this->render('index', [
                'category' => $category,
                'dataProviderVariations' => $dataProviderVariations,
                'categorySlugs' => $slug,
                'categoryIds' => $categoryIds
            ]);
        }
    }

    public function actionShop()
    {
        $modelCategories = $this->findCategoryByRoot();

        $categoryIds = [];
        foreach ($modelCategories as $category) {
            $categoryIds = array_merge($categoryIds, ProductCategory::getChildrenIds($category));
        }

        $dataProviderVariations = new ActiveDataProvider([
            'query' => ProductFilterForm::filter(
                ProductVariation::find()
                    ->alias('default_variations')
                    ->with('preview', 'price', 'promotion')
                    ->leftJoin(Product::tableName().' product', 'product.default_variation_id = default_variations.id')
                    ->leftJoin(ProductVariation::tableName().' variations', 'variations.product_id = product.id')
                    ->joinWith('product.productRelationCategories prodRelCat')
                    ->where(['variations.active' => 1, 'product.active' => 1, 'prodRelCat.product_category_id' => $categoryIds])
                    ->groupBy('default_variations.product_id'),
                $categoryIds
            ),
            'pagination' => [
                'pageSize' => 28,
                'validatePage' => false
            ],
        ]);
        if (Yii::$app->request->isAjax) {
            return $this->renderPartial('index-ajax', [
                'dataProviderVariations' => $dataProviderVariations,
                'categorySlugs' => 'shop',
            ]);
        }

        if ($page = CorePage::findShopPage()) {
            $this->setMetaTitle($page->meta_title);
            $this->setMetaDescription($page->meta_description);
            $this->setMetaKeywords($page->meta_keywords);
        }

        return $this->render('shop', [
            'dataProviderVariations' => $dataProviderVariations,
            'categorySlugs' => 'shop',
            'categoryIds' => $categoryIds
        ]);
    }

    /**
     * Show new arrivals products.
     *
     * @return mixed
     */
    public function actionNews()
    {
        $modelCategories = $this->findCategoryByRoot();

        $categoryIds = [];
        foreach ($modelCategories as $category) {
            $categoryIds = array_merge($categoryIds, ProductCategory::getChildrenIds($category));
        }

        $dataProviderVariations = new ActiveDataProvider([
            'query' => ProductFilterForm::filter(
                ProductVariation::find()
                    ->alias('default_variations')
                    ->with('preview', 'price', 'promotion')
                    ->leftJoin(Product::tableName().' product', 'product.default_variation_id = default_variations.id')
                    ->leftJoin(ProductVariation::tableName().' variations', 'variations.product_id = product.id')
                    ->joinWith('product.productRelationCategories prodRelCat')
                    ->where(['variations.active' => 1, 'product.active' => 1, 'prodRelCat.product_category_id' => $categoryIds])
                    ->groupBy('default_variations.product_id')
                    ->orderBy('created_at', SORT_DESC),
                $categoryIds
            ),
            'pagination' => [
                'pageSize' => 28,
                'validatePage' => false
            ],
        ]);

        if (Yii::$app->request->isAjax) {

            return $this->renderPartial('index-ajax', [
                'dataProviderVariations' => $dataProviderVariations,
                'categorySlugs' => 'shop',
            ]);
        }

        if ($page = CorePage::findShopNewPage()) {
            $this->setMetaTitle($page->meta_title);
            $this->setMetaDescription($page->meta_description);
            $this->setMetaKeywords($page->meta_keywords);
        }


        return $this->render('shop', [
            'dataProviderVariations' => $dataProviderVariations,
            'categorySlugs' => 'shop',
            'categoryIds' => $categoryIds
        ]);
    }

    /**
     * Find active category by slug.
     *
     * @throws NotFoundHttpException If not found category
     * @param string $slug Category slug
     * @return \frontend\modules\product\models\ProductCategory
     */
    private function findCategoryBySlug($slug)
    {
        $category = ProductCategory::find()
            ->where(['slug' => $slug])
            ->active()
            ->one();

        if (!$category) {
            throw new NotFoundHttpException('Product category not found');
        }

        return $category;
    }

    private function findCategoryByRoot()
    {
        $categorys = ProductCategory::find()
            ->where(['parent_id' => 0])
            ->active()
            ->all();

        if (!$categorys) {
            throw new NotFoundHttpException('Product category not found');
        }

        return $categorys;
    }
}
