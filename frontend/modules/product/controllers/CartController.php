<?php

namespace frontend\modules\product\controllers;

use Yii;
use frontend\modules\core\controllers\FrontController;
use frontend\modules\product\models\ProductVariation;
use yii\data\ActiveDataProvider;
use frontend\modules\product\models\forms\ProductCartForm;
use yii\helpers\VarDumper;
use yii\web\Response;
use yii\filters\VerbFilter;
use frontend\modules\product\models\forms\CouponForm;
use frontend\modules\product\helpers\CartHelper;
use yii\web\NotFoundHttpException;
use frontend\modules\product\models\OrderShippingInformation;
use frontend\modules\core\models\Setting;
use frontend\modules\product\models\ProductVariationPack;
use frontend\modules\core\models\CorePage;

/**
 * Cart controller.
 */
class CartController extends FrontController
{
    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'remove-product-from-cart' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Show user cart.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $country = Yii::$app->userCountry->getCountry();

        if (CartHelper::hasCoupon()) {
            CartHelper::removeCoupon();

            return $this->refresh();
        }

        $modalText = Yii::$app->session->get(Setting::MODAL_SESSION_KEY, null);

        if (Yii::$app->session->has(Setting::MODAL_SESSION_KEY)) {
            Yii::$app->session->remove(Setting::MODAL_SESSION_KEY);
        }

        $totalPrice = CartHelper::getTotalPriceWithCoupon();
        $totalPriceNotDiscount = CartHelper::getTotalPrice();
        $freeShipping = CartHelper::getInfoFreeShipping($totalPrice);
        $cart = CartHelper::getCart();
        $freeShippingSum = Yii::$app->userCountry->getSumFreeShippingByCountry();
        $shippingPrice = (is_null($freeShippingSum) || $freeShippingSum > $totalPrice) ? Yii::$app->userCountry->getSumStandartShippingByCountry() : 0;
        $tax = (float)$totalPrice / 100 * OrderShippingInformation::TAX_PERCENT;
        $discountPrice = $totalPriceNotDiscount-$totalPrice;
        $discountPrice = ($discountPrice > 0.01)?$discountPrice:0;

        $productVariations = ProductVariation::getActiveProductVariationsByIds(CartHelper::getProductIds());

        $couponForm = new CouponForm();
        $countItems = CartHelper::getCountProductsInCart();

        if (CartHelper::hasCoupon()) {
            $coupon = CartHelper::getCoupon();
            $couponForm->code = $coupon->code;
        }

        if (!$countItems) {

            if ($page = CorePage::findCartPage()) {
                $this->setMetaTitle($page->meta_title);
                $this->setMetaDescription($page->meta_description);
                $this->setMetaKeywords($page->meta_keywords);
            }

            return $this->render('empty-cart', compact('modalText'));
        }

        if ($page = CorePage::findCartPage()) {
            $this->setMetaTitle($page->meta_title);
            $this->setMetaDescription($page->meta_description);
            $this->setMetaKeywords($page->meta_keywords);
        }

        return $this->render('index', compact(
            'productVariations',
            'cart',
            'totalPrice',
            'totalPriceNotDiscount',
            'couponForm',
            'shippingPrice',
            'countItems',
            'freeShipping',
            'tax',
            'discountPrice',
            'modalText'
        ));
    }

    /**
     * Add product to cart.
     *
     * @param int $id Product variation id
     * @return array JSON response
     */
    public function actionAddProductToCart($id)
    {
        $response = [
            'variationId' => $id,
            'status' => false,
            'totalPrice' => CartHelper::getTotalPriceWithCoupon(),
            'popupFreeShipping' => null,
            'popupItemTemplate' => null,
        ];

        Yii::$app->getResponse()->format = Response::FORMAT_JSON;

        $model = ProductVariation::find()
            ->where(['id' => $id])
            ->with('price', 'promotion')
            ->active()
            ->one();

        if (!$model) {
            return $response;
        }

        $formAddToCart = new ProductCartForm($id, ['scenario' => ProductCartForm::SCENARIO_ADD]);

        $cartItem = CartHelper::getProductById($id);

        $formAddToCart->load(Yii::$app->getRequest()->post());

        if (!is_null($cartItem)) {
            $formAddToCart->count += $cartItem->count;
        }

        if (!$formAddToCart->validate()) {
            return $response;
        }

        $cartDTO = $model->generateStructureToCart($id, $formAddToCart->count);

        CartHelper::addToCart($cartDTO);

        if (CartHelper::isEmptyCart()) {
            $totalPrice = $cartDTO->totalPrice;
        } else {
            $totalPrice = CartHelper::getTotalPrice();
        }

        $cart = CartHelper::getCart();

        $response[ 'status' ] = true;
        $response[ 'totalPrice' ] = $totalPrice;
        $response[ 'popupFreeShipping' ] = $this->renderPartial('free-shipping-popup', [
            'infoFreeShipping' => CartHelper::getInfoFreeShipping($totalPrice)
        ]);
        $response[ 'popupItemTemplate' ] = $this->renderPartial('item-product-popup', [
            'cart'             => $cart,
            'productVariation' => $model,
        ]);

        $freeShippingSum = Yii::$app->userCountry->getSumFreeShippingByCountry();
        $response[ 'shippingPrice' ] = (is_null($freeShippingSum) || $freeShippingSum > $response[ 'totalPrice' ]) ? Yii::$app->userCountry->getSumStandartShippingByCountry() : 0;

        return $response;
    }

    /**
     * Increment product count in cart.
     *
     * @return array JSON response
     */
    public function actionIncrementProductCount()
    {
        $id = Yii::$app->getRequest()->post('id');

        $response = [
            'status' => false,
            'count' => 0,
            'totalPrice' => CartHelper::getTotalPriceWithCoupon(),
        ];

        Yii::$app->getResponse()->format = Response::FORMAT_JSON;

        try {
            $productVariation = $this->getProductVariationByIdWithInventory($id);
        } catch (NotFoundException $e) {
            return $response;
        }

        $productInCart = CartHelper::getProductById($id);

        if (!$productInCart || !$this->validateWithPack($productVariation, $productInCart)) {
            return $response;
        }

        if ($productVariation->isProductSet()) {
            $inventorieCount = $productVariation->product->countSet;
        } else {
            $inventorieCount = $productVariation->inventorie->count;
        }

        if ($productInCart->count < $inventorieCount) {
            CartHelper::incrementProductCount($id);

            $response[ 'count' ] = $productInCart->count;
            $response[ 'status' ] = true;
            $response[ 'totalPrice' ] = ProductVariation::viewFormatPrice(CartHelper::getTotalPriceWithCoupon());
            $response[ 'totalPriceNotDiscount'] = ProductVariation::viewFormatPrice(CartHelper::getTotalPrice());
            $infoFreeShipping = CartHelper::getInfoFreeShipping($response[ 'totalPrice' ]);
            $response[ 'freeShipping' ] = $infoFreeShipping;
            $response[ 'tax' ] = ProductVariation::viewFormatPrice((float)$response[ 'totalPrice' ] / 100 * OrderShippingInformation::TAX_PERCENT);
            $response[ 'discountPrice' ] =  ProductVariation::viewFormatPrice($response[ 'totalPriceNotDiscount'] - $response[ 'totalPrice' ]);

            $response[ 'popupFreeShipping' ] = $this->renderPartial('free-shipping-popup', [
                'infoFreeShipping' => $infoFreeShipping
            ]);

            $cart = CartHelper::getCart();
            $response[ 'popupItemTemplate' ] = $this->renderPartial('item-product-popup', [
                'cart'             => $cart,
                'productVariation' => $productVariation,
            ]);

            $freeShippingSum = Yii::$app->userCountry->getSumFreeShippingByCountry();
            $response[ 'shippingPrice' ] = ProductVariation::viewFormatPrice(
                (is_null($freeShippingSum) || $freeShippingSum > $response[ 'totalPrice' ]) ? Yii::$app->userCountry->getSumStandartShippingByCountry() : 0
            );
        }

        return $response;
    }

    /**
     * Decrement product count in cart.
     *
     * @return array JSON response
     */
    public function actionDecrementProductCount()
    {
        $id = Yii::$app->getRequest()->post('id');

        $response = [
            'status' => false,
            'count' => 0,
            'totalPrice' => CartHelper::getTotalPriceWithCoupon(),
        ];

        Yii::$app->getResponse()->format = Response::FORMAT_JSON;

        try {
            $productVariation = $this->getProductVariationByIdWithInventory($id);
        } catch (NotFoundException $e) {
            return $response;
        }

        $productInCart = CartHelper::getProductById($id);

        if (!$productInCart) {
            return $response;
        }

        if ($productInCart->count > 1) {
            CartHelper::decrementProductCount($id);

            $response[ 'count' ] = $productInCart->count;
            $response[ 'status' ] = true;
            $response[ 'totalPrice' ] = ProductVariation::viewFormatPrice(CartHelper::getTotalPriceWithCoupon());
            $response[ 'totalPriceNotDiscount'] = ProductVariation::viewFormatPrice(CartHelper::getTotalPrice());
            $infoFreeShipping = CartHelper::getInfoFreeShipping($response[ 'totalPrice' ]);
            $response[ 'freeShipping' ] = $infoFreeShipping;
            $response[ 'tax' ] = ProductVariation::viewFormatPrice((float)$response[ 'totalPrice' ] / 100 * OrderShippingInformation::TAX_PERCENT);
            $response[ 'discountPrice' ] =  ProductVariation::viewFormatPrice($response[ 'totalPriceNotDiscount'] - $response[ 'totalPrice' ]);

            $response[ 'popupFreeShipping' ] = $this->renderPartial('free-shipping-popup', [
                'infoFreeShipping' => $infoFreeShipping
            ]);

            $cart = CartHelper::getCart();
            $response[ 'popupItemTemplate' ] = $this->renderPartial('item-product-popup', [
                'cart'             => $cart,
                'productVariation' => $productVariation,
            ]);

            $freeShippingSum = Yii::$app->userCountry->getSumFreeShippingByCountry();
            $response[ 'shippingPrice' ] = ProductVariation::viewFormatPrice(
                (is_null($freeShippingSum) || $freeShippingSum > $response[ 'totalPrice' ]) ? Yii::$app->userCountry->getSumStandartShippingByCountry() : 0
            );
        }

        return $response;
    }

    /**
     * Remove product from cart.
     *
     * @return array JSON response
     */
    public function actionRemoveProductFromCart()
    {
        $response = [
            'status' => false,
            'totalPrice' => CartHelper::getTotalPriceWithCoupon(),
        ];

        Yii::$app->getResponse()->format = Response::FORMAT_JSON;

        $id = Yii::$app->getRequest()->post('id');

        $model = ProductVariation::find()
            ->where(['id' => $id])
            ->active()
            ->one();

        if (!$model) {
            return $response;
        }

        $response[ 'status' ] = true;
        CartHelper::removeProductFromCart($id);
        $response[ 'totalPrice' ] = ProductVariation::viewFormatPrice(CartHelper::getTotalPriceWithCoupon());
        $response[ 'totalPriceNotDiscount'] = ProductVariation::viewFormatPrice(CartHelper::getTotalPrice());
        $response[ 'freeShipping' ] = CartHelper::getInfoFreeShipping($response[ 'totalPrice' ]);
        $response[ 'popupFreeShipping' ] = $this->renderPartial('free-shipping-popup', [
            'infoFreeShipping' => $response[ 'freeShipping' ]
        ]);
        $response[ 'tax' ] = ProductVariation::viewFormatPrice((float)$response[ 'totalPrice' ] / 100 * OrderShippingInformation::TAX_PERCENT);
        $response[ 'discountPrice' ] =  ProductVariation::viewFormatPrice($response[ 'totalPriceNotDiscount'] - $response[ 'totalPrice' ]);

        $response[ 'popupFreeShipping' ] = $this->renderPartial('free-shipping-popup', [
            'infoFreeShipping' => CartHelper::getInfoFreeShipping($response[ 'totalPrice' ])
        ]);

        $cart = CartHelper::getCart();
        $response[ 'popupItemTemplate' ] = '';


        $freeShippingSum = Yii::$app->userCountry->getSumFreeShippingByCountry();
        $response[ 'shippingPrice' ] = ProductVariation::viewFormatPrice(
            (is_null($freeShippingSum) || $freeShippingSum > $response[ 'totalPrice' ]) ? Yii::$app->userCountry->getSumStandartShippingByCountry() : 0
        );

        $response[ 'emptyCart' ] = $this->renderPartial('empty-cart');

        return $response;
    }

    /**
     * {@inheritdoc}
     *
     * @param yii\base\Action $action {@inheritdoc}
     * @return bool
     */
    public function beforeAction($action)
    {
        if ($action->id === 'index') {
            $response = CartHelper::isChangeProducts();

            if ($response->result === true) {
                Yii::$app->session->set(Setting::MODAL_SESSION_KEY, $response->text);
            }
        }

        return parent::beforeAction($action);
    }

    /**
     * Get product variation with inventory by id.
     *
     * @access private
     * @param int $id Product variation id.
     * @return frontend\modules\product\models\ProductVariation
     */
    private function getProductVariationByIdWithInventory($id)
    {
        $model = ProductVariation::find()
            ->where(['id' => $id])
            ->with('inventorie')
            ->active()
            ->one();

        if (!$model || !$model->inventorie) {
            new NotFoundHttpException('Product variation not found');
        }

        return $model;
    }

    /**
     * Validate product with packs.
     *
     * @access private
     * @param frontend\modules\product\models\ProductVariation Product variation model.
     * @param frontend\modules\product\helpers\CheckoutDTO Product in cart.
     * @return bool
     */
    private function validateWithPack($productVariation, $productInCart)
    {
        $cart = CartHelper::getCart();

        $variations = ProductVariation::find()
            ->where(['id' => CartHelper::getProductIds()])
            ->andWhere(['<>', 'id', $productVariation->id])
            ->with('product', 'inventorie')
            ->indexBy('id')
            ->all();

        if (!$productVariation->isProductAnPack()) {
            $currentCount = $productInCart->count + 1;

            if ($productVariation->isProductSet()) {
                $maxCount = $productVariation->product->countSet;
            } else {
                $maxCount = $productVariation->inventorie->count;
            }

            $mainPackProductVariation = $productVariation;

        } else {
            $mainPackProductVariation = $productVariation->product->mainPackProduct->getProductVariations()->with('inventorie')->one();

            if (!$mainPackProductVariation || !$mainPackProductVariation->inventorie) {
                return false;
            }

            $maxCount = $mainPackProductVariation->inventorie->count;

            $currentCount = ($productInCart->count + 1) * $productVariation->inventorie->count;

            if (isset($cart[ $mainPackProductVariation->id ])) {
                $currentCount += $cart[ $mainPackProductVariation->id ]->count;
            }
        }

        foreach ($variations as $itemVariation) {
            if (!$itemVariation->isProductAnPack()) {
                continue;
            }

            $mainPackProductVariationItem = $itemVariation->product->mainPackProduct->getProductVariations()->with('inventorie')->one();

            if ($mainPackProductVariationItem->id !== $mainPackProductVariation->id || !$mainPackProductVariationItem || !$mainPackProductVariationItem->inventorie) {
                continue;
            }

            $currentCount += $cart[ $itemVariation->id ]->count * $itemVariation->inventorie->count;
        }

        if ($currentCount > $maxCount) {
            return false;
        }

        return true;
    }
}
