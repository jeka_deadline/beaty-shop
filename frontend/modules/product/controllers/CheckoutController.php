<?php

namespace frontend\modules\product\controllers;

use frontend\modules\product\helpers\Klarna;
use Yii;
use frontend\modules\core\controllers\FrontController;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use frontend\widgets\bootstrap4\ActiveForm;
use frontend\modules\product\models\forms\CheckoutShippingForm;
use frontend\modules\product\models\forms\CheckoutPaymentForm;
use frontend\modules\product\models\forms\CheckoutBillingForm;
use frontend\modules\product\models\forms\CheckoutOrderReviewForm;
use frontend\modules\user\models\UserShippingAddress;
use frontend\modules\user\models\UserPaymentCard;
use frontend\modules\product\models\OrderShippingInformation;
use frontend\modules\product\helpers\CartHelper;
use frontend\modules\product\models\Order;
use frontend\modules\product\helpers\CheckoutDTO;
use frontend\modules\product\helpers\Paypal;
use frontend\modules\product\helpers\Payone;
use frontend\modules\product\models\ProductVariation;
use yii\helpers\Url;
use Exception;
use yii\web\MethodNotAllowedHttpException;
use frontend\modules\core\models\Setting;
use Klarna\Rest\Transport\Exception\ConnectorException;
use frontend\modules\core\models\CorePage;
use frontend\modules\geo\models\Country;

/**
 * Frontend controller for checkout operations.
 */
class CheckoutController extends FrontController
{
    private $sessionKeyForShippingPart = 'checkout-shipping-part';

    private $sessionKeyForBillingPart = 'checkout-billing-part';

    private $sessionKeyForPaymentPart = 'checkout-payment-part';

    private $sessionKeySuccessOrder = 'success-order-id';

    private $sessionKeyPseudocardpan = 'pseudocardpan';

    private $sessionKeyTruncatedcardpan = 'truncatedcardpan';

    /**
     * Display page for checkout information.
     *
     * @throws \yii\web\NotFoundHttpException If user cart is empty
     * @return mixed
     */
    public function actionIndex()
    {
        if (!CartHelper::isValidCart()) {
            return $this->redirect(['/product/cart/index']);
        }

        $this->layout = '/checkout';

        $session = Yii::$app->session;

        if ($session->has($this->sessionKeyForShippingPart)) {
            $session->remove($this->sessionKeyForShippingPart);
        }

        if ($session->has($this->sessionKeyForBillingPart)) {
            $session->remove($this->sessionKeyForBillingPart);
        }

        if ($session->has($this->sessionKeyForPaymentPart)) {
            $session->remove($this->sessionKeyForPaymentPart);
        }

        // if user cart empty
        if (CartHelper::isEmptyCart()) {
            throw new NotFoundHttpException("The requested page does not exist.");
        }

        $userId = (!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id : null;
        $isAuthUser = ($userId) ? true : false;

        // create forms for page checkout

        /**
         * Checkout shipping form.
         *
         * @var \frontend\modules\product\models\forms\CheckoutShippingForm $checkoutShippingForm
         */
        $checkoutShippingForm = new CheckoutShippingForm($userId);
        $checkoutShippingForm->useShippingAddressForBilling = true;
        $checkoutShippingForm->shippingMethod = OrderShippingInformation::SHIPPING_STANDART_METHOD;

        $totalPrice = CartHelper::getTotalPriceWithCoupon();
        $tax = $totalPrice / 100 * OrderShippingInformation::TAX_PERCENT;
        $cart = CartHelper::getCart();
        $shippingPrice = $checkoutShippingForm->getShippingStandartMethodPrice();

        $productVariations = ProductVariation::getActiveProductVariationsByIds(CartHelper::getProductIds());

        $totalPriceNotDiscount = ProductVariation::viewFormatPrice(CartHelper::getTotalPrice());
        $discountPrice = $totalPriceNotDiscount-$totalPrice;
        $discountPrice = ($discountPrice > 0.01)?$discountPrice:0;

        if ($page = CorePage::findCheckoutPage()) {
            $this->setMetaTitle($page->meta_title);
            $this->setMetaDescription($page->meta_description);
            $this->setMetaKeywords($page->meta_keywords);
        }

        return $this->render('index', [
            'checkoutShippingForm' => $checkoutShippingForm,
            'isAuthUser' => $isAuthUser,
            'totalPrice' => ProductVariation::viewFormatPrice($totalPrice),
            'totalPriceNotDiscount' => $totalPriceNotDiscount,
            'discountPrice' => $discountPrice,
            'tax' => ProductVariation::viewFormatPrice((float)$tax),
            'cart' => $cart,
            'productVariations' => $productVariations,
            'shippingPrice' => ProductVariation::viewFormatPrice($shippingPrice),
        ]);
    }

    /**
     * Save checkout shipping form in session.
     *
     * @return
     */
    public function actionSaveShippingPart()
    {
        $userId = (!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id : null;

        $checkoutShippingForm = new CheckoutShippingForm($userId);

        $response = [
            'status' => false,
            'html' => null,
            'billing_and_payment_form' => null,
        ];

        Yii::$app->response->format = Response::FORMAT_JSON;

        $oldCheckoutShippingFormAttributes = Yii::$app->session->get($this->sessionKeyForShippingPart, null);

        if ($checkoutShippingForm->load(Yii::$app->request->post()) && $checkoutShippingForm->validate()) {
            Yii::$app->session->set($this->sessionKeyForShippingPart, $checkoutShippingForm->attributes);
            $checkoutBillingForm = new CheckoutBillingForm();
            $checkoutPaymentForm = new CheckoutPaymentForm();
            $checkoutPaymentForm->type = Order::CREDIT_METHOD;

            $response[ 'status' ] = true;
            $response[ 'html' ] = $this->renderPartial('saved-shipping-part', compact('checkoutShippingForm'));

            if (Yii::$app->session->has($this->sessionKeyForPaymentPart)) {
                $checkoutPaymentForm->setAttributes(Yii::$app->session->get($this->sessionKeyForPaymentPart));
            }
            if (Yii::$app->session->has($this->sessionKeyForBillingPart) && !$checkoutShippingForm->useShippingAddressForBilling) {
                $checkoutBillingForm->setAttributes(Yii::$app->session->get($this->sessionKeyForBillingPart));
            }
            $response[ 'billing_and_payment_form' ] = $this->renderAjax('checkout-billing-and-payment-form', [
                'checkoutBillingForm' => $checkoutBillingForm,
                'isNeedBillingForm' => ($checkoutShippingForm->useShippingAddressForBilling) ? false : true,
                'checkoutPaymentForm' => $checkoutPaymentForm,
                'isAuthUser' => !Yii::$app->user->isGuest,
            ]);
        }

        return $response;
    }

    /**
     * Ajax validation shipping form on page checkout
     *
     * @return
     */
    public function actionAjaxValidateShippingPart()
    {
        $userId = (!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id : null;

        $checkoutShippingForm = new CheckoutShippingForm($userId);

        if (Yii::$app->request->isAjax && $checkoutShippingForm->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($checkoutShippingForm);
        }
    }

    /**
     * Get checkout shipping form from session.
     *
     * @return
     */
    public function actionEditCheckoutShippingPart()
    {
        $response = [
            'status' => false,
            'html' => null,
        ];

        $userId = (!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id : null;

        Yii::$app->response->format = Response::FORMAT_JSON;

        $checkoutShippingForm = new CheckoutShippingForm($userId);

        if (Yii::$app->session->has($this->sessionKeyForShippingPart)) {
            $checkoutShippingForm->setAttributes(Yii::$app->session->get($this->sessionKeyForShippingPart));

            $response[ 'status' ] = true;
            $response[ 'html' ] = $this->renderAjax('checkout-shipping-form', [
                'checkoutShippingForm' => $checkoutShippingForm,
                'isAuthUser' => !Yii::$app->user->isGuest,
            ]);
        }

        return $response;
    }

    /**
     * Ajax validation billing and payment forms on page checkout
     *
     * @return
     */
    public function actionAjaxValidateBillingAndPaymentPart()
    {
        $userId = (!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id : null;
        $errors = [];

        $checkoutBillingForm = new CheckoutBillingForm();
        $checkoutPaymentForm = new CheckoutPaymentForm();
        $checkoutShippingForm = new CheckoutShippingForm($userId);

        Yii::$app->response->format = Response::FORMAT_JSON;

        if (!Yii::$app->session->has($this->sessionKeyForShippingPart)) {
            return $errors;
        }

        $checkoutShippingForm->setAttributes(Yii::$app->session->get($this->sessionKeyForShippingPart));

        if (Yii::$app->request->isAjax) {
            $errors = [];

            if ($checkoutPaymentForm->load(Yii::$app->request->post())) {
                $errors = $errors + ActiveForm::validate($checkoutPaymentForm);
            }

            if (!$checkoutShippingForm->useShippingAddressForBilling) {
                $checkoutBillingForm->load(Yii::$app->request->post());

                $errors = $errors + ActiveForm::validate($checkoutBillingForm);
            }

            return $errors;
        }
    }

    /**
     * Save checkout shipping form in session.
     *
     * @return
     */
    public function actionSaveBillingAndPaymentPart()
    {
        $userId = (!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id : null;

        $checkoutBillingForm = new CheckoutBillingForm();
        $checkoutPaymentForm = new CheckoutPaymentForm();
        $checkoutShippingForm = new CheckoutShippingForm($userId);

        $response = [
            'status' => false,
            'html' => null,
            'order_review' => null,
        ];

        Yii::$app->response->format = Response::FORMAT_JSON;

        if (!Yii::$app->session->has($this->sessionKeyForShippingPart)) {
            return $response;
        }

        $checkoutShippingForm->setAttributes(Yii::$app->session->get($this->sessionKeyForShippingPart));

        if (!$checkoutPaymentForm->load(Yii::$app->request->post()) || !$checkoutPaymentForm->validate()) {
            return $response;
        }

        if (!$checkoutShippingForm->useShippingAddressForBilling) {
            if (!$checkoutBillingForm->load(Yii::$app->request->post()) || !$checkoutBillingForm->validate()) {
                return $response;
            }
        }

        Yii::$app->session->set($this->sessionKeyForPaymentPart, $checkoutPaymentForm->attributes);
        Yii::$app->session->set($this->sessionKeyTruncatedcardpan, Yii::$app->request->post('truncatedcardpan'));
        Yii::$app->session->set($this->sessionKeyPseudocardpan, Yii::$app->request->post('pseudocardpan'));

        if (!$checkoutShippingForm->useShippingAddressForBilling) {
            Yii::$app->session->set($this->sessionKeyForBillingPart, $checkoutBillingForm->attributes);
        } else {
            Yii::$app->session->set($this->sessionKeyForBillingPart, '');
        }

        $orderReviewForm = new CheckoutOrderReviewForm();

        $response[ 'status' ] = true;
        $response[ 'html' ] = $this->renderPartial('saved-billing-and-payment-part', compact('checkoutPaymentForm', 'checkoutShippingForm', 'checkoutBillingForm'));
        $response[ 'order_review' ] = $this->renderAjax('checkout-order-review', compact('orderReviewForm'));


        return $response;
    }

    /**
     * Get credit card form
     *
     * @return
     */
    public function actionGetPaymentCreditCardForm($type)
    {
        $userId = (!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id : null;

        $form                      = new ActiveForm();
        $checkoutPaymentForm       = new CheckoutPaymentForm();
        $checkoutPaymentForm->type = $type;

        if ($type === Order::CREDIT_METHOD) {
            return $this->renderAjax('block-payment-credit-card', [
                'form' => $form,
                'checkoutPaymentForm' => $checkoutPaymentForm,
                'isAuthUser' => !Yii::$app->user->isGuest,
            ]);
        }

        return '';
    }

    /**
     * Get form for edit billing information form and payment form.
     *
     * @return
     */
    public function actionEditBillingAndPaymentPart()
    {
        $response = [
            'status' => false,
            'html' => null,
        ];

        $userId = (!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id : null;

        Yii::$app->response->format = Response::FORMAT_JSON;

        $checkoutBillingForm = new CheckoutBillingForm();
        $checkoutPaymentForm = new CheckoutPaymentForm();
        $checkoutShippingForm = new CheckoutShippingForm($userId);

        if (
            Yii::$app->session->has($this->sessionKeyForShippingPart) &&
            Yii::$app->session->has($this->sessionKeyForPaymentPart) &&
            Yii::$app->session->has($this->sessionKeyForBillingPart)
        ) {

            $checkoutShippingForm->setAttributes(Yii::$app->session->get($this->sessionKeyForShippingPart));
            $checkoutPaymentForm->setAttributes(Yii::$app->session->get($this->sessionKeyForPaymentPart));
            $checkoutPaymentForm->savedPaymentCardId = '';

            if (Yii::$app->session->get($this->sessionKeyForBillingPart) !== '') {
                $checkoutBillingForm->setAttributes(Yii::$app->session->get($this->sessionKeyForBillingPart));
            }

            $response[ 'status' ] = true;
            $response[ 'html' ] = $this->renderAjax('checkout-billing-and-payment-form', [
                'checkoutBillingForm' => $checkoutBillingForm,
                'isNeedBillingForm' => ($checkoutShippingForm->useShippingAddressForBilling) ? false : true,
                'checkoutPaymentForm' => $checkoutPaymentForm,
                'isAuthUser' => !Yii::$app->user->isGuest,
            ]);
        }

        return $response;
    }

    /**
     * Get checkout shipping form for saved shipping information for user.
     *
     * @throws \yii\web\NotFoundHttpException If request not ajax
     * @return JSON response with mixed content
     */
    public function actionGetShippingFormForSavedAddress()
    {
        if (Yii::$app->request->isAjax) {
            $userId = Yii::$app->user->identity->id;
            $isAuthUser = ($userId) ? true : false;

            Yii::$app->response->format = Response::FORMAT_JSON;

            $response = [
                'status' => false,
                'html' => '',
            ];

            $checkoutShippingFormTmp = new CheckoutShippingForm($userId);
            $checkoutShippingFormTmp->load(Yii::$app->request->post());

            $userShippingAddress = UserShippingAddress::findOne([
                'user_id' => $userId,
                'id'      => $checkoutShippingFormTmp->savedShippingAddressId,
            ]);

            $response[ 'status' ] = true;

            // if user find saved shipping information, then create shipping form with fill data
            if ($userShippingAddress) {
                $checkoutShippingForm = new CheckoutShippingForm($userId, $userShippingAddress);
                // create shipping form with empty fill data
            } else {
                $checkoutShippingForm = new CheckoutShippingForm($userId);
            }

            $totalPrice = CartHelper::getTotalPriceWithCoupon();
            $tax = $totalPrice / 100 * OrderShippingInformation::TAX_PERCENT;

            $checkoutShippingForm->useShippingAddressForBilling = $checkoutShippingFormTmp->useShippingAddressForBilling;
            $checkoutShippingForm->shippingMethod = $checkoutShippingFormTmp->shippingMethod;
            $checkoutShippingForm->savedShippingAddressId = $checkoutShippingFormTmp->savedShippingAddressId;
            $checkoutShippingForm->company = $checkoutShippingFormTmp->company;

            $form = new ActiveForm();

            $response[ 'html' ] = $this->renderAjax('checkout-shipping-form', compact('checkoutShippingForm', 'form', 'isAuthUser'));
            $response[ 'shipping' ] = $checkoutShippingForm->getShippingStandartMethodPrice();
            $response[ 'shippingDate' ] = $checkoutShippingForm->shippingMethodInfo;
            $response[ 'total' ]    = ProductVariation::viewFormatPrice((float)$totalPrice + $tax + $response[ 'shipping' ]);

            return $response;
        }
    }

    /**
     * Get checkout payment form for saved payment cards for user.
     *
     * @throws \yii\web\NotFoundHttpException If request not ajax
     * @param int $savedPaymentCardId Saved payment card id for user
     * @return JSON response with mixed content
     */
    public function actionGetPaymentFormForSavedPaymentCard()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            if (Yii::$app->user->isGuest) {
                return [
                    'status' => false,
                ];
            }
            $userId = Yii::$app->user->identity->id;
            $isAuthUser = true ;

            $response = [
                'status' => false,
                'html' => '',
                'number' => '',
                'pseudocardpan' => '',
            ];

            $checkoutPaymentFormTmp = new CheckoutPaymentForm();
            $checkoutPaymentFormTmp->load(Yii::$app->request->post());

            $userPaymentCard = UserPaymentCard::findOne([
                'user_id' => $userId,
                'id' => $checkoutPaymentFormTmp->savedPaymentCardId
            ]);

            $form = new ActiveForm();
            $checkoutPaymentForm = new CheckoutPaymentForm();

            $checkoutPaymentForm->type = Order::CREDIT_METHOD;

            $response[ 'html' ] = $this->renderAjax('block-payment-credit-card', compact('checkoutPaymentForm', 'form', 'isAuthUser'));

            if ($userPaymentCard) {
                $response[ 'status' ] = true;
                $response[ 'number' ] = $userPaymentCard->number;
                $response[ 'pseudocardpan' ] = $userPaymentCard->pseudocardpan;
                $response[ 'html' ] = $this->renderPartial('saved-user-card', compact('userPaymentCard'));
            }

            return $response;
        }
    }

    /**
     * Action for change shipping method.
     *
     * @return
     */
    public function actionChangeShippingMethod()
    {
        $response = [
            'shipping' => 0,
            'total'    => 0,
            'shippingDate' => null
        ];

        $userId = (!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id : null;

        Yii::$app->response->format = Response::FORMAT_JSON;

        $checkoutShippingForm = new CheckoutShippingForm($userId);
        $checkoutShippingForm->load(Yii::$app->request->post());

        $totalPrice = CartHelper::getTotalPriceWithCoupon();
        $tax = $totalPrice / 100 * OrderShippingInformation::TAX_PERCENT;

        $response[ 'shipping' ] = $checkoutShippingForm->getShippingPriceByMethod();
        $response[ 'shippingDate' ] = $checkoutShippingForm->shippingMethodInfo;
        $response[ 'total' ]    = ProductVariation::viewFormatPrice((float)$totalPrice + $tax + $response[ 'shipping' ]);

        return $response;
    }

    /**
     * Change country in shipping.
     *
     * @param int $countryId Country id.
     * @return string JSON format.
     */
    public function actionChangeCountry($countryId)
    {
        $response = [
            'shippingMethodsBlock' => null,
            'shipping' => 0,
            'total' => 0,
            'shippingDate' => null,
        ];

        $country = Country::findByIdWithDefault($countryId);

        $form = new ActiveForm();

        $userId = (!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id : null;

        Yii::$app->response->format = Response::FORMAT_JSON;

        $checkoutShippingForm = new CheckoutShippingForm($userId);
        $checkoutShippingForm->shippingMethod = $checkoutShippingForm->getShippingStandartMethod();
        $checkoutShippingForm->countryId = $countryId;

        $totalPrice = CartHelper::getTotalPriceWithCoupon();
        $tax = $totalPrice / 100 * OrderShippingInformation::TAX_PERCENT;

        $nameView = (strtolower($country->code) === 'de') ? 'checkout-shipping-methods-block-germany' : 'checkout-shipping-methods-block-other-countries';

        $response[ 'shipping' ] = $checkoutShippingForm->getShippingStandartMethodPrice();
        $response[ 'shippingDate' ] = $checkoutShippingForm->shippingMethodInfo;
        $response[ 'total' ]    = ProductVariation::viewFormatPrice((float)$totalPrice + $tax + $response[ 'shipping' ]);
        $response[ 'shippingMethodsBlock' ] = $this->renderPartial($nameView, compact('checkoutShippingForm', 'form'));

        return $response;
    }

    /**
     * Process checkout.
     *
     * @return
     */
    public function actionProcess()
    {
        if (!CartHelper::isValidCart()) {
            return $this->redirect(['/product/cart/index']);
        }

        $userId = (!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id : null;
        $session = Yii::$app->session;

        if (!$session->has($this->sessionKeyForShippingPart) || !$session->has($this->sessionKeyForBillingPart) || !$session->has($this->sessionKeyForPaymentPart)) {
            return $this->redirect(['index']);
        }

        /**
         * Checkout shipping form.
         *
         * @var \frontend\modules\product\models\forms\CheckoutShippingForm $checkoutShippingForm
         */
        $checkoutShippingForm = new CheckoutShippingForm($userId);

        /**
         * Checkout payment form.
         *
         * @var \frontend\modules\product\models\forms\CheckoutPaymentForm $checkoutPaymentForm
         */
        $checkoutPaymentForm = new CheckoutPaymentForm();

        /**
         * Checkout billing form.
         *
         * @var \frontend\modules\product\models\forms\CheckoutBillingForm $checkoutBillingForm
         */
        $checkoutBillingForm = new CheckoutBillingForm();

        $checkoutShippingForm->setAttributes($session->get($this->sessionKeyForShippingPart));
        $checkoutPaymentForm->setAttributes($session->get($this->sessionKeyForPaymentPart));

        if (!$checkoutShippingForm->validate() || !$checkoutPaymentForm->validate()) {
            return $this->redirect(['index']);
        }

        if (!$checkoutShippingForm->useShippingAddressForBilling) {
            $checkoutBillingForm->setAttributes($session->get($this->sessionKeyForBillingPart));

            if (!$checkoutBillingForm->validate()) {
                return $this->redirect(['index']);
            }
        }

        if ($session->has($this->sessionKeyPseudocardpan)) {
            $checkoutPaymentForm->pseudocardpan = $session->get($this->sessionKeyPseudocardpan);
            $checkoutPaymentForm->number = $session->get($this->sessionKeyTruncatedcardpan);
        }

        /**
         * @var \frontend\modules\product\helpers\CheckoutDTO               $orderDto
         * @var \frontend\modules\product\models\forms\CheckoutShippingForm $checkoutShippingForm
         * @var \frontend\modules\product\models\forms\CheckoutBillingForm  $checkoutBillingForm
         * @var \frontend\modules\product\models\forms\CheckoutPaymentForm  $checkoutPaymentForm
         */
        $orderDto = new CheckoutDTO($checkoutShippingForm, $checkoutBillingForm, $checkoutPaymentForm);

        if (CartHelper::hasCoupon()) {
            $coupon = CartHelper::getCoupon();
        } else {
            $coupon = null;
        }

        $totalPriceWithCoupon = CartHelper::getTotalPriceWithCoupon();
        $totalPrice = CartHelper::getTotalPrice();

        if (!CartHelper::isValidCart()) {
            return $this->redirect(['/product/cart/index']);
        }

        /** @var bool|\frontend\modules\product\models\Order $order */
        $order = Order::createNewOrder($orderDto, CartHelper::getCart(), $totalPrice, $totalPriceWithCoupon, $coupon, $userId);

        if ($order) {
            switch ($checkoutPaymentForm->type) {
                // paypal
                case Order::PAYPAL_METHOD:
                    return $this->createPaypalOrder($order->id);
                // Klarna
                case Order::KLARNA_METHOD:
                    return $this->createKlarnaOrder($order->id);
                default:
                    return $this->createPayoneOrder($order->id, $session->get($this->sessionKeyPseudocardpan));
            }

            $session->set($this->sessionKeySuccessOrder, $order->id);
        }

        throw new Exception('Error create order');
    }

    /**
     * Finish payment paypal.
     *
     * @throws \yii\web\NotFoundHttpException If order not found or exist error in paypal api
     * @param int $orderNumber Order number
     * @return string
     */
    public function actionFinishPaypal($orderNumber)
    {
        $order = Order::find()
            ->where(['order_number' => $orderNumber, 'status' => 'new', 'type_payment' => Order::PAYPAL_METHOD])
            ->one();

        if (!$order) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $cart = CartHelper::getCart();

        Order::setPaidSuccessfully($order, $cart, CartHelper::getProductIds());
        CartHelper::emptyCart();

        Yii::$app->session->set($this->sessionKeySuccessOrder, $order->id);

        return $this->redirect(['thanks']);
    }

    /**
     * Finish payment klarna.
     *
     * @throws \yii\web\NotFoundHttpException If order not found or exist error in paypal api
     * @param int $orderNumber Order number
     * @return string
     */
    public function actionFinishKlarna($orderNumber)
    {
        $order = Order::find()
            ->where(['order_number' => $orderNumber, 'status' => 'new', 'type_payment' => Order::KLARNA_METHOD])
            ->one();

        if (!$order) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $cart = CartHelper::getCart();

        Order::setPaidSuccessfully($order, $cart, CartHelper::getProductIds());
        CartHelper::emptyCart();

        Yii::$app->session->set($this->sessionKeySuccessOrder, $order->id);

        return $this->redirect(['thanks']);
    }

    /**
     * Display page thanks for success order.
     *
     * @return
     */
    public function actionThanks()
    {
        $this->layout = '/checkout';

        if (!Yii::$app->session->has($this->sessionKeySuccessOrder)) {
            throw new NotFoundHttpException('Page not found');
        }

        $order = Order::find()
            ->with('orderShippingInformation', 'orderProducts', 'orderProducts.productVariation')
            ->where(['status' => Order::ORDER_STATUS_PAID, 'id' => Yii::$app->session->get($this->sessionKeySuccessOrder)])
            ->orWhere(['status' => Order::ORDER_STATUS_PREAUTORIZATION, 'id' => Yii::$app->session->get($this->sessionKeySuccessOrder)])
            ->one();

        if (!$order) {
            throw new NotFoundHttpException('Page not found');
        }

        if ($page = CorePage::findThanksPage()) {
            $this->setMetaTitle($page->meta_title);
            $this->setMetaDescription($page->meta_description);
            $this->setMetaKeywords($page->meta_keywords);
        }

        return $this->render('/order/thanks-order', compact('order'));
    }

    /**
     * Payone credit card payment back.
     *
     * @return string
     */
    public function actionCheckoutPaymentBack()
    {
        return $this->redirect(['index']);
    }

    public function actionCheckoutPaymentError()
    {
        $message = (Yii::$app->session->hasFlash('payment-error')) ? Yii::$app->session->getFlash('payment-error') : 'Payment error';

        throw new \Exception($message);
    }

    /**
     * Payone credit card payment success.
     *
     * @param string $reference Order id.
     * @return string
     */
    public function actionSuccessPayone($reference)
    {
        $order = Order::find()
            ->where(['order_number' => $reference, 'status' => 'new', 'type_payment' => Order::CREDIT_METHOD])
            ->one();

        if (!$order) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $cart = CartHelper::getCart();

        Order::setPaidPreautorization($order, $order->credit_txid, $cart, CartHelper::getProductIds());
        CartHelper::emptyCart();

        Yii::$app->session->set($this->sessionKeySuccessOrder, $order->id);

        return $this->redirect(['thanks']);
    }

    /**
     * Start payment paypal service.
     *
     * @access private
     * @param int $orderId Order id
     * @return string
     */
    private function createPaypalOrder($orderId)
    {
        $cart = CartHelper::getCart();
        $shippingInfo = OrderShippingInformation::find()->with('country')->where(['order_id' => $orderId])->one();
        $order = Order::findOne($orderId);

        try {
            $response = Paypal::createRequest($order, $cart, $shippingInfo);
        } catch (\Exception $e) {
            Yii::$app->session->setFlash('payment-error', $e->getMessage());

            return $this->redirect('checkout-payment-error');
        }

        if ($response[ 'status' ] === 'REDIRECT') {

            return $this->redirect($response[ 'redirecturl' ]);
        }

        return $this->redirect('checkout-payment-error');
    }

    /**
     * Start payment klarna service.
     *
     * @access private
     * @param int $orderId Order id
     * @return string
     */
    private function createKlarnaOrder($orderId)
    {
        $cart = CartHelper::getCart();
        $shippingInfo = OrderShippingInformation::find()->with('country')->where(['order_id' => $orderId])->one();
        $order = Order::findOne($orderId);
        try {
            $response = Klarna::createRequest($order, $cart, $shippingInfo);
        } catch (\Exception $e) {
            Yii::$app->session->setFlash('payment-error', $e->getMessage());

            return $this->redirect('checkout-payment-error');
        }

        if ($response[ 'status' ] === 'REDIRECT') {

            return $this->redirect($response[ 'redirecturl' ]);
        }

        return $this->redirect('checkout-payment-error');
    }

    /**
     * Set fail paypal payment.
     *
     * @access private
     * @return string
     */
    private function createPayoneOrder($orderId, $pseudocardpan)
    {
        $cart = CartHelper::getCart();
        $shippingInfo = OrderShippingInformation::find()->with('country')->where(['order_id' => $orderId])->one();
        $order = Order::findOne($orderId);
        try {
            $response = Payone::sendPreautorization($order, $cart, $shippingInfo, $pseudocardpan);
        } catch (\Exception $e) {
            Yii::$app->session->setFlash('payment-error', $e->getMessage());

            return $this->redirect('checkout-payment-error');
        }

        if ($response[ 'status' ] === 'APPROVED') {
            $order->credit_txid = $response[ 'txid' ];

            Order::setPaidPreautorization($order, $response[ 'txid' ], $cart, CartHelper::getProductIds());
            CartHelper::emptyCart();

            Yii::$app->session->set($this->sessionKeySuccessOrder, $order->id);

            return $this->redirect(['thanks']);
        }

        if ($response[ 'status' ] === 'REDIRECT') {
            return $this->redirect($response[ 'redirecturl' ]);
        }

        return $this->redirect('checkout-payment-error');
    }
}