<?php

namespace frontend\modules\product\controllers;

use backend\modules\product\models\forms\UploadImagesProductVariationForm;
use frontend\modules\product\models\forms\ProductItemFilterForm;
use frontend\modules\product\models\forms\ProductSearchFilterForm;
use frontend\modules\product\models\forms\ProductSearchForm;
use frontend\modules\product\models\ProductImage;
use frontend\modules\product\models\ProductRelationCategorie;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use frontend\modules\product\models\ProductVariation;
use frontend\modules\product\models\forms\ProductReviewForm;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use frontend\modules\product\models\ProductReview;
use frontend\modules\product\models\Product;
use frontend\modules\product\models\ProductCategory;
use frontend\modules\core\controllers\FrontController;
use frontend\modules\product\models\forms\ProductCartForm;
use yii\web\Response;
use frontend\modules\core\models\CorePage;

/**
 * Frontend controller for Product.
 */
class ProductController extends FrontController
{
    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
               'class' => AccessControl::className(),
                'only' => ['get-reply-review-form', 'create-review'],
                'rules' => [
                    [
                        'actions' => ['get-reply-review-form', 'create-review'],
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
        ];
    }

    public function actionFilterVariations()
    {
        if (!Yii::$app->request->isAjax) throw new NotFoundHttpException("Not found");

        Yii::$app->response->format = Response::FORMAT_JSON;

        $selectFilterId = Yii::$app->request->post('selectFilterId');

        $result = [
            'status' => 'error',
            'selectFilterId' => $selectFilterId
        ];

        if (!($product_id = Yii::$app->request->post('id', null))) return $result;

        $modelProductVariations = $this->getProductItemFilter($product_id);

        $result['status'] = 'ok';
        if (!$modelProductVariations) {
            $result['status'] = 'not';
            $this->resetFilterOneParam($selectFilterId);
            $modelProductVariations = $this->getProductItemFilter($product_id);
        };

        $this->clearDownFilters($selectFilterId);
        $result['query']['search'] = $this->getFiltersQuery();
        $result['query']['selectFilters'] = Yii::$app->request->post('filters');

        if (!$modelProductVariations) {
            $result['status'] = 'not';
            return $result;
        }

        $buttonActive = count($modelProductVariations)==1?true:false;

        $firstProductVariation = reset($modelProductVariations);
        $result['variation']['data'] = $firstProductVariation->getAttributes([
            'id',
            'product_id',
            'name',
            'vendor_code',
            'description',
            'short_description'
        ]);
        $result['variation']['images'] = array_values(ArrayHelper::map($firstProductVariation->images, 'id', function ($model) {
            return $model->urlImage;
        }));
        $result['variation']['price'] = $firstProductVariation->viewNumberFormatFullPrice;

        $result['variation']['isdiscount'] = $firstProductVariation->isDiscount;
        $result['variation']['promotion'] = ProductVariation::viewFormatPrice($firstProductVariation->priceDiscount);

        $result['variation']['properties'] = ArrayHelper::map($firstProductVariation->properties, 'filter_id', 'value');
        $result['variation']['count'] = $firstProductVariation->getMaxCountForCart();

        $filters = [];
        foreach ($modelProductVariations as $modelProductVariation) {
            foreach ($modelProductVariation->properties as $propertie) {
                $filters[$propertie->filter_id][$propertie->value] = $propertie->value;
            }
        }

        $this->disableLastFilter($selectFilterId);
        $modelProductVariations = $this->getProductItemFilter($product_id);
        if ($modelProductVariations) {
            foreach ($modelProductVariations as $modelProductVariation) {
                foreach ($modelProductVariation->properties as $propertie) {
                    if ($selectFilterId == $propertie->filter_id) $filters[$propertie->filter_id][$propertie->value] = $propertie->value;
                }
            }
        }

        $result['filters'] = $filters;
        $result['cart']['buttonActive'] = $buttonActive;
        $result['cart']['formAction'] = Url::toRoute(['/product/cart/add-product-to-cart', 'id' => $firstProductVariation->id]);

        //print_r($result[ 'variation' ]);
        //exit;

        return $result;
    }

    private function resetFilterOneParam($id) {
        $params = Yii::$app->request->getBodyParams();
        if (isset($params['filters']) && is_array($params['filters'])) {
            foreach ($params['filters'] as $key => $value) {
                if ($key != $id) $params['filters'][$key] = '';
            }
        }
        Yii::$app->request->setBodyParams($params);
    }

    private function getFiltersQuery() {
        $queryPost = Yii::$app->request->post();
        unset($queryPost['id']);
        unset($queryPost['selectFilterId']);
        return http_build_query($queryPost);
    }

    private function disableLastFilter($id) {
        $params = Yii::$app->request->getBodyParams();
        if (isset($params['filters']) && is_array($params['filters'])) {
            foreach ($params['filters'] as $key => $value) {
                if ($key == $id) $params['filters'][$key] = '';
            }
        }
        Yii::$app->request->setBodyParams($params);
    }

    private function clearDownFilters($id) {
        $params = Yii::$app->request->getBodyParams();
        if (isset($params['filters']) && is_array($params['filters'])) {
            $flag = false;
            foreach ($params['filters'] as $key => $value) {
                if ($flag) $params['filters'][$key] = '';
                if ($key == $id) $flag = true;
            }
        }
        Yii::$app->request->setBodyParams($params);
    }

    private function getProductItemFilter($product_id) {
        return ProductItemFilterForm::filter(
            ProductVariation::find()
                ->with('product', 'withProperties', 'inventorie')
                ->alias('variations')
                ->joinWith(['product product'])
                ->where(['variations.active' => 1, 'product.active' => 1, 'product.id' => (int) $product_id]),
            $product_id
        )->all();
    }

    /**
     * Dispay page product from slug without categories slug.
     *
     * @param string $slug Slug product
     * @return mixed
     */
    public function actionIndex($slug, $variation = null)
    {
        $masterProduct = $this->findProductBySlug($slug);

        $queryVariationProduct = $masterProduct->getDefaultProductVariation()
            ->active();

        if ($variation) {
            $queryVariationProduct->where(['id' => $variation]);
        }

        $firstVariationProduct = $queryVariationProduct
            ->one();

        if (!$firstVariationProduct) {
            throw new NotFoundHttpException("Product not found");
        }

        $formAddToCart = new ProductCartForm($firstVariationProduct->id, ['scenario' => ProductCartForm::SCENARIO_ADD]);
        $formReview    = new ProductReviewForm($masterProduct->id);

        $isUserVoiceRating = false;

        if (!Yii::$app->user->isGuest) {
            $isUserVoiceRating = $masterProduct->isUserVoiceRating(Yii::$app->user->identity->id);
        }

        $this->setMetaTitle($masterProduct->meta_title);
        $this->setMetaDescription($masterProduct->meta_description);
        $this->setMetaKeywords($masterProduct->meta_keywords);
        $this->setSocialTags($firstVariationProduct);

        switch ($masterProduct->type) {
            case Product::TYPE_PRODUCT_SET:
                return $this->render('product-set', [
                    'variationProduct' => $firstVariationProduct,
                    'formReview' => $formReview,
                    'formAddToCart' => $formAddToCart,
                    'masterProduct' => $masterProduct,
                    'isUserVoiceRating' => $isUserVoiceRating,
                ]);
            default:
                return $this->render('product', [
                    'variationProduct' => $firstVariationProduct,
                    'formReview' => $formReview,
                    'formAddToCart' => $formAddToCart,
                    'masterProduct' => $masterProduct,
                    'isUserVoiceRating' => $isUserVoiceRating,
                ]);
        }
    }

    /**
     * Get html form for reply answer review.
     *
     * @param int $reviewId Parent review id
     * @return null|mixed
     */
    public function actionGetReplyReviewForm($reviewId)
    {
        $review = ProductReview::findOne(['id' => $reviewId, 'active' => 1]);

        if (!$review) {
            return null;
        }

        $formReview = new ProductReviewForm($review->product_id);
        $formReview->parentId = $review->id;

        return $this->renderAjax('form-review', compact('formReview'));
    }

    /**
     * Create review to product variation.
     *
     * @throws \yii\web\NotFoundHttpException If product variation not found
     * @param int $productId Product variation id
     * @return string
     */
    public function actionCreateReview($productId)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $response = [
            'status' => false,
            'html' => null,
        ];

        $product = Product::findOne(['id' => $productId, 'active' => 1]);

        if (!$product) {
            throw new NotFoundHttpException("Product not found");
        }

        $formReview = new ProductReviewForm($productId);

        if ($formReview->load(Yii::$app->request->post()) && $formReview->create()) {
            $response['status'] = true;
            $response['html'] = $this->renderPartial('success-create-review');
            //Yii::$app->session->setFlash('success', 'Review successful added, but add on site after moderate');
        } else {
            $response['html'] = $this->renderAjax('unsuccess-create-review');
        }

        return $response;
    }

    /**
     * Change product rating.
     *
     * @param int $id Product id
     * @param float $value Rating value
     * @return Response JSON
     */
    public function actionChangeProductRating($id, $value)
    {
        $response = [
            'status' => false,
            'content' => null,
        ];

        if (Yii::$app->user->isGuest) {
            return $response;
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        $product = Product::findActiveProductById($id);

        if (!$product) {
            return $response;
        }

        $rating = $product->changeRating(Yii::$app->user->identity->id, $value);

        $response[ 'status' ] = true;
        $response[ 'content' ] = $this->renderAjax('rating-widget', [
            'masterProduct' => $product,
            'isUserVoiceRating' => true,
        ]);

        $response[ 'content' ] .= $this->renderPartial('@app/modules/core/views/layouts/js_alert', [
            'text' => 'You voice success add',
            'alertClass' => 'alert-success',
            'closeButton' => true,
        ]);

        return $response;
    }

    public function actionSearch() {
        $formProductSearch = new ProductSearchForm();
        if (!($formProductSearch->load(Yii::$app->request->get()) && $formProductSearch->validate())) {
            return $this->redirect(['/']);
        }

        $queryVariations = ProductSearchFilterForm::filter(ProductVariation::find()
            ->alias('default_variations')
            ->with('preview')
            ->leftJoin(Product::tableName().' product', 'product.default_variation_id = default_variations.id')
            ->leftJoin(ProductVariation::tableName().' variations', 'variations.product_id = product.id')
            ->where(['variations.active' => 1])
            ->andWhere(['product.active' => 1])
            ->andWhere(['LIKE', 'variations.name', $formProductSearch->query])
            ->groupBy('default_variations.product_id'));

        $dataProductProviders = new ActiveDataProvider([
            'query' => $queryVariations,
            'pagination' => [
                'pageSize' => 25,
                'validatePage' => false
            ],
        ]);

        if (Yii::$app->request->isAjax) {
            if (Yii::$app->request->get('page')) {
                return $this->renderPartial('search-ajax', [
                    'dataProductProviders' => $dataProductProviders,
                ]);
            } else {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $modelVariations = $queryVariations->all();

                $out = [];
                if ($modelVariations) {
                    foreach ($modelVariations as $modelVariation) {
                        $out[] = [
                            'id' => $modelVariation->id,
                            'name' => $modelVariation->name,
                            'value' => $modelVariation->name,
                            'image' => ($modelVariation->preview?$modelVariation->preview->getUrlImage():''),
                            'url' => Url::to([
                                '/product/product/index',
                                'slug' => $modelVariation->product?$modelVariation->product->slug:null
                            ])
                        ];
                    }
                }
                return $out;
            }
        } else {
            $modelVariations = $queryVariations->all();
            $findProductIds = ArrayHelper::map($modelVariations,'product_id', 'product_id');
            $findVariationIds = ArrayHelper::map($modelVariations,'product_id', 'product_id');
            $findCategorieIds = ArrayHelper::map(
                ProductRelationCategorie::findAll(['product_id' => $findProductIds]),
                'product_category_id',
                'product_category_id'
            );

            if ($page = CorePage::findSearchPage()) {
                $this->setMetaTitle($page->meta_title);
                $this->setMetaDescription($page->meta_description);
                $this->setMetaKeywords($page->meta_keywords);
            }

            return $this->render('search', [
                'dataProductProviders' => $dataProductProviders,
                'formProductSearch' => $formProductSearch,
                'findCategorieIds' => $findCategorieIds,
                'findVariationIds' => $findVariationIds,
            ]);
        }
    }

    public function actionMaxCount()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (!Yii::$app->request->isAjax) {
            $response = [
                'status' => 'error',
                'count' => null
            ];
            return $response;
        }

        $id = 0;

        $url = parse_url(Yii::$app->request->post('url',[]));
        if (isset($url['query'])) {
            parse_str($url['query'],$url);
            $id = (int) (isset($url['id'])?$url['id']:0);
        }

        if (($variationProduct = ProductVariation::findOne($id)))
        {
            $response = [
                'status' => 'ok',
                'count' => $variationProduct->getMaxCountForCart()
            ];
        }

        return $response;
    }

    public function actionLoadTabSetVariation() {
        $response = [
            'status' => 'error',
            'html' => null
        ];

        Yii::$app->response->format = Response::FORMAT_JSON;

        if (
            !(Yii::$app->request->isAjax
            && ($modelVariation = ProductVariation::findOne(Yii::$app->request->get('id',0))))
        ) {
            return $response;
        }

        $modelProduct = $modelVariation->product;

        $response = [
            'status' => 'ok',
            'html' => $this->renderAjax('load-tab-set-variation', [
                'modelVariation' => $modelVariation,
                'masterProduct' => $modelProduct,
            ])
        ];

        return $response;
    }

        /**
     * Register social tags.
     *
     * @access protected
     * @return void
     */
    protected function setSocialTags($product)
    {
        if ($product->isHasPreview()) {
            $this->view->registerMetaTag(['name' => 'og:image', 'content' => Url::toRoute([$product->getPreviewWithoutPlaceholder()], true)]);
        }
    }

    /**
     * Find active product by slug.
     *
     * @throws \yii\web\NotFoundHttpException If product not found
     * @param string $slug Product slug
     * @param int $categoryId Category id
     * @return \frontend\modules\product\models\Product
     */
    private function findProductBySlug($slug)
    {
        $product = Product::find()
            ->with('productVariations')
            ->where(['slug' => $slug])
            ->active()
            ->one();

        if (!$product || empty($product->productVariations)) {
            throw new NotFoundHttpException("Product not found");
        }

        return $product;
    }

}
