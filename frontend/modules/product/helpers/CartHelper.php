<?php

namespace frontend\modules\product\helpers;

use frontend\modules\core\models\Setting;
use frontend\modules\product\models\Coupon;
use frontend\modules\product\models\ProductVariation;
use Yii;
use StdClass;
use yii\helpers\VarDumper;

/**
 * Helper class to work with cart.
 */
class CartHelper
{
    /**
     * Name key cookie cart.
     *
     * @var string $cookieCartKey
     */
    private static $cookieCartKey = 'user-cart';

    /**
     * Name key cookie coupon.
     *
     * @var string $cookieCartKey
     */
    private static $cookieCouponKey = 'user-coupon';

    private static $cart;

    /**
     * Get cart from cookie.
     *
     * @return array
     */
    public static function getCart()
    {
        if (self::$cart === false) {
            return [];
        }

        if (!empty(self::$cart)) {
            return self::$cart;
        }

        $cookies = Yii::$app->request->cookies;

        return $cookies->getValue(self::$cookieCartKey, []);
    }

    /**
     * Check if cart is empty.
     *
     * @return bool
     */
    public static function isEmptyCart()
    {
        $cookies = Yii::$app->getRequest()->getCookies();

        return ((self::$cart === false) || (!$cookies->has(self::$cookieCartKey)));
    }

    /**
     * Check if coupon apply.
     *
     * @return bool
     */
    public static function hasCoupon()
    {
        $cookies = Yii::$app->getRequest()->getCookies();

        return $cookies->has(self::$cookieCouponKey);
    }

    /**
     * Remove coupon.
     *
     * @return void
     */
    public static function removeCoupon()
    {
        $cookies = Yii::$app->getResponse()->getCookies();

        if (self::hasCoupon()) {
            $cookies->remove(self::$cookieCouponKey);
        }

        return Yii::$app->response;
    }

    /**
     * Set coupon to cookie.
     *
     * @param string $couponCode Coupon code
     * @param float $priceWithCoupon Total price with coupone
     * @return void
     */
    public static function addCoupon($coupon)
    {
        $cookies = Yii::$app->getResponse()->getCookies();

        $cookies->add(new \yii\web\Cookie([
            'name' => self::$cookieCouponKey,
            'value' => $coupon,
        ]));

        $totalPrice = self::getTotalPrice();

        if ($coupon->type === Coupon::FIX_TYPE) {
            $totalPrice -= $coupon->value;
        } else if ($coupon->type === Coupon::PERCENT_TYPE) {
            $totalPrice = $totalPrice * (100 - $coupon->value) / 100;
        }

        return $totalPrice;
    }

    /**
     * Get coupon from cookie.
     *
     * @return array|null
     */
    public static function getCoupon()
    {
        $cookies = Yii::$app->getRequest()->getCookies();

        if (self::hasCoupon()) {

            return $cookies->getValue(self::$cookieCouponKey);
        }

        return null;
    }

    /**
     * Set empty cart.
     *
     * @return void
     */
    public static function emptyCart()
    {
        $cookies = Yii::$app->getResponse()->getCookies();

        if (!self::isEmptyCart()) {
            unset($cookies[ self::$cookieCartKey ]);
            $cookies->remove(self::$cookieCartKey);
            self::$cart = false;
        }

        if (self::hasCoupon()) {
            self::removeCoupon();
        }

        return Yii::$app->getResponse();
    }

    /**
     * Add variation product to cart.
     *
     * @param int $id Product variation id
     * @param CartDTO $data Data product structure for cart
     * @return void
     */
    public static function addToCart(CartDTO $data)
    {
        if (!CartHelper::isEmptyCart()) {
            $cart = CartHelper::getCart();

            $cart[ $data->productVariationId ] = $data;
        } else {
            $cart = [
                $data->productVariationId => $data,
            ];
        }

        return self::setCart($cart);
    }

    /**
     * Remove product variation from cart.
     *
     * @param int $id Product variation id
     * @return void
     */
    public static function removeProductFromCart($id)
    {
        $cart = CartHelper::getCart();

        if (isset($cart[ $id ])) {
            unset($cart[ $id ]);
        }

        if (!empty($cart)) {
            CartHelper::setCart($cart);
        } else {
            CartHelper::emptyCart();
        }

        $totalPrice = 0;

        foreach ($cart as $productVariationId => $item) {
            $totalPrice += $item->totalPrice;
        }

        return $totalPrice;
    }

    /**
     * Get total cart price.
     *
     * @return float
     */
    public static function getTotalPrice()
    {
        $totalPrice = 0;

        if (!self::isEmptyCart()) {
            $cart = CartHelper::getCart();

            foreach ($cart as $productVariationId => $item) {
                $totalPrice += $item->totalPrice;
            }
        }

        return $totalPrice;
    }

    /**
     * Get total cart price with coupon.
     *
     * @return float
     */
    public static function getTotalPriceWithCoupon()
    {
        $totalPrice = self::getTotalPrice();

        if (self::hasCoupon()) {
            $coupon = self::getCoupon();

            if ($coupon->type === Coupon::FIX_TYPE) {
                $totalPrice -= $coupon->value;
            } else if ($coupon->type === Coupon::PERCENT_TYPE) {
                $totalPrice = $totalPrice * (100 - $coupon->value) / 100;
            }
        }

        return $totalPrice;
    }

    /**
     * Get array product variations ids, which located in cart.
     *
     * @return int[]
     */
    public static function getProductIds()
    {
        if (self::isEmptyCart()) {
            return [];
        }

        return array_keys(CartHelper::getCart());
    }

    /**
     * Get product from cart by id
     *
     * @param int $id Product variation id
     * @return CartDTO $data Data product structure for cart
     */
    public static function getProductById($id)
    {
        $cart = self::getCart();

        if (isset($cart[ $id ])) {
            return $cart[ $id ];
        }

        return null;
    }

    public static function getCountProductsInCart()
    {
        return count(self::getCart());
    }

    /**
     * Increment product count in cart by product id.
     *
     * @param int $id Product variation id
     * @return void
     */
    public static function incrementProductCount($id)
    {
        $cart = self::getCart();

        if (isset($cart[ $id ])) {
            $cart[ $id ]->count++;
            $cart[ $id ]->totalPrice += $cart[ $id ]->getPrice();
        }

        return self::setCart($cart);
    }

    /**
     * Decrement product count in cart by product id.
     *
     * @param int $id Product variation id
     * @return void
     */
    public static function decrementProductCount($id)
    {
        $cart = self::getCart();

        if (isset($cart[ $id ])) {
            $cart[ $id ]->count--;
            $cart[ $id ]->totalPrice -= $cart[ $id ]->getPrice();
        }

        return self::setCart($cart);
    }

    /**
     * Check is user cart products changes.
     *
     * @static
     * @return StdClass
     */
    public static function isChangeProducts()
    {
        $response = new StdClass();
        $response->result = false;
        $response->text = '';

        $productIds = self::getProductIds();
        $cart = self::getCart();

        if (empty($productIds)) {
            return $response;
        }

        $variations = ProductVariation::find()
            ->where(['id' => $productIds])
            ->with('inventorie', 'price', 'promotion')
            ->indexBy('id')
            ->all();

        foreach ($variations as $id => $variation) {

            $count = $variation->inventorie->count;

            //paked
            if ($variation->isProductAnPack()) {
                $mainPackProductVariation = $variation->product->mainPackProduct->getProductVariations()->with('inventorie')->one();

                if ($mainPackProductVariation && $mainPackProductVariation->inventorie) {
                    $count = floor($mainPackProductVariation->inventorie->count / $variation->inventorie->count);
                } else {
                    $count = 0;
                }
            }

            if ($variation->isProductSet()) {
                $count = $variation->product->countSet;
            }

            if ($count == 0) {
                self::removeProductFromCart($id);

                if (!$response->result) {
                    $response->result = true;
                }

                $response->text .= sprintf('<b><ins>%s %s</ins></b> - ausverkauft.<br>', $variation->vendor_code, $variation->name);

                continue;
            }
            $itemCart = $cart[ $id ];

            if ($cart[ $id ]->count > $count) {
                $itemCart->count = $count;

                if (!$response->result) {
                    $response->result = true;
                }

                $response->text .= sprintf('<b><ins>%s %s</ins></b> - nur noch %s Stück auf Lager.<br>', $variation->vendor_code, $variation->name, $itemCart->count);
            }


            $itemCart->price = $variation->price->price;

            if ($variation->promotion && $variation->promotion->price) {
                $itemCart->promotionPrice = $variation->promotion->price;
                $itemCart->totalPrice = $itemCart->count * $itemCart->promotionPrice;
            } else {
                $itemCart->promotionPrice = null;
                $itemCart->totalPrice = $itemCart->count * $itemCart->price;
            }

            self::updateProductInCart($id, $itemCart);
        }

        if ($response->result) {
            $response->text = 'Leider sind die Artikel nicht mehr in der ausgewählten Menge verfügbar.<br>' . $response->text;
        }

        return $response;
    }

    /**
     * Check is user cart is valid.
     *
     * @static
     * @return bool
     */
    public static function isValidCart()
    {
        $productIds = self::getProductIds();
        $cart = self::getCart();

        if (empty($productIds)) {
            return false;
        }

        $variations = ProductVariation::find()
            ->where(['id' => $productIds])
            ->with('inventorie', 'price', 'promotion')
            ->indexBy('id')
            ->all();

        foreach ($variations as $id => $variation) {
            if ($variation->isProductAnPack()) {
                $mainPackProductVariation = $variation->product->mainPackProduct->getProductVariations()->with('inventorie')->one();

                if (!$mainPackProductVariation || !$mainPackProductVariation->inventorie) {
                    return false;
                }

                $count = floor($mainPackProductVariation->inventorie->count / $variation->inventorie->count);

                if ($count == 0 || $cart[ $id ]->count > $count) {
                    return false;
                }
            } else {
                if ($variation->isProductSet()) {
                    $inventorieCount = $variation->product->countSet;
                }
                else {
                    $inventorieCount = $variation->inventorie->count;
                }
                if ($inventorieCount == 0 || $cart[ $id ]->count > $inventorieCount) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Set cart to cookie.
     *
     * @param array $cart
     * @return void
     */
    private static function setCart($cart)
    {
        $cookies = Yii::$app->getResponse()->getCookies();

        $cookies->add(new \yii\web\Cookie([
            'name' => self::$cookieCartKey,
            'value' => $cart,
        ]));

        self::$cart = $cart;

        return Yii::$app->getResponse();
    }

    private static function updateProductInCart($id, CartDTO $cartDto)
    {
        $cart = self::getCart();

        if ($cartDto->count === 0) {
            self::removeProductFromCart($id);
        } else {
            $cart[ $id ] = $cartDto;
        }

        self::setCart($cart);
    }

    /**
     * Get info free shipping.
     *
     * @param float $totalPrice
     * @return array
     */
    public static function getInfoFreeShipping($totalPrice)
    {
        $freeShipping = Yii::$app->userCountry->getSumFreeShippingByCountry();
        $value = $totalPrice / $freeShipping * 100;
        $remainder = $freeShipping - $totalPrice;

        return [
            'value' => ProductVariation::viewFormatPrice($value <= 100 ? $value : 100),
            'remainder' => ProductVariation::viewFormatPrice($remainder >= 0 ? $remainder : 0),
            'max' => ProductVariation::viewFormatPrice($freeShipping)
        ];
    }
}
