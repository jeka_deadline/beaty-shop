<?php

namespace frontend\modules\product\helpers;

use Yii;
use common\helpers\Payone as CommonPayone;
use yii\helpers\Url;
use common\components\Payone as BasePayone;

class Klarna extends CommonPayone
{
    const CURRENCY_EUR = 'EUR';
    const CURRENCY_USD = 'USD';

    public static function createRequest($order, $cart, $shippingInfo)
    {
        $personalData = self::getUserPersonalData($shippingInfo);
        $orderParameters = self::getOrderParameters($order);
        //$items = self::getItems($cart);

        $request = array_merge(self::getDefaults(), $personalData, $orderParameters);

        return BasePayone::sendRequest($request);
    }

    private static function getOrderParameters($order)
    {
        $config = Yii::$app->params[ 'klarna' ];
        $iban = (isset($config[ 'iban' ])) ? $config[ 'iban' ] : '';
        $bic = (isset($config[ 'bic' ])) ? $config[ 'bic' ] : '';

        return [
            "request" => "authorization",
            "clearingtype" => "sb",             // sb for Online Bank Transfer
            "onlinebanktransfertype" => "PNT",  // PNT for Sofort
            "bankcountry" => "DE",
            "iban" => $iban,
            "bic" => $bic,
            "amount" => str_replace('.', '', number_format($order->full_sum, 2, '.', '')),
            'currency' => self::CURRENCY_EUR,
            "reference" => $order->order_number,
            "narrative_text" => "Just an order",
            "successurl" => Url::toRoute(['/product/checkout/finish-klarna', 'orderNumber' => $order->order_number], true),
            "errorurl" => Url::toRoute(['/product/checkout/checkout-payment-error'], true),
            "backurl" => Url::toRoute(['/product/checkout/checkout-payment-back'], true),
        ];
    }
}