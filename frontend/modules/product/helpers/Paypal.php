<?php

namespace frontend\modules\product\helpers;

use frontend\modules\product\models\Order;
use common\helpers\Payone as CommonPayone;
use yii\helpers\Url;
use common\components\Payone as BasePayone;

/**
 * Class helper for paypal.
 */
class Paypal extends CommonPayone
{
    /**
     * Currency type usd.
     *
     * @var string
     */
    const CURRENCY_EUR = 'EUR';

    public static function createRequest($order, $cart, $shippingInfo)
    {
        $personalData = self::getUserPersonalData($shippingInfo);
        $orderParameters = self::getOrderParameters($order);
        //$items = self::getItems($cart);

        $request = array_merge(self::getDefaults(), $personalData, $orderParameters);

        return BasePayone::sendRequest($request);
    }

    private static function getOrderParameters($order)
    {
        return [
            "request" => "authorization",
            "clearingtype" => "wlt", // wallet clearing type
            "wallettype" => "PPE", // PPE for Paypal
            "amount" => str_replace('.', '', number_format($order->full_sum, 2, '.', '')),
            'currency' => self::CURRENCY_EUR,
            "reference" => $order->order_number,
            "narrative_text" => "Just an order",
            "successurl" => Url::toRoute(['/product/checkout/finish-paypal', 'orderNumber' => $order->order_number], true),
            "errorurl" => Url::toRoute(['/product/checkout/checkout-payment-error'], true),
            "backurl" => Url::toRoute(['/product/checkout/checkout-payment-back'], true),
        ];
    }
}
