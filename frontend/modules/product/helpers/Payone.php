<?php

namespace frontend\modules\product\helpers;

use yii\helpers\Url;
use common\components\Payone as BasePayone;
use common\helpers\Payone as CommonPayone;

class Payone extends CommonPayone
{
    public static function sendPreautorization($order, $cart, $shippingInfo, $pseudocardpan)
    {
        $personalData = self::getUserPersonalData($shippingInfo);
        $creditCard = self::getCreditCard($order, $cart, $pseudocardpan);
        $items = self::getItems($cart);

        $request = array_merge(self::getDefaults(), $personalData, $creditCard, $items);

        return BasePayone::sendRequest($request);
    }

    private static function getCreditCard($order, $cart, $pseudocardpan)
    {
        return [
            "clearingtype" => "cc", // cc for credit card
            "reference" => $order->order_number,
            "amount" => str_replace('.', '', number_format($order->full_sum, 2, '.', '')),
            "currency" => "EUR",
            "request" => "preauthorization",
            "successurl" => Url::toRoute(['/product/checkout/success-payone', 'reference' => $order->order_number], true),
            "errorurl" => Url::toRoute(['/product/checkout/checkout-payment-error'], true),
            "backurl" => Url::toRoute(['/product/checkout/checkout-payment-back'], true),
            "pseudocardpan" => $pseudocardpan // pseudo card pan received from previous checkout steps, no other card details required
        ];
    }
}