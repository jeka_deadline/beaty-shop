<?php

namespace frontend\modules\product\helpers;

use frontend\modules\product\models\forms\CheckoutShippingForm;
use frontend\modules\product\models\forms\CheckoutBillingForm;
use frontend\modules\product\models\forms\CheckoutPaymentForm;

/**
 * Checkout Data Transfer Object to transfer shipping, billing, payment
 */
class CheckoutDTO
{
    /**
     * Shipping information from checkout page.
     *
     * @var \frontend\modules\product\models\forms\CheckoutShippingForm $shippingForm
     */
    private $shippingForm;

    /**
     * Billing information from checkout page.
     *
     * @var \frontend\modules\product\models\forms\CheckoutBillingForm $billingForm
     */
    private $billingForm;

    /**
     * Payment information from checkout page.
     *
     * @var \frontend\modules\product\models\forms\CheckoutPaymentForm $paymentForm
     */
    private $paymentForm;

    /**
     * Set information from checkout page.
     *
     * @param \frontend\modules\product\models\forms\CheckoutShippingForm $shippingForm Shipping information
     * @param \frontend\modules\product\models\forms\CheckoutBillingForm $billingForm Billing information
     * @param \frontend\modules\product\models\forms\CheckoutPaymentForm $paymentForm Payment information
     * @return void
     */
    public function __construct(CheckoutShippingForm $shippingForm, CheckoutBillingForm $billingForm, CheckoutPaymentForm $paymentForm)
    {
        $this->shippingForm = $shippingForm;
        $this->billingForm = $billingForm;
        $this->paymentForm = $paymentForm;
    }

    /**
     * Get shipping information.
     *
     * @return \frontend\modules\product\models\forms\CheckoutShippingForm
     */
    public function getShippingForm()
    {
        return $this->shippingForm;
    }

    /**
     * Get billing information.
     *
     * @return \frontend\modules\product\models\forms\CheckoutBillingForm
     */
    public function getBillingForm()
    {
        return $this->billingForm;
    }

    /**
     * Get payment information.
     *
     * @return \frontend\modules\product\models\forms\CheckoutPaymentForm
     */
    public function getPaymentForm()
    {
        return $this->paymentForm;
    }
}
