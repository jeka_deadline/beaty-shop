<?php

namespace frontend\modules\product\helpers;

use frontend\modules\product\models\Product;

class ProductHelper
{
    public static function getBestSellersProduct($limit = 0)
    {
        return Product::find()
            ->joinWith('bestSellers')
            ->where(['active' => 1])
            ->limit($limit)
            ->all();
    }
}