<?php

namespace frontend\modules\product\helpers;

/**
 * Data Transfer Object class which store product information for cart.
 */
class CartDTO
{
    /**
     * Product variation id.
     *
     * @var int $productVariationId
     */
    public $productVariationId;

    /**
     * Prodict variation name.
     *
     * @var string $productVariationName
     */
    public $productVariationName;

    /**
     * Count some variation product.
     *
     * @var int $count
     */
    public $count;

    /**
     * Price some variation product.
     *
     * @var float $price
     */
    public $price;

    /**
     * Promo price some variation product.
     *
     * @var float $promotionPrice
     */
    public $promotionPrice;

    /**
     * Total price some variation product multiply count.
     *
     * @var float $totalPrice
     */
    public $totalPrice;

    public function getPrice() {
        return $this->promotionPrice?$this->promotionPrice:$this->price;
    }
}
