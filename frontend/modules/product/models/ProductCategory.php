<?php

namespace frontend\modules\product\models;

use common\models\product\ProductCategory as BaseProductCategory;
use common\models\product\ProductCategoryLangField;
use frontend\modules\core\behaviors\LangBehavior;
use frontend\queries\ActiveQuery;
use yii\helpers\Url;
use DateTime;

/**
 * Frontend product category model extends common product category model.
 */
class ProductCategory extends BaseProductCategory
{
    private static $linkShopNow;

    public function behaviors()
    {
        return [
            [
                'class' => LangBehavior::className(),
                'langModel' => ProductCategoryLangField::className(),
                'modelForeignKey' => 'product_category_id',
                'attributes' => [
                    'name', 'description', 'meta_title', 'meta_description', 'meta_keywords',
                ],
            ],
        ];
    }

    public static function getLinkShopNow()
    {
        if (!empty(self::$linkShopNow)) {
            return self::$linkShopNow;
        }

        $category = self::find()
            ->orderBy(['parent_id' => SORT_DESC])
            ->one();

        $url = [];

        while ($category->parent) {
            $url[] = $category->slug;

            $category = $category->parent;
        }

        $url[] = $category->slug;

        self::$linkShopNow = implode('/', array_reverse($url));

        return self::$linkShopNow;
    }

    public static function getChildrenIds($category, $ids = [])
    {
        $ids[ $category->id ] = $category->id;

        if ($category->children) {

            foreach ($category->children as $child) {
                $ids = $ids + call_user_func_array(['self', __METHOD__], [$child, $ids]);
            }
        }

        return  $ids;
    }

    /**
     * Generate list url for sitemap
     *
     * @param string $frequently Value frequently
     * @param string $priority Value priority
     * @return array
     */
    public static function getListItemsForSitemap($frequently, $priority = '0.5')
    {
        $items = [];
        $categories = static::find()
            ->where(['active' => 1])
            ->all();

        foreach ($categories as $category) {
            $dateUpdatePage = new DateTime($category->updated_at);

            $items[] = [
                'loc'         => Url::toRoute([$category->url], true),
                'lastmod'     => $dateUpdatePage->format(DateTime::W3C),
                'changefreq'  => $frequently,
                'priority'    => $priority,
            ];
        }

        return $items;
    }

    /**
     * {@inheritdoc}
     *
     * @return \frontend\queries\ActiveQuery
     */
    public static function find()
    {
        return new ActiveQuery(get_called_class());
    }

    /**
     * Get slug category from string slugs.
     *
     * @param string $slug String list slugs divide slash
     * @return string
     */
    public static function getLastCategorySlug($slug)
    {
        $lastPosSlash = strrpos($slug, '/');

        if ($lastPosSlash === false) {

            return $slug;
        }

        return substr($slug, strrpos($slug, '/') + 1);
    }

    /**
     * Check nesting slugs.
     *
     * @var string $slug String list slugs divide slash
     * @return bool
     */
    public static function checkNestingSlugs($slug)
    {
        $dataSlug = explode('/', $slug);

        $productCategoriesQuery = self::find()
            ->where(['slug' => $dataSlug])
            ->active();

        if (count($dataSlug) <> $productCategoriesQuery->count()) {
            return false;
        }

        $productCategories = $productCategoriesQuery->orderBy(['parent_id' => SORT_ASC])
            ->asArray()
            ->all();

        $fullSlug = [];

        foreach ($productCategories as $category) {
            $fullSlug[] = $category[ 'slug' ];
        }

        if (implode('/', $fullSlug) === $slug) {
            return true;
        }

        return false;
    }

    /**
     * Get full slug with nested categories.
     *
     * @return string
     */
    public function getFullSlug()
    {
        $dataSlug = [$this->slug];

        $parent = $this->parent;

        while ($parent) {
            $dataSlug[] = $parent->slug;

            $parent = $parent->parent;
        }

        return implode('/', array_reverse($dataSlug));
    }

    public function getUrl()
    {
        return '/' . $this->getFullSlug();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryRelationProducts()
    {
        return $this->hasMany(ProductRelationCategorie::className(), ['product_category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['id' => 'product_id'])
            ->viaTable(ProductRelationCategorie::tableName(), ['product_category_id' => 'id']);
    }

    /**
     * Find a category from the list in the branch
     * @param $tree
     * @param $selectIds array
     * @return bool
     */
    public static function findSelectTree($tree, $selectIds) {
        if (!is_array($selectIds)) return false;

        if (in_array($tree['id'], $selectIds)) return true;

        if (isset($tree['children'])) {
            foreach ($selectIds as $id) {
                if (self::findNodeTree($tree['children'], $id)) return true;
            }
        }
        return false;
    }

    public function getParent()
    {
        return $this->hasOne(ProductCategory::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildren()
    {
        return $this->hasMany(static::className(), ['parent_id' => 'id'])->orderBy('display_order');
    }
}
