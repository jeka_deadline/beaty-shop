<?php

namespace frontend\modules\product\models;

use Yii;

class ProductFilter extends \common\models\product\ProductFilter
{
    public function getProductCategorys()
    {
        return $this->hasMany(ProductCategory::className(), ['id' => 'product_category_id'])
            ->viaTable(ProductCategorieRelationFilter::tableName(), ['filter_id' => 'id']);
    }
}
