<?php

namespace frontend\modules\product\models;

use Yii;
use frontend\modules\geo\models\Country;
use common\models\product\OrderBillingInformation as BaseOrderBillingInformation;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * Frontend order billing information extends common order billing information
 */
class OrderBillingInformation extends BaseOrderBillingInformation
{
    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
}
