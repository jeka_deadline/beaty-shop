<?php

namespace frontend\modules\product\models;

use Yii;
use common\models\product\ProductReview as BaseProductReview;
use frontend\modules\user\models\User;

/**
 * Frontend product review model extends common product review model.
 */
class ProductReview extends BaseProductReview
{
    /**
     * Create reviews tree structure.
     *
     * @param \frontend\modules\product\models\ProductReview[] $reviews
     * @return array
     */
    public static function createTree($reviews)
    {
        $nested = [];

        foreach ( $reviews as &$review ) {
            if (!$review[ 'parent_id' ]) {
                $nested[ $review[ 'id' ] ] = &$review;
            } else {
                $pid = $review[ 'parent_id' ];
                if (isset($reviews[ $pid ])) {
                    if (!isset($reviews[ $pid ][ 'children' ])) {
                        $reviews[ $pid ][ 'children' ] = [];
                    }

                    $reviews[ $pid ][ 'children' ][ $review[ 'id' ] ] = &$review;
                }
            }
        }

        return $nested;
    }

    /**
     * Get first letter user surname.
     *
     * @return string
     */
    public function getFirstLetterSurname()
    {
        return mb_substr($this->surname, 0, 1, 'UTF-8');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductPropertie()
    {
        return $this->hasOne(ProductProperties::className(), ['id' => 'product_propertie_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
