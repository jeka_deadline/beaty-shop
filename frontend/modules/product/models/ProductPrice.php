<?php

namespace frontend\modules\product\models;

class ProductPrice extends \common\models\product\ProductPrice
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductVariation()
    {
        return $this->hasOne(ProductVariation::className(), ['id' => 'product_variation_id']);
    }
}
