<?php

namespace frontend\modules\product\models;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class ProductGlobalFilter extends Model
{

    const FILTER_PRICE = 'price';

    public function getFilters($category_id) {
        $filters = [];
        $filters[self::FILTER_PRICE] = $this->createPriceFilter($category_id);
        return $filters;
    }

    private function createPriceFilter($category_id) {
        $modelFilter = new ProductFilter();
        $modelFilter->name = $this->getLabel(self::FILTER_PRICE);
        $modelFilter->type = ProductFilter::TYPE_RANGE;

        $min = 0;
        $max = 0;
        $tableNamePrice = ProductPrice::tableName();
        $tableNameVariation = ProductVariation::tableName();
        $tableNameProduct = Product::tableName();
        $modelProductRelationCategorie = ProductRelationCategorie::find()->where(['product_category_id' => $category_id])->all();
        $rows = (new \yii\db\Query())
            ->select(['min('.$tableNamePrice.'.price) min', 'max('.$tableNamePrice.'.price) max'])
            ->from($tableNamePrice)
            ->innerJoin($tableNameVariation, $tableNamePrice.'.product_variation_id = '.$tableNameVariation.'.id')
            ->innerJoin($tableNameProduct, $tableNameVariation.'.product_id = '.$tableNameProduct.'.id')
            ->where([
                $tableNameVariation.'.active' => 1,
                $tableNameProduct.'.active' => 1,
                $tableNameProduct.'.id' => ArrayHelper::map($modelProductRelationCategorie,'product_id','product_id'),
            ])
            ->one();

        if ($rows) {
            $min = ProductVariation::viewFormatPrice($rows['min']);
            $max = ProductVariation::viewFormatPrice($rows['max']);
        }

        if ((float)$min == (float)$max) {
            $min = 0;
        }

        if (!$max) {
            $max = 99999;
        }

        return [
            'filter' => $modelFilter,
            'listValues' => [
                $min => $min,
                $max => $max
            ],
        ];
    }

    public static function Factory() {
        return new ProductGlobalFilter();
    }

    public function getLabel($name) {
        $list = $this->getAllList();
        return $list[self::FILTER_PRICE];
    }

    public function getAllList() {
        return [
            self::FILTER_PRICE => 'Price',
        ];
    }

    public function getSearchFilters($variationIds) {
        $filters = [];
        $filters[self::FILTER_PRICE] = $this->createSearchPriceFilter($variationIds);
        return $filters;
    }

    private function createSearchPriceFilter($variationIds) {
        $modelFilter = new ProductFilter();
        $modelFilter->name = $this->getLabel(self::FILTER_PRICE);
        $modelFilter->type = ProductFilter::TYPE_RANGE;

        $min = 0;
        $max = 0;

        $price = ArrayHelper::map(
            ProductVariation::find()
                ->with('price', 'promotion')
                ->where(['id' => $variationIds])->all(),
            'id',
            function ($model) {
                return $model->viewFullPrice;
            }
        );

        if (is_array($price)) {
            $min = min($price);
            $max = max($price);
        }

        return [
            'filter' => $modelFilter,
            'listValues' => [
                $min => $min,
                $max => $max
            ],
        ];
    }
}
