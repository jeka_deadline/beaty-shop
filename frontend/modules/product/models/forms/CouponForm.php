<?php

namespace frontend\modules\product\models\forms;

use yii\base\Model;
use frontend\modules\product\models\Coupon;
use Datetime;

/**
 * Coupon form for cart.
 */
class CouponForm extends Model
{
    /**
     * Code coupon.
     *
     * @var string $code
     */
    public $code;

    /**
     * Model coupon.
     *
     * @var \frontend\modules\product\models\Coupon $coupon
     */
    private $coupon;

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return [
            ['code', 'required'],
            ['code', 'exist', 'targetClass' => Coupon::className(), 'targetAttribute' => 'code', 'filter' => function($query) {
                return $query->active();
            }],
            ['code', 'validateDate'],
        ];
    }

    public function validateDate()
    {
        $coupon = $this->getCoupon();

        if (empty($coupon->finish_date)) {
            return;
        }

        $couponDate = new Datetime($coupon->finish_date);
        $currentDate = new Datetime();

        if ($couponDate < $currentDate) {
            $this->addError('code', 'Code ist ungültig.');
        }
    }

    /**
     * Find coupon by code.
     *
     * @return null|\frontend\modules\product\models\Coupon
     */
    public function getCoupon()
    {
        if (!$this->coupon) {
            $this->coupon = Coupon::find()
                ->where(['code' => $this->code])
                ->active()
                ->one();
        }

        return $this->coupon;
    }
}
