<?php

namespace frontend\modules\product\models\forms;

use Yii;
use yii\base\Model;
use frontend\modules\geo\models\Country;
use frontend\modules\geo\models\City;
use yii\helpers\ArrayHelper;
use frontend\modules\user\models\User;

/**
 * Billing form for shop checkout.
 */
class CheckoutBillingForm extends Model
{
    /**
     * User title.
     *
     * @var string $title
     */
    public $title;
    /**
     * User surname.
     *
     * @var string $surname
     */
    public $surname;

    /**
     * User name.
     *
     * @var string $name
     */
    public $name;

    /**
     * User country id.
     *
     * @var int $countryId
     */
    public $countryId;

    /**
     * User city.
     *
     * @var string $city
     */
    public $city;

    /**
     * User company.
     *
     * @var string company
     */
    public $company;

    /**
     * User address 1.
     *
     * @var string $address1
     */
    public $address1;

    /**
     * User address2.
     *
     * @var string $address2
     */
    public $address2;

    /**
     * User zip code.
     *
     * @var string $zip
     */
    public $zip;

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['surname', 'name', 'countryId', 'city', 'address1', 'zip', 'title'], 'required'],
            [['surname', 'name', 'address1', 'address2'], 'trim'],
            //[['zip'], 'match', 'pattern' => '#^\d{5}$#'],
            [['surname', 'name'], 'string', 'max' => 50],
            [['city', 'company'], 'string', 'max' => 255],
            ['title', 'string', 'max' => 10],
            ['countryId', 'exist', 'targetClass' => Country::className(), 'targetAttribute' => 'id'],
            ['address2', 'string'],
            ['zip', 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'zip' => Yii::t('product', 'Zip Code'),
            'countryId' => Yii::t('product', 'Country'),
            'email' => 'e-mail',
            'phone' => Yii::t('product', 'Phone'),
            'city' => Yii::t('product', 'City'),
            'company' => Yii::t('product', 'Company'),
            'name' => Yii::t('product', 'First Name'),
            'surname' => Yii::t('product', 'Last Name'),
        ];
    }

    /**
     * Get hash list countries.
     *
     * @return array
     */
    public function getListCountriesForDropDown()
    {
        return Country::getHashMap('id', 'name');
    }

    /**
     * Get user titles.
     *
     * @return array
     */
    public function getListUserTitles()
    {
        return User::getListUserTitles();
    }

    /**
     * Get country name.
     *
     * @return string.
     */
    public function getCountryName()
    {
        $country = $this->getContry();

        if (!$country) {
            return null;
        }

        return $country->name;
    }

    /**
     * Get human title.
     *
     * @return string|null
     */
    public function getHumanTitle()
    {
        return User::getHumanTitle($this->title);
    }

    /**
     * Get country.
     *
     * @return \frontend\modules\geo\models\Country
     */
    private function getContry()
    {
        return Country::findOne($this->countryId);
    }
}
