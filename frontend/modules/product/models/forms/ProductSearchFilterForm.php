<?php

namespace frontend\modules\product\models\forms;

use frontend\modules\product\models\ProductFilter;
use frontend\modules\product\models\ProductGlobalFilter;
use frontend\modules\product\models\ProductPrice;
use frontend\modules\product\models\ProductPromotion;
use frontend\modules\product\models\ProductVariationPropertie;
use Yii;
use yii\base\Model;
use yii\validators\StringValidator;
use yii\helpers\ArrayHelper;

class ProductSearchFilterForm extends Model
{
    /**
     * Types of sorts.
     */
    const TYPE_SORT_PRICE_ASK = 'price_ask';
    const TYPE_SORT_PRICE_DESK = 'price_desk';
    const TYPE_SORT_NEW = 'new';
    const TYPE_SORT_PROMOTION = 'promotion';
    const TYPE_SORT_RATING = 'rating';

    public $filters;
    public $sort;

    /**
     * Sort by default.
     * @var string
     */
    private $typeSortDefault = self::TYPE_SORT_NEW;

    public function init()
    {
        parent::init();
        $this->sort = $this->typeSortDefault;
    }

    public function rules()
    {
        return [
            [['filters'], 'safe'],
            [['sort'], 'string', 'max' => 20],
            [['filters'], 'each', 'rule' => ['validateCellString']],
        ];
    }

    public function validateCellString($attribute, $params)
    {
        if (is_array($this->$attribute)) {
            foreach ($this->$attribute as $key=>$value) {
                $this->validateString($value);
            }
        } else {
            $this->validateString($this->$attribute);
        }
    }

    public function validateString($attribute,$max = 255)
    {
        $validator = new StringValidator();
        $validator->max = $max;
        if (!$validator->validate($attribute, $error)) {
            $this->addError($attribute, 'Error in the filter parameter.');
        }
    }

    public function formName()
    {
        return '';
    }

    public static function filter($query) {
        $formFilter = new ProductFilterForm();
        $formFilter->load(Yii::$app->request->get());

        $query = self::setSortQuery($query, $formFilter);

        if (empty($formFilter->filters)) {
            return $query;
        }

        if (!$formFilter->validate()) {
            return $query;
        }

        $modelFilters = ProductFilter::find()
            ->where(['id' => array_keys($formFilter->filters)])
            ->indexBy('id')
            ->all();

        $queryFilters = ProductVariationPropertie::find()
            ->with('productVariation');

        $countActiveFilters = 0;
        $variationAllIds = [];
        foreach ($formFilter->filters as $filter_id => $value) {
            if (!$value) continue;
            if (isset($modelFilters[$filter_id])) {
                switch ($modelFilters[$filter_id]->type) {
                    case ProductFilter::TYPE_RANGE:
                        $queryFilters = self::getRange($queryFilters,$filter_id,$value);
                        break;
                    default:
                        $queryFilters->orWhere(['filter_id' => $filter_id, 'value' => $value]);
                }
                $countActiveFilters++;
            } else {
                $variationAllIds = array_merge($variationAllIds, self::setGlobalFilter($filter_id, $value));
            }
        }

        $queryFilters->groupBy(['product_variation_id', 'value']);
        if (!($modelFindVariations = $queryFilters->all())) return $query;

        $variationCounts = [];

        foreach ($modelFindVariations as $modelFindVariation) {
            if (isset($variationCounts[$modelFindVariation->product_variation_id])) {
                $variationCounts[$modelFindVariation->product_variation_id]++;
            } else {
                $variationCounts[$modelFindVariation->product_variation_id]=1;
            }
        }

        $variationIds = [];
        foreach ($variationCounts as $key => $count){
            if ($count == $countActiveFilters) $variationIds[$key] = $key;
        }

        $query->andWhere([
            'variations.id' => $variationIds && $variationAllIds?array_intersect($variationIds, $variationAllIds):array_merge($variationIds, $variationAllIds)
        ]);

        return $query;
    }

    private static function getRange($query, $filter_id, $value)
    {
        if (strpos($value, ',') > 0) {
            $values = explode(',', $value);
            if ((is_integer($values[0]) && is_integer($values[0]))) return $query;
            return $query->orWhere([
                'AND',
                ['filter_id' => $filter_id],
                ['between', 'value', (float)$values[0], (float)$values[1]],
            ]);
        }
        return $query;
    }

    public static function getSortListLabel() {
        return [
            self::TYPE_SORT_NEW => 'New Arrivals',
            self::TYPE_SORT_PROMOTION => 'Best Sellers',
            self::TYPE_SORT_PRICE_ASK => 'Price Low to High',
            self::TYPE_SORT_PRICE_DESK => 'Price High to Low',
            self::TYPE_SORT_RATING => 'Relevance',
        ];
    }

    private static function setSortQuery($query, $formFilter) {
        $sortType = $formFilter->sort;
        switch ($sortType) {
            case self::TYPE_SORT_PRICE_ASK:
                $query->joinWith('price')->orderBy([ProductPrice::tableName().'.price' => SORT_ASC]);
                break;
            case self::TYPE_SORT_PRICE_DESK:
                $query->joinWith('price')->orderBy([ProductPrice::tableName().'.price' => SORT_DESC]);
                break;
            case self::TYPE_SORT_NEW:
                $query->orderBy(['product.created_at' => SORT_DESC]);
                break;
            case self::TYPE_SORT_PROMOTION:
                $query->joinWith('promotion')->orderBy([ProductPromotion::tableName().'.price' => SORT_ASC]);
                break;
            case self::TYPE_SORT_RATING:
                $query->orderBy(['product.rating' => SORT_DESC]);
                break;
        }
        return $query;
    }

    private static function setGlobalFilter($filter_id, $value) {
        $variationIds = [];
        switch ($filter_id) {
            case ProductGlobalFilter::FILTER_PRICE:
                $queryPrice = ProductPrice::find();
                $queryPrice = self::getRangePrice($queryPrice, $value);
                $variationIds = array_merge($variationIds, ArrayHelper::map($queryPrice->all(), 'product_variation_id', 'product_variation_id'));
                break;
        }
        return $variationIds;
    }

    private static function getRangePrice($query, $value)
    {
        if (strpos($value, ',') > 0) {
            $values = explode(',', $value);
            if (!(isset($values[0]) && isset($values[1]))) return $query;
            return $query->orWhere(['between', 'price', (float)$values[0], (float)$values[1]]);
        }
        return $query;
    }
}
