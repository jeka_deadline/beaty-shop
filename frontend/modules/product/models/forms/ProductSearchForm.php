<?php

namespace frontend\modules\product\models\forms;

use Yii;
use yii\base\Model;

class ProductSearchForm extends Model
{
    public $query;

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['query'], 'string'],
            [['query'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'query' => Yii::t('product', 'Search'),
        ];
    }

    public function formName()
    {
        return '';
    }
}
