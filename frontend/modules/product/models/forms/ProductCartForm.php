<?php

namespace frontend\modules\product\models\forms;

use yii\base\Model;
use yii\web\NotFoundHttpException;
use frontend\modules\product\helpers\CartHelper;
use frontend\modules\product\models\ProductVariation;

/**
 * Product form for shop.
 */
class ProductCartForm extends Model
{
    /**
     * Scenario for product to cart.
     *
     * @var string
     */
    const SCENARIO_ADD = 'add';

    /**
     * Count products.
     *
     * @var int $count
     */
    public $count;

    /**
     * Product variation model.
     *
     * @access private
     * @var frontend\modules\product\models\ProductVariation $variation
     */
    private $variation;

    /**
     * {@inheritdoc}
     *
     * @param int $productVariationId
     * @param array $config {@inheritdoc}
     * @return void
     */
    public function __construct($productVariationId, $config = [])
    {
        $variation = ProductVariation::find()
            ->where(['id' => $productVariationId])
            ->with('inventorie', 'product')
            ->one();

        if (!$variation || !$variation->inventorie) {
            throw new NotFoundHttpException('Not found product inventory');
        }

        $this->variation = $variation;
        $this->count = 1;

        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return [
            ['count', 'required', 'on' => self::SCENARIO_ADD],
            ['count', 'integer'],
            ['count', 'validateCount']
        ];
    }

    /**
     * Validate count products
     *
     * @return void
     */
    public function validateCount()
    {
        if ($this->count < 1) {
            $this->addError('count', 'Min count must be 1');

            return false;
        }

        $cart = CartHelper::getCart();

        $variation = $this->variation;

        $variations = ProductVariation::find()
            ->where(['id' => CartHelper::getProductIds()])
            ->andWhere(['<>', 'id', $this->variation->id])
            ->with('product', 'inventorie')
            ->indexBy('id')
            ->all();

        if (!$variation->isProductAnPack()) {
            $currentCount = $this->count;
            if ($variation->isProductSet()) {
                $maxCount = $variation->product->countSet;
            } else {
                $maxCount = $variation->inventorie->count;
            }

            $mainPackProductVariation = $this->variation;


        } else {
            $mainPackProductVariation = $variation->product->mainPackProduct->getProductVariations()->with('inventorie')->one();

            if (!$mainPackProductVariation || !$mainPackProductVariation->inventorie) {
                $this->addError('count', 'Main pack not found');
            }

            $maxCount = $mainPackProductVariation->inventorie->count;

            $currentCount = $this->count * $variation->inventorie->count;

            if (isset($cart[ $mainPackProductVariation->id ])) {
                $currentCount += $cart[ $mainPackProductVariation->id ]->count;
            }
        }

        foreach ($variations as $itemVariation) {
            if (!$itemVariation->isProductAnPack()) {
                continue;
            }

            $mainPackProductVariationItem = $itemVariation->product->mainPackProduct->getProductVariations()->with('inventorie')->one();

            if ($mainPackProductVariationItem->id !== $mainPackProductVariation->id || !$mainPackProductVariationItem || !$mainPackProductVariationItem->inventorie) {
                continue;
            }

            if ($variation->isProductSet()) {
                $inventorieCount = $variation->product->countSet;
            } else {
                $inventorieCount = $variation->inventorie->count;
            }

            $currentCount += $cart[ $itemVariation->id ]->count * $inventorieCount;
        }

        if ($currentCount > $maxCount) {
            $this->addError('count', 'Max count this product is ' . $maxCount);
        }
    }
}
