<?php

namespace frontend\modules\product\models\forms;

use frontend\modules\product\models\Product;
use frontend\modules\product\models\ProductVariation;
use Yii;
use yii\base\Model;
use frontend\modules\product\models\ProductReview;
use frontend\modules\user\models\User;

/**
 * Form for create product review.
 */
class ProductReviewForm extends Model
{
    /**
     * Parent review id.
     *
     * @var int
     */
    public $parentId;

    /**
     * Theme review.
     * @var string
     */
    public $subject;

    /**
     * Rating product.
     * @var float
     */
    public $rating;

    /**
     * Text review.
     *
     * @var string
     */
    public $text;

    /**
     * Product variation id
     *
     * @var int
     */
    private $productId;

    private $accessSetRating;

    /**
     * {@inheritdoc}
     *
     * @param int $productId Product variation id
     * @param array $config
     * @return void
     */
    public function __construct($productId, $config = [])
    {
        $this->productId = $productId;
        $this->parentId = 0;

        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['text', 'subject'], 'string'],
            [['text', 'subject', 'rating'], 'required'],
            [['rating'], 'number'],
            [['parentId'], 'exist', 'targetClass' => ProductReview::className(), 'targetAttribute' => 'id', 'when' => function($model) {
                return !empty($model->parentId);
            }],
        ];
    }

    /**
     * Add product review.
     *
     * @return bool
     */
    public function create()
    {
        if (!$this->validate() && $this->isAccessSetRating()) {
            return false;
        }

        $modelProduct = Product::find()
            ->where(['id' => $this->productId])
            ->one();

        if (!$modelProduct) {
            return false;
        }

        $modelRating = $modelProduct->changeRating(Yii::$app->user->identity->id, $this->rating);

        $user = User::find()
            ->where(['id' => Yii::$app->user->identity->id])
            ->with('profile')
            ->one();

        $model = new ProductReview();
        $model->user_id = $user->id;
        $model->username = ($user->profile) ? $user->profile->name : '';
        $model->surname = ($user->profile) ? $user->profile->surname : '';
        $model->product_id = $this->productId;
        $model->parent_id = $this->parentId;
        $model->subject = $this->subject;
        $model->text = $this->text;
        $model->rating_id = $modelRating->id;

        return ($model->validate() && $model->save());
    }

    /**
     * Get product variation id.
     *
     * @return int
     */
    public function getProductId()
    {
        return $this->productId;
    }

    public function isAccessSetRating() {
        if (!empty($this->accessSetRating)) return $this->accessSetRating;

        if (($modelVariation = ProductVariation::find()
            ->alias('variations')
            ->joinWith('orders orders')
            ->where([
                'orders.user_id' => Yii::$app->user->identity->id,
                'variations.product_id' => $this->productId
            ])
            ->groupBy('variations.product_id')
            ->one()))
        {
            $this->accessSetRating = true;
        } else {
            $this->accessSetRating = false;
        }

        return $this->accessSetRating;
    }

    public function attributeLabels()
    {
        return [
            'subject' => Yii::t('product', 'Subject'),
            'rating' => Yii::t('product', 'Rating'),
            'text' => Yii::t('product', 'Text'),
        ];
    }
}
