<?php

namespace frontend\modules\product\models\forms;

use Yii;
use yii\base\Model;
use frontend\modules\user\models\UserPaymentCard;
use yii\helpers\ArrayHelper;
use frontend\modules\product\models\Order;

/**
 * Payment form for shop checkout.
 */
class CheckoutPaymentForm extends Model
{
    /**
     * Type payment.
     *
     * @var string $type
     */
    public $type;

    /**
     * Pseudocardpan.
     *
     * @var string $type
     */
    public $pseudocardpan;

    /**
     * Number.
     *
     * @var string $type
     */
    public $number;

    /**
     * User saved payment card id.
     *
     * @var int $savedPaymentCardId
     */
    public $savedPaymentCardId;

    /**
     * Check mark if need save card for user.
     *
     * @var string $saveCard
     */
    public $saveCard;

    /**
     * User id.
     *
     * @var string $userId
     */
    private $userId;

    /**
     * {@inheritdoc}
     *
     * @param $userId User id
     * @param null|\frontend\modules\user\models\UserPaymentCard $userSavedPaymentCard User saved payment card
     * @param array $config {@inheritdoc}
     * @return void
     */
    public function __construct($config = [])
    {
        $this->userId = (Yii::$app->user->isGuest) ? null : Yii::$app->user->identity->id;

        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function rules()
    {
        return [
            // rules for type attribute
            ['type', 'required'],
            ['type', 'in', 'range' => [Order::PAYPAL_METHOD, Order::CREDIT_METHOD, Order::KLARNA_METHOD]],
            ['saveCard', 'boolean'],
            ['savedPaymentCardId', 'safe'],
        ];
    }

    /**
     * Get list payment methods.
     *
     * @return array
     */
    public function getPaymentMethods()
    {
        return [
            Order::CREDIT_METHOD => 'Credit card',
            Order::PAYPAL_METHOD => 'PayPal',
            Order::KLARNA_METHOD => 'Klarna',
        ];
    }

    public function getCreditCardMethod()
    {
        return Order::CREDIT_METHOD;
    }

    public function getPaypalMethod()
    {
        return Order::PAYPAL_METHOD;
    }

    public function getKlarnaMethod()
    {
        return Order::KLARNA_METHOD;
    }

    public function getNamePaymentMethod()
    {
        switch ($this->type) {
            case Order::CREDIT_METHOD:
                return $this->getCreditCardMethod();
            case Order::PAYPAL_METHOD:
                return $this->getPaypalMethod();
            case Order::KLARNA_METHOD:
                return $this->getKlarnaMethod();
            default:
                return $this->getCreditCardMethod();
        }
    }

    /**
     * If payment card type is credit card.
     *
     * @return bool
     */
    public function isCreditCardPaymentMethod()
    {
        return $this->type === Order::CREDIT_METHOD;
    }

    /**
     * Get hash list saved payment cards for user.
     *
     * @return array
     */
    public function getUserSavedPaymentCards()
    {
        return ArrayHelper::map(UserPaymentCard::findAllPaymentCardsByUserId($this->userId), 'id', 'name');
    }

    public function attributeLabels()
    {
        return [
            'credit_card' => Yii::t('core', 'Credit or Debit Card'),
            'saveCard' => Yii::t('core', 'Save Credit Card for later use'),
        ];
    }
}
