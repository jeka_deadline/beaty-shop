<?php

namespace frontend\modules\product\models\forms;

use frontend\modules\product\models\Product;
use frontend\modules\product\models\ProductFilter;
use frontend\modules\product\models\ProductGlobalFilter;
use frontend\modules\product\models\ProductPrice;
use frontend\modules\product\models\ProductPromotion;
use frontend\modules\product\models\ProductVariationPropertie;
use function PHPSTORM_META\type;
use Yii;
use yii\base\Model;
use yii\validators\StringValidator;
use yii\helpers\ArrayHelper;

class ProductItemFilterForm extends Model
{
    public $filters;

    public function rules()
    {
        return [
            [['filters'], 'safe'],
            [['filters'], 'each', 'rule' => ['validateCellString']],
        ];
    }

    public function validateCellString($attribute, $params)
    {
        if (is_array($this->$attribute)) {
            foreach ($this->$attribute as $key=>$value) {
                $this->validateString($value);
            }
        } else {
            $this->validateString($this->$attribute);
        }
    }

    public function validateString($attribute,$max = 255)
    {
        $validator = new StringValidator();
        $validator->max = $max;
        if (!$validator->validate($attribute, $error)) {
            $this->addError($attribute, 'Error in the filter parameter.');
        }
    }

    public function formName()
    {
        return '';
    }

    public static function filter($query, $product_id = null) {
        if (!$product_id) return $query;

        $formFilter = new ProductItemFilterForm();
        $formFilter->load(Yii::$app->request->post()?:Yii::$app->request->get());

        if (empty($formFilter->filters)) {
            return $query;
        }

        if (!$formFilter->validate()) {
            return $query;
        }

        $modelFilters = ProductFilter::find()
            ->where(['id' => array_keys($formFilter->filters)])
            ->indexBy('id')
            ->all();

        $queryFilters = ProductVariationPropertie::find()
            ->joinWith('productVariation variation');

        $countActiveFilters = 0;
        foreach ($formFilter->filters as $filter_id => $value) {
            if (!$value) continue;
            switch ($modelFilters[$filter_id]->type) {
                case ProductFilter::TYPE_RANGE:
                    $queryFilters = self::getRange($queryFilters,$filter_id,$value);
                    break;
                default:
                    $queryFilters->orWhere(['filter_id' => $filter_id, 'value' => $value]);
            }
            $countActiveFilters++;
        }

        if (!$countActiveFilters) return $query;

        $queryFilters->andWhere(['variation.product_id' => $product_id])
            ->groupBy(['product_variation_id', 'value']);
        if (!($modelFindVariations = $queryFilters->all())) return $query;

        $variationIds = [];

        foreach ($modelFindVariations as $modelFindVariation) {
            if (isset($variationIds[$modelFindVariation->product_variation_id])) {
                $variationIds[$modelFindVariation->product_variation_id]++;
            } else {
                $variationIds[$modelFindVariation->product_variation_id]=1;
            }
        }

        foreach ($variationIds as $key => $count){
            if ($count != $countActiveFilters) unset($variationIds[$key]);
        }

        $query->andWhere([
            'variations.id' => array_keys($variationIds)
        ]);

        return $query;
    }

    private static function getRange($query, $filter_id, $value)
    {
        if (strpos($value, ',') > 0) {
            $values = explode(',', $value);
            if ((is_integer($values[0]) && is_integer($values[0]))) return $query;
            return $query->orWhere([
                'AND',
                ['filter_id' => $filter_id],
                ['between', 'value', (float)$values[0], (float)$values[1]],
            ]);
        }
        return $query;
    }

    public static function count() {
        $filters = Yii::$app->request->post('filters')?:Yii::$app->request->get('filters');
        if (!is_array($filters)) return 0;
        $count = 0;
        foreach ($filters as $id => $value) {
            if ($value) $count++;
        }
        return $count;
    }

    public static function allCount() {
        $filters = Yii::$app->request->post('filters')?:Yii::$app->request->get('filters');
        if (!is_array($filters)) return 0;
        return count($filters);
    }
}
