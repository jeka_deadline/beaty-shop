<?php

namespace frontend\modules\product\models\forms;

use DateTime;
use frontend\modules\user\models\UserShippingAddress;
use frontend\modules\product\models\forms\CheckoutBillingForm;
use frontend\modules\product\models\OrderShippingInformation;
use Yii;
use yii\helpers\ArrayHelper;
use frontend\modules\core\models\Setting;
use frontend\modules\product\helpers\CartHelper;
use frontend\modules\core\validators\PhoneValidator;
use frontend\modules\geo\models\Country;

/**
 * Shipping form for shop checkout extends checkout billing form.
 */
class CheckoutShippingForm extends CheckoutBillingForm
{
    /**
     * User saved shipping address id.
     *
     * @var int $savedShippingAddressId
     */
    public $savedShippingAddressId;

    /**
     * User phone.
     *
     * @var string $phone
     */
    public $phone;

    /**
     * User email.
     *
     * @var string $email
     */
    public $email;

    /**
     * Check mark if user checkout shipping information use equal checkout billing information.
     *
     * @var bool $useShippingAddressForBilling
     */
    public $useShippingAddressForBilling;

    /**
     * Check mark if need save this address for current user.
     *
     * @var bool $saveAddress
     */
    public $saveAddress;

    /**
     * Shipping method.
     *
     * @var string $shippingMethod
     */
    public $shippingMethod;

    /**
     * User id.
     *
     * @var int $userId
     */
    private $userId;

    /**
     * {@inheritdoc}
     *
     * @param int $userId User id
     * @param null|\frontend\modules\user\models\UserShippingAddress $userSavedShippingAddress User saved address model
     * @param array $config {@interitdoc}
     * @return void
     */
    public function __construct($userId, UserShippingAddress $userSavedShippingAddress = null, $config = [])
    {
        parent::__construct($config);

        $this->userId = $userId;

        if ($userSavedShippingAddress) {
            $this->fillAttributes($userSavedShippingAddress);
        } else {
            $country = Yii::$app->userCountry->getCountry();

            if ($country) {
                $this->countryId = $country->id;
            }
        }
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['email', 'shippingMethod', 'phone'], 'required'],
                ['email', 'trim'],
                ['email', 'email'],
                //['phone', PhoneValidator::className()],
                ['phone', 'safe'],
                [['useShippingAddressForBilling', 'saveAddress',], 'boolean'],
                ['savedShippingAddressId', 'safe'],
                ['shippingMethod', 'in', 'range' => [OrderShippingInformation::SHIPPING_STANDART_METHOD, OrderShippingInformation::SHIPPING_EXPRESS_METHOD, OrderShippingInformation::SHIPPING_PICKUP_METHOD]],
            ]
        );
    }

    /**
     * Get list shipping methods.
     *
     * @return array
     */
    public function getListShippingMethods()
    {
        return [
            OrderShippingInformation::SHIPPING_PICKUP_METHOD => 'Selbstabholer',
            OrderShippingInformation::SHIPPING_STANDART_METHOD => 'Standard',
            OrderShippingInformation::SHIPPING_EXPRESS_METHOD => 'Express',
        ];
    }

    /**
     * Get list shipping methods for other countries.
     *
     * @return array
     */
    public function getListShippingMethodsOtherCountries()
    {
        return [
            OrderShippingInformation::SHIPPING_STANDART_METHOD => 'Standard',
        ];
    }

    /**
     * Get shipping standart method.
     *
     * @return string
     */
    public function getShippingStandartMethod()
    {
        return OrderShippingInformation::SHIPPING_STANDART_METHOD;
    }

    /**
     * Check is shipping country is German.
     *
     * @return bool
     */
    public function isCountryGerman()
    {
        $country = Country::findOne($this->countryId);

        if ($country && (strtolower($country->code) === Country::GERMANY_COUNTRY_CODE)) {
            return true;
        }

        return false;
    }

    /**
     * Get shipping express method.
     *
     * @return string
     */
    public function getShippingExpressMethod()
    {
        return OrderShippingInformation::SHIPPING_EXPRESS_METHOD;
    }

    /**
     * Get shipping pickup method.
     *
     * @return string
     */
    public function getShippingPickupMethod()
    {
        return OrderShippingInformation::SHIPPING_PICKUP_METHOD;
    }

    /**
     * Get shipping standart method price.
     *
     * @return float
     */
    public function getShippingStandartMethodPrice()
    {
        $country = Country::findOne($this->countryId);

        if (!$country) {
            $country = Country::find()
                ->where(['like', 'code', Country::GERMANY_COUNTRY_CODE])
                ->one();
        }

        if (!$country) {
            return OrderShippingInformation::SHIPPING_PRICE_STANDART_METHOD;
        }

        // shipping for GERNAMY
        if (strtolower($country->code) === Country::GERMANY_COUNTRY_CODE) {
            $totalPrice = CartHelper::getTotalPrice();
            $freeShippingSum = Setting::getFreeShippingSettingValue();

            if (is_null($freeShippingSum) || $totalPrice < $freeShippingSum) {
                return OrderShippingInformation::SHIPPING_PRICE_STANDART_METHOD;
            }

            return 0;
        }

        $totalPrice = CartHelper::getTotalPrice();

        // shipping for SWITZERLAND
        if (strtolower($country->code) === Country::SWITZERLAND_COUNTRY_CODE) {
            $freeShippingSumForOtherCountries = Setting::getFreeShippingSettingForSwitzerlandValue();
            if (is_null($freeShippingSumForOtherCountries) || $totalPrice < $freeShippingSumForOtherCountries) {
                return OrderShippingInformation::SHIPPING_PRICE_SWITZERLAND_STANDART_METHOD;
            }

            return 0;

        }

        $freeShippingSumForOtherCountries = Setting::getFreeShippingSettingForOtherCountriesValue();

        // shipping for Europe
        if (is_null($freeShippingSumForOtherCountries) || $totalPrice < $freeShippingSumForOtherCountries) {
            return OrderShippingInformation::SHIPPING_PRICE_EU_COUNTRIES_STANDART_METHOD;
        }

        return 0;

    }

    /**
     * Get shipping express method price.
     *
     * @return float
     */
    public function getShippingExpressMethodPrice()
    {
        return OrderShippingInformation::SHIPPING_PRICE_EXPRESS_METHOD;
    }

    /**
     * Get shipping pickup method price.
     *
     * @return float
     */
    public function getShippingPickupMethodPrice()
    {
        return OrderShippingInformation::SHIPPING_PRICE_PICKUP_METHOD;
    }

    /**
     * Get shipping price by method.
     *
     * @return float
     */
    public function getShippingPriceByMethod()
    {
        switch ($this->shippingMethod) {
            case OrderShippingInformation::SHIPPING_STANDART_METHOD:
                return $this->getShippingStandartMethodPrice();
            case OrderShippingInformation::SHIPPING_EXPRESS_METHOD:
                return $this->getShippingExpressMethodPrice();
            case OrderShippingInformation::SHIPPING_PICKUP_METHOD:
                return $this->getShippingPickupMethodPrice();
            default:
                return $this->getShippingStandartMethodPrice();
        }
    }

    /**
     * Get shipping method name.
     *
     * @return string
     */
    public function getShippingMethodName()
    {
        switch ($this->shippingMethod) {
            case OrderShippingInformation::SHIPPING_STANDART_METHOD:
                return 'Standard';
            case OrderShippingInformation::SHIPPING_EXPRESS_METHOD:
                return 'Express';
            case OrderShippingInformation::SHIPPING_PICKUP_METHOD:
                return 'Selbstabholer';
            default:
                return 'Standart';
        }
    }

    public function getShippingMethodInfo()
    {
        switch ($this->shippingMethod) {
            case OrderShippingInformation::SHIPPING_STANDART_METHOD:
                return Yii::t('shipping', 'Fast delivery about 1-3 working days');
            case OrderShippingInformation::SHIPPING_EXPRESS_METHOD:
                return Yii::t('shipping', 'Delivery the next working day when ordering until 15:00');
            case OrderShippingInformation::SHIPPING_PICKUP_METHOD:
                return Yii::t('shipping', 'Pick up the next working day from 10am to 6pm in our studio. Kaiserstrasse 59, 44135 Dortmund');
            default:
                return Yii::t('shipping', 'Fast delivery about 1-3 working days');
        }
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(
            parent::attributeLabels(),
            [
                'savedShippingAddressId' => Yii::t('product', 'Address'),
                'useShippingAddressForBilling' => Yii::t('product', 'Use this address for billing'),
                'shippingMethod' => Yii::t('core', 'select shipping speed'),
                'saveAddress' => Yii::t('core', 'Save Address for later use'),
            ]
        );
    }

    /**
     * Get hash list user saved addresses.
     *
     * @return array
     */
    public function getUserSavedAddresses()
    {
        return ArrayHelper::map(UserShippingAddress::findAllAddressesForUser($this->userId), 'id', 'name_address');
    }

    /**
     * Get date shipping by shipping method.
     *
     * @return string
     */
    public function getDateShippingByMethod()
    {
        $date = new DateTime();

        switch ($this->shippingMethod) {
            case OrderShippingInformation::SHIPPING_STANDART_METHOD:
                $date->modify('+5days');
                break;
            case OrderShippingInformation::SHIPPING_EXPRESS_METHOD:
                $date->modify('+3days');
                break;
            default:
                $date->modify('+5days');
                break;
        }

        $dayName = Yii::t('core', $date->format('l'));
        $monthName = Yii::t('core', $date->format('F'));

        return sprintf('%s, %s %sth', $dayName, $monthName, $date->format('j'));
    }

    /**
     * Get date shipping by standart method.
     *
     * @return string
     */
    public function getShippingStandartMethodDate()
    {
        $date = new DateTime();
        $date->modify('+5days');

        return $date->format('d/m/y');
    }

    /**
     * Get date shipping by express method.
     *
     * @return string
     */
    public function getShippingExpressMethodDate()
    {
        $date = new DateTime();
        $date->modify('+3days');

        return $date->format('d/m/y');
    }

    /**
     * Get phone mask.
     *
     * @return string
     */
    public function getPhoneMask()
    {
        return PhoneValidator::getGermanPhoneMask();
    }

    /**
     * Fill attributes with user saved shipping information.
     *
     * @param \frontend\modules\user\models\UserShippingAddress $userSavedShippingAddress Model user saved shipping address
     * @return void
     */
    private function fillAttributes($userSavedShippingAddress)
    {
        $this->savedShippingAddressId = $userSavedShippingAddress->id;
        $this->title                  = $userSavedShippingAddress->title;
        $this->surname                = $userSavedShippingAddress->surname;
        $this->name                   = $userSavedShippingAddress->name;
        $this->countryId              = $userSavedShippingAddress->country_id;
        $this->city                   = $userSavedShippingAddress->city;
        $this->address1               = $userSavedShippingAddress->address1;
        $this->address2               = $userSavedShippingAddress->address2;
        $this->zip                    = $userSavedShippingAddress->zip;
        $this->phone                  = $userSavedShippingAddress->phone;
        $this->email                  = $userSavedShippingAddress->email;
    }
}
