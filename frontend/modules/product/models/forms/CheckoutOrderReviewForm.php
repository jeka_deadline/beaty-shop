<?php

namespace frontend\modules\product\models\forms;

use Yii;
use yii\base\Model;

/**
 * Form for page checkout page block order review.
 */
class CheckoutOrderReviewForm extends Model
{
/**
     * Field data protection..
     *
     * @var bool $dataProtection
     */
    public $dataProtection;

    /**
     * Field agb.
     *
     * @var bool $agb
     */
    public $agb;

    /**
     * Field newsletter.
     *
     * @var bool $newsletter
     */
    public $newsletter;

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['dataProtection', 'agb', 'newsletter'], 'boolean'],
            ['dataProtection', 'compare', 'compareValue' => 1, 'operator' => '===', 'type' => 'number'],
            ['agb', 'compare', 'compareValue' => 1, 'operator' => '===', 'type' => 'number', 'message' => Yii::t('core', 'The checkbox must be checked')],
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'dataProtection' => 'Dateschutz',
            'agb' => 'AGB',
            'newsletter' => 'Newsletter abonnieren',
        ];
    }
}
