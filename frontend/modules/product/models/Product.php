<?php

namespace frontend\modules\product\models;

use frontend\modules\core\behaviors\LangBehavior;
use frontend\queries\ActiveQuery;
use yii\helpers\ArrayHelper;
use DateTime;
use yii\helpers\Url;

/**
 * Frontend product model extends common product model.
 */
class Product extends \common\models\product\Product
{
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                [
                    'class' => LangBehavior::className(),
                    'langModel' => ProductLangField::className(),
                    'modelForeignKey' => 'product_id',
                    'attributes' => [
                        'meta_title',
                        'meta_description',
                        'meta_keywords',
                    ]
                ],
            ]
        );
    }

    /**
     * {@inheritdoc}
     *
     * @return \frontend\queries\ActiveQuery
     */
    public static function find()
    {
        return new ActiveQuery(get_called_class());
    }

    /**
     * Get first active variation product from master.
     *
     * @return null|\frontend\modules\product\models\ProductVariation
     */
    public function getFirstProductVariation()
    {
        return $this->getDefaultProductVariation()
            ->active()
            ->one();
    }

    /**
     * Check if user is appreciated product.
     *
     * @param int $userId User id
     * @return bool
     */
    public function isUserVoiceRating($userId)
    {
        $productUserRating = ProductRating::find()
            ->where(['user_id' => $userId, 'product_id' => $this->id])
            ->one();

        if ($productUserRating) {
            return true;
        }

        return false;
    }

    /**
     * Check is product have a moderate reviews.
     *
     * @return bool
     */
    public function hasModerateRewiews()
    {
        $count = $this->getReviews()
            ->where(['active' => 1, 'parent_id' => 0])
            ->count();

        return ($count) ? true : false;
    }

    /**
     * Get product slug with categories slug's by category id.
     *
     * @param int $categoryId Category id
     * @return string
     */
    public function getFullSlugWithCategoriesByCategoryId($categoryId)
    {
        $slug = '';

        $productCategory = ProductCategory::findOne($id);

        if ($productCategory) {
            $slug = $productCategory->getFullSlug();
        }

        return '/' . $slug . $this->slug;
    }

    /**
     * Find active product by id.
     *
     * @param int $id Product id
     * @return null|\frontend\modules\product\models\Product
     */
    public static function findActiveProductById($id)
    {
        return static::find()
            ->with('productVariations')
            ->where(['id' => $id])
            ->active()
            ->one();
    }

    /**
     * Change master product rating.
     *
     * @param int $userId User id
     * @param float $value Product rating value
     * @return float Average product rating
     */
    public function changeRating($userId, $value)
    {
        $productUserRating = new ProductRating();
        $productUserRating->user_id = $userId;
        $productUserRating->value = $value;
        $productUserRating->product_id = $this->id;

        $productUserRating->save();

        $average = ProductRating::find()
            ->where([
                'product_id' => $this->id,
                'active' => 1
            ])
            ->average('value');

        self::updateAttributes(['rating' => $average], ['id' => $this->id]);

        return $productUserRating;
    }

    public function getCountReviews() {
        return ProductRating::find()->where([
            'product_id' => $this->id,
            'active' => 1
        ])->count();
    }

    /**
     * Generate list url for sitemap
     *
     * @param string $frequently Value frequently
     * @param string $priority Value priority
     * @return array
     */
    public static function getListItemsForSitemap($frequently, $priority = '0.5')
    {
        $items = [];
        $products = static::find()
            ->where(['active' => 1])
            ->all();

        foreach ($products as $product) {
            $dateUpdatePage = new DateTime($product->updated_at);

            $items[] = [
                'loc'         => Url::toRoute(['/product/product/index', 'slug' => $product->slug], true),
                'lastmod'     => $dateUpdatePage->format(DateTime::W3C),
                'changefreq'  => $frequently,
                'priority'    => $priority,
            ];
        }

        return $items;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductVariations()
    {
        return $this->hasMany(ProductVariation::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBestSeller()
    {
        return $this->hasOne(ProductBestSeller::className(), ['product_id' => 'id']);
    }

    public function getMainPackProduct()
    {
        return $this->hasMany(Product::className(), ['id' => 'product_id'])
            ->viaTable(Pack::tableName(), ['product_pack_id' => 'id'])
            ->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFreeRecommendet()
    {
        return $this->hasOne(ProductFreeRecommendet::className(), ['product_recommended_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDefaultProductVariation()
    {
        return $this->hasOne(ProductVariation::className(), ['id' => 'default_variation_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActiveProductVariations()
    {
        return $this->hasMany(ProductVariation::className(), ['product_id' => 'id'])
            ->where(['active' => 1]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductRelationCategories()
    {
        return $this->hasMany(ProductRelationCategorie::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductCategories()
    {
        return $this->hasMany(ProductCategory::className(), ['id' => 'product_category_id'])
            ->viaTable(ProductRelationCategorie::tableName(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviews()
    {
        return $this->hasMany(ProductReview::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecommendets()
    {
        return $this->hasMany(Product::className(), ['id' => 'product_recommended_id'])
            ->viaTable(ProductRecommendet::tableName(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAllPropertis()
    {
        return $this->hasMany(ProductVariationPropertie::className(), ['product_variation_id' => 'id'])
            ->viaTable(ProductVariation::tableName(), ['product_id' => 'id'], function ($query) {
                $query->where(['active' => 1]);
            });
    }

    public function getAllPropertisCountNotNull()
    {
        return $this->hasMany(ProductVariationPropertie::className(), ['product_variation_id' => 'id'])
            ->viaTable(ProductVariation::tableName(), ['product_id' => 'id'], function ($query) {
                $query->leftJoin('product_inventories inventories', 'product_variations.id = inventories.product_variation_id')
                    ->where(['active' => 1])
                    ->andWhere(['>', 'inventories.count', 0])
                    ->andWhere(['IS NOT', 'inventories.count', null]);
            });
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventories()
    {
        return $this->hasMany(ProductInventorie::className(), ['product_variation_id' => 'id'])
            ->viaTable(ProductVariation::tableName(), ['product_id' => 'id'], function ($query) {
                $query->where(['active' => 1]);
            });
    }

    public function getProductSetVariationWithData() {
        return $this
            ->hasMany(ProductSetVariation::className(), ['product_set_id' => 'id'])
            ->with('productVariation')
            ->orderBy(['display_order' => SORT_ASC]);
    }

    public function getCountSet() {
        $count = 0;
        switch ($this->type) {
            case Product::TYPE_PRODUCT_SET:
                $count = $this->getMinCountSet();
                break;
            case Product::TYPE_PRODUCT_EDIT_SET:
                break;
        }
        return $count;
    }

    private function getMinCountSet() {
        $minCount = PHP_INT_MAX;
        $modelSetVariations = ProductSetVariation::find()
            ->with('productVariation')
            ->where(['product_set_id' => $this->id])
            ->all();
        if ($modelSetVariations) {
            foreach ($modelSetVariations as $setVariation) {
                if (!(($productVariation = $setVariation->productVariation) && $productVariation->inventorie)) continue;
                $count = floor($productVariation->inventorie->count/$setVariation->count);
                $minCount = ($minCount>$count)?$count:$minCount;
            }
        }
        return $minCount;
    }
}
