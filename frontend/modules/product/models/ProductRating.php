<?php

namespace frontend\modules\product\models;

use common\models\product\ProductRating as BaseProductRating;
use frontend\modules\user\models\User;
use yii\behaviors\TimestampBehavior;
use \yii\db\Expression;

/**
 * Frontend product rating model extends common product rating model.
 */
class ProductRating extends BaseProductRating
{
    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' =>TimestampBehavior::className(),
                'value' => new Expression('NOW()')
            ]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
