<?php

namespace frontend\modules\product\models;

use Yii;
use common\models\product\OrderProductVariation as BaseOrderProductVariation;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * Frontend order product variation extends common order product variation
 */
class OrderProductVariation extends BaseOrderProductVariation
{
    private $fullprice = null;

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductVariation()
    {
        return $this->hasOne(ProductVariation::className(), ['id' => 'product_variation_id']);
    }

    public function getFullPrice() {
        if ($this->fullprice!=null) return $this->fullprice;

        $obj = new \stdClass();
        $obj->price = $this->price?:null;
        $obj->promotion = $this->promotion?:null;
        $obj->finalprice = $obj->promotion?:$obj->price;

        $this->fullprice = $obj;

        return $this->fullprice;
    }

    public function getViewFullPrice() {
        return $this->fullPrice->finalprice;
    }
}
