<?php

namespace frontend\modules\product\models;

use frontend\modules\core\behaviors\LangBehavior;
use frontend\queries\ActiveQuery;
use frontend\modules\product\helpers\CartDTO;
use yii\helpers\ArrayHelper;
use frontend\modules\core\models\Setting;
use frontend\modules\product\helpers\CartHelper;

/**
 * Frontend product variation model extends common product variation model.
 */
class ProductVariation extends \common\models\product\ProductVariation
{
    private $fullprice = null;

    /**
     * Url placeholders.
     *
     * @var string
     */
    const URL_PLACEHOLDERS = 'http://via.placeholder.com/';

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                [
                    'class' => LangBehavior::className(),
                    'langModel' => ProductVariationLangField::className(),
                    'modelForeignKey' => 'product_variation_id',
                    'attributes' => [
                        'name',
                        'description',
                        'short_description',
                    ],
                ]
            ]
        );
    }

    /**
     * {@inheritdoc}
     *
     * @return \frontend\queries\ActiveQuery
     */
    public static function find()
    {
        return new ActiveQuery(get_called_class());
    }

    /**
     * Get product variation image preview.
     *
     * @param int $width Width preview placeholder.
     * @param int $height Width preview placeholder.
     * @return string
     */
    public function getImagePreview($width = 100, $height = 100)
    {
        return ($this->images) ? $this->images[ 0 ]->getUrlImage() : self::URL_PLACEHOLDERS . "{$width}x{$height}";
    }

    /**
     * Check if variation has preview.
     *
     * @return bool
     */
    public function isHasPreview()
    {
        if ($this->images) {
            return true;
        }

        return false;
    }

    /**
     * Get variation preview without placeholder.
     *
     * @return string|null
     */
    public function getPreviewWithoutPlaceholder()
    {
        if ($this->isHasPreview()) {
            return $this->images[ 0 ]->getUrlImage();
        }

        return null;
    }

    /**
     * Generate Data transfer object for cart.
     *
     * @param int $count Count porducts
     * @return \frontend\modules\product\helpers\CartDTO
     */
    public function generateStructureToCart($id, $count)
    {
        $price = ($this->promotion) ? $this->promotion->price : $this->price->price;

        $cartDTO = new CartDTO();

        $cartDTO->productVariationId = $id;
        $cartDTO->productVariationName = $this->name;
        $cartDTO->count = $count;
        $cartDTO->price = self::viewFormatPrice($this->price->price);
        $cartDTO->promotionPrice = ($this->promotion) ? self::viewFormatPrice($this->promotion->price) : null;

        $cartDTO->totalPrice = self::viewFormatPrice($price * $count);

        return $cartDTO;
    }

    public function getMaxCountForCart()
    {
        $product = CartHelper::getProductById($this->id);

        // Товар не находится в корзине
        if (!$product) {
            // обработка пака
            if ($this->isProductAnPack()) {
                $mainPackProductVariation = $this->product->mainPackProduct->getProductVariations()->with('inventorie')->one();

                if (!$mainPackProductVariation || !$mainPackProductVariation->inventorie) {
                    return 0;
                }

                if ($mainPackProductVariation->inventorie->count) {
                    return floor($mainPackProductVariation->inventorie->count / $this->inventorie->count);
                }

                return 0;
            }

            if ($this->isProductSet()) {
                return $this->product->countSet;
            }

            return $this->inventorie->count;
        // Товар находится в корзине
        } else {
            // обработка пака
            if ($this->isProductAnPack()) {
                $mainPackProductVariation = $this->product->mainPackProduct->getProductVariations()->with('inventorie')->one();

                if (!$mainPackProductVariation || !$mainPackProductVariation->inventorie) {
                    return 0;
                }

                if ($mainPackProductVariation->inventorie->count) {
                    return floor($mainPackProductVariation->inventorie->count / $this->inventorie->count) - $product->count;
                }

                return 0;
            }

            if ($this->isProductSet()) {
                return $this->product->countSet - $product->count;
            }

            $count = $this->inventorie->count - $product->count;

            if ($count) {
                return $count;
            }

            return 0;
        }
    }

    public function isProductSet() {
        return $this->product->isProductSet();
    }

    /**
     * Get product other variations.
     *
     * @return \frontend\modules\product\models\ProductVariations[]
     */
    public function getOtherProductVariations()
    {
        return static::find()
            ->where(['product_id' => $this->product_id])
            ->andWhere(['<>', 'id', $this->id])
            ->active()
            ->all();
    }

    public function getIdForCart()
    {
        return str_pad($this->id, 10, 0, STR_PAD_LEFT);
    }

    /**
     * Find active product variations by ids.
     *
     * @param int[] Product variation ids
     * @return \frontend\modules\product\models\ProductVariations[]
     */
    public static function getActiveProductVariationsByIds($ids)
    {
        return static::find()
            ->with('product')
            ->where(['id' => $ids])
            ->active()
            ->all();
    }

    public function getFullPrice() {
        if ($this->fullprice!=null) return $this->fullprice;

        $obj = new \stdClass();
        $obj->price = $this->price?$this->price->price:null;
        $obj->promotion = $this->promotion?$this->promotion->price:null;
        $obj->finalprice = $obj->promotion?$obj->promotion:$obj->price;

        $this->fullprice = $obj;

        return $this->fullprice;
    }

    /**
     * Check if product has pack item
     *
     * @return bool
     */
    public function isProductAnPack()
    {
        return $this->product->isAnPack();
    }

    /**
     * Check is this product variation has double price.
     *
     * @return bool
     */
    public function isHasDoublePrice()
    {
        $count = ProductPromotion::find()
            ->joinWith(['productVariation variation', 'productVariation.product masterProduct'])
            ->where([
                'masterProduct.id' => $this->product_id,
                'variation.active' => 1,
            ])
            ->groupBy('price')
            ->count();

        if ($count > 1) {
            return true;
        }

        $count = ProductPrice::find()
            ->joinWith(['productVariation variation', 'productVariation.product masterProduct'])
            ->where([
                'masterProduct.id' => $this->product_id,
                'variation.active' => 1,
            ])
            ->groupBy('price')
            ->count();

        if ($count > 1) {
            return true;
        }

        return false;
    }

    /**
     * Get double price for products.
     *
     * @return string
     */
    public function getDoublePrice()
    {
        $count = ProductPromotion::find()
            ->joinWith(['productVariation variation', 'productVariation.product masterProduct'])
            ->where([
                'masterProduct.id' => $this->product_id,
                'variation.active' => 1,
            ])
            ->groupBy('price')
            ->count();

        if ($count > 1) {
            $query = ProductPromotion::find()
                ->joinWith('productVariation.product masterProduct')
                ->where(['masterProduct.id' => $this->product_id])
                ->groupBy('price');

            $min = $query->min('price');
            $max = $query->max('price');
        } else {
            $query = ProductPrice::find()
                ->joinWith('productVariation.product masterProduct')
                ->where(['masterProduct.id' => $this->product_id])
                ->groupBy('price');

            $min = $query->min('price');
            $max = $query->max('price');
        }

        $currency = Setting::value('product.shop.currency');

        return $currency . self::viewFormatPrice($min) . ' - ' . $currency . self::viewFormatPrice($max);
    }

    public function getViewFullPrice() {
        return $this->fullPrice->finalprice;
    }

    public function getViewNumberFormatFullPrice() {
        return self::viewFormatPrice($this->viewFullPrice);
    }

    public static function viewFormatPrice($value) {
        return number_format($value,2, '.', '');
    }

    public function getPriceDiscount() {
        return $this->price?$this->price->price:0;
    }

    public function getIsDiscount() {
        return $this->promotion && $this->promotion->price;
    }

    // =============================================================== Relations ========================================================

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPreview()
    {
        return $this->hasOne(ProductImage::className(), ['product_variation_id' => 'id'])->orderBy(['sort' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImages()
    {
        return $this->hasMany(ProductImage::className(), ['product_variation_id' => 'id'])->orderBy(['sort' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFreeRecommendets()
    {
        return $this->hasMany(ProductFreeRecommendet::className(), ['product_recommended_id' => 'id'])
            ->viaTable(Product::tableName(), ['default_variation_id' => 'id']);
    }

    public function getProperties()
    {
        return $this->hasMany(ProductVariationPropertie::className(), ['product_variation_id' => 'id'])->groupBy('filter_id');
    }

    public function getWithProperties()
    {
        return $this->hasMany(ProductVariationPropertie::className(), ['product_variation_id' => 'id'])->groupBy('filter_id');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['id' => 'order_id'])
            ->viaTable(OrderProductVariation::tableName(), ['product_variation_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventorie()
    {
        return $this->hasOne(ProductInventorie::className(), ['product_variation_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrice()
    {
        return $this->hasOne(ProductPrice::className(), ['product_variation_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromotion()
    {
        return $this->hasOne(ProductPromotion::className(), ['product_variation_id' => 'id']);
    }
}
