<?php

namespace frontend\modules\product\models;

use backend\modules\product\models\forms\UploadImagesProductVariationForm;

class ProductImage extends \common\models\product\ProductImage
{
    public function getUrlImage() {
        return '/' . self::$path . $this->filename;
    }
}
