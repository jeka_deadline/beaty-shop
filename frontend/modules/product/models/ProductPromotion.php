<?php

namespace frontend\modules\product\models;


class ProductPromotion extends \common\models\product\ProductPromotion
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductVariation()
    {
        return $this->hasOne(ProductVariation::className(), ['id' => 'product_variation_id']);
    }

}
