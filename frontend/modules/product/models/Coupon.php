<?php

namespace frontend\modules\product\models;

use common\models\product\Coupon as BaseCoupon;
use frontend\queries\ActiveQuery;

/**
 * Frontend coupon model extends common coupon model.
 */
class Coupon extends BaseCoupon
{
    /**
     * {@inheritdoc}
     *
     * @return \frontend\queries\ActiveQuery
     */
    public static function find()
    {
        return new ActiveQuery(get_called_class());
    }
}
