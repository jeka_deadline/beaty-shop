<?php

namespace frontend\modules\product\models;

use Yii;
use DateTime;
use frontend\modules\user\models\User;
use common\models\product\Order as BaseOrder;
use frontend\modules\product\helpers\CheckoutDTO;
use frontend\modules\product\models\OrderShippingInformation;
use frontend\modules\product\models\OrderBillingInformation;
use yii\behaviors\TimestampBehavior;
use frontend\modules\product\models\ProductVariation;
use yii\db\Expression;
use frontend\modules\user\models\UserShippingAddress;
use frontend\modules\user\models\UserPaymentCard;
use frontend\modules\core\models\EmailTemplate;
use kartik\mpdf\Pdf;
use frontend\modules\product\helpers\CartHelper;
use frontend\modules\product\models\forms\CheckoutPaymentForm;
use frontend\modules\core\models\Setting;

/**
 * Frontend order model extends common order model.
 */
class Order extends BaseOrder
{
    public static $keyCrypt = 'mYCjdT0UPF';

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * Create new order.
     *
     * @static
     * @param \frontend\modules\product\helpers\CheckoutDTO $checkoutDto Checkout Data Transfer Object with checkout form information from page checkout
     * @param \frontend\modules\product\helpers\CheckoutDTO[] $cart Cart for current user
     * @param float $totalSum Total sum
     * @param int|null $userId User id if user is not guest.
     * @return bool|static
     */
    public static function createNewOrder(CheckoutDTO $checkoutDto, $cart, $totalSum, $totalPriceWithCoupon, $coupon, $userId = null)
    {
        $checkoutPayment = $checkoutDto->getPaymentForm();
        $checkoutShippingForm = $checkoutDto->getShippingForm();

        $order = new static();
        $order->user_id = $userId;
        $order->status = self::ORDER_STATUS_NEW;
        $order->type_payment = $checkoutPayment->type;
        $order->total_sum = ProductVariation::viewFormatPrice($totalSum);
        $order->shipping_price = $checkoutShippingForm->getShippingPriceByMethod();
        $order->tax = ProductVariation::viewFormatPrice((float)$totalPriceWithCoupon / 100 * OrderShippingInformation::TAX_PERCENT);
        $order->sum_discount = (CartHelper::hasCoupon()) ? ProductVariation::viewFormatPrice($totalSum - $totalPriceWithCoupon) : 0;
        $order->full_sum = ProductVariation::viewFormatPrice((float)$totalPriceWithCoupon + $order->shipping_price + $order->tax);

        if ($coupon) {
            $order->coupon_code = $coupon->code;
        }

        $order->generateOrderNumber();

        $transaction = Yii::$app->db->beginTransaction();

        if (!$order->validate() || !$order->save()) {
            $transaction->rollback();

            return false;
        }

        $checkoutShippingForm    = $checkoutDto->getShippingForm();
        $orderShipping           = self::createOrderShippingModel($checkoutShippingForm);
        $orderShipping->order_id = $order->id;

        $validate = $orderShipping->validate() && $orderShipping->save();

        // save user address if user check mark saved address on form shipping
        if ($userId && $checkoutShippingForm->saveAddress) {
            $validate = UserShippingAddress::saveAddressFromCheckout($checkoutShippingForm, $userId) && $validate;
        }

        if (!$validate) {
            $transaction->rollback();

            return false;
        }

        $orderProductVariationIds = array_keys($cart);

        $productVariations = ProductVariation::find()
            ->where(['id' => $orderProductVariationIds])
            ->all();

        $orderProductVariations = self::createOrderProductVariations($productVariations, $cart);

        foreach ($orderProductVariations as $orderProductVariation) {
            $orderProductVariation->order_id = $order->id;

            if (!$orderProductVariation->validate() || !$orderProductVariation->save()) {
                $transaction->rollback();

                return false;
            }
        }

        if (!$checkoutShippingForm->useShippingAddressForBilling) {
            $orderBilling = self::createOrderBillingModel($checkoutDto->getBillingForm());
            $orderBilling->order_id = $order->id;

            if (!$orderBilling->validate() || $orderBilling->save()) {
                if (!$orderProductVariation->validate() || !$orderProductVariation->save()) {
                    $transaction->rollback();

                    return false;
                }
            }
        }

        if ($checkoutPayment->saveCard && !UserPaymentCard::savePaymentCardFromCheckout($checkoutPayment, $userId)) {
            $transaction->rollback();

            return false;
        }

        $transaction->commit();

        if ($coupon && empty($coupon->finish_date)) {
            $coupon->active = 0;
            $coupon->save(false);
        }

        return $order;

    }

    /**
     * Set order status paid and change count product variations.
     *
     * @static
     * @param \frontend\modules\product\models\Order $order Order
     * @param \frontend\modules\product\helpers\CartDTO $cart User cart products
     * @param int[] $productVariationIds Product variation ids
     * @return void
     */
    public static function setPaidSuccessfully($order, $cart, $productVariationIds)
    {
        $order->status = self::ORDER_STATUS_PAID;
        $order->save(false);

        $productVariations = ProductVariation::find()
            ->where(['id' => $productVariationIds])
            ->all();

        self::updateProductVariationsCount($productVariations, $cart);
        self::sendUserOrderLetter($order);
        self::sendNotificationForSmallCountProducts($productVariationIds);

        if (Setting::getIsSendAdminLettersForNewOrders()) {
            self::sendAdminLetter($order);
        }
    }

    /**
     * Set order status paid and change count product variations.
     *
     * @static
     * @param \frontend\modules\product\models\Order $order Order
     * @param \frontend\modules\product\helpers\CartDTO $cart User cart products
     * @param int[] $productVariationIds Product variation ids
     * @return void
     */
    public static function setPaidPreautorization($order, $txid, $cart, $productVariationIds)
    {
        $order->status = self::ORDER_STATUS_PREAUTORIZATION;
        $order->credit_txid = $txid;
        $order->save(false);

        $productVariations = ProductVariation::find()
            ->where(['id' => $productVariationIds])
            ->all();

        self::updateProductVariationsCount($productVariations, $cart);
        self::sendUserOrderLetter($order);
        self::sendNotificationForSmallCountProducts($productVariationIds);
        if (Setting::getIsSendAdminLettersForNewOrders()) {
            self::sendAdminLetter($order);
        }
    }

    /**
     * Get orders by user id.
     *
     * @static
     * @param int $userId User id.
     * @return \frontend\modules\product\models\Order[]
     */
    public static function getUserOrders($userId)
    {
        return static::find()
            ->with('orderShippingInformation', 'orderProducts', 'orderProducts.productVariation')
            ->where(['user_id' => $userId])
            ->andWhere(['not in', 'status', [self::ORDER_STATUS_CANCEL, self::ORDER_STATUS_NEW]])
            ->orderBy(['created_at' => SORT_DESC])
            ->all();
    }

    /**
     * Get date shipping by method.
     *
     * @return string
     */
    public function getDateShippingOrder()
    {
        $date = new DateTime($this->created_at);

        switch ($this->orderShippingInformation->shipping_method) {
            case OrderShippingInformation::SHIPPING_STANDART_METHOD:
                $date->modify('+5days');
                break;
            case OrderShippingInformation::SHIPPING_EXPRESS_METHOD:
                $date->modify('+3days');
                break;
            default:
                $date->modify('+5days');
                break;
        }

        return $date->format('Y-m-d H:i:s');
    }

    /**
     * Get human shipping date.
     *
     * @return string
     */
    public function getDateShippingOrderHumanFormat()
    {
        $date = new DateTime($this->getDateShippingOrder());

        return $date->format('l, F j') . 'th';
    }

    /**
     * Get created at date by format.
     *
     * @param string $format Format date.
     * @return string
     */
    public function getCreatedAtFormat($format = 'd.m.Y')
    {
        $date = new DateTime($this->created_at);

        return $date->format($format);
    }

    /**
     * Get pdf number format.
     *
     * @return string
     */
    public function getPdfNumberFormat()
    {
        $date = new DateTime($this->created_at);
        $indexInDate = 0;

        $listRecordsByDate = self::find()
            ->where(['=', 'DATE(created_at)', $date->format('Y-m-d')])
            ->all();

        foreach ($listRecordsByDate as $index => $record) {
            if ($record->id === $this->id) {
              $indexInDate = $index + 1;
            }
        }

        $indexInDate = str_pad($indexInDate, 2, 0, STR_PAD_LEFT);

        return 'IL-' . $date->format('Ymd') . '-' . $indexInDate;
    }

    /**
     * Get pdf user id format.
     *
     * @return string
     */
    public function getPdfUserIdFormat()
    {
        $userId = $this->user_id + 100;

        return str_pad($userId, 6, 0, STR_PAD_LEFT);
    }

    /**
     * Get order human status.
     *
     * @return string
     */
    public function getHumanStatus()
    {
        switch ($this->status) {
            case self::ORDER_STATUS_PREAUTORIZATION:
                return 'Autorisierung';
            case self::ORDER_STATUS_CANCEL:
                return 'Abgebrochen';
            case self::ORDER_STATUS_PAID:
                return 'Bezahlt';
            case self::ORDER_STATUS_DELIVERED:
                return 'Versendet';
            default:
                return '';
        }
    }

    /**
     * Send user order message.
     *
     * @static
     * @param \frontend\modules\product\models\Order $order Order model.
     * @return bool
     */
    public static function sendUserOrderLetter(Order $order)
    {
        $template = EmailTemplate::findTemplateUserOrder();

        if (!$template) {
            return false;
        }

        $text = $template->getTextWithReplacedPlaceholdersUserOrder($order);

        $mailer = Yii::$app->mailer;

        $content = Yii::$app->view->renderFile('@app/modules/product/views/order/generate-pdf.php', ['modelOrder' => $order]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'format' => Pdf::FORMAT_A4,
            'cssFile' => Yii::getAlias('@frontend/web/css/pdf.css'),
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,
            'marginBottom' => 37,
            'options' => ['title' => 'Order Report'],
        ]);

        $api = $pdf->getApi();
        $api->SetFooter(Yii::$app->view->renderFile('@app/modules/product/views/order/pdf-footer.php'));

        $fileName = md5(time()) . '.pdf';
        $filePath = Yii::getAlias('@frontend/runtime/' . $fileName);

        $attachment =  $pdf->output($content, $filePath, Pdf::DEST_FILE);

        $result =  $mailer->compose()
            ->setFrom([$mailer->transport->getUsername() => $template->from])
            ->setTo($order->orderShippingInformation->email)
            ->setHtmlBody($text)
            ->setSubject($template->subject)
            ->attach($filePath)
            ->send();

        unlink($filePath);

        return $result;

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderBillingInformation()
    {
        return $this->hasOne(OrderBillingInformation::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderProducts()
    {
        return $this->hasMany(OrderProductVariation::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderShippingInformation()
    {
        return $this->hasOne(OrderShippingInformation::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Generate order number.
     *
     * @return void
     */
    public function generateOrderNumber()
    {
        $this->order_number = '' . time();
    }

    /**
     * Create order shipping model with shipping information for this order.
     *
     * @static
     * @access private
     * @param \frontend\modules\product\models\forms\CheckoutShippingForm $checkoutShippingForm Shipping form from page checkout
     * @return \frontend\modules\product\models\OrderShippingInformation
     */
    private static function createOrderShippingModel($checkoutShippingForm)
    {
        $orderShipping = new OrderShippingInformation();

        $orderShipping->shipping_method = $checkoutShippingForm->shippingMethod;
        $orderShipping->title           = $checkoutShippingForm->title;
        $orderShipping->surname         = $checkoutShippingForm->surname;
        $orderShipping->name            = $checkoutShippingForm->name;
        $orderShipping->country_id      = $checkoutShippingForm->countryId;
        $orderShipping->city            = $checkoutShippingForm->city;
        $orderShipping->company         = $checkoutShippingForm->company;
        $orderShipping->address1        = $checkoutShippingForm->address1;
        $orderShipping->address2        = $checkoutShippingForm->address2;
        $orderShipping->zip             = $checkoutShippingForm->zip;
        $orderShipping->phone           = str_replace(' ', '', $checkoutShippingForm->phone);
        $orderShipping->email           = $checkoutShippingForm->email;

        return $orderShipping;
    }

    /**
     * Create order product variation model for this order.
     *
     * @static
     * @access private
     * @param \frontend\modules\product\models\ProductVariation[] $productVariations List product variations wich exist in user cart
     * @param \frontend\modules\product\helpers\CartDTO $cart User cart
     * @return \frontend\modules\product\models\ProductVariation[]
     */
    private static function createOrderProductVariations($productVariations, $cart)
    {
        $orderProductVariations = [];

        foreach ($productVariations as $productVariation) {
            $orderProductVariation = new OrderProductVariation();
            $orderProductVariation->product_variation_id = $productVariation->id;
            $orderProductVariation->product_variation_name = $productVariation->name;
            $orderProductVariation->count = $cart[ $productVariation->id ]->count;
            $orderProductVariation->price = $cart[ $productVariation->id ]->price;
            $orderProductVariation->promotion = $cart[ $productVariation->id ]->promotionPrice;

            $orderProductVariations[] = $orderProductVariation;
        }

        return $orderProductVariations;
    }

    /**
     * Create order billing model with billing information for this order.
     *
     * @static
     * @access private
     * @param \frontend\modules\product\models\forms\CheckoutBillingForm $checkoutBillingForm Billing form from page checkout
     * @return \frontend\modules\product\models\OrderBillingInformation
     */
    private static function createOrderBillingModel($checkoutBillingForm)
    {
        $orderBilling = new OrderBillingInformation();

        $orderBilling->title      = $checkoutBillingForm->title;
        $orderBilling->surname    = $checkoutBillingForm->surname;
        $orderBilling->name       = $checkoutBillingForm->name;
        $orderBilling->country_id = $checkoutBillingForm->countryId;
        $orderBilling->city       = $checkoutBillingForm->city;
        $orderBilling->company    = $checkoutBillingForm->company;
        $orderBilling->address1   = $checkoutBillingForm->address1;
        $orderBilling->address2   = $checkoutBillingForm->address2;
        $orderBilling->zip        = $checkoutBillingForm->zip;

        return $orderBilling;
    }

    /**
     * Update product variations inventory.
     *
     * @static
     * @access private
     * @param \frontend\modules\product\models\ProductVariation[] $productVariations List product variations wich exist in user cart
     * @param \frontend\modules\product\helpers\CartDTO $cart User cart
     * @return void
     */
    private static function updateProductVariationsCount($productVariations, $cart)
    {
        foreach ($productVariations as $productVariation) {
            $countProductVariation = $productVariation->inventorie->count;

            if ($productVariation->isProductAnPack()) {
                $count = $productVariation->inventorie->count;
                $mainPackProductVariation = $productVariation->product->mainPackProduct->getProductVariations()->with('inventorie')->one();

                if ($mainPackProductVariation && $mainPackProductVariation->inventorie) {
                    $mainPackProductVariation->inventorie->count -= $count * $cart[ $productVariation->id ]->count;
                    $mainPackProductVariation->inventorie->save();
                }
            } else {
                $countProductVariation -= $cart[ $productVariation->id ]->count;
                $productVariation->inventorie->count = $countProductVariation;
                $productVariation->inventorie->save();
            }
        }
    }

    /**
     * Send admin order information letter.
     *
     * @static
     * @return void
     */
    private static function sendAdminLetter($order)
    {
        $mailer = Yii::$app->mailer;

        $mailer->compose()
            ->setFrom([$mailer->transport->getUsername() => 'info'])
            ->setTo(Yii::$app->params[ 'adminEmail' ])
            ->setSubject('New order')
            ->setTextBody(sprintf('New order with number %s', $order->order_number))
            ->send();
    }

    /**
     * Send admin notification for small count products after order.
     *
     * @access private
     * @return null|bool
     */
    private static function sendNotificationForSmallCountProducts($productVariationIds)
    {
        $variations = ProductVariation::find()
            ->alias('t')
            ->joinWith('inventorie inventory')
            ->with('properties')
            ->where(['t.id' => $productVariationIds])
            ->andWhere(['<', 'inventory.count', Setting::value('product.send.email.limit.quantity')])
            ->andWhere(['>=', 'inventory.count', 0])
            ->all();

        if (!$variations) {
            return null;
        }

        $message = Yii::$app->view->renderFile('@frontend/modules/product/views/order/notification-template-small-products.php', compact('variations'));

        $mailer = Yii::$app->mailer;

        return $mailer->compose()
            ->setFrom([$mailer->transport->getUsername() => 'info'])
            ->setTo(Yii::$app->params[ 'adminEmail' ])
            ->setSubject('List production ends on order.')
            ->setHtmlBody($message)
            ->send();
    }
}
