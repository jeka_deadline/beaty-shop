<?php

namespace frontend\modules\product\models;

use Yii;
use common\models\product\OrderShippingInformation as BaseOrderShippingInformation;
use frontend\modules\geo\models\Country;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use frontend\modules\user\models\User;

/**
 * Frontend order shipping information extends common order shipping information
 */
class OrderShippingInformation extends BaseOrderShippingInformation
{
    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    public function getShippingTimeDeliveryByMethod()
    {
        switch ($this->shipping_method) {
            case self::SHIPPING_STANDART_METHOD:
                return Yii::t('product', 'Delivery within 1-2 working days');
            case self::SHIPPING_EXPRESS_METHOD:
                return Yii::t('shipping', 'Delivery the next working day when ordering until 15:00');
            case self::SHIPPING_PICKUP_METHOD:
                return Yii::t('shipping', 'Pick up the next working day from 10am to 6pm in our studio. Kaiserstrasse 59, 44135 Dortmund');
            default:
                return Yii::t('product', 'Delivery within 1-2 working days');
        }
    }

    public function getHumanShippingMethod()
    {
        switch ($this->shipping_method) {
            case self::SHIPPING_STANDART_METHOD:
                return 'Standard';
            case self::SHIPPING_EXPRESS_METHOD:
                return 'Express';
            case self::SHIPPING_PICKUP_METHOD:
                return 'Selbstabholer';
            default:
                return 'Standard';
        }
    }

    /**
     * Get human title.
     *
     * @return string|null
     */
    public function getHumanTitle()
    {
        return User::getHumanTitle($this->title);
    }
}
