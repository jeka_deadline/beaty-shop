$(function() {
    $(document).on('ready pjax:success', function() {
        $('.star-ratings').barrating({
            theme: "css-stars",
            readonly: true,
        });
    });
});