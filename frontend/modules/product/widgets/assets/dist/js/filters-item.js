function setReceivedProductData(data) {

    var variation = data.variation.data;
    $('[data-model-attr="name"]').text(variation.name);
    $('[data-model-attr="price"]').text(data.variation.price);
    $('[data-model-attr="promotion"]').text(data.variation.promotion);
    $('[data-model-attr="count"]').val(data.variation.count);
    if (data.variation.isdiscount) {
        $('[data-model-attr="promotion"]').parent('div').show();
    } else {
        $('[data-model-attr="promotion"]').parent('div').hide();
    }
    $('[data-model-attr="description"]').html(variation.description);
    $('#form-cart').attr('action',data.cart.formAction);

    var htmlImages = '';
    $.each(data.variation.images, function(id, filename) {
        htmlImages+='<li data-thumb="'+filename+'"><img class="shop-item-page__zoom" src="'+filename+'" alt="variation.name"></li>';
    });
    htmlImages = '<div class="shop-item-page__item-slider col-xl-7 col-lg-7"><ul id="shop-item-page__item-slider">' + htmlImages + '</ul><div class="shop-item-page__zoom-here"></div></div>';
    $(document).find('.shop-item-page__item-slider').replaceWith(htmlImages);
    $('#productcartform-count').val(1);

    if ($("body").hasClass('shop-item-page')) {
        itemSlider();
        $('.shop-item-page__item-slider').css('visibility', 'visible');

        var out4 = $(document).find('.shop-item-page__zoom-here');
        $(document).find('.shop-item-page__zoom').parent().zoom({
            target: out4,
            magnify: 2,
            onZoomIn: function () {
                $(document).find('.shop-item-page__zoom-here').css('z-index', '1');
            },
            onZoomOut: function () {
                $(document).find('.shop-item-page__zoom-here').css('z-index', '0');
            }
        });
    }

    if (data.cart.buttonActive) {
        $('#form-cart button[type="submit"]').removeAttr("disabled");
    } else {
        $('#form-cart button[type="submit"]').attr("disabled", true);
    }
}

function postAJAXFilterVariations(el) {

    var fieldName = el?el.attr('data-input-name'):null;

    var form = $('form[data-type-form="filter"]');

    var data = form.serializeArray();

    data.push({
        name: 'selectFilterId',
        value: fieldName?parseInt(fieldName.replace(/\D+/g,"")):0
    });
    $.ajax({
        method: "POST",
        url: el?el.parents('form').attr('action'):form.attr('action'),
        dataType: "json",
        headers: {
            'X-CSRF-Token': yii.getCsrfToken(),
        },
        data: data,
        success: function (data, textStatus) {
            if (data.status=='error' || !textStatus == 'success') return;

            setReceivedProductData(data);

            var filters = data.filters;
            $('.shop-item-page__choose-block:not(.event-filter-disabled)').addClass('shop-item-page__choose-block-disabled');
            $('.filter-5-item:not(.event-filter-disabled)').addClass('disabled-color');
            $.each(filters, function (id, filter) {
                $.each(filter, function (key, value) {
                    $('.filters-' + id + '-value[data-value="' + value + '"]:not(.event-filter-disabled)').removeClass('shop-item-page__choose-block-disabled');
                    $('.filters-' + id + '-value[data-value="' + value + '"]:not(.event-filter-disabled)').removeClass('disabled-color');
                });
            });

            $('.shop-item-page__choose-block:not(.event-filter-disabled)').removeClass('shop-item-page__choose-block-active');
            $('.filter-5-item:not(.event-filter-disabled)').removeClass('active-color');

            if (data.query) {
                window.history.pushState(null, null, '?' + data.query.search);
                $.each(data.query.selectFilters, function (index, value) {
                    $('input:hidden[name="filters['+index+']"], input:text[name="filters['+index+']"], input:password[name="filters['+index+']"], input:file[name="filters['+index+']"], select[name="filters['+index+']"], textarea[name="filters['+index+']"]').val(value);
                    $('input:radio[name="filters['+index+']"], input:checkbox[name="filters['+index+']"]').removeAttr('checked').removeAttr('selected');

                    $('.shop-item-page__choose-block[data-input-name="filters['+index+']"][data-value="' + value + '"]:not(.event-filter-disabled)').addClass('shop-item-page__choose-block-active');
                    $('.filter-5-item[data-input-name="filters['+index+']"][data-value="' + value + '"]:not(.event-filter-disabled)').addClass('active-color');
                });
            }
        }
    });
}

$(function() {
    $('body').on('click', '.shop-item-page__choose-block, .filter-5-wrapper', function (e) {
        var el = $(e.target);

        if (el.hasClass('shop-item-page__choose-block')) {
            if (el.hasClass('event-filter-disabled')) return;

            var oldValue = el.parents('.shop-item-page__length-blocks').find('input[type=hidden]').val();
            var value = el.attr('data-value');
            el.parents('.shop-item-page__length-blocks').find('input[type=hidden]').val(oldValue == value ? null : value);
        } else if (el.hasClass('filter-5-wrapper')) {
            if (el.hasClass('event-filter-disabled')) return;

            var oldValue = el.parents('.filter-5-wrapper').find('input[type=hidden]').val();
            var value = el.attr('data-value');
            el.parents('.filter-5-wrapper').find('input[type=hidden]').val(oldValue == value ? null : value);
        }

        postAJAXFilterVariations(el);
    });

    if (window.location.search)
        postAJAXFilterVariations(null);
});

function itemSlider() {
    var verticalHorizontal = true;
    if ($(window).width() < 1199) {
      verticalHorizontal = false;
    }

    document.lightsliderOnItem = $(document).find('#shop-item-page__item-slider').lightSlider({
      gallery: true,
      item: 1,
      vertical: verticalHorizontal,
      verticalHeight: 430, //555
      vThumbWidth: 100,
      thumbItem: 4,
      thumbMargin: 10,
      slideMargin: 0,
      galleryMargin: 70,
      adaptiveHeight: true,
      loop: true,
      controls: true,
      responsive: [
        {
          breakpoint: 1199,
          settings: {
            thumbItem: 4,
            thumbMargin: 45,
          },
        },
        {
          breakpoint: 992,
          settings: {
            thumbItem: 0,
            gallery: false,
            adaptiveHeight: true,
            galleryMargin: 30,
          },
        },
        {
          breakpoint: 400,
          settings: {
            verticalHeight: 300,
            gallery: false,
            adaptiveHeight: true,
          },
        },
        {
          breakpoint: 0,
          settings: { // settings for width 0px to 480px
            adaptiveHeight: true,
            //item: 2,
            //slideMove: 1
          }
        }
      ],
      onSliderLoad: function (el) {
        changeRowsContainer();
      },
    });

    document.lightsliderOnItem.refresh();
}

/*change rows container*/
function changeRowsContainer(){
  //console.log($('.shop-item-page__item-slider .lSSlideWrapper .lSAction').length);
  //if($('.lSSlideWrapper .lSAction').length === 1) {
  $(document).find('.shop-item-page__item-slider > .lSAction').remove();
  $(document).find('.lSAction').appendTo('.shop-item-page__item-slider');
  $(document).find('.lSAction').css('opacity', 1);
  //}
}
