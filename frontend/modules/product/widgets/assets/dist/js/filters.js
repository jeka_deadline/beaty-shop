function getSerializeDataForm() {
    var serialize = $('form[data-type-form="filter"]').serialize();
    if (serialize)
        return serialize;
    return $('form[data-type-form="search-filter"]').serialize();
}

$(function() {
    $(document).on('change', '[name^="filters["]:not(.not-submit)', function( e ) {
       window.location.href = $(this).parents('form').attr('action') + '?' + getSerializeDataForm();
    });

    $('body').on('click', '.filter-1-item, .filter-2-item', function (e) {
        var el = $(e.target);
        var oldValue = el.parents('.filter-1-wrapper').find('input[type=hidden]').val();
        var value = el.attr('data-value');
        el.parents('.filter-1-wrapper').find('input[type=hidden]').val(oldValue == value?null:value);
    });

    $('body').on('click', '.filter-5-item', function (e) {
        var el = $(e.target);
        var oldValue = el.parents('.filter-5-wrapper').find('input[type=hidden]').val();
        var value = el.attr('data-value');
        el.parents('.filter-5-wrapper').find('input[type=hidden]').val(oldValue == value?null:value);
    });

    $(document).on('click', '.filter-submit', function(e) {
        window.location.href = $(this).parents('form').attr('action') + '?' + getSerializeDataForm();
    });

    //TO-DO Old version. Can be deleted.
    // $(document).on('change', '[name="sort"]:not(.not-submit)', function( e ) {
    //     window.location.href = $(this).parents('form').attr('action') + '?' + $('form[data-type-form="filter"]').serialize();
    // });

    //Displaying the sort type
    if (window.location.search.indexOf('sort=')!=-1) {
        $('.shop-page__sort-by').text($('.sort-variants a[data-value="' + $('input[name="sort"]').val() + '"]').text());
    }

    /**
     * Specify sorting
     */
    $(document).on('click', '.sort-block .option', function( e ) {
        var value = $(e.target).attr('data-value');
        if (value == null) return;
        $('input[name="sort"]').val(value);
        window.location.href = $(this).parents('form').attr('action') + '?' + getSerializeDataForm();
    });

    /**
     * AJAX Load the next page in the directory.
     */
    $(document).on('click', '.load-next-page', function(e) {
        var currentPage = parseInt($('.pagination .active:last').text());
        var nextPage = $('.pagination li:not(.next) a[data-page='+currentPage+']');
        var target = e.target;

        if (!nextPage.length) return;

        var nextPageHref = nextPage.attr('href');
        if (nextPageHref == null) return;

        $.get(nextPageHref, function(data) {
            window.history.pushState(null, null, nextPageHref);
            $('.list-catalog').append(data);

            var li = nextPage.parent('li');
            li.addClass('active disabled');
            li.html('<span>'+li.text()+'</span>');

            var pageNext = li.next().find('a');
            var aNext = $('.pagination li.next a');

            if (!pageNext.length) return;

            if (currentPage == pageNext.attr('data-page')) {
                $(target).hide(); // hidden buton more
                li.next()
                    .addClass('active disabled')
                    .html('<span>»</span>')
            } else {
                aNext.attr('href', pageNext.attr('href'));
                aNext.attr('data-page', pageNext.attr('data-page'));
            }
        });
    });

});