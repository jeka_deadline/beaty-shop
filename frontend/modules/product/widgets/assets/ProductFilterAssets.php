<?php

namespace frontend\modules\product\widgets\assets;

use yii\web\AssetBundle;

class ProductFilterAssets extends AssetBundle
{
    public $sourcePath = '@frontend/modules/product/widgets/assets/dist';

    public $css = [
    ];

    public $js = [
        'js/filters.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
        'frontend\widgets\bootstrap4\BootstrapAsset',
        'frontend\widgets\bootstrap4\BootstrapPluginAsset',
        'frontend\modules\product\assets\CategoryAsset',
    ];

    public $publishOptions = [
        'forceCopy' => (YII_DEBUG) ? true : false,
    ];

}