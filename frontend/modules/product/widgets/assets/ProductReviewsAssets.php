<?php

namespace frontend\modules\product\widgets\assets;

use yii\web\AssetBundle;

class ProductReviewsAssets extends AssetBundle
{
    public $sourcePath = '@frontend/modules/product/widgets/assets/dist';

    public $css = [
    ];

    public $js = [
        'js/reviews.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
        'frontend\widgets\bootstrap4\BootstrapAsset',
        'frontend\widgets\bootstrap4\BootstrapPluginAsset',
    ];

    public $publishOptions = [
        'forceCopy' => (YII_DEBUG) ? true : false,
    ];

}