<?php

namespace frontend\modules\product\widgets\assets;

use yii\web\AssetBundle;

class ProductItemFilterAssets extends AssetBundle
{
    public $sourcePath = '@frontend/modules/product/widgets/assets/dist';

    public $css = [
    ];

    public $js = [
        'js/filters-item.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
        'frontend\widgets\bootstrap4\BootstrapAsset',
        'frontend\widgets\bootstrap4\BootstrapPluginAsset',
    ];

    public $publishOptions = [
        'forceCopy' => (YII_DEBUG) ? true : false,
    ];

}