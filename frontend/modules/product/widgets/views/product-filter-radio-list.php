<?php

use frontend\widgets\bootstrap4\Html;

$id = Html::getInputId($model, $attribute);
$name = Html::getInputName($model, $attribute);
?>

<div class="filter-1-wrapper">
    <?= Html::hiddenInput($name, $value)?>
    <?php foreach ($items as $key => $val): ?>
        <div class="filter-1-item <?= $id ?>-value<?= $value==$val ?' active':'' ?>" data-value="<?= $key ?>"><?= $val ?></div>
    <?php endforeach; ?>
</div>