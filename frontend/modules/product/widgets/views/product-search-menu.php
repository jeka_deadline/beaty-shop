<?php

?>
<?php foreach ($tree as $id => $node): ?>
    <?php if ($node['show_search']) :?>
        <p><a href="<?= $slug . '/' . $node[ 'slug' ] ?>"><?= $node['name'] ?></a></p>
    <?php endif; ?>
    <?php if (isset($node[ 'children' ])):?>
        <?= $this->render('product-search-menu', [
                'tree' => $node[ 'children' ],
                'level' => $level+1,
                'slug' => $slug . '/' . $node[ 'slug' ],
                'breadcrumbs' => $breadcrumbs
        ]) ?>
    <?php endif; ?>
<?php endforeach; ?>
