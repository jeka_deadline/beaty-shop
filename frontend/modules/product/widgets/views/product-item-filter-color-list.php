<?php

use frontend\widgets\bootstrap4\Html;

$id = Html::getInputId($model, $attribute);
$name = Html::getInputName($model, $attribute);

?>

<?= Html::hiddenInput($name, $value)?>
<?php foreach ($items as $key => $val): ?>
    <div
            class="filter-5-item <?= $id ?>-value<?= $value==$val ?' active-color':'' ?>"
            data-input-name="<?= $name ?>"
            data-value="<?= $key ?><?= isset($itemsEnable[$val]) ?'':' disabled-color event-filter-disabled' ?>"
            style="background-color: <?= $val ?>;"
    >
    </div>
<?php endforeach; ?>