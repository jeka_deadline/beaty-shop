<?php

use frontend\modules\core\models\Setting;
use frontend\widgets\bootstrap4\Html;

$id = Html::getInputId($model, $attribute);
$name = Html::getInputName($model, $attribute);
?>

<div class="filter-4-wrapper">
    <?= Html::hiddenInput($name, $value)?>
    <div class="filter-inputs">
        <input id="<?= $id ?>-min">
        <input id="<?= $id ?>-max">
        <div id="<?= $id ?>-filter-slider" class="filter-slider"></div>
    </div>
</div>

<?php
$currency = Setting::value('product.shop.currency');
$uniqid = uniqid();
$script = <<< JS
    if ($('body').hasClass('shop-page') || $('body').hasClass('search-page')) {
        var filterSlider{$uniqid} = document.getElementById('{$id}-filter-slider');

        noUiSlider.create(filterSlider{$uniqid}, {
            start: [{$minValue}, {$maxValue}],
            connect: true,
            range: {
                'min': {$min},
                'max': {$max}
            },
        });

        var inputMin{$uniqid} = document.getElementById('{$id}-min');
        var inputMax{$uniqid} = document.getElementById('{$id}-max');


        filterSlider{$uniqid}.noUiSlider.on('update', function (values, handle) {

            var value = values[handle];

            if (handle) {
                inputMax{$uniqid}.value = '{$currency}' + value;
            } else {
                inputMin{$uniqid}.value = '{$currency}' + value;
            }
            $(this.target).parents('.filter-4-wrapper').find('input[type=hidden]').val(
                inputMin{$uniqid}.value.replace(/[^\d\.]/g,"")
                +','
                +inputMax{$uniqid}.value.replace(/[^\d\.]/g,"")
            );
        });

        inputMax{$uniqid}.addEventListener('change', function () {
            filterSlider{$uniqid}.noUiSlider.set([null, this.value]);
        });

        inputMin{$uniqid}.addEventListener('change', function () {
            filterSlider{$uniqid}.noUiSlider.set([this.value, null]);
        });
    }
JS;

$this->registerJs($script, yii\web\View::POS_READY);
?>