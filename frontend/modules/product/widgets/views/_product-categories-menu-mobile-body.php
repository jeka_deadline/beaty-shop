<?php

use frontend\modules\product\models\ProductCategory;

?>
<div class="card-body">
    <?php foreach ($tree as $id => $node): ?>
        <?php
            if (!($selectIds==null || ProductCategory::findSelectTree($node,$selectIds))) continue;
        ?>

        <?php if (isset($node[ 'url' ])) : ?>
        <?php else : ?>

            <a class="<?= ((isset($breadcrumbs[$level]) && $breadcrumbs[$level]==$node[ 'slug' ])?'active':'') ?>" href="<?= $slug . '/' . $node[ 'slug' ] ?>"><?= $node['name'] ?></a>

        <?php endif; ?>

        <?php if (isset($node[ 'children' ])):?>
            <div class="collapse" id="collapseAdaptive<?= $id ?>" aria-labelledby="headingAdaptive<?= $id ?>" data-parent="#accordion2">
                <?= $this->render('_product-categories-menu-body', [
                    'tree' => $node[ 'children' ],
                    'level' => $level+1,
                    'slug' => $slug . '/' . $node[ 'slug' ],
                    'breadcrumbs' => $breadcrumbs,
                    'selectIds' => $selectIds
                ]) ?>
            </div>
        <?php endif; ?>

    <?php endforeach; ?>
</div>
