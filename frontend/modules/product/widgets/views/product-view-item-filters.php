<?php

use backend\modules\product\models\ProductFilter;

?>
<div class="shop-item-page__length-block-wrapper">
    <?php if ($variationProduct->properties): ?>
        <?php foreach ($variationProduct->properties as $property) : ?>
            <?php if (($filter = $property->filter)): ?>

<?php switch ($filter->type):?>
<?php case ProductFilter::TYPE_COLORLIST: ?>
                        <div class="shop-item-page__color-block-wrapper">
                            <p><?= $filter->name ?></p>
                            <div class="filter-5-wrapper">
                                <div class="filter-5-item disabled-color" style="background-color: <?= $property->value ?>;"></div>
                            </div>
                        </div>
                        <?php break; ?>
<?php default: ?>
                        <div class="shop-item-page__length-block-wrapper">
                            <p><?= $filter->name ?></p>
                            <div class="shop-item-page__length-blocks">
                                <div class="shop-item-page__choose-block shop-item-page__choose-block-disabled">
                                    <?= $property->value ?>
                                </div>
                            </div>
                        </div>
                        <?php break; ?>

                    <?php endswitch; ?>

            <?php endif; ?>
        <?php endforeach; ?>
    <?php endif; ?>
</div>

