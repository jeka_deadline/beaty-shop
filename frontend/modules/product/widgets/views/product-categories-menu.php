<?php

use frontend\modules\product\models\ProductCategory;

?>

<?php foreach ($tree as $id => $node): ?>
    <?php
        if (!($selectIds==null || ProductCategory::findSelectTree($node,$selectIds))) continue;
    ?>
    <div class="card">
        <div class="card-header" id="heading<?= $id ?>">
            <h5 class="mb-0">

                <?php if (isset($node[ 'url' ])) : ?>

                    <a href="<?= $node[ 'url' ] ?>"><?= $node[ 'name' ] ?></a>

                <?php else : ?>

                    <a href="<?= $slug . '/' . $node[ 'slug' ] ?>"><?= $node['name'] ?></a>

                <?php endif; ?>

            </h5>

            <?php if (!isset($node[ 'url' ])) : ?>

                <button class="aside-plus-button btn btn-link<?= ((isset($breadcrumbs[$level]) && $breadcrumbs[$level]!=$node[ 'slug' ])?' collapsed':'') ?>" data-toggle="collapse" data-target="#collapse<?= $id ?>" aria-expanded="true" aria-controls="collapse<?= $id ?>">
                    <svg class="shop-plus-aside svg">
                        <use xlink:href="#shop-plus"></use>
                    </svg>
                </button>

            <?php endif; ?>

        </div>
        <?php if (isset($node[ 'children' ])):?>
            <div class="collapse<?= ((isset($breadcrumbs[$level]) && $breadcrumbs[$level]==$node[ 'slug' ])?' show':'') ?>" id="collapse<?= $id ?>" aria-labelledby="heading<?= $id ?>" data-parent="#accordion">
                <?= $this->render('_product-categories-menu-body', [
                        'tree' => $node[ 'children' ],
                        'level' => $level+1,
                        'slug' => $slug . '/' . $node[ 'slug' ],
                        'breadcrumbs' => $breadcrumbs,
                        'selectIds' => $selectIds,
                ]) ?>
            </div>
        <?php endif; ?>
    </div>
<?php endforeach; ?>
