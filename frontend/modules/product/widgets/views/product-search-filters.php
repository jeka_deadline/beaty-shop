<?php

use backend\modules\product\models\ProductFilter;
use frontend\modules\product\widgets\ProductFilterColorList;
use frontend\modules\product\widgets\ProductFilterRadioList;
use frontend\modules\product\widgets\ProductFilterRange;
use frontend\widgets\bootstrap4\ActiveForm;
use frontend\widgets\bootstrap4\Html;
use frontend\widgets\slider\Slider;
use yii\helpers\Url;

$formAction = $formAction?"/{$formAction}":null;

?>
<?php $form = ActiveForm::begin([
    'action' => $formAction,
    'method' => 'get',
    'options'=> [
        'data-type-form' => $groupForm
    ]
]); ?>
<div class="row filter-row" id="filters">
<?php foreach ($filters as $id => $filter): ?>
    <div class="filter-1 col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">
        <h6><?= $filter['filter']->name ?></h6>
<?php switch ($filter['filter']->type):?>
<?php case ProductFilter::TYPE_RANGE: ?>
            <?= $form->field($formFilters, 'filters['.($filter['filter']->id?$filter['filter']->id:$id).']')->widget(ProductFilterRange::classname(), [
                'min' => intval(min($filter['listValues'])),
                'max'=> intval(max($filter['listValues'])),
            ])->label(false) ?>
            <?php break; ?>
        <?php case ProductFilter::TYPE_SLIDER: ?>
            <?= $form->field($formFilters, 'filters['.$filter['filter']->id.']')->widget(Slider::classname(), [
                'options' => [
                    'class' => 'not-submit'
                ],
                'sliderColor'=>Slider::TYPE_GREY,
                'pluginOptions'=>[
                    'min' => intval(min($filter['listValues'])),
                    'max'=> intval(max($filter['listValues'])),
                    'step'=>1,
                ],
            ])->label(false) ?>
            <?php break; ?>
        <?php case ProductFilter::TYPE_CHECKBOX: ?>
            <?= $form->field($formFilters, 'filters['.$filter['filter']->id.']')->checkboxList($filter['listValues'])->label(false); ?>
            <?php break; ?>
        <?php case ProductFilter::TYPE_DROPDOWN: ?>
            <?= $form->field($formFilters, 'filters['.$filter['filter']->id.']')->dropDownList($filter['listValues'],['prompt' => ''])->label(false); ?>
            <?php break; ?>
        <?php case ProductFilter::TYPE_RADIOLIST: ?>
            <?= $form->field($formFilters, 'filters['.$filter['filter']->id.']')->widget(ProductFilterRadioList::className(),[
                'items' => $filter['listValues']
            ])->label(false); ?>
            <?php break; ?>
        <?php case ProductFilter::TYPE_COLORLIST: ?>
            <?= $form->field($formFilters, 'filters['.$filter['filter']->id.']')->widget(ProductFilterColorList::className(),[
                'items' => $filter['listValues']
            ])->label(false); ?>
            <?php break; ?>
    <?php endswitch; ?>
    </div>
<?php endforeach; ?>
</div>
<div class="row filter-buttons">
    <?= Html::button('apply', ['class' => 'btn btn-primary filter-submit']) ?>
    <a class="filter-clear" href="<?= $formAction.'?query='.Yii::$app->request->get('query') ?>">Clear</a>
</div>

<?php ActiveForm::end(); ?>
