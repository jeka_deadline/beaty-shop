<?php

use frontend\modules\core\models\Setting;
use yii\helpers\Url;

?>

<?php if ($records): ?>

    <section class="shop-item-page__carousel1 container">
        <h3><?= Yii::t('product', 'You may also like') ?></h3>
        <div class="owl-carousel owl-theme all-pages-carousel">

            <?php foreach ($records as $id => $productRecord):?>

                <?php if (!($productVariation = $productRecord->defaultProductVariation)) continue; ?>

                <div class="item">
                    <a href="<?= Url::to(['/product/product/index', 'slug' => $productRecord->slug])?>" title="Read more">
                        <div class="img">
                            <?php if ($productVariation->preview): ?>
                                <img src="<?= $productVariation->preview->getUrlImage() ?>" alt="<?= $productVariation->name ?>">
                            <?php endif; ?>
                        </div>
                        <header>
                            <?= $productVariation->name ?>
                        </header>
                        <div class="price"><?= Setting::value('product.shop.currency') ?><?= $productVariation->viewNumberFormatFullPrice; ?></div>
                    </a>
                </div>

            <?php endforeach; ?>

        </div>
    </section>

<?php endif; ?>