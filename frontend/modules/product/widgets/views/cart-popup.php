<?php

use frontend\modules\core\models\Setting;
use yii\helpers\Url;
use frontend\modules\product\models\ProductVariation;

?>
<div class="cart-hover-menu">
    <header>
        <div class="cart-hover-menu__header-1"><?= Yii::t('core', 'Free shipping'); ?></div>
        <div class="cart-hover-menu__header-2"><?= Yii::t('core', 'Buy another {0} and get free shipping', Setting::value('product.shop.currency') . $infoFreeShipping['remainder']); ?></div>
        <div class="progress-wrapper">
            <div class="progress" style="height: 2px;">
                <div class="progress-bar" role="progressbar" style="width: <?= $infoFreeShipping['value'] ?>%;" aria-valuenow="<?= $infoFreeShipping['value'] ?>" aria-valuemin="0" aria-valuemax="<?= $infoFreeShipping['max'] ?>"></div>
            </div>
            <div class="progress-bar-value">55</div>
        </div>
    </header>
    <div class="cart-hover-menu__items">

        <?php foreach ($productVariations as $productVariation) : ?>

            <?= $this->render('@app/modules/product/views/cart/item-product-popup', compact('productVariation', 'cart')); ?>

        <?php endforeach; ?>

    </div>
    <footer>
        <div class="cart-hover-menu__subtotal">
            <div class="cart-hover-menu__subtotal-text"><?= Yii::t('product', 'Subtotal') ?>:</div>
            <div class="cart-hover-menu__subtotal-price"><?= Setting::value('product.shop.currency') ?><span><?= ProductVariation::viewFormatPrice($totalPrice); ?></span></div>
        </div>
        <a class="btn btn-primary" href="<?= Url::toRoute(['/product/cart/index']); ?>"><?= Yii::t('product', 'view cart') ?></a>
    </footer>
</div>