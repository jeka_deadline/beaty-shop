<?php

?>
<?php if ($visible):?>
    <div class="shop-page__load-more">
        <button class="btn btn-secondary col-xl-10 col-lg-10 offset-lg-1 load-next-page"><?= Yii::t('core', 'Load more'); ?></button>
    </div>
<?php endif; ?>