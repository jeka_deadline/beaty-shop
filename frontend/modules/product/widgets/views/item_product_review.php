<?php

use yii\helpers\Html;
use yii\helpers\Url;

/** @var \frontend\modules\core\components\View $this */
/** @var \frontend\modules\product\models\UserReview $model */
/** @var int $marginLeft */

?>

<div class="row shop-item-page__comment" style="margin-left: <?= $marginLeft; ?>px">
    <div class="col-xl-2">
        <div class="shop-item-page__comment-name">

            <?= sprintf('%s %s.', $model->username, $model->firstLetterSurname); ?>

        </div>
        <div class="shop-item-page__comment-date"><?= date('d.m.Y', strtotime($model->created_at)); ?></div>
    </div>
    <div class="col-xl-8 shop-item-page__comment-body">
        <div class="shop-item-page__comment-title-wrapper">
            <?= Html::dropDownList('review-rating-'.$model->id,round($model->rating?$model->rating->value:0),[
                '1'=>'1',
                '2'=>'2',
                '3'=>'3',
                '4'=>'4',
                '5'=>'5',
            ],[
                'prompt' => '',
                'class' => 'star-ratings',
            ]) ?>
            <div class="shop-item-page__comment-title"><?= $model->subject; ?></div>
        </div>
        <div class="shop-item-page__comment-content">
            <?= $model->text; ?>
        </div>
    </div>
    <?php if (!Yii::$app->user->isGuest && false) : ?>
        <?= Html::a(
            'Reply',
            '#',
            [
                'class' => 'link-get-reply-review',
                'data-url' => Url::toRoute(['/product/product/get-reply-review-form', 'reviewId' => $model->id])
            ]
        ); ?>
    <?php endif; ?>
    <hr/>
</div>