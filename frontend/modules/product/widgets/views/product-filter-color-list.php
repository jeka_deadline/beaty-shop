<?php

use frontend\widgets\bootstrap4\Html;

$id = Html::getInputId($model, $attribute);
$name = Html::getInputName($model, $attribute);

?>

<div class="filter-5-wrapper">
    <?= Html::hiddenInput($name, $value)?>
    <?php foreach ($items as $key => $val): ?>
        <div class="filter-5-item <?= $id ?>-value<?= $value==$val ?' active-color':'' ?>" data-value="<?= $key ?>" style="background-color: <?= $val ?>;"></div>
    <?php endforeach; ?>
</div>