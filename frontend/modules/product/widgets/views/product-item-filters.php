<?php

use backend\modules\product\models\ProductFilter;
use frontend\modules\product\widgets\ProductFilterColorList;
use frontend\modules\product\widgets\ProductFilterRadioList;
use frontend\modules\product\widgets\ProductFilterRange;
use frontend\widgets\bootstrap4\ActiveForm;
use frontend\widgets\bootstrap4\Html;
use frontend\widgets\slider\Slider;
use yii\helpers\Url;

?>
<?php $form = ActiveForm::begin([
    'action' => $formAction?$formAction:null,
    'method' => 'get',
    'options'=> [
        'data-type-form' => $groupForm
    ]
]); ?>
<div class="shop-item-page__length-block-wrapper">
<?php foreach ($filters as $id => $filter): ?>
<?php switch ($filter['filter']->type):?>
<?php case ProductFilter::TYPE_RANGE: ?>
            <div class="shop-item-page__length-block-wrapper">
                <p><?= $filter['filter']->name ?></p>
                <?= $form->field($formFilters, 'filters['.($filter['filter']->id?$filter['filter']->id:$id).']',[
                    'options' => [
                        'class' => 'shop-item-page__length-blocks',
                    ]
                ])->widget(ProductFilterRange::classname(), [
                    'min' => min($filter['listValues']),
                    'max'=> max($filter['listValues']),
                ])->label(false) ?>
            </div>
            <?php break; ?>
        <?php case ProductFilter::TYPE_SLIDER: ?>
            <div class="shop-item-page__length-block-wrapper">
                <p><?= $filter['filter']->name ?></p>
                <?= $form->field($formFilters, 'filters['.$filter['filter']->id.']',[
                    'options' => [
                        'class' => 'shop-item-page__length-blocks',
                    ]
                ])->widget(Slider::classname(), [
                    'options' => [
                        'class' => 'not-submit'
                    ],
                    'sliderColor'=>Slider::TYPE_GREY,
                    'pluginOptions'=>[
                        'min' => min($filter['listValues']),
                        'max'=> max($filter['listValues']),
                        'step'=>1,
                    ],
                ])->label(false) ?>
            </div>
            <?php break; ?>
        <?php case ProductFilter::TYPE_CHECKBOX: ?>
            <div class="shop-item-page__length-block-wrapper">
                <p><?= $filter['filter']->name ?></p>
                <?= $form->field($formFilters, 'filters['.$filter['filter']->id.']',[
                    'options' => [
                        'class' => 'shop-item-page__length-blocks',
                    ]
                ])->checkboxList($filter['listValues'])->label(false); ?>
            </div>
            <?php break; ?>
        <?php case ProductFilter::TYPE_DROPDOWN: ?>
            <div class="shop-item-page__length-block-wrapper">
                <p><?= $filter['filter']->name ?></p>
                <?= $form->field($formFilters, 'filters['.$filter['filter']->id.']',[
                    'options' => [
                        'class' => 'shop-item-page__length-blocks',
                    ]
                ])->dropDownList($filter['listValues'],['prompt' => ''])->label(false); ?>
            </div>
            <?php break; ?>
        <?php case ProductFilter::TYPE_RADIOLIST: ?>
            <div class="shop-item-page__length-block-wrapper">
                <p><?= $filter['filter']->name ?></p>
                <?= $form->field($formFilters, 'filters['.$filter['filter']->id.']',[
                    'options' => [
                        'class' => 'shop-item-page__length-blocks',
                    ]
                ])->widget(ProductFilterRadioList::className(),[
                    'template' => 'product-item-filter-radio-list',
                    'items' => $filter['listValues'],
                    'itemsEnable' => $filter['listActiveValues'],
                ])->label(false); ?>
            </div>
            <?php break; ?>
        <?php case ProductFilter::TYPE_COLORLIST: ?>
            <div class="shop-item-page__color-block-wrapper">
                <p><?= $filter['filter']->name ?></p>
                <?= $form->field($formFilters, 'filters['.$filter['filter']->id.']',[
                    'options' => [
                        'class' => 'filter-5-wrapper',
                    ]
                ])->widget(ProductFilterColorList::className(),[
                    'template' => 'product-item-filter-color-list',
                    'items' => $filter['listValues'],
                    'itemsEnable' => $filter['listActiveValues'],
                ])->label(false); ?>
            </div>
            <?php break; ?>
    <?php endswitch; ?>
<?php endforeach; ?>
</div>

<?php ActiveForm::end(); ?>

