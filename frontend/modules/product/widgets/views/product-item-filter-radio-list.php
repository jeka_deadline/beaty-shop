<?php

use frontend\widgets\bootstrap4\Html;

$id = Html::getInputId($model, $attribute);
$name = Html::getInputName($model, $attribute);

?>

<?= Html::hiddenInput($name, $value)?>
<?php foreach ($items as $key => $val): ?>
    <div
        class="shop-item-page__choose-block <?= $id ?>-value<?= $value==$val ?' shop-item-page__choose-block-active':'' ?><?= isset($itemsEnable[$val]) ?'':' shop-item-page__choose-block-disabled  event-filter-disabled' ?>"
        data-input-name="<?= $name ?>"
        data-value="<?= $key ?>"
    >
        <?= $val ?>
    </div>
<?php endforeach; ?>
