<?php

use frontend\widgets\bootstrap4\ActiveForm;
use frontend\widgets\bootstrap4\Html;
use frontend\widgets\typeahead\Typeahead;
use yii\helpers\Url;
use yii\web\JsExpression;

$queryId = Html::getInputId($formProductSearch, 'query');

$template = '<div>'.
    '<div class="media">'.
    '<div class="media-left media-middle">'.
    '<img class="media-object" src="{{image}}" style="max-height: 50px">'.
    '</div>'.
    '<div class="media-body">'.
    '<p>{{name}}</p>'.
    '</div>'.
    '</div>'.
    '</div>';

?>

<?php $form = ActiveForm::begin([
    'method' => 'get',
    'action' => Url::to(['/search']),
    'options' => [
        'style' => 'width: 100%;',
        'data-type-form' => $groupForm
    ]
]); ?>

    <?= $form->field($formProductSearch, 'query', [
        'template' => '{input}{hint}',
        'options' => [
            'class' => '',
        ]
    ])->widget(Typeahead::className(),[
        'options' => $options,
        'dataset' => [
            [
                'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                'display' => 'value',
                'templates' => [
                    'notFound' => '<div class="text-danger" style="padding:0 8px">Unable to find repositories for selected query.</div>',
                    'suggestion' => new JsExpression("Handlebars.compile('{$template}')")
                ],
                'remote' => [
                    'url' => Url::to(['/search']).'?query=%QUERY',
                    'wildcard' => '%QUERY'
                ]
            ]
        ],
        'pluginEvents' => [
            'typeahead:select' => 'function(ev, suggestion) { window.location.href = suggestion.url; }',
        ]
    ])->label(false) ?>
    <div class="input-group-append">
        <?= Html::submitButton('<svg class="search svg"><use xlink:href="#search"></use></svg>', []) ?>
    </div>

<?php ActiveForm::end(); ?>
