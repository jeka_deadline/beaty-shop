<?php

use yii\widgets\LinkPager;

?>

<div class="shop-item-page__pagination">
    <nav aria-label="Page navigation in shop item page">
        <?= LinkPager::widget([
            'pagination' => $pages,
            'nextPageLabel' => 'Next',
            'prevPageLabel' => 'Previous',
            'disableCurrentPageButton' => false,
            'disabledListItemSubTagOptions' => [
                'tag' => 'a',
                'class' => 'page-link',
                'tabindex' => "-1"
            ],
            'options' => [
                'class' => 'pagination justify-content-center',
            ],
            'linkContainerOptions' =>[
                'class' => 'page-item'
            ],
            'linkOptions' => [
                'class' => 'page-link'
            ],
        ]);
        ?>
<!--        <ul class="pagination justify-content-center">-->
<!--            <li class="page-item disabled prev"><a class="page-link" href="#" tabindex="-1">Previous</a></li>-->
<!--            <li class="page-item"><a class="page-link" href="#">1</a></li>-->
<!--            <li class="page-item"><a class="page-link" href="#">2</a></li>-->
<!--            <li class="page-item"><a class="page-link" href="#">3</a></li>-->
<!--            <li class="page-item next"><a class="page-link" href="#">Next</a></li>-->
<!--        </ul>-->
    </nav>
</div>