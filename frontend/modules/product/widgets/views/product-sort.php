<?php

use frontend\modules\product\models\forms\ProductFilterForm;
use frontend\widgets\bootstrap4\ActiveForm;
use frontend\widgets\bootstrap4\Html;

$idSort = Html::getInputId($formFilters, 'sort');
?>
<?php $form = ActiveForm::begin([
    'action' => $formAction?"/{$formAction}":null,
    'method' => 'get',
    'options'=>[
        'data-type-form' => $groupForm
    ]
]); ?>

<div class="<?=$idSort?>-block">
    <div class="shop-page__sort-by"><?= Yii::t('product', 'sort') ?></div>
    <div class="sort-variants">
        <?= $form->field($formFilters, 'sort')->hiddenInput()->label(false); ?>
        <ul>
            <?php foreach (ProductFilterForm::getSortListLabel() as $code => $value): ?>
                <li><a href="#" class="option" data-value="<?= $code ?>"><?= $value ?></a></li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>

<?php ActiveForm::end(); ?>
