<?php

namespace frontend\modules\product\widgets;

use frontend\modules\product\models\forms\ProductFilterForm;
use frontend\modules\product\models\ProductFilter;
use frontend\modules\product\models\ProductGlobalFilter;
use frontend\modules\product\models\ProductVariationPropertie;
use frontend\modules\product\widgets\assets\ProductFilterAssets;
use Yii;
use yii\base\Widget;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Widget to show product reviews
 */
class ProductFilters extends Widget
{
    public $category = null;
    public $categoryIds = null;
    public $formAction = null;

    public $groupForm = 'filter';

    public function init() {
        parent::init();
        $this->registerAssets();
    }

    public function run()
    {
        if (!$this->categoryIds) {
            $this->categoryIds = $this->category?$this->category->id:null;
        }

        if (!($this->categoryIds)) return '';

        $productFilters = ProductFilter::find()
            ->joinWith('productCategorys')
            ->where(['product_category_id' => $this->categoryIds])
            ->all();

        $filterIds = ArrayHelper::map($productFilters, 'id', 'id');
        $modelFilters = ArrayHelper::map($productFilters, 'id', function ($model) {
            return $model;
        });

        if (!$this->categoryIds) {
            $this->categoryIds = $this->category->id;
        }

        $filters = ProductGlobalFilter::Factory()->getFilters($this->categoryIds);

        $rowVariationProperties = (new Query())->from(ProductVariationPropertie::tableName())
            ->where(['filter_id' => $filterIds, 'product_category_id' => $this->categoryIds])
            ->groupBy('value')
            ->all();

        if ($rowVariationProperties) {
            foreach ($rowVariationProperties as $rowVariationPropertie) {
                $filters[$rowVariationPropertie['filter_id']]['filter'] = $modelFilters[$rowVariationPropertie['filter_id']];
                $filters[$rowVariationPropertie['filter_id']]['listValues'][$rowVariationPropertie['value']] = $rowVariationPropertie['value'];
            }
        }

        $formFilters = new ProductFilterForm();
        $formFilters->load(Yii::$app->request->get());
        if (!$formFilters->validate()) return '';

        return $this->render('product-filters', [
            'filters' => $filters,
            'formFilters' => $formFilters,
            'formAction' => $this->formAction,
            'groupForm' => $this->groupForm,
        ]);
    }

    public function registerAssets() {
        $view=$this->getView();
        ProductFilterAssets::register($view);
    }
}