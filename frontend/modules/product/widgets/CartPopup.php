<?php

namespace frontend\modules\product\widgets;

use yii\base\Widget;
use frontend\modules\product\helpers\CartHelper;
use frontend\modules\product\models\ProductVariation;

class CartPopup extends Widget
{
    public function run()
    {
        $cart = CartHelper::getCart();

        $productVariations = ProductVariation::find()
            ->with('withProperties', 'preview')
            ->where(['id' => CartHelper::getProductIds()])
            ->active()
            ->all();

        $totalPrice = CartHelper::getTotalPrice();

        $infoFreeShipping = CartHelper::getInfoFreeShipping($totalPrice);

        return $this->render('cart-popup', compact('cart', 'productVariations', 'totalPrice', 'infoFreeShipping'));
    }
}