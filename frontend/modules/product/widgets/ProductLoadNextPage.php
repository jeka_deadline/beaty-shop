<?php

namespace frontend\modules\product\widgets;

use frontend\modules\product\models\forms\ProductFilterForm;
use frontend\modules\product\models\ProductVariationPropertie;
use frontend\modules\product\widgets\assets\ProductFilterAssets;
use Yii;
use yii\base\Widget;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Widget to show product reviews
 */
class ProductLoadNextPage extends Widget
{
    public $dataProvider = null;

    public function init() {
        parent::init();
        $this->registerAssets();
    }

    public function run()
    {
        $visible = false;
        if ($this->dataProvider) {
            $this->dataProvider->prepare();
            $pageCount = $this->dataProvider->pagination->pageCount;
            $currentPage = $this->dataProvider->pagination->page;
            if ($pageCount > 1 && $pageCount > ($currentPage+1)) $visible = true;
        }
        return $this->render('product-load-next-page', [
            'visible' => $visible
        ]);
    }

    public function registerAssets() {
        $view=$this->getView();
        ProductFilterAssets::register($view);
    }
}