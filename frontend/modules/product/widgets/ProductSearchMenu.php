<?php

namespace frontend\modules\product\widgets;

use Yii;
use yii\base\Widget;
use frontend\modules\product\models\ProductCategory;

/**
 * Widget which show nested product categories menu.
 */
class ProductSearchMenu extends Widget
{
    public $template = 'product-search-menu';

    /**
     * {@inheritdoc}
     *
     * @return mixed
     */
    public function run()
    {
        $breadcrumbs = explode('/', Yii::$app->request->get('slug',''));

        $categories = ProductCategory::find()
            ->orderBy(['display_order' => SORT_DESC])
            ->active()
            ->indexBy('id')
            ->asArray()
            ->all();

        $treeCategories = ProductCategory::createTree($categories);

        return $this->render($this->template,[
            'tree' =>  $treeCategories,
            'level' => 0,
            'slug' =>  '',
            'breadcrumbs' => $breadcrumbs?$breadcrumbs:[]
        ]);
    }
}