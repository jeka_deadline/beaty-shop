<?php

namespace frontend\modules\product\widgets;

use frontend\modules\product\models\forms\ProductFilterForm;
use frontend\modules\product\models\ProductVariationPropertie;
use frontend\modules\product\widgets\assets\ProductFilterAssets;
use Yii;
use yii\base\Widget;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Widget to show product reviews
 */
class ProductSort extends Widget
{
    public $formAction = null;

    public $groupForm = 'filter';

    public function init() {
        parent::init();
        $this->registerAssets();
    }

    public function run()
    {
        $formFilters = new ProductFilterForm();
        $formFilters->load(Yii::$app->request->get());
        if (!$formFilters->validate()) return '';

        return $this->render('product-sort', [
            'formFilters' => $formFilters,
            'formAction' => $this->formAction,
            'groupForm' => $this->groupForm,
        ]);
    }

    public function registerAssets() {
        $view=$this->getView();
        ProductFilterAssets::register($view);
    }
}