<?php

namespace frontend\modules\product\widgets;

use Yii;
use yii\base\Widget;
use frontend\modules\product\models\ProductCategory;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * Widget which show nested product categories menu.
 */
class ProductCategoriesMenu extends Widget
{
    private static $treeCategories = null;

    public $template = 'product-categories-menu';

    public $selectIds = null;

    /**
     * {@inheritdoc}
     *
     * @return mixed
     */
    public function run()
    {
        $breadcrumbs = explode('/', Yii::$app->request->get('slug',''));

        $treeCategories = array_merge([
            [
                'name' => Yii::t('product', 'New Arrivals'),
                'url' => Url::toRoute(['/product/product-category/news']),
            ]
      ], self::getTreeCategories());

        return $this->render($this->template,[
            'tree' =>  $treeCategories,
            'level' => 0,
            'slug' =>  '',
            'breadcrumbs' => $breadcrumbs?$breadcrumbs:[],
            'selectIds' => $this->selectIds
        ]);
    }

    private static function getTreeCategories() {
        if (is_null(self::$treeCategories)) {
            $categories = ProductCategory::find()
                ->orderBy('display_order')
                ->active()
                ->indexBy('id')
                ->all();

            if ($categories) {
                $categories = ArrayHelper::toArray($categories);
            }

            self::$treeCategories = ProductCategory::createTree($categories);
        }
        return self::$treeCategories;
    }
}