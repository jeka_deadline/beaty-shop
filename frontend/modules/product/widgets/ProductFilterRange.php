<?php

namespace frontend\modules\product\widgets;

use frontend\modules\product\widgets\assets\ProductFilterAssets;
use frontend\widgets\bootstrap4\Html;
use frontend\widgets\bootstrap4\InputWidget;

/**
 * Widget to show product reviews
 */
class ProductFilterRange extends InputWidget
{
    public $template = 'product-filter-range';

    public $min = 0;
    public $max = 0;

    public function init() {
        parent::init();
        $this->registerAssets();
        $this->initInputWidget();
    }

    public function run()
    {
        $minValue = $this->min;
        $maxValue = $this->max;

        if (strpos($this->value, ',') > 0) {
            $values = explode(',', $this->value);
            if (isset($values[0]) && isset($values[1])) {
                $minValue = $values[0];
                $maxValue = $values[1];
            }
        }


        return $this->render($this->template, [
            'min' => $this->min,
            'max' => $this->max,
            'minValue' => $minValue,
            'maxValue' => $maxValue,
            'options' => $this->options,
            'hasModel' => $this->hasModel(),
            'model' => $this->model,
            'attribute' => $this->attribute,
            'value' => $this->value,
        ]);
    }

    public function registerAssets() {
        $view=$this->getView();
        ProductFilterAssets::register($view);
    }

    protected function initInputWidget()
    {
        if ($this->hasModel()) {
            $this->name = !isset($this->options['name']) ? Html::getInputName($this->model, $this->attribute) : $this->options['name'];
            $this->value = !isset($this->options['value'])? Html::getAttributeValue($this->model, $this->attribute) : $this->options['value'];
        }
    }
}