<?php

namespace frontend\modules\product\widgets;

use frontend\modules\product\models\forms\ProductItemFilterForm;
use frontend\modules\product\models\ProductFilter;
use frontend\modules\product\widgets\assets\ProductItemFilterAssets;
use Yii;
use yii\base\Widget;
use yii\helpers\ArrayHelper;

/**
 * Widget to show product reviews
 */
class ProductViewItemFilters extends Widget
{
    public $variationProduct = null;

    public function run()
    {
        if (!$this->variationProduct) return '';

        return $this->render('product-view-item-filters', [
            'variationProduct' => $this->variationProduct,
        ]);
    }
}