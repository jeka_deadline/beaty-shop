<?php

namespace frontend\modules\product\widgets;

use frontend\modules\product\widgets\assets\ProductReviewsAssets;
use yii\base\Widget;
use frontend\modules\product\models\ProductReview;
use yii\data\Pagination;
use yii\widgets\LinkPager;

/**
 * Widget to show product reviews.
 */
class ProductReviews extends Widget
{
    /**
     * Product model.
     *
     * @var \frontend\modules\product\models\ProductVariation
     */
    public $product;

    /**
     * Max level reviews.
     *
     * @var int
     */
    public $maxLevel = 1;

    /**
     * Count nodes parent_id 0 on page.
     *
     * @var int $countNodesOnPage
     */
    public $countNodesOnPage = 5;

    public function init() {
        parent::init();
        $this->registerAssets();
    }

    /**
     * {@inheritdoc}
     *
     * @return mixed
     */
    public function run()
    {
        $reviewsQuery = $this->product->getReviews()
            ->with('user','user.profile','rating')
            ->where(['active' => 1, 'parent_id' => 0])
            ->orderBy(['created_at' => SORT_ASC]);

        $countQuery = clone $reviewsQuery;

        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => $this->countNodesOnPage,
            'pageParam' => 'comment-page',
        ]);

        $pages->pageSizeParam = false;

        $reviews = $reviewsQuery->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        foreach ($reviews as $review) {
            $this->generateItemReview($review, 0);
        }

        echo $this->render('_product_review_pagination', [
            'pages' => $pages,
        ]);
    }

    /**
     * Display item comment.
     *
     * @param \frontend\modules\product\models\ProductReview $review Item review
     * @param int $level Level nested review
     * @return void
     */
    private function generateItemReview(ProductReview $review, $level = 0)
    {
        if ($level + 1 >= $this->maxLevel) {
            $level = $this->maxLevel - 1;
        }

        echo $this->render('item_product_review', [
            'marginLeft' => $level * 40 - 15 ,
            'model' => $review,
        ]);

        $children = $review->getChildren()
            ->with('user','user.profile')
            ->where(['active' => 1])
            ->orderBy(['created_at' => SORT_ASC])
            ->all();

        if ($children) {
            foreach ($children as $child) {
                call_user_func_array([$this, __METHOD__], [$child, $level + 1]);
            }
        }
    }

    public function registerAssets() {
        $view=$this->getView();
        ProductReviewsAssets::register($view);
    }
}