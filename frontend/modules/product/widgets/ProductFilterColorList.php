<?php

namespace frontend\modules\product\widgets;

use frontend\modules\product\widgets\assets\ProductFilterAssets;
use frontend\widgets\bootstrap4\Html;
use frontend\widgets\bootstrap4\InputWidget;

/**
 * Widget to show product reviews
 */
class ProductFilterColorList extends InputWidget
{
    public $template = 'product-filter-color-list';

    public $items = [];

    public $itemsEnable = [];

    public function init() {
        parent::init();
        $this->registerAssets();
        $this->initInputWidget();
    }

    public function run()
    {

        return $this->render($this->template, [
            'items' => $this->items,
            'itemsEnable' => $this->itemsEnable,
            'options' => $this->options,
            'hasModel' => $this->hasModel(),
            'model' => $this->model,
            'attribute' => $this->attribute,
            'value' => $this->value,
        ]);
    }

    public function registerAssets() {
        $view=$this->getView();
        ProductFilterAssets::register($view);
    }

    protected function initInputWidget()
    {
        if ($this->hasModel()) {
            $this->name = !isset($this->options['name']) ? Html::getInputName($this->model, $this->attribute) : $this->options['name'];
            $this->value = !isset($this->options['value'])? Html::getAttributeValue($this->model, $this->attribute) : $this->options['value'];
        }
    }
}