<?php

namespace frontend\modules\product\widgets;

use frontend\modules\product\models\forms\ProductSearchForm;
use Yii;
use yii\base\Widget;

/**
 * Widget to show product reviews
 */
class ProductSearch extends Widget
{
    public $template = 'product-search';

    public $groupForm = 'filter';

    public $options = [];

    public function run()
    {
        $formProductSearch = new ProductSearchForm();

        $formProductSearch->load(Yii::$app->request->get());

        return $this->render($this->template, [
            'formProductSearch' => $formProductSearch,
            'groupForm' => $this->groupForm,
            'options' => $this->options,
        ]);
    }
}