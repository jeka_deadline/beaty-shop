<?php

namespace frontend\modules\product\widgets;

use frontend\modules\product\models\forms\ProductItemFilterForm;
use frontend\modules\product\models\ProductFilter;
use frontend\modules\product\widgets\assets\ProductItemFilterAssets;
use Yii;
use yii\base\Widget;
use yii\helpers\ArrayHelper;

/**
 * Widget to show product reviews
 */
class ProductItemFilters extends Widget
{
    public $product = null;
    public $variationProduct = null;
    public $formAction = null;

    public $groupForm = 'filter';

    public function init() {
        parent::init();
        $this->registerAssets();
    }

    public function run()
    {
        if (
        !(
            $this->variationProduct
            && $this->variationProduct->properties
            && $this->product
            && $this->product->activeProductVariations
            && $this->product->allPropertis
        )
        ) return '';

        $rowVariationProperties = [];
        $filterIds = [];
        $rowCountNotNullVariationProperties = [];

        foreach ($this->product->allPropertis as $propertie) {
            $rowVariationProperties[$propertie->filter_id][$propertie->value] = $propertie->value;
            $rowCountNotNullVariationProperties[$propertie->filter_id] = [];
            $filterIds[$propertie->filter_id] = $propertie->filter_id;
        }

        foreach ($this->product->allPropertisCountNotNull as $propertie) {
            $rowCountNotNullVariationProperties[$propertie->filter_id][$propertie->value] = $propertie->value;
        }

        if (empty($rowVariationProperties)) return '';

        $modelFilters = ArrayHelper::map(ProductFilter::find()->where(['id' => $filterIds])->all(), 'id', function ($model) {
            return $model;
        });

        if (!$modelFilters) return '';

        foreach ($modelFilters as $filter_id => $modelFilter) {
            $filters[$filter_id]['filter'] = $modelFilter;
            $filters[$filter_id]['listValues'] = $rowVariationProperties[$filter_id];
            $filters[$filter_id]['listActiveValues'] = $rowCountNotNullVariationProperties[$filter_id];
        }

        $formFilters = new ProductItemFilterForm();
        $formFilters->load(Yii::$app->request->get());
        if (!$formFilters->validate()) return '';

        return $this->render('product-item-filters', [
            'filters' => $filters,
            'formFilters' => $formFilters,
            'formAction' => $this->formAction,
            'groupForm' => $this->groupForm,
        ]);
    }

    public function registerAssets() {
        $view=$this->getView();
        ProductItemFilterAssets::register($view);
    }
}