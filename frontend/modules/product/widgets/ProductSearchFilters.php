<?php

namespace frontend\modules\product\widgets;

use frontend\modules\product\models\forms\ProductSearchFilterForm;
use frontend\modules\product\models\ProductFilter;
use frontend\modules\product\models\ProductGlobalFilter;
use frontend\modules\product\models\ProductVariation;
use frontend\modules\product\models\ProductVariationPropertie;
use frontend\modules\product\widgets\assets\ProductFilterAssets;
use Yii;
use yii\base\Widget;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Widget to show product reviews
 */
class ProductSearchFilters extends Widget
{
    public $query = null;
    public $formAction = null;

    public $findVariationIds = null;

    public $groupForm = 'filter';

    public function init() {
        parent::init();
        $this->registerAssets();
    }

    public function run()
    {
        if (!$this->query) return '';

        $modelVariations = ProductVariation::find()
            ->with('price')
            ->alias('variations')
            ->joinWith('product product')
            ->where(['variations.active' => 1])
            ->andWhere(['product.active' => 1])
            ->andWhere(['LIKE', 'variations.name', $this->query])
            ->indexBy('id')
            ->all();

        if (!$modelVariations) return '';

        $variationIds = ArrayHelper::map($modelVariations, 'id','id');

        $modelPropertis = ProductVariationPropertie::find()
            ->where(['product_variation_id' => $variationIds])
            ->all();

        if (!$modelPropertis) return '';

        $rowVariationProperties = [];
        $filterIds = [];
        foreach ($modelPropertis as $propertie) {
            $rowVariationProperties[$propertie->filter_id][$propertie->value] = $propertie->value;
            $filterIds[$propertie->filter_id] = $propertie->filter_id;
        }

        $modelFilters = ArrayHelper::map(ProductFilter::find()->where(['id' => $filterIds])->all(), 'id', function ($model) {
            return $model;
        });

        $filters = [];

        if ($this->findVariationIds) {
            $filters = array_merge($filters, ProductGlobalFilter::Factory()->getSearchFilters($this->findVariationIds));
        }

        foreach ($modelFilters as $filter_id => $modelFilter) {
            $filters[$filter_id]['filter'] = $modelFilter;
            $filters[$filter_id]['listValues'] = $rowVariationProperties[$filter_id];
        }

        $formFilters = new ProductSearchFilterForm();
        $formFilters->load(Yii::$app->request->get());
        if (!$formFilters->validate()) return '';

        return $this->render('product-search-filters', [
            'filters' => $filters,
            'formFilters' => $formFilters,
            'formAction' => $this->formAction,
            'groupForm' => $this->groupForm,
        ]);
    }

    public function registerAssets() {
        $view=$this->getView();
        ProductFilterAssets::register($view);
    }
}