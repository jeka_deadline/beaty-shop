<?php

namespace frontend\modules\product\widgets;

use frontend\modules\core\models\Setting;
use Yii;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\modules\product\models\Product;
use frontend\modules\product\models\ProductCategory;
use frontend\modules\product\models\ProductVariation;

class SmallSliderWithProducts extends Widget
{
    public $mainWrapperOptions;

    public $wrapperOptions;

    public $headerOptions;

    public $itemOptions;

    public $typeWidget;

    public $limitItems = 10;

    public $product;

    public $shopNowLink = [
        'template' => '<div class="row justify-content-center align-items-center"><a class="btn btn-primary" href="{link}">{text}</a></div>',
        'link' => '#',
        'text' => 'Shop now',
    ];

    public $itemTemplate = '<a href="{link}" title="{name}"><div class="img">{image}</div><header>{name}</header><div class="price-wrapper">{price}</div><div class="zzz">zzgl. 19% USt.</div></a>';

    private $items;

    const TYPE_BEST_SELLERS = 'best-sellers';

    const TYPE_ALL_RECOMMENDS = 'all-recommends';

    const TYPE_RECOMMENDS_FROM_PRODUCT = 'recommends-from-product';

    public function init()
    {
        parent::init();
        $this->shopNowLink[ 'link' ] = '/shop';
        $this->shopNowLink[ 'text' ] = Yii::t('core', 'Shop now');
        $this->setItems();
    }

    public function run()
    {
        if (empty($this->items) || !is_array($this->items)) {
            return false;
        }

        $headerTitle = ArrayHelper::remove($this->headerOptions, 'title', 'Title');
        $headerTag = ArrayHelper::remove($this->headerOptions, 'tag', 'h3');
        $mainWrapperTag = ArrayHelper::remove($this->mainWrapperOptions, 'tag', 'div');
        $wrapperTag = ArrayHelper::remove($this->wrapperOptions, 'tag', 'div');
        $itemTag = ArrayHelper::remove($this->itemOptions, 'tag', 'div');

        $content = $this->renderHeader($headerTitle, $headerTag);

        $content .= $this->renderItems($wrapperTag, $itemTag);
        $content .= $this->renderShopNowLink();

        if ($mainWrapperTag !== false) {
            $content = Html::tag($mainWrapperTag, $content, $this->mainWrapperOptions);
        }

        echo $content;
    }

    private function renderItems($wrapperTag, $itemTag)
    {
        $content = '';

        foreach ($this->items as $item) {
            $content .= $this->renderItem($item, $itemTag);
        }

        if ($wrapperTag !== false) {
            return Html::tag($wrapperTag, $content, $this->wrapperOptions);
        }

        return $content;
    }

    private function renderItem($item, $itemTag)
    {
        $variation = $item->defaultProductVariation;

        if (!$variation) {
            return null;
        }

        $currency = Setting::value('product.shop.currency');

        $blockPrice = '';

        if ($variation->isHasDoublePrice()) {
            $blockPrice = "<div class='price'>{$variation->doublePrice}</div>";
        } else {
            if ($variation->isDiscount) {
                $blockPrice = "<div class='sale'>{$currency} " . ProductVariation::viewFormatPrice($variation->priceDiscount) . "</div>";
            }

            $blockPrice .= "<div class='price'>{$currency} " .  $variation->viewNumberFormatFullPrice . "</div>";
        }

        $replacedBlocks = [
            '{image}' => Html::img($variation->getImagePreview(170, 170), ['alt' => $variation->name]),
            '{name}' => $variation->name,
            '{price}' => $blockPrice,
            '{link}' => Url::to(['/product/product/index', 'slug' => $item->slug]),
            '{currency}' => Setting::value('product.shop.currency'),
        ];

        $block = strtr($this->itemTemplate, $replacedBlocks);

        if ($itemTag !== false) {
            return Html::tag($itemTag, $block, $this->itemOptions);
        }

        return $block;
    }

    private function renderHeader($headerTitle, $headerTag)
    {
        if ($headerTag !== false) {
            return Html::tag($headerTag, $headerTitle, $this->headerOptions);
        }

        return $headerTitle;
    }

    private function renderShopNowLink()
    {
        if (!empty($this->shopNowLink) && isset($this->shopNowLink[ 'template' ])) {
            $link = ArrayHelper::remove($this->shopNowLink, 'link', '#');
            $text = ArrayHelper::remove($this->shopNowLink, 'text', Yii::t('core', 'Shop now'));

            return strtr($this->shopNowLink[ 'template' ], [
                '{link}' => $link,
                '{text}' => $text,
            ]);
        }

        return '';
    }

    private function setItems()
    {
        switch ($this->typeWidget) {
            case self::TYPE_BEST_SELLERS:
                $this->getBestSellerProducts();
                break;
            case self::TYPE_RECOMMENDS_FROM_PRODUCT:
                $this->getRecommensFromProduct();
                break;
            default:
                $this->getAllRecommends();
                break;
        }
    }

    private function getBestSellerProducts()
    {
        $this->items = Product::find()
            ->with('defaultProductVariation','defaultProductVariation.preview')
            ->joinWith('bestSeller', true, 'JOIN')
            ->where(['active' => 1])
            ->limit($this->limitItems)
            ->all();
    }

    private function getAllRecommends()
    {
        $this->items = Product::find()
            ->with('defaultProductVariation','defaultProductVariation.preview')
            ->joinWith('freeRecommendet', true, 'JOIN')
            ->where(['active' => 1])
            ->limit($this->limitItems)
            ->all();
    }

    private function getRecommensFromProduct()
    {
        $this->items = $this->product->getRecommendets()->with('defaultProductVariation','defaultProductVariation.preview')
            ->where(['active' => 1])
            ->indexBy('id')
            ->orderBy(['id' => SORT_ASC])
            ->limit($this->limitItems)
            ->all();
    }
}