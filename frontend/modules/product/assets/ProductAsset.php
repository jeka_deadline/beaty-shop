<?php

namespace frontend\modules\product\assets;

use yii\web\AssetBundle;

/**
 * Asset for product review.
 */
class ProductAsset extends AssetBundle
{
    /**
     * {@inheritdoc}
     *
     * @var array $js
     */
    public $js = [
        'js/lightslider.min.js',
        'js/jquery.zoom.min.js',
        'js/jquery.barrating.min.js',
    ];

    /**
     * {@inheritdoc}
     *
     * @var array $depends
     */
    public $depends = [
        'yii\web\JqueryAsset',
    ];

    /**
     * {@inheritdoc}
     *
     * @var array $publishOptions
     */
    public $publishOptions = [
        'forceCopy' => (YII_DEBUG) ? true : false,
    ];

    /**
     * {@inheritdoc}. Set source path for this asset.
     *
     * @return void
     */
    public function init()
    {
        parent::init();

        $this->sourcePath = __DIR__ . DIRECTORY_SEPARATOR . 'src';
    }
}
