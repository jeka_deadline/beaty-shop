$(function() {
    var xhrTabProductSet = null;

    $(document).on('click', '.productset-onclick-load-tab', function (e) {

        if (xhrTabProductSet) xhrTabProductSet.abort();

        var button = $(e.target);

        if (button.hasClass('collapsed')) return;

        var tabContents = $('#accordionSets .card .card-body .card-body-block-wrapper');
        tabContents.empty();

        xhrTabProductSet = $.ajax({
            url: 'product/load-tab-set-variation',
            method: 'GET',
            data: {
                id: $(this).attr('data-id')
            },
            dataType: 'JSON',
        }).done(function (data) {
            if (data.status = 'ok') {
                if (document.lightsliderOnItem2 && document.lightsliderOnItem2.hasOwnProperty('destroy')) document.lightsliderOnItem2.destroy();
                button.parents('.card').find('.card-body-block-wrapper').html(data.html);
            }
        });
        xhrTabProductSet.then(function() {
            setTimeout(function() {
                itemSlider2();
            }, 350);
            $('.shop-item-page__item-slider').css('visibility', 'visible');
            var outTwo = $('.shop-item-page__zoom-here-two');

            var out5 = $(document).find('.shop-item-page__zoom-here-two');
            if ($(window).width() > 991) {
                $(document).find('.shop-item-page__zoom-two').parent().zoom({
                    target: out5,
                    magnify: 2,
                    onZoomIn: function () {
                        $(document).find('.shop-item-page__zoom-here-two').css('z-index', '1');
                    },
                    onZoomOut: function () {
                        $(document).find('.shop-item-page__zoom-here-two').css('z-index', '0');
                    }
                });
            }
        })
    });
});