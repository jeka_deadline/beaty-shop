$(function() {
    $(document).on('click', '.link-get-reply-review', function (e) {
        e.preventDefault();

        var self = $(this);

        $.ajax({
            url: $(this).data('url'),
            method: 'GET',
            dataType: 'html',
        }).done(function (data) {
            if (data) {
                $('#block-reviews').find('form').remove();
                $(data).insertAfter($(self).closest('.panel'))
            }
        })
    });

    let flagSubmitProductReview = false;

    $(document).on('beforeSubmit', '#form-create-review', function(e) {
        if (flagSubmitProductReview) return false;
        flagSubmitProductReview = true;

        let form = $(this);

        var button = form.find('button[type=submit]');

        button.attr('disabled', 'disabled');

        $.ajax({
            url: $(form).attr('action'),
            method: $(form).attr('method'),
            data: $(form).serialize(),
        }).done(function(data) {
            showModal(data.html);

            form.yiiActiveForm('resetForm');
            form.find('select').barrating('clear');
            form.trigger('reset');

            $(button).removeAttr('disabled', 'disabled');
            flagSubmitProductReview = false;
        });

        return false;
    });

});