$(function() {
    $('#product-rating').on('rating:change', function(event, value, caption) {
        $.ajax({
            url: $(this).data('url'),
            data: {'value': value},
            dataType: 'json',
        }).done(function(data) {
            if (data.status) {
                $('.rating-container').replaceWith(data.content);
            }
        })
    });
});