$(function() {

    function ajaxGetMaxCountForCart(value) {
        $.ajax({
            url: 'product/max-count',
            method: 'post',
            data: {
                url: $('#form-cart').attr('action')
            },
            dataType: 'json'
        }).done(function(data) {
            if (data.status = 'ok') {
                $('[data-model-attr=count]').val(data.count);
                if (data.count < parseInt(value.val())) value.val(data.count);
            }
        });
    }

    $(document).find('#form-cart button').attr('type', 'submit');

    $('.shop-item-page__quantiti-of-order .minus').click(function (e) {
        var value = $(e.target).parent().find('input[type=text]');
        val = parseInt(value.val()) - 1;
        val = val < 1 ? 1 : val;
        value.val(val);

        if (val>1) ajaxGetMaxCountForCart(value);
        return false;
    });

    $('.shop-item-page__quantiti-of-order .plus').click(function (e) {
        var value = $(e.target).parent().find('input[type=text]');
        val = parseInt(value.val()) + 1;
        if (val > $(document).find('[data-model-attr=count]').val()) {
            return false;
        }

        value.val(val);
        inputPosition();
        ajaxGetMaxCountForCart(value);
        return false;
    });

    // decrement product count on page cart
    $('.cart-page__quantiti-of-order .minus').click(function (event) {
        event.preventDefault();

        var data      = {};
        var priceItem = $(this).closest('.cart-page__item-wrapper').data('price');
        var url       = $(document).find('#cart-items-wrapper').data('minus-count-url');
        var valNow    = $(event.target).parent().find('input');
        var val       = parseInt(valNow.val());
        var id        = $(this).closest('.cart-page__item-wrapper').data('id');

        if (!val) {
            return false;
        }

        data.id = id;
        jQuery.extend(data, getParamsCsrf());

        $.ajax({
            url: url,
            method: 'post',
            data: data,
            dataType: 'json'
        }).done(function(data) {
            if (data.status) {
                valNow.val(data.count);
                inputPosition();

                addProductToPopup(id, data.popupItemTemplate);
                addFreeShippingToPopup(data.popupFreeShipping);
                changeSubTotalPrice(data.totalPriceNotDiscount);
                changeTaxPrice(data.tax);
                changeBruttoPrice(parseFloat(data.totalPrice) + parseFloat(data.tax));
                changeTotalPrice(parseFloat(data.totalPrice) + parseFloat(data.tax) + parseFloat(data.shippingPrice));
                changePriceItemInPopupCart(id, priceItem * data.count);
                updateShippingInformation(data.shippingPrice);
                $('.js-free-shipping-remainder').text(data.freeShipping.remainder);

                changeDiscountPrice(data.discountPrice);
            }
        });
    });

    // increment product count on page cart
    $('.cart-page__quantiti-of-order .plus').click(function (event) {
      event.preventDefault();

        var data      = {};
        var priceItem = $(this).closest('.cart-page__item-wrapper').data('price');
        var url       = $(document).find('#cart-items-wrapper').data('plus-count-url');
        var valNow    = $(event.target).parent().find('input');
        var val       = parseInt(valNow.val());
        var id        = $(this).closest('.cart-page__item-wrapper').data('id');

        if (!val) {
            return false;
        }

        data.id = id;
        jQuery.extend(data, getParamsCsrf());

        $.ajax({
            url: url,
            method: 'post',
            data: data,
            dataType: 'json'
        }).done(function(data) {
            if (data.status) {
                valNow.val(data.count);
                inputPosition();

                addProductToPopup(id, data.popupItemTemplate);
                addFreeShippingToPopup(data.popupFreeShipping);
                changeSubTotalPrice(data.totalPriceNotDiscount);
                changeTaxPrice(data.tax);
                changeBruttoPrice(parseFloat(data.totalPrice) + parseFloat(data.tax));
                changeTotalPrice(parseFloat(data.totalPrice) + parseFloat(data.tax) + parseFloat(data.shippingPrice));
                changePriceItemInPopupCart(id, priceItem * data.count);
                updateShippingInformation(data.shippingPrice);
                $('.js-free-shipping-remainder').text(data.freeShipping.remainder);
                changeDiscountPrice(data.discountPrice);
            }
        });
    });

    // event add product to cart
    $(document).on('submit', '#form-cart', function( e ) {
        e.preventDefault();
        $.ajax({
            url: $(this).attr('action'),
            method: $(this).attr('method'),
            data: $(this).serialize(),
            dataType: 'json'
        }).done(function(data) {
            if (data.status) {
                addProductToPopup(data.variationId, data.popupItemTemplate);
                addFreeShippingToPopup(data.popupFreeShipping);
                changeSubTotalPrice(data.totalPriceNotDiscount);
                changeTotalPrice(parseFloat(data.totalPrice));
                changeTotalPricePopUpCart(parseFloat(data.totalPrice));
                $(document).find('.main-enter__basket > a > p').html('');
                $(document).find('.cart-hover-menu').addClass('show-cart-hover-menu');

                setTimeout(function() {$(document).find('.cart-hover-menu').removeClass('show-cart-hover-menu');}, 2000);

                if ($(window).width() < 991) {
                  event.preventDefault();
                  $(document).find('.added-to-cart-wrapper').css('right', '25px');
                  setTimeout(function() {
                    $(document).find('.added-to-cart-wrapper').css('right', '-100%');
                  }, 3000);
                }
            }
        })
    });

    // event remove product from cart
    $(document).on('click', '.js-remove-product-from-cart', function( e ) {
        e.preventDefault();
        var self = $(this);
        var url = $(document).find('#cart-items-wrapper').data('remove-url');
        var data = {};
        var priceItem = $(this).closest('.cart-page__item-wrapper').data('price');
        var countItem = $(this).closest('.cart-page__quantiti-of-order-edit').find('.cart-page__quantiti-of-order input[type=text]').val();
        var fullPrice = countItem * priceItem;
        var id        = $(this).closest('.cart-page__item-wrapper').data('id');

        data.id = id;

        jQuery.extend(data, getParamsCsrf());

        $.ajax({
            url: url,
            method: 'post',
            data: data,
            dataType: 'json'
        }).done(function(data) {
            if (data.status) {
                addFreeShippingToPopup(data.popupFreeShipping);
                changeSubTotalPrice(data.totalPriceNotDiscount);
                changeTotalPrice(parseFloat(data.totalPrice) + parseFloat(data.tax) + parseFloat(data.shippingPrice));
                updateShippingInformation(data.shippingPrice);
                changeTaxPrice(data.tax);
                changeBruttoPrice(parseFloat(data.totalPrice) + parseFloat(data.tax));
                changeDiscountPrice(data.discountPrice);

                $('.js-free-shipping-remainder').text(data.freeShipping.remainder);

                $(self).closest('.cart-page__item-wrapper').remove();

                changeCountItemsIncart(data.emptyCart);
                removeProductFromPopupCart(id);
            }
        });
    });

    // event apply coupon
    $(document).on('submit', '#coupon-form', function( e ) {
        e.preventDefault();

        $.ajax({
            url: $(this).attr('action'),
            data: $(this).serialize(),
            method: 'post',
            dataType: 'json',
        }).done(function(data) {
            changeSubTotalPrice(data.totalPriceNotDiscount);
            changeTotalPrice(parseFloat(data.totalPrice) + parseFloat(data.tax) + parseFloat(data.shippingPrice));
            updateShippingInformation(data.shippingPrice);
            changeTaxPrice(data.tax);
            changeBruttoPrice(parseFloat(data.totalPrice) + parseFloat(data.tax));
            changeDiscountPrice(data.discountPrice);
            $(document).find('#cart-page__promotional-input').replaceWith(data.html);
            $(document).find('#cart-page__promotional-input').css('display', 'block');
            if ($('#couponform-code').val() != '') {
                $('.clear-input-coupon-code').css("visibility", "visible");
            }
        })
    });

    // Clear Cupon
    $(document).on('click', '.clear-input-coupon-code', function(e) {
        var form = $(this).parents('form');
        form.find('#couponform-code').val('');

        $.ajax({
            url: $(form).attr('data-action-clear'),
            data: $(form).serialize(),
            method: 'post',
            dataType: 'json',
        }).done(function(data) {
            changeSubTotalPrice(data.totalPriceNotDiscount);
            changeTotalPrice(parseFloat(data.totalPrice) + parseFloat(data.tax) + parseFloat(data.shippingPrice));
            updateShippingInformation(data.shippingPrice);
            changeTaxPrice(data.tax);
            changeBruttoPrice(parseFloat(data.totalPrice) + parseFloat(data.tax));
            changeDiscountPrice(data.discountPrice);
        })
    });

    $(document).on('keyup', '#couponform-code', function () {
        $('.clear-input-coupon-code').css("visibility", "visible");
    });
    $(document).on('click', '.clear-input-coupon-code', function () {
        $('#couponform-code').val('');
        $('.clear-input-coupon-code').css("visibility", "hidden");
        $('#couponform-code').focus();
    });

    if ($('#couponform-code').val() != '') {
        $('.clear-input-coupon-code').css("visibility", "visible");
    }

    //End clear cupon

    // change subtotal price
    function changeSubTotalPrice(price)
    {
        price = parseFloat(price).toFixed(2);

        if ($(document).find('#subtotal-price').length) {
            $(document).find('#subtotal-price').text(price);
        }

        return price;
    }

    // change total price
    function changeTotalPrice(price)
    {
        price = parseFloat(price).toFixed(2);
        if ($(document).find('.cart-page__order-total-price p span').length) {
            $(document).find('.cart-page__order-total-price p span').text(price);
        }
    }

    function changeTotalPricePopUpCart(price) {
        price = parseFloat(price).toFixed(2);
        if ($(document).find('.cart-hover-menu__subtotal-price span').length) {
            $(document).find('.cart-hover-menu__subtotal-price span').text(price);
        }
    }

    // change price items in popup cart
    function changePriceItemInPopupCart(id, price)
    {
        price = parseFloat(price).toFixed(2);
        var domItemPrice = $(document).find('.cart-hover-menu__item[data-id="' + id + '"] .cart-hover-menu__item-info-price span');

        if($(domItemPrice).length) {
            $(domItemPrice).text(price);
        }
    }

    function changeBruttoPrice(price)
    {
        price = parseFloat(price).toFixed(2);

        if ($(document).find('#brutto-price').length) {
            $(document).find('#brutto-price').text(price);
        }
    }

    function changeTaxPrice(price)
    {
        price = parseFloat(price).toFixed(2);

        if ($(document).find('#tax-price').length) {
            $(document).find('#tax-price').text(price);
        }
    }

    function changeDiscountPrice(price)
    {
        price = parseFloat(price).toFixed(2);
        if ($(document).find('#discount-price').length) {
            if (parseFloat(price) > 0.01) {
                $(document).find('#discount-price').prev().text('-');
            } else {
                $(document).find('#discount-price').prev().text('');
            }
            $(document).find('#discount-price').text(price);
        }
    }

    // Change piple count product in header
    function changeCountItemsIncart(templateEmptyCart)
    {
        var domCountItemsInCart = $(document).find('.header__numbers-of-orders p');

        if ($(domCountItemsInCart).length) {
            var count = parseInt($(domCountItemsInCart).text());

            count--;

            if (count > 0) {
                $(domCountItemsInCart).text(count);
            } else {
                $(document).find('.header__numbers-of-orders').remove();
                $('body').removeAttr('class').attr('class', 'cart-empty');
                $(document).find('.cart-page__shopping-bag').replaceWith(templateEmptyCart);
                $(document).find('.cart-page__also-like').remove();
                $(document).find('.cart-page__order-wrapper').remove();
                $(document).find('.main-enter__basket > a > p').html('Empty');
            }

            var text = '(' + count + ' artikel)';

            $(document).find('.container.cart-page__shopping-bag > h1 > span').text(text);
        }
    }

    // Remove product from popup cart
    function removeProductFromPopupCart(id)
    {
        var domItem = $(document).find('.cart-hover-menu__item[data-id="' + id + '"]');

        if ($(domItem).length) {
            $(domItem).remove();
        }
    }

    // Get csrf params
    function getParamsCsrf()
    {
        var data = {};
        var param = $('meta[name=csrf-param]').attr("content");
        var token = $('meta[name=csrf-token]').attr("content");

        data[ param ] = token;

        return data;
    }

    function addProductToPopup(id, htmlBlock, basketTemplate)
    {
        var domItem = $(document).find('.cart-hover-menu__item[data-id="' + id + '"]');

        if ($(domItem).length) {
            $(domItem).replaceWith(htmlBlock);

        } else {
            $(document).find('.cart-hover-menu__items').append(htmlBlock);
        }

        var basketDom = $(document).find('.header__numbers-of-orders p');

        if (!$(basketDom).length) {
            $('<div class="header__numbers-of-orders"><p></p></div>').insertAfter($(document).find('.main-enter__basket svg'));
        }

        $(document).find('.header__numbers-of-orders p').text($(document).find('.cart-hover-menu__items > .cart-hover-menu__item').length);
    }

    function addFreeShippingToPopup(htmlBlock)
    {
        var dom = $(document).find('.cart-hover-menu header');

        if (dom.length) {
            dom.html(htmlBlock);
            $('.progress-bar-value').text($('.cart-hover-menu .progress-bar').attr('aria-valuenow') + " %");
        }
    }

    function updateShippingInformation(shippingPrice)
    {
        shippingPrice = parseFloat(shippingPrice).toFixed(2);

        $(document).find('#shipping-price').html(shippingPrice);
    }

});