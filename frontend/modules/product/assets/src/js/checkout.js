$(function() {
    // auto fill user saved address
    $(document).on('change', '#checkout-page__select-home', function( e ) {
        e.preventDefault();

        $.ajax({
            url: $(this).data('url'),
            method: 'post',
            data: $(document).find('#order-shipping-form').serialize(),
            dataType: 'json',
            headers: {
                'X-CSRF-Token': yii.getCsrfToken(),
            },
        }).done(function(data) {
            if (data.status) {
                $(document).find('#order-shipping-form').replaceWith(data.html);
            }

            $(document).find('.checkout-page__aside-total-wrapper .checkout-page__aside-num span').html(data.total);
            $(document).find('.checkout-page__aside-shippeng-wrapper .checkout-page__aside-num span').html(data.shipping.toFixed(2));
            $(document).find('.checkout-page__when-get').html(data.shippingDate);

        });
    });

    // submit shipping form and save
    $(document).on('submit', '#order-shipping-form', function( e ) {
        e.preventDefault();
        var form = $(this);

        $.ajax({
            url: $(this).attr('action'),
            data: $(this).serialize(),
            method: $(this).attr('method'),
            dataType: 'json',
            headers: {
                'X-CSRF-Token': yii.getCsrfToken(),
            },
        }).done(function(data) {
            if (data.status) {
                $(document).find('.checkout-page__form-wrapper.first-form-wrapper .checkout-page__blocks-black-header a').css('display', 'block');
                if (!$(document).find('.checkout-page__form-wrapper.first-form-wrapper .checkout-page__blocks-black-header svg.green_ok').length) {
                    $('<svg class="green_ok svg"><use xlink:href="#green_ok"></use></svg>').insertBefore($(document).find('.checkout-page__form-wrapper.first-form-wrapper .checkout-page__blocks-black-header h6'));
                }

                $(document).find('.checkout-page__form-wrapper.first-form-wrapper .checkout-page__blocks-black-header h6').addClass('with-green-ok');
                $(form).replaceWith(data.html);

                if (data.billing_and_payment_form) {
                    $(document).find('.checkout-page__form-wrapper.second-form-wrapper').children('.checkout-page__form').html(data.billing_and_payment_form);
                    var wrapper = $(document).find('.checkout-page__form-wrapper.third-form-wrapper');
                    if ($(wrapper).children('.checkout-page__form').length) {
                        $(wrapper).children('.checkout-page__form').remove();
                    }
                    if ($(wrapper).children('.justify-content-end').length) {
                        $(wrapper).children('.justify-content-end').remove();
                    }
                    $(document).find('.checkout-page__form-wrapper.second-form-wrapper .checkout-page__blocks-black-header a').css('display', 'none');
                }
            }
        });
    });

    $(document).on('submit', '#order-review-form', function( e ) {
        $(this).find('[type=submit]').attr('disabled', 'disabled');
    });

    $(document).on('change', '#checkout-page__country-add', function( e ) {
        e.preventDefault();

        $.ajax({
            url: $(this).data('url'),
            method: 'get',
            data: {countryId: $(this).val()}
        }).done(function(data) {
            console.log(data);
            $(document).find('.checkout-page__aside-total-wrapper .checkout-page__aside-num span').html(data.total);
            $(document).find('.checkout-page__aside-shippeng-wrapper .checkout-page__aside-num span').html(data.shipping.toFixed(2));
            $(document).find('.checkout-page__when-get').html(data.shippingDate);
            $(document).find('#shipping-methods').html(data.shippingMethodsBlock);
        });
    });

    // submit payment and billing forms and save
    $(document).on('submit', '#order-payment-form', function( e ) {
        e.preventDefault();

        var formData = $(this).serializeArray();

        if (formData[ 4 ].value !== 'credit') {
            sendPaymentForm();

            return;
        }

        if (formData[ 5 ] && formData[ 5 ].value !== '') {
            sendPaymentForm();

            return;
        }

        var checkResult = check();
        if (checkResult === false) {
            return false;
        }
    });

    // click link edit shipping form
    $(document).on('click', '.checkout-page__form-wrapper.first-form-wrapper .checkout-page__blocks-black-header a', function( e ) {
        e.preventDefault();

        $.ajax({
            url: $(this).attr('href'),
            method: 'get',
            dataType: 'json'
        }).done(function(data) {
            if (data.status) {
                $(document).find('.checkout-page__form-wrapper.first-form-wrapper .checkout-page__form-ready').replaceWith(data.html);
            }
        });
    });

    // click link edit billing and payment form
    $(document).on('click', '.checkout-page__form-wrapper.second-form-wrapper .checkout-page__blocks-black-header a', function( e ) {
        e.preventDefault();

        $.ajax({
            url: $(this).attr('href'),
            method: 'get',
            dataType: 'json'
        }).done(function(data) {
            if (data.status) {
                $(document).find('.checkout-page__form-wrapper.second-form-wrapper .checkout-page__form-ready').replaceWith(data.html);
            }
        });
    });

    // auto fill user saved payment card
    $(document).on('change', '#checkout-page__select-card', function( e ) {
        var self = $(this);
        $.ajax({
            url: $(this).data('url'),
            method: 'post',
            data: $(document).find('#order-payment-form').serialize(),
            dataType: 'json',
            headers: {
                'X-CSRF-Token': yii.getCsrfToken(),
            },
        }).done(function(data) {
            if (data.status) {
                $(document).find('#iframe').next().remove();
                $(document).find('#iframe').remove();
                $(data.html).insertAfter($(self).parent());
            } else {
                $(document).find('#block-payment-credit-card').html(data.html);
            }

            $(document).find('#pseudocardpan').val(data.pseudocardpan);
            $(document).find('#truncatedcardpan').val(data.number);
        });
    });

    // change payment type radio button
    $(document).on('change', '#select-payment-type-js input', function( e ) {
        $.ajax({
            url: $(document).find('#select-payment-type-js').data('url'),
            method: 'get',
            data: {type: $(this).val()}
        }).done(function(response) {
            console.log(response);
            $(document).find('#block-payment-credit-card').html(response);
        });
    });

    // change shipping method
    $(document).on('change', '#shipping-method-js input', function( e ) {
        $.ajax({
            url: $(document).find('#shipping-method-js').data('url'),
            method: 'post',
            data: $(document).find('#order-shipping-form').serialize(),
            dataType: 'json',
            headers: {
                'X-CSRF-Token': yii.getCsrfToken(),
            },
        }).done(function(data) {
            $(document).find('.checkout-page__aside-total-wrapper .checkout-page__aside-num span').html(data.total);
            $(document).find('.checkout-page__aside-shippeng-wrapper .checkout-page__aside-num span').html(data.shipping.toFixed(2));
            $(document).find('.checkout-page__when-get').html(data.shippingDate);
        });
    });
});

function check() { // Function called by submitting PAY-button
    if (window.iframes.isComplete()) {
        window.iframes.creditCardCheck('checkCallback'); // Perform "CreditCardCheck" to create and get a
        // PseudoCardPan; then call your function
        "checkCallback"
    } else {
        return false;
    }
}

function checkCallback(response) {
    console.debug(response);
    if (response.status === "VALID") {
        document.getElementById("pseudocardpan").value = response.pseudocardpan;
        document.getElementById("truncatedcardpan").value = response.truncatedcardpan;
        sendPaymentForm();
    }
}

function sendPaymentForm()
{
    var form = $(document).find('#order-payment-form');

    $.ajax({
        url: $(form).attr('action'),
        data: $(form).serialize(),
        method: $(form).attr('method'),
        dataType: 'json',
        headers: {
            'X-CSRF-Token': yii.getCsrfToken(),
        },
    }).done(function(data) {
        if (data.status) {
            $(document).find('.checkout-page__form-wrapper.second-form-wrapper .checkout-page__blocks-black-header a').css('display', 'block');

            if (!$(document).find('.checkout-page__form-wrapper.second-form-wrapper .checkout-page__blocks-black-header svg.green_ok').length) {
                $('<svg class="green_ok svg"><use xlink:href="#green_ok"></use></svg>').insertBefore($(document).find('.checkout-page__form-wrapper.second-form-wrapper .checkout-page__blocks-black-header h6'));
            }

            $(document).find('.checkout-page__form-wrapper.second-form-wrapper .checkout-page__blocks-black-header h6').addClass('with-green-ok');

            $(form).replaceWith(data.html);
            var wrapper = $(document).find('.checkout-page__form-wrapper.third-form-wrapper');
            if (!$(wrapper).children('.checkout-page__form').length) {
                $(wrapper).append(data.order_review);
            }
        }
    });
}