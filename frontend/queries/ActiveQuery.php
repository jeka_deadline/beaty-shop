<?php

namespace frontend\queries;

use yii\db\ActiveQuery as YiiActiveQuery;

class ActiveQuery extends YiiActiveQuery
{
    public function active()
    {
        $this->andWhere(['active' => 1]);

        return $this;
    }
}
