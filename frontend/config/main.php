<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        'log',
        [
            'class' => 'frontend\components\LanguageSelector',
        ],
    ],
    'controllerNamespace' => 'frontend\controllers',
    'layoutPath' => '@app/modules/core/views/layouts',
    'sourceLanguage' => 'en',
    'language' => 'de',
    'modules' => [
        'academy' => [
            'class' => 'frontend\modules\academy\Module'
        ],
        'core' => [
            'class' => 'frontend\modules\core\Module',
        ],
        'product' => [
            'class' => 'frontend\modules\product\Module',
        ],
        'user' => [
            'class' => 'frontend\modules\user\Module',
        ],
        'geo' => [
            'class' => 'frontend\modules\geo\Module',
        ],
        'pages' => [
            'class' => 'frontend\modules\pages\Module',
        ],
        'blog' => [
            'class' => 'frontend\modules\blog\Module',
        ],
    ],
    'components' => [
        'user' => [
            'identityClass' => 'frontend\modules\user\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => ['user/security/login'],
            'identityCookie' => [
                'name' => '_frontendUser', // unique for frontend
                'path'=>'/frontend/web'  // correct path for the frontend app.
            ],
        ],
        'userCountry' => [
            'class' => '\frontend\modules\core\components\UserCountry',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'reCaptcha' => [
            'name' => 'reCaptcha',
            'class' => 'himiklab\yii2\recaptcha\ReCaptcha',
            'siteKey' => $params[ 'recaptcha-site-key' ],
            'secret' => $params[ 'recaptcha-secret' ],
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'facebook' => [
                    'class' => '\yii\authclient\clients\Facebook',
                    'clientId' => $params[ 'facebook-client-id' ],
                    'clientSecret' => $params[ 'facebook-client-secret' ],
                ],
                'google' => [
                    'class' => '\yii\authclient\clients\Google',
                    'clientId' => $params[ 'google-client-id' ],
                    'clientSecret' => $params[ 'google-client-secret' ],
                    'returnUrl' => $params[ 'google-redirect-url' ],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'core/index/error',
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
            'savePath' => __DIR__ . '/../runtime', // a temporary folder on frontend
        ],
        'request' => [
            'baseUrl' => '',
        ],
        'assetManager' => [
            'bundles' => [
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [],
                ],
                'yii\web\JqueryAsset' => [
                    'sourcePath' => null,
                    'basePath' => '@webroot',
                    'baseUrl' => '@web',
                    'js' => [
                        'js/jquery.min.js',
                    ],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '' => 'core/index/index',
                '/shop' => 'product/product-category/shop',
                '/cart' => 'product/cart/index',
                '/shop/news' => 'product/product-category/news',
                '/search' => 'product/product/search',
                '/trainer-distributor' => '/core/trainer-distributor/index',
                '/thanks' => '/product/checkout/thanks',
                '/academy' => '/academy/academy/index',
                '/careers' => 'core/careers/index',
                '/find-studio' => 'core/find-studio/index',
                '/blog' => '/blog/article/index',
                '/login' => '/user/security/login',
                '/contacts' => '/core/index/contact-us',
                '/sitemap.xml' => '/core/index/sitemap',
                '/course/<slug:[\w\d_-]+>' => '/academy/academy/course',
                '/page/<uri:[\w\d_-]+>' => '/pages/index/page',
                '/product/<slug:[\w\d_-]+>' => 'product/product/index',
                '/blog/<slug:[\w\d_-]+>' => '/blog/article/article',
                [
                    'class' => 'frontend\components\ProductUrlRule',
                ],

            ],
        ],
        'i18n' => [
            'translations' => [
                'core' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    'fileMap' => [
                        'core' => 'core.php',
                    ],
                ],
                'academy' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    'fileMap' => [
                        'academy' => 'academy.php',
                    ],
                ],
                'product' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    'fileMap' => [
                        'product' => 'product.php',
                    ],
                ],
                'user' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    'fileMap' => [
                        'user' => 'user.php',
                    ],
                ],
                'shipping' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    'fileMap' => [
                        'shipping' => 'shipping.php',
                    ],
                ],
                'payment' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    'fileMap' => [
                        'payment' => 'payment.php',
                    ],
                ],
                'blog' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    'fileMap' => [
                        'blog' => 'blog.php',
                    ],
                ],
            ],
        ],
        'view' => [
            'class' => 'frontend\modules\core\components\View',
        ],

        'shortcodes' => [
            'class' => 'tpoxa\shortcodes\Shortcode',
            'callbacks' => [
                'blog_slider' => ['frontend\modules\blog\widgets\ShortCodeBlogSlider', 'widget'],
            ]
        ],
    ],
    'params' => $params,
];
