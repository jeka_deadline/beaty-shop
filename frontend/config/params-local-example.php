<?php
return [
    'recaptcha-site-key' => 'recaptcha site key',
    'recaptcha-secret' => 'recaptcha secret',
    'facebook-client-id' => 'facebook client id',
    'facebook-client-secret' => 'facebook client secret',
    'google-client-id' => 'google client id',
    'google-client-secret' => 'google client secret',
    'google-redirect-url' => 'google redirect url',
    'adminEmail' => 'admin@example.com',
    'klarna' => [
        'MERCHANT_ID' => 'PK02313_234d226580e9',
        'SHARED_SECRET' => 'u61nrgY6tsNQVKak',
    ]
];
