<?php

$config = [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'cookieValidationKey',
        ],
        'paypal'=> [
            'class'        => 'frontend\components\Paypal',
            'clientId'     => 'paypal client id',
            'clientSecret' => 'paypal client secret',
             // This is config file for the PayPal system
             'config'       => [
                  'http.ConnectionTimeOut' => 30,
                  'http.Retry'             => 1,
                  'http.CURLOPT_SSL_VERIFYHOST' => false,
                  'http.CURLOPT_SSL_VERIFYPEER' => false,
                  'mode'                   => 'sandbox',
                  'log.LogEnabled'         => YII_DEBUG ? 1 : 0,
                  'log.FileName'           => Yii::getAlias('@frontend/runtime/logs/paypal.log'),
                  'log.LogLevel'            => 'ERROR',
            ]
        ],
    ],
];

if (!YII_ENV_TEST) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
