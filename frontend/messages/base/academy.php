<?php
return [
    'this month' => 'this month',
    'this year' => 'this year',
    'this week' => 'this week',
    'Sort by' => 'Sort by',
    'learn more' => 'learn more',
    'Duration' => 'Duration',
    'Register' => 'Register',
    'Final Information' => 'Final Information',
    'Date' => 'Date',
    'Location' => 'Location',
    'Trainer' => 'Trainer',
    'Free Seats' => 'Free Seats',
    'read completely' => 'read completely',
    'Infinity Lashes Academy' => 'Infinity Lashes Academy',
    'Courses, Events, Education' => 'Courses, Events, Education',
];