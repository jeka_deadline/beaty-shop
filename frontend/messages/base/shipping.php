<?php

return [
    'Add new' => 'Add new',
    'My adresses' => 'My adresses',
    'Select the primary' => 'Select the primary',
    'Edit' => 'Edit',
    'Delete' => 'Delete',
    'Add shiping address' => 'Add shipping address',
    'Add address line' => 'Add address line',
    'Remove address line' => 'Remove address line',
    'Update shipping address' => 'Update shipping address',
    'Fast delivery about 1-3 working days' => 'Fast delivery about 1-3 working days',
    'Delivery the next working day when ordering until 15:00' => 'Delivery the next working day when ordering until 15:00',
    'Shipping Speed' => 'Shipping Speed',
    'Pick up the next working day from 10am to 6pm in our studio. Kaiserstrasse 59, 44135 Dortmund' => 'Pick up the next working day from 10am to 6pm in our studio. Kaiserstrasse 59, 44135 Dortmund',
];