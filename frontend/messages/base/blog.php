<?php

return [
    'News, articles and everything you wanted to know' => 'News, articles and everything you wanted to know',
    'Previous Article' => 'Previous Article',
    'Back to Blog' => 'Back to Blog',
    'Next Article' => 'Next Article',
    'Previous' => 'Previous',
    'Next' => 'Next',
    'Related articles' => 'Related articles',
];