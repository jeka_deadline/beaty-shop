<?php
return [
    'Find a Studio' => 'Find a Studio',
    'Find Studio' => 'Find Studio',
    'Sign in' => 'Sign in',
    'Join' => 'Join',
    'Empty' => 'Empty',
    'Shop' => 'Shop',
    'Academy' => 'Academy',
    'Blog' => 'Blog',
    'About us' => 'About us',
    'Contact us' => 'Contact us',
    'Contact Us' => 'Contact Us',
    'Shop now' => 'Shop now',
    'see more' => 'see more',
    'best-sellers' => 'best sellers',
    'latest article' => 'latest article',
    'All Articles' => 'All Articles',
    'Recommended for you' => 'Recommended for you',
    'Back to the top' => 'back to the top',
    'Customer support' => 'Customer support',
    'Subscribe' => 'Subscribe',
    'Shipping' => 'Shipping',
    'Return' => 'Return',
    'Terms and Conditions' => 'Terms and Conditions',
    'Cookie policies' => 'Cookie policies',

    'Customer Service' => 'Customer Service',
    'General Enquiries' => 'General Enquiries',
    'Mrs.' => 'Mrs.',
    'Mr.' => 'Mr.',
    'First Name' => 'First Name',
    'Last Name' => 'Last Name',
    'Company' => 'Company',
    'Your Message' => 'Your Message',
    'Send message' => 'Send message',
    'Attach file' => 'Attach file',

    'Custumer support' => 'Custumer support',
    'The company' => 'The company',
    'Legal' => 'Legal',
    'Find us on' => 'Find us on',
    'Family Brand' => 'Family Brand',
    'Delivery and Cost' => 'Delivery and Cost',

    'Search' => 'Search',
    'Careers' => 'Careers',
    'New courses from infinity lashes' => 'New courses from infinity lashes',
    'Become a trainer' => 'Become a trainer',
    'Become a distributor' => 'Become a distributor',
    'You can attach your resume or other documents. The file size should not be more than 5MB' => 'You can attach your resume or other documents. The file size should not be more than 5MB',
    'Submit' => 'Submit',
    'E-mail Address' => 'E-mail Address',
    'Page not found' => 'Page not found',
    'We apologise, we cannot find the page you are looking for. It has either been moved, deleted or no longer exists. Please contact our Client Services or navigate to another page. Thank you.' => 'We apologise, we cannot find the page you are looking for. It has either been moved, deleted or no longer exists. Please contact our Client Services or navigate to another page. Thank you.',
    'homepage' => 'homepage',
    'client services' => 'client services',
    'Tax number 1' => 'Tax number 1',
    'Tax number 2' => 'Tax number 2',
    'Free shipping' => 'Free shipping',
    'Buy another {0} and get free shipping' => 'Buy another {0} and get free shipping',
    'City, country' => 'City, country',
    'You can add or update your Infiniti Lashes Payment Method at any time.' => 'You can add or update your Infiniti Lashes Payment Method at any time.',
    'Save' => 'Save',
    'To give you the best possible experience, this  site use cookies. By continuing you are giving your consent to cookies being used. To find out more or adjust cooking settings, <a href="{0}">click here.</a>' => 'To give you the best possible experience, this  site use cookies. By continuing you are giving your consent to cookies being used. To find out more or adjust cooking settings, <a href="{0}">click here.</a>',
    'Let’s keep the conversation going' => 'Let’s keep the conversation going',
    'Receive our newsletter and discover our products and surprises.' => 'Receive our newsletter and discover our products and surprises.',
    'By subscribing to our newsletter, you agree that your data will be processed in compliance with our <a href="{0}">General Terms and Conditions of Use</a> and our <a href="{1}">Privacy Policy</a>.' => 'By subscribing to our newsletter, you agree that your data will be processed in compliance with our <a href="{0}">General Terms and Conditions of Use</a> and our <a href="{1}">Privacy Policy</a>',
    'Our team of staff is available Monday to Saturday from 9:00 am to 8:00 pm (Dortmund time) to answer your questions' => 'Our team of staff is available Monday to Saturday from 9:00 am to 8:00 pm (Dortmund time) to answer your questions',
    'You can also write to us using the form below. Please complete all fields marked with an *.' => 'You can also write to us using the form below. Please complete all fields marked with an *.',
    'Select your language' => 'Select your language',
    'Checkout' => 'Checkout',
    'Edit' => 'Edit',
    'Select saved address' => 'Select saved address',
    'Choose saved address' => 'Choose saved address',
    'Your billing information' => 'Your billing information',
    'You have indicated the same shipping and billing address. If you want to change your choice, uncheck the box \'Use this address for billing\' and fill out the form.' => 'You have indicated the same shipping and billing address. If you want to change your choice, uncheck the box \'Use this address for billing\' and fill out the form.',
    'select payment method' => 'select payment method',
    'in your cart' => 'in your cart',
    'select shipping speed' => 'select shipping speed',
    'Save Address for later use' => 'Save Address for later use',
    'Select saved Credit Card' => 'Select saved Credit Card',
    'add card' => 'add card',
    'Choose saved payment card' => 'Choose saved payment card',
    'Credit or Debit Card' => 'Credit or Debit Card',
    'Save Credit Card for later use' => 'Save Credit Card for later use',
    'your order information' => 'your order information',
    'By clicking the Place Order button, you confirm that you have read and understood, and accept our <a href="{0}">AGB</a>, <a href="{1}">Return Policy</a>, and <a href="{2}">Private Policy</a>' => 'By clicking the Place Order button, you confirm that you have read and understood, and accept our <a href="{0}">AGB</a>, <a href="{1}">Return Policy</a>, and <a href="{2}">Private Policy</a>',
    'place order' => 'place order',
    'Woosh! Here are the results for' => 'Woosh! Here are the results for',
    'Sorry, we didn’t find any matches for' => 'Sorry, we didn’t find any matches for',
    'Check your spelling or use a more general term to try your search again.' => 'Check your spelling or use a more general term to try your search again.',
    'If you continue to have problems:' => 'If you continue to have problems:',
    '<a href="{0}">Provide feedback</a> to help improve our search' => '<a href="{0}">Provide feedback</a> to help improve our search',
    'Frequently Searched:' => 'Frequently Searched:',
    'Payment Method' => 'Payment Method',
    'Billing Details' => 'Billing Details',
    'Get it by' => 'Get it by',
    'Sunday' => 'Sunday',
    'Monday' => 'Monday',
    'Tuesday' => 'Tuesday',
    'Wednesday' => 'Wednesday',
    'Thursday' => 'Thursday',
    'Friday' => 'Friday',
    'Saturday' => 'Saturday',
    'January' => 'January',
    'February' => 'February',
    'March' => 'March',
    'April' => 'April',
    'May' => 'May',
    'June' => 'June',
    'July' => 'July',
    'August' => 'August',
    'September' => 'September',
    'October' => 'October',
    'November' => 'November',
    'December' => 'December',
    'Security' => 'Security',
    'Pay' => 'Pay',
    'Defective delivery' => 'Defective delivery',
    'Defect' => 'Defect',
    'Others' => 'Others',
    'Not set checkbox agree rules <a href="0">AGB</a> and <a href="{1}">Cookie</a>' => 'Not set checkbox agree rules <a href="0">AGB</a> and <a href="{1}">Cookie</a>',
    'Load more' => 'Load more',
    'Thank you so much! Your letter has been successfully sent, we will contact you as soon as possible.' => 'Thank you so much! Your letter has been successfully sent, we will contact you as soon as possible.',
    'Thank You! You are now signed up for the Infinity Lashes newsletter as <span>{0}</span>' => 'Thank You! You are now signed up for the Infinity Lashes newsletter as <span>{0}</span>',
    'Your application is accepted, we will contact you as soon as possible' => 'Your application is accepted, we will contact you as soon as possible',
    'The checkbox must be checked' => 'The checkbox must be checked',
    'Request' => 'Request',
    'Your token is re-created, check your email' => 'Your token is re-created, check your email',
    'Information message' => 'Information message',
];