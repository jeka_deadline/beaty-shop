<?php

return [
    'Add new' => 'Add new',
    'My cards' => 'My cards',
    'Select the primary' => 'Select the primary',
    'Edit' => 'Edit',
    'Delete' => 'Delete',
    'Add card' => 'Add card',
    'You can add or update your Infiniti Lashes Payment Method at any time.' => 'You can add or update your Infiniti Lashes Payment Method at any time.',
];