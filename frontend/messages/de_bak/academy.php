<?php
return [
  'this month' => 'in diesem Monat',
  'this year' => 'in diesem Jahr',
  'this week' => 'diese Woche',
  'Sort by' => 'Sortieren',
  'learn more' => 'mehr erfahren',
  'Duration' => 'Dauer',
  'Register' => 'Anmelden',
  'Final Information' => 'Zusammenfassung',
  'Date' => 'Datum',
  'Location' => 'Ort',
  'Trainer' => 'Trainer',
  'Free Seats' => 'Freie Plätze',
  'read completely' => 'weiterlesen',
  'Infinity Lashes Academy' => 'Infinity Lash Artist Academy',
  'Courses, Events, Education' => 'Schulungen, Events, Aus- und Weiterbildung',
];