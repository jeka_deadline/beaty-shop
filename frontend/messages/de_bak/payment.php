<?php

return [
    'Add new' => 'Eine Karte hinzufügen',
    'My cards' => 'Meine Kreditkarten',
    'Select the primary' => 'Als standard festlegen',
    'Edit' => 'Bearbeiten',
    'Delete' => 'Löschen',
    'Add card' => 'Kreditkarte hinzufügen',
    'You can add or update your Infiniti Lashes Payment Method at any time.' => 'Kreditkarten löschen bzw. hinzufügen',
];