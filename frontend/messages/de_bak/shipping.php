<?php

return [
    'Add new' => 'Adresse hinzufügen',
    'My adresses' => 'Meine Adressen',
    'Select the primary' => 'Als standat festlegen',
    'Edit' => 'Bearbeiten',
    'Delete' => 'Löschen',
    'Add shipping address' => 'Versandadresse hinzufügen',
    'Add address line' => 'Adresszeile hinzufügen',
    'Remove address line' => 'Adresszeile löschen',
    'Update shipping address' => 'Versandadresse aktualisieren',
    'Fast delivery about 1-3 working days' => 'Schnelle Lieferung ca. 1-3 Werktage',
    'Delivery the next working day when ordering until 15:00' => 'Zustellung am nächsten Werktag bei der Bestellung bis 15:00',
    'Shipping Speed' => 'Versandart',
    'Pick up the next working day from 10am to 6pm in our studio. Kaiserstrasse 59, 44135 Dortmund' => 'Abholbereit am nächsten Werktag von 10 bis 18Uhr in unserem Studio. Kaiserstraße 59, 44135 Dortmund',
];