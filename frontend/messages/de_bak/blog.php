<?php

return [
    'News, articles and everything you wanted to know' => 'Neuigkeiten, Angebote, sowie Tipps & Tricks, die Sie schon immer wissen wollten.',
    'Previous Article' => 'zurück',
    'Back to Blog' => 'zurück zum Blog',
    'Next Article' => 'weiterlesen',
    'Previous' => 'zurück',
    'Next' => 'weiter',
    'Related articles' => 'ähnliche Artikel',
];