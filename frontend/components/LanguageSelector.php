<?php
namespace frontend\components;

use frontend\modules\core\models\Language;
use Yii;
use yii\base\BootstrapInterface;

class LanguageSelector implements BootstrapInterface
{
    public $supportedLanguages = [];

    public function bootstrap($app)
    {
        $preferredLanguage = isset($app->request->cookies['language']) ? (string)$app->request->cookies['language'] : $app->language;

        if (empty($preferredLanguage)) {
            $preferredLanguage = $app->request->getPreferredLanguage($this->supportedLanguages);
        }

        $app->language = $preferredLanguage;
        $this->saveSessionInfoLang($app->language);
    }

    public function saveSessionInfoLang($code) {
        $session = Yii::$app->session;

        if (isset($session['language']) && $session['language']['code'] == $code) return;

        $language = Language::find()
            ->where(['code' => $code])
            ->one()
            ->toArray();

        if ($language) {
            $session['language'] = $language;
        }
    }
}