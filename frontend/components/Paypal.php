<?php

namespace frontend\components;

use yii\base\Component;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;

/**
 * Paypal component.
 */
class Paypal extends Component
{
    /**
     * Client paypal id.
     *
     * @var string $clientId
     */
    public $clientId;

    /**
     * Client paypal secret.
     *
     * @var string $clientSecret
     */
    public $clientSecret;

    /**
     * Config for paypal lib.
     *
     * @var array $config
     */
    public $config;

    /**
     * Paypal api context.
     *
     * @var \PayPal\Rest\ApiContext $apiContext
     */
    private $apiContext;

    /**
     * Init component.
     *
     * @return void
     */
    public function init()
    {
        $this->apiContext = new ApiContext(new OAuthTokenCredential($this->clientId, $this->clientSecret));

        if (!empty($this->config)) {
            $this->apiContext->setConfig($this->config);
        }
    }

    /**
     * Get paypal api context.
     *
     * @return \PayPal\Rest\ApiContext
     */
    public function getApiContext()
    {
        return $this->apiContext;
    }
}