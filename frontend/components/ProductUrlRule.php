<?php

namespace frontend\components;

use yii\helpers\Url;
use yii\web\UrlRuleInterface;
use yii\base\BaseObject;
use frontend\modules\product\models\ProductCategory;
use frontend\modules\product\models\Product;

class ProductUrlRule extends BaseObject implements UrlRuleInterface
{
    public function createUrl($manager, $route, $params)
    {
        if (isset($params['slug']) && ($route == 'product/product-category/index')) {
            $slug = '/'.$params['slug'];
            unset($params['slug']);
            return Url::to(array_merge([$slug], $params));
        }

        return false;
    }

    public function parseRequest($manager, $request)
    {
        $pathInfo     = $request->getPathInfo();
        $lastPosSlash = strrpos($pathInfo, '/');

        if ($lastPosSlash !== false) {
            $slug = substr($pathInfo, strrpos($pathInfo, '/') + 1);
        } else {
            $slug = $pathInfo;
        }

        $category = ProductCategory::findOne(['slug' => $slug]);

        if ($category) {
            return ['product/product-category/index', ['slug' => $pathInfo]];
        }

        return false;
    }
}