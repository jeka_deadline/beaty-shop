<?php
namespace frontend\components;

use common\models\price_book\UseridGroup;
use yii\base\Component;

class GetUserIdGroup extends Component{
    

    public function init(){
        parent::init();
        
    }

    public function getId($user_id){
        $group = UseridGroup::find()->where(['user_id'=>$user_id])->one();
        return $group->group_id;
    }

}
?>