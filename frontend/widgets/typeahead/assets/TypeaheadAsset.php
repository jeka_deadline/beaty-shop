<?php

namespace frontend\widgets\typeahead\assets;

use yii\web\AssetBundle;

class TypeaheadAsset extends AssetBundle
{
    public $sourcePath = '@frontend/widgets/typeahead/assets/dist';

    public $css = [
        'css/typeahead.css',
        'css/typeahead-kv.css',
        'css/typeaheadjs.css',
    ];

    public $js = [
        'js/typeahead.bundle.js',
        'js/typeahead-kv.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
        'frontend\widgets\bootstrap4\BootstrapAsset',
        'frontend\widgets\bootstrap4\BootstrapPluginAsset',
    ];

    public $publishOptions = [
        'forceCopy' => (YII_DEBUG) ? true : false,
    ];
}
