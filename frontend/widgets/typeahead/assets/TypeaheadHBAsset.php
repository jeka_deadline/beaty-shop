<?php

namespace frontend\widgets\typeahead\assets;

use yii\web\AssetBundle;

class TypeaheadHBAsset extends AssetBundle
{
    public $sourcePath = '@frontend/widgets/typeahead/assets/dist';

    public $js = [
        'js/handlebars.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
        'frontend\widgets\bootstrap4\BootstrapAsset',
        'frontend\widgets\bootstrap4\BootstrapPluginAsset',
    ];

    public $publishOptions = [
        'forceCopy' => (YII_DEBUG) ? true : false,
    ];
}
