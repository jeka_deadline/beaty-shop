<?php

/**
 * @package   yii2-krajee-base
 * @author    Kartik Visweswaran <kartikv2@gmail.com>
 * @copyright Copyright &copy; Kartik Visweswaran, Krajee.com, 2014 - 2017
 * @version   1.8.9
 */

namespace frontend\widgets\typeahead\assets;

use Yii;
use yii\web\AssetBundle;

/**
 * Common base widget asset bundle for all Krajee widgets
 *
 * @author Kartik Visweswaran <kartikv2@gmail.com>
 * @since 1.0
 */
class WidgetAsset extends AssetBundle
{
    public $sourcePath = '@frontend/widgets/typeahead/assets/dist';

    public $css = [
        'css/kv-widgets.css',
    ];

    public $js = [
        'js/kv-widgets.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
        'frontend\widgets\bootstrap4\BootstrapAsset',
        'frontend\widgets\bootstrap4\BootstrapPluginAsset',
    ];

    public $publishOptions = [
        'forceCopy' => (YII_DEBUG) ? true : false,
    ];
}
