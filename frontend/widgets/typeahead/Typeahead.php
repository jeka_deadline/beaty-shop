<?php

namespace frontend\widgets\typeahead;

use frontend\widgets\bootstrap4\Html;
use frontend\widgets\typeahead\assets\TypeaheadAsset;
use frontend\widgets\typeahead\assets\TypeaheadHBAsset;
use frontend\widgets\typeahead\assets\WidgetAsset;
use yii\base\InvalidConfigException;
use yii\web\View;

class Typeahead extends \kartik\typeahead\Typeahead
{
    /**
     * Runs the widget
     *
     * @return string|void
     * @throws \yii\base\InvalidConfigException
     */
    public function run()
    {
        if (empty($this->dataset) || !is_array($this->dataset)) {
            throw new InvalidConfigException("You must define the 'dataset' property for Typeahead which must be an array.");
        }
        if (!is_array(current($this->dataset))) {
            throw new InvalidConfigException("The 'dataset' array must contain an array of datums. Invalid data found.");
        }
        $this->_defaultSuggest = is_array($this->defaultSuggestions) && !empty($this->defaultSuggestions);
        if ($this->_defaultSuggest) {
            $this->pluginOptions['minLength'] = 0;
        }
        $this->validateConfig();
        $this->initDataset();
        $this->registerAssets();
        $this->initOptions();
        echo Html::tag('div', $this->getInput('textInput'), $this->container);
    }

    /**
     * Registers the needed assets
     */
    public function registerAssets()
    {
        $view = $this->getView();
        TypeaheadAsset::register($view);
        if ($this->useHandleBars) {
            TypeaheadHBAsset::register($view);
        }
        $this->registerPluginOptions('typeahead');
        $id = $this->options['id'];
        $view->registerJs("{$this->_bloodhound}kvInitTA('{$id}', {$this->_hashVar}, {$this->_dataset});");
        $this->registerPluginEvents($view);
    }

    public function registerWidgetJs($js, $pos = View::POS_READY, $key = null)
    {
        if (empty($js)) {
            return;
        }
        $view = $this->getView();
        WidgetAsset::register($view);
        $view->registerJs($js, $pos, $key);
        if (!empty($this->pjaxContainerId) && ($pos === View::POS_LOAD || $pos === View::POS_READY)) {
            $pjax = 'jQuery("#' . $this->pjaxContainerId . '")';
            $evComplete = 'pjax:complete.' . hash('crc32', $js);
            $script = "setTimeout(function(){ {$js} }, 100);";
            $view->registerJs("{$pjax}.off('{$evComplete}').on('{$evComplete}',function(){ {$script} });");
            // hack fix for browser back and forward buttons
            if ($this->enablePopStateFix) {
                $view->registerJs("window.addEventListener('popstate',function(){window.location.reload();});");
            }
        }
    }
}