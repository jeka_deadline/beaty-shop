<?php

namespace frontend\widgets\slider;

use frontend\widgets\bootstrap4\Html;
use frontend\widgets\slider\assets\SliderAsset;
use frontend\widgets\typeahead\assets\WidgetAsset;
use yii\web\View;

class Slider extends \kartik\slider\Slider
{

    private $_isDisabled = false;

    /**
     * @inheritdoc
     */
    public function run()
    {
        $this->pluginName = $this->pluginConflict ? 'bootstrapSlider' : 'slider';

        if (!empty($this->value) || $this->value === 0) {
            if (is_array($this->value)) {
                throw new InvalidConfigException("Value cannot be passed as an array. If you wish to setup a range slider, pass the two values together as strings separated with a ',' sign.");
            }
            if (strpos($this->value, ',') > 0) {
                $values = explode(',', $this->value);
                static::validateValue($values[0]);
                static::validateValue($values[1]);
                $this->pluginOptions['value'] = [(float)$values[0], (float)$values[1]];
                $this->pluginOptions['range'] = true;
            } else {
                static::validateValue($this->value);
                $this->pluginOptions['value'] = (float)$this->value;
            }
        } else {
            // initialize value
            $this->pluginOptions['value'] = null;
        }

        Html::addCssClass($this->options, 'form-control');

        // initialize if disabled
        $this->_isDisabled = ((!empty($this->options['disabled']) && $this->options['disabled']) ||
                (!empty($this->options['readonly']) && $this->options['readonly'])) || (isset($this->disabled) &&
                $this->disabled) || (isset($this->readonly) && $this->readonly);
        if ($this->_isDisabled) {
            $this->pluginOptions['enabled'] = false;
        }

        $this->registerAssets();
        echo $this->getInput('textInput');
    }

    /**
     * Registers the needed assets
     */
    public function registerAssets()
    {
        $view = $this->getView();
        SliderAsset::register($view);

        // register plugin
        $id = "$('#" . $this->options['id'] . "')";
        $this->pluginOptions['id'] = $this->options['id'] . '-slider';
        $this->registerPlugin($this->pluginName);
        // register CSS styles
        $cssStyle = null;
        if (!empty($this->handleColor) && !$this->_isDisabled) {
            $isTriangle = (!empty($this->pluginOptions['handle']) && $this->pluginOptions['handle'] == 'triangle');
            $cssStyle = $this->getCssColor('handle', $this->handleColor, $isTriangle);
        }
        if (!empty($this->sliderColor) && !$this->_isDisabled) {
            $cssStyle .= $this->getCssColor('selection', $this->sliderColor);
        }
        if ($cssStyle != null && !$this->_isDisabled) {
            $view->registerCss($cssStyle);
        }

        // trigger change event on slider stop, so that client validation
        // is triggered for yii active fields
        $view->registerJs("{$id}.on('slideStop', function(){{$id}.trigger('change')});");
    }

    public function registerWidgetJs($js, $pos = View::POS_READY, $key = null)
    {
        if (empty($js)) {
            return;
        }
        $view = $this->getView();
        WidgetAsset::register($view);
        $view->registerJs($js, $pos, $key);
        if (!empty($this->pjaxContainerId) && ($pos === View::POS_LOAD || $pos === View::POS_READY)) {
            $pjax = 'jQuery("#' . $this->pjaxContainerId . '")';
            $evComplete = 'pjax:complete.' . hash('crc32', $js);
            $script = "setTimeout(function(){ {$js} }, 100);";
            $view->registerJs("{$pjax}.off('{$evComplete}').on('{$evComplete}',function(){ {$script} });");
            // hack fix for browser back and forward buttons
            if ($this->enablePopStateFix) {
                $view->registerJs("window.addEventListener('popstate',function(){window.location.reload();});");
            }
        }
    }
}
