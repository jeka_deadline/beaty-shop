<?php

namespace frontend\widgets\slider\assets;

use yii\web\AssetBundle;

class SliderAsset extends AssetBundle
{
    public $sourcePath = '@frontend/widgets/slider/assets/dist';

    public $css = [
        'css/bootstrap-slider.css',
    ];

    public $js = [
        'js/bootstrap-slider.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
        'frontend\widgets\bootstrap4\BootstrapAsset',
        'frontend\widgets\bootstrap4\BootstrapPluginAsset',
    ];

    public $publishOptions = [
        'forceCopy' => (YII_DEBUG) ? true : false,
    ];
}
