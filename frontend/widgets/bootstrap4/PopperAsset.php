<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\widgets\bootstrap4;

use yii\web\AssetBundle;

/**
 * Asset bundle for Popper javascript files.
 *
 * @author Artur Zhdanov <zhdanovartur@gmail.com>
 * @since 2.0
 */
class PopperAsset extends AssetBundle
{
    // todo:
    //public $sourcePath = '@frontend/widgets/bootstrap4/assets';

    public $js = [
        'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js',
    ];
}
