<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\widgets\bootstrap4;

use yii\web\AssetBundle;

/**
 * Asset bundle for the Twitter bootstrap javascript files.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class BootstrapPluginAsset extends AssetBundle
{
    // todo:
    //public $sourcePath = '@frontend/widgets/bootstrap4/assets';

    public $js = [
        '/js/bootstrap.min.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'frontend\widgets\bootstrap4\BootstrapAsset',
        'frontend\widgets\bootstrap4\PopperAsset',
    ];
}
