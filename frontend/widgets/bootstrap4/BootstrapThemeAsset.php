<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\widgets\bootstrap4;

use yii\web\AssetBundle;

/**
 * Asset bundle for the Twitter bootstrap default theme.
 *
 * @author Alexander Makarov <sam@rmcreative.ru>
 * @since 2.0
 */
class BootstrapThemeAsset extends AssetBundle
{
    // todo:
    //public $sourcePath = '@frontend/widgets/bootstrap4/assets';

    public $css = [
        'css/bootstrap-theme.css',
    ];

    public $depends = [
        'frontend\widgets\bootstrap4\BootstrapAsset',
    ];
}
