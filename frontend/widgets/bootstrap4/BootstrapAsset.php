<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\widgets\bootstrap4;

use yii\web\AssetBundle;

/**
 * Asset bundle for the Twitter bootstrap css files.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class BootstrapAsset extends AssetBundle
{
    // todo:
    //public $sourcePath = '@frontend/widgets/bootstrap4/assets';

    public $css = [
        '/css/bootstrap.min.css',
    ];

    public $publishOptions = [
        'forceCopy' => (YII_DEBUG) ? true : false,
    ];
}
