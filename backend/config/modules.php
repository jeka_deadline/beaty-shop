<?php
return [
    'academy' => [
        'class' => 'backend\modules\academy\Module',
    ],
    'core' => [
        'class' => 'backend\modules\core\Module',
    ],
    'user' => [
        'class' => 'backend\modules\user\Module',
    ],
    'product' => [
        'class' => 'backend\modules\product\Module',
    ],
    'geo' => [
        'class' => 'backend\modules\geo\Module',
    ],
    'pages' => [
        'class' => 'backend\modules\pages\Module',
    ],
    'statistic' => [
        'class' => 'backend\modules\statistic\Module',
    ],
    'blog' => [
        'class' => 'backend\modules\blog\Module',
    ],
    'permit' => [
        'class' => 'developeruz\db_rbac\Yii2DbRbac',
        'params' => [
            'userClass' => 'backend\modules\user\models\User',
            'accessRoles' => ['superadmin'],
        ]
    ],
    'gridview' =>  [
        'class' => '\kartik\grid\Module'
        // enter optional module parameters below - only if you need to
        // use your own export download action or custom translation
        // message source
        // 'downloadAction' => 'gridview/export/download',
        // 'i18n' => []
    ],
]
?>