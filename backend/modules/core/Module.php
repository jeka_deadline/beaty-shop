<?php

namespace backend\modules\core;

/**
 * Core class module extends yii base Module
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\core\controllers';

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function init()
    {
        return parent::init();
    }
}
