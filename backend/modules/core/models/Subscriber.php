<?php

namespace backend\modules\core\models;

use common\models\core\Subscriber as BaseSubscriber;

/**
 * Backend subscriber model extends common subscriber model.
 */
class Subscriber extends BaseSubscriber
{
    /**
     * Get count new subscribers.
     *
     * @return int
     */
    public static function getCountNewSubscribers()
    {
        return static::find()
            ->where(['is_new' => 1])
            ->count();
    }

    /**
     * Check is has new subscribers.
     *
     * @return bool
     */
    public static function isHasNewSubscribers()
    {
        return (static::getCountNewSubscribers()) ? true : false;
    }
}
