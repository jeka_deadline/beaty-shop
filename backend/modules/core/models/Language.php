<?php

namespace backend\modules\core\models;

use Yii;
use common\models\core\Language as BaseLanguage;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

class Language extends BaseLanguage
{
    public static function getActiveLanguages()
    {
        return static::find()
            ->where(['active' => 1])
            ->all();
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                ['code', 'match', 'pattern' => '#^[a-z]+$#i'],
            ]
        );
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ];
    }
}
