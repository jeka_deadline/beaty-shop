<?php

namespace backend\modules\core\models;

use Yii;
use common\models\core\SupportMessage as BaseSupportMessage;

class SupportMessage extends BaseSupportMessage
{
    /**
     * Get count new support messages.
     *
     * @return int
     */
    public static function getCountNewMessages()
    {
        return static::find()
            ->where(['is_new' => 1])
            ->count();
    }

    /**
     * Check is has new support messages.
     *
     * @return bool
     */
    public static function isHasNewMessages()
    {
        return (static::getCountNewMessages()) ? true : false;
    }

    /**
     * Get support files for current messages for view.
     *
     * @return string
     */
    public function viewSupportFiles()
    {
        $files = $this->getSupportFiles();
        $frontendPath = self::getFilePath() . '/' . $this->id . '/';
        $result = [];

        foreach ($files as $file) {
            $arrayExplode = explode(DIRECTORY_SEPARATOR, $file);
            $fileName = array_pop($arrayExplode);
            $result[] = "<p><a target='_blank' href='/{$frontendPath}{$fileName}'>{$fileName}</a></p>";
        }

        return implode('', $result);
    }
}
