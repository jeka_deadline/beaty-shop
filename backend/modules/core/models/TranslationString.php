<?php

namespace backend\modules\core\models;

use Yii;
use yii\base\Model;

class TranslationString extends Language
{
    public static $path;

    public $listString = [];

    public function __construct(array $config = [])
    {
        parent::__construct($config);

        self::$path = Yii::getAlias('@frontend/messages');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['listString'], 'safe'],
        ];
    }

    public function loadBlocks()
    {
        $tmpPath = $this->getPath();
        foreach ($this->getListBlock() as $block) {
            $pathFile = $tmpPath.$block.'.php';
            if (file_exists($pathFile)) {
                $this->listString[$block] = include $pathFile;
            }
        }
    }

    public function saveBlocks() {
        if (!$this->code) return false;

        $tmpPath = $this->getPath();
        foreach ($this->listString as $block => $list) {
            $pathFile = $tmpPath.$block.'.php';
            if (file_exists($pathFile)) {
                $output = str_replace(['array (',')','&#40','&#41'], ['[',']','(',')'], var_export($list, true));
                file_put_contents($pathFile, "<?php\nreturn " .  $output . ';');
            }
        }
        return true;
    }

    public function createBlocks() {
        $tmpPath = $this->getPath();
        if (is_dir($tmpPath)) return false;

        mkdir($tmpPath, 0777, true);
        $pathBase = self::$path.'/base/';

        foreach (glob($pathBase.'*.php') as $filename) {
            copy($filename, $tmpPath.basename($filename));
        }

        return true;
    }

    public function getListBlock() {
        $tmpPath = $this->getPath();
        $listBlock = [];
        foreach (glob($tmpPath.'*.php') as $filename) {
            $path_parts = pathinfo($filename);
            $listBlock[] = $path_parts['filename'];
        }
        return $listBlock;
    }

    public function getListString() {
        return $this->listString;
    }

    public function getListBlockLabels() {
        $listLabels = $this->getListBlock();
        $result = [];
        foreach ($listLabels as $label) {
            $result[$label] = ucfirst($label);
        }
        return $result;
    }

    private function getPath() {
        return self::$path.'/'.$this->code.'/';
    }
}