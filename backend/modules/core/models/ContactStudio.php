<?php

namespace backend\modules\core\models;

use backend\modules\core\behaviors\LangBehavior;
use yii\helpers\ArrayHelper;

class ContactStudio extends \common\models\core\ContactStudio
{
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                [
                    'class' => LangBehavior::className(),
                    'langModel' => ContactStudioLangField::className(),
                    'modelForeignKey' => 'contact_studio_id',
                    'languages' => Language::find()->all(),
                    'attributes' => [
                        'name',
                        'description',
                    ],
                ]
            ]
        );
    }
}
