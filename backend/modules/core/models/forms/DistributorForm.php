<?php

namespace backend\modules\core\models\forms;

use Yii;
use yii\base\Model;
use backend\modules\core\models\TrainerDistributor;
use common\models\user\User;

/**
 * Create distributor form.
 */
class DistributorForm extends Model
{
    /**
     * Distributor sex.
     *
     * @var string
     */
    public $sex;

    /**
     * Distributor first name.
     *
     * @var string
     */
    public $firstName;

    /**
     * Distributor last name.
     *
     * @var string
     */
    public $lastName;

    /**
     * Distributor phone.
     *
     * @var string
     */
    public $phone;

    /**
     * Distributor email.
     *
     * @var string
     */
    public $email;

    /**
     * Distributor country.
     *
     * @var string
     */
    public $country;

    /**
     * Distributor city.
     *
     * @var string
     */
    public $city;

    /**
     * Distributor address.
     *
     * @var string
     */
    public $address;

    /**
     * Distributor company.
     *
     * @var string
     */
    public $company;

    /**
     * Distributor tax number 1.
     *
     * @var string
     */
    public $taxNumber1;

    /**
     * Distributor tax number 2.
     *
     * @var string
     */
    public $taxNumber2;

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['sex', 'firstName', 'lastName', 'phone', 'email'], 'required'],
            [['firstName', 'lastName', 'phone', 'email', 'country', 'city', 'address', 'company', 'taxNumber1', 'taxNumber2'], 'filter', 'filter' => 'trim'],
            [['firstName', 'lastName'], 'string', 'max' => 50],
            [['email', 'country', 'city', 'company', 'taxNumber1', 'taxNumber2'], 'string', 'max' => 100],
            ['address', 'string'],
            ['email', 'email'],
            ['sex', 'in', 'range' => array_keys($this->getListUserTitles())],
        ];
    }

    /**
     * Create new distributor.
     *
     * @return bool
     */
    public function create()
    {
        if (!$this->validate()) {
            return false;
        }

        $model = new TrainerDistributor();

        $model->setAttributes($this->transformAttributes());

        if (!$model->validate() || !$model->save()) {
            return false;
        }

        $model->sendDistributorEmail();

        return true;
    }

    /**
     * Get user titles.
     *
     * @return array
     */
    public function getListUserTitles()
    {
        return User::getListUserTitles();
    }

    /**
     * Transform model attributes to table fields and set fields value.
     *
     * @access private
     * @return array
     */
    private function transformAttributes()
    {
        return [
            'sex' => $this->sex,
            'first_name' => $this->firstName,
            'last_name' => $this->lastName,
            'phone' => $this->phone,
            'email' => $this->email,
            'country' => $this->country,
            'city' => $this->city,
            'address' => $this->address,
            'company' => $this->company,
            'tax_number_1' => $this->taxNumber1,
            'tax_number_2' => $this->taxNumber2,
            'type' => TrainerDistributor::TYPE_DISTRUBUTOR,
            'register_code' => self::generateRegisterCode(),
            'is_new' => 0,
        ];
    }

    /**
     * Generate distributor register code.
     *
     * @access private
     * @return string
     */
    private function generateRegisterCode()
    {
        $code = md5(uniqid());

        while (TrainerDistributor::findOne(['register_code' => $code])) {
            $code = md5(uniqid());
        }

        return $code;
    }
}
