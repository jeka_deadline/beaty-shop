<?php

namespace backend\modules\core\models;

use backend\modules\core\behaviors\LangBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

class Slogan extends \common\models\core\Slogan
{
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                [
                    'class' => LangBehavior::className(),
                    'langModel' => SloganLangField::className(),
                    'modelForeignKey' => 'slogan_id',
                    'languages' => Language::find()->all(),
                    'attributes' => [
                        'slogan',
                    ],
                ]
            ]
        );
    }
}
