<?php

namespace backend\modules\core\models;

use backend\modules\core\behaviors\LangBehavior;
use yii\helpers\ArrayHelper;

class Career extends \common\models\core\Career
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                [
                    'class' => LangBehavior::className(),
                    'langModel' => CareerLangField::className(),
                    'modelForeignKey' => 'career_id',
                    'languages' => Language::find()->all(),
                    'attributes' => [
                        'name',
                        'description',
                    ],
                ]
            ]
        );
    }
}
