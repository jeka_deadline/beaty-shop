<?php

namespace backend\modules\core\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

class Setting extends \common\models\core\Setting
{
    public $file;

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
            ]
        );
    }

    public function getUrlImageInput() {
        if (!$this->value) return [];

        return ['/'.self::$path.$this->value];
    }

    public function saveImage()
    {
        if ($this->validate() && $this->file) {
            $allPath = Yii::getAlias('@frontend/web') . '/' . self::$path;
            if (!is_dir($allPath)) {
                mkdir($allPath, 0777, true);
            }

            $file = $this->file;
            $filename = md5(uniqid($file->baseName)).'.'.$file->extension;
            if ($file->saveAs($allPath . $filename)) {
                chmod($allPath . $filename, 0777);
            }

            $this->deleteImage();
            $this->updateAttributes(['value' => $filename]);

            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        switch ($this->type) {
            case self::TYPE_IMAGE:
                $this->file = UploadedFile::getInstance($this, 'file');
                $this->saveImage();
                break;
        }
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }

        $this->deleteImage();

        return true;
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'file' => 'Image',
        ]);
    }

    public function deleteImage() {
        switch ($this->type) {
            case self::TYPE_IMAGE:
                $filePath = Yii::getAlias('@frontend/web') . '/' . self::$path. '/'. $this->value;
                if ($this->value && file_exists($filePath)) {
                    unlink($filePath);
                }
                break;
        }
    }
}
