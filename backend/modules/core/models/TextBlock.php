<?php

namespace backend\modules\core\models;

use Yii;
use common\models\core\TextBlock as BaseTextBlock;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * Backend text block model extends common text block model.
 */
class TextBlock extends BaseTextBlock
{
    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                ['code', 'match', 'pattern' => '#^[a-z][a-z\d_-]+[a-z\d]$#i'],
            ]
        );
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ];
    }
}
