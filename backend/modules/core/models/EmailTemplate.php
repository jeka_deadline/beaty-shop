<?php

namespace backend\modules\core\models;

use Yii;
use common\models\core\EmailTemplate as BaseEmailTemplate;
use yii\helpers\ArrayHelper;

/**
 * Backend email template model extends common email template model.
 */
class EmailTemplate extends BaseEmailTemplate
{
    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                ['code', 'match', 'pattern' => '#^[a-z][a-z\d-]*[a-z]$#'],
            ]
        );
    }

    /**
     * Is model has placeholders.
     *
     * @return bool
     */
    public function hasPlaceholders()
    {
        if (!$this->isNewRecord && in_array($this->code, [
            self::CONFIRM_EMAIL_TEMPLATE_CODE,
            self::SUPPORT_MESSAGE_TEMPLATE_CODE,
            self::RESET_PASSWORD_TEMPLATE_CODE,
        ])) {
            return true;
        }

        return false;
    }

    /**
     * Get list placeholders for current template
     *
     * @return string
     */
    public function getPlaceholders()
    {
        switch ($this->code) {
            case self::CONFIRM_EMAIL_TEMPLATE_CODE:
                return $this->getConfirmEmailMessagePlaceholders();
            case self::SUPPORT_MESSAGE_TEMPLATE_CODE:
                return $this->getSupportMessagePlaceholders();
            case self::RESET_PASSWORD_TEMPLATE_CODE:
                return $this->getResetPasswordMessageTemplate();
            default:
                return '';
        }
    }

    /**
     * Placeholders for template confirm email.
     *
     * @return string
     */
    private function getConfirmEmailMessagePlaceholders()
    {
        return '<p>{confirmEmailLink} - Link for confirm email</p>';
    }

    /**
     * Placeholders for support message.
     *
     * @return string
     */
    private function getSupportMessagePlaceholders()
    {
        return '<p>{ticketId} - Ticket id</p><p>{supportMessage} User - support message</p>';
    }

    /**
     * Placeholders for template reset password.
     *
     * @return string
     */
    private function getResetPasswordMessageTemplate()
    {
        return '<p>{resetPasswordLink} - Link for reset password</p>';
    }
}
