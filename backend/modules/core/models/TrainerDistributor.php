<?php

namespace backend\modules\core\models;

use Yii;

class TrainerDistributor extends \common\models\core\TrainerDistributor
{
    public function createRegisterCode()
    {
        $code = md5(uniqid());

        while(self::findOne(['register_code' => $code])) {
            $code = md5(uniqid());
        }

        $this->updateAttributes(['register_code' => $code]);
        $this->sendDistributorEmail();
    }

    public static function getCountNew()
    {
        return static::find()
            ->where(['is_new' => 1])
            ->count();
    }

    public static function getCountNewTrainer()
    {
        return static::find()
            ->where([
                'is_new' => 1,
                'type' => self::TYPE_TRAINER
            ])
            ->count();
    }

    public static function getCountNewDistributor()
    {
        return static::find()
            ->where([
                'is_new' => 1,
                'type' => self::TYPE_DISTRUBUTOR
            ])
            ->count();
    }

    public static function isHasNew()
    {
        return (static::getCountNew()) ? true : false;
    }

    /**
     * Send distributor email with register link
     *
     * @return bool
     */
    public function sendDistributorEmail()
    {
        $registerLink = $this->registerLink;

        $mailer = Yii::$app->mailer;

        return $mailer->compose()
            ->setFrom([$mailer->transport->getUsername() => 'info'])
            ->setTo($this->email)
            ->setHtmlBody("You success register as distributor. You <a href='{$registerLink}'>link</a>")
            ->setSubject('You register link')
            ->send();
    }

    public function getRegisterLink()
    {
        if ($this->type !== self::TYPE_DISTRUBUTOR || empty($this->register_code)) {
            return '';
        }

        return Yii::$app->urlManagerFrontEnd->createAbsoluteUrl(['/user/security/login', 'distributor_code' => $this->register_code]);
    }
}
