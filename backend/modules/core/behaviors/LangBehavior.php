<?php

namespace backend\modules\core\behaviors;

use yii\base\Behavior;
use common\models\core\Language;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\base\UnknownPropertyException;
use yii\validators\Validator;

class LangBehavior extends Behavior
{
    /**
     * Array languages
     *
     * @var array
     */
    public $languages = [];

    /**
     * Name field in table which relation model and lang fields
     *
     * @var string
     */
    public $modelForeignKey;

    /**
     * Name field in table which relation lang fields and lang
     *
     * @var string
     */
    public $langForeingKey = 'lang_id';

    /**
     * Language model for model
     *
     * @var \yii\db\ActiveRecord
     */
    public $langModel;

    /**
     * List attributes which translated
     *
     * @var array
     */
    public $attributes;

    /**
     * List attributes with postfix lang code
     *
     * @var array
     */
    private $listAttributes;

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function attach($owner)
    {
        parent::attach($owner);

        if (!is_array($this->attributes) || empty($this->attributes)) {
            throw new \Exception("Attributes can'n be empty");
        }

        $validators = $owner->getValidators();

        foreach ($this->languages as $language) {
            foreach ($this->attributes as $attributeName) {
                $attributeWithLang = $attributeName . '_' . $language->code;
                $this->listAttributes[ $attributeWithLang ] = '';
                $validators[] = Validator::createValidator('safe', $owner, $attributeWithLang, []);
            }
        }
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_FIND => 'afterFind',
            ActiveRecord::EVENT_AFTER_INSERT => 'saveLangFields',
            ActiveRecord::EVENT_AFTER_UPDATE => 'saveLangFields',
        ];
    }

    /**
     * Save or update lang fields.
     *
     * @return void
     */
    public function saveLangFields($event)
    {
        $currentModel = $this->owner;
        $langModelQuery = call_user_func([$this->langModel, 'find']);

        $langModels = $langModelQuery->where([$this->modelForeignKey => $currentModel->id])->indexBy($this->langForeingKey)->all();

        foreach ($this->languages as $language) {
            if (!isset($langModels[ $language->id ])) {
                $model = new $this->langModel();
                $model->{$this->langForeingKey} = $language->id;
                $model->{$this->modelForeignKey} = $this->owner->id;
            } else {
                $model = $langModels[ $language->id ];
            }

            foreach ($this->attributes as $attributeName) {
                $model->{$attributeName} = $this->getLangAttribute($attributeName, $language->code);
            }

            $model->save();
        }
    }

    /**
     * Get attribute with lang.
     *
     * @param string $name Name attribute
     * @param string $lang Lang code
     * @return string|null
     */
    private function getLangAttribute($name, $lang)
    {
        $propertyName = $name . '_' . $lang;

        if ($this->canGetProperty($propertyName)) {
            return $this->listAttributes[ $propertyName ];
        }

        return null;
    }

    /**
     * Set attribute with lang.
     *
     * @param string $name Name attribute
     * @param string $lang Lang code
     * @param string $value Value attribute
     * @return void
     */
    private function setLangAttribute($name, $lang, $value = '')
    {
        $propertyName = $name . '_' . $lang;

        if ($this->canSetProperty($propertyName)) {
            $this->listAttributes[ $propertyName ] = $value;
        }
    }

    /**
     * Method which run after find ActiveRecord and set lang attributes
     *
     * @param $event
     * @return void
     */
    public function afterFind($event)
    {
        $currentModel = $this->owner;
        $langModelQuery = call_user_func([$this->langModel, 'find']);

        $langModels = $langModelQuery->where([$this->modelForeignKey => $currentModel->id])->all();

        foreach ($langModels as $langModel) {
            foreach ($this->languages as $language) {
                if ($langModel->{$this->langForeingKey} !== $language->id) {
                    continue;
                }

                foreach ($this->attributes as $attributeName) {
                    $propertyName = $attributeName . '_' . $language->code;
                    if (!$this->canSetProperty($propertyName)) {
                        continue;
                    }

                    $this->setLangAttribute($attributeName, $language->code, $langModel->{$attributeName});
                }
            }
        }
    }

    /**
     * {@inheritdoc}
     *
     * @param string $name Name attribute class
     * @throws UnknownPropertyException If not exist property object
     * @return string
     */
    public function __get($name)
    {
        try {
            return parent::__get($name);
        } catch (UnknownPropertyException $e) {
            if ($this->canGetProperty($name)) {
                return $this->listAttributes[ $name ];
            } else {
                throw $e;
            }
        }
    }

    /**
     * {@inheritdoc}
     *
     * @param string $name Name attribute class
     * @param mixed $value Value attribute class
     * @throws UnknownPropertyException If not exist property object
     * @return string
     */
    public function __set($name, $value)
    {
        try {
            return parent::__set($name, $value);
        } catch (UnknownPropertyException $e) {
            if ($this->canSetProperty($name)) {
                $this->listAttributes[ $name ] = $value;
            } else {
                throw $e;
            }
        }
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function canGetProperty($name, $checkVars = true, $checkBehaviors = true)
    {
        if (isset($this->listAttributes[ $name ])) {
            return true;
        }

        return parent::canGetProperty($name, $checkVars, $checkBehaviors);
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function canSetProperty($name, $checkVars = true, $checkBehaviors = true)
    {
        if (isset($this->listAttributes[ $name ])) {
            return true;
        }

        return parent::canSetProperty($name, $checkVars, $checkBehaviors);
    }
}
