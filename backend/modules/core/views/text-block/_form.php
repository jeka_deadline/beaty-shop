<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\core\models\TextBlock */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="text-block-form">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]); ?>

        <?= $form->field($model, 'code')->textInput(['maxlength' => true]); ?>

        <?= $form->field($model, 'description')->textarea(['rows' => 3]); ?>

        <?= $form->field($model, 'content')->textarea(['rows' => 6]); ?>

        <?= $form->field($model, 'active')->checkbox(); ?>

        <div class="form-group">

            <?= Html::submitButton('Save', ['class' => 'btn btn-success']); ?>

        </div>

    <?php ActiveForm::end(); ?>

</div>
