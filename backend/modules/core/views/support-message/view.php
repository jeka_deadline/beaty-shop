<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\core\models\SupportMessage */

$this->title = 'Support message id: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Support Messages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="support-message-view">

    <p>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'surname',
            'name',
            'company',
            'email:email',
            'subject',
            'text:ntext',
            'is_new',
            'created_at',
            'updated_at',
            [
                'label' => 'Files',
                'format' => 'raw',
                'value' => function ($model) { return $model->viewSupportFiles(); }
            ],
        ],
    ]) ?>

</div>
