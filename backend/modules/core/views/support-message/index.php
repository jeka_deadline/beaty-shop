<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\core\models\searchModels\SupportMessageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Support Messages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="support-message-index table-responsive">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'surname',
            'name',
            'email:email',
            'created_at',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}{delete}',
            ],
        ],
    ]); ?>
</div>
