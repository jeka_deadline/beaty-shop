<?php

use backend\modules\core\models\TrainerDistributor;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\core\models\TrainerDistributor */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Trainer Distributors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trainer-distributor-view">

    <p>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php if ($model->type === $model::TYPE_DISTRUBUTOR) : ?>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'sex',
                'first_name',
                'last_name',
                'company',
                'phone',
                'email:email',
                'country',
                'city',
                'address:ntext',
                'created_at',
                [
                    'label' => 'Attached files',
                    'format' => 'raw',
                    'value' => function($model){
                        $links = '';
                        $allPath = $model->getPath();
                        if (is_dir($allPath)) {
                            $files = scandir($allPath);
                            foreach ($files as $file) {
                                if (!($file=='.' || $file=='..')) {
                                    $links .= Html::a($file, $model->getUrlDir() . $file) . '<br>';
                                }
                            }
                        }
                        return $links;
                    },
                ],
                [
                   'attribute' => 'tax_number_1',
                   'label' => 'Steuernummer',
                ],
                [
                   'attribute' => 'tax_number_2',
                   'label' => 'Umsatzsteuernummer',
                ],
                [
                    'attribute' => 'register_code',
                    'label' => 'Register link',
                    'format' => 'raw',
                    'value' => function($model) {
                        if (empty($model->register_code)) {
                            return Html::a('Create register link', ['create-register-link', 'id' => $model->id], ['class' => 'btn btn-success']);
                        }

                        return Yii::$app->urlManagerFrontEnd->createAbsoluteUrl(['/user/security/login', 'distributor_code' => $model->register_code]);
                    }
                ],
            ],
        ]) ?>

    <?php else : ?>

        <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'sex',
            'first_name',
            'last_name',
            'company',
            'phone',
            'email:email',
            'country',
            'city',
            'address:ntext',
            'created_at',
            [
                'label' => 'Attached files',
                'format' => 'raw',
                'value' => function($model){
                    $links = '';
                    $allPath = $model->getPath();
                    if (is_dir($allPath)) {
                        $files = scandir($allPath);
                        foreach ($files as $file) {
                            if (!($file=='.' || $file=='..')) {
                                $links .= Html::a($file, $model->getUrlDir() . $file) . '<br>';
                            }
                        }
                    }
                    return $links;
                },
            ],
        ],
    ]) ?>

    <?php endif; ?>

</div>
