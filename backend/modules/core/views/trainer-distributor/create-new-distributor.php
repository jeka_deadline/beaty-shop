<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\core\models\forms\DistributorForm */
/* @var $form ActiveForm */

$this->title = 'Create distributor';
$this->params['breadcrumbs'][] = ['label' => 'Distributors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="create-new-distributor">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'sex')->dropDownList($model->getListUserTitles()); ?>

        <?= $form->field($model, 'firstName'); ?>

        <?= $form->field($model, 'lastName'); ?>

        <?= $form->field($model, 'phone'); ?>

        <?= $form->field($model, 'email'); ?>

        <?= $form->field($model, 'country'); ?>

        <?= $form->field($model, 'city'); ?>

        <?= $form->field($model, 'address'); ?>

        <?= $form->field($model, 'company'); ?>

        <?= $form->field($model, 'taxNumber1'); ?>

        <?= $form->field($model, 'taxNumber2'); ?>

        <div class="form-group">

            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']); ?>

        </div>

    <?php ActiveForm::end(); ?>

</div><!-- create-new-distributor -->
