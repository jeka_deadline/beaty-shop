<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\core\models\searchModels\TrainerDistributorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Trainer Distributors';
$this->params['breadcrumbs'][] = $this->title;
?>
<?= Html::a('Create new distributor', ['create-new-distributor'], ['class' => 'btn btn-success']); ?>
<div class="trainer-distributor-index table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'type',
            'sex',
            'first_name',
            'last_name',
            'phone',
            'email:email',
            [
                'class' => 'yii\grid\DataColumn',
                'header' => 'Register link',
                'format' => 'raw',
                'value' => function($model) {
                    return $model->registerLink;
                },
                'contentOptions' => [
                    'style' => 'white-space: normal',
                ],
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {delete}',
            ],
        ],
    ]); ?>
</div>
