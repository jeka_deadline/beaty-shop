<?php

use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\core\models\Slogan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="slogan-form">

    <?php $form = ActiveForm::begin(); ?>

        <?php
            $items = [
                [
                    'label' => 'Career',
                    'content' => $this->render(
                        '_form_tab_slogan',
                        compact(
                            'model',
                            'form')
                    ),
                ],
            ];

            foreach ($languages as $language) {
                $items[] = [
                    'label' => $language->name,
                    'content' => $this->render('_form_tab_lang_fields', compact('model','product_id', 'language', 'form')),
                ];
            }
        ?>

        <?= Tabs::widget([
            'items' => $items,
        ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
