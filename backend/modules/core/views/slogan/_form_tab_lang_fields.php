<?php

use mihaildev\ckeditor\CKEditor;
use yii\helpers\Html;

?>

<?= $form->field($model, 'slogan_' . $language->code)->textInput(['maxlength' => true])->label($model->getAttributeLabel('slogan')) ?>
