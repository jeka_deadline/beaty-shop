<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\core\models\Slogan */

$this->title = 'Create Slogan';
$this->params['breadcrumbs'][] = ['label' => 'Slogans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slogan-create">

    <?= $this->render('_form', [
        'model' => $model,
        'languages' => $languages
    ]) ?>

</div>
