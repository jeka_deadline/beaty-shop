<?php

use backend\modules\academy\models\Register;
use backend\modules\core\models\Subscriber;
use backend\modules\core\models\TrainerDistributor;
use backend\modules\product\models\Order;
use backend\modules\core\models\SupportMessage;

?>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->identity->profile->name; ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Dashboard ', 'options' => ['class' => 'header']],
                    ['label' => 'Shop statistic', 'icon' => 'area-chart', 'url' => ['/statistic/index/index']],
                    ['label' => 'Site management', 'options' => ['class' => 'header']],
                    ['label' => 'Gii', 'icon' => 'gears', 'url' => ['/gii'], 'visible' => (YII_DEBUG) ? true : false],
                    ['label' => 'Slogans', 'icon' => 'hand-rock-o', 'url' => ['/core/slogan/index']],
                    [
                        'label' => 'Language',
                        'url' => '#',
                        'icon' => 'language',
                        'items' => [
                            [
                                'label' => 'Languages',
                                'url' => ['/core/language/index'],
                                'icon' => 'language',
                            ],
                            [
                                'label' => 'Translation strings',
                                'url' => ['/core/translation-strings/index'],
                                'icon' => 'th-list',
                            ],
                        ],
                    ],
                    ['label' => 'Text blocks', 'icon' => 'text-width', 'url' => ['/core/text-block/index']],
                    ['label' => 'Email templates', 'url' => ['/core/email-template/index']],
                    ['label' => 'Settings', 'icon' => 'cog', 'url' => ['/core/setting/index']],
                    [
                        'label' => 'Pages',
                        'icon' => 'copy',
                        'url' => '#',
                        'items' => [
                            [
                                'label' => 'Pages',
                                'url' => ['/pages/page/index'],
                                'icon' => 'copy',
                            ],
                            [
                                'label' => 'Core pages',
                                'url' => ['/core/core-pages/index'],
                            ],
                            [
                                'label' => 'Careers',
                                'url' => ['/core/career/index'],
                                'icon' => 'buysellads',
                            ],
                            [
                                'label' => 'Contacts studios',
                                'url' => ['/core/contact-studio/index'],
                                'icon' => 'map-o',
                            ],
                        ],
                    ],
                    ['label' => 'Support messages', 'icon' => 'envelope', 'url' => ['/core/support-message/index'], 'template' => sprintf('<a href="{url}">{icon} {label}%s</a>', (SupportMessage::isHasNewMessages() ? '<span class="pull-right-container"><small class="label pull-right bg-red">' . SupportMessage::getCountNewMessages() . '</small></span>' : ''))],
                    [
                        'label' => 'Geo',
                        'url' => '#',
                        'icon' => 'globe',
                        'items' => [
                            [
                                'label' => 'Countries',
                                'url' => ['/geo/country/index'],
                                'icon' => 'flag',
                            ],
                            [
                                'label' => 'Cities',
                                'url' => ['/geo/city/index'],
                                'icon' => 'building',
                            ],
                        ],
                    ],
                    ['label' => 'Store', 'options' => ['class' => 'header']],
                    [
                        'label' => 'Shop management',
                        'url' => '#',
                        'icon' => 'desktop',
                        'items' => [
                            [
                                'label' => 'Product categories',
                                'url' => ['/product/product-category/index'],
                                'icon' => 'th-large',
                            ],
                            [
                                'label' => 'Products',
                                'url' => ['/product/product/index'],
                                'icon' => 'shopping-cart',
                            ],
                            [
                                'label' => 'Sets',
                                'url' => ['/product/set/index'],
                                'icon' => 'object-group',
                            ],
                            [
                                'label' => 'Edit Sets',
                                'url' => ['/product/edit-set/index'],
                                'icon' => 'object-ungroup',
                            ],
                            [
                                'label' => 'Stock',
                                'url' => ['/product/product-variation/stock'],
                                'icon' => 'archive',
                            ],
                            [
                                'label' => 'Product reviews',
                                'url' => ['/product/product-review/index'],
                                'icon' => 'comments',
                            ],
                            [
                                'label' => 'Recommendation',
                                'url' => ['/product/free-recommendet/index'],
                                'icon' => 'random',
                            ],
                            [
                                'label' => 'Best seller products',
                                'url' => ['/product/best-seller/index'],
                                'icon' => 'star',
                            ],
                            [
                                'label' => 'Filters',
                                'url' => ['/product/product-filter/index'],
                                'icon' => 'filter',
                            ],
                            [
                                'label' => 'Orders',
                                'url' => ['/product/order/index'],
                                'icon' => 'bell',
                                'template' => sprintf('<a href="{url}">{icon} {label}%s</a>', (Order::isHasNewOrders() ? '<span class="pull-right-container"><small class="label pull-right bg-red">' . Order::getCountNewOrders() . '</small></span>' : '')),
                            ],
                        ],
                    ],
                    [
                        'label' => 'Coupons',
                        'url' => ['/product/coupon/index'],
                        'icon' => 'gift',
                    ],
                    ['label' => 'Blog', 'options' => ['class' => 'header']],
                    [
                        'label' => 'Articles',
                        'url' => ['/blog/article/index'],
                        'icon' => 'book',
                    ],
                    [
                        'label' => 'Sliders',
                        'url' => ['/blog/slider-image/index'],
                        'icon' => 'file-image-o',
                    ],

                    ['label' => 'Academy', 'options' => ['class' => 'header']],
                    [
                        'label' => 'Academy management',
                        'url' => '#',
                        'icon' => 'institution',
                        'template' => sprintf('<a href="{url}">{icon} {label}%s</a>', (Register::isHasNew() ? '<span class="pull-right-container"><small class="label pull-right bg-red">' . Register::getCountNew() . '</small></span>' : '')),
                        'items' => [
                            [
                                'label' => 'Registered',
                                'url' => ['/academy/register/index'],
                                'icon' => 'edit',
                            ],
                            [
                                'label' => 'Academy',
                                'url' => ['/academy/academy/index'],
                                'icon' => 'font',
                            ],
                        ],
                    ],
                    ['label' => 'Users', 'options' => ['class' => 'header']],
                    ['label' => 'Management users', 'icon' => 'users', 'url' => ['/user/admin/index']],
                    ['label' => 'Subscribers', 'icon' => 'envelope', 'url' => ['/core/subscribe/index'], 'template' => sprintf('<a href="{url}">{icon} {label}%s</a>', (Subscriber::isHasNewSubscribers() ? '<span class="pull-right-container"><small class="label pull-right bg-red">' . Subscriber::getCountNewSubscribers() . '</small></span>' : ''))],
                    ['label' => 'Trainer Distributor', 'icon' => 'envelope', 'url' => ['/core/trainer-distributor/index'], 'template' => sprintf('<a href="{url}">{icon} {label}%s</a>', (TrainerDistributor::isHasNew() ? '<span class="pull-right-container"><small class="label pull-right bg-red">' . TrainerDistributor::getCountNew() . '</small></span>' : ''))],
                    [
                        'label' => 'Roles',
                        'icon' => 'key',
                        'url' => '#',
                        'items' => [
                            [
                                'label' => 'Roles',
                                'url' => ['/permit/access/role'],
                                'icon' => 'user',
                            ],
                            [
                                'label' => 'Permissions',
                                'url' => ['/permit/access/permission'],
                                'icon' => 'magnet',
                            ],
                        ],
                    ],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                ],
            ]
        ) ?>

    </section>

</aside>