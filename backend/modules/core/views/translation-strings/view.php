<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\core\models\Language */

$this->title = $modelTranslationStrings->name;
$this->params['breadcrumbs'][] = ['label' => 'Translation Strings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="translation-strings-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $modelTranslationStrings->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $modelTranslationStrings,
        'attributes' => [
            'id',
            'name',
            'code',
        ],
    ]) ?>

    <?php foreach ($modelTranslationStrings->listBlockLabels as $name => $block): ?>
        <h4><?= $name ?></h4>
        <?= $this->render('_view_block', compact('modelTranslationStrings',  'name')) ?>
    <?php endforeach;?>
</div>
