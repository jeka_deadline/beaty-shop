<?php

use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\core\models\Language */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="translation-strings-form">

    <?php $form = ActiveForm::begin(); ?>

        <?php
            $items = [];

            foreach ($modelTranslationStrings->listBlockLabels as $name => $block) {
                $items[] = [
                    'label' => $block,
                    'content' => $this->render('_form_tab_block', compact('modelTranslationStrings', 'form', 'name')),
                ];
            }
        ?>

        <?= Tabs::widget([
            'items' => $items,
        ]); ?>

        <div class="form-group">

            <?= Html::submitButton('Save', ['class' => 'btn btn-success']); ?>

        </div>

    <?php ActiveForm::end(); ?>

</div>
