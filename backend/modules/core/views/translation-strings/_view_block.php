<?php

?>

<table id="w0" class="table table-striped table-bordered detail-view">
    <tbody>
    <?php if (isset($modelTranslationStrings->listString[$name])): ?>
        <?php foreach ($modelTranslationStrings->listString[$name] as $key => $value): ?>
            <tr>
                <th><?= $key ?></th>
                <td><?= $value ?></td>
            </tr>
        <?php endforeach; ?>
    <?php endif; ?>
    </tbody>
</table>
