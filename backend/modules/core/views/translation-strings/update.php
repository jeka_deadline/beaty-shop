<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\core\models\Language */

$this->title = 'Update Translation Strings:';
$this->params['breadcrumbs'][] = ['label' => 'Translation Strings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $modelTranslationStrings->name, 'url' => ['view', 'id' => $modelTranslationStrings->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="translation-strings-update">

    <?= $this->render('_form', [
        'modelTranslationStrings' => $modelTranslationStrings
    ]) ?>

</div>
