<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\core\models\searchModels\LanguageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Translation Strings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="translation-strings-index table-responsive">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}{update}'
            ],
        ],
    ]); ?>
</div>
