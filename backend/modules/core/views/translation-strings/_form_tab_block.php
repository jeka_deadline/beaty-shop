<?php

use yii\helpers\Html;

$nameTextInput = Html::getInputName($modelTranslationStrings, 'listString');

?>
<br>
<div class="row">
    <div class="col-lg-12">
        <?php if (isset($modelTranslationStrings->listString[$name])): ?>
            <?php foreach ($modelTranslationStrings->listString[$name] as $key => $value): ?>
                <div class="form-group">
                    <?= Html::label(strip_tags($key), $nameTextInput.'['.$name.']['.$key.']', ['class' => 'control-label']) ?>
                    <?= Html::textInput($nameTextInput.'['.$name.']['.$key.']', $value, ['class' => 'form-control']) ?>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>

