<?php

use mihaildev\ckeditor\CKEditor;
use yii\helpers\Html;

?>

<?= $form->field($model, 'name_' . $language->code)->textInput(['maxlength' => true])->label($model->getAttributeLabel('name')) ?>

<?= $form->field($model, 'description_' . $language->code)->widget(CKEditor::className(),[
    'editorOptions' => [
        'toolbar' => [
            [ 'name' => 'document', 'items' => [ 'Source'] ],
            [ 'name' => 'clipboard', 'items' => [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] ],
            [ 'name' => 'basicstyles', 'items' => [ 'Bold', 'Italic', 'Underline',  'Strike',  'Subscript', 'Superscript', 'RemoveFormat'] ],
            '/',
            [ 'name' => 'paragraph', 'items' => [ 'Format' ] ],
            [ 'name' => 'list', 'items' => [ 'NumberedList', 'BulletedList', 'Outdent', 'Indent', 'Blockquote' ] ],
            [ 'name' => 'color', 'items' => [ 'TextColor', 'BGColor'] ],
        ],
    ]
])->label($model->getAttributeLabel('description')) ?>
