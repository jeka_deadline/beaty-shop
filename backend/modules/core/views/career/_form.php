<?php

use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\core\models\Career */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="career-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    $items = [
        [
            'label' => 'Contact Studio',
            'content' => $this->render(
                '_form_tab_career',
                compact(
                    'model',
                    'form')
            ),
        ],
    ];

    foreach ($languages as $language) {
        $items[] = [
            'label' => $language->name,
            'content' => $this->render('_form_tab_lang_fields', compact('model','product_id', 'language', 'form')),
        ];
    }

    ?>

    <?= Tabs::widget([
        'items' => $items,
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
