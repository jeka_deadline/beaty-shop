<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\core\models\CorePage */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Core Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="core-page-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'meta_title',
            'meta_keywords',
            'meta_description',
        ],
    ]) ?>

</div>
