<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\core\models\CorePage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="core-page-form">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]); ?>

        <?= $form->field($model, 'meta_title')->textarea(['maxlength' => true]); ?>

        <?= $form->field($model, 'meta_keywords')->textarea(['maxlength' => true]); ?>

        <?= $form->field($model, 'meta_description')->textarea(['maxlength' => true]); ?>

        <div class="form-group">

            <?= Html::submitButton('Save', ['class' => 'btn btn-success']); ?>

        </div>

    <?php ActiveForm::end(); ?>

</div>
