<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\core\models\CorePage */

$this->title = 'Update Core Page: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Core Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="core-page-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
