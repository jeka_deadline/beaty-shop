<?php

use yii\helpers\Html;
use yii\grid\GridView;

/** @var \yii\web\View $this */
/** @var \backend\modules\core\models\searchModels\EmailTemplateSearch $searchModel */
/** @var \yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Email Templates';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="email-template-index table-responsive">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'code',
            'subject',
            'from',
            //'text:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
