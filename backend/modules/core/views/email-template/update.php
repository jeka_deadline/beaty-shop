<?php

use yii\helpers\Html;

/** @var \yii\web\View $this */
/** @var \backend\modules\core\models\EmailTemplate $model */

$this->title = 'Update Email Template: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Email Templates', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="email-template-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
