<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\core\models\CoreSetting */

$this->title = 'Create Core Setting';
$this->params['breadcrumbs'][] = ['label' => 'Settings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="core-setting-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
