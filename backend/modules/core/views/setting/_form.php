<?php

use backend\modules\core\models\Setting;
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\core\models\CoreSetting */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="core-setting-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= ''//$form->field($model, 'key')->textInput(['maxlength' => true, 'disabled' => 'disabled']) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?php if (!$model->isNewRecord): ?>
        <?php if ($model->type == 'text'):
          ?>
            <?= $form->field($model, 'value')->textarea(['rows' => 6]) ?>

        <?php endif; ?>
        <?php if ($model->type == 'image'): ?>

            <?= $form->field($model, 'file')->widget(FileInput::classname(), [
                'options' => [
                    'accept' => 'image/*',
                ],
                'pluginOptions' => [
                    'initialPreview' => $model->getUrlImageInput(),
                    'initialPreviewAsData'=>true,

                    'browseClass' => 'btn btn-success',
                    'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                    'browseLabel' =>  'Select Photo',
                ],
            ]) ?>
        <?php endif; ?>

        <?php if ($model->type === 'checkbox') : ?>

            <?= $form->field($model, 'value')->checkbox(); ?>

        <?php endif; ?>
    <?php endif; ?>

    <?= ''//$form->field($model, 'type')->dropDownList(Setting::typeLabels()) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
