<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\core\models\ContactStudio */

$this->title = 'Create Contact Studio';
$this->params['breadcrumbs'][] = ['label' => 'Contact Studios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-studio-create">

    <?= $this->render('_form', [
        'model' => $model,
        'languages' => $languages
    ]) ?>

</div>
