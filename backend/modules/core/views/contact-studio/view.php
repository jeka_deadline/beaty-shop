<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\core\models\ContactStudio */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Contact Studios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-studio-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'description:ntext',
            'address',
            'lat',
            'lng',
            'type',
            'display_order',
            'active',
            'created_at',
        ],
    ]) ?>

</div>
