<?php

use mihaildev\ckeditor\CKEditor;

?>

<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'description')->widget(CKEditor::className(),[
    'editorOptions' => [
        'toolbar' => [
            [ 'name' => 'document', 'items' => [ 'Source'] ],
            [ 'name' => 'clipboard', 'items' => [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] ],
            [ 'name' => 'basicstyles', 'items' => [ 'Bold', 'Italic', 'Underline',  'Strike',  'Subscript', 'Superscript', 'RemoveFormat'] ],
            '/',
            [ 'name' => 'paragraph', 'items' => [ 'Format' ] ],
            [ 'name' => 'list', 'items' => [ 'NumberedList', 'BulletedList', 'Outdent', 'Indent', 'Blockquote' ] ],
            [ 'name' => 'color', 'items' => [ 'TextColor', 'BGColor'] ],
        ],
    ]
]) ?>

<?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'lat')->textInput() ?>

<?= $form->field($model, 'lng')->textInput() ?>

<?= $form->field($model, 'display_order')->textInput() ?>

<?= $form->field($model, 'active')->checkbox() ?>
