<?php

namespace backend\modules\core\controllers;

use backend\modules\core\models\TranslationString;
use Yii;
use backend\modules\core\models\Language;
use backend\modules\core\models\searchModels\LanguageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TranslationStringsController implements the CRUD actions for Language model.
 */
class TranslationStringsController extends Controller
{
    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \developeruz\db_rbac\behaviors\AccessBehavior::className(),
                'login_url' => Yii::$app->user->loginUrl,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Language models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LanguageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Language model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $modelTranslationStrings = $this->findModel($id);

        if ($modelTranslationStrings->code) $modelTranslationStrings->loadBlocks();

        return $this->render('view', [
            'modelTranslationStrings' => $modelTranslationStrings
        ]);
    }

    /**
     * Updates an existing Language model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $modelTranslationStrings = $this->findModel($id);

        if (
            $modelTranslationStrings->load(Yii::$app->request->post())
            && $modelTranslationStrings->validate()
            && $modelTranslationStrings->saveBlocks()
        ) {
            return $this->redirect(['view', 'id' => $modelTranslationStrings->id]);
        }

        if ($modelTranslationStrings->code) {
            $modelTranslationStrings->createBlocks();
            $modelTranslationStrings->loadBlocks();
        }

        return $this->render('update', [
            'modelTranslationStrings' => $modelTranslationStrings
        ]);
    }

    /**
     * Finds the Language model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Language the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TranslationString::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
