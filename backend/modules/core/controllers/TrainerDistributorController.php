<?php

namespace backend\modules\core\controllers;

use Yii;
use backend\modules\core\models\TrainerDistributor;
use backend\modules\core\models\searchModels\TrainerDistributorSearch;
use backend\modules\core\models\forms\DistributorForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TrainerDistributorController implements the CRUD actions for TrainerDistributor model.
 */
class TrainerDistributorController extends Controller
{
    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \developeruz\db_rbac\behaviors\AccessBehavior::className(),
                'login_url' => Yii::$app->user->loginUrl,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function afterAction($action, $result)
    {
        $result = parent::afterAction($action, $result);

        $cookies = Yii::$app->getResponse()->getCookies();

        if ($action->id === 'index') {

            TrainerDistributor::updateAll(['is_new' => 0]);
        }

        return $result;
    }

    /**
     * Lists all TrainerDistributor models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TrainerDistributorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreateNewDistributor()
    {
        $model = new DistributorForm();

        if ($model->load(Yii::$app->request->post()) && $model->create()) {
            return $this->redirect(['index']);
        }

        return $this->render('create-new-distributor', compact('model'));
    }

    public function actionCreateRegisterLink($id)
    {
        $model = TrainerDistributor::find()
            ->where(['type' => TrainerDistributor::TYPE_DISTRUBUTOR, 'id' => $id])
            ->one();

        if (!$model) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        if (empty($model->register_code)) {
            $model->createRegisterCode();
        }

        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * Displays a single TrainerDistributor model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    /**
     * Deletes an existing TrainerDistributor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TrainerDistributor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TrainerDistributor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TrainerDistributor::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
