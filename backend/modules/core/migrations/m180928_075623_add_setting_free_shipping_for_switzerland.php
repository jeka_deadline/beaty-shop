<?php

use yii\db\Migration;

/**
 * Class m180928_075623_add_setting_free_shipping_for_switzerland
 */
class m180928_075623_add_setting_free_shipping_for_switzerland extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        $this->insert('{{%core_settings}}', [
            'key' => 'order.free.shipping.switzerland.country',
            'name' => 'Free shipping for Switzerland country',
            'value' => 300,
        ]);
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function safeDown()
    {
        return true;
    }
}
