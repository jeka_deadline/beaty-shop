<?php

use yii\db\Migration;

/**
 * Class m180828_063906_add_columns_to_trainer_distributors
 */
class m180828_063906_add_columns_to_trainer_distributors extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%core_trainer_distributors}}', 'tax_number_1', $this->string(100)->defaultValue(null));
        $this->addColumn('{{%core_trainer_distributors}}', 'tax_number_2', $this->string(100)->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%core_trainer_distributors}}', 'tax_number_1');
        $this->dropColumn('{{%core_trainer_distributors}}', 'tax_number_2');
    }
}
