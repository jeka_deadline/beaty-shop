<?php

use yii\db\Migration;
use common\models\core\EmailTemplate;

/**
 * Class m180607_123353_reset_password_email_template
 */
class m180607_123353_reset_password_email_template extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        $this->batchInsert('{{%core_email_templates}}', ['code', 'type', 'subject', 'from', 'text'], [
            [
                'code' => EmailTemplate::RESET_PASSWORD_TEMPLATE_CODE,
                'type' => 'core',
                'subject' => 'Reset password',
                'from' => 'info',
                'text' => 'For reset password go to <a href="{resetPasswordLink}">this link</a>. Link be available 30 minutes',
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function safeDown()
    {
        return true;
    }
}
