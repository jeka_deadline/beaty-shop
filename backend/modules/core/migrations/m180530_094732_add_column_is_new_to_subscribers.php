<?php

use yii\db\Migration;

/**
 * Class m180530_094732_add_column_is_new_to_subscribers
 */
class m180530_094732_add_column_is_new_to_subscribers extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        $this->addColumn('{{%core_subscribers}}', 'is_new', $this->smallInteger(1)->defaultValue(1) . ' after email');
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        $this->dropColumn('{{%core_subscribers}}', 'is_new');
    }
}
