<?php

use yii\db\Migration;

/**
 * Class m180613_120741_add_column_type_to_core_settings_table
 */
class m180613_120741_add_column_type_to_core_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%core_settings}}', 'type', $this->string(100)->defaultValue('text'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%core_settings}}', 'type');
    }
}
