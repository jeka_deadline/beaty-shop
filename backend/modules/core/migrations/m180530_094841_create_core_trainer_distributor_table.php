<?php

use yii\db\Migration;

/**
 * Handles the creation of table `core_trainer_distributor`.
 */
class m180530_094841_create_core_trainer_distributor_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('core_trainer_distributors', [
            'id' => $this->primaryKey(),
            'sex' => $this->string(20)->defaultValue(null),
            'first_name' => $this->string(50)->notNull(),
            'last_name' => $this->string(50)->notNull(),
            'phone' => $this->string(20)->notNull(),
            'email' => $this->string(100)->notNull(),
            'country' => $this->string(100)->defaultValue(null),
            'state' => $this->string(100)->defaultValue(null),
            'city' => $this->string(100)->defaultValue(null),
            'address' => $this->text()->defaultValue(null),
            'type' => $this->string(15)->defaultValue(null),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);

        $this->createIndex('ix_core_trainer_distributor_type', 'core_trainer_distributors', 'type');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('core_trainer_distributors');
    }
}
