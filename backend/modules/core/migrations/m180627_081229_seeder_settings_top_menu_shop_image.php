<?php

use yii\db\Migration;

/**
 * Class m180627_081229_seeder_settings_top_menu_shop_image
 */
class m180627_081229_seeder_settings_top_menu_shop_image extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('{{%core_settings}}', [
            'key' => 'top.menu.shop.image',
            'name' => 'Picture in the menu shop',
            'value' => '',
            'type' => 'image',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }

}
