<?php

use yii\db\Migration;

/**
 * Class m180614_133723_seeder_settings_contactus_message
 */
class m180614_133723_seeder_settings_contactus_message extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        $this->insert('{{%core_settings}}', [
            'key' => 'core.success.submit.contactus',
            'name' => 'Modal message for success submit form contact us',
            'value' => 'Your application is accepted, we will contact you as soon as possible',
            'type' => 'text',
        ]);
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        return true;
    }
}
