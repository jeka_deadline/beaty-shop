<?php

use yii\db\Migration;

/**
 * Class m180914_092720_add_fields_created_updated_to_core_pages
 */
class m180914_092720_add_fields_created_updated_to_core_pages extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        $this->addColumn('{{%core_pages}}', 'created_at', $this->timestamp()->null()->defaultValue(null));
        $this->addColumn('{{%core_pages}}', 'updated_at', $this->timestamp()->null()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        $this->dropColumn('{{%core_pages}}', 'created_at');
        $this->dropColumn('{{%core_pages}}', 'updated_at');
    }
}
