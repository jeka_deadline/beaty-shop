<?php

use yii\db\Migration;

/**
 * Class m180910_133127_add_new_core_pages
 */
class m180910_133127_add_new_core_pages extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert('{{%core_pages}}', ['name', 'code', 'meta_title'], [
            [
                'name' => 'Страница новых товаров',
                'code' => 'shop-new',
                'meta_title' => 'Infinity lashes new products',
            ],
            [
                'name' => 'Страница входа',
                'code' => 'login',
                'meta_title' => 'Infinity lashes login',
            ],
            [
                'name' => 'Страница поиска',
                'code' => 'search',
                'meta_title' => 'Infinity lashes search',
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        return true;
    }
}
