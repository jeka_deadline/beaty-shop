<?php

use yii\db\Migration;

/**
 * Class m180531_073251_add_column_is_new_to_core_trainer_distributors_table
 */
class m180531_073251_add_column_is_new_to_core_trainer_distributors_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%core_trainer_distributors}}', 'is_new', $this->smallInteger(1)->defaultValue(1));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%core_trainer_distributors}}', 'is_new');
    }
}
