<?php

use yii\db\Migration;

/**
 * Class m180810_130137_seeder_settings_phone_address
 */
class m180810_130137_seeder_settings_phone_address extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('{{%core_settings}}', [
            'key' => 'core.contact.phone',
            'name' => 'Contact number',
            'value' => '+491732595108',
            'type' => 'text',
        ]);

        $this->insert('{{%core_settings}}', [
            'key' => 'core.contact.address',
            'name' => 'Contact address',
            'value' => 'Ostwall 31a 44135 Dortmund',
            'type' => 'text',
        ]);

        $this->insert('{{%core_settings}}', [
            'key' => 'core.contact.address.maplink',
            'name' => 'Contact address link map',
            'value' => 'https://goo.gl/maps/77c6f2fsbRH2',
            'type' => 'text',
        ]);

        $this->insert('{{%core_settings}}', [
            'key' => 'core.contact.email',
            'name' => 'Contact E-mail',
            'value' => 'info@infinitylashes.com',
            'type' => 'text',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }

}
