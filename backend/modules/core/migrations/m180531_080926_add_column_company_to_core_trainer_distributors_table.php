<?php

use yii\db\Migration;

/**
 * Class m180531_080926_add_column_company_to_core_trainer_distributors_table
 */
class m180531_080926_add_column_company_to_core_trainer_distributors_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%core_trainer_distributors}}', 'company', $this->string(100)->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%core_trainer_distributors}}', 'company');
    }
}
