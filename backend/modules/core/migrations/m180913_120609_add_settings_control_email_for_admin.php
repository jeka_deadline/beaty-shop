<?php

use yii\db\Migration;

/**
 * Class m180913_120609_add_settings_control_email_for_admin
 */
class m180913_120609_add_settings_control_email_for_admin extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        $this->insert('{{%core_settings}}', [
            'key' => 'send.email.toadmin.subscribers',
            'name' => 'Send information letters to admin for new subscribers',
            'value' => 1,
            'type' => 'checkbox',
        ]);

        $this->insert('{{%core_settings}}', [
            'key' => 'send.email.toadmin.neworders',
            'name' => 'Send information letters to admin for new orders',
            'value' => 1,
            'type' => 'checkbox',
        ]);
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function safeDown()
    {
        return true;
    }
}
