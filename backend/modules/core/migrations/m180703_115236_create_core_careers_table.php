<?php

use yii\db\Migration;

/**
 * Handles the creation of table `core_careers`.
 */
class m180703_115236_create_core_careers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('core_careers', [
            'id'               => $this->primaryKey(),
            'name' => $this->string(255)->defaultValue(null),
            'description' => $this->text()->defaultValue(null),
            'display_order'    => $this->integer()->defaultValue(0),
            'active'           => $this->smallInteger(1)->defaultValue(1),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);

        $this->createTable('core_careers_lang_fields', [
            'id' => $this->primaryKey(),
            'career_id' => $this->integer()->notNull(),
            'lang_id' => $this->integer()->notNull(),
            'name' => $this->string(255)->notNull(),
            'description' => $this->text()->defaultValue(null),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);

        $this->createRelations();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropRelations();

        $this->dropTable('core_careers_lang_fields');
        $this->dropTable('core_careers');
    }

    private function createRelations()
    {
        $this->createIndex('ix_core_careers_lang_fields_lang_id', '{{%core_careers_lang_fields}}', 'lang_id');
        $this->addForeignKey(
            'fk_core_careers_lang_fields_lang_id',
            '{{%core_careers_lang_fields}}',
            'lang_id',
            '{{%core_languages}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex('ix_core_careers_lang_fields_career_id', '{{%core_careers_lang_fields}}', 'career_id');
        $this->addForeignKey(
            'fk_core_careers_lang_fields_career_id',
            '{{%core_careers_lang_fields}}',
            'career_id',
            '{{%core_careers}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    private function dropRelations()
    {
        $this->dropForeignKey('fk_core_careers_lang_fields_lang_id','{{%core_careers_lang_fields}}');
        $this->dropIndex('ix_core_careers_lang_fields_lang_id', '{{%core_careers_lang_fields}}');
        $this->dropForeignKey('fk_core_careers_lang_fields_career_id','{{%core_careers_lang_fields}}');
        $this->dropIndex('ix_core_careers_lang_fields_career_id', '{{%core_careers_lang_fields}}');
    }
}
