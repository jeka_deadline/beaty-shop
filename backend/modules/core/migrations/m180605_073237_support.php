<?php

use yii\db\Migration;
use common\models\core\EmailTemplate;

/**
 * Class m180605_073237_support
 */
class m180605_073237_support extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        $this->createTable('{{%core_support_messages}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(10)->notNull(),
            'surname' => $this->string(50)->notNull(),
            'name' => $this->string(50)->notNull(),
            'email' => $this->string(100)->notNull(),
            'subject' => $this->string(100)->notNull(),
            'text' => $this->text()->notNull(),
            'is_new' => $this->smallInteger(1)->defaultValue(1),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);

        $this->batchInsert('{{%core_email_templates}}', ['code', 'type', 'subject', 'from', 'text'], [
            [
                'code' => EmailTemplate::CONFIRM_EMAIL_TEMPLATE_CODE,
                'type' => 'core',
                'subject' => 'Confirm email',
                'from' => 'info',
                'text' => 'For confirm email go to <a href="{confirmEmailLink}">this link</a>',
            ],
            [
                'code' => EmailTemplate::SUPPORT_MESSAGE_TEMPLATE_CODE,
                'type' => 'core',
                'subject' => 'Created support ticket',
                'from' => 'info',
                'text' => 'You create new support ticket. Ticket id = {ticketId}. You message: {supportMessage}',
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        $this->dropTable('{{%core_support_messages}}');
    }
}
