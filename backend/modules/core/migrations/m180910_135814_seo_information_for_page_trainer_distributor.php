<?php

use yii\db\Migration;

/**
 * Class m180910_135814_seo_information_for_page_trainer_distributor
 */
class m180910_135814_seo_information_for_page_trainer_distributor extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        $this->batchInsert('{{%core_pages}}', ['name', 'code', 'meta_title'], [
            [
                'name' => 'Страница тренеров и дистрибьюторов',
                'code' => 'trainers-distributors',
                'meta_title' => 'Infinity lashes trainers/distributors',
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function safeDown()
    {
        return true;
    }
}
