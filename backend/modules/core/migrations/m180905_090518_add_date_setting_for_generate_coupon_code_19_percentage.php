<?php

use yii\db\Migration;

/**
 * Class m180905_090518_add_date_setting_for_generate_coupon_code_19_percentage
 */
class m180905_090518_add_date_setting_for_generate_coupon_code_19_percentage extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        $dates = [
            [
                'key' => 'finish-date.register-with-discount',
                'name' => 'Finish date for register with discount 10 percentage ',
                'value' => '30.09.2018',
                'type' => 'text',
            ],
        ];

        $this->batchInsert(
            '{{%core_settings}}',
            ['key', 'name', 'value', 'type'],
            $dates
        );
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        return true;
    }
}
