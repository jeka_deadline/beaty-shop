<?php

use yii\db\Migration;

/**
 * Class m180412_073949_add_field_content_to_table_text_blocks
 */
class m180412_073949_add_field_content_to_table_text_blocks extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        $this->addColumn('{{%core_text_blocks}}', 'content', $this->text()->null() . ' after description');
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        $this->dropColumn('{{%core_text_blocks}}', 'content');
    }
}
