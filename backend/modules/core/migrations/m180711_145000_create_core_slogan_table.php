<?php

use yii\db\Migration;

/**
 * Handles the creation of table `core_slogan`.
 */
class m180711_145000_create_core_slogan_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('core_slogans', [
            'id' => $this->primaryKey(),
            'slogan' => $this->string(255)->notNull(),
            'display_order' => $this->integer()->defaultValue(0),
            'active' => $this->smallInteger(1)->defaultValue(1),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);

        $this->createTable('core_slogans_lang_fields', [
            'id' => $this->primaryKey(),
            'slogan_id' => $this->integer()->notNull(),
            'lang_id' => $this->integer()->notNull(),
            'slogan' => $this->string(255)->notNull(),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);

        $this->createRelations();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropRelations();

        $this->dropTable('core_slogans_lang_fields');
        $this->dropTable('core_slogans');
    }

    private function createRelations()
    {
        $this->createIndex('ix_core_slogans_lang_fields_lang_id', '{{%core_slogans_lang_fields}}', 'lang_id');
        $this->addForeignKey(
            'fk_core_slogans_lang_fields_lang_id',
            '{{%core_slogans_lang_fields}}',
            'lang_id',
            '{{%core_languages}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex('ix_core_slogans_lang_fields_slogan_id', '{{%core_slogans_lang_fields}}', 'slogan_id');
        $this->addForeignKey(
            'fk_core_slogans_lang_fields_slogan_id',
            '{{%core_slogans_lang_fields}}',
            'slogan_id',
            '{{%core_slogans}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    private function dropRelations()
    {
        $this->dropForeignKey('fk_core_slogans_lang_fields_lang_id','{{%core_slogans_lang_fields}}');
        $this->dropIndex('ix_core_slogans_lang_fields_lang_id', '{{%core_slogans_lang_fields}}');
        $this->dropForeignKey('fk_core_slogans_lang_fields_slogan_id','{{%core_slogans_lang_fields}}');
        $this->dropIndex('ix_core_slogans_lang_fields_slogan_id', '{{%core_slogans_lang_fields}}');
    }
}
