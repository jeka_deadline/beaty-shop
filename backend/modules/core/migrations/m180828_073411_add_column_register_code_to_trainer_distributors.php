<?php

use yii\db\Migration;

/**
 * Class m180828_073411_add_column_register_code_to_trainer_distributors
 */
class m180828_073411_add_column_register_code_to_trainer_distributors extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        $this->addColumn('{{%core_trainer_distributors}}', 'register_code', $this->string(32)->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        $this->dropColumn('{{%core_trainer_distributors}}', 'register_code');
    }
}
