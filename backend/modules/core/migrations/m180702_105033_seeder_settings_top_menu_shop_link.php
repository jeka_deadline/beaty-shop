<?php

use yii\db\Migration;

/**
 * Class m180702_105033_seeder_settings_top_menu_shop_link
 */
class m180702_105033_seeder_settings_top_menu_shop_link extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('{{%core_settings}}', [
            'key' => 'top.menu.shop.link',
            'name' => 'Link picture in the menu shop',
            'value' => '/shop',
            'type' => 'text',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
