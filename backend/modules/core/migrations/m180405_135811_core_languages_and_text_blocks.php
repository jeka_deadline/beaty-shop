<?php

use yii\db\Migration;

/**
 * Class m180405_135811_core_languages
 */
class m180405_135811_core_languages_and_text_blocks extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        // table site languages
        $this->createTable('{{%core_languages}}', [
            'id'         => $this->primaryKey(),
            'name'       => $this->string(255)->notNull(),
            'code'       => $this->string(2)->notNull()->unique(),
            'active'     => $this->smallInteger(1)->defaultValue(0),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);

        // table site text blocks
        $this->createTable('{{%core_text_blocks}}', [
            'id'          => $this->primaryKey(),
            'name'        => $this->string(255)->notNull(),
            'description' => $this->text()->null(),
            'code'        => $this->string(255)->notNull()->unique(),
            'active'     => $this->smallInteger(1)->defaultValue(0),
            'created_at'  => $this->timestamp()->null()->defaultValue(null),
            'updated_at'  => $this->timestamp()->null()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        $this->dropTable('{{%core_languages}}');
        $this->dropTable('{{%core_text_blocks}}');
    }
}
