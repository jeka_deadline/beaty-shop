<?php

use yii\db\Migration;

/**
 * Handles the creation of table `core_settings`.
 */
class m180531_125619_create_core_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('core_settings', [
            'id' => $this->primaryKey(),
            'key' => $this->string(255)->notNull()->unique(),
            'name' => $this->string(255)->defaultValue(null),
            'value' => $this->text()->defaultValue(null),
        ]);

        $this->createIndex('ix_core_settings_key', 'core_settings', 'key');

        $this->insertData();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('core_settings');
    }

    private function insertData()
    {
        $this->insert('core_settings', [
            'key' => 'order.free.shipping',
            'name' => 'Free shipping',
            'value' => 100,
        ]);
    }
}
