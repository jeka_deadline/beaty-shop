<?php

use yii\db\Migration;

/**
 * Class m180828_125452_add_permission_create_register_link_for_distributor
 */
class m180828_125452_add_permission_create_register_link_for_distributor extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $permitions = [
            [
                'name' => 'core/trainer-distributor/create-register-link',
                'type' => 2,
                'description' => 'Module Core. Create register link for distributor.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
        ];

        $this->batchInsert(
            '{{%auth_item}}',
            ['name', 'type', 'description', 'created_at', 'updated_at'],
            $permitions
        );

        $data = [];

        foreach ($permitions as $permition) {
            $data[] = [
                'parent' => 'superadmin',
                'child' => $permition['name'],
            ];
        }

        if ($data) {
            $this->batchInsert(
                '{{%auth_item_child}}',
                ['parent', 'child'],
                $data
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return true;
    }
}
