<?php

use yii\db\Migration;

/**
 * Class m180828_074141_create_permission_for_create_distributor
 */
class m180828_074141_create_permission_for_create_distributor extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        $permitions = [
            [
                'name' => 'core/trainer-distributor/create-new-distributor',
                'type' => 2,
                'description' => 'Module Core. Create distributor.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
        ];

        $this->batchInsert(
            '{{%auth_item}}',
            ['name', 'type', 'description', 'created_at', 'updated_at'],
            $permitions
        );

        $data = [];

        foreach ($permitions as $permition) {
            $data[] = [
                'parent' => 'superadmin',
                'child' => $permition['name'],
            ];
        }

        if ($data) {
            $this->batchInsert(
                '{{%auth_item_child}}',
                ['parent', 'child'],
                $data
            );
        }
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        return true;
    }
}
