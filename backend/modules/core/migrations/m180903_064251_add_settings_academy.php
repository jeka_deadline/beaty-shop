<?php

use yii\db\Migration;

/**
 * Class m180903_064251_add_settings_academy
 */
class m180903_064251_add_settings_academy extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        $settings = [
            [
                'key' => 'academy.contact-email',
                'name' => 'Academy contact email',
                'value' => 'academy@infinity-lashes.de',
                'type' => 'text',
            ],
            [
                'key' => 'academy.contact-phone',
                'name' => 'Academy contact phone',
                'value' => '+49(176)41715692',
                'type' => 'text',
            ]
        ];

        $this->batchInsert(
            '{{%core_settings}}',
            ['key', 'name', 'value', 'type'],
            $settings
        );
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        return true;
    }
}
