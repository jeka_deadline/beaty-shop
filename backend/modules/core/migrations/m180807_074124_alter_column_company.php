<?php

use yii\db\Migration;

/**
 * Class m180807_074124_alter_column_company
 */
class m180807_074124_alter_column_company extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('core_support_messages', 'company', $this->string(100)->defaultValue(null) . ' after name');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('core_support_messages', 'company', $this->string(100)->notNull() . ' after name');
    }
}
