<?php

use yii\db\Migration;

/**
 * Class m180910_124713_create_permission_core_pages
 */
class m180910_124713_create_permission_core_pages extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        $permitions = [
            [
                'name' => 'core/core-pages/index',
                'type' => 2,
                'description' => 'Module Core. Show core pages',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'core/core-pages/update',
                'type' => 2,
                'description' => 'Module Core. Update core page',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'core/core-pages/view',
                'type' => 2,
                'description' => 'Module Core. View core page',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
        ];

        $this->batchInsert(
            '{{%auth_item}}',
            ['name', 'type', 'description', 'created_at', 'updated_at'],
            $permitions
        );

        $data = [];

        foreach ($permitions as $permition) {
            $data[] = [
                'parent' => 'superadmin',
                'child' => $permition['name'],
            ];
        }

        if ($data) {
            $this->batchInsert(
                '{{%auth_item_child}}',
                ['parent', 'child'],
                $data
            );
        }
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function safeDown()
    {
        return true;
    }
}
