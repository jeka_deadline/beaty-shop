<?php

use yii\db\Migration;

/**
 * Class m180618_094327_add_company_field_to_support_messages
 */
class m180618_094327_add_company_field_to_support_messages extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        $this->addColumn('{{%core_support_messages}}', 'company', $this->string(100)->notNull() . ' after name');
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        $this->dropColumn('{{%core_support_messages}}', 'company');
    }
}
