<?php

use yii\db\Migration;

/**
 * Class m180511_120215_create_email_templates
 */
class m180511_120215_create_email_templates extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        $this->createTable('{{%core_email_templates}}', [
            'id' => $this->primaryKey(),
            'code' => $this->string(100)->notNull()->unique(),
            'type' => $this->string(20)->defaultValue('user'),
            'subject' => $this->string(255)->notNull(),
            'from' => $this->string(100)->notNull(),
            'text' => $this->text()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        $this->dropTable('{{%core_email_templates}}');
    }
}
