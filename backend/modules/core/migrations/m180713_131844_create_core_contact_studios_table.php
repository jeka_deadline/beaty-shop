<?php

use yii\db\Migration;

/**
 * Handles the creation of table `core_contact_studios`.
 */
class m180713_131844_create_core_contact_studios_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('core_contact_studios', [
            'id' => $this->primaryKey(),
            'name' => $this->string(60)->notNull(),
            'description' => $this->text()->defaultValue(null),
            'address' => $this->string(80)->notNull(),
            'lat' => $this->float()->notNull(),
            'lng' => $this->float()->notNull(),
            'type' => $this->string(30)->defaultValue(null),
            'display_order' => $this->integer()->defaultValue(0),
            'active' => $this->smallInteger(1)->defaultValue(1),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);

        $this->createTable('core_contact_studios_lang_fields', [
            'id' => $this->primaryKey(),
            'contact_studio_id' => $this->integer()->notNull(),
            'lang_id' => $this->integer()->notNull(),
            'name' => $this->string(60)->notNull(),
            'description' => $this->text()->defaultValue(null),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);

        $this->createRelations();

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropRelations();

        $this->dropTable('core_contact_studios_lang_fields');
        $this->dropTable('core_contact_studios');
    }

    private function createRelations()
    {
        $this->createIndex('ix_core_contact_studios_lang_fields_lang_id', '{{%core_contact_studios_lang_fields}}', 'lang_id');
        $this->addForeignKey(
            'fk_core_contact_studios_lang_fields_lang_id',
            '{{%core_contact_studios_lang_fields}}',
            'lang_id',
            '{{%core_languages}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex('ix_core_contact_studios_lang_fields_contact_studio_id', '{{%core_contact_studios_lang_fields}}', 'contact_studio_id');
        $this->addForeignKey(
            'fk_core_contact_studios_lang_fields_contact_studio_id',
            '{{%core_contact_studios_lang_fields}}',
            'contact_studio_id',
            '{{%core_contact_studios}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    private function dropRelations()
    {
        $this->dropForeignKey('fk_core_contact_studios_lang_fields_lang_id','{{%core_contact_studios_lang_fields}}');
        $this->dropIndex('ix_core_contact_studios_lang_fields_lang_id', '{{%core_contact_studios_lang_fields}}');
        $this->dropForeignKey('fk_core_contact_studios_lang_fields_contact_studio_id','{{%core_contact_studios_lang_fields}}');
        $this->dropIndex('ix_core_contact_studios_lang_fields_contact_studio_id', '{{%core_contact_studios_lang_fields}}');
    }
}
