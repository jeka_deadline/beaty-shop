<?php

use yii\db\Migration;

/**
 * Class m180926_120209_add_setting_free_shipping_for_other_countries
 */
class m180926_120209_add_setting_free_shipping_for_other_countries extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        $this->insert('{{%core_settings}}', [
            'key' => 'order.free.shipping.other.countries',
            'name' => 'Free shipping for other countries',
            'value' => 300,
        ]);
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function safeDown()
    {
        return true;
    }
}
