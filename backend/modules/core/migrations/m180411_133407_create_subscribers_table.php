<?php

use yii\db\Migration;
use yii\db\Expression;

/**
 * Handles the creation of table `subsribers`.
 */
class m180411_133407_create_subscribers_table extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        // table subscribers
        $this->createTable('{{%core_subscribers}}', [
            'id'         => $this->primaryKey(),
            'email'      => $this->string(100)->unique()->notNull(),
            'created_at' => $this->timestamp()->defaultValue(new Expression('NOW()')),
        ]);
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        $this->dropTable('{{%core_subscribers}}');
    }
}
