<?php

use yii\db\Migration;

/**
 * Class m180910_122019_static_pages_seo
 */
class m180910_122019_static_pages_seo extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        $this->createTable('{{%core_pages}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'code' => $this->string(100)->unique(),
            'meta_title' => $this->string(255)->null(),
            'meta_keywords' => $this->string(255)->null(),
            'meta_description' => $this->string(255)->null(),
        ]);

        $this->insertPages();
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        $this->dropTable('{{%core_pages}}');
    }

    /**
     * Insert core pages.
     *
     * @access private
     * @return void
     */
    private function insertPages()
    {
        $this->batchInsert('{{%core_pages}}', ['name', 'code', 'meta_title'], [
            [
                'name' => 'Главная',
                'code' => 'main',
                'meta_title' => 'Infinity lashes',
            ],
            [
                'name' => 'Страница академии',
                'code' => 'academy-index',
                'meta_title' => 'Infinity lashes academy',
            ],
            [
                'name' => 'Страница магазина',
                'code' => 'shop',
                'meta_title' => 'Infinity lashes shop',
            ],
            [
                'name' => 'Страница блога',
                'code' => 'blog',
                'meta_title' => 'Infinity lashes blog',
            ],
            [
                'name' => 'Страница контактов',
                'code' => 'contact',
                'meta_title' => 'Infinity lashes contacts',
            ],
            [
                'name' => 'Страница аккаунта пользователя',
                'code' => 'user-account',
                'meta_title' => 'Infinity lashes account',
            ],
            [
                'name' => 'Страница корзины',
                'code' => 'cart',
                'meta_title' => 'Infinity lashes cart',
            ],
            [
                'name' => 'Страница чекаута',
                'code' => 'checkout',
                'meta_title' => 'Infinity lashes checkout',
            ],
            [
                'name' => 'Страница благодарности',
                'code' => 'thanks',
                'meta_title' => 'Infinity lashes thanks',
            ],
            [
                'name' => 'Страница карьеры',
                'code' => 'careers',
                'meta_title' => 'Infinity lashes jobs',
            ],
            [
                'name' => 'Страница студий',
                'code' => 'studois',
                'meta_title' => 'Infinity lashes studios',
            ],
        ]);
    }
}
