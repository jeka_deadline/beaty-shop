<?php

namespace backend\modules\academy\models;

use backend\modules\core\behaviors\LangBehavior;
use backend\modules\core\models\Language;
use Yii;
use yii\helpers\ArrayHelper;

class InfoBlock extends \common\models\academy\InfoBlock
{
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                [
                    'class' => LangBehavior::className(),
                    'langModel' => InfoBlockLangField::className(),
                    'modelForeignKey' => 'info_block_id',
                    'languages' => Language::find()->all(),
                    'attributes' => [
                        'name',
                        'data',
                    ],
                ]
            ]
        );
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;

        if (($block = $this->block) && $block->load(Yii::$app->request->post()) && $block->validate()) {
            $this->data = serialize($block);
            return true;
        }
        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        if (is_array($this->blocks)) {
            foreach ($this->languages as $language) {
                $model = $this->factoryClassInfoBlock();
                $model->attributes = $this->blocks[$language->code];
                $this->{'data_' . $language->code} = serialize($model);
            }
        }

        parent::afterSave($insert, $changedAttributes);
    }

    public function getLangBlocks()
    {
        if (!$this->blocks) {
            foreach ($this->languages as $language) {
                $this->blocks[$language->code] = $this->getUnserializeData($this->{'data_' . $language->code});
            }
        }
        return $this->blocks;
    }
}
