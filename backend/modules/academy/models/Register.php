<?php

namespace backend\modules\academy\models;

use Yii;

class Register extends \common\models\academy\Register
{

    public static function getCountNew()
    {
        return static::find()
            ->where(['is_new' => 1])
            ->count();
    }

    public static function isHasNew()
    {
        return (static::getCountNew()) ? true : false;
    }
}
