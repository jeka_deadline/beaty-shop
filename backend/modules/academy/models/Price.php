<?php

namespace backend\modules\academy\models;

use backend\modules\core\behaviors\LangBehavior;
use backend\modules\core\models\Language;
use yii\helpers\ArrayHelper;

class Price extends \common\models\academy\Price
{
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                [
                    'class' => LangBehavior::className(),
                    'langModel' => PriceLangField::className(),
                    'modelForeignKey' => 'academy_price_id',
                    'languages' => Language::find()->all(),
                    'attributes' => [
                        'name',
                        'description',
                    ],
                ]
            ]
        );
    }
}
