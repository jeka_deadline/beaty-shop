<?php

namespace backend\modules\academy\models;


use backend\modules\core\behaviors\LangBehavior;
use backend\modules\core\models\Language;
use Imagine\Image\Box;
use Imagine\Image\Point;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\imagine\Image;
use yii\web\UploadedFile;

class Academy extends \common\models\academy\Academy
{
    public $file;
    public $small_file;
    public $crop_info;

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['file', 'small_file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
                [['crop_info'], 'safe']
            ]
        );
    }


    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                [
                    'class' => LangBehavior::className(),
                    'langModel' => AcademysLangField::className(),
                    'modelForeignKey' => 'academy_id',
                    'languages' => Language::find()->all(),
                    'attributes' => [
                        'name',
                        'description',
                        'note',
                    ],
                ]
            ]
        );
    }

    public function saveImage()
    {
        if ($this->validate() && $this->file) {
            $allPath = Yii::getAlias('@frontend/web') . '/' . self::$path;
            if (!is_dir($allPath)) {
                mkdir($allPath, 0777, true);
            }

            $file = $this->file;
            $filename = md5(uniqid($file->baseName)).'.'.$file->extension;
            if ($file->saveAs($allPath . $filename)) {
                chmod($allPath . $filename, 0777);
            }
            $this->updateAttributes(['image' => $filename]);

            return true;
        } else {
            return false;
        }
    }

    public function saveSmallImage()
    {
        if ($this->validate() && $this->small_file) {
            $path = Yii::getAlias('@frontend/web') . '/' . self::$path;
            if (!is_dir($path)) {
                mkdir($path, 0777, true);
            }

            $image = Image::getImagine()->open($this->small_file->tempName);

            $file = $this->small_file;
            $filename = md5(uniqid($file->baseName)) . '.' . $file->extension;
            $pathThumbImage = $path . $filename;

            if ($this->crop_info) {
                // rendering information about crop of ONE option
                $cropInfo = Json::decode($this->crop_info)[0];
                $cropInfo['dWidth'] = (int)$cropInfo['dWidth']; //new width image
                $cropInfo['dHeight'] = (int)$cropInfo['dHeight']; //new height image
                $cropInfo['x'] = $cropInfo['x']; //begin position of frame crop by X
                $cropInfo['y'] = $cropInfo['y']; //begin position of frame crop by Y
                // Properties bolow we don't use in this example
                //$cropInfo['ratio'] = $cropInfo['ratio'] == 0 ? 1.0 : (float)$cropInfo['ratio']; //ratio image.
                $cropInfo['width'] = (int)$cropInfo['width']; //width of cropped image
                $cropInfo['height'] = (int)$cropInfo['height']; //height of cropped image
                //$cropInfo['sWidth'] = (int)$cropInfo['sWidth']; //width of source image
                //$cropInfo['sHeight'] = (int)$cropInfo['sHeight']; //height of source image

                //saving thumbnail
                $newSizeThumb = new Box($cropInfo['dWidth'], $cropInfo['dHeight']);
                $cropSizeThumb = new Box($cropInfo['width'], $cropInfo['height']); //frame size of crop
                $cropPointThumb = new Point($cropInfo['x'], $cropInfo['y']);

                $image->resize($newSizeThumb)
                    ->crop($cropPointThumb, $cropSizeThumb)
                    ->save($pathThumbImage, ['quality' => 100]);
            } else {
                $image->save($pathThumbImage, ['quality' => 100]);
            }

            $this->updateAttributes(['small_image' => $filename]);

            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $this->file = UploadedFile::getInstance($this, 'file');
        $this->saveImage();
        $this->small_file = UploadedFile::getInstance($this, 'small_file');
        $this->saveSmallImage();
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }

        $filePath = Yii::getAlias('@frontend/web') . '/' . self::$path. '/'. $this->image;
        if ($this->image && file_exists($filePath)) {
            unlink($filePath);
        }

        $filePath = Yii::getAlias('@frontend/web') . '/' . self::$path. '/'. $this->small_image;
        if ($this->small_image && file_exists($filePath)) {
            unlink($filePath);
        }

        return true;
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'small_file' => 'Small Image',
        ]);
    }

}
