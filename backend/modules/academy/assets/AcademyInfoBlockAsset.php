<?php

namespace backend\modules\academy\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AcademyInfoBlockAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [

    ];
    public $js = [
        'js/academy-info-block.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public $publishOptions = [
        'forceCopy' => YII_DEBUG?true:false,
    ];
}
