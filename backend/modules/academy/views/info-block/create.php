<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\academy\models\InfoBlock */

$this->title = 'Create Info Block';
$this->params['breadcrumbs'][] = ['label' => 'Info Blocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="info-block-create">

    <?= $this->render('_form', [
        'modelInfoBlock' => $modelInfoBlock,
        'languages' => $languages,
        'academy_id' => $academy_id
    ]) ?>

</div>
