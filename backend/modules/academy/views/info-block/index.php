<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\academy\models\searchModels\InfoBlockSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Info Blocks';
$this->params['breadcrumbs'][] = $this->title;

$this->title = 'Update Academy';
$this->params['breadcrumbs'][] = ['label' => 'Academys', 'url' => ['/academy/academy/index']];
$this->params['breadcrumbs'][] = ['label' => $academy_id, 'url' => ['/academy/academy/view', 'id' => $academy_id]];
$this->params['breadcrumbs'][] = 'Update';

?>
<div class="info-block-index table-responsive">
    <ul class="nav nav-tabs">
        <li role="presentation"><a href="<?= Url::to(['/academy/academy/update', 'id' => $academy_id]) ?>">Course</a></li>
        <li role="presentation" class="active"><a href="<?= Url::to(['/academy/info-block/index', 'academy_id' => $academy_id]) ?>">Info Blocks</a></li>
        <li role="presentation"><a href="<?= Url::to(['/academy/price/index', 'academy_id' => $academy_id]) ?>">Prices</a></li>
    </ul>

    <br>

    <p>
        <?= Html::a('Create Info Block', ['create', 'academy_id' => $academy_id], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'display_order',
            [
                'attribute' => 'active',
                'value' => function($model){
                    return ($model->active)?'Yes':'No';
                },
                'filter' => Html::activeDropDownList($searchModel, 'active', [1=>'Yes',0=>'No'], ['class' => 'form-control', 'prompt' => '']),
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'urlCreator' => function ($action, $model, $key, $index) use ($academy_id) {
                    $params = is_array($key) ? $key : ['id' => (string) $key];
                    $params['academy_id'] = $academy_id;
                    $params[0] =  $action;
                    return Url::toRoute($params);
                }
            ],
        ],
    ]); ?>
</div>
