<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\academy\models\InfoBlock */

$this->title = 'Update nfo Blocks: ' . $modelInfoBlock->name;
$this->params['breadcrumbs'][] = ['label' => 'Info Blocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $academy_id, 'url' => ['/academy/academy/view', 'id' => $academy_id]];
$this->params['breadcrumbs'][] = 'Update';

?>
<div class="info-block-update">

    <?= $this->render('_form', [
        'modelInfoBlock' => $modelInfoBlock,
        'languages' => $languages,
        'academy_id' => $academy_id
    ]) ?>

</div>
