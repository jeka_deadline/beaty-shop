<?php

use yii\helpers\Html;

?>

<?= $form->field($modelInfoBlock, 'name_' . $language->code)->textInput(['maxlength' => true])->label($modelInfoBlock->getAttributeLabel('name')) ?>

<?php if (!$modelInfoBlock->isNewRecord && $modelInfoBlock->langBlocks[$language->code] && $modelInfoBlock->langBlocks[$language->code]->langAttributes): ?>
    <div class="panel panel-default">
        <div class="panel-body">
            <?= Html::tag('h4','Info Block Data') ?>
                <?php foreach ($modelInfoBlock->langBlocks[$language->code]->langAttributes as $name => $attribute):?>
                    <?= $this->render('fields/'.$modelInfoBlock->langBlocks[$language->code]->attributeTemplate($name), [
                        'form' => $form,
                        'model' => $modelInfoBlock->langBlocks[$language->code],
                        'name' => $name,
                        'options' => [
                            'name' => Html::getInputName($modelInfoBlock, 'blocks')
                                .'['.$language->code
                                .']['
                                .$name
                                .']'
                        ]
                    ]) ?>
                <?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>
