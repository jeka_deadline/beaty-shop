<?php

use backend\modules\academy\models\InfoBlock;
use yii\helpers\Html;

?>

<?= $form->field($modelInfoBlock, 'academy_id')->hiddenInput(['value' => $academy_id])->label(false) ?>

<?= $form->field($modelInfoBlock, 'name')->textInput(['maxlength' => true]) ?>

<?php if ($modelInfoBlock->isNewRecord): ?>
    <?= $form->field($modelInfoBlock, 'type')->dropDownList(InfoBlock::typeLabels()) ?>
<?php endif; ?>

<?= $form->field($modelInfoBlock, 'display_order')->textInput() ?>

<?= $form->field($modelInfoBlock, 'active')->checkbox() ?>

<?php if (!$modelInfoBlock->isNewRecord && $modelInfoBlock->block): ?>
    <div class="panel panel-default">
        <div class="panel-body">
            <?= Html::tag('h4','Info Block Data') ?>
                <?php foreach ($modelInfoBlock->block->attributes as $name => $attribute):?>
                    <?= $this->render('fields/'.$modelInfoBlock->block->attributeTemplate($name), [
                        'form' => $form,
                        'model' => $modelInfoBlock->block,
                        'name' => $name
                    ]) ?>
                <?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>