<?php

use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\academy\models\InfoBlock */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="info-block-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    $items = [
        [
            'label' => 'InfoBlock',
            'content' => $this->render(
                '_form_tab_info_block',
                compact(
                    'modelInfoBlock',
                    'academy_id',
                    'form')
            ),
        ],
    ];

    foreach ($languages as $language) {
        $items[] = [
            'label' => $language->name,
            'content' => $this->render('_form_tab_lang_fields', compact('modelInfoBlock','academy_id', 'language', 'form')),
        ];
    }
    ?>

    <?= Tabs::widget([
        'items' => $items,
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
