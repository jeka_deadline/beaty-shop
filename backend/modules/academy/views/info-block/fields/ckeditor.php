<?php

use mihaildev\ckeditor\CKEditor;

if (!isset($options)) $options = [];
?>
<?= $form->field($model, $name)->widget(CKEditor::className(), $options) ?>
