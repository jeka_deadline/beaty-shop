<?php

use kartik\file\FileInput;

if (!isset($options)) $options = [];

?>
<?= $form->field($model, $name)->widget(FileInput::classname(), [
    'options' => array_merge([
        'accept' => 'image/*',
    ],$options),
    'pluginOptions' => [
        'initialPreview' => $model->getUrlImageInput(),
        'initialPreviewAsData'=>true,

        'browseClass' => 'btn btn-success',
        'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
        'browseLabel' =>  'Select Photo',
    ],
]) ?>
