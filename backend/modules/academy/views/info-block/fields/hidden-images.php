<?php

use backend\modules\academy\assets\AcademyInfoBlockAsset;
use yii\helpers\Html;

AcademyInfoBlockAsset::register($this);

if (!isset($options)) $options = [];

$options['multiple'] = 'true';
$options['class'] = 'hide';

$inputId = Html::getInputId($model, $name);

?>
<?= $form->field($model, $name.'[]')->dropDownList($model->$name?array_combine($model->$name, $model->$name):[], $options)->label(false) ?>


<?php
$script = <<< JS
$(function(){
    $('#{$inputId} option').prop('selected', true);
});
JS;

$this->registerJs($script, yii\web\View::POS_READY);
?>
