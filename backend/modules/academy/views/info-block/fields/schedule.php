<?php

use backend\modules\academy\widgets\LobiList\LobiList;
use yii\helpers\Html;

if (!isset($options)) $options = [];

$inputId = Html::getInputId($model, $name);
$inputName = Html::getInputName($model, $name);

?>
<?= $form->field($model, $name)->hiddenInput($options) ?>

<?= LobiList::widget([
    'id' => $inputId,
    'name' => isset($options['name'])?$options['name']:$inputName,
]); ?>

