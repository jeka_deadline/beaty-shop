<?php

use kartik\file\FileInput;
use yii\helpers\Url;

if (!isset($options)) $options = [];

?>
<?= $form->field($model, $name.'[]')->widget(FileInput::classname(), [
    'options' => array_merge([
        'multiple' => true,
        'accept' => 'image/*',
    ],$options),
    'pluginOptions' => [
        'initialPreview' => $model->getUrlImagesInput(),
        'initialPreviewConfig' => $model->getImagesInputConfig(),
        'initialPreviewAsData'=>true,

        'browseClass' => 'btn btn-success',
        'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
        'browseLabel' =>  'Select Photo',

        'overwriteInitial'=>false,
    ],
    'pluginEvents' => [
        'filedeleted' => 'deleteInfoBlockImages',
        'filesorted' => 'sortInfoBlockImages',
    ]
]) ?>