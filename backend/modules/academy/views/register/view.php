<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\academy\models\Register */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Registers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="register-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute'=>'academy_id',
                'value'=> (($model->academy_id)?$model->academy->name:''),
            ],
            [
                'attribute'=>'price_id',
                'format' => 'raw',
                'value'=> function ($model) {
                    if (!$model->price_id) return '';
                    $price = $model->price;
                    return "<p>{$price->name}</p><p>{$price->description}</p><p>{$price->price} $</p>";
                },
            ],
            'sex',
            'first_name',
            'last_name',
            'phone',
            'email:email',
            'company',
            'seats',
            'is_new:boolean',
            'created_at',
//            'updated_at',
        ],
    ]) ?>

</div>
