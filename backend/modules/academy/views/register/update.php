<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\academy\models\Register */

$this->title = 'Update Register: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Registers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="register-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
