<?php

use backend\modules\academy\models\Academy;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\academy\models\searchModels\RegisterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Registers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="register-index table-responsive">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'academy_id',
                'value' => function($model){
                    return ($model->academy_id)?$model->academy->name:'';
                },
                'filter' => ArrayHelper::map(Academy::find()->all(), 'id', 'name')
            ],
            'sex',
            'first_name',
            'last_name',
            'phone',
            'email:email',
            'seats',
//            'is_new',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
