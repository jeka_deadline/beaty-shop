<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\modules\academy\models\Academy */

$this->title = 'Update Academy: ' . $modelAcademy->name;
$this->params['breadcrumbs'][] = ['label' => 'Academies', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $modelAcademy->name, 'url' => ['view', 'id' => $modelAcademy->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="academy-update">
    <ul class="nav nav-tabs">
        <li role="presentation" class="active"><a href="<?= Url::to(['/academy/academy/update', 'id' => $modelAcademy->id]) ?>">Course</a></li>
        <li role="presentation"><a href="<?= Url::to(['/academy/info-block/index', 'academy_id' => $modelAcademy->id]) ?>">Info Blocks</a></li>
        <li role="presentation"><a href="<?= Url::to(['/academy/price/index', 'academy_id' => $modelAcademy->id]) ?>">Prices</a></li>
    </ul>
    <br>

    <?= $this->render('_form', [
        'modelAcademy' => $modelAcademy,
        'languages' => $languages
    ]) ?>

</div>
