<?php

use yii\helpers\Html;

?>

<?= $form->field($modelAcademy, 'name_' . $language->code)->textInput(['maxlength' => true])->label($modelAcademy->getAttributeLabel('name')) ?>

<?= $form->field($modelAcademy, 'description_' . $language->code)->textarea(['rows' => 6])->label($modelAcademy->getAttributeLabel('description')) ?>

<?= $form->field($modelAcademy, 'note_' . $language->code)->textarea(['rows' => 6, 'maxlength' => true])->label($modelAcademy->getAttributeLabel('note')) ?>
