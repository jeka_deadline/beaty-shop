<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\academy\models\searchModels\AcademySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Academies';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="academy-index table-responsive">

    <p>
        <?= Html::a('Create Academy', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'slug',
            'name',
            'date',
//            'duration',
            'price',
            [
                'attribute' => 'active',
                'value' => function($model){
                    return ($model->active)?'Yes':'No';
                },
                'filter' => Html::activeDropDownList($searchModel, 'active', [1=>'Yes',0=>'No'], ['class' => 'form-control', 'prompt' => '']),
            ],
            'display_order',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
