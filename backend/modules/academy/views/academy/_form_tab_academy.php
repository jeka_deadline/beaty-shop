<?php

use bupy7\cropbox\CropboxWidget;
use kartik\date\DatePicker;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;

?>

<?= $form->field($modelAcademy, 'name')->textInput(['maxlength' => true]) ?>

<?= $form->field($modelAcademy, 'slug')->textInput(['maxlength' => true]) ?>

<?= $form->field($modelAcademy, 'date')->widget(DatePicker::className(), [
    'type' => DatePicker::TYPE_COMPONENT_APPEND,
    'pluginOptions' => [
        'autoclose'=>true,
        'startDate' => '1992-01-01',
        'format' => 'yyyy-mm-dd'
    ]
]) ?>

<?= $form->field($modelAcademy, 'description')->widget(CKEditor::className()) ?>

<?= $form->field($modelAcademy, 'note')->textarea(['rows' => 6, 'maxlength' => true]) ?>

<?php if (!$modelAcademy->isNewRecord): ?>
    <?= $form->field($modelAcademy, 'file')->widget(FileInput::classname(), [
        'options' => [
            'accept' => 'image/*',
        ],
        'pluginOptions' => [
            'initialPreview' => $modelAcademy->getUrlImageInput(),
            'initialPreviewAsData'=>true,

            'browseClass' => 'btn btn-success',
            'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
            'browseLabel' =>  'Select Photo',
        ],
    ]) ?>
    <?= $form->field($modelAcademy, 'small_file')->widget(CropboxWidget::className(), [
        'pluginOptions' => [
            'variants' => [
                [
                    'width' => 370,
                    'height' => 420
                ]
            ]
        ],
        'croppedDataAttribute' => 'crop_info',
        'croppedImagesUrl' => [
            $modelAcademy->urlSmallImage
        ],
    ]) ?>
<?php endif; ?>

<?= $form->field($modelAcademy, 'duration')->textInput() ?>

<?= $form->field($modelAcademy, 'price')->textInput() ?>

<?= $form->field($modelAcademy, 'display_order')->textInput() ?>

<?= $form->field($modelAcademy, 'active')->checkbox() ?>