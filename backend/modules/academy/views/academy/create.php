<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\academy\models\Academy */

$this->title = 'Create Academy';
$this->params['breadcrumbs'][] = ['label' => 'Academies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="academy-create">

    <?= $this->render('_form', [
        'modelAcademy' => $modelAcademy,
        'languages' => $languages
    ]) ?>

</div>
