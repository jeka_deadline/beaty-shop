<?php

use yii\helpers\Html;

?>

<?= $form->field($modelPrice, 'name_' . $language->code)->textInput(['maxlength' => true])->label($modelPrice->getAttributeLabel('name')) ?>

<?= $form->field($modelPrice, 'description_' . $language->code)->textarea(['rows' => 6])->label($modelPrice->getAttributeLabel('description')) ?>