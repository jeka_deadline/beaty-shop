<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\academy\models\Price */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Prices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<ul class="nav nav-tabs">
    <li role="presentation"><a href="<?= Url::to(['/academy/academy/update', 'id' => $academy_id]) ?>">Academy</a></li>
    <li role="presentation"><a href="<?= Url::to(['/academy/info-block/index', 'academy_id' => $academy_id]) ?>">Info Blocks</a></li>
    <li role="presentation" class="active"><a href="<?= Url::to(['/academy/price/index', 'academy_id' => $academy_id]) ?>">Prices</a></li>
</ul>

<br>

<div class="price-view">

    <p>
        <?= Html::a('List', ['index', 'id' => $model->id, 'academy_id' => $academy_id], ['class' => 'btn btn-default']) ?>
        <?= Html::a('Update', ['update', 'id' => $model->id, 'academy_id' => $academy_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id, 'academy_id' => $academy_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'description:ntext',
            'price',
            'best_offer:boolean',
            'active:boolean',
        ],
    ]) ?>

</div>
