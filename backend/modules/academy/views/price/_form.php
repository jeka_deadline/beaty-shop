<?php

use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\academy\models\Price */
/* @var $form yii\widgets\ActiveForm */
?>

<ul class="nav nav-tabs">
    <li role="presentation"><a href="<?= Url::to(['/academy/academy/update', 'id' => $academy_id]) ?>">Course</a></li>
    <li role="presentation"><a href="<?= Url::to(['/academy/info-block/index', 'academy_id' => $academy_id]) ?>">Info Blocks</a></li>
    <li role="presentation" class="active"><a href="<?= Url::to(['/academy/price/index', 'academy_id' => $academy_id]) ?>">Prices</a></li>
</ul>

<br>

<div class="price-form">
    <?php $form = ActiveForm::begin(); ?>

    <?php
    $items = [
        [
            'label' => 'Default',
            'content' => $this->render(
                '_form_tab_price',
                compact(
                    'modelPrice',
                    'academy_id',
                    'form')
            ),
        ],
    ];

    foreach ($languages as $language) {
        $items[] = [
            'label' => $language->name,
            'content' => $this->render('_form_tab_lang_fields', compact('modelPrice','academy_id', 'language', 'form')),
        ];
    }
    ?>

    <?= Tabs::widget([
        'items' => $items,
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
