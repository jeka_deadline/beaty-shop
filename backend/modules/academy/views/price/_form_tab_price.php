<?php

?>

<?= $form->field($modelPrice, 'academy_id')->hiddenInput(['value' => $academy_id])->label(false) ?>

<?= $form->field($modelPrice, 'name')->textInput(['maxlength' => true]) ?>

<?= $form->field($modelPrice, 'description')->textarea(['rows' => 6]) ?>

<?= $form->field($modelPrice, 'price')->textInput() ?>

<?= $form->field($modelPrice, 'best_offer')->checkbox() ?>

<?= $form->field($modelPrice, 'active')->checkbox() ?>