<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\academy\models\Price */

$this->title = 'Create Price';
$this->params['breadcrumbs'][] = ['label' => 'Academys', 'url' => ['/academy/academy/index']];
$this->params['breadcrumbs'][] = ['label' => $academy_id, 'url' => ['/academy/academy/view', 'id' => $academy_id]];
$this->params['breadcrumbs'][] = ['label' => 'Prices', 'url' => ['index', 'academy_id' => $academy_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="price-create">


    <?= $this->render('_form', [
        'modelPrice' => $modelPrice,
        'languages' => $languages,
        'academy_id' => $academy_id
    ]) ?>

</div>
