<?php

use yii\db\Migration;

/**
 * Class m180814_115020_seeder_settings_academy_disable
 */
class m180814_115020_seeder_settings_academy_disable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('{{%core_settings}}', [
            'key' => 'academy.enable',
            'name' => 'Enable the academy',
            'value' => 0,
            'type' => 'text',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180814_115020_seeder_settings_academy_disable cannot be reverted.\n";

        return false;
    }
}
