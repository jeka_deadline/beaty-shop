<?php

use yii\db\Migration;

/**
 * Handles the creation of table `info_blocks`.
 */
class m180602_140733_create_info_blocks_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('academy_info_blocks', [
            'id' => $this->primaryKey(),
            'academy_id' => $this->integer()->notNull(),
            'name' => $this->string(255)->notNull(),
            'type' => $this->string(100)->null(),
            'data' => $this->text()->defaultValue(null),
            'display_order' => $this->integer()->defaultValue(0),
            'active' => $this->smallInteger(1)->defaultValue(1),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);

        $this->createTable('academy_info_blocks_lang_fields', [
            'id' => $this->primaryKey(),
            'info_block_id' => $this->integer()->notNull(),
            'lang_id' => $this->integer()->notNull(),
            'name' => $this->string(255)->notNull(),
            'data' => $this->text()->defaultValue(null),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);

        $this->createRelations();

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropRelations();

        $this->dropTable('academy_info_blocks_lang_fields');
        $this->dropTable('academy_info_blocks');
    }

    private function createRelations()
    {
        $this->createIndex('ix_academy_info_blocks_academy_id', '{{%academy_info_blocks}}', 'academy_id');
        $this->addForeignKey(
            'fk_academy_info_blocks_academy_id',
            '{{%academy_info_blocks}}',
            'academy_id',
            '{{%academy_academys}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex('ix_academy_info_blocks_lang_fields_lang_id', '{{%academy_info_blocks_lang_fields}}', 'lang_id');
        $this->addForeignKey(
            'fk_academy_info_blocks_lang_fields_lang_id',
            '{{%academy_info_blocks_lang_fields}}',
            'lang_id',
            '{{%core_languages}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex('ix_academy_info_blocks_lang_fields_info_block_id', '{{%academy_info_blocks_lang_fields}}', 'info_block_id');
        $this->addForeignKey(
            'fk_academy_info_blocks_lang_fields_info_block_id',
            '{{%academy_info_blocks_lang_fields}}',
            'info_block_id',
            '{{%academy_info_blocks}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    private function dropRelations()
    {
        $this->dropForeignKey('fk_academy_info_blocks_lang_fields_lang_id','{{%academy_info_blocks_lang_fields}}');
        $this->dropIndex('ix_academy_info_blocks_lang_fields_lang_id', '{{%academy_info_blocks_lang_fields}}');
        $this->dropForeignKey('fk_academy_info_blocks_lang_fields_info_block_id','{{%academy_info_blocks_lang_fields}}');
        $this->dropIndex('ix_academy_info_blocks_lang_fields_info_block_id', '{{%academy_info_blocks_lang_fields}}');
        $this->dropForeignKey('fk_academy_info_blocks_academy_id','{{%academy_info_blocks}}');
        $this->dropIndex('ix_academy_info_blocks_academy_id', '{{%academy_info_blocks}}');
    }
}
