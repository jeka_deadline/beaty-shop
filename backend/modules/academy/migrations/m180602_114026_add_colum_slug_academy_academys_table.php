<?php

use yii\db\Migration;

/**
 * Class m180602_114026_add_colum_slug_academy_academys_table
 */
class m180602_114026_add_colum_slug_academy_academys_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('academy_academys', 'slug', $this->string(255)->notNull()->unique());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('academy_academys', 'slug');
    }
}
