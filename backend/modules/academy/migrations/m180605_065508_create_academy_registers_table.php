<?php

use yii\db\Migration;

/**
 * Handles the creation of table `academy_registers`.
 */
class m180605_065508_create_academy_registers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('academy_registers', [
            'id' => $this->primaryKey(),
            'academy_id' => $this->integer()->notNull(),
            'sex' => $this->string(20)->defaultValue(null),
            'first_name' => $this->string(50)->notNull(),
            'last_name' => $this->string(50)->notNull(),
            'phone' => $this->string(20)->notNull(),
            'email' => $this->string(100)->notNull(),
            'is_new' => $this->smallInteger(1)->defaultValue(1),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);

        $this->createRelations();

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropRelations();

        $this->dropTable('academy_registers');
    }

    private function createRelations()
    {
        $this->createIndex('ix_academy_registers_academy_id', '{{%academy_registers}}', 'academy_id');
        $this->addForeignKey(
            'fk_academy_registers_academy_id',
            '{{%academy_registers}}',
            'academy_id',
            '{{%academy_academys}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    private function dropRelations()
    {
        $this->dropForeignKey('fk_academy_registers_academy_id','{{%academy_registers}}');
        $this->dropIndex('ix_academy_registers_academy_id', '{{%academy_registers}}');
    }
}
