<?php

use yii\db\Migration;

/**
 * Class m180608_132906_add_colum_small_image_academy_academys_table
 */
class m180608_132906_add_colum_small_image_academy_academys_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('academy_academys', 'small_image', $this->string(255)->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('academy_academys', 'small_image');
    }
}
