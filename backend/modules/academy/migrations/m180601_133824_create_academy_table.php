<?php

use yii\db\Migration;

/**
 * Handles the creation of table `academy`.
 */
class m180601_133824_create_academy_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('academy_academys', [
            'id' => $this->primaryKey(),
            'image' => $this->string(255)->defaultValue(null),
            'name' => $this->string(255)->notNull(),
            'description' => $this->text()->defaultValue(null),
            'note' => $this->string(1000)->null(),
            'duration' => $this->integer()->defaultValue(0),
            'price' => $this->float()->defaultValue(0),
            'active' => $this->smallInteger(1)->defaultValue(1),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);

        $this->createTable('academy_academys_lang_fields', [
            'id' => $this->primaryKey(),
            'academy_id' => $this->integer()->notNull(),
            'lang_id' => $this->integer()->notNull(),
            'name' => $this->string(255)->notNull(),
            'description' => $this->text()->defaultValue(null),
            'note' => $this->string(1000)->null(),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);

        $this->createRelations();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropRelations();

        $this->dropTable('academy_academys');
        $this->dropTable('academy_academys_lang_fields');
    }

    private function createRelations()
    {
        $this->createIndex('ix_academy_academys_lang_fields_lang_id', '{{%academy_academys_lang_fields}}', 'lang_id');
        $this->addForeignKey(
            'fk_academy_academys_lang_fields_lang_id',
            '{{%academy_academys_lang_fields}}',
            'lang_id',
            '{{%core_languages}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex('ix_academy_academys_lang_fields_academy_id', '{{%academy_academys_lang_fields}}', 'academy_id');
        $this->addForeignKey(
            'fk_academy_academys_lang_fields_academy_id',
            '{{%academy_academys_lang_fields}}',
            'academy_id',
            '{{%academy_academys}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    private function dropRelations()
    {
        $this->dropForeignKey('fk_academy_academys_lang_fields_lang_id','{{%academy_academys_lang_fields}}');
        $this->dropIndex('ix_academy_academys_lang_fields_lang_id', '{{%academy_academys_lang_fields}}');
        $this->dropForeignKey('fk_academy_academys_lang_fields_academy_id','{{%academy_academys_lang_fields}}');
        $this->dropIndex('ix_academy_academys_lang_fields_academy_id', '{{%academy_academys_lang_fields}}');
    }
}
