<?php

use yii\db\Migration;

/**
 * Class m180611_132928_add_colum_company_seats_academy_registers_table
 */
class m180611_132928_add_colum_company_seats_academy_registers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('academy_registers', 'company', $this->string(100)->defaultValue(null));
        $this->addColumn('academy_registers', 'seats', $this->integer()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('academy_registers', 'company');
        $this->dropColumn('academy_registers', 'seats');
    }
}
