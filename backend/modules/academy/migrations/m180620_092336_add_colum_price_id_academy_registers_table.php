<?php

use yii\db\Migration;

/**
 * Class m180620_092336_add_colum_price_id_academy_registers_table
 */
class m180620_092336_add_colum_price_id_academy_registers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('academy_registers', 'price_id', $this->integer()->defaultValue(null));

        $this->createRelations();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropRelations();

        $this->dropColumn('academy_registers', 'price_id');
    }

    private function createRelations()
    {
        $this->createIndex('ix_academy_registers_price_id', '{{%academy_registers}}', 'price_id');
        $this->addForeignKey(
            'fk_academy_registers_price_id',
            '{{%academy_registers}}',
            'price_id',
            '{{%academy_prices}}',
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    private function dropRelations()
    {
        $this->dropForeignKey('fk_academy_registers_price_id','{{%academy_registers}}');
        $this->dropIndex('ix_academy_registers_price_id', '{{%academy_registers}}');
    }
}
