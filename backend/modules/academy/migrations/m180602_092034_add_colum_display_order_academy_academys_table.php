<?php

use yii\db\Migration;

/**
 * Class m180602_092034_add_colum_display_order_academy_academys_table
 */
class m180602_092034_add_colum_display_order_academy_academys_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('academy_academys', 'display_order', $this->integer()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('academy_academys', 'display_order');
    }

}
