<?php

use yii\db\Migration;

/**
 * Handles the creation of table `academy_prices`.
 */
class m180619_115507_create_academy_prices_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('academy_prices', [
            'id' => $this->primaryKey(),
            'academy_id' => $this->integer()->notNull(),
            'name' => $this->string(255)->defaultValue(null),
            'description' => $this->text()->defaultValue(null),
            'price' => $this->float()->defaultValue(0),
            'best_offer' => $this->smallInteger(1)->defaultValue(0),
            'active' => $this->smallInteger(1)->defaultValue(1),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);

        $this->createTable('academy_prices_lang_fields', [
            'id' => $this->primaryKey(),
            'academy_price_id' => $this->integer()->notNull(),
            'lang_id' => $this->integer()->notNull(),
            'name' => $this->string(255)->defaultValue(null),
            'description' => $this->text()->defaultValue(null),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);

        $this->createRelations();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropRelations();

        $this->dropTable('academy_prices');
        $this->dropTable('academy_prices_lang_fields');
    }

    private function createRelations()
    {
        $this->createIndex('ix_academy_prices_academy_id', '{{%academy_prices}}', 'academy_id');
        $this->addForeignKey(
            'fk_academy_prices_academy_id',
            '{{%academy_prices}}',
            'academy_id',
            '{{%academy_academys}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex('ix_academy_prices_lang_fields_lang_id', '{{%academy_prices_lang_fields}}', 'lang_id');
        $this->addForeignKey(
            'fk_academy_prices_lang_fields_lang_id',
            '{{%academy_prices_lang_fields}}',
            'lang_id',
            '{{%core_languages}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex('ix_academy_prices_lang_fields_academy_price_id', '{{%academy_prices_lang_fields}}', 'academy_price_id');
        $this->addForeignKey(
            'fk_academy_prices_lang_fields_academy_price_id',
            '{{%academy_prices_lang_fields}}',
            'academy_price_id',
            '{{%academy_prices}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    private function dropRelations()
    {
        $this->dropForeignKey('fk_academy_prices_academy_id','{{%academy_prices}}');
        $this->dropIndex('ix_academy_prices_academy_id', '{{%academy_prices}}');

        $this->dropForeignKey('fk_academy_prices_lang_fields_lang_id','{{%academy_prices_lang_fields}}');
        $this->dropIndex('ix_academy_prices_lang_fields_lang_id', '{{%academy_prices_lang_fields}}');

        $this->dropForeignKey('fk_academy_prices_lang_fields_academy_price_id','{{%academy_prices_lang_fields}}');
        $this->dropIndex('ix_academy_prices_lang_fields_academy_price_id', '{{%academy_prices_lang_fields}}');
    }
}
