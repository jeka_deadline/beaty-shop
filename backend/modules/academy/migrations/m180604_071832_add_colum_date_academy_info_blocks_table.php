<?php

use yii\db\Migration;

/**
 * Class m180604_071832_add_colum_date_academy_info_blocks_table
 */
class m180604_071832_add_colum_date_academy_info_blocks_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('academy_academys', 'date', $this->timestamp()->null()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('academy_academys', 'date');
    }
}
