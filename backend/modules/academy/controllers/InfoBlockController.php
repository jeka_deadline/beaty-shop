<?php

namespace backend\modules\academy\controllers;

use backend\modules\core\models\Language;
use common\models\academy\infoBlockModels\ImageSlider;
use Yii;
use backend\modules\academy\models\InfoBlock;
use backend\modules\academy\models\searchModels\InfoBlockSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * InfoBlockController implements the CRUD actions for InfoBlock model.
 */
class InfoBlockController extends Controller
{

    private $academy_id = null;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \developeruz\db_rbac\behaviors\AccessBehavior::className(),
                'login_url' => Yii::$app->user->loginUrl,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all InfoBlock models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new InfoBlockSearch();
        $searchModel->academy_id = $this->academy_id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'academy_id' => $this->academy_id
        ]);
    }

    /**
     * Displays a single InfoBlock model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            'academy_id' => $this->academy_id
        ]);
    }

    /**
     * Creates a new InfoBlock model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $modelInfoBlock = new InfoBlock();

        $languages = Language::find()->all();

        if ($modelInfoBlock->load(Yii::$app->request->post()) && $modelInfoBlock->save()) {
            return $this->redirect(['index', 'academy_id' => $this->academy_id]);
        }

        return $this->render('create', [
            'modelInfoBlock' => $modelInfoBlock,
            'academy_id' => $this->academy_id,
            'languages' => $languages
        ]);
    }

    /**
     * Updates an existing InfoBlock model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $modelInfoBlock = $this->findModel($id);

        $languages = Language::find()->all();

        if (
            $modelInfoBlock->load(Yii::$app->request->post())
            && $modelInfoBlock->getAttribute('type') == $modelInfoBlock->getOldAttribute('type')
            && $modelInfoBlock->save()
        ) {
            return $this->redirect(['index', 'academy_id' => $this->academy_id]);
        }

        return $this->render('update', [
            'modelInfoBlock' => $modelInfoBlock,
            'academy_id' => $this->academy_id,
            'languages' => $languages
        ]);
    }

    /**
     * Deletes an existing InfoBlock model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index', 'academy_id' => $this->academy_id]);
    }

    /**
     * Finds the InfoBlock model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return InfoBlock the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = InfoBlock::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function beforeAction($action)
    {
        $this->academy_id = Yii::$app->request->get('academy_id', null);
        return parent::beforeAction($action);
    }

    public function actionImageDelete() {
        if (Yii::$app->request->isAjax && ($key = Yii::$app->request->post('key', null))) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            ImageSlider::deleteImage($key);
            return [];
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
