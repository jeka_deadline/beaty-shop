<?php

namespace backend\modules\academy\controllers;

use backend\modules\core\models\Language;
use Yii;
use backend\modules\academy\models\Price;
use backend\modules\academy\models\searchModels\PriceSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PriceController implements the CRUD actions for Price model.
 */
class PriceController extends Controller
{

    private $academy_id = null;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \developeruz\db_rbac\behaviors\AccessBehavior::className(),
                'login_url' => Yii::$app->user->loginUrl,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        $this->academy_id = Yii::$app->request->get('academy_id', null);
        return parent::beforeAction($action);
    }

    /**
     * Lists all Price models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PriceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'academy_id' => $this->academy_id,
        ]);
    }

    /**
     * Displays a single Price model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            'academy_id' => $this->academy_id,
        ]);
    }

    /**
     * Creates a new Price model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $modelPrice = new Price();

        $languages = Language::find()->all();

        if ($modelPrice->load(Yii::$app->request->post()) && $modelPrice->save()) {
            return $this->redirect(['index', 'academy_id' => $this->academy_id]);
        }

        return $this->render('create', [
            'modelPrice' => $modelPrice,
            'academy_id' => $this->academy_id,
            'languages' => $languages
        ]);
    }

    /**
     * Updates an existing Price model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $modelPrice = $this->findModel($id);

        $languages = Language::find()->all();

        if ($modelPrice->load(Yii::$app->request->post()) && $modelPrice->save()) {
            return $this->redirect([
                '/academy/price/view',
                'id' => $modelPrice->id,
                'academy_id' => $this->academy_id
            ]);
        }

        return $this->render('update', [
            'modelPrice' => $modelPrice,
            'academy_id' => $this->academy_id,
            'languages' => $languages
        ]);
    }

    /**
     * Deletes an existing Price model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index', 'academy_id' => $this->academy_id]);
    }

    /**
     * Finds the Price model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Price the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Price::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
