<?php

namespace backend\modules\academy\controllers;

use backend\modules\core\models\Language;
use Yii;
use backend\modules\academy\models\Academy;
use backend\modules\academy\models\searchModels\AcademySearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AcademyController implements the CRUD actions for Academy model.
 */
class AcademyController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \developeruz\db_rbac\behaviors\AccessBehavior::className(),
                'login_url' => Yii::$app->user->loginUrl,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Academy models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AcademySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Academy model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Academy model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $modelAcademy = new Academy();

        $languages = Language::find()->all();

        if ($modelAcademy->load(Yii::$app->request->post()) && $modelAcademy->save()) {
            return $this->redirect(['view', 'id' => $modelAcademy->id]);
        }

        return $this->render('create', [
            'modelAcademy' => $modelAcademy,
            'languages' => $languages
        ]);
    }

    /**
     * Updates an existing Academy model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $modelAcademy = $this->findModel($id);

        $languages = Language::find()->all();

        if ($modelAcademy->load(Yii::$app->request->post()) && $modelAcademy->save()) {
            return $this->redirect(['view', 'id' => $modelAcademy->id]);
        }

        $modelAcademy->date = date('Y-m-d', strtotime($modelAcademy->date));

        return $this->render('update', [
            'modelAcademy' => $modelAcademy,
            'languages' => $languages
        ]);
    }

    /**
     * Deletes an existing Academy model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Academy model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Academy the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Academy::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
