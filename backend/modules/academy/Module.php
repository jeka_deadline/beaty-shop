<?php

namespace backend\modules\academy;

use Yii;

/**
 * Module definition for Products
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\academy\controllers';

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function init()
    {
        parent::init();
    }
}