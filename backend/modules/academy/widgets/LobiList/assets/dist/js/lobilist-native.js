function getDataLobiList(list) {
    var result = {};
    var title = list.$options.title;
    result.title = title;
    result.items = [];
    for (itemInsex in list.$items) {
        var item = list.$items[itemInsex];
        result.items.push({
            title: item.title,
            description: item.description,
            dueDate: item.dueDate,
        });
    }
    return result;
}

function getDefaultOptionsLobiList() {
    return [
        {
            id: 'day1',
            title: '',
            items: []
        }
    ]
}

function updateDataLobiList(selector) {
    var lists = [];
    $(selector).data('lobiList').$lists.forEach(function (list,index) {
        if (list.$elWrapper.parents(selector).length == 0) return;
        lists.push(getDataLobiList(list));
    });
    var json = JSON.stringify(lists);
    if (json == '[{"title":"","items":[]}]' || json == '[]') {
        return '';
    } else {
        return json;
    }
}