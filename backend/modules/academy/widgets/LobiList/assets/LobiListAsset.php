<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\modules\academy\widgets\LobiList\assets;

use yii\web\AssetBundle;

class LobiListAsset extends AssetBundle
{
    public $sourcePath = '@backend/modules/academy/widgets/LobiList/assets/dist';

    public $css = [
        'css/lobilist.min.css',
    ];

    public $js = [
        'js/jquery.ui.touch-punch-improved.js',
        'js/lobilist.min.js',
        'js/lobilist-native.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\jui\JuiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public $publishOptions = [
        'forceCopy' => (YII_DEBUG) ? true : false,
    ];
}