<?php

?>


<div class="lobilist" data-name="<?= $name ?>"></div>

<?php
$uniqid = uniqid();
$script = <<< JS
$(function(){
    var lobilist_parse = [];
    if ($('[name="{$name}"]').val()) {
        lobilist_parse = $.parseJSON($('[name="{$name}"]').val());
    } else {
        lobilist_parse = getDefaultOptionsLobiList();
    }
    $('[data-name="{$name}"]').lobiList({
        useCheckboxes: false,
        controls: ['edit', 'add', 'remove'],
        sortable: false,
        lists: lobilist_parse,
        afterItemAdd: function () {
            console.log('[data-name="{$name}"]');
            $('[name="{$name}"]').val(updateDataLobiList('[data-name="{$name}"]'));
        },
        afterItemUpdate: function () {
            $('[name="{$name}"]').val(updateDataLobiList('[data-name="{$name}"]'));
        },
        afterItemDelete: function (me, item) {
            delete me.\$items[item.id];
            $('[name="{$name}"]').val(updateDataLobiList('[data-name="{$name}"]'));
        },
        afterListAdd: function () {
            $('[name="{$name}"]').val(updateDataLobiList('[data-name="{$name}"]'));
        },
        afterListRemove: function (me) {
            delete me;
            $('[name="{$name}"]').val(updateDataLobiList('[data-name="{$name}"]'));
        },       
        titleChange: function(list,oldTitle,newTitle) {
            list.\$options.title = newTitle;
            $('[name="{$name}"]').val(updateDataLobiList('[data-name="{$name}"]'));
        }
    });
    
    $(document).on('click', '.lobilist-actions button, .lobilist-footer button', function(e){
        e.preventDefault();
    });
});
JS;

$this->registerJs($script, yii\web\View::POS_READY);
?>



