<?php

namespace backend\modules\academy\widgets\LobiList;

use backend\modules\academy\widgets\LobiList\assets\LobiListAsset;
use yii\base\Widget;

class LobiList extends Widget
{
    public $template = 'lobi-list';

    public $name;

    public function init() {
        parent::init();
        $this->registerAssets();
    }

    public function run()
    {
        return $this->render($this->template, [
            'id' => $this->id,
            'name' => $this->name
        ]);
    }

    public function registerAssets() {
        $view=$this->getView();
        LobiListAsset::register($view);
    }
}