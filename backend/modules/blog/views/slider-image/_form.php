<?php

use backend\modules\blog\assets\SliderImageAsset;
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\blog\models\SliderImage */
/* @var $form yii\widgets\ActiveForm */

SliderImageAsset::register($this);

?>

<div class="slider-image-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?php if (!$model->isNewRecord): ?>

        <?= $form->field($model, 'data')->dropDownList($model->data?array_combine($model->data, $model->data):[],[
            'multiple' => 'multiple',
            'class' => 'hide',
        ])->label(false) ?>


        <?= $form->field($model, 'files[]')->widget(FileInput::classname(), [
            'options' => [
                'multiple' => true,
                'accept' => 'image/*',
            ],
            'pluginOptions' => [
                'initialPreview' => $model->getUrlImagesInput(),
                'initialPreviewConfig' => $model->getImagesInputConfig(),
                'initialPreviewAsData'=>true,

                'browseClass' => 'btn btn-success',
                'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                'browseLabel' =>  'Select Photo',

                'overwriteInitial'=>false,
            ],
            'pluginEvents' => [
                'filedeleted' => 'deleteBlogSliderImages',
                'filesorted' => 'sortBlogSliderImages',
            ]
        ]) ?>

    <?php endif; ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
