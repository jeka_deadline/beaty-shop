<?php

use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\blog\models\Article */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
/**
 * Add plugin sliger to ckeditor.
 */

$sliders = json_encode($sliders);

$script = <<< JS
    var listSlider = $sliders;
    CKEDITOR.plugins.addExternal('slider', '/admin/js/ckeditor/plugins/slider/plugin.js');
    CKEDITOR.plugins.addExternal('youtube', '/admin/js/ckeditor/plugins/youtube/plugin.js');
JS;

$this->registerJs($script, yii\web\View::POS_END);
?>

<div class="article-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    $items = [
        [
            'label' => 'Article',
            'content' => $this->render(
                '_form_tab_article',
                compact(
                    'model',
                    'form')
            ),
        ],
    ];

    foreach ($languages as $language) {
        $items[] = [
            'label' => $language->name,
            'content' => $this->render('_form_tab_lang_fields', compact('model','product_id', 'language', 'form')),
        ];
    }

    ?>

    <?= Tabs::widget([
        'items' => $items,
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
