<?php

use mihaildev\ckeditor\CKEditor;
use yii\helpers\Html;
use mihaildev\elfinder\ElFinder;

?>

<?= $form->field($model, 'name_' . $language->code)->textInput(['maxlength' => true])->label($model->getAttributeLabel('name')) ?>

<?= $form->field($model, 'meta_title_' . $language->code)->textInput(['maxlength' => true])->label($model->getAttributeLabel('meta_title')) ?>

<?= $form->field($model, 'meta_description_' . $language->code)->textarea(['rows' => 6])->label($model->getAttributeLabel('meta_description')) ?>

<?= $form->field($model, 'meta_keywords_' . $language->code)->textInput(['maxlength' => true])->label($model->getAttributeLabel('meta_keywords')) ?>

<?= $form->field($model, 'content_' . $language->code)->widget(CKEditor::className(), [
    'editorOptions' => ElFinder::ckeditorOptions(
        [
            'elfinder',
            'path' => 'blog/manager',
            'filter' => [
                'text/plain',
                'image/png',
                'image/jpeg',
                'image/jpeg',
                'image/jpeg',
                'image/gif',
                'application/pdf',
                'application/msword',
                'application/rtf',
                'application/vnd.ms-excel',
                'application/msword',
                'application/vnd.ms-excel',
            ]
        ],
        [
            'entities' => false,
            'extraPlugins' => 'slider',
        ]),
])->label($model->getAttributeLabel('content')) ?>
