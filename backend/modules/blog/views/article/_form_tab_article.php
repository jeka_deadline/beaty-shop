<?php

use backend\modules\blog\assets\ArticleAsset;
use bupy7\cropbox\CropboxWidget;
use kartik\date\DatePicker;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;

?>

<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'date')->widget(DatePicker::className(), [
    'type' => DatePicker::TYPE_COMPONENT_APPEND,
    'pluginOptions' => [
        'autoclose'=>true,
        'startDate' => '1992-01-01',
        'format' => 'yyyy-mm-dd'
    ]
]) ?>

<?= $form->field($model, 'meta_title')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'meta_description')->textarea(['rows' => 6]) ?>

<?= $form->field($model, 'meta_keywords')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'content')->widget(CKEditor::className(), [
    'editorOptions' => ElFinder::ckeditorOptions(
        [
            'elfinder',
            'path' => 'blog/manager',
            'filter' => [
                'text/plain',
                'image/png',
                'image/jpeg',
                'image/jpeg',
                'image/jpeg',
                'image/gif',
                'application/pdf',
                'application/msword',
                'application/rtf',
                'application/vnd.ms-excel',
                'application/msword',
                'application/vnd.ms-excel',
            ]
        ],
        [
        'entities' => false,
        'extraPlugins' => 'slider,youtube',
    ]),
]) ?>

<?php if (!$model->isNewRecord): ?>
    <?= $form->field($model, 'file')->widget(FileInput::classname(), [
        'options' => [
            'accept' => 'image/*',
        ],
        'pluginOptions' => [
            'initialPreview' => $model->getUrlImageInput(),
            'initialPreviewConfig' => $model->getImagesInputConfig(),
            'initialPreviewAsData'=>true,

            'browseClass' => 'btn btn-success',
            'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
            'browseLabel' =>  'Select Photo',
        ],
    ]) ?>
    <?= $form->field($model, 'small_file')->widget(CropboxWidget::className(), [
        'pluginOptions' => [
            'variants' => [
                [
                    'width' => 370,
                    'height' => 490
                ]
            ]
        ],
        'croppedDataAttribute' => 'crop_info',
        'croppedImagesUrl' => [
            $model->urlSmallImage
        ],
    ]) ?>
<?php endif; ?>

<?= $form->field($model, 'active')->checkbox() ?>
