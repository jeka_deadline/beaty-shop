<?php

namespace backend\modules\blog\controllers;

use backend\modules\blog\models\SliderImage;
use backend\modules\core\models\Language;
use Yii;
use backend\modules\blog\models\Article;
use backend\modules\blog\models\searchModels\ArticleSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \developeruz\db_rbac\behaviors\AccessBehavior::className(),
                'login_url' => Yii::$app->user->loginUrl,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Article models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Article model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Article model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Article();

        $languages = Language::find()->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        if ($model->date) {
            $model->date = date('Y-m-d', strtotime($model->date));
        } else {
            $model->date = date('Y-m-d');
        }

        return $this->render('create', [
            'model' => $model,
            'languages' => $languages,
            'sliders' => $this->getListSlider()
        ]);
    }

    /**
     * Updates an existing Article model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $languages = Language::find()->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        if ($model->date) {
            $model->date = date('Y-m-d', strtotime($model->date));
        } else {
            $model->date = date('Y-m-d');
        }

        return $this->render('update', [
            'model' => $model,
            'languages' => $languages,
            'sliders' => $this->getListSlider()
        ]);
    }

    /**
     * Deletes an existing Article model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Article the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Article::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function getListSlider()
    {
        $list = [];
        if (!($modelSliders = SliderImage::find()->all())) return $list;

        foreach ($modelSliders as $slider) {
            $list[] = [
                '[blog_slider name="'.$slider->name.'" id='.$slider->id.']',
                $slider->name,
                $slider->name,
            ];
        }
        return $list;
    }

    public function actionImageDelete() {
        if (
            Yii::$app->request->isAjax
            && ($key = Yii::$app->request->post('key', null))
        ) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            Article::deleteImage($key);
            return [];
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
