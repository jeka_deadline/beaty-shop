<?php

namespace backend\modules\blog\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class SliderImageAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [

    ];
    public $js = [
        'js/blog-slider-image.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public $publishOptions = [
        'forceCopy' => YII_DEBUG?true:false,
    ];
}
