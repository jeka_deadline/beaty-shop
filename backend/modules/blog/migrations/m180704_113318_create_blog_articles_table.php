<?php

use yii\db\Migration;

/**
 * Handles the creation of table `blog_articles`.
 */
class m180704_113318_create_blog_articles_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('blog_articles', [
            'id' => $this->primaryKey(),
            'slug' => $this->string(255)->notNull()->unique(),
            'name' => $this->string(255)->notNull(),
            'meta_title' => $this->string(255)->null(),
            'meta_description' => $this->text()->null(),
            'meta_keywords' => $this->string(255)->null(),
            'content' => $this->text()->defaultValue(null),
            'image' => $this->string(255)->defaultValue(null),
            'small_image' => $this->string(255)->defaultValue(null),
            'date' => $this->timestamp()->null()->defaultValue(null),
            'active' => $this->smallInteger(1)->defaultValue(1),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);

        $this->createTable('blog_articles_lang_fields', [
            'id' => $this->primaryKey(),
            'article_id' => $this->integer()->notNull(),
            'lang_id' => $this->integer()->notNull(),
            'name' => $this->string(255)->notNull(),
            'content' => $this->text()->defaultValue(null),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);

        $this->createRelations();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropRelations();

        $this->dropTable('blog_articles_lang_fields');
        $this->dropTable('blog_articles');
    }

    private function createRelations()
    {
        $this->createIndex('ix_blog_articles_lang_fields_lang_id', '{{%blog_articles_lang_fields}}', 'lang_id');
        $this->addForeignKey(
            'fk_blog_articles_lang_fields_lang_id',
            '{{%blog_articles_lang_fields}}',
            'lang_id',
            '{{%core_languages}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex('ix_blog_articles_lang_fields_article_id', '{{%blog_articles_lang_fields}}', 'article_id');
        $this->addForeignKey(
            'fk_blog_articles_lang_fields_article_id',
            '{{%blog_articles_lang_fields}}',
            'article_id',
            '{{%blog_articles}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    private function dropRelations()
    {
        $this->dropForeignKey('fk_blog_articles_lang_fields_lang_id','{{%blog_articles_lang_fields}}');
        $this->dropIndex('ix_blog_articles_lang_fields_lang_id', '{{%blog_articles_lang_fields}}');
        $this->dropForeignKey('fk_blog_articles_lang_fields_article_id','{{%blog_articles_lang_fields}}');
        $this->dropIndex('ix_blog_articles_lang_fields_article_id', '{{%blog_articles_lang_fields}}');
    }
}
