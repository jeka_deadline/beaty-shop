<?php

use yii\db\Migration;

/**
 * Class m180709_123807_add_meta_to_blog_articles_table
 */
class m180709_123807_add_meta_to_blog_articles_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('blog_articles_lang_fields', 'meta_title', $this->string(255)->null());
        $this->addColumn('blog_articles_lang_fields', 'meta_description', $this->text()->null());
        $this->addColumn('blog_articles_lang_fields', 'meta_keywords', $this->string(255)->null());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('blog_articles_lang_fields', 'meta_title');
        $this->dropColumn('blog_articles_lang_fields', 'meta_description');
        $this->dropColumn('blog_articles_lang_fields', 'meta_keywords');
    }
}
