<?php

use yii\db\Migration;

/**
 * Handles the creation of table `blog_slider_images`.
 */
class m180705_141643_create_blog_slider_images_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('blog_slider_images', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'data' => $this->text()->defaultValue(null),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('blog_slider_images');
    }
}
