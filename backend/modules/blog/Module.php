<?php

namespace backend\modules\blog;

/**
 * Blog class module extends yii base Module
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\blog\controllers';

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function init()
    {
        return parent::init();
    }
}
