<?php

namespace backend\modules\blog\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;


class SliderImage extends \common\models\blog\SliderImage
{
    public $files;

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['files'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg', 'maxFiles' => 0],
            ]
        );
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;

        $this->files = UploadedFile::getInstances($this, 'files');
        $this->uploadImages();

        $this->data = serialize($this->data);

        return true;
    }

    public function uploadImages()
    {
        if ($this->validate()) {
            $basePath = Yii::getAlias('@frontend/web');
            $allPath = $basePath . '/' . self::$path . '/' . $this->id . '/' ;
            if (!is_dir($allPath)) {
                mkdir($allPath, 0777, true);
            }

            $data = is_array($this->data)?$this->data:[];
            foreach ($this->files as $key => $file) {
                $filename = md5(uniqid($file->baseName)).'.'.$file->extension;
                if ($file->saveAs($allPath . $filename)) {
                    chmod($allPath . $filename, 0777);
                    $data[] = $filename;
                }
            }
            $this->data = $data;
            return true;
        } else {
            return false;
        }
    }

    public function getUrlImagesInput() {
        if (!$this->data) return [];

        $initialPreview = [];
        foreach ($this->data as $image) {
            $initialPreview[] = '/'.self::$path . '/' . $this->id . '/'.$image;
        }
        return $initialPreview;
    }

    public function getImagesInputConfig() {
        if (!$this->data) return [];

        $initialPreviewConfig = [];
        foreach ($this->data as $image) {
            $initialPreviewConfig[] = [
                'url' => 'image-delete',
                'key' => $this->id.'/'.$image,
            ];
        }
        return $initialPreviewConfig;
    }

    public static function deleteImage($filename) {
        $filePath = Yii::getAlias('@frontend/web') . '/' . self::$path . '/' . $filename;
        if (file_exists($filePath)) {
            $pathinfo = pathinfo($filename);
            $id = (int) $pathinfo['dirname'];
            $image = $pathinfo['basename'];
            $model = SliderImage::findOne($id);
            if ($model && ($key=array_search($image, $model->data)) && $key!==false) {
                $data = $model->data;
                unset($data[$key]);
                $model->data = $data;
                $model->save();
            }
            unlink($filePath);
            return true;
        }
        return false;
    }

    public function afterDelete()
    {
        $path = Yii::getAlias('@frontend/web') . '/'.self::$path . $this->id . '/';

        array_map(function ($pathFile) {
            if (file_exists($pathFile)) {
                unlink($pathFile);
            }
            return $pathFile;
        }, glob($path.'*'));

        if (file_exists($path)) rmdir($path);

        parent::afterDelete();
    }


}
