<?php

use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\product\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form">

    <?php $form = ActiveForm::begin(); ?>
        <?php
            $items = [
                [
                    'label' => 'Set',
                    'content' => $this->render('_form_tab_product', compact(
                        'form',
                        'model',
                        'listCategories',
                        'modelVariation',
                        'languages',
                        'modelInventorie',
                        'modelPrice',
                        'modelPromotion',
                        'formUploadImages'
                    )),
                ],
            ];

            foreach ($languages as $language) {
                $items[] = [
                    'label' => $language->name,
                    'content' => $this->render('_form_tab_lang_fields', compact('model','language', 'form', 'modelVariation')),
                ];
            }

            $items[] = [
                'label' => 'Items',
                'content' => $this->render(
                    '_form_items',
                    compact(
                        'model',
                        'form')
                ),
            ];


            $items[] = [
                'label' => 'Recommended products',
                'content' => $this->render(
                    '_form_recommended_products',
                    compact(
                        'model',
                        'product_id',
                        'modelInventorie',
                        'modelPrice',
                        'modelPromotion',
                        'formUploadImages',
                        'form')
                ),
            ];
        ?>

        <?= Tabs::widget([
            'items' => $items,
        ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
