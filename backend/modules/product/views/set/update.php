<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\modules\product\models\Product */

$this->title = 'Update Set';
$this->params['breadcrumbs'][] = ['label' => 'Sets', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-update">
    <?= $this->render('_form', [
        'model' => $model,
        'listCategories' => $listCategories,
        'modelVariation' => $modelVariation,
        'languages' => $languages,
        'modelInventorie' => $modelInventorie,
        'modelPrice' => $modelPrice,
        'modelPromotion' => $modelPromotion,
        'formUploadImages' => $formUploadImages,
    ]) ?>

</div>
