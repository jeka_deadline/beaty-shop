<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\product\models\Product */

$this->title = 'Create Set';
$this->params['breadcrumbs'][] = ['label' => 'Sets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-create">

    <?= $this->render('_form', [
        'model' => $model,
        'listCategories' => $listCategories,
        'modelVariation' => $modelVariation,
        'languages' => $languages,
        'modelInventorie' => $modelInventorie,
        'modelPrice' => $modelPrice,
        'modelPromotion' => $modelPromotion,
    ]) ?>

</div>
