<?php

use backend\modules\product\widgets\ProductSetItemsInputWidget;
use yii\helpers\Url;

?>

<?= $form->field($model, 'productSetVariations')->widget(ProductSetItemsInputWidget::className(),[
    'urlSearch' => Url::to(['/product/set/search-set-variations']),
])->label(false); ?>
