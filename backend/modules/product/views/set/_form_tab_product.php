<?php

/** @var \yii\web\View $this */

use backend\modules\product\assets\ProductVariationAsset;
use kartik\file\FileInput;
use yii\helpers\Url;
use mihaildev\ckeditor\CKEditor;

/** @var \backend\modules\product\models\ProductCategory $model */
/** @var \yii\widgets\ActiveForm $form */
/** @var array $listCategories */

ProductVariationAsset::register($this);

?>

<?= $form->field($model, 'productCategories')->dropDownList($listCategories, ['prompt' => '', 'multiple' => true]); ?>

<?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'meta_title')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'meta_description')->textarea(['rows' => 6]) ?>

<?= $form->field($model, 'meta_keywords')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'active')->checkbox(); ?>

<?= $form->field($modelVariation, 'name')->textInput(['maxlength' => true]) ?>

<?= $form->field($modelVariation, 'vendor_code')->textInput(['maxlength' => true]) ?>

<?= $form->field($modelVariation, 'description')->widget(CKEditor::className()) ?>

<?= $form->field($modelVariation, 'short_description')->textarea(['rows' => 6, 'maxlength' => true]) ?>

<?= $form->field($modelPrice, 'price')->textInput(['id' => 'input-price']) ?>

<?= $form->field($modelPromotion, 'price')->textInput(['id' => 'input-promotion']) ?>

<?= $form->field($modelInventorie, 'count')->textInput() ?>

<?php if (!$modelVariation->isNewRecord): ?>
    <?= $form->field($formUploadImages, 'imageFiles[]')->widget(FileInput::classname(), [
        'options' => [
            'multiple' => true,
            'accept' => 'image/*',
        ],
        'pluginOptions' => [
            'required' => false,

            'allowedFileExtensions' => ["jpg", "png", "jpeg"],
            'showUpload' => false,
            'uploadUrl' => Url::to(['image-upload']),
            'uploadExtraData' => [
                'product_variation_id' => $modelVariation->id
            ],

            'initialPreview' => $formUploadImages->getInitialPreview(),
            'initialPreviewConfig' => $formUploadImages->getInitialPreviewConfig(),
            'initialPreviewAsData'=>true,
            'overwriteInitial'=>false,

            'showRemove' => false,
            'browseClass' => 'btn btn-success',
            'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
            'browseLabel' =>  'Select Photo',
        ],
        'pluginEvents' => [
            "filesorted" => "productVariationImagesSort",
        ]
    ]) ?>
<?php endif; ?>
