<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var \yii\web\View $this */
/** @var \backend\modules\product\models\forms\CouponCreateForm $model */
/** @var \yii\widgets\ActiveForm $form */
?>

<div class="coupon-form">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'count')->textInput(); ?>

        <?= $form->field($model, 'type')->dropDownList($model->getListCouponTypes()); ?>

        <?= $form->field($model, 'value')->textInput(); ?>

        <?= $form->field($model, 'active')->checkbox(); ?>

        <div class="form-group">
             <?= Html::submitButton('Create', ['class' => 'btn btn-success']); ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>
