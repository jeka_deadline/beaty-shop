<?php

use backend\modules\product\models\ProductFilter;
use kartik\color\ColorInput;

?>
<?php foreach ($modelVariation->allProperties as $property): ?>
<?php if ($property->filter->type==ProductFilter::TYPE_COLORLIST): ?>
    <?= $form->field($modelVariation, 'propertieFields['.$property->filter_id.']')->widget(ColorInput::classname(),[
        'options' => [
            'value' => $property->value,
            'readonly' => true
        ]
    ])->label($property->label); ?>
<?php else: ?>
    <?= $form->field($modelVariation, 'propertieFields['.$property->filter_id.']')->textInput(['value' => $property->value])->label($property->label); ?>
<?php endif; ?>
<?php endforeach; ?>
