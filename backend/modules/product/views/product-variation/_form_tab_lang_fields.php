<?php

use yii\helpers\Html;

?>

<?= $form->field($modelVariation, 'name_' . $language->code)->textInput(['maxlength' => true])->label($modelVariation->getAttributeLabel('name')) ?>

<?= $form->field($modelVariation, 'description_' . $language->code)->textarea(['rows' => 6])->label($modelVariation->getAttributeLabel('description')) ?>

<?= $form->field($modelVariation, 'short_description_' . $language->code)->textarea(['rows' => 6, 'maxlength' => true])->label($modelVariation->getAttributeLabel('short_description')) ?>
