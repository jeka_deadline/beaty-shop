<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\product\models\ProductVariationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Update Product';
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['/product/product/index']];
$this->params['breadcrumbs'][] = ['label' => $product_id, 'url' => ['/product/product/view', 'id' => $product_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-propertie-index table-responsive">
    <ul class="nav nav-tabs">
        <li role="presentation"><a href="<?= Url::to(['/product/product/update', 'id' => $product_id]) ?>">Product</a></li>
        <li role="presentation" class="active"><a href="<?= Url::to(['/product/product-variation/index', 'product_id' => $product_id]) ?>">Variations</a></li>
    </ul>

    <br>

    <p>
        <?= Html::a('Create Variation', ['create', 'product_id' => $product_id], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'vendor_code',
            [
                'attribute' => 'count',
                'value' => function ($model) {
                    return $model->inventorie?$model->inventorie->count:'0';
                },
            ],
            'price.price',
            'active',
            //'created_at',
            //'updated_at',

            [
                'class' => 'yii\grid\ActionColumn',
                'urlCreator' => function ($action, $model, $key, $index) use ($product_id) {
                    $params = is_array($key) ? $key : ['id' => (string) $key];
                    $params['product_id'] = $product_id;
                    $params[0] =  $action;
                    return Url::toRoute($params);                }
            ],
        ],
    ]); ?>
</div>
