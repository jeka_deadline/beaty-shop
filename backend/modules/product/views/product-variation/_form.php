<?php

use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\product\models\ProductVariation */
/* @var $form yii\widgets\ActiveForm */
?>

<ul class="nav nav-tabs">
    <li role="presentation"><a href="<?= Url::to(['/product/product/update', 'id' => $product_id]) ?>">Product</a></li>
    <li role="presentation" class="active"><a href="<?= Url::to(['/product/product-variation/index', 'product_id' => $product_id]) ?>">Variations</a></li>
</ul>
<br>

<div class="product-propertie-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-variation'
    ]); ?>

        <?php
            $items = [
                [
                    'label' => 'Variation',
                    'content' => $this->render(
                            '_form_tab_variation',
                            compact(
                                    'modelVariation',
                                    'product_id',
                                    'modelInventorie',
                                    'modelPrice',
                                    'modelPromotion',
                                    'formUploadImages',
                                    'form')
                    ),
                ],
            ];

            foreach ($languages as $language) {
                $items[] = [
                    'label' => $language->name,
                    'content' => $this->render('_form_tab_lang_fields', compact('modelVariation','product_id', 'language', 'form')),
                ];
            }

            if ($modelVariation->allProperties) {
                $items[] = [
                    'label' => 'Properties',
                    'content' => $this->render(
                        '_form_tab_properties',
                        compact(
                            'modelVariation',
                            'product_id',
                            'form')
                    ),
                ];
            }
        ?>

        <?= Tabs::widget([
            'items' => $items,
        ]); ?>

        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>
