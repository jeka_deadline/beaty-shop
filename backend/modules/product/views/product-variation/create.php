<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\product\models\ProductVariation */

$this->title = 'Create Product Propertie';
$this->params['breadcrumbs'][] = ['label' => 'Product Properties', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-propertie-create">

    <?= $this->render('_form', compact(
        'modelVariation',
        'product_id',
        'languages',
        'modelInventorie',
        'modelPrice',
        'modelPromotion',
        'packs'
    )); ?>

</div>
