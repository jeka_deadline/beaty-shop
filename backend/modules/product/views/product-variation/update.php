<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\product\models\ProductVariation */

$this->title = 'Update Product Propertie: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Product Properties', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $product_id, 'url' => ['/product/product/view', 'id' => $product_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-propertie-update">

    <?= $this->render('_form', compact(
        'modelVariation',
        'product_id',
        'languages',
        'modelInventorie',
        'modelPrice',
        'modelPromotion',
        'formUploadImages',
        'packs'
    )); ?>


</div>
