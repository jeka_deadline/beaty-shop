<?php

use yii\helpers\Html;


/** @var \yii\web\View $this */
/** @var \backend\modules\product\models\ProductCategory $model */
/** @var array $listCategories */

$this->title = 'Create Product Category';
$this->params['breadcrumbs'][] = ['label' => 'Product Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-category-create">

    <?= $this->render('_form', compact('model', 'listCategories', 'languages')); ?>

</div>
