<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/** @var \yii\web\View $this */
/** @var \backend\modules\product\models\searchModels\ProductCategorySearch $searchModel */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @vat array $listCategories */

$this->title = 'Product Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-category-index table-responsive">
    <p>

        <?= Html::a('Create Product Category', ['create'], ['class' => 'btn btn-success']); ?>

    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'parent_id',
                'filter' => $listCategories,
                'value' => function($model) { return ($model->parent) ? $model->parent->name : ''; }
            ],
            'name',
            'slug',
            //'display_order',
            //'active',
            //'meta_title',
            //'meta_description:ntext',
            //'meta_keywords',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
