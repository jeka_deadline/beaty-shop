<?php

use backend\modules\product\assets\ProductVariationAsset;
use backend\modules\product\models\ProductFilter;
use frontend\widgets\bootstrap4\Html;
use kartik\sortinput\SortableInput;
use yii\helpers\ArrayHelper;

ProductVariationAsset::register($this);

$nameInput = Html::getInputName($model, 'productFilters');

?>
<div class="filters-block">
<?= $form->field($model, 'productFilters')
    ->dropDownList(ArrayHelper::map(ProductFilter::find()->all(),'id', 'name'), ['prompt' => '', 'multiple' => true, 'class' => 'hidden'])
    ->label(false); ?>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group field-productcategory-filters-two">
                <label class="control-label" for="productcategory-filters-two">Add Filters</label>
                <?= SortableInput::widget([
                    'name'=> 'v1-'.$nameInput,
                    'items' => $model->inputFilterItems,
                    'hideInput' => true,
                    'sortableOptions' => [
                        'connected'=>true,
                        'pluginEvents' => [
                            'sortupdate' => 'productFiltersSortUpdate',
                        ]
                    ],
                    'options' => [
                        'class'=>'form-control',
                        'readonly'=>true,
                        'data-name-input' => $nameInput
                    ]
                ]) ?>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group field-productcategory-filters-two">
                <label class="control-label" for="productcategory-filters-two">All Filters</label>
                <?= SortableInput::widget([
                    'name'=> 'v2-'.$nameInput,
                    'items' => $model->inputExcludeFilterItems,
                    'hideInput' => true,
                    'sortableOptions' => [
                        'connected'=>true,
                        'pluginEvents' => [
                            'sortupdate' => 'productFiltersSortUpdate',
                        ]
                    ],
                    'options' => [
                        'class'=>'form-control',
                        'readonly'=>true,
                        'data-name-input' => $nameInput
                    ]
                ]) ?>
            </div>
        </div>
    </div>
</div>
