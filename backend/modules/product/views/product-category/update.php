<?php

use yii\helpers\Html;

/** @var \yii\web\View $this */
/** @var \backend\modules\product\models\ProductCategory $model */
/** @var array $listCategories */

$this->title = 'Update Product Category: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Product Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-category-update">

    <?= $this->render('_form', compact('model', 'listCategories', 'languages')) ?>

</div>
