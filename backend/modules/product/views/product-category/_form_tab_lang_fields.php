<?php

use yii\helpers\Html;

?>

<?= $form->field($model, 'name_' . $language->code)->label($model->getAttributeLabel('name')); ?>

<?= $form->field($model, 'description_' . $language->code)->textarea()->label($model->getAttributeLabel('description')); ?>

<?= $form->field($model, 'meta_title_' . $language->code)->label($model->getAttributeLabel('meta_title')); ?>

<?= $form->field($model, 'meta_keywords_' . $language->code)->textarea()->label($model->getAttributeLabel('meta_keywords')); ?>

<?= $form->field($model, 'meta_description_' . $language->code)->textarea()->label($model->getAttributeLabel('meta_description')); ?>