<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Tabs;

/** @var \yii\web\View $this */
/** @var \backend\modules\product\models\ProductCategory $model */
/** @var \yii\widgets\ActiveForm $form */
/** @var array $listCategories */
?>

<div class="product-category-form">

    <?php $form = ActiveForm::begin(); ?>

        <?php

            $items = [
                [
                    'label' => 'Category',
                    'content' => $this->render('_form_tab_category', compact('model', 'form', 'listCategories')),
                ],
            ];

            foreach ($languages as $language) {
                $items[] = [
                    'label' => $language->name,
                    'content' => $this->render('_form_tab_lang_fields', compact('model', 'language', 'form')),
                ];
            }

            $items[] = [
                'label' => 'Filter',
                'content' => $this->render('_form_tab_filter', compact('model', 'form')),
            ];

        ?>

        <?= Tabs::widget([
              'items' => $items,
        ]); ?>

        <div class="form-group">

            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>

        </div>

    <?php ActiveForm::end(); ?>

</div>
