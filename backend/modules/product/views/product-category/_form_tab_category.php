<?php
/** @var \yii\web\View $this */
/** @var \backend\modules\product\models\ProductCategory $model */
/** @var \yii\widgets\ActiveForm $form */
/** @var array $listCategories */
?>

<?= $form->field($model, 'parent_id')->dropDownList($listCategories, ['prompt' => '']); ?>

<?= $form->field($model, 'name')->textInput(['maxlength' => true]); ?>

<?= $form->field($model, 'slug')->textInput(['maxlength' => true]); ?>

<?= $form->field($model, 'description')->textarea(['rows' => 6]); ?>

<?= $form->field($model, 'meta_title')->textInput(['maxlength' => true]); ?>

<?= $form->field($model, 'meta_description')->textarea(['rows' => 6]); ?>

<?= $form->field($model, 'meta_keywords')->textInput(['maxlength' => true]); ?>

<?= $form->field($model, 'display_order')->textInput(); ?>

<?= $form->field($model, 'display_order_hmenu')->textInput(); ?>

<?= $form->field($model, 'active')->checkbox(); ?>

<?= $form->field($model, 'show_search')->checkbox(); ?>