<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\modules\product\models\Product */

$this->title = 'Update Product';
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-update">
    <ul class="nav nav-tabs">
        <li role="presentation" class="active"><a href="<?= Url::to(['/product/product/update', 'id' => $model->id]) ?>">Product</a></li>
        <li role="presentation"><a href="<?= Url::to(['/product/product-variation/index', 'product_id' => $model->id]) ?>">Variations</a></li>
    </ul>
    <br>

    <?= $this->render('_form', compact('model', 'listCategories', 'languages')) ?>

</div>
