<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\StringHelper;
use yii\widgets\ActiveForm;

/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var \backend\modules\product\models\searchModels\ProductSearch $searchModel */
/** @var \yii\web\View $this */

?>
<br>
<p>
    <?= Html::a('Create Product', ['create'], ['class' => 'btn btn-success']) ?>
</p>


<?php $form = ActiveForm::begin([
    'action' => ['move-to-category'],
    'options' => [
        'id' => 'move-to-category',
    ],
]); ?>

    <?= $form->field($duplicateToCategoryForm, 'categoryId')->dropDownList($duplicateToCategoryForm->getListCategories()); ?>

    <?= Html::submitButton('Move to category', ['class' => 'btn btn-primary']); ?>

<?php ActiveForm::end(); ?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'layout' => '{summary}{pager}{items}{pager}',
    'options' => [
        'id' => 'grid-with-select',
    ],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'class' => 'yii\grid\CheckboxColumn',
        ],
        /*[
            'attribute' => 'id',
            'format' => 'raw',
            'label' => '',
            'filter' => false,
            'value' => function($model) use ($duplicateToCategoryForm) {
                return Html::activeCheckbox($duplicateToCategoryForm, 'ids[' . $model->id . ']', ['label' => false]);
            }
        ],*/
        'slug',
        [
            'attribute' => 'meta_title',
            'value' => function($model){ return StringHelper::truncate($model->meta_title, 50, '...', 'UTF-8'); }
        ],
        [
            'class' => 'yii\grid\DataColumn',
            'format' => 'raw',
            'header' => 'Categories',
            'value' => function($model) {
                return $model->categoriesRowList;
            }
        ],
        [
            'class' => 'yii\grid\DataColumn',
            'format' => 'raw',
            'value' => function($model) {
                return Html::a('Show variations', ['/product/product-variation/index', 'product_id' => $model->id]);
            }
        ],
        ['class' => 'yii\grid\ActionColumn'],
    ],
]); ?>