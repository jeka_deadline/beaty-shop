<?php

use backend\modules\product\widgets\ProductRecommendetsInputWidget;
use yii\helpers\Url;

?>

<?= $form->field($model, 'recommendets')->widget(ProductRecommendetsInputWidget::className(),[
    'urlSearch' => Url::to(['/product/product/search-recommendet-products']),
])->label(false); ?>
