<?php

use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\product\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form">

    <?php $form = ActiveForm::begin(); ?>
        <?php
            $items = [
                [
                    'label' => 'Product',
                    'content' => $this->render('_form_tab_product', compact('model',  'listCategories', 'form')),
                ],
            ];

            foreach ($languages as $language) {
                $items[] = [
                    'label' => $language->name,
                    'content' => $this->render('_form_tab_lang_fields', compact('model','language', 'form')),
                ];
            }

            $items[] = [
                'label' => 'Recommended products',
                'content' => $this->render(
                    '_form_recommended_products',
                    compact(
                        'model',
                        'product_id',
                        'modelInventorie',
                        'modelPrice',
                        'modelPromotion',
                        'formUploadImages',
                        'form')
                ),
            ];

            if (!$model->isAnPack() && !$model->isNewRecord) {

                $items[] = [
                    'label' => 'Packs',
                    'content' => $this->render(
                        '_form_product_packs',
                        compact('model', 'form')
                    ),
                ];

            }
        ?>

        <?= Tabs::widget([
            'items' => $items,
        ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
