<?php

/** @var \yii\web\View $this */
/** @var \backend\modules\product\models\ProductCategory $model */
/** @var \yii\widgets\ActiveForm $form */
/** @var array $listCategories */

?>

<?= $form->field($model, 'productCategories')->dropDownList($listCategories, ['prompt' => '', 'multiple' => true]); ?>

<?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'meta_title')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'meta_description')->textarea(['rows' => 6]) ?>

<?= $form->field($model, 'meta_keywords')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'active')->checkbox(); ?>