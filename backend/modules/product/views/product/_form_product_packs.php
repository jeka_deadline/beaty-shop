<?php

use backend\modules\product\widgets\ProductRecommendetsInputWidget;
use yii\helpers\Url;

?>

<?= $form->field($model, 'packs')->widget(ProductRecommendetsInputWidget::className(),[
    'urlSearch' => Url::to(['/product/product/search-recommendet-products']),
    'template' => 'product-packs'
])->label(false); ?>
