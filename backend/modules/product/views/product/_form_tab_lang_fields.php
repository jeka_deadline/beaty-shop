<?php

use yii\helpers\Html;

?>

<?= $form->field($model, 'meta_title_' . $language->code)->textInput(['maxlength' => true])->label($model->getAttributeLabel('meta_title')) ?>

<?= $form->field($model, 'meta_description_' . $language->code)->textarea(['rows' => 6])->label($model->getAttributeLabel('meta_description')) ?>

<?= $form->field($model, 'meta_keywords_' . $language->code)->textInput(['maxlength' => true])->label($model->getAttributeLabel('meta_keywords')) ?>
