<?php

use yii\helpers\Html;
use yii\bootstrap\Tabs;

/** @var \yii\web\View $this */
/** @var \backend\modules\product\models\ProductSearch $searchModel */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var \backend\modules\product\models\forms\ImportInventoryForm $importInventoryForm */
/** @var array $exportInventoryFormats */
/** @var \backend\modules\product\models\forms\ExportInventoryForm $exportInventoryFrom */
/** @var \backend\modules\product\models\forms\ExportSalePriceForm $exportSalePriceForm */
/** @var \backend\modules\product\models\forms\ImportSalePriceForm $importSalePriceForm */

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index table-responsive">

    <?= Tabs::widget([
        'items' => [
            [
                'label' => 'Products',
                'content' => $this->render('products-tab', compact('dataProvider', 'searchModel', 'duplicateToCategoryForm')),
            ],
            [
                'label' => 'Export',
                'content' => $this->render('export-tab', compact('exportInventoryFrom', 'exportInventoryFormats', 'exportSalePriceForm', 'exportProductsForm')),
            ],
            [
                'label' => 'Import',
                'content' => $this->render('import-tab', compact('importInventoryForm', 'importSalePriceForm', 'importProductsForm')),
            ],
        ],
    ]); ?>
</div>
