<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

/** @var \yii\web\View $this */
/** @var \yii\widgets\ActiveForm $form */
/** @var array $exportInventoryFormats */
/** @var \backend\modules\product\models\forms\ExportInventoryForm $exportInventoryFrom */
/** @var \backend\modules\product\models\forms\ExportSalePriceForm $exportSalePriceForm */

?>

<br>

<?php $form = ActiveForm::begin([
        'action' => ['export-products'],
        'options' => [
            'class' => 'form-inline',
        ]
    ]); ?>

    <?= $form->field($exportProductsForm, 'format')->dropDownList($exportProductsForm->getExportFormats())->label(false); ?>

    <?= Html::submitButton('Export products', ['class' => 'btn btn-primary']); ?>

<?php ActiveForm::end(); ?>