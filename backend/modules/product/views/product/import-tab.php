<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

/** @var \yii\web\View $this */
/** @var \yii\widgets\ActiveForm $form */
/** @var \backend\modules\product\models\forms\ImportInventoryForm $importInventoryForm */
/** @var \backend\modules\product\models\forms\ImportSalePriceForm $importSalePriceForm */

?>

<br>

<?php $form = ActiveForm::begin([
    'action' => ['import-products'],
    'options' => [
        'class' => 'form-inline',
        'enctype' => 'multipart/form-data',
    ]
]); ?>

    <?= $form->field($importProductsForm, 'file')->fileInput(['accept' => '.xls'])->label(false); ?>

    <br>

    <?= $form->field($importProductsForm, 'images')->fileInput(['accept' => '.zip'])->label('Images zip archive'); ?>

    <?= Html::submitButton('Import products', ['class' => 'btn btn-primary']); ?>

<?php ActiveForm::end(); ?>
