<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\product\models\ProductReview */

$this->title = 'Create Product Review';
$this->params['breadcrumbs'][] = ['label' => 'Product Reviews', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="product-review-create">

    <?= $this->render('_form', compact('model', 'listProductVariations')); ?>

</div>
