<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\rating\StarRating;

/* @var $this yii\web\View */
/* @var $model backend\modules\product\models\ProductReview */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-review-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'product_id')->dropDownList($listProductVariations); ?>

    <?= $form->field($model, 'username'); ?>

    <?= $form->field($model, 'surname'); ?>

    <?= $form->field($model, 'subject')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]); ?>

    <?= $form->field($model, 'formRating')->widget(StarRating::classname(), [
        'pluginOptions' => ['step' => 0.5]
    ]); ?>


    <?= $form->field($model, 'active')->checkbox(); ?>

    <?= $form->field($model, 'admin_answer')->textarea(['rows' => 6])->label('Answer'); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
