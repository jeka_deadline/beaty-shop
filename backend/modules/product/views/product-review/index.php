<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\product\models\searchModels\ProductReviewSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Product Reviews';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-review-index table-responsive">
    <p>
        <?= Html::a('Create Product Review', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'user_id',
                'value' => function($model) { return ($model->user) ? $model->user->email : ''; },
            ],
            [
                'attribute' => 'product_id',
                'value' => function($model) { return $model->product->defaultProductVariation->name; },
            ],
            'subject:ntext',
            [
                'attribute' => 'text',
                'value' => function($model){ return StringHelper::truncate($model->text, 62, '...', 'UTF-8'); }
            ],
            //'active',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
