<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\product\models\ProductFreeRecommendet */

$this->title = 'Update recommendation: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'recommendation', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'recommendation: ' .$model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-free-recommendet-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
