<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\product\models\ProductFreeRecommendetSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Recommendation';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-free-recommendet-index table-responsive">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create recommendation', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'product.defaultProductVariation.name',
            'display_order',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
