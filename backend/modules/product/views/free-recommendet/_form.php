<?php

use backend\modules\product\widgets\ProductFreeRecommendetsInputWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\product\models\ProductFreeRecommendet */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="product-free-recommendet-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'product_recommended_id')->widget(ProductFreeRecommendetsInputWidget::className(),[
        'urlSearch' => Url::to(['/product/product/search-recommendet-products']),
    ]); ?>

    <?= $form->field($model, 'display_order')->textInput(); ?>

    <div class="form-group">

        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>
