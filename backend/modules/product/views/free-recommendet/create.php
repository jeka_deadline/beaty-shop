<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\product\models\ProductFreeRecommendet */

$this->title = 'Create recommendation';
$this->params['breadcrumbs'][] = ['label' => 'Recommendation', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-free-recommendet-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
