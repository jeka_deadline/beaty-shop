<?php

use backend\modules\product\models\Product;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\grid\DataColumn;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\modules\product\models\Order */

$this->title = 'Order #' . $model->order_number;
$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-view">
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>

        <?php if ($model->status === $model::ORDER_STATUS_PREAUTORIZATION) : ?>

            <?= Html::a('Caputy', ['caputy', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>

        <?php endif; ?>
    </p>

    <h2>Order</h2>
    <div class="well">

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'order_number',
                'user_id',
                'coupon_code',
                'type_payment',
                'status',
                [
                    'attribute' => 'total_sum',
                    'value' => function($model) {
                        return $model->total_sum . ' €';
                    }
                ],
                [
                    'attribute' => 'tax',
                    'value' => function($model) {
                        return $model->tax . ' €';
                    }
                ],
                [
                    'attribute' => 'shipping_price',
                    'value' => function($model) {
                        return $model->shipping_price . ' €';
                    }
                ],
                [
                    'attribute' => 'full_sum',
                    'value' => function($model) {
                        return $model->full_sum . ' €';
                    }
                ],
                'created_at',
            ],
        ]) ?>

    </div>
    <h2>Products</h2>
    <div class="well table-responsive">

        <?= GridView::widget([
            'dataProvider' => $productDataProvider,
            'columns' => [
                [
                    'attribute' => 'product_variation_name',
                    'format' => 'raw',
                    'value' => function($model) {
                        $html = $model->product_variation_name;

                        if ($model->productVariation->allProperties) {
                            foreach ($model->productVariation->allProperties as $property) {
                                $html .= sprintf('<p style="margin-left: 20px"><b>%s</b>: %s</p>', $property->filter->name, $property->humanValue);
                            }
                        }

                        if (($product = $model->product)) {
                            switch ($product->type) {
                                case Product::TYPE_PRODUCT_SET:
                                    $setVariationDataProvider = new ActiveDataProvider([
                                        'query' => $product->getProductSetRelationVariations(),
                                        'sort' => false,
                                    ]);
                                    $html .= $this->render('_set_variations', compact('setVariationDataProvider'));
                                    break;
                            }
                        }

                        return $html;
                    }
                ],
                [
                    'class' => DataColumn::className(),
                    'header' => 'Vendor code',
                    'content' => function($model){ return $model->productVariation->vendor_code; },
                ],
                'count',
                [
                    'attribute' => 'price',
                    'value' => function($model) {
                        return $model->price . ' €';
                    }
                ],
                [
                    'attribute' => 'promotion',
                    'value' => function($model) {
                        if (!$model->promotion) {
                            return '';
                        }

                        return $model->promotion . ' €';
                    }
                ],
            ],
            'summary' => false,
        ]); ?>

    </div>
    <h2>Shipping information

        <?php if ($model->orderShippingInformation) : ?>

            <a target="_blank" class="btn btn-info" href="<?= Url::toRoute(['/product/order/print-shipping-document', ' orderShippingId' => $model->orderShippingInformation->id]); ?>">Print</a>
            <a target="_blank" class="btn btn-warning" href="<?= Url::toRoute(['/product/order/print-ls', 'orderId' => $model->id]); ?>">LS</a>
            <a target="_blank" class="btn btn-primary" href="<?= Url::toRoute(['/product/order/print-invoice', 'orderId' => $model->id]); ?>">R</a>

        <?php endif; ?>

    </h2>
    <div class="well">

        <?php if ($model->orderShippingInformation) : ?>

            <?= DetailView::widget([
                'model' => $model->orderShippingInformation,
                'attributes' => [
                    'shipping_method',
                    'title',
                    'surname',
                    'name',
                    'company',
                    [
                        'attribute' => 'country_id',
                        'value' => function($model){ return $model->country->name; },
                    ],
                    'city',
                    'address1',
                    'address2',
                    'zip',
                    'phone',
                    'email',
                ],
            ]) ?>

      <?php endif; ?>

    </div>
    <h2>Billing information</h2>
    <div class="well">

        <?php if ($model->orderBillingInformation) : ?>

            <?= DetailView::widget([
                'model' => $model->orderBillingInformation,
                'attributes' => [
                    'title',
                    'surname',
                    'name',
                    'company',
                    [
                        'attribute' => 'country_id',
                        'value' => function($model){ return $model->country->name; },
                    ],
                    'city',
                    'address1',
                    'address2',
                    'zip',
                ],
            ]) ?>

        <?php else : ?>

            <?php if ($model->orderShippingInformation) : ?>

                <?= DetailView::widget([
                    'model' => $model->orderShippingInformation,
                    'attributes' => [
                        'title',
                        'surname',
                        'name',
                        'company',
                        [
                            'attribute' => 'country_id',
                            'value' => function($model){ return $model->country->name; },
                        ],
                        'city',
                        'address1',
                        'address2',
                        'zip',
                    ],
                ]) ?>

          <?php endif; ?>

        <?php endif; ?>

    </div>
</div>
