<table style="width: 100%; font-size: 12px; color: #000">
    <tr>
        <td><strong>Infinity Lashes</strong></td>
        <td><strong>Kontaktinformationen</strong></td>
        <td><strong>Bankverbindung</strong></td>
        <td></td>
    </tr>
    <tr>
        <td>Kaiserstraße 59</td>
        <td>Sergey Pinskiy</td>
        <td>IBAN</td>
        <td>DE50 4404 0037 0216 6023 00</td>
    </tr>
    <tr>
        <td>44135 Dortmund</td>
        <td>Tel. +49 1732595108</td>
        <td>SWIFT/BIC</td>
        <td>COBADEFFXXX</td>
    </tr>
    <tr>
        <td>Deutschland</td>
        <td>Email: info@infinity-lashes.de</td>
        <td>PayPal</td>
        <td>paypal.me/infinitylashes</td>
    </tr>
    <tr>
        <td>USt.-IdNr. DE312720717</td>
        <td>www.infinity-lashes.de</td>
        <td></td>
        <td></td>
    </tr>
</table>