<?php

use yii\grid\GridView;

?>

<div class="panel panel-default" style="margin-left: 20px">
    <div class="panel-heading">
        Product list
    </div>
    <div class="panel-body">
        <?= GridView::widget([
            'dataProvider' => $setVariationDataProvider,
            'columns' => [
                [
                    'label' => 'Name',
                    'value' => function($model) {
                        return $model->productVariation?$model->productVariation->name:'';
                    }
                ],
                [
                    'label' => 'Properties',
                    'format' => 'raw',
                    'value' => function($model) {
                        if (!(($productVariation = $model->productVariation) && $productVariation->groupProperties)) return '';

                        $html = '';
                        foreach ($productVariation->groupProperties as $property) {
                            $html .= sprintf('<p><b>%s</b>: %s</p>', $property->filter->name, $property->humanValue);
                        }
                        $html .= sprintf('<p><b>%s</b>: %s</p>', 'Vendor code', ($model->productVariation?$model->productVariation->vendor_code:''));

                        return $html;
                    }
                ],
                'count',
            ],
            'summary' => false,
        ]); ?>
    </div>
</div>
