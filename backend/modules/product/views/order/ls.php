<?php

use yii\data\ActiveDataProvider;
use yii\widgets\DetailView;
use yii\widgets\ListView;
use yii\helpers\Url;
use frontend\modules\core\models\Setting;
use frontend\modules\product\models\ProductVariation;

?>

<div style="text-align: center;">
    <img style="width: 60%" src="/files/core/email/img/infiniry_logo.png" alt="">
</div>

<table style="margin: 45 0px 20px 0px;">
    <tr>
        <td><i>Infinity Lashes</i></td>
    </tr>
    <tr>
        <td><i>Kaiserstraße 59</i></td>
    </tr>
    <tr>
        <td><i>44135 Dortmund</i></td>
    </tr>
</table>

<div style="width: 50%; float: left;">

    <?php if ($modelOrder->orderShippingInformation) : ?>

        <table style="float: left; width: 50%;">
            <tr>
                <td><strong>Empfänger:</strong></td>
            </tr>
            <tr>
                <td>
                    <?= $modelOrder->orderShippingInformation->company; ?>
                </td>
            </tr>
            <tr>
                <td><?= $modelOrder->orderShippingInformation->name . ' ' . $modelOrder->orderShippingInformation->surname ;?></td>
            </tr>
            <tr>
                <td><?= $modelOrder->orderShippingInformation->address1; ?></td>
            </tr>
            <tr>
                <td><?= $modelOrder->orderShippingInformation->zip; ?> <?= $modelOrder->orderShippingInformation->city; ?></td>
            </tr>
            <tr>
                <td>
                    <?= (!empty($modelOrder->orderShippingInformation->country)) ? $modelOrder->orderShippingInformation->country->name : ''; ?>
                </td>
            </tr>
        </table>

    <?php endif; ?>

</div>

<div style="width: 50%; float: right;">
    <div style="background-color: #fff; margin: 0px 0px 3px 5px; font-size: 16px"><strong>Lieferschein <?= $modelOrder->pdfNumberFormat; ?></strong></div>
    <table style="width: 50%; font-size: 14px; background-color: #eee">
        <tr>
            <td class="textr">Rechnungsdatum</td>
            <td style="padding-left: 20px"><?= $modelOrder->getCreatedAtFormat(); ?></td>
        </tr>
        <tr>
            <td class="textr">Rechnungsnummer</td>
            <td style="padding-left: 20px"><?= $modelOrder->fullPdfNumberFormat; ?></td>
        </tr>
        <tr>
            <td class="textr">Kundennummer</td>
            <td style="padding-left: 20px"><?= ($modelOrder->user_id) ? $modelOrder->pdfUserIdFormat : '--'; ?></td>
        </tr>
        <tr>
            <td class="textr">Zahlungsziel</td>
            <td style="padding-left: 20px"><?= $modelOrder->type_payment; ?></td>
        </tr>
    </table>
</div>

<div style="clear: both; margin: 0pt; padding: 0pt; "></div>

<div style="margin: 20px 0px;">
    <span><strong>Zusätzliche Informationen</strong></span><br>
    <span><i>Die Ware bleibt bis zur vollständigen Bezahlung Eigentum von Infinity Lashes.</i></span>
</div>

<?php if ($modelOrder->orderProducts): ?>
<?php $price = 0; ?>
<?php $discount = $modelOrder->sum_discount; ?>

    <table class="bordered" style="width: 100%;" cellspacing="0" cellpadding="0">
        <thead>
        <tr style="background-color: #C5D9F1">
            <th style="text-align: center; border-left: 1px solid #000">Artikel</th>
            <th style="text-align: center; width: 20px;">Menge</th>
        </tr>
        </thead>
        <tbody>
        <?php $countProducts = count($modelOrder->orderProducts); ?>

        <?php foreach ($modelOrder->orderProducts as $index => $product) : ?>
        <?php $productPrice = ($product->promotion) ? $product->promotion : $product->price; ?>

            <?php if ($countProducts == $index + 1) : ?>

                <?php //echo '<pre>'; print_r($product->productVariation->vendor_code); exit; ?>

                <tr>
                    <td style="border-left: 1px solid #000">

                        [ <?= $product->productVariation->vendor_code; ?> ] <?= $product->product_variation_name; ?>

                        <?php if ($product->productVariation->allProperties) :?>

                            <table style="border: 0px; margin-left: 10px">

                                <?php foreach ($product->productVariation->allProperties as $property) : ?>

                                    <tr>
                                        <td style="border: 0px;"><?= sprintf('%s: %s', $property->filter->name, $property->humanValue); ?></td>
                                    </tr>

                                <?php endforeach; ?>

                            </table>

                        <?php endif; ?>

                        <?php if (
                            $product->productVariation
                            && ($setProduct = $product->productVariation->product)
                            && ($setRelationVariations = $setProduct->productSetRelationVariations)
                        ) :?>

                            <table style="border: 1px; margin-left: 20px">

                                <?php foreach ($setRelationVariations as $setRelationVariation) : ?>
                                    <?php if (!($variation = $setRelationVariation->productVariation)) continue;?>
                                    <tr>
                                        <td style="border: 0px;">[ <?= $variation->vendor_code; ?> ] <?= $variation->name; ?></td>
                                    </tr>
                                    <tr>
                                        <td style="border: 0px;"><?= sprintf('%s: %s', 'Count', $setRelationVariation->count); ?></td>
                                    </tr>
                                    <?php foreach ($variation->groupProperties as $property) : ?>

                                        <tr>
                                            <td style="border: 0px;"><?= sprintf('%s: %s', $property->filter->name, $property->humanValue); ?></td>
                                        </tr>

                                    <?php endforeach; ?>

                                <?php endforeach; ?>

                            </table>

                        <?php endif; ?>

                    </td>
                    <td class="textr"><?= $product->count; ?></td>
                </tr>

            <?php else : ?>

                <tr>
                    <td style="border-bottom: 0px; border-left: 1px solid #000">

                        [ <?= $product->productVariation->vendor_code; ?> ] <?= $product->product_variation_name; ?>

                        <?php if ($product->productVariation->allProperties) :?>

                            <table style="border: 0px; margin-left: 10px">

                                <?php foreach ($product->productVariation->allProperties as $property) : ?>

                                    <tr>
                                        <td style="border: 0px;"><?= sprintf('%s: %s', $property->filter->name, $property->humanValue); ?></td>
                                    </tr>

                                <?php endforeach; ?>

                            </table>

                        <?php endif; ?>

                        <?php if (
                                $product->productVariation
                                && ($setProduct = $product->productVariation->product)
                                && ($setRelationVariations = $setProduct->productSetRelationVariations)
                        ) :?>

                            <table style="border: 1px; margin-left: 20px">

                                <?php foreach ($setRelationVariations as $setRelationVariation) : ?>
                                    <?php if (!($variation = $setRelationVariation->productVariation)) continue;?>
                                    <tr>
                                        <td style="border: 0px;">[ <?= $variation->vendor_code; ?> ] <?= $variation->name; ?></td>
                                    </tr>
                                    <tr>
                                        <td style="border: 0px;"><?= sprintf('%s: %s', 'Count', $setRelationVariation->count); ?></td>
                                    </tr>
                                    <?php foreach ($variation->groupProperties as $property) : ?>

                                        <tr>
                                            <td style="border: 0px;"><?= sprintf('%s: %s', $property->filter->name, $property->humanValue); ?></td>
                                        </tr>

                                    <?php endforeach; ?>

                                <?php endforeach; ?>

                            </table>

                        <?php endif; ?>

                    </td>
                    <td class="textr" style="border-bottom: 0px;"><?= $product->count; ?></td>
                </tr>

            <?php endif; ?>

        <?php $price += ProductVariation::viewFormatPrice($product->count * $productPrice); ?>
        <?php endforeach; ?>
        </tbody>
    </table>

<?php endif; ?>
