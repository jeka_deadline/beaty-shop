<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\grid\DataColumn;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\product\models\searchModels\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orders';
$this->params['breadcrumbs'][] = $this->title;

$js = <<<JS
$(document).on('click', '.btn-xs', function( e ) {
    $(this).remove();
});
JS;

$this->registerJs($js, $this::POS_END);
?>
<div class="order-index table-responsive">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'order_number',
                'options' => [
                    'style' => 'width:100px',
                ],
            ],
            [
                'attribute' => 'invoice_number',
                'class' => 'yii\grid\DataColumn',
                'value' => function($model) {
                    return $model->fullPdfNumberFormat;
                },
                'header' => 'Rechnungsnummer',
            ],
            [
                'attribute' => 'status',
                'label' => 'Step payment',
                'value' => function($model){ return $model->getStepPayment(); },
                'filter' => false,
                'options' => [
                    'style' => 'width:150px',
                ],
                'enableSorting' => false,
            ],
            [
                'attribute' => 'is_new',
                'value' => function($model) { return ($model->is_new) ? 'Yes' : 'No'; },
                'filter' => [1 => 'Yes', 0 => 'No'],
                'options' => [
                    'style' => 'width:50px',
                ],
                'enableSorting' => false,
            ],
            [
                'attribute' => 'total_sum',
                'value' => function($model) {
                    return $model->total_sum . ' €';
                },
                'options' => [
                    'style' => 'width:200px',
                ],
                'enableSorting' => false,
                'filter' => false,
            ],
            [
                'attribute' => 'tax',
                'value' => function($model) {
                    return $model->tax . ' €';
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'shipping_price',
                'value' => function($model) {
                    return $model->shipping_price . ' €';
                },
                'options' => [
                    'style' => 'width:200px',
                ],
                'enableSorting' => false,
            ],
            [
                'attribute' => 'full_sum',
                'value' => function($model) {
                    return $model->full_sum . ' €';
                },
                'options' => [
                    'style' => 'width:200px',
                ],
                'enableSorting' => false,
            ],
            [
                'attribute' => 'distributor',
                'filter' => $listDistributors,
                'header' => 'Distributor',
                'value' => function($model) {
                    if ($model->user) {
                        return $model->user->distributorFullName;
                    }

                    return null;
                }
            ],
            [
                'attribute' => 'created_at',
                'filter' => false,
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'format' => 'raw',
                'header' => 'Print',
                'value' => function($model) {
                    $html = '';

                    if ($model->orderShippingInformation) {
                        if (empty($model->print_shipping_date)) {
                            $html .= Html::a('SH', ['print-shipping-document', 'orderShippingId' => $model->orderShippingInformation->id], ['class' => 'btn btn-primary btn-xs', 'target' => '_blank']);
                        }
                    }

                    if (empty($model->print_ls_date)) {
                        $html .= ' ' . Html::a('LS', ['print-ls', 'orderId' => $model->id], ['class' => 'btn btn-primary btn-xs', 'target' => '_blank']);
                    }

                    if (empty($model->print_invoice_date) && ($model->status === $model::ORDER_STATUS_PAID || $model->status === $model::ORDER_STATUS_PREAUTORIZATION)) {
                        $html .= ' ' . Html::a('R', ['print-invoice', 'orderId' => $model->id], ['class' => 'btn btn-primary btn-xs', 'target' => '_blank']);
                    }

                    return $html;
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
