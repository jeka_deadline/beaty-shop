<style>
    body {
        background: url(<?= Yii::getAlias('@web/img/dhl.jpg'); ?>);
        background-repeat: no-repeat;
        background-size: 100% 100%;
    }
    * {
        margin: 0px;
        padding: 0px;
        box-sizing: border-box;
    }
    #wrapper {

        margin-top: 2.1cm;
        /* */
    }

    #wrapper2 {
        margin-top: 14.1cm;
    }

    #wrapper, #wrapper2 {
        margin-left: 1.3cm;
    }

    div > span {
        height: 0.85cm;
        padding: 0px;
        margin: 0px;
        display: inline-block;
    }
    div > span:first-child {
        width: 6.8cm;
    }

    div > span:last-child {
        width: 6.8cm;
    }

    @media print {
        * {
            -webkit-print-color-adjust: exact;
        }
    }

</style>

<div id="wrapper">
    <div>
        <span>Infinity Lashes</span>
        <span><?= $orderShipping->company . '&nbsp;'; ?></span>
    </div>
    <div style="margin-top: -0.63cm;">
        <span></span>
        <span><?= $orderShipping->name . ' ' . $orderShipping->surname; ?></span>
    </div>
    <div style="margin-top: -0.05cm;">
        <span>Kaiserstraße 59</span>
        <span><?= $orderShipping->address1; ?></span>
    </div>
    <div style="margin-top: -0.02cm">
        <span style="padding-right: 0cm;"><span style="width: 2cm; margin-right: 0.7cm">44135</span>Dortmund</span>
        <span><span style="width: 2.1cm; margin-right: 1.3cm"><?= $orderShipping->zip; ?></span><?= $orderShipping->city; ?></span>
    </div>
</div>
<div id="wrapper2">
    <div>
        <span>Infinity Lashes</span>
        <span><?= $orderShipping->company . '&nbsp;'; ?></span>
    </div>
    <div style="margin-top: -0.7cm;">
        <span></span>
        <span><?= $orderShipping->name . ' ' . $orderShipping->surname; ?></span>
    </div>
    <div style="margin-top: -0.05cm;">
        <span>Kaiserstraße 59</span>
        <span><?= $orderShipping->address1; ?></span>
    </div>
    <div style="margin-top: -0.08cm">
        <span style="padding-right: 0cm;"><span style="width: 2cm; margin-right: 0.7cm">44135</span>Dortmund</span>
        <span><span style="width: 2.1cm; margin-right: 1.3cm"><?= $orderShipping->zip; ?></span><?= $orderShipping->city; ?></span>
    </div>
</div>

<script>
    window.print();
</script>