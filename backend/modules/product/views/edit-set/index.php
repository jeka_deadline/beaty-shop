<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\StringHelper;

$this->title = 'Edit Sets';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index table-responsive">

    <br>
    <p>
        <?= Html::a('Create Edit Set', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => '{summary}{pager}{items}{pager}',
        'options' => [
            'id' => 'grid-with-select',
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'slug',
            [
                'attribute' => 'meta_title',
                'value' => function($model){ return StringHelper::truncate($model->meta_title, 50, '...', 'UTF-8'); }
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'format' => 'raw',
                'header' => 'Categories',
                'value' => function($model) {
                    return $model->categoriesRowList;
                }
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
