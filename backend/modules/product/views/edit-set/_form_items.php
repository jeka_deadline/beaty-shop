<?php

use backend\modules\product\widgets\ProductSetItemsInputWidget;
use yii\helpers\Url;

?>

<?= $form->field($model, 'productSetProducts')->widget(ProductSetItemsInputWidget::className(),[
    'urlSearch' => Url::to(['/product/edit-set/search-set-products']),
    'template' => 'product-set-product'
])->label(false); ?>
