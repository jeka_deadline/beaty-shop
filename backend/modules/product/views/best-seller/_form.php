<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\product\widgets\ChooseProductBestSellerWidget;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\modules\product\models\ProductBestSeller */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-best-seller-form">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'product_id')->widget(ChooseProductBestSellerWidget::className(),[
            'urlSearch' => Url::to(['/product/product/search-recommendet-products']),
        ]); ?>

        <?= $form->field($model, 'display_order')->textInput(); ?>

        <div class="form-group">

            <?= Html::submitButton('Save', ['class' => 'btn btn-success']); ?>

        </div>

    <?php ActiveForm::end(); ?>

</div>
