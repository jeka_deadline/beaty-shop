<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\product\models\searchModels\ProductBestSellerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Product Best Sellers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-best-seller-index table-responsive">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Create Product Best Seller', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'product.defaultProductVariation.name',
            'display_order',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
