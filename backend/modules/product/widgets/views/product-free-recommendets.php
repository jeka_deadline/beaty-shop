<?php

use backend\modules\product\models\ProductVariation;
use kartik\typeahead\Typeahead;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

$template = '<div>'.
    '<div class="media">'.
    '<div class="media-left media-middle">'.
    '<img class="media-object" src="{{image}}" style="max-height: 50px">'.
    '</div>'.
    '<div class="media-body">'.
    '<p>{{name}}</p>'.
    '</div>'.
    '</div>'.
    '</div>';

$name = Html::getInputName($model, $attribute);
$id = Html::getInputId($model, $attribute);

if ($model->product && $model->product->defaultProductVariation) {
    $value = $model->product->defaultProductVariation->name;
} else {
    $value = '';
}

?>
<div class="product-free-recommendets-input-widget">
    <?= Html::activeHiddenInput($model, $attribute, [
    ]) ?>

    <div class="row">
        <div class="col-lg-12">
            <p></p>
            <div class="form-group">
                <?= Typeahead::widget([
                    'name' => $id.'-search',
                    'value' => $value,
                    'options' => [
                        'placeholder' => 'Find the recommended product ...'
                    ],
                    'dataset' => [
                        [
                            'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                            'display' => 'value',
                            'templates' => [
                                'notFound' => '<div class="text-danger" style="padding:0 8px">Unable to find repositories for selected query.</div>',
                                'suggestion' => new JsExpression("Handlebars.compile('{$template}')")
                            ],
                            'remote' => [
                                'url' => $urlSearch . '?q=%QUERY',
                                'wildcard' => '%QUERY'
                            ]
                        ]
                    ],
                    'pluginEvents' => [
                        'typeahead:select' => 'productFreeRecommendetsWidgetSearchSelect',
                    ]
                ]) ?>
            </div>
        </div>
    </div>
</div>