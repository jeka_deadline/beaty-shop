<?php

use kartik\typeahead\Typeahead;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

$template = '<div>'.
    '<div class="media">'.
    '<div class="media-left media-middle">'.
    '<img class="media-object" src="{{image}}" style="max-height: 50px">'.
    '</div>'.
    '<div class="media-body">'.
    '<p>{{name}}</p>'.
    '</div>'.
    '</div>'.
    '</div>';

$name = Html::getInputName($model, $attribute);
$id = Html::getInputId($model, $attribute);
?>
<div class="product-set-variations-input-widget">
    <?= Html::activeDropDownList($model, $attribute, ArrayHelper::map($model->$attribute,'id', function ($model) {
        return $model->defaultProductVariation?$model->defaultProductVariation->name:'';
    }), [
        'prompt' => '',
        'multiple' => true,
        'class' => 'hidden'
    ]) ?>

    <div class="row">
        <div class="col-lg-4"></div>
        <div class="col-lg-4">
            <p></p>
            <div class="form-group">
                <?= Typeahead::widget([
                    'name' => $id.'-search',
                    'options' => ['placeholder' => 'Find the recommendation product ...'],
                    'dataset' => [
                        [
                            //'prefetch' => $urlSearch,
                            'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                            'display' => 'value',
                            'templates' => [
                                'notFound' => '<div class="text-danger" style="padding:0 8px">Unable to find repositories for selected query.</div>',
                                'suggestion' => new JsExpression("Handlebars.compile('{$template}')")
                            ],
                            'remote' => [
                                'url' => $urlSearch . '?q=%QUERY',
                                'wildcard' => '%QUERY'
                            ]
                        ]
                    ],
                    'pluginEvents' => [
                        'typeahead:select' => 'productEditSetWidgetSearchSelect',
                    ]
                ]) ?>
            </div>
        </div>
        <div class="col-lg-4"></div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body" id="<?= $id.'-panel' ?>">
                    <?php
                    $items = [];
                    if ($model->$attribute) {
                        $setRelations = $model->productSetRelationProducts;
                        foreach ($model->$attribute as $key => $product) {
                            if (!($productVariation = $product->defaultProductVariation)) continue;
                            $setRelation = $setRelations[$product->id];
                            $product_id = $product->id;
                            echo $this->render('_item_products', compact('productVariation', 'setRelation', 'product_id'));
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>