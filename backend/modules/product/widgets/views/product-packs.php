<?php

use backend\modules\product\models\ProductVariation;
use kartik\typeahead\Typeahead;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

$template = '<div>'.
    '<div class="media">'.
    '<div class="media-left media-middle">'.
    '<img class="media-object" src="{{image}}" style="max-height: 50px">'.
    '</div>'.
    '<div class="media-body">'.
    '<p>{{name}}</p>'.
    '</div>'.
    '</div>'.
    '</div>';

$name = Html::getInputName($model, $attribute);
$id = Html::getInputId($model, $attribute);
?>
<div class="product-recommendets-input-widget">
    <?= Html::activeDropDownList($model, $attribute, ArrayHelper::map($model->$attribute,'id', function ($model) {
        return $model->defaultProductVariation?$model->defaultProductVariation->name:'';
    }), [
        'prompt' => '',
        'multiple' => true,
        'class' => 'hidden'
    ]) ?>

    <div class="row">
        <div class="col-lg-4"></div>
        <div class="col-lg-4">
            <p></p>
            <div class="form-group">
                <?= Typeahead::widget([
                    'name' => $id.'-search',
                    'options' => ['placeholder' => 'Find the pack product ...'],
                    'dataset' => [
                        [
                            //'prefetch' => $urlSearch,
                            'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                            'display' => 'value',
                            'templates' => [
                                'notFound' => '<div class="text-danger" style="padding:0 8px">Unable to find repositories for selected query.</div>',
                                'suggestion' => new JsExpression("Handlebars.compile('{$template}')")
                            ],
                            'remote' => [
                                'url' => $urlSearch . '?q=%QUERY',
                                'wildcard' => '%QUERY'
                            ]
                        ]
                    ],
                    'pluginEvents' => [
                        'typeahead:select' => 'productRecommendetsWidgetSearchSelect',
                    ]
                ]) ?>
            </div>
        </div>
        <div class="col-lg-4"></div>
    </div>



    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body" id="<?= $id.'-panel' ?>">
                    <?php if ($model->$attribute):?>
                        <?php foreach ($model->$attribute as $key=>$item): ?>
                            <div class="col-sm-3 col-md-3 product-item">
                                <div class="thumbnail" style="height: 300px">
                                    <div class="text-right">
                                        <a href="#" class="text-danger click-delete" data-id="<?= $item->id ?>">
                                            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                        </a>
                                    </div>
                                    <?php if(($productVariation = $item->defaultProductVariation)): ?>
                                        <?php if($productVariation->preview): ?>
                                            <img src="<?= $productVariation->preview->getUrlImage() ?>" alt="<?= $productVariation->name ?>" style="height: 182px">
                                        <?php else: ?>
                                            <img src="" alt="" style="height: 182px">
                                        <?php endif; ?>
                                        <div class="caption">
                                            <h4><?= $productVariation->name ?></h4>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
