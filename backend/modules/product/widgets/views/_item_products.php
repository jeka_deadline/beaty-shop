<?php

use yii\helpers\Html;

?>
<div class="col-sm-3 col-md-3 product-item">
    <div class="thumbnail" style="height: 424px">
        <div class="text-right">
            <a href="#" class="text-danger click-delete" data-id="<?= $product_id ?>">
                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
            </a>
        </div>
        <?php if($productVariation->preview): ?>
            <img src="<?= $productVariation->preview->getUrlImage() ?>" alt="<?= $productVariation->name ?>" style="height: 182px">
        <?php else: ?>
            <img src="" alt="" style="height: 182px">
        <?php endif; ?>
        <div class="caption">
            <h4><?= mb_strimwidth($productVariation->name, 0, 80, "..."); ?></h4>
            <div class="form-group">
                <?= Html::activeLabel($setRelation, 'count', ['class' => 'control-label']) ?>&emsp;
                <?= Html::textInput(str_replace('[','['.$product_id.'][',Html::getInputName($setRelation, 'count')), $setRelation->count, [
                    'class' => 'form-control',
                    'id' => Html::getInputId($setRelation, 'count').'-'.$product_id,
                    'data-id' => $product_id,
                ]) ?>
                <div class="help-block"></div>
            </div>
            <div class="form-group">
                <?= Html::activeLabel($setRelation, 'display_order') ?>&emsp;
                <?= Html::textInput(str_replace('[','['.$product_id.'][',Html::getInputName($setRelation, 'display_order')), $setRelation->display_order, [
                    'class' => 'form-control',
                    'id' => Html::getInputId($setRelation, 'count').'-'.$product_id,
                    'data-id' => $product_id,
                ]) ?>
                <div class="help-block"></div>
            </div>
        </div>
    </div>
</div>