<?php
namespace backend\modules\product\widgets;

use backend\modules\product\assets\ProductVariationAsset;
use yii\widgets\InputWidget;

class ProductFreeRecommendetsInputWidget extends InputWidget
{

    public $template  = 'product-free-recommendets';

    public $urlSearch = '';

    public function init() {
        parent::init();
        $this->registerAssets();
    }

    public function run() {
        if ($this->hasModel()) {
            return $this->render($this->template, [
                'model' => $this->model,
                'attribute' => $this->attribute,
                'urlSearch' => $this->urlSearch,
            ]);
        }
    }

    public function registerAssets() {
        $view=$this->getView();
        ProductVariationAsset::register($view);
    }
}