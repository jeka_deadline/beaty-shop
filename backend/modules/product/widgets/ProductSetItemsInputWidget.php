<?php
namespace backend\modules\product\widgets;

use backend\modules\product\assets\ProductSetVariationAsset;
use yii\widgets\InputWidget;

class ProductSetItemsInputWidget extends InputWidget
{

    public $template  = 'product-set-variation';

    public $urlSearch = '';

    public function init() {
        parent::init();
        $this->registerAssets();
    }

    public function run() {
        if ($this->hasModel()) {
            return $this->render($this->template, [
                'model' => $this->model,
                'attribute' => $this->attribute,
                'urlSearch' => $this->urlSearch,
            ]);
        }
    }

    public function registerAssets() {
        $view=$this->getView();
        ProductSetVariationAsset::register($view);
    }
}