<?php

namespace backend\modules\product\helpers;

use DOMDocument;

class XmlCategory
{
    /**
     * Root.
     */
    private $dom;

    /**
     * Node categories.
     */
    private $categories;

    /**
     * Node category.
     */
    private $category;

    /**
     * Array names for category with language.
     *
     * @var array
     */
    private $name;

    /**
     * Array descriptions for category with language.
     *
     * @var array
     */
    private $description;

    /**
     * Array for meta with lang.
     *
     * @var array
     */
    private $meta;

    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct($version, $encoding = 'UTF-8')
    {
        $this->dom = new DOMDocument($version, $encoding);
        $this->categories = $this->dom->createElement('categories');
    }

    /**
     * Set category slug to category node.
     *
     * @param string $slug Category slug
     * @return $this
     */
    public function setCategorySlug($slug)
    {
        if (!$this->category) {
            $this->createNodeCategory();
        }

        $this->category->appendChild($this->dom->createElement('slug', $slug));

        return $this;
    }

    /**
     * Set category id to category node.
     *
     * @param string $id Category id
     * @return $this
     */
    public function setCategoryId($id)
    {
        if (!$this->category) {
            $this->createNodeCategory();
        }

        $this->category->appendChild($this->dom->createElement('id', $id));

        return $this;
    }

    /**
     * Set category display order to category node.
     *
     * @param string $displayOrder Category display order
     * @return $this
     */
    public function setCategoryDisplayOrder($displayOrder)
    {
        if (!$this->category) {
            $this->createNodeCategory();
        }

        $this->category->appendChild($this->dom->createElement('displayOrder', $displayOrder));

        return $this;
    }

    /**
     * Set category active to category node.
     *
     * @param string $active Category active
     * @return $this
     */
    public function setCategoryActive($active)
    {
        if (!$this->category) {
            $this->createNodeCategory();
        }

        $this->category->appendChild($this->dom->createElement('active', $active));

        return $this;
    }

    /**
     * Set category parent id to category node.
     *
     * @param int $parentId Category parent id
     * @return $this
     */
    public function setCategoryParent($parent)
    {
        if (!$this->category) {
            $this->createNodeCategory();
        }

        $this->category->appendChild($this->dom->createElement('parent', $parent));

        return $this;
    }

    /**
     * Set category show search to category node.
     *
     * @param int $showSearch Show this category on page search
     * @return $this
     */
    public function setCategoryShowSearch($showSearch)
    {
        if (!$this->category) {
            $this->createNodeCategory();
        }

        $this->category->appendChild($this->dom->createElement('showSearch', $showSearch));

        return $this;
    }

    /**
     * Set category show search to category node.
     *
     * @param int $displayOrderHmenu Sort categories on horizontal menu
     * @return $this
     */
    public function setCategoryDisplayOrderHmenu($displayOrderHmenu)
    {
        if (!$this->category) {
            $this->createNodeCategory();
        }

        $this->category->appendChild($this->dom->createElement('displayOrderHmenu', $displayOrderHmenu));

        return $this;
    }

    /**
     * Set category name to category node.
     *
     * @param string $name Category name
     * @param string $lang Language
     * @return $this
     */
    public function setCategoryName($name, $lang)
    {
        $this->name[ $lang ] = $name;

        return $this;
    }

    /**
     * Set category description to category node.
     *
     * @param string $description Category description
     * @param string $lang Language
     * @return $this
     */
    public function setCategoryDescription($description, $lang)
    {
        $this->description[ $lang ] = $description;

        return $this;
    }

    /**
     * Set meta description to category node.
     *
     * @param string $metaDescription Meta description
     * @param string $lang Language
     * @return $this
     */
    public function setCategoryMetaDescription($metaDescription, $lang)
    {
        $this->meta[ 'description' ][ $lang ] = $metaDescription;

        return $this;
    }

    /**
     * Set meta title to category node.
     *
     * @param string $metaTitle Meta title
     * @param string $lang Language
     * @return $this
     */
    public function setCategoryMetaTitle($metaTitle, $lang)
    {
        $this->meta[ 'title' ][ $lang ] = $metaTitle;

        return $this;
    }

    /**
     * Set meta keywords to category node.
     *
     * @param string $metaKeywords Meta keywords
     * @param string $lang Language
     * @return $this
     */
    public function setCategoryMetaKeywords($metaKeywords, $lang)
    {
        $this->meta[ 'keywords' ][ $lang ] = $metaKeywords;

        return $this;
    }

    /**
     * Append category node to node categories.
     *
     * @return void
     */
    public function appendCategory()
    {
        $this->createNodeName();
        $this->createNodeDescription();
        $this->createNodeMeta();

        $this->categories->appendChild($this->category);

        $this->category = null;
        $this->name = [];
        $this->description = [];
        $this->meta = [];
    }

    /**
     * Create category node.
     *
     * @return void
     */
    private function createNodeCategory()
    {
        if (!$this->category) {
            $this->category = $this->dom->createElement('category');
        }
    }

    /**
     * Create category node "name".
     *
     * @return void
     */
    private function createNodeName()
    {
        $nodeName = $this->dom->createElement('name');

        foreach ($this->name as $lang => $name) {
            $nodeName->appendChild($this->dom->createElement($lang, $name));
        }

        $this->category->appendChild($nodeName);
    }

    /**
     * Create category node "meta".
     *
     * @return void
     */
    private function createNodeMeta()
    {
        $meta = $this->dom->createElement('meta');

        $title = $this->dom->createElement('title');
        $description = $this->dom->createElement('description');
        $keywords = $this->dom->createElement('keywords');

        foreach ($this->meta[ 'title' ] as $lang => $metaTitle) {
            $title->appendChild($this->dom->createElement($lang, $metaTitle));
        }

        foreach ($this->meta[ 'keywords' ] as $lang => $metaKeywords) {
            $keywords->appendChild($this->dom->createElement($lang, $metaKeywords));
        }

        foreach ($this->meta[ 'description' ] as $lang => $metaDescription) {
            $description->appendChild($this->dom->createElement($lang, $metaDescription));
        }

        $meta->appendChild($title);
        $meta->appendChild($description);
        $meta->appendChild($keywords);

        $this->category->appendChild($meta);
    }

    /**
     * Create category node "description".
     *
     * @return void
     */
    private function createNodeDescription()
    {
        $nodeDescription = $this->dom->createElement('description');

        foreach ($this->description as $lang => $description) {
            $nodeDescription->appendChild($this->dom->createElement($lang, $description));
        }

        $this->category->appendChild($nodeDescription);
    }

    /**
     * Save xml.
     *
     * @param string $path Path save file
     * @return void
     */
    public function save($path)
    {
        $this->dom->appendChild($this->categories);
        $this->dom->save($path);
    }
}