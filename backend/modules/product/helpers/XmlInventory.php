<?php

namespace backend\modules\product\helpers;

use DOMDocument;

class XmlInventory
{
    /**
     * Root.
     */
    private $dom;

    /**
     * Node inventories.
     */
    private $inventories;

    /**
     * Node inventory.
     */
    private $inventory;

    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct($version, $encoding = 'UTF-8')
    {
        $this->dom = new DOMDocument($version, $encoding);
        $this->inventories = $this->dom->createElement('inventories');
    }

    /**
     * Set inventory id to inventory node.
     *
     * @param int $id Inventory id
     * @return $this
     */
    public function setInventoryId($id)
    {
        if (!$this->inventory) {
            $this->createNodeInventory();
        }

        $this->inventory->appendChild($this->dom->createElement('id', $id));

        return $this;
    }

    /**
     * Set inventory count to inventory node.
     *
     * @param int $count Inventory count
     * @return $this
     */
    public function setInventoryCount($count)
    {
        if (!$this->inventory) {
            $this->createNodeInventory();
        }

        $this->inventory->appendChild($this->dom->createElement('count', $count));

        return $this;
    }

    /**
     * Set inventory createdAt to inventory node.
     *
     * @param string $createdAt Inventory created_at
     * @return $this
     */
    public function setInventoryCreatedAt($createdAt)
    {
        if (!$this->inventory) {
            $this->createNodeInventory();
        }

        $this->inventory->appendChild($this->dom->createElement('createdAt', $createdAt));

        return $this;
    }

    /**
     * Set inventory updatedAt to inventory node.
     *
     * @param string $updatedAt Inventory updated_at
     * @return $this
     */
    public function setInventoryUpdatedAt($updatedAt)
    {
        if (!$this->inventory) {
            $this->createNodeInventory();
        }

        $this->inventory->appendChild($this->dom->createElement('updatedAt', $updatedAt));

        return $this;
    }

    /**
     * Set inventory product variation to inventory node.
     *
     * @param id $productVariationId Product variation id
     * @param string $productVariationName Product variation name
     * @return $this
     */
    public function setInventoryProductVariation($productVariationId, $productVariationName)
    {
        if (!$this->inventory) {
            $this->createNodeInventory();
        }

        $nodeProductVariation = $this->createNodeProductVariation();

        $nodeProductVariation->appendChild($this->dom->createElement('id', $productVariationId));
        $nodeProductVariation->appendChild($this->dom->createElement('name', $productVariationName));

        $this->inventory->appendChild($nodeProductVariation);

        return $this;
    }

    /**
     * Append inventory node to node inventories.
     *
     * @return void
     */
    public function appendInventory()
    {

        $this->inventories->appendChild($this->inventory);

        $this->inventory = null;
    }

    /**
     * Create inventory node.
     *
     * @return void
     */
    private function createNodeInventory()
    {
        if (!$this->inventory) {
            $this->inventory = $this->dom->createElement('inventory');
        }
    }

    /**
     * Create product variation node.
     *
     * @return object
     */
    private function createNodeProductVariation()
    {
        return $this->dom->createElement('productVariation');
    }

    /**
     * Save xml.
     *
     * @param string $path Path save file
     * @return void
     */
    public function save($path)
    {
        $this->dom->appendChild($this->inventories);
        $this->dom->save($path);
    }
}