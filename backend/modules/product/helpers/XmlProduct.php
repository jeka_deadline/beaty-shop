<?php

namespace backend\modules\product\helpers;

use DOMDocument;

/**
 * Export xml products.
 */
class XmlProduct extends BaseXml
{
    /**
     * {@inheritdoc}
     */
    protected $itemNodeName = 'product';

    /**
     * {@inheritdoc}
     */
    protected $itemsNodeName = 'products';

    /**
     * Node meta.
     *
     * @var array $meta
     */
    protected $meta;

    /**
     * Add product vategories node.
     *
     * @param array $categories Product categories list.
     * @return $this
     */
    public function setCategories($categories)
    {
        if ($this->isEmptyItemNode()) {
            $this->createItemNode();
        }

        $categoriesNode = $this->dom->createElement('categories');

        foreach ($categories as $category) {
            $categoryNode = $this->dom->createElement('category');

            $categoryNode->appendChild($this->dom->createElement('id', $category[ 'id' ]));
            $categoryNode->appendChild($this->dom->createElement('name', $category[ 'name' ]));

            $categoriesNode->appendChild($categoryNode);

            unset($categoryNode);
        }

        $this->appendChildToItemNode($categoriesNode);

        unset($categoriesNode);

        return $this;
    }

    /**
     * Add meta information.
     *
     * @param array $metaInformation Meta information list with language.
     * @return $this
     */
    public function setMetaInformation($metaInformation)
    {
        if ($this->isEmptyItemNode()) {
            $this->createItemNode();
        }

        foreach ($metaInformation as $attributeName => $information) {
            $methodName = 'set' . $this->attributeNameToCamelCase($attributeName, false);

            if (method_exists($this, $methodName)) {
                foreach ($information as $language => $value) {
                    call_user_func_array([$this, $methodName], [$value, $language]);
                }
            }
        }

        $this->createNodeMeta();

        return $this;
    }

    /**
     * Add product variations node.
     *
     * @param array $productVariations Product variations list.
     * @return $this
     */
    public function setProductVariations($productVariations)
    {
        $variationsNode = $this->createNewDomElement('variations');

        foreach ($productVariations as $product) {
            $variationNode = $this->createNewDomElement('variation');

            $variationNode->appendChild($this->createNewDomElement('vendorCode', $product[ 'vendor_code' ]));
            $variationNode->appendChild($this->createNewDomElement('defaultVariation', $product[ 'defaultVariation' ]));
            $variationNode->appendChild($this->createNewDomElement('count', $product[ 'count' ]));
            $variationNode->appendChild($this->createNewDomElement('price', $product[ 'price' ]));
            $variationNode->appendChild($this->createNewDomElement('promotion', $product[ 'promotion' ]));
            $variationNode->appendChild($this->createNewDomElement('active', $product[ 'active' ]));

            // node name
            $nameNode = $this->createNewDomElement('name');

            foreach ($product[ 'name' ] as $lang => $langName) {
                $langNode = $this->createNewDomElement($lang);
                $langNode->appendChild($this->createNewTextNode($langName));
                $nameNode->appendChild($langNode);
            }

            $variationNode->appendChild($nameNode);
            //end node name

            // node short description
            $shortDescriptionNode = $this->createNewDomElement('shortDescription');

            foreach ($product[ 'short_description' ] as $lang => $langShortDescription) {
                $langNode = $this->createNewDomElement($lang);
                $langNode->appendChild($this->createNewTextNode($langShortDescription));
                $shortDescriptionNode->appendChild($langNode);
            }

            $variationNode->appendChild($shortDescriptionNode);
            //end node short description

            // node description
            $descriptionNode = $this->createNewDomElement('description');

            foreach ($product[ 'description' ] as $lang => $langDescription) {
                $langNode = $this->createNewDomElement($lang);
                $langNode->appendChild($this->createNewTextNode($langDescription));
                $descriptionNode->appendChild($langNode);
            }

            $variationNode->appendChild($descriptionNode);
            //end node description

            // node images
            $imagesNode = $this->createNewDomElement('images');

            foreach ($product[ 'images' ] as $image) {
                $imageNode = $this->createNewDomElement('image');

                $imageNode->appendChild($this->createNewDomElement('path', $image[ 'path' ]));
                $imageNode->appendChild($this->createNewDomElement('sort', $image[ 'sort' ]));
                $imageNode->appendChild($this->createNewDomElement('isPreview', $image[ 'isPreview' ]));

                $imagesNode->appendChild($imageNode);

                unset($imageNode);
            }
            //end node images

            $variationNode->appendChild($imagesNode);

            unset($imagesNode);

            // node properties
            $propertiesNode = $this->createNewDomElement('properties');

            foreach ($product[ 'properties' ] as $property) {
                $propertyNode = $this->createNewDomElement('property');

                $propertyNode->appendChild($this->createNewDomElement('filter', $property[ 'filter' ]));
                $propertyNode->appendChild($this->createNewDomElement('productCategory', $property[ 'product_category' ]));
                $propertyNode->appendChild($this->createNewDomElement('value', $property[ 'value' ]));

                $propertiesNode->appendChild($propertyNode);

                unset($propertyNode);
            }

            $variationNode->appendChild($propertiesNode);

            unset($propertiesNode);
            // end node properties

            $variationsNode->appendChild($variationNode);

            unset($variationNode);
        }

        $this->appendChildToItemNode($variationsNode);

        unset($variationsNode);

        return $this;
    }

    /**
     * Set meta description to product node.
     *
     * @access protected
     * @param string $metaDescription Meta description
     * @param string $lang Language
     * @return $void
     */
    protected function setMetaDescription($metaDescription, $lang)
    {
        $this->meta[ 'description' ][ $lang ] = $metaDescription;
    }

    /**
     * Set meta title to product node.
     *
     * @access protected
     * @param string $metaTitle Meta title
     * @param string $lang Language
     * @return $void
     */
    protected function setMetaTitle($metaTitle, $lang)
    {
        $this->meta[ 'title' ][ $lang ] = $metaTitle;
    }

    /**
     * Set meta keywords to product node.
     *
     * @access protected
     * @param string $metaKeywords Meta keywords
     * @param string $lang Language
     * @return $void
     */
    protected function setMetaKeywords($metaKeywords, $lang)
    {
        $this->meta[ 'keywords' ][ $lang ] = $metaKeywords;
    }

    /**
     * Create category node "meta".
     *
     * @access protected
     * @return void
     */
    protected function createNodeMeta()
    {
        $meta = $this->createNewDomElement('meta');

        $title = $this->createNewDomElement('title');
        $description = $this->createNewDomElement('description');
        $keywords = $this->createNewDomElement('keywords');

        foreach ($this->meta[ 'title' ] as $lang => $metaTitle) {
            $langNode = $this->createNewDomElement($lang);
            $langNode->appendChild($this->createNewTextNode($metaTitle));
            $title->appendChild($langNode);
        }

        $meta->appendChild($title);

        foreach ($this->meta[ 'keywords' ] as $lang => $metaKeywords) {
            $langNode = $this->createNewDomElement($lang);
            $langNode->appendChild($this->createNewTextNode($metaKeywords));
            $keywords->appendChild($langNode);
        }

        $meta->appendChild($keywords);

        foreach ($this->meta[ 'description' ] as $lang => $metaDescription) {
            $langNode = $this->createNewDomElement($lang);
            $langNode->appendChild($this->createNewTextNode($metaDescription));
            $description->appendChild($langNode);
        }

        $meta->appendChild($description);

        $this->appendChildToItemNode($meta);
    }
}
