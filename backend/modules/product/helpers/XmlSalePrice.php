<?php

namespace backend\modules\product\helpers;

use DOMDocument;

class XmlSalePrice
{
    /**
     * Root.
     */
    private $dom;

    /**
     * Node prices.
     */
    private $prices;

    /**
     * Node price.
     */
    private $price;

    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct($version, $encoding = 'UTF-8')
    {
        $this->dom = new DOMDocument($version, $encoding);
        $this->prices = $this->dom->createElement('prices');
    }

    /**
     * Set sale price id to price node.
     *
     * @param int $id Sale price id
     * @return $this
     */
    public function setSalePriceId($id)
    {
        if (!$this->price) {
            $this->createNodePrice();
        }

        $this->price->appendChild($this->dom->createElement('id', $id));

        return $this;
    }

    /**
     * Set price to price node.
     *
     * @param float $price Sale item price
     * @return $this
     */
    public function setSalePrice($price)
    {
        if (!$this->price) {
            $this->createNodePrice();
        }

        $this->price->appendChild($this->dom->createElement('price', $price));

        return $this;
    }

    /**
     * Set price createdAt to price node.
     *
     * @param string $createdAt Price created_at
     * @return $this
     */
    public function setSalePriceCreatedAt($createdAt)
    {
        if (!$this->price) {
            $this->createNodePrice();
        }

        $this->price->appendChild($this->dom->createElement('createdAt', $createdAt));

        return $this;
    }

    /**
     * Set price updatedAt to price node.
     *
     * @param string $updatedAt Price updated_at
     * @return $this
     */
    public function setSalePriceUpdatedAt($updatedAt)
    {
        if (!$this->price) {
            $this->createNodePrice();
        }

        $this->price->appendChild($this->dom->createElement('updatedAt', $updatedAt));

        return $this;
    }

    /**
     * Set price product variation to price node.
     *
     * @param id $productVariationId Product variation id
     * @param string $productVariationName Product variation name
     * @return $this
     */
    public function setSalePriceProductVariation($productVariationId, $productVariationName)
    {
        if (!$this->price) {
            $this->createNodePrice();
        }

        $nodeProductVariation = $this->createNodeProductVariation();

        $nodeProductVariation->appendChild($this->dom->createElement('id', $productVariationId));
        $nodeProductVariation->appendChild($this->dom->createElement('name', $productVariationName));

        $this->price->appendChild($nodeProductVariation);

        return $this;
    }

    /**
     * Append price node to node prices.
     *
     * @return void
     */
    public function appendItemPrice()
    {

        $this->prices->appendChild($this->price);

        $this->price = null;
    }

    /**
     * Create price node.
     *
     * @return void
     */
    private function createNodePrice()
    {
        if (!$this->price) {
            $this->price = $this->dom->createElement('price');
        }
    }

    /**
     * Create product variation node.
     *
     * @return object
     */
    private function createNodeProductVariation()
    {
        return $this->dom->createElement('productVariation');
    }

    /**
     * Save xml.
     *
     * @param string $path Path save file
     * @return void
     */
    public function save($path)
    {
        $this->dom->appendChild($this->prices);
        $this->dom->save($path);
    }
}