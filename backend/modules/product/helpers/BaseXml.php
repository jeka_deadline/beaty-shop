<?php

namespace backend\modules\product\helpers;

use DOMDocument;

/**
 * Base xml creator
 */
class BaseXml
{
    /**
     * Root node.
     *
     * @var DOMDocument $dom
     */
    protected $dom;

    /**
     * Item node.
     *
     * @var DOMElement $itemNode
     */
    protected $itemNode;

    /**
     * Items node.
     *
     * @var DOMElement $itemsNode
     */
    protected $itemsNode;

    /**
     * Name item node.
     *
     * @var string $itemNodeName
     */
    protected $itemNodeName = 'item';

    /**
     * Name items node.
     *
     * @var string $itemNodeName
     */
    protected $itemsNodeName = 'items';

    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct($version, $encoding = 'UTF-8')
    {
        $this->dom = new DOMDocument($version, $encoding);
        $this->createMainNode();
    }

    /**
     * Set simple model attributees.
     *
     * @param array|object $item
     * @param array $listAttributes Add list attributes to item node.
     * @return $this
     */
    public function setSimpleAttributes($item, $listAttributes)
    {
        if ($this->isEmptyItemNode()) {
            $this->createItemNode();
        }

        $itemAsObject = is_array($item) ? false : true;

        foreach ($listAttributes as $attribute) {
            if ($itemAsObject) {
                $this->appendChildToItemNode($this->dom->createElement($this->attributeNameToCamelCase($attribute), $item->{$attribute}));
            } else {
                $this->appendChildToItemNode($this->dom->createElement($this->attributeNameToCamelCase($attribute), $item[ $attribute ]));
            }
        }

        return $this;
    }

    /**
     * Save xml.
     *
     * @param string $path Path save file
     * @return void
     */
    public function saveAsFile($fileName)
    {
        $this->dom->appendChild($this->items);
        $this->dom->save($fileName);
    }

    /**
     * Append item node to main node.
     *
     * @return this
     */
    public function appendItemToMainNode()
    {
        if (!$this->isEmptyItemNode()) {
            $this->items->appendChild($this->item);

            $this->item = '';
        }

        return $this;
    }

    /**
     * Modify attribute to CamelCase or camelCase.
     *
     * @access protected
     * @param string $string Attribute.
     * @param bool $capitalizeFirstCharacter If need modify attribute name to format camelCase.
     * @return string
     */
    protected function attributeNameToCamelCase($string, $capitalizeFirstCharacter = true)
    {

        $str = str_replace(' ', '', ucwords(str_replace('_', ' ', $string)));

        if ($capitalizeFirstCharacter) {
            $str[ 0 ] = mb_strtolower($str[ 0 ], 'utf-8');
        }

        return $str;
    }

    /**
     * Create item node.
     *
     * @access protected
     * @return void
     */
    protected function createItemNode()
    {
        $this->item = $this->dom->createElement($this->itemNodeName);
    }

    /**
     * Check is empty item node.
     *
     * @access protected
     * @return bool
     */
    protected function isEmptyItemNode()
    {
        return empty($this->item);
    }

    /**
     * Append node to item node.
     *
     * @access protected
     * @param DOMElement $child Node child element.
     * @return void
     */
    protected function appendChildToItemNode($child)
    {
        $this->item->appendChild($child);
    }

    /**
     * Create main node.
     *
     * @access protected
     * @return void
     */
    protected function createMainNode()
    {
        $this->items = $this->dom->createElement($this->itemsNodeName);
    }

    /**
     * Create node.
     *
     * @access protected
     * @param string $name Name node.
     * @param string $value Node value.
     * @return DOMElement
     */
    protected function createNewDomElement($name, $value = null)
    {
        if (is_null($value)) {
            return $this->dom->createElement($name);
        }
        if (empty($value)) {
            $node = $this->dom->createElement($name);
            $node->appendChild($this->createNewTextNode( '' ));

            return $node;
        }

        $value = strtr($value, [
            '"' => '&quot;',
            '\'' => '&apos;',
            '&' => '&amp;'
        ]);

        return $this->dom->createElement($name, $value);
    }

    /**
     * Create text node.
     *
     * @access protected
     * @param string $value Text node value.
     * @return DOMText
     */
    protected function createNewTextNode($value = null)
    {
        $value = strtr($value, [
            '"' => '&quot;',
            '\'' => '&apos;',
            '&' => '&amp;'
        ]);

        return $this->dom->createTextNode($value);
    }
}
