<?php

namespace backend\modules\product\controllers;

use backend\modules\core\models\Language;
use backend\modules\product\models\forms\UploadImagesProductVariationForm;
use backend\modules\product\models\ProductImage;
use backend\modules\product\models\ProductInventorie;
use backend\modules\product\models\ProductPrice;
use backend\modules\product\models\ProductPromotion;
use backend\modules\product\models\searchModels\ProductVariationSearch;
use backend\modules\product\models\searchModels\ProductVariationStockSearch;
use Yii;
use backend\modules\product\models\ProductVariation;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\helpers\Url;

/**
 * ProductPropertieController implements the CRUD actions for ProductPropertie model.
 */
class ProductVariationController extends Controller
{

    private $product_id = null;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \developeruz\db_rbac\behaviors\AccessBehavior::className(),
                'login_url' => Yii::$app->user->loginUrl,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ProductPropertie models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductVariationSearch();
        $searchModel->product_id = $this->product_id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Url::remember();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'product_id' => $this->product_id,
        ]);
    }

    /**
     * Displays a single ProductPropertie model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            'product_id' => $this->product_id,
        ]);
    }

    public function actionDuplicate($id)
    {
        $model = $this->findModel($id);

        $newModel = new ProductVariation();

        $newModel->attributes = $model->attributes();

        $transaction = Yii::$app->db->beginTransaction();

        if ($newModel->validate() && $newModel->save()) {
            $newModelPrice = new ProductPrice();
            $newModelInventorie = new ProductInventorie();

            $newModelPrice->attributes = $model->price->attributes;
            $newModelInventorie->attributes = $model->inventorie->attributes;
        }
    }

    /**
     * Creates a new ProductPropertie model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $modelVariation = new ProductVariation();
        $modelInventorie = new ProductInventorie();
        $modelPrice = new ProductPrice();
        $modelPromotion = new ProductPromotion();

        $languages = Language::find()->all();

        if (
            $modelVariation->load(Yii::$app->request->post())
            && $modelVariation->save()
            && $modelPrice->load(Yii::$app->request->post())
            && $modelPromotion->load(Yii::$app->request->post())
            && $modelInventorie->load(Yii::$app->request->post())
        ) {
            $modelVariation->link('price',$modelPrice);
            if ($modelPromotion->price) $modelVariation->link('promotion',$modelPromotion);
            $modelVariation->link('inventorie',$modelInventorie);

            return $this->redirect(['index', 'product_id' => $this->product_id]);
        }

        $modelVariation->active = true;

        return $this->render('create', [
            'modelVariation' => $modelVariation,
            'product_id' => $this->product_id,
            'languages' => $languages,
            'modelInventorie' => $modelInventorie,
            'modelPrice' => $modelPrice,
            'modelPromotion' => $modelPromotion,
        ]);
    }

    /**
     * Updates an existing ProductPropertie model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $modelVariation = $this->findModel($id);

        $formUploadImages = new UploadImagesProductVariationForm();
        $formUploadImages->setProductVariation($modelVariation);

        $languages = Language::find()->all();

        if (!$modelVariation->price) {
            $modelVariation->price = new ProductPrice();
        }

        if (!$modelVariation->promotion) {
            $modelVariation->promotion = new ProductPromotion();
        }

        if (!$modelVariation->inventorie) {
            $modelVariation->inventorie = new ProductInventorie();
        }

        if (
            $modelVariation->load(Yii::$app->request->post())
            && $modelVariation->save()
            && $modelVariation->price
            && $modelVariation->price->load(Yii::$app->request->post())
            && $modelVariation->inventorie
            && $modelVariation->inventorie->load(Yii::$app->request->post())
        ) {
            $modelVariation->link('price',$modelVariation->price);
            $modelVariation->link('inventorie',$modelVariation->inventorie);

            if ($modelVariation->promotion && $modelVariation->promotion->load(Yii::$app->request->post())) {
                if ($modelVariation->promotion->price) {
                    $modelVariation->link('promotion',$modelVariation->promotion);
                } else {
                    $modelVariation->promotion->delete();
                }
            }

            $formUploadImages->imageFiles = UploadedFile::getInstances($formUploadImages, 'imageFiles');
            if ($formUploadImages->upload()) {
                return (($url = Url::previous()))?$this->redirect($url):$this->redirect(['index', 'product_id' => $this->product_id]);
            }
        }

        return $this->render('update', [
            'modelVariation' => $modelVariation,
            'product_id' => $this->product_id,
            'languages' => $languages,
            'modelInventorie' => $modelVariation->inventorie,
            'modelPrice' => $modelVariation->price,
            'modelPromotion' => $modelVariation->promotion,
            'formUploadImages' => $formUploadImages,
        ]);
    }

    public function actionImageUpload() {
        if (Yii::$app->request->isAjax && ($product_variation_id = Yii::$app->request->post('product_variation_id', null))) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $modelVariation = $this->findModel($product_variation_id);
            $formUploadImages = new UploadImagesProductVariationForm();
            $formUploadImages->setProductVariation($modelVariation);
            $formUploadImages->imageFiles = UploadedFile::getInstances($formUploadImages, 'imageFiles');
            if ($formUploadImages->upload()) {
                return [];
            }
            return false;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionImageDelete() {
        if (Yii::$app->request->isAjax && ($key = Yii::$app->request->post('key', null))) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $modelImage = $this->findModelImage($key);

            if ($modelImage->delete()) {
                return [];
            }
            return false;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionImageSort() {
        if (Yii::$app->request->isAjax && ($data = Yii::$app->request->post('data', null))) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $image_ids = ArrayHelper::getColumn($data,'key');
            if (!($modelImages = ProductImage::findAll($image_ids))) return false;
            $modelImages = ArrayHelper::map($modelImages, 'id', function ($model) {
                return $model;
            });

            foreach ($data as $key => $image) {
                if (isset($image['key'])) {
                    $modelImages[$image['key']]->sort = $key;
                    $modelImages[$image['key']]->save();
                }
            }
            return true;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Show product variations with count.
     *
     * @return mixed
     */
    public function actionStock()
    {
        $searchModel = new ProductVariationStockSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Url::remember();

        return $this->render('stock', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionChangeCount()
    {
        $model = ProductVariation::find()
            ->where(['id' => Yii::$app->request->post('pk')])
            ->with('inventorie')
            ->one();

        if (!$model || !$model->inventorie) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $model->inventorie->count = Yii::$app->request->post('value', 0);
        $model->inventorie->save();
    }

    /**
     * Deletes an existing ProductPropertie model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index', 'product_id' => $this->product_id]);
    }

    /**
     * Finds the ProductPropertie model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProductVariation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProductVariation::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findModelImage($id)
    {
        if (($model = ProductImage::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


    public function beforeAction($action)
    {
        $this->product_id = Yii::$app->request->get('product_id', null);
        return parent::beforeAction($action);
    }
}
