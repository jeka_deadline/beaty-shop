<?php

namespace backend\modules\product\controllers;

use Yii;
use backend\modules\product\models\Order;
use backend\modules\product\models\searchModels\OrderSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use common\helpers\Payone;
use backend\modules\product\models\OrderShippingInformation;
use kartik\mpdf\Pdf;
use yii\helpers\ArrayHelper;
use backend\modules\core\models\TrainerDistributor;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \developeruz\db_rbac\behaviors\AccessBehavior::className(),
                'login_url' => Yii::$app->user->loginUrl,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $listDistributors = ArrayHelper::map(TrainerDistributor::find()->where(['type' => TrainerDistributor::TYPE_DISTRUBUTOR])->all(), 'id', function($model){ return $model->first_name . ' ' . $model->last_name;});

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'listDistributors' => $listDistributors,
        ]);
    }

    /**
     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        $productDataProvider = new ActiveDataProvider([
            'query' => $model->getOrderProducts(),
        ]);

        if ($model->is_new) {
            $model->resetFlagNew();
        }

        return $this->render('view', compact('model', 'productDataProvider'));
    }

    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Order();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->credit_txid) {
                try {
                    $status = Payone::sendCapture($model);
                } catch (\Exception $e) {
                    throw new \Exception($e->getMessage());
                }

                if ($status[ 'status' ] === 'APPROVED') {
                    $model->status = Order::ORDER_STATUS_PAID;
                    $model->credit_txid = null;
                } else {
                    throw new \Exception($status[ 'errormessage' ]);
                }
            }

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Order model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Caputy order from payment type credit card.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws Exception if response caputy has errors.
     */
    public function actionCaputy($id)
    {
        $model = Order::findOne([
            'id' => $id,
            'status' => Order::ORDER_STATUS_PREAUTORIZATION,
        ]);

        if (!$model) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        try {
            $status = Payone::sendCapture($model);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

        if ($status[ 'status' ] === 'APPROVED') {
            $model->status = Order::ORDER_STATUS_PAID;
            $model->credit_txid = null;
            $model->save();

            return $this->redirect(['view', 'id' => $model->id]);
        }

        throw new \Exception($status[ 'errormessage' ]);
    }

    public function actionPrintShippingDocument($orderShippingId)
    {
        $orderShipping = OrderShippingInformation::findOne($orderShippingId);

        if (!$orderShipping) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        if (empty($orderShipping->order->print_shipping_date)) {
            $orderShipping->order->updateAttributes(['print_shipping_date' => date('Y-m-d H:i:s')]);
        }

        return $this->renderPartial('print_order_shipping', compact('orderShipping'));
    }

    /**
     * Display user account page.
     *
     * @return mixed
     */
    public function actionPrintLs($orderId)
    {
        $modelOrder = Order::find()
            ->with('orderProducts', 'orderProducts.productVariation', 'orderShippingInformation', 'orderBillingInformation')
            ->where([
                'id' => (int)$orderId,
            ])
            ->one();

        if ($modelOrder === null) {
            throw new NotFoundHttpException("Not order found", 1);
        }

        if (empty($modelOrder->print_ls_date)) {
            $modelOrder->updateAttributes(['print_ls_date' => date('Y-m-d H:i:s')]);
        }

        $content = $this->renderPartial('ls', compact('modelOrder'));

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'format' => Pdf::FORMAT_A4,
            'cssFile' => Yii::getAlias('@frontend/web/css/pdf.css'),
            'destination' => Pdf::DEST_BROWSER,
            'marginBottom' => 37,
            'content' => $content,
            'options' => ['title' => 'Order Report'],
            'methods' => [
                'SetFooter' => $this->renderPartial('pdf-footer'),
            ],
        ]);

        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;

        return $pdf->render();
    }

    /**
     * Display user invoice.
     *
     * @return mixed
     */
    public function actionPrintInvoice($orderId)
    {
        $modelOrder = Order::find()
            ->with('orderProducts', 'orderProducts.productVariation', 'orderShippingInformation', 'orderBillingInformation')
            ->where([
                'id' => (int)$orderId,
            ])
            ->one();

        if ($modelOrder === null) {
            throw new NotFoundHttpException("Not order found", 1);
        }

        if (empty($modelOrder->print_invoice_date)) {
            $modelOrder->updateAttributes(['print_invoice_date' => date('Y-m-d H:i:s')]);
        }

        $content = $this->renderPartial('user-invoice', compact('modelOrder'));

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'format' => Pdf::FORMAT_A4,
            'cssFile' => Yii::getAlias('@frontend/web/css/pdf.css'),
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,
            'options' => ['title' => 'Order Report'],
            'methods' => [
                'SetFooter' => $this->renderPartial('pdf-footer'),
            ],
        ]);

        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;

        return $pdf->render();
    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
