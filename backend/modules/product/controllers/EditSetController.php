<?php

namespace backend\modules\product\controllers;

use backend\modules\core\models\Language;
use backend\modules\product\models\forms\UploadImagesProductVariationForm;
use backend\modules\product\models\forms\ValidateSetItemForm;
use backend\modules\product\models\ProductCategory;
use backend\modules\product\models\ProductInventorie;
use backend\modules\product\models\ProductPrice;
use backend\modules\product\models\ProductPromotion;
use backend\modules\product\models\ProductSetProduct;
use backend\modules\product\models\ProductVariation;
use backend\modules\product\models\searchModels\EditSetSearch;
use Yii;
use backend\modules\product\models\Product;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\web\Response;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class EditSetController extends Controller
{
    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \developeruz\db_rbac\behaviors\AccessBehavior::className(),
                'login_url' => Yii::$app->user->loginUrl,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Product models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EditSetSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', compact(
            'searchModel',
            'dataProvider'
        ));
    }

    /**
     * Displays a single Product model.
     *
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Product();

        $listCategories = ProductCategory::getTreeCategoriesForSelect();

        $modelVariation = new ProductVariation();
        $modelInventorie = new ProductInventorie();
        $modelPrice = new ProductPrice();
        $modelPromotion = new ProductPromotion();

        $languages = Language::find()->all();

        $model->type = Product::TYPE_PRODUCT_EDIT_SET;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $modelVariation->active = $model->active;
            $modelVariation->product_id = $model->id;
            if (
                $modelVariation->load(Yii::$app->request->post())
                && $modelVariation->save()
                && $modelPrice->load(Yii::$app->request->post())
                && $modelPromotion->load(Yii::$app->request->post())
                && $modelInventorie->load(Yii::$app->request->post())
            ) {
                $modelVariation->link('price',$modelPrice);
                if ($modelPromotion->price) $modelVariation->link('promotion',$modelPromotion);
                $modelVariation->link('inventorie',$modelInventorie);

                $this->saveProductSetProducts($model);

                return $this->redirect(['index']);
            }
        }

        $model->active = true;

        return $this->render('create', [
            'model' => $model,
            'listCategories' => $listCategories,
            'modelVariation' => $modelVariation,
            'languages' => $languages,
            'modelInventorie' => $modelInventorie,
            'modelPrice' => $modelPrice,
            'modelPromotion' => $modelPromotion,
        ]);
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $listCategories = ProductCategory::getTreeCategoriesForSelect();

        $languages = Language::find()->all();

        $modelVariation = $this->findVariationModel($model->id);

        $formUploadImages = new UploadImagesProductVariationForm();
        $formUploadImages->setProductVariation($modelVariation);

        if (!$modelVariation->price) {
            $modelVariation->price = new ProductPrice();
        }

        if (!$modelVariation->promotion) {
            $modelVariation->promotion = new ProductPromotion();
        }

        if (!$modelVariation->inventorie) {
            $modelVariation->inventorie = new ProductInventorie();
        }

        if (
            $model->load(Yii::$app->request->post()) && $model->save()
            && $modelVariation->load(Yii::$app->request->post())
            && $modelVariation->save()
            && $modelVariation->price
            && $modelVariation->price->load(Yii::$app->request->post())
            && $modelVariation->inventorie
            && $modelVariation->inventorie->load(Yii::$app->request->post())
        ) {
            $modelVariation->link('price',$modelVariation->price);
            $modelVariation->link('inventorie',$modelVariation->inventorie);

            if ($modelVariation->promotion && $modelVariation->promotion->load(Yii::$app->request->post())) {
                if ($modelVariation->promotion->price) {
                    $modelVariation->link('promotion',$modelVariation->promotion);
                } else {
                    $modelVariation->promotion->delete();
                }
            }

            $this->saveProductSetProducts($model);

            $formUploadImages->imageFiles = UploadedFile::getInstances($formUploadImages, 'imageFiles');
            if ($formUploadImages->upload()) {
                return $this->redirect(['index']);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'listCategories' => $listCategories,
            'modelVariation' => $modelVariation,
            'languages' => $languages,
            'modelInventorie' => $modelVariation->inventorie,
            'modelPrice' => $modelVariation->price,
            'modelPromotion' => $modelVariation->promotion,
            'formUploadImages' => $formUploadImages,
        ]);
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findVariationModel($product_id)
    {
        if (($modelVariation = ProductVariation::find()->where(['product_id' => $product_id])->one()) !== null) {
            return $modelVariation;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionSearchRecommendetProducts($q = null) {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $modelVariations = ProductVariation::find()
                ->alias('default_variations')
                ->with('preview')
                ->leftJoin(Product::tableName().' product', 'product.default_variation_id = default_variations.id')
                ->leftJoin(ProductVariation::tableName().' variations', 'variations.product_id = product.id')
                ->where(['variations.active' => 1])
                ->andWhere(['product.active' => 1])
                ->andWhere(['LIKE', 'variations.name', $q])
                ->orderBy('variations.name')
                ->groupBy('default_variations.product_id')
                ->all();

            $out = [];
            if ($modelVariations) {
                foreach ($modelVariations as $modelVariation) {
                    $out[] = [
                        'id' => $modelVariation->id,
                        'name' => $modelVariation->name,
                        'value' => $modelVariation->name,
                        'image' => ($modelVariation->preview?$modelVariation->preview->getUrlImage():''),
                        'product_id' => $modelVariation->product ? $modelVariation->product->id : null,
                    ];
                }
            }
            return $out;
        }
    }

    public function actionSearchSetProducts($q = null) {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $modelVariations = ProductVariation::find()
                ->alias('default_variations')
                ->with('preview')
                ->leftJoin(Product::tableName().' product', 'product.default_variation_id = default_variations.id')
                ->leftJoin(ProductVariation::tableName().' variations', 'variations.product_id = product.id')
                ->where(['variations.active' => 1])
                ->andWhere(['product.active' => 1])
                ->andWhere(['LIKE', 'variations.name', $q])
                ->orderBy('variations.name')
                ->groupBy('default_variations.product_id')
                ->all();

            $out = [];
            if ($modelVariations) {
                foreach ($modelVariations as $modelVariation) {
                    $product_id = $modelVariation->product ? $modelVariation->product->id : null;
                    $setRelation = new ProductSetProduct();
                    $setRelation->count = 1;
                    $setRelation->display_order = 0;
                    $out[] = [
                        'id' => $modelVariation->id,
                        'name' => $modelVariation->name,
                        'value' => $modelVariation->name,
                        'image' => ($modelVariation->preview?$modelVariation->preview->getUrlImage():''),
                        'product_id' => $product_id,
                        'template' => $this->renderPartial('_item_products', [
                            'product_id' => $product_id,
                            'productVariation' => $modelVariation,
                            'setRelation' => $setRelation,
                        ])
                    ];
                }
            }
            return $out;
        }
    }

    public function actionValidateSetItems () {
        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $formValidate = new ValidateSetItemForm();

            if (
                $formValidate->load(Yii::$app->request->post()) && $formValidate->validate()
                && ($modelProduct = Product::findOne($formValidate->id))
            ) {
                if ($modelProduct->minCountVariations >= $formValidate->count) {
                    return [
                        'status' => 'ok',
                        'errors' => null,
                    ];
                }

                return [
                    'status' => 'error',
                    'errors' => [
                        'count' => [
                            'The specified quantity of the goods is more than presence in a warehouse.'
                        ]
                    ],
                ];
            } else {
                return [
                    'status' => 'error',
                    'errors' => $formValidate->getErrors()
                ];
            }
        }
    }

    private function saveProductSetProducts($modelProduct) {
        if (($productSetProducts = Yii::$app->request->post('ProductSetProduct')) && is_array($productSetProducts)) {
            $modelSetProducts = $modelProduct->productSetRelationProducts;
            foreach ($productSetProducts as $variation_id => $data) {
                if ($modelSetProducts && isset($modelSetProducts[$variation_id])) {
                    $modelSetProducts[$variation_id]->count = $data['count'];
                    $modelSetProducts[$variation_id]->display_order = $data['display_order'];
                    $modelSetProducts[$variation_id]->save();
                }
            }
        }
    }
}
