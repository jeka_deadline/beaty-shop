<?php

namespace backend\modules\product\controllers;

use backend\modules\core\models\Language;
use backend\modules\product\models\ProductCategory;
use backend\modules\product\models\ProductVariation;
use backend\modules\product\models\searchModels\ProductSearch;
use Yii;
use backend\modules\product\models\Product;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\modules\product\models\forms\ExportProductsForm;
use backend\modules\product\models\forms\ImportProductsForm;
use backend\modules\product\models\ProductInventorie;
use yii\web\UploadedFile;
use backend\modules\product\models\ProductPrice;
use backend\modules\product\models\ProductPromotion;
use yii\web\Response;
use yii2tech\spreadsheet\Spreadsheet;
use yii2tech\spreadsheet\SerialColumn;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use backend\modules\product\models\forms\DuplicateCategoryForm;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller
{
    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \developeruz\db_rbac\behaviors\AccessBehavior::className(),
                'login_url' => Yii::$app->user->loginUrl,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Product models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $duplicateToCategoryForm = new DuplicateCategoryForm();

        $exportProductsForm = new ExportProductsForm();
        $importProductsForm = new ImportProductsForm();

        return $this->render('index', compact(
            'searchModel',
            'dataProvider',
            'exportProductsForm',
            'importProductsForm',
            'duplicateToCategoryForm'
        ));
    }

    /**
     * Displays a single Product model.
     *
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Product();

        $listCategories = ProductCategory::getTreeCategoriesForSelect();

        $languages = Language::find()->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        $model->active = true;

        return $this->render('create', compact('model', 'languages', 'listCategories'));
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $listCategories = ProductCategory::getTreeCategoriesForSelect();

        $languages = Language::find()->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', compact('model', 'languages', 'listCategories'));
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Export products.
     *
     * @return index
     */
    public function actionExportProducts()
    {
        $exporter = new Spreadsheet([
            'dataProvider' => new ActiveDataProvider([
                'query' => ProductVariation::find()->with('product', 'product.categories', 'inventorie', 'price', 'promotion', 'images', 'properties', 'properties.filter')
            ]),
            'columns' => [
                [
                    'attribute' => 'product.categories',
                    'value' => function($model) {
                        $content = '';

                        if ($model->product && $model->product->categories) {
                            foreach ($model->product->categories as $category) {
                                $content .= $category->name . PHP_EOL;
                            }
                        }

                        return $content;
                    },
                    'header' => 'Category',
                ],
                'product.slug',
                'name',
                'vendor_code',
                'short_description',
                'description',
                'active',
                'inventorie.count',
                'price.price',
                'promotion.price',
                'product.meta_title',
                'product.meta_keywords',
                'product.meta_description',
                [
                    'attribute' => 'product.default_variation_id',
                    'value' => function($model) {
                        if ($model->product->default_variation_id == $model->id) {
                            return '1';
                        }

                        return '';
                    },
                    'header' => 'Is default',
                ],
                [
                    'attribute' => 'images',
                    'value' => function($model) {
                        $content = '';
                        foreach ($model->images as $image) {
                            $content .= Yii::$app->urlManagerFrontEnd->createAbsoluteUrl($image->getUrlImage()) . PHP_EOL;
                        }

                        return $content;
                    }
                ],
                [
                    'header' => 'Filters',
                    'attribute' => 'properties',
                    'value' => function($model) {
                        $content = '';

                        foreach ($model->properties as $property) {
                            $content .= $property->filter->name . ':' . $property->value . PHP_EOL;
                        }

                        return $content;
                    }
                ]
            ],
        ]);

        $exporter->render();

        $exporter->send(time() . '.xls');
    }

    /**
     * Import products.
     *
     * @return string
     */
    public function actionImportProducts()
    {
        $model = new ImportProductsForm();

        if (Yii::$app->request->isPost) {
            $model->file = UploadedFile::getInstance($model, 'file');

            if ($model->validate()) {

                $result = $model->import($model->file->tempName);

                /*Yii::$app->session->setFlash('success', sprintf('New products: %s', $result[ 'news' ]));

                Yii::$app->session->setFlash('info', sprintf('Updated products: %s', $result[ 'updated' ]));

                $countErrors = $result[ 'errors' ];
                $listErrors = $result[ 'list-errors' ];

                $message = sprintf('Count errors: %s', $result[ 'errors' ]);

                foreach ($listErrors as $error) {
                    $message .= '<br>' . $error;
                }

                Yii::$app->session->setFlash('danger', $message);*/
            }
        }

        return $this->redirect(['index']);
    }

    public function actionMoveToCategory()
    {
        $form = new DuplicateCategoryForm();

        if ($form->load(Yii::$app->request->post())) {
            $form->moveToCategory();
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionSearchRecommendetProducts($q = null) {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $modelVariations = ProductVariation::find()
                ->alias('default_variations')
                ->with('preview')
                ->leftJoin(Product::tableName().' product', 'product.default_variation_id = default_variations.id')
                ->leftJoin(ProductVariation::tableName().' variations', 'variations.product_id = product.id')
                ->where(['variations.active' => 1])
                ->andWhere(['product.active' => 1])
                ->andWhere(['LIKE', 'variations.name', $q])
                ->orderBy('variations.name')
                ->groupBy('default_variations.product_id')
                ->all();

            $out = [];
            if ($modelVariations) {
                foreach ($modelVariations as $modelVariation) {
                    $out[] = [
                        'id' => $modelVariation->id,
                        'name' => $modelVariation->name,
                        'value' => $modelVariation->name,
                        'image' => ($modelVariation->preview?$modelVariation->preview->getUrlImage():''),
                        'product_id' => $modelVariation->product ? $modelVariation->product->id : null,
                    ];
                }
            }
            return $out;
        }
    }
}
