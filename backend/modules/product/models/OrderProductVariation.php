<?php

namespace backend\modules\product\models;

use Yii;
use common\models\product\OrderProductVariation as BaseOrderProductVariation;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * Frontend order product variation extends common order product variation
 */
class OrderProductVariation extends BaseOrderProductVariation
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductVariation()
    {
        return $this->hasOne(ProductVariation::className(), ['id' => 'product_variation_id']);
    }

    public static function getTopProductVariations()
    {
        return self::find()
            ->with('productVariation', 'product')
            ->groupBy('product_variation_id')
            ->orderBy(['COUNT(*)' => SORT_DESC])
            ->limit(20);
    }

    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id'])
            ->viaTable(ProductVariation::tableName(), ['id' => 'product_variation_id']);
    }
}
