<?php

namespace backend\modules\product\models;

use backend\modules\core\behaviors\LangBehavior;
use backend\modules\core\models\Language;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * @property ProductVariationPack[] $packs
 */
class ProductVariation extends \common\models\product\ProductVariation
{
    public $count;

    private $loadProperties = null;
    public $propertieFields = null;
    public $default = null;

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['default'], 'integer'],
                [['propertieFields'], 'safe']
            ]
        );
    }

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                [
                    'class' => LangBehavior::className(),
                    'langModel' => ProductVariationLangField::className(),
                    'modelForeignKey' => 'product_variation_id',
                    'languages' => Language::find()->all(),
                    'attributes' => [
                        'name',
                        'description',
                        'short_description',
                    ],
                ]
            ]
        );
    }

    public function afterFind()
    {
        parent::afterFind();
        $this->default = ($this->defaultProduct && $this->defaultProduct->default_variation_id == $this->id) ? 1 : null;
    }

    public function setPrice($value) {
        $this->price = $value;
    }

    public function setPromotion($value) {
        $this->promotion = $value;
    }

    public function setInventorie($value) {
        $this->inventorie = $value;
    }

    public function getAllProperties()
    {
        if (!($this->product && $this->product->productCategories)) return null;

        $modelCategorieFilters = ProductCategorieRelationFilter::find()
            ->with('filter')
            ->where([
                'product_category_id' => ArrayHelper::map($this->product->productCategories,'id','id')
            ])
            ->indexBy('filter_id')
            ->all();

        if (!($modelCategorieFilters)) return null;

        if ($this->propertieFilters) {
            $listPropertieIds = ArrayHelper::map($this->propertieFilters, 'filter_id', 'filter_id');
        } else {
            $listPropertieIds = [];
        }

        $newProperties = [];
        foreach ($modelCategorieFilters as $modelCategorieFilter) {
            if ($modelCategorieFilter->filter && array_search($modelCategorieFilter->filter->id, $listPropertieIds) === false) {
                $modelPropertie = new ProductVariationPropertie();
                $modelPropertie->filter_id = $modelCategorieFilter->filter->id;
                $modelPropertie->product_variation_id = $this->id;
                $newProperties[] = $modelPropertie;
            }
        }
        return array_merge($this->propertieFilters, $newProperties);
    }

    public function load($data, $formName = null)
    {
        if (!parent::load($data, $formName)) return false;
        $this->loadProperties($data, $formName);
        return true;
    }

    private function loadProperties($data, $formName = null)
    {
        $scope = $formName === null ? $this->formName() : $formName;
        if ($scope === '' && !empty($data)) {
            $this->setLoadProperties($data);
            return true;
        } elseif (isset($data[$scope]) && isset($data[$scope]['propertieFields'])) {
            $this->setLoadProperties($data[$scope]['propertieFields']);
            return true;
        }

        return false;
    }

    private function setLoadProperties($properties) {
        foreach ($properties as $filter_id => $value) {
            $property = new ProductVariationPropertie();
            $property->filter_id = $filter_id;
            $property->product_variation_id = $this->id;
            $property->value = $value;
            $this->loadProperties[$filter_id] = $property;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $this->saveProperties();

        $this->saveDefaultVariationId();
    }

    /**
     * Attacn new filter to product variation.
     *
     * @param \backend\modules\product\models\ProductFilter $filter Product filter model.
     * @param string $value Value filter.
     * @return bool
     */
    public function attachNewFilter($filter, $value)
    {
        if (!$this->product || !$this->product->productCategories) {
            return false;
        }

        $category = $this->product->getProductCategories()->one();

        if (!$category) {
            return false;
        }

        $model = new ProductCategorieRelationFilter();

        $model->product_category_id = $category->id;
        $model->filter_id = $filter->id;

        if (!$model->validate() || !$model->save()) {
            return false;
        }

        $model = new ProductVariationPropertie();

        $model->filter_id = $filter->id;
        $model->product_variation_id = $this->id;
        $model->product_category_id = $category->id;
        $model->value = $value;

        return ($model->validate() && $model->save());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLangFields()
    {
        return $this->hasMany(ProductVariationLangField::className(), ['product_variation_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImages()
    {
        return $this->hasMany(ProductImage::className(), ['product_variation_id' => 'id'])->orderBy(['sort' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventorie()
    {
        return $this->hasOne(ProductInventorie::className(), ['product_variation_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrice()
    {
        return $this->hasOne(ProductPrice::className(), ['product_variation_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromotion()
    {
        return $this->hasOne(ProductPromotion::className(), ['product_variation_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPreview()
    {
        return $this->hasOne(ProductImage::className(), ['product_variation_id' => 'id'])->orderBy(['sort' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperties()
    {
        return $this->hasMany(ProductVariationPropertie::className(), ['product_variation_id' => 'id']);
    }

    public function getGroupProperties()
    {
        return $this->hasMany(ProductVariationPropertie::className(), ['product_variation_id' => 'id'])->groupBy('filter_id');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertieFilters()
    {
        return $this->hasMany(ProductVariationPropertie::className(), ['product_variation_id' => 'id'])->indexBy('filter_id');
    }

    /**
     * @return bool
     */
    private function saveProperties()
    {
        if (is_null($this->loadProperties)) return false;
        if (!($this->product && $this->product->productCategories)) return false;

        $modelCategorieFilters = ProductCategorieRelationFilter::find()
            ->with('filter')
            ->where([
                'product_category_id' => ArrayHelper::map($this->product->productCategories,'id','id')
            ])->all();

        if (!($modelCategorieFilters)) return false;

        foreach ($modelCategorieFilters as $modelCategorieFilter) {
            if (!isset($this->loadProperties[$modelCategorieFilter->filter_id])) continue;
            if (
                ($propertie = $this->filter_search(
                    $modelCategorieFilter->product_category_id,
                    $modelCategorieFilter->filter_id,
                    $this->properties
                ))
            ) {
                $propertie->value = $this->loadProperties[$propertie->filter_id]->value;
                $propertie->value ? $propertie->save() : $propertie->delete();
            } else {
                $propertie = new ProductVariationPropertie();
                $propertie->attributes = $this->loadProperties[$modelCategorieFilter->filter_id]->attributes;
                $propertie->product_category_id = $modelCategorieFilter->product_category_id;
                $propertie->value ? $this->link('properties', $propertie) : null;
            }
        }
        return true;
    }

    private function filter_search($product_category_id, $filter_id, $properties) {
        foreach ($properties as $propertie) {
            if ($propertie->filter_id == $filter_id && $propertie->product_category_id == $product_category_id) return $propertie;
        }
        return false;
    }

    private function saveDefaultVariationId() {
        if (!$this->product) return;
        if ($this->default) {
            $this->product->updateAttributes(['default_variation_id' => $this->id]);
        }
        if (!$this->product->default_variation_id) {
            $this->product->updateAttributes(['default_variation_id' => $this->id]);
        }
    }
}
