<?php

namespace backend\modules\product\models\forms;

use Yii;
use yii\base\Model;
use backend\modules\product\models\Product;
use backend\modules\product\models\ProductVariation;
use backend\modules\product\helpers\XmlProduct;
use yii\helpers\ArrayHelper;
use common\models\core\Language;
use common\models\product\ProductImage;
use yii\helpers\Url;

/**
 * Form for export products.
 */
class ExportProductsForm extends Model
{
    /**
     * Export format.
     *
    * @var string $format
    */
    public $format;

    /**
     * Array products.
     *
     * @var \backend\modules\product\models\Product[] $listProducts
     */
    private $listProducts;

    /**
     * File name for export.
     *
     * @var string
     */
    const FILE_NAME = 'products';

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return [
            ['format', 'in', 'range' => [Product::EXPORT_XLS]],
        ];
    }

    /**
     * Hash map languages by code-code.
     *
     * @access private
     * @var array $languages
     */
    private $languages;

    /**
     * Export categories.
     *
     * @return void
     */
    public function export()
    {
        $this->languages = ArrayHelper::map(Language::find()->all(), 'code', 'code');

        $this->listProducts = Product::find()
            ->alias('t')
            ->select(['t.id', 't.slug', 't.active', 'rating', 't.meta_title', 't.meta_description', 't.meta_keywords', 't.default_variation_id'])
            ->with([
                'langFields' => function($query) {
                    return $query->select(['meta_title', 'meta_description', 'meta_keywords', 'lang_id', 'product_id']);
                },
                'langFields.lang' => function($query) {
                    return $query->select(['id', 'code']);
                },
                'categories' => function($query) {
                    return $query->select(['id', 'name']);
                },
                'defaultProductVariation' => function($query) {
                    return $query->select(['id', 'product_id', 'name']);
                },
                'productVariations' => function($query) {
                    return $query->select(['id', 'product_id', 'name', 'vendor_code', 'short_description', 'description', 'active']);
                },
                'productVariations.langFields' => function($query) {
                    return $query->select(['id', 'product_variation_id', 'name', 'short_description', 'description', 'lang_id']);
                },
                'productVariations.langFields.lang' => function($query) {
                    return $query->select(['id', 'code']);
                },
                'productVariations.properties' => function($query) {
                    return $query->select(['id', 'value', 'filter_id', 'product_variation_id', 'product_category_id']);
                },
                'productVariations.properties.filter' => function($query) {
                    return $query->select(['id', 'name']);
                },
                'productVariations.properties.productCategory' => function($query) {
                    return $query->select(['id', 'name']);
                },
                'productVariations.images' => function($query) {
                    return $query->select(['id', 'product_variation_id', 'filename', 'sort', 'preview']);
                },
                'productVariations.inventorie' => function($query) {
                    return $query->select(['id', 'product_variation_id', 'count']);
                },
                'productVariations.price' => function($query) {
                    return $query->select(['id', 'product_variation_id', 'price']);
                },
                'productVariations.promotion' => function($query) {
                    return $query->select(['id', 'product_variation_id', 'price']);
                }
            ])
            ->asArray()
            ->all();

        return $this->exportXml();
    }

    /**
     * List export types.
     *
     * @return array
     */
    public function getExportFormats()
    {
        return [
            Product::EXPORT_XLS => 'XLS',
        ];
    }

    /**
     * Export products to xml.
     *
     * @access private
     * @return void
     */
    private function exportXml()
    {
        $xml = new XmlProduct('1.0');

        foreach ($this->listProducts as $itemProduct) {

            $metaInformation = $this->getProductMetaInformation($itemProduct);
            $defaultVariationName = (isset($itemProduct[ 'defaultProductVariation' ][ 'name' ]) ? $itemProduct[ 'defaultProductVariation' ][ 'name' ] : null);
            $productVariations = $this->getProductVariations($itemProduct[ 'productVariations' ], $defaultVariationName);

            $xml->setSimpleAttributes($itemProduct, ['slug', 'active', 'rating'])
                ->setCategories($itemProduct[ 'categories' ])
                ->setMetaInformation($metaInformation)
                ->setProductVariations($productVariations)
                ->appendItemToMainNode();
        }

        $fileXml = Yii::getAlias('@app/runtime/' . self::FILE_NAME . '.xml');

        if (!file_exists($fileXml)) {
            touch($fileXml, 0777);
        }

        $xml->saveAsFile($fileXml);

        return $fileXml;
    }

    /**
     * Build meta information structure,
     *
     * @access private
     * @param array|object $itemProduct Product item array attributes or Product item object.
     * @return array
     */
    private function getProductMetaInformation($itemProduct)
    {
        $metaInformation = [
            'meta_title' => [
                'defaultLanguage' => $itemProduct[ 'meta_title' ],
            ],
            'meta_description' => [
                'defaultLanguage' => $itemProduct[ 'meta_description' ],
            ],
            'meta_keywords' => [
                'defaultLanguage' => $itemProduct[ 'meta_keywords' ],
            ],
        ];

        foreach ($itemProduct[ 'langFields' ] as $langAttributes) {
            $metaInformation[ 'meta_title' ][ $langAttributes[ 'lang' ][ 'code'  ] ] = $langAttributes[ 'meta_title' ];
            $metaInformation[ 'meta_keywords' ][ $langAttributes[ 'lang' ][ 'code'  ] ] = $langAttributes[ 'meta_keywords' ];
            $metaInformation[ 'meta_description' ][ $langAttributes[ 'lang' ][ 'code'  ] ] = $langAttributes[ 'meta_description' ];
        }

        foreach ($this->languages as $languageCode) {
            if (!isset($metaInformation[ 'meta_title' ][ $languageCode ])) {
                $metaInformation[ 'meta_title' ][ $languageCode ] = '';
            }

            if (!isset($metaInformation[ 'meta_keywords' ][ $languageCode ])) {
                $metaInformation[ 'meta_keywords' ][ $languageCode ] = '';
            }

            if (!isset($metaInformation[ 'meta_description' ][ $languageCode ])) {
                $metaInformation[ 'meta_description' ][ $languageCode ] = '';
            }
        }

        return $metaInformation;
    }

    /**
     * Build product variations structure,
     *
     * @access private
     * @param array $productVariations List product variations array attributes.
     * @param int $defaultVariationName Default product variation name.
     * @return array
     */
    private function getProductVariations($productVariations, $defaultVariationName)
    {
        $products = [];

        foreach ($productVariations as $index => $product) {
            $products[ $index ] = [
                'vendor_code' => $product[ 'vendor_code' ],
                'name' => [],
                'short_description' => [],
                'description' => [],
                'images' => [],
                'properties' => [],
                'count' => (isset($product[ 'inventorie' ][ 'count' ])) ? $product[ 'inventorie' ][ 'count' ] : 0,
                'price' => (isset($product[ 'price' ][ 'price' ])) ? $product[ 'price' ][ 'price' ] : 0,
                'promotion' => (isset($product[ 'promotion' ][ 'price' ])) ? $product[ 'promotion' ][ 'price' ] : 0,
                'active' => $product[ 'active' ],
                'defaultVariation' => ($product[ 'name' ] == $defaultVariationName) ? 1 : 0,
            ];

            $products[ $index ][ 'name' ][ 'defaultLanguage' ] = $product[ 'name' ];
            $products[ $index ][ 'short_description' ][ 'defaultLanguage' ] = $product[ 'short_description' ];
            $products[ $index ][ 'description' ][ 'defaultLanguage' ] = $product[ 'description' ];

            foreach ($product[ 'langFields' ] as $langAttributes) {
                $products[ $index ][ 'name' ][ $langAttributes[ 'lang' ][ 'code'  ] ] = $langAttributes[ 'name' ];
                $products[ $index ][ 'short_description' ][ $langAttributes[ 'lang' ][ 'code'  ] ] = $langAttributes[ 'short_description' ];
                $products[ $index ][ 'description' ][ $langAttributes[ 'lang' ][ 'code'  ] ] = $langAttributes[ 'description' ];
            }

            foreach ($this->languages as $languageCode) {
                if (!isset($products[ $index ][ 'name' ][ $languageCode ])) {
                    $products[ $index ][ 'name' ][ $languageCode ] = '';
                }

                if (!isset($products[ $index ][ 'short_description' ][ $languageCode ])) {
                    $products[ $index ][ 'short_description' ][ $languageCode ] = '';
                }

                if (!isset($products[ $index ][ 'description' ][ $languageCode ])) {
                    $products[ $index ][ 'description' ][ $languageCode ] = '';
                }
            }

            foreach ($product[ 'properties' ] as $property) {
                $products[ $index ][ 'properties' ][] = [
                    'filter' => $property[ 'filter' ][ 'name' ],
                    'product_category' => $property[ 'productCategory' ][ 'name' ],
                    'value' => $property[ 'value' ],
                ];
            }

            $imagePath = ProductImage::$path;

            foreach ($product[ 'images' ] as $image) {
                $products[ $index ][ 'images' ][] = [
                    'path' => Url::to($imagePath . $image[ 'filename' ], true),
                    'sort' => $image[ 'sort' ],
                    'isPreview' => $image[ 'preview' ],
                ];
            }
        }

        return $products;
    }
}
