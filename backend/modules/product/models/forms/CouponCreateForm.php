<?php

namespace backend\modules\product\models\forms;

use Yii;
use yii\base\Model;
use backend\modules\product\models\Coupon;

/**
 * Form to create coupons.
 */
class CouponCreateForm extends Model
{
    /**
     * @var integer $count Count create coupons
     */
    public $count;

    /**
     * @var string $type Type coupon
     */
    public $type;

    /**
     * @var integer $value Value coupon
     */
    public $value;

    /**
     * @var boolean $active Active coupon or no
     */
    public $active;

    public $dateFinish;

    /**
     * {@inheritdoc}. Rules for validate this form
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['type', 'value', 'count'], 'required'],
            [['type'], 'in', 'range' => ['fix', 'percent']],
            [['value', 'count'], 'integer'],
            [['active'], 'boolean'],
        ];
    }

    /**
     * Get list coupon types.
     *
     * @return array
     */
    public function getListCouponTypes()
    {
        return Coupon::getListCouponTypes();
    }

    /**
     * Create new coupons.
     *
     * @return boolean
     */
    public function createCoupons()
    {
        if (!$this->validate()) {
            return false;
        }

        for ($i = $this->count; $i > 0; $i--) {
            $coupon = new Coupon();

            $coupon->value = $this->value;
            $coupon->code  = Coupon::generateGUID();
            $coupon->type  = $this->type;

            $coupon->save();
        }

        return true;
    }
}
