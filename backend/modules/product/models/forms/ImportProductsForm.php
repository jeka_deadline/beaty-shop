<?php

namespace backend\modules\product\models\forms;

use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Yii;
use StdClass;
use yii\base\Model;
use backend\modules\product\models\Product;
use yii\helpers\ArrayHelper;
use backend\modules\product\models\ProductCategory;
use backend\modules\product\models\ProductRelationCategorie;
use backend\modules\product\models\ProductVariation;
use backend\modules\product\models\ProductImage;
use backend\modules\product\models\ProductInventorie;
use backend\modules\product\models\ProductPrice;
use backend\modules\product\models\ProductPromotion;
use backend\modules\product\models\ProductVariationPropertie;
use backend\modules\product\models\ProductFilter;
use backend\modules\product\models\factories\ProductCategoryFactory;
use backend\modules\product\models\factories\ProductFactory;
use backend\modules\product\models\factories\ProductVariationFactory;
use backend\modules\product\models\factories\ProductFilterFactory;
use PhpOffice\PhpSpreadsheet\Reader\Xls;
use yii\web\UploadedFile;

/**
 * Form import products.
 */
class ImportProductsForm extends Model
{
    /**
     * Import file.
     *
     * @var string $file
     */
    public $file;

    /**
     * Zip archive with images.
     *
     * @var string file
     */
    public $images;

    /**
     * List categories.
     *
     * @var array $listCategories
     */
    private static $listCategories;

    /**
     * List products.
     *
     * @var array $listProducts
     */
    private static $listProducts;

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['file'], 'file', 'skipOnEmpty' => false, 'checkExtensionByMimeType' => false, 'extensions' => [Product::EXPORT_XLS]],
            [['images'], 'file', 'skipOnEmpty' => true, 'checkExtensionByMimeType' => false, 'extensions' => ['zip']],
        ];
    }

    const EXTRACT_ARCHIVE_PATH = '@backend/runtime/import';

    /**
     * Import products.
     *
     * @param string $file File.
     * @return void
     */
    public function import($file)
    {
        $archive = UploadedFile::getInstance($this, 'images');

        $this->clearArchiveDirectory();

        if ($archive) {
            $this->extractFile($archive);
        }

        $xls = new Xls();

        $spreadsheet = $xls->load($file);

        $worksheet = $spreadsheet->getActiveSheet();

        $data = $worksheet->toArray();

        $rows = $data[ 0 ];

        $indexRows = $this->getIndexRows($data[ 0 ]);
        unset($data[ 0 ]);

        if ($indexRows->productVariationVendorCodeIndex === false) {
            return false;
        }

        foreach ($data as $row) {

            $categories = [];

            if ($indexRows->categoryIndex !== false) {
                $categories = $this->getCategoriesByNames($row[ $indexRows->categoryIndex ]);
            }

            $productVariation = $this->getProductVariationByVendorCode($row[ $indexRows->productVariationVendorCodeIndex ], $indexRows, $row);

            if (!$productVariation) {
                continue;
            }

            if ($productVariation->isNewRecord) {
                // варианция новая, не нет slug'a продукта
                if ($indexRows->productSlugIndex === false) {
                    continue;
                }

                $product = $this->getProductBySlug($row[ $indexRows->productSlugIndex ], $indexRows, $row);

            } else {
                $product = $productVariation->product;
            }

            $transaction = Yii::$app->db->beginTransaction();

            if (!$product->validate() || !$product->save()) {

                $transaction->rollback();
                continue;
            }

            $productVariation->product_id = $product->id;

            if (!$productVariation->validate() || !$productVariation->save()) {

                $transaction->rollback();
                continue;
            }

            if ($indexRows->productVariationDefaultIndex !== false && $row[ $indexRows->productVariationDescriptionIndex ]) {
                $product->updateAttributes([
                    'default_variation_id' => $productVariation->id,
                ]);
            }

            $this->saveVariationPrice($productVariation, $indexRows, $row);
            $this->saveVariationPromotion($productVariation, $indexRows, $row);
            $this->saveVariationInventory($productVariation, $indexRows, $row);
            $this->saveLinksCategoryProduct($categories, $product);

            if ($indexRows->productVariationFiltersIndex !== false) {
                $this->saveVariationProperties($productVariation, $row[ $indexRows->productVariationFiltersIndex ]);
            }

            if ($indexRows->productVariationImagesIndex !== false) {
                $images = trim($row[ $indexRows->productVariationImagesIndex ]);

                if (!empty($images)) {

                    $images = explode(PHP_EOL, $images);
                    $this->saveVariationImages($productVariation, $images);
                }
            }

            $transaction->commit();
        }

        $this->clearArchiveDirectory();
    }

    /**
     * Get indexes name import fileds in excel.
     *
     * @access private
     * @param array $rows First row in excel table.
     * @return object
     */
    private function getIndexRows($rows)
    {
        $object = new StdClass();

        $object->categoryIndex = false;
        $object->productSlugIndex = false;
        $object->productVariationNameIndex = false;
        $object->productVariationVendorCodeIndex = false;
        $object->productVariationShortDescriptionIndex = false;
        $object->productVariationDescriptionIndex = false;
        $object->productVariationActiveIndex = false;
        $object->productVariationCountIndex = false;
        $object->productVariationPriceIndex = false;
        $object->productVariationPromotionIndex = false;
        $object->productMetaTitleIndex = false;
        $object->productMetaKeywordsIndex = false;
        $object->productMetaDescriptionIndex = false;
        $object->productVariationImagesIndex = false;
        $object->productVariationFiltersIndex = false;
        $object->productVariationDefaultIndex = false;

        foreach ($rows as $index => $rowName) {
            if (strtolower($rowName) === 'category') {
                $object->categoryIndex = $index;
            }

            if (strtolower($rowName) === 'slug') {
                $object->productSlugIndex = $index;
            }

            if (strtolower($rowName) === 'name') {
                $object->productVariationNameIndex = $index;
            }

            if (strtolower($rowName) === 'vendor code') {
                $object->productVariationVendorCodeIndex = $index;
            }

            if (strtolower($rowName) === 'short description') {
                $object->productVariationShortDescriptionIndex = $index;
            }

            if (strtolower($rowName) === 'description') {
                $object->productVariationDescriptionIndex = $index;
            }

            if (strtolower($rowName) === 'active') {
                $object->productVariationActiveIndex = $index;
            }

            if (strtolower($rowName) === 'count') {
                $object->productVariationCountIndex = $index;
            }

            if (strtolower($rowName) === 'price') {
                $object->productVariationPriceIndex = $index;
            }

            if (strtolower($rowName) === 'promotion') {
                $object->productVariationPromotionIndex = $index;
            }

            if (strtolower($rowName) === 'meta title') {
                $object->productMetaTitleIndex = $index;
            }

            if (strtolower($rowName) === 'meta keywords') {
                $object->productMetaKeywordsIndex = $index;
            }

            if (strtolower($rowName) === 'meta description') {
                $object->productMetaDescriptionIndex = $index;
            }

            if (strtolower($rowName) === 'images') {
                $object->productVariationImagesIndex = $index;
            }

            if (strtolower($rowName) === 'filters') {
                $object->productVariationFiltersIndex = $index;
            }

            if (strtolower($rowName) === 'is default') {
                $object->productVariationDefaultIndex = $index;
            }
        }

        return $object;
    }

    /**
     * Get categories by list names divide PHP_EOL constant.
     *
     * @access private
     * @param string $categoryNames String with category names.
     * @return \backend\modules\product\models\ProductCategory
     */
    private function getCategoriesByNames($categoryNames)
    {
        $categoryNames = explode(PHP_EOL, $categoryNames);

        $categories = [];

        foreach ($categoryNames as $categoryName) {
            if (empty($categoryName)) {
                continue;
            }
            $categoryNameLower = mb_strtolower($categoryName, 'UTF-8');
            if (isset(self::$listCategories[ $categoryNameLower ])) {
                $categories[] = self::$listCategories[ $categoryNameLower ];
            } else {
                $category = ProductCategory::find()->where(['like', 'name', $categoryNameLower])->one();

                if (!$category) {
                    $category = ProductCategoryFactory::createEmptyCategory();

                    $category->name = $categoryName;
                    $category->generateSlug();
                }

                self::$listCategories[ $categoryNameLower ] = $category;
                $categories[] = $category;

                unset($category);
            }
        }

        return $categories;
    }

    /**
     * Get product by vendor slug.
     *
     * @access private
     * @param string $slug Slug in excel row.
     * @param object $indexRows Object with index excel rows.
     * @param array $row Excel row.
     * @return \backend\modules\product\models\Product
     */
    private function getProductBySlug($slug, $indexRows, $row)
    {
        if (isset(self::$listProducts[ $slug ])) {
            return self::$listProducts[ $slug ];
        }

        $product = Product::findOne([
            'slug' => $slug,
        ]);

        if (!$product) {
            $attributes = [
                'slug'                 => $slug,
                'active'               => 1,
                'default_variation_id' => null,
                'rating'               => 0,
            ];

            if ($indexRows->productMetaTitleIndex !== false) {
                $attributes[ 'meta_title' ] = $row[ $indexRows->productMetaTitleIndex ];
            }

            if ($indexRows->productMetaKeywordsIndex !== false) {
                $attributes[ 'meta_keywords' ] = $row[ $indexRows->productMetaKeywordsIndex ];
            }

            if ($indexRows->productMetaDescriptionIndex !== false) {
                $attributes[ 'meta_description' ] = $row[ $indexRows->productMetaDescriptionIndex ];
            }

            $product = ProductFactory::createNewEmptyProduct($attributes);
        }

        self::$listProducts[ $slug ] = $product;

        unset($attributes);

        return $product;
    }

    /**
     * Get product variation by vendor code.
     *
     * @access private
     * @param string $vendorCode Vendor code in excel row.
     * @param object $indexRows Object with index excel rows.
     * @param array $row Excel row.
     * @return \backend\modules\product\models\ProductVariation
     */
    private function getProductVariationByVendorCode($vendorCode, $indexRows, $row)
    {
        $variation = ProductVariation::findOne([
            'vendor_code' => $vendorCode,
        ]);

        if (!$variation) {
            if ($indexRows->productVariationNameIndex === false) {
                return false;
            }
            $attributes = [
                'active' => 1,
                'vendor_code' => (string)$vendorCode,
                'name' => $row[ $indexRows->productVariationNameIndex ],
            ];

            if ($indexRows->productVariationShortDescriptionIndex !== false) {
                $attributes[ 'short_description' ] = $row[ $indexRows->productVariationShortDescriptionIndex ];
            }

            if ($indexRows->productVariationDescriptionIndex !== false) {
                $attributes[ 'description' ] = $row[ $indexRows->productVariationDescriptionIndex ];
            }

            $variation = ProductVariationFactory::createNewEmptyVariation($attributes);

            unset($attributes);
        }

        return $variation;
    }

    /**
     * Save product variation price.
     *
     * @access private
     * @param object $indexRows Object with index excel rows.
     * @param array $row Excel row.
     * @return void
     */
    private function saveVariationPrice($variation, $indexRows, $row)
    {
        if ($indexRows->productVariationPriceIndex !== false) {
            $price = ($variation->price) ? $variation->price : new ProductPrice();

            $price->product_variation_id = $variation->id;
            $price->price = $row[ $indexRows->productVariationPriceIndex ];

            $price->save();

            unset($price);
        }
    }

    /**
     * Save product variation promotion price.
     *
     * @access private
     * @param object $indexRows Object with index excel rows.
     * @param array $row Excel row.
     * @return void
     */
    private function saveVariationPromotion($variation, $indexRows, $row)
    {
        if ($indexRows->productVariationPromotionIndex !== false) {
            if (empty($row[ $indexRows->productVariationPromotionIndex ])) {
                if ($variation->promotion) {
                    $variation->promotion->delete();
                }

                return;
            }

            $price = ($variation->promotion) ? $variation->promotion : new ProductPromotion();

            $price->product_variation_id = $variation->id;
            $price->price = $row[ $indexRows->productVariationPromotionIndex ];

            $price->save();

            unset($price);
        }
    }

    /**
     * Save product variation inventory.
     *
     * @access private
     * @param object $indexRows Object with index excel rows.
     * @param array $row Excel row.
     * @return void
     */
    private function saveVariationInventory($variation, $indexRows, $row)
    {
        if ($indexRows->productVariationCountIndex !== false) {
            $inventory = ($variation->inventorie) ? $variation->inventorie : new ProductInventorie();

            $inventory->product_variation_id = $variation->id;
            $inventory->count = $row[ $indexRows->productVariationCountIndex ];

            $inventory->save();

            unset($inventory);
        }
    }

    /**
     * Save product links with categoriess.
     *
     * @access private
     * @param \frontend\modules\product\models\ProductVariation $variation Product variation model.
     * @param sring $properties List properties.
     * @return void
     */
    private function saveLinksCategoryProduct($categories, $product)
    {
        $categoriesRelations = ArrayHelper::map($product->productRelationCategories, 'product_category_id', function($model) { return $model; });

        foreach ($categories as $category) {
            if ($category->isNewRecord) {
                $category->save(false);

                self::$listCategories[ $category->slug ] = $category;
            }

            if (isset($categoriesRelations[ $category->id ])) {
                unset($categoriesRelations[ $category->id ]);

                continue;
            }

            $model = new ProductRelationCategorie();

            $model->product_category_id = $category->id;
            $model->product_id = $product->id;

            $model->save();

            unset($model);
        }

        foreach ($categoriesRelations as $model) {
            $model->delete();
        }
    }

    /**
     * Save product variations properties.
     *
     * @access private
     * @param \frontend\modules\product\models\ProductVariation $variation Product variation model.
     * @param sring $properties List properties.
     * @return void
     */
    private function saveVariationProperties($variation, $properties)
    {
        $properties = explode(PHP_EOL, $properties);

        foreach ($properties as $property) {
            if (empty($property)) {
                continue;
            }

            $propertyData = explode(':', $property);

            if (!isset($propertyData[ 0 ], $propertyData[ 1 ])) {
                continue;
            }

            $propertyName = mb_strtolower($propertyData[ 0 ], 'UTF-8');
            $propertyValue = $propertyData[ 1 ];

            $filter = ProductFilter::find()
                ->where(['like', 'name', $propertyName])
                ->one();

            $transaction = Yii::$app->db->beginTransaction();

            if (!$filter) {
                $filter = ProductFilterFactory::createNewFilter($propertyName);

                if (!$filter->validate() || !$filter->save()) {
                    $transaction->rollback();

                    continue;
                }
            }

            $productProperty = ProductVariationPropertie::find()
                ->where([
                    'filter_id' => $filter->id,
                    'product_variation_id' => $variation->id,
                ])->one();

            if ($productProperty) {
                $productProperty->updateAttributes(['value' => $propertyValue]);

                $transaction->commit();

                continue;
            }

            if (!$variation->attachNewFilter($filter, $propertyValue)) {
                $transaction->rollback();

                continue;
            }

            $transaction->commit();

            unset($propertyData);
            unset($filter);
            unset($propertyValue);
            unset($propertyName);
        }
    }

    /**
     * Remove product variations old images.
     *
     * @access private
     * @param \backend\modules\product\models\ProductImage[] Old variation images.
     * @return void
     */
    private function removeOldImages($images)
    {
        foreach ($images as $image) {
            $image->delete();
        }
    }

    /**
     * Save product variation images.
     *
     * @access private
     * @param \frontend\modules\product\models\ProductVariation $variation Product variation model.
     * @param sring $images List images.
     * @return void
     */
    private function saveVariationImages($variation, $images)
    {
        $filePath = Yii::getAlias('@frontend/web/' . ProductImage::$path);
        $oldImages = $variation->images;

        foreach ($images as $imagePath) {
            $imagePath = trim($imagePath);
            if (empty($imagePath)) {
                continue;
            }

            $pathUrl = parse_url($imagePath);

            if (empty($pathUrl[ 'scheme' ])) {
                $imageUrlPath = Yii::getAlias(self::EXTRACT_ARCHIVE_PATH . '/' . $imagePath);
            } else {
                $imageUrlPath = $imagePath;
            }

            if (empty($pathUrl[ 'scheme' ]) && (!file_exists($imageUrlPath) || !is_file($imageUrlPath))) {
                continue;
            }

            $dataFiles = explode(DIRECTORY_SEPARATOR, $imagePath);

            $baseFileName = array_pop($dataFiles);
            $fileNameInformation = explode('.', $baseFileName);

            if (!isset($fileNameInformation[ 0 ]) || !isset($fileNameInformation[ 1 ])) {
                continue;
            }

            $fileName = md5($fileNameInformation[ 0 ]);
            $fileExtension = $fileNameInformation[ 1 ];

            $productVariationImage = new ProductImage();
            $productVariationImage->filename = $fileName . '.' . $fileExtension;
            $productVariationImage->product_variation_id = $variation->id;
            $imageFile = file_get_contents($imageUrlPath);

            if (file_put_contents($filePath . $fileName . '.' . $fileExtension, $imageFile)) {
                $productVariationImage->save();
            }

            unset($imageFile);
            unset($productVariationImage);
            unset($fileName);
            unset($fileExtension);
            unset($baseFileName);
            unset($fileNameInformation);
            unset($dataFiles);
            unset($imageUrlPath);
            unset($pathUrl);
            unset($imagePath);
        }

        $this->removeOldImages($oldImages);
    }

    /**
     * Extract zip file.
     *
     * @access private
     * @param \yii\web\UploadedFile $file Zip archive.
     * @return void
     */
    private function extractFile(UploadedFile $file)
    {
        $zip = new \ZipArchive;

        $zipped = $zip->open($file->tempName);

        $extract = $zip->extractTo(Yii::getAlias(self::EXTRACT_ARCHIVE_PATH));
    }

    /**
     * Clear import directory.
     *
     * @access private
     * @return void
     */
    private function clearArchiveDirectory()
    {
        if (!file_exists(Yii::getAlias(self::EXTRACT_ARCHIVE_PATH))) {
            mkdir(Yii::getAlias(self::EXTRACT_ARCHIVE_PATH), 0777, true);
        }

        $rii = new RecursiveIteratorIterator(new RecursiveDirectoryIterator(Yii::getAlias(self::EXTRACT_ARCHIVE_PATH)));

        foreach ($rii as $file) {
            if ($file->isDir()){
                continue;
            }

            unlink($file->getPathName());
        }
    }
}
