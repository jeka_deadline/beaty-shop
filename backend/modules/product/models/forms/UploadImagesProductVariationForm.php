<?php
namespace backend\modules\product\models\forms;

use backend\modules\product\models\ProductImage;
use Yii;
use yii\base\Model;
use yii\helpers\Url;

class UploadImagesProductVariationForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $imageFiles;
    public $moveImageFiles;
    private $modelProductVariation;

    public function rules()
    {
        return [
            [['imageFiles'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg', 'maxFiles' => 0],
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $basePath = Yii::getAlias('@frontend/web');
            $allPath = $basePath . '/' . ProductImage::$path;
            if (!is_dir($allPath)) {
                mkdir($allPath, 0777, true);
            }
            foreach ($this->imageFiles as $key => $file) {
                $filename = md5(uniqid($file->baseName)).'.'.$file->extension;
                if ($file->saveAs($allPath . $filename)) {
                    $this->moveImageFiles[$key] = [
                        'path' => $allPath,
                        'filename' => $filename,
                    ];
                    chmod($allPath . $filename, 0777);
                    $this->saveImage($key);
                }
            }
            return true;
        } else {
            return false;
        }
    }

    private function saveImage($key) {
        $countImage = $this->modelProductVariation->images?count($this->modelProductVariation->images):0;
        $modelImage = new ProductImage();
        $modelImage->filename = $this->moveImageFiles[$key]['filename'];
        $modelImage->sort = $countImage + $key;
        $modelImage->preview = 0;
        $this->modelProductVariation->link('images', $modelImage);
    }

    public function setProductVariation($modelProductVariation) {
        $this->modelProductVariation = $modelProductVariation;
    }

    public function getInitialPreview() {
        if (!$this->modelProductVariation->images) return [];

        $basePath = Yii::getAlias('@frontend/web');
        $allPath = $basePath . '/' . ProductImage::$path;

        $initialPreview = [];
        foreach ($this->modelProductVariation->images as $modelImage) {
            $initialPreview[] = '/'.ProductImage::$path.$modelImage->filename;
        }
        return $initialPreview;
    }

    public function getInitialPreviewConfig() {
        if (!$this->modelProductVariation->images) return [];

        $initialPreviewConfig = [];
        foreach ($this->modelProductVariation->images as $modelImage) {
            $initialPreviewConfig[] = [
                'url' => Url::toRoute(['product-variation/image-delete']),
                'key' => $modelImage->id,
            ];
        }
        return $initialPreviewConfig;
    }
}