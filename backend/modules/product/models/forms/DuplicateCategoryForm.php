<?php

namespace backend\modules\product\models\forms;

use yii\base\Model;
use backend\modules\product\models\ProductCategory;
use backend\modules\product\models\Product;
use backend\modules\product\models\ProductRelationCategorie;

/**
 * Form for fast add list products to categories.
 */
class DuplicateCategoryForm extends Model
{
    /**
     * Category id.
     *
     * @var int
     */
    public $categoryId;

    /**
     * List ids.
     *
     * @var int[] $ids
     */
    public $ids;

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return [
            ['categoryId', 'required'],
            ['categoryId', 'exist', 'targetClass' => ProductCategory::className(), 'targetAttribute' => 'id'],
            ['ids', 'safe'],
        ];
    }

    /**
     * Get list categories for select tag.
     *
     * @return array
     */
    public function getListCategories()
    {
        return ProductCategory::getTreeCategoriesForSelect();
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'categoryId' => 'Category',
        ];
    }

    public function moveToCategory()
    {
        if (empty($this->ids)) {
            return true;
        }

        $products = Product::find()
            ->where(['id' => $this->ids])
            ->with('categories')
            ->all();

        foreach ($products as $product) {
            $count = ProductRelationCategorie::find()
                ->where(['product_id' => $product->id, 'product_category_id' => $this->categoryId])
                ->count();

            if ($count) {
                continue;
            }

            $model = new ProductRelationCategorie();
            $model->product_id = $product->id;
            $model->product_category_id = $this->categoryId;
            $model->save(false);
        }

        return true;
    }
}
