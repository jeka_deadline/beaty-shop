<?php

namespace backend\modules\product\models\forms;

use Yii;
use yii\base\Model;

class ValidateSetItemForm extends Model
{
    public $count;

    public $id;

    public function rules()
    {
        return [
            [['id', 'count'], 'required'],
            [['id'], 'integer'],
            [['count'], 'integer', 'min' => 1],
        ];
    }

    public function formName()
    {
        return '';
    }
}
