<?php

namespace backend\modules\product\models;

use Yii;
use common\models\product\ProductCategoryLangField as BaseProductCategoryLangField;
use backend\modules\core\models\Language;


class ProductCategoryLangField extends BaseProductCategoryLangField
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(Language::className(), ['id' => 'lang_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductCategory()
    {
        return $this->hasOne(ProductCategory::className(), ['id' => 'product_category_id']);
    }
}
