<?php

namespace backend\modules\product\models;

use Yii;
use common\models\product\OrderShippingInformation as BaseOrderShippingInformation;
use backend\modules\geo\models\Country;

/**
 * Frontend order shipping information extends common order shipping information
 */
class OrderShippingInformation extends BaseOrderShippingInformation
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
}
