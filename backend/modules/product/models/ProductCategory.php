<?php

namespace backend\modules\product\models;

use Yii;
use common\models\product\ProductCategory as BaseProductCategory;
use yii\helpers\ArrayHelper;
use backend\modules\core\behaviors\LangBehavior;
use backend\modules\core\models\Language;

/**
 * Backend product category model extends common product category model.
 */
class ProductCategory extends BaseProductCategory
{
    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['parent_id', 'display_order', 'display_order_hmenu'], 'default', 'value' => 0],
                ['slug', 'match', 'pattern' => '#^[a-z][a-z\d-]*[a-z\d]$#'],
                ['productFilters', 'safe']
            ]
        );
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                [
                    'class' => LangBehavior::className(),
                    'langModel' => ProductCategoryLangField::className(),
                    'modelForeignKey' => 'product_category_id',
                    'languages' => Language::find()->all(),
                    'attributes' => [
                        'name', 'description', 'meta_title', 'meta_description', 'meta_keywords',
                    ],
                ],
                \e96\behavior\RelationalBehavior::className(),
            ]
        );
    }

    /**
     * Generate slug for category model.
     *
     * @return void
     */
    public function generateSlug()
    {
        $letter = chr(rand(97,122));
        $slug = $letter . md5(uniqid());

        $category = self::findOne([
            'slug' => $slug,
        ]);

        while ($category) {
            $slug = $letter . md5(uniqid());

            $category = self::findOne([
                'slug' => $slug,
            ]);
        }

        $this->slug = $slug;
    }

    /**
     * Get categories list for select tag.
     *
     * @param int $excludeId Exclude id category
     * @return array
     */
    public static function getTreeCategoriesForSelect($excludeId = 0)
    {
        $hashListCategories = self::find()->indexBy('id')->asArray()->all();
        $tree = static::createTree($hashListCategories);

        return static::generateSelectItems($tree, $excludeId);
    }

    /**
     * Generate array structure for select from tree structure.
     *
     * @param \backend\modules\product\models\ProductCategory $tree
     * @param int $excludeId Exclude id from select
     * @return array
     */
    public static function generateSelectItems(array $tree, $excludeId, $level = 0)
    {
        static $items = [];

        foreach ($tree as $id => $node) {
            if ($excludeId == $id) {
                continue;
            }

            $items[ $node[ 'id' ] ] = str_repeat('-', $level) . $node[ 'name' ];

            if (isset($node[ 'children' ])) {
                call_user_func_array(['self', __METHOD__], [$node[ 'children' ], $excludeId, $level + 2]);
            }
        }

        return $items;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLangFields()
    {
        return $this->hasMany(ProductCategoryLangField::className(), ['product_category_id' => 'id']);
    }

    public function getInputFilterItems() {
        return ArrayHelper::map($this->productFilters,'id', function ($model) {
            return ['content' => $model->name];
        });
    }

    public function getInputExcludeFilterItems() {
        $result = [];
        $listFilterIds = ArrayHelper::map($this->productFilters,'id', 'id');

        $modelFilters = ProductFilter::find()->all();

        if (!$modelFilters) return [];

        foreach ($modelFilters as $modelFilter) {
            if (array_search($modelFilter->id, $listFilterIds)===false)
                $result[$modelFilter->id] = ['content' => $modelFilter->name];
        }

        return $result;
    }

    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);

        //Clear product properties after changing filters in the category.
        ProductVariationPropertie::deleteAll(['AND',['product_category_id' => $this->id],['NOT IN', 'filter_id', ArrayHelper::map($this->productFilters, 'id', 'id')]]);
    }
}
