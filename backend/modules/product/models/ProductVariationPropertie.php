<?php

namespace backend\modules\product\models;

use Yii;

class ProductVariationPropertie extends \common\models\product\ProductVariationPropertie
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductCategory()
    {
        return $this->hasOne(ProductCategory::className(), ['id' => 'product_category_id']);
    }
}
