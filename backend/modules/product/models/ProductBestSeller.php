<?php

namespace backend\modules\product\models;

use Yii;
use common\models\product\ProductBestSeller as BaseProductBestSeller;

class ProductBestSeller extends BaseProductBestSeller
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
