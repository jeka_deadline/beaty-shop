<?php

namespace backend\modules\product\models;

use DateTime;
use Yii;
use backend\modules\user\models\User;
use common\models\product\Order as BaseOrder;
use backend\modules\product\models\OrderShippingInformation;
use backend\modules\product\models\OrderBillingInformation;

/**
 * Frontend order model extends common order model.
 */
class Order extends BaseOrder
{
    /**
     * Get List order statuses.
     *
     * @return array
     */
    public function getListStatuses()
    {
        return [
            self::ORDER_STATUS_NEW => 'New',
            self::ORDER_STATUS_PAID => 'Paid',
            self::ORDER_STATUS_CANCEL => 'Cancel',
            self::ORDER_STATUS_DELIVERED => 'Delivered',
        ];
    }

    /**
     * Check if has new orders.
     *
     * @return bool
     */
    public static function isHasNewOrders()
    {
        return (self::getCountNewOrders()) ? true : false;
    }

    /**
     * Reset order flag new.
     *
     * @return void
     */
    public function resetFlagNew()
    {
        $this->updateAttributes(['is_new' => 0]);
    }

    /**
     * Get count new orders.
     *
     * @return int
     */
    public static function getCountNewOrders()
    {
        return self::find()
            ->where(['is_new' => 1])
            ->count();
    }

    /**
     * Get count status paid.
     *
     * @return int
     */
    public static function getCountStatusNewOrders()
    {
        return self::find()
            ->where(['status' => self::ORDER_STATUS_NEW])
            ->count();
    }

    /**
     * Get count status paid.
     *
     * @return int
     */
    public static function getCountStatusPaidOrders()
    {
        return self::find()
            ->where(['status' => self::ORDER_STATUS_PAID])
            ->count();
    }

    public static function getRecentOrders()
    {
        return self::find()
            ->where(['in', 'status', [self::ORDER_STATUS_PAID, self::ORDER_STATUS_PREAUTORIZATION, self::ORDER_STATUS_DELIVERED]])
            ->orderBy(['created_at' => SORT_DESC])
            ->limit(20);
    }

    /**
     * Get step payment order status.
     *
     * @return string
     */
    public function getStepPayment()
    {
        switch ($this->status) {
            case self::ORDER_STATUS_NEW:
                return 'new (wait paid)';
            default:
                return $this->status;
        }
    }

    /**
     * Get created at date by format.
     *
     * @param string $format Format date.
     * @return string
     */
    public function getCreatedAtFormat($format = 'd.m.Y')
    {
        $date = new DateTime($this->created_at);

        return $date->format($format);
    }

    /**
     * Get pdf number format.
     *
     * @return string
     */
    public function getPdfNumberFormat()
    {
        $date = new DateTime($this->created_at);
        $indexInDate = 0;

        $listRecordsByDate = self::find()
            ->where(['=', 'DATE(created_at)', $date->format('Y-m-d')])
            ->all();

        foreach ($listRecordsByDate as $index => $record) {
            if ($record->id === $this->id) {
              $indexInDate = $index + 1;
            }
        }

        $indexInDate = str_pad($indexInDate, 2, 0, STR_PAD_LEFT);

        return $date->format('Ymd') . '-' . $indexInDate;
    }

    /**
     * Get pdf number format.
     *
     * @return string
     */
    public function getFullPdfNumberFormat()
    {
        return 'IL-' . $this->getPdfNumberFormat();
    }

    /**
     * Get pdf user id format.
     *
     * @return string
     */
    public function getPdfUserIdFormat()
    {
        $userId = $this->user_id + 100;

        return str_pad($userId, 6, 0, STR_PAD_LEFT);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderBillingInformation()
    {
        return $this->hasOne(OrderBillingInformation::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderProducts()
    {
        return $this->hasMany(OrderProductVariation::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderShippingInformation()
    {
        return $this->hasOne(OrderShippingInformation::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
