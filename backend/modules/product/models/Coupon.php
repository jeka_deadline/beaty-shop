<?php

namespace backend\modules\product\models;

use common\models\product\Coupon as BaseCoupon;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * Backend coupon model extends common coupon model.
 */
class Coupon extends BaseCoupon
{
    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * Get list coupon types.
     *
     * @return array
     */
    public static function getListCouponTypes()
    {
        return [
          self::FIX_TYPE     => 'Fix',
          self::PERCENT_TYPE => 'Percent',
        ];
    }
}
