<?php

namespace backend\modules\product\models\factories;

use backend\modules\product\models\Product;

/**
 * Factory class for product model.
 */
class ProductFactory
{
    /**
     * Create new empty product model.
     *
     * @static
     * @param array $attributes List array attributes for inizialization.
     * @return \backend\modules\product\models\Product
     */
    public static function createNewEmptyProduct($attributes = [])
    {
        $model = new Product();

        foreach ($attributes as $nameAttribute => $value) {
            if ($model->hasAttribute($nameAttribute)) {
                $model->{$nameAttribute} = $value;
            }
        }

        return $model;
    }
}
