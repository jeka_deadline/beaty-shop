<?php

namespace backend\modules\product\models\factories;

use backend\modules\product\models\ProductCategory;

/**
 * Factory class for product category model.
 */
class ProductCategoryFactory
{
    /**
     * Create new empty product category model.
     *
     * @static
     * @return \backend\modules\product\models\ProductCategory
     */
    public static function createEmptyCategory()
    {
        $model = new ProductCategory();

        $model->display_order = 0;
        $model->active = 1;
        $model->description = null;
        $model->meta_title = null;
        $model->meta_description = null;
        $model->meta_keywords = null;
        $model->show_search = 0;
        $model->display_order_hmenu = 0;
        $model->parent_id = 0;

        return $model;
    }
}