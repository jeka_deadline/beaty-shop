<?php

namespace backend\modules\product\models\factories;

use backend\modules\product\models\ProductFilter;

/**
 * Factory class for product filter model.
 */
class ProductFilterFactory
{
    /**
     * Create new empty product filter model.
     *
     * @static
     * @param string $name Name filter.
     * @return \backend\modules\product\models\ProductFilter
     */
    public static function createNewFilter($name)
    {
        $model = new ProductFilter();
        $model->name = $name;

        if ($name === 'color' || $name === 'colour') {
            $model->type = 'colorlist';
        } else {
            $model->type = 'radiolist';
        }

        return $model;
    }
}