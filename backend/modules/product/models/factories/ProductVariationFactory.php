<?php

namespace backend\modules\product\models\factories;

use backend\modules\product\models\ProductVariation;

/**
 * Factory class for product variation model.
 */
class ProductVariationFactory
{
    /**
     * Create new empty product variation model.
     *
     * @static
     * @param array $attributes List array attributes for inizialization.
     * @return \backend\modules\product\models\ProductVariation
     */
    public static function createNewEmptyVariation($attributes = [])
    {
        $model = new ProductVariation();

        foreach ($attributes as $nameAttribute => $value) {
            if ($model->hasAttribute($nameAttribute)) {
                $model->{$nameAttribute} = $value;
            }
        }

        return $model;
    }
}