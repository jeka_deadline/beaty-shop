<?php

namespace backend\modules\product\models;

use backend\modules\core\behaviors\LangBehavior;
use common\models\core\Language;
use yii\helpers\ArrayHelper;

class Product extends \common\models\product\Product
{
    /**
     * Type export format xml.
     *
     * @var string
     */
    const EXPORT_XML = 'xml';

    /**
     * Type export format xls.
     *
     * @var string
     */
    const EXPORT_XLS = 'xls';

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['recommendets', 'productCategories', 'packs', 'productSetVariations', 'productSetProducts'], 'safe']
            ]
        );
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                [
                    'class' => LangBehavior::className(),
                    'langModel' => ProductLangField::className(),
                    'modelForeignKey' => 'product_id',
                    'languages' => Language::find()->all(),
                    'attributes' => [
                        'meta_title',
                        'meta_description',
                        'meta_keywords',
                    ]
                ],
                \e96\behavior\RelationalBehavior::className(),
            ]
        );
    }

    /**
     * Get categories list separate comma.
     *
     * @return string
     */
    public function getCategoriesRowList()
    {
        return implode(', ', ArrayHelper::map($this->categories, 'id', 'name'));
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLangFields()
    {
        return $this->hasMany(ProductLangField::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(ProductCategory::className(), ['id' => 'product_category_id'])
            ->viaTable(ProductRelationCategorie::tableName(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductVariations()
    {
        return $this->hasMany(ProductVariation::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDefaultProductVariation()
    {
        return $this->hasOne(ProductVariation::className(), ['id' => 'default_variation_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecommendets()
    {
        return $this->hasMany(Product::className(), ['id' => 'product_recommended_id'])
            ->viaTable(ProductRecommendet::tableName(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPacks()
    {
        return $this->hasMany(Product::className(), ['id' => 'product_pack_id'])
            ->viaTable(Pack::tableName(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductRelationCategories()
    {
        return $this->hasMany(ProductRelationCategorie::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductSetVariations()
    {
        return $this->hasMany(ProductVariation::className(), ['id' => 'product_variation_id'])
            ->viaTable(ProductSetVariation::tableName(), ['product_set_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductSetRelationVariations()
    {
        return $this->hasMany(ProductSetVariation::className(), ['product_set_id' => 'id'])->indexBy('product_variation_id');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductSetProducts()
    {
        return $this->hasMany(Product::className(), ['id' => 'product_id'])
            ->viaTable(ProductSetProduct::tableName(), ['product_set_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductSetRelationProducts()
    {
        return $this->hasMany(ProductSetProduct::className(), ['product_set_id' => 'id'])->indexBy('product_id');
    }

    public function getMaxCountVariations() {
        return ProductInventorie::find()
            ->joinWith('productVariation')
            ->where(['product_id' => $this->id])
            ->max('count');
    }

    public function getMinCountVariations() {
        return ProductInventorie::find()
            ->joinWith('productVariation')
            ->where(['product_id' => $this->id])
            ->min('count');
    }
}
