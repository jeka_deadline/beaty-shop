<?php

namespace backend\modules\product\models;

use Yii;

class ProductImage extends \common\models\product\ProductImage
{
    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }
        $basePath = Yii::getAlias('@frontend/web');
        $allPath = $basePath . '/' . self::$path;
        $filename = $allPath . $this->filename;
        if(file_exists($filename)) {
            unlink($filename);
        }
        return true;
    }

    public function getUrlImage() {
        return '/' . self::$path . $this->filename;
    }
}
