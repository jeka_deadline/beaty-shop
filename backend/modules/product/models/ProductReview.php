<?php

namespace backend\modules\product\models;

use Yii;
use common\models\product\ProductReview as BaseProductReview;
use yii\helpers\ArrayHelper;

/**
 * Backend product review model extends common product review model.
 */
class ProductReview extends BaseProductReview
{
    public $formRating;

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                ['formRating', 'safe'],
            ]
        );
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if (!$this->rating) {
            $rating = new ProductRating();
            $rating->user_id = $this->user_id;
            $rating->product_id = $this->product_id;
        } else {
            $rating = $this->rating;
        }

        $rating->value = $this->formRating;
        $rating->active = $this->active;

        $isNewRating = $rating->isNewRecord;

        $rating->save();
        Product::updateRating($this->product_id);

        if ($isNewRating) {
            $this->updateAttributes(['rating_id' => $rating->id]);
        }
    }

    public function afterDelete()
    {
        $model = ProductRating::findOne($this->rating_id);

        if ($model) {
            $model->delete();
        }

        Product::updateRating($this->product_id);
    }
}
