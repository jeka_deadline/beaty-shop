<?php

namespace backend\modules\product\models;

use Yii;
use backend\modules\geo\models\Country;
use common\models\product\OrderBillingInformation as BaseOrderBillingInformation;

/**
 * Frontend order billing information extends common order billing information
 */
class OrderBillingInformation extends BaseOrderBillingInformation
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
}
