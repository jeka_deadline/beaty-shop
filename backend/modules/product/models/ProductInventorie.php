<?php

namespace backend\modules\product\models;

class ProductInventorie extends \common\models\product\ProductInventorie
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductVariation()
    {
        return $this->hasOne(ProductVariation::className(), ['id' => 'product_variation_id']);
    }
}
