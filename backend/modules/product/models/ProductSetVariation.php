<?php

namespace backend\modules\product\models;

use Yii;

class ProductSetVariation extends \common\models\product\ProductSetVariation
{
    public function getProductVariation()
    {
        return $this->hasOne(ProductVariation::className(), ['id' => 'product_variation_id']);
    }
}
