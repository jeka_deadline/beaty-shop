<?php

namespace backend\modules\product\models\searchModels;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\product\models\Order;

/**
 * OrderSearch represents the model behind the search form of `backend\modules\product\models\Order`.
 */
class OrderSearch extends Order
{
    public $distributor;

    public $invoice_number;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'is_new'], 'integer'],
            [['order_number', 'type_payment', 'status', 'created_at', 'updated_at'], 'safe'],
            [['total_sum'], 'number'],
            [['distributor'], 'safe'],
            [['invoice_number'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find()
            ->orderBy(['created_at' => SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->distributor) {
            $query->joinWith('user.distributor dis')
                ->where(['dis.id' => $this->distributor]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'total_sum' => $this->total_sum,
            'is_new' => $this->is_new,
        ]);

        $query->andFilterWhere(['like', 'order_number', $this->order_number])
            ->andFilterWhere(['like', 'type_payment', $this->type_payment])
            ->andFilterWhere(['like', 'status', $this->status]);

        //Caution! artificial intelligence works. Begin
        if (
            $this->invoice_number
            && is_array($invoice_number = explode('-', $this->invoice_number))
        ) {
            $flagSetFilterDate = false;
            $countSetIfNumber = 0;
            $elseForeach = true;
            foreach ($invoice_number as $value) {
                if (date('Ymd', strtotime($value)) === $value) {
                    $query->andFilterWhere(['=', 'DATE(created_at)', $value]);
                    $flagSetFilterDate = true;
                    $elseForeach = false;
                    continue;
                }
                if (is_numeric($value) && $flagSetFilterDate) {
                    $value = ((int) $value) - 1;
                    $dataProvider->pagination = false;
                    $query
                        ->orderBy('created_at')
                        ->limit(1)
                        ->offset(($value>0?$value:0));
                    $elseForeach = false;
                    continue;
                }
                if (mb_strtolower($value) == 'il') {
                    $query->andWhere(true);
                    $elseForeach = false;
                    continue;
                }
                if (is_numeric($value) && !$flagSetFilterDate) {
                    switch ($countSetIfNumber) {
                        case 0:
                            $query->andFilterWhere(['like', 'REPLACE(DATE(created_at),"-","")', $value]);
                            break;
                        case 1:
                            $value = ((int)$value) - 1;
                            $dataProvider->pagination = false;
                            $query->orderBy('created_at')
                                ->limit(1)
                                ->offset(($value>0?$value:0));
                            break;
                    }
                    $countSetIfNumber++;
                    $elseForeach = false;
                    continue;
                }
            }
            if ($elseForeach) {
                $query->andFilterWhere(['like', 'LOWER(CONVERT(created_at, CHAR))', mb_strtolower($this->invoice_number, 'UTF-8')]);
            }
        }
        // end.

        return $dataProvider;
    }
}
