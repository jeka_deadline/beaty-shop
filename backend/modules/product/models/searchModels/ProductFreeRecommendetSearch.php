<?php

namespace backend\modules\product\models\searchModels;

use backend\modules\product\models\ProductFreeRecommendet;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ProductFreeRecommendetSearch represents the model behind the search form of `backend\modules\product\models\ProductFreeRecommendet`.
 */
class ProductFreeRecommendetSearch extends ProductFreeRecommendet
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'product_recommended_id', 'display_order'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProductFreeRecommendet::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->with('product.defaultProductVariation');

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'product_recommended_id' => $this->product_recommended_id,
            'display_order' => $this->display_order,
        ]);

        return $dataProvider;
    }
}
