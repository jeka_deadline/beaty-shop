<?php

use yii\db\Migration;

/**
 * Class m180602_111145_new_fields_table_orders
 */
class m180602_111145_new_fields_table_orders extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        $this->addColumn('{{%product_orders}}', 'coupon_code', $this->string(255)->null() . ' after total_sum');
        $this->addColumn('{{%product_orders}}', 'sum_without_coupon', $this->float()->null()->defaultValue(0) . ' after total_sum');
        $this->addColumn('{{%product_orders}}', 'full_sum', $this->float()->notNull() . ' after total_sum');
        $this->addColumn('{{%product_orders}}', 'tax', $this->float()->null()->defaultValue(0) . ' after total_sum');
        $this->addColumn('{{%product_orders}}', 'shipping_price', $this->float()->null()->defaultValue(0) . ' after total_sum');
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        $this->dropColumn('{{%product_orders}}', 'coupon_code');
        $this->dropColumn('{{%product_orders}}', 'sum_without_coupon');
        $this->dropColumn('{{%product_orders}}', 'full_sum');
        $this->dropColumn('{{%product_orders}}', 'tax');
        $this->dropColumn('{{%product_orders}}', 'shipping_price');
    }
}
