<?php

use yii\db\Migration;

/**
 * Class m180424_094438_add_colum_category_id_variation_properties_table
 */
class m180424_094438_add_colum_category_id_variation_properties_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('product_variation_properties', 'product_category_id', $this->integer()->notNull());

        $this->createRelations();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropRelations();
        $this->dropColumn('product_variation_properties', 'product_category_id');
    }

    private function createRelations()
    {
        $this->createIndex('ix_product_variation_properties_product_category_id', '{{%product_variation_properties}}', 'product_category_id');
        $this->addForeignKey(
            'fk_product_variation_properties_product_category_id',
            '{{%product_variation_properties}}',
            'product_category_id',
            '{{%product_categories}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    private function dropRelations()
    {
        $this->dropForeignKey('fk_product_variation_properties_product_category_id','{{%product_variation_properties}}');
        $this->dropIndex('ix_product_variation_properties_product_category_id', '{{%product_variation_properties}}');
    }
}
