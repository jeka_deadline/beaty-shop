<?php

use yii\db\Migration;

/**
 * Class m180602_093417_add_column_is_new_to_table_orders
 */
class m180602_093417_add_column_is_new_to_table_orders extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        $this->addColumn('{{%product_orders}}', 'is_new', $this->smallInteger(1)->defaultValue(1) . ' after total_sum');
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        $this->dropColumn('{{%product_orders}}', 'is_new');
    }
}
