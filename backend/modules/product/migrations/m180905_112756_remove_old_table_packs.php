<?php

use yii\db\Migration;

/**
 * Class m180905_112756_remove_old_table_packs
 */
class m180905_112756_remove_old_table_packs extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk_product_order_products_pack_id', '{{%product_order_products}}');
        $this->dropIndex('ix_product_order_products_pack_id', '{{%product_order_products}}');
        $this->dropColumn('{{%product_order_products}}', 'pack_id');

        $this->dropForeignKey('fk_product_variation_packs_product_variation_id','{{%product_variation_packs}}');
        $this->dropIndex('ix_product_variation_packs_product_variation_id', '{{%product_variation_packs}}');

        $this->dropTable('{{%product_variation_packs}}');
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        return true;
    }
}
