<?php

use yii\db\Migration;

/**
 * Class m180924_065036_add_permission_stock
 */
class m180924_065036_add_permission_stock extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        $permitions = [
            [
                'name' => 'product/product-variation/stock',
                'type' => 2,
                'description' => 'Module Product. Show products stock',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
        ];

        $this->batchInsert(
            '{{%auth_item}}',
            ['name', 'type', 'description', 'created_at', 'updated_at'],
            $permitions
        );

        $data = [];

        foreach ($permitions as $permition) {
            $data[] = [
                'parent' => 'superadmin',
                'child' => $permition['name'],
            ];
        }

        if ($data) {
            $this->batchInsert(
                '{{%auth_item_child}}',
                ['parent', 'child'],
                $data
            );
        }
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function safeDown()
    {
        return true;
    }
}
