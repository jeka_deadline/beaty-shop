<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product_filters`.
 */
class m180420_132929_create_product_filters_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('product_filters', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'type' => $this->string(100)->null(),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);

        $this->createTable('product_variation_properties', [
            'id' => $this->primaryKey(),
            'product_variation_id' => $this->integer()->notNull(),
            'filter_id' => $this->integer()->notNull(),
            'value' => $this->string(255)->null(),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);

        $this->createTable('{{%product_categorie_relation_filters}}', [
            'id' => $this->primaryKey(),
            'product_category_id' => $this->integer()->notNull(),
            'filter_id' => $this->integer()->notNull(),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);

        $this->createRelations();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropRelations();
        $this->dropTable('product_categorie_relation_filters');
        $this->dropTable('product_variation_properties');
        $this->dropTable('product_filters');
    }

    private function createRelations()
    {
        $this->createIndex('ix_product_variation_properties_variation_id', '{{%product_variation_properties}}', 'product_variation_id');
        $this->addForeignKey(
            'fk_product_variation_properties_variation_id',
            '{{%product_variation_properties}}',
            'product_variation_id',
            '{{%product_variations}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->createIndex('ix_product_variation_properties_filter_id', '{{%product_variation_properties}}', 'filter_id');
        $this->addForeignKey(
            'fk_product_variation_properties_filter_id',
            '{{%product_variation_properties}}',
            'filter_id',
            '{{%product_filters}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex('ix_product_categorie_relation_filters_product_category_id', '{{%product_categorie_relation_filters}}', 'product_category_id');
        $this->addForeignKey(
            'fk_product_categorie_relation_filters_product_category_id',
            '{{%product_categorie_relation_filters}}',
            'product_category_id',
            '{{%product_categories}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->createIndex('ix_product_categorie_relation_filters_filter_id', '{{%product_categorie_relation_filters}}', 'filter_id');
        $this->addForeignKey(
            'fk_product_categorie_relation_filters_filter_id',
            '{{%product_categorie_relation_filters}}',
            'filter_id',
            '{{%product_filters}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    private function dropRelations()
    {
        $this->dropForeignKey('fk_product_categorie_relation_filters_filter_id','{{%product_categorie_relation_filters}}');
        $this->dropIndex('ix_product_categorie_relation_filters_filter_id', '{{%product_categorie_relation_filters}}');
        $this->dropForeignKey('fk_product_categorie_relation_filters_product_category_id','{{%product_categorie_relation_filters}}');
        $this->dropIndex('ix_product_categorie_relation_filters_product_category_id', '{{%product_categorie_relation_filters}}');

        $this->dropForeignKey('fk_product_variation_properties_filter_id','{{%product_variation_properties}}');
        $this->dropIndex('ix_product_variation_properties_filter_id', '{{%product_variation_properties}}');
        $this->dropForeignKey('fk_product_variation_properties_variation_id','{{%product_variation_properties}}');
        $this->dropIndex('ix_product_variation_properties_variation_id', '{{%product_variation_properties}}');
    }
}
