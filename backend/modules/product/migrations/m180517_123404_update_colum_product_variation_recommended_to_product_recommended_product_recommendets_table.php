<?php

use yii\db\Migration;

/**
 * Class m180517_123404_update_colum_product_variation_recommended_to_product_recommended_product_recommendets_table
 */
class m180517_123404_update_colum_product_variation_recommended_to_product_recommended_product_recommendets_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropOldRelations();
        $this->dropColumn('product_recommendets', 'product_variation_id');
        $this->dropColumn('product_recommendets', 'product_variation_recommended_id');

        $this->addColumn('product_recommendets', 'product_id', $this->integer()->notNull());
        $this->addColumn('product_recommendets', 'product_recommended_id', $this->integer()->notNull());
        $this->createRelations();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropRelations();
        $this->dropColumn('product_recommendets', 'product_id');
        $this->dropColumn('product_recommendets', 'product_recommended_id');

        $this->addColumn('product_recommendets', 'product_variation_id',$this->integer()->notNull());
        $this->addColumn('product_recommendets', 'product_variation_recommended_id',$this->integer()->notNull());
        $this->createOldRelations();
    }

    /**
     * Create relations between tables.
     *
     * @return void
     */
    private function createRelations()
    {
        $this->createIndex('ix_product_recommendets_product_id', '{{%product_recommendets}}', 'product_id');
        $this->addForeignKey(
            'fk_product_recommendets_product_id',
            '{{%product_recommendets}}',
            'product_id',
            '{{%product_products}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex('ix_product_recommendets_product_recommended_id', '{{%product_recommendets}}', 'product_recommended_id');
        $this->addForeignKey(
            'fk_product_recommendets_product_recommended_id',
            '{{%product_recommendets}}',
            'product_recommended_id',
            '{{%product_products}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    private function dropRelations()
    {
        $this->dropForeignKey('fk_product_recommendets_product_id', '{{%product_recommendets}}');
        $this->dropIndex('ix_product_recommendets_product_id', '{{%product_recommendets}}');

        $this->dropForeignKey('fk_product_recommendets_product_recommended_id', '{{%product_recommendets}}');
        $this->dropIndex('ix_product_recommendets_product_recommended_id', '{{%product_recommendets}}');
    }

    private function createOldRelations()
    {
        $this->createIndex('ix_product_recommendets_product_variation_id', '{{%product_recommendets}}', 'product_variation_id');
        $this->addForeignKey(
            'fk_product_recommendets_product_variation_id',
            '{{%product_recommendets}}',
            'product_variation_id',
            '{{%product_variations}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex('ix_product_recommendets_product_variation_recommended_id', '{{%product_recommendets}}', 'product_variation_recommended_id');
        $this->addForeignKey(
            'fk_product_recommendets_product_variation_recommended_id',
            '{{%product_recommendets}}',
            'product_variation_recommended_id',
            '{{%product_variations}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    private function dropOldRelations()
    {
        $this->dropForeignKey('fk_product_recommendets_product_variation_id','{{%product_recommendets}}');
        $this->dropIndex('ix_product_recommendets_product_variation_id', '{{%product_recommendets}}');
        $this->dropForeignKey('fk_product_recommendets_product_variation_recommended_id','{{%product_recommendets}}');
        $this->dropIndex('ix_product_recommendets_product_variation_recommended_id', '{{%product_recommendets}}');
    }
}
