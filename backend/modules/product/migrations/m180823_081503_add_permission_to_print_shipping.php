<?php

use yii\db\Migration;

/**
 * Class m180823_081503_add_permission_to_print_shipping
 */
class m180823_081503_add_permission_to_print_shipping extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $permitions = [
            [
                'name' => 'product/order/print-shipping-document',
                'type' => 2,
                'description' => 'Module Product. Print shipping document.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
        ];

        $this->batchInsert(
            '{{%auth_item}}',
            ['name', 'type', 'description', 'created_at', 'updated_at'],
            $permitions
        );

        $data = [];

        foreach ($permitions as $permition) {
            $data[] = [
                'parent' => 'superadmin',
                'child' => $permition['name'],
            ];
        }

        if ($data) {
            $this->batchInsert(
                '{{%auth_item_child}}',
                ['parent', 'child'],
                $data
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return true;
    }
}
