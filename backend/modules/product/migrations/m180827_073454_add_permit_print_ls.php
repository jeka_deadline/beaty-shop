<?php

use yii\db\Migration;

/**
 * Class m180827_073454_add_permit_print_ls
 */
class m180827_073454_add_permit_print_ls extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $permitions = [
            [
                'name' => 'product/order/print-ls',
                'type' => 2,
                'description' => 'Module Product. Print order ls.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
        ];

        $this->batchInsert(
            '{{%auth_item}}',
            ['name', 'type', 'description', 'created_at', 'updated_at'],
            $permitions
        );

        $data = [];

        foreach ($permitions as $permition) {
            $data[] = [
                'parent' => 'superadmin',
                'child' => $permition['name'],
            ];
        }

        if ($data) {
            $this->batchInsert(
                '{{%auth_item_child}}',
                ['parent', 'child'],
                $data
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return true;
    }
}
