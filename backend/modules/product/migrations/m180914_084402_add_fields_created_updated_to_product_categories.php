<?php

use yii\db\Migration;

/**
 * Class m180914_084402_add_fields_created_updated_to_product_categories
 */
class m180914_084402_add_fields_created_updated_to_product_categories extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        $this->addColumn('{{%product_categories}}', 'created_at', $this->timestamp()->null()->defaultValue(null));
        $this->addColumn('{{%product_categories}}', 'updated_at', $this->timestamp()->null()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        $this->dropColumn('{{%product_categories}}', 'created_at');
        $this->dropColumn('{{%product_categories}}', 'updated_at');
    }
}
