<?php

use yii\db\Migration;

/**
 * Class m180419_063636_change_table_product_ratings
 */
class m180419_063636_change_table_product_ratings extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        // drop relation between table `product variation` and table `product ratings`
        $this->dropForeignKey('fk_product_ratings_product_variation_id', '{{%product_ratings}}');
        $this->dropIndex('ix_product_ratings_product_variation_id', '{{%product_ratings}}');

        $this->renameColumn('{{%product_ratings}}', 'product_variation_id', 'product_id');

        // create relation between table `product` and table `product ratings`
        $this->createIndex('ix_product_ratings_product_id', '{{%product_ratings}}', 'product_id');
        $this->addForeignKey(
            'fk_product_ratings_product_id',
            '{{%product_ratings}}',
            'product_id',
            '{{%product_products}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addColumn('{{%product_products}}', 'rating', $this->float()->defaultValue(0) . ' after active');
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        $this->dropColumn('{{%product_products}}', 'rating');

        // drop relation between table `product` and table `product ratings`
        $this->dropForeignKey('fk_product_ratings_product_id', '{{%product_ratings}}');
        $this->dropIndex('ix_product_ratings_product_id', '{{%product_ratings}}');

        $this->renameColumn('{{%product_ratings}}', 'product_id', 'product_variation_id');

        // create relation between table `product variation` and table `product ratings`
        $this->createIndex('ix_product_ratings_product_variation_id', '{{%product_ratings}}', 'product_variation_id');
        $this->addForeignKey(
            'fk_product_ratings_product_variation_id',
            '{{%product_ratings}}',
            'product_variation_id',
            '{{%product_variations}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }
}
