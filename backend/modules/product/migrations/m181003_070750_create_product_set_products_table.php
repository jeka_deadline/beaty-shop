<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product_set_products`.
 */
class m181003_070750_create_product_set_products_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('product_set_products', [
            'id' => $this->primaryKey(),
            'product_set_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'count' => $this->integer()->defaultValue(1),
            'display_order' => $this->integer()->defaultValue(0),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);

        $this->createRelations();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropRelations();
        $this->dropTable('product_set_products');
    }

    private function createRelations()
    {
        $this->createIndex('ix_product_set_products_product_set_id', '{{%product_set_products}}', 'product_set_id');
        $this->addForeignKey(
            'fk_product_set_products_product_set_id',
            '{{%product_set_products}}',
            'product_set_id',
            '{{%product_products}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex('ix_product_set_products_product_id', '{{%product_set_products}}', 'product_id');
        $this->addForeignKey(
            'fk_product_set_products_product_id',
            '{{%product_set_products}}',
            'product_id',
            '{{%product_products}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    private function dropRelations()
    {
        $this->dropForeignKey('fk_product_set_products_product_id','{{%product_set_products}}');
        $this->dropIndex('ix_product_set_products_product_id', '{{%product_set_products}}');

        $this->dropForeignKey('fk_product_set_products_product_set_id','{{%product_set_products}}');
        $this->dropIndex('ix_product_set_products_product_set_id', '{{%product_set_products}}');
    }
}
