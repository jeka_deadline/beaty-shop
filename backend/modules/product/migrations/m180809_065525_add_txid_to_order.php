<?php

use yii\db\Migration;

/**
 * Class m180809_065525_add_txid_to_order
 */
class m180809_065525_add_txid_to_order extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%product_orders}}', 'credit_txid', $this->string(255)->null() . ' after is_new');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%product_orders}}', 'credit_txid');
    }
}
