<?php

use yii\db\Migration;

/**
 * Class m180524_130545_add_colum_show_search_product_categories_table
 */
class m180524_130545_add_colum_show_search_product_categories_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('product_categories', 'show_search', $this->smallInteger(1)->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('product_categories', 'show_search');
    }
}
