<?php

use yii\db\Migration;

/**
 * Class m181002_133302_add_permission_sets
 */
class m181002_133302_add_permission_sets extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        $permitions = [
            [
                'name' => 'product/set/create',
                'type' => 2,
                'description' => 'Module Product. Permission to create a product set.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/set/delete',
                'type' => 2,
                'description' => 'Module Product. Permission to delete a set.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/set/index',
                'type' => 2,
                'description' => 'Module Product. Permission to view the list of sets.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/set/update',
                'type' => 2,
                'description' => 'Module Product. Permission to edit the set.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/set/view',
                'type' => 2,
                'description' => 'Module Product. Permission to view the set.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/set/search-recommendet-products',
                'type' => 2,
                'description' => 'Module Product. Permission to search for products in the section recommended products for the set.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/set/search-set-variations',
                'type' => 2,
                'description' => 'Module Product. Permission to search for variations to add them to the set.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/set/validate-set-items',
                'type' => 2,
                'description' => 'Module Product. Permission to check the form variation set.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
        ];

        $this->batchInsert(
            '{{%auth_item}}',
            ['name', 'type', 'description', 'created_at', 'updated_at'],
            $permitions
        );

        $data = [];

        foreach ($permitions as $permition) {
            $data[] = [
                'parent' => 'superadmin',
                'child' => $permition['name'],
            ];
        }

        if ($data) {
            $this->batchInsert(
                '{{%auth_item_child}}',
                ['parent', 'child'],
                $data
            );
        }
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function safeDown()
    {
        return true;
    }
}
