<?php

use yii\db\Migration;

/**
 * Class m180924_145032_seeder_settings_product_limit_quantity_send_mail
 */
class m180924_145032_seeder_settings_product_limit_quantity_send_mail extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('{{%core_settings}}', [
            'key' => 'product.send.email.limit.quantity',
            'name' => 'Send a message when the quantity of the product is less than the value.',
            'value' => '10',
            'type' => 'text',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
