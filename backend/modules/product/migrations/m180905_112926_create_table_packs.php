<?php

use yii\db\Migration;

/**
 * Class m180905_112926_create_table_packs
 */
class m180905_112926_create_table_packs extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        $this->createTable('{{%product_packs}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'product_pack_id' => $this->integer()->notNull(),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);

        $this->createRelations();
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        $this->dropRelations();
        $this->dropTable('{{%product_packs}}');
    }

    /**
     * Set relations.
     *
     * @access private
     * @return void
     */
    private function createRelations()
    {
        $this->createIndex('ix_product_packs_product_id', '{{%product_packs}}', 'product_id');
        $this->addForeignKey(
            'fk_product_packs_product_id',
            '{{%product_packs}}',
            'product_id',
            '{{%product_products}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex('ix_product_packs_product_pack_id', '{{%product_packs}}', 'product_pack_id');
        $this->addForeignKey(
            'fk_product_packs_product_pack_id',
            '{{%product_packs}}',
            'product_pack_id',
            '{{%product_products}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * Set relations.
     *
     * @access private
     * @return void
     */
    private function dropRelations()
    {
        $this->dropForeignKey('fk_product_packs_product_pack_id', '{{%product_packs}}');
        $this->dropForeignKey('fk_product_packs_product_id', '{{%product_packs}}');

        $this->dropIndex('ix_product_packs_product_pack_id', '{{%product_packs}}');
        $this->dropIndex('ix_product_packs_product_id', '{{%product_packs}}');
    }
}
