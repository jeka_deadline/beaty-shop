<?php

use yii\db\Migration;

/**
 * Class m180627_080810_seeder_settings_currency_free_shipping
 */
class m180627_080810_seeder_settings_currency_free_shipping extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
//        $this->insert('{{%core_settings}}', [
//            'key' => 'order.free.shipping',
//            'name' => 'Free shipping',
//            'value' => '50',
//            'type' => 'text',
//        ]);

        $this->insert('{{%core_settings}}', [
            'key' => 'product.shop.currency',
            'name' => 'The currency to display in the store.',
            'value' => '$',
            'type' => 'text',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
