<?php

use yii\db\Migration;

/**
 * Class m180903_122551_create_product_variation_packs
 */
class m180903_122551_create_product_variation_packs extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        $this->createTable('{{%product_variation_packs}}', [
            'id' => $this->primaryKey(),
            'product_variation_id' => $this->integer(10)->notNull(),
            'count' => $this->integer(10)->notNull(),
            'price' => $this->double()->notNull(),
            'promotion' => $this->double()->defaultValue(0),
        ]);

        $this->createRelations();
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        $this->dropRelations();
        $this->dropTable('{{%product_variation_packs}}');
    }

    /**
     * Create ralations between tables.
     *
     * @access private
     * @return void
     */
    private function createRelations()
    {
        $this->createIndex('ix_product_variation_packs_product_variation_id', '{{%product_variation_packs}}', 'product_variation_id');
        $this->addForeignKey(
            'fk_product_variation_packs_product_variation_id',
            '{{%product_variation_packs}}',
            'product_variation_id',
            '{{%product_variations}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * Drop ralations between tables.
     *
     * @access private
     * @return void
     */
    private function dropRelations()
    {
        $this->dropForeignKey('fk_product_variation_packs_product_variation_id','{{%product_variation_packs}}');
        $this->dropIndex('ix_product_variation_packs_product_variation_id', '{{%product_variation_packs}}');
    }
}
