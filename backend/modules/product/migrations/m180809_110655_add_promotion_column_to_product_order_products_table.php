<?php

use yii\db\Migration;

/**
 * Handles adding promotion to table `product_order_products`.
 */
class m180809_110655_add_promotion_column_to_product_order_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('product_order_products', 'promotion','REAL NULL DEFAULT 0');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('product_order_products', 'promotion');
    }
}
