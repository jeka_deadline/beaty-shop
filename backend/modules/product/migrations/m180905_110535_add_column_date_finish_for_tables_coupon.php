<?php

use yii\db\Migration;

/**
 * Class m180905_110535_add_column_date_finish_for_tables_coupon
 */
class m180905_110535_add_column_date_finish_for_tables_coupon extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        $this->addColumn('{{%product_coupons}}', 'finish_date', $this->timestamp()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        $this->dropColumn('{{%product_coupons}}', 'finish_date');
    }
}
