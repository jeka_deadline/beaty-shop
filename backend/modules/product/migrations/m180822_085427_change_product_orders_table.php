<?php

use yii\db\Migration;

/**
 * Class m180822_085427_change_product_orders_table
 */
class m180822_085427_change_product_orders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('{{%product_orders}}', 'sum_without_coupon', 'sum_discount');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return true;
    }
}
