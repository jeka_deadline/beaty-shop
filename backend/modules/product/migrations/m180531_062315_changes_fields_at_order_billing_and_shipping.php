<?php

use yii\db\Migration;

/**
 * Class m180531_062315_changes_fields_at_order_billing_and_shipping
 */
class m180531_062315_changes_fields_at_order_billing_and_shipping extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        $this->addColumn('{{%product_order_shipping_information}}', 'title', $this->string(10)->null() . ' after shipping_tracking_number');
        $this->addColumn('{{%product_order_billing_information}}', 'title', $this->string(10)->null() . ' after order_id');

        $this->addColumn('{{%product_order_shipping_information}}', 'company', $this->string(255)->null() . ' after name');
        $this->addColumn('{{%product_order_billing_information}}', 'company', $this->string(255)->null() . ' after name');

        // drop relations between table `product_order_shipping_information` and table `geo_cities`
        $this->dropForeignKey('fk_product_order_shipping_information_city_id', '{{%product_order_shipping_information}}');
        $this->dropIndex('ix_product_order_shipping_information_city_id', '{{%product_order_shipping_information}}');

        // drop relations between table `product_order_billing_information` and table `geo_cities`
        $this->dropForeignKey('fk_product_order_billing_information_city_id', '{{%product_order_billing_information}}');
        $this->dropIndex('ix_product_order_billing_information_city_id', '{{%product_order_billing_information}}');

        $this->dropColumn('{{%product_order_shipping_information}}', 'city_id');
        $this->dropColumn('{{%product_order_billing_information}}', 'city_id');

        $this->addColumn('{{%product_order_shipping_information}}', 'city', $this->string(255)->notNull() . ' after country_id');
        $this->addColumn('{{%product_order_billing_information}}', 'city', $this->string(255)->notNull() . ' after country_id');
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        $this->dropColumn('{{%product_order_shipping_information}}', 'title');
        $this->dropColumn('{{%product_order_billing_information}}', 'title');

        $this->dropColumn('{{%product_order_shipping_information}}', 'company');
        $this->dropColumn('{{%product_order_billing_information}}', 'company');

        $this->dropColumn('{{%product_order_shipping_information}}', 'city');
        $this->dropColumn('{{%product_order_billing_information}}', 'city');

        $this->addColumn('{{%product_order_shipping_information}}', 'city_id', $this->integer(11)->defaultValue(null), ' after country_id');
        $this->addColumn('{{%product_order_billing_information}}', 'city_id', $this->integer(11)->defaultValue(null), ' after country_id');

        // create relations between table `product_order_shipping_information` and table `geo_cities`
        $this->createIndex('ix_product_order_shipping_information_city_id', '{{%product_order_shipping_information}}', 'city_id');
        $this->addForeignKey(
            'fk_product_order_shipping_information_city_id',
            '{{%product_order_shipping_information}}',
            'city_id',
            '{{%geo_cities}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        // create relations between table `product_order_billing_information` and table `geo_cities`
        $this->createIndex('ix_product_order_billing_information_city_id', '{{%product_order_billing_information}}', 'city_id');
        $this->addForeignKey(
            'fk_product_order_billing_information_city_id',
            '{{%product_order_billing_information}}',
            'city_id',
            '{{%geo_cities}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }
}
