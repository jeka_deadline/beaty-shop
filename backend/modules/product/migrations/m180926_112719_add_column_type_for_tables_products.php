<?php

use yii\db\Migration;

/**
 * Class m180926_112719_add_column_type_for_tables_products
 */
class m180926_112719_add_column_type_for_tables_products extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%product_products}}', 'type', $this->string(12)->defaultValue('product'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%product_products}}', 'type');
    }

}
