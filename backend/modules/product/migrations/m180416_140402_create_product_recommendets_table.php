<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product_recommendets`.
 */
class m180416_140402_create_product_recommendets_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('product_recommendets', [
            'id' => $this->primaryKey(),
            'product_variation_id' => $this->integer()->notNull(),
            'product_variation_recommended_id' => $this->integer()->notNull(),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);

        $this->createRelations();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropRelations();
        $this->dropTable('product_recommendets');
    }

    private function createRelations()
    {
        $this->createIndex('ix_product_recommendets_product_variation_id', '{{%product_recommendets}}', 'product_variation_id');
        $this->addForeignKey(
            'fk_product_recommendets_product_variation_id',
            '{{%product_recommendets}}',
            'product_variation_id',
            '{{%product_variations}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->createIndex('ix_product_recommendets_product_variation_recommended_id', '{{%product_recommendets}}', 'product_variation_recommended_id');
        $this->addForeignKey(
            'fk_product_recommendets_product_variation_recommended_id',
            '{{%product_recommendets}}',
            'product_variation_recommended_id',
            '{{%product_variations}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    private function dropRelations()
    {
        $this->dropForeignKey('fk_product_recommendets_product_variation_recommended_id','{{%product_recommendets}}');
        $this->dropIndex('ix_product_recommendets_product_variation_recommended_id', '{{%product_recommendets}}');

        $this->dropForeignKey('fk_product_recommendets_product_variation_id','{{%product_recommendets}}');
        $this->dropIndex('ix_product_recommendets_product_variation_id', '{{%product_recommendets}}');
    }
}
