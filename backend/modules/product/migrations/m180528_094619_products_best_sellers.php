<?php

use yii\db\Migration;

/**
 * Class m180528_094619_products_best_sellers
 */
class m180528_094619_products_best_sellers extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        $this->createTable('{{%product_best_sellers}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'display_order' => $this->integer()->defaultValue(0),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);

        $this->createRelations();
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        $this->dropRelations();
        $this->dropTable('{{%product_best_sellers}}');
    }

    /**
     * Create ralations between tables.
     *
     * @return void
     */
    private function createRelations()
    {
        $this->createIndex('ix_product_best_sellers_product_id', '{{%product_best_sellers}}', 'product_id');
        $this->addForeignKey(
            'fk_product_best_sellers_product_id',
            '{{%product_best_sellers}}',
            'product_id',
            '{{%product_products}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * Drop ralations between tables.
     *
     * @return void
     */
    private function dropRelations()
    {
        $this->dropForeignKey('fk_product_best_sellers_product_id','{{%product_best_sellers}}');
        $this->dropIndex('ix_product_best_sellers_product_id', '{{%product_best_sellers}}');
    }
}
