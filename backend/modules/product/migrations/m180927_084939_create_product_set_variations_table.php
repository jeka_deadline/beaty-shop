<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product_set_variations`.
 */
class m180927_084939_create_product_set_variations_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('product_set_variations', [
            'id' => $this->primaryKey(),
            'product_set_id' => $this->integer()->notNull(),
            'product_variation_id' => $this->integer()->notNull(),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);

        $this->createRelations();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropRelations();
        $this->dropTable('product_set_variations');
    }

    private function createRelations()
    {
        $this->createIndex('ix_product_set_variations_product_set_id', '{{%product_set_variations}}', 'product_set_id');
        $this->addForeignKey(
            'fk_product_set_variations_product_set_id',
            '{{%product_set_variations}}',
            'product_set_id',
            '{{%product_products}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex('ix_product_set_variations_product_variation_id', '{{%product_set_variations}}', 'product_variation_id');
        $this->addForeignKey(
            'fk_product_set_variations_product_variation_id',
            '{{%product_set_variations}}',
            'product_variation_id',
            '{{%product_variations}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    private function dropRelations()
    {
        $this->dropForeignKey('fk_product_set_variations_product_variation_id','{{%product_set_variations}}');
        $this->dropIndex('ix_product_set_variations_product_variation_id', '{{%product_set_variations}}');

        $this->dropForeignKey('fk_product_set_variations_product_set_id','{{%product_set_variations}}');
        $this->dropIndex('ix_product_set_variations_product_set_id', '{{%product_set_variations}}');
    }
}
