<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product_reviews`.
 */
class m180411_113548_create_product_reviews_table extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        // table product reviews
        $this->createTable('{{%product_reviews}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'product_variation_id' => $this->integer()->notNull(),
            'parent_id' => $this->integer()->defaultValue(0),
            'text' => $this->text(),
            'active' => $this->smallInteger(1)->defaultValue(0),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);

        // table product ratings
        $this->createTable('{{%product_ratings}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'product_variation_id' => $this->integer()->notNull(),
            'value' => $this->float()->defaultValue(0),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);

        $this->createRelations();
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        $this->dropRelations();

        $this->dropTable('{{%product_ratings}}');
        $this->dropTable('{{%product_reviews}}');
    }

    /**
     * Create relations.
     *
     * @return void
     */
    private function createRelations()
    {
        // create relation between table `users` and table `product reviews`
        $this->createIndex('ix_product_reviews_user_id', '{{%product_reviews}}', 'user_id');
        $this->addForeignKey(
            'fk_product_reviews_user_id',
            '{{%product_reviews}}',
            'user_id',
            '{{%user_users}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        // create relation between table `product properties` and table `product reviews`
        $this->createIndex('ix_product_reviews_product_variation_id', '{{%product_reviews}}', 'product_variation_id');
        $this->addForeignKey(
            'fk_product_reviews_product_variation_id',
            '{{%product_reviews}}',
            'product_variation_id',
            '{{%product_variations}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        // create relation between table `users` and table `product ratings`
        $this->createIndex('ix_product_ratings_user_id', '{{%product_ratings}}', 'user_id');
        $this->addForeignKey(
            'fk_product_ratings_user_id',
            '{{%product_ratings}}',
            'user_id',
            '{{%user_users}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        // create relation between table `product properties` and table `product ratings`
        $this->createIndex('ix_product_ratings_product_variation_id', '{{%product_ratings}}', 'product_variation_id');
        $this->addForeignKey(
            'fk_product_ratings_product_variation_id',
            '{{%product_ratings}}',
            'product_variation_id',
            '{{%product_variations}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * Drop relations.
     *
     * @return void
     */
    private function dropRelations()
    {
        // drop relation between table `product properties` and table `product ratings`
        $this->dropForeignKey('fk_product_ratings_product_variation_id', '{{%product_ratings}}');
        $this->dropIndex('ix_product_ratings_product_variation_id', '{{%product_ratings}}');

        // drop relation between table `users` and table `product ratings`
        $this->dropForeignKey('fk_product_ratings_user_id', '{{%product_ratings}}');
        $this->dropIndex('ix_product_ratings_user_id', '{{%product_ratings}}');

        // drop relation between table `product properties` and table `product reviews`
        $this->dropForeignKey('fk_product_reviews_product_variation_id', '{{%product_reviews}}');
        $this->dropIndex('ix_product_reviews_product_variation_id', '{{%product_reviews}}');

        // drop relation between table `users` and table `product reviews`
        $this->dropForeignKey('fk_product_reviews_user_id', '{{%product_reviews}}');
        $this->dropIndex('ix_product_reviews_user_id', '{{%product_reviews}}');
    }
}
