<?php

use yii\db\Migration;

class m160610_134100_coupons extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function up()
    {
        // table coupons
        $this->createTable('{{%product_coupons}}', [
            'id'         => $this->primaryKey(),
            'code'       => $this->string(255)->unique()->notNull(),
            'type'       => 'ENUM("fix", "percent")',
            'value'      => $this->float()->notNull(),
            'active'     => $this->smallInteger()->defaultValue(1),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function down()
    {
        $this->dropTable('{{%product_coupons}}');
    }
}
