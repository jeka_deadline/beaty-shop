<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `active_column_product_free_recommendets`.
 */
class m180529_093827_drop_active_column_product_free_recommendets_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('product_free_recommendets', 'active');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('product_free_recommendets', 'active', $this->smallInteger(1)->defaultValue(0));
    }
}
