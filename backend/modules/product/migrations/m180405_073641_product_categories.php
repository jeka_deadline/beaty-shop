<?php

use yii\db\Migration;

/**
 * Class m180405_073641_product_categories
 */
class m180405_073641_product_categories extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        // table product categories
        $this->createTable('{{%product_categories}}', [
            'id'               => $this->primaryKey(),
            'parent_id'        => $this->integer()->defaultValue(0),
            'name'             => $this->string(255)->notNull(),
            'slug'             => $this->string(255)->notNull()->unique(),
            'description'      => $this->text()->defaultValue(null),
            'display_order'    => $this->integer()->defaultValue(0),
            'active'           => $this->smallInteger(1)->defaultValue(1),
            'meta_title'       => $this->string(255)->null(),
            'meta_description' => $this->text()->null(),
            'meta_keywords'    => $this->string(255)->null(),
        ]);

        // table product category lang fieds
        $this->createTable('{{%product_category_lang_fields}}', [
            'id' => $this->primaryKey(),
            'product_category_id' => $this->integer()->notNull(),
            'lang_id' => $this->integer()->notNull(),
            'name' => $this->string(255)->null(null),
            'description' => $this->string(255)->null(),
            'meta_title' => $this->string(255)->null(),
            'meta_keywords' => $this->string(255)->null(),
            'meta_description' => $this->text()->null(),
        ]);

        $this->createRelations();
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        $this->dropRelations();
        $this->dropTable('{{%product_category_lang_fields}}');
        $this->dropTable('{{%product_categories}}');
    }

    private function createRelations()
    {
        $this->createIndex('ix_product_category_lang_fields_lang_id', '{{%product_category_lang_fields}}', 'lang_id');
        $this->addForeignKey(
            'fk_product_category_lang_fields_lang_id',
            '{{%product_category_lang_fields}}',
            'lang_id',
            '{{%core_languages}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex('ix_product_category_lang_fields_product_category_id', '{{%product_category_lang_fields}}', 'product_category_id');
        $this->addForeignKey(
            'fk_product_category_lang_fields_product_category_id',
            '{{%product_category_lang_fields}}',
            'product_category_id',
            '{{%product_categories}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    private function dropRelations()
    {
        $this->dropForeignKey('fk_product_category_lang_fields_lang_id', '{{%product_category_lang_fields}}');
        $this->dropIndex('ix_product_category_lang_fields_lang_id', '{{%product_category_lang_fields}}');

        $this->dropForeignKey('fk_product_category_lang_fields_product_category_id', '{{%product_category_lang_fields}}');
        $this->dropIndex('ix_product_category_lang_fields_product_category_id', '{{%product_category_lang_fields}}');
    }
}
