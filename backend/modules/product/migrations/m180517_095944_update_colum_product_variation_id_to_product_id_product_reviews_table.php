<?php

use yii\db\Migration;

/**
 * Class m180517_095944_update_colum_product_variation_id_to_product_id_product_reviews_table
 */
class m180517_095944_update_colum_product_variation_id_to_product_id_product_reviews_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropOldRelations();
        $this->dropColumn('product_reviews', 'product_variation_id');

        $this->addColumn('product_reviews', 'product_id', $this->integer()->notNull());
        $this->createRelations();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropRelations();
        $this->dropColumn('product_reviews', 'product_id');

        $this->addColumn('product_reviews', 'product_variation_id',$this->integer()->notNull());
        $this->createOldRelations();
    }

    /**
     * Create relations between tables.
     *
     * @return void
     */
    private function createRelations()
    {
        $this->createIndex('ix_product_reviews_product_id', '{{%product_reviews}}', 'product_id');
        $this->addForeignKey(
            'fk_product_reviews_product_id',
            '{{%product_reviews}}',
            'product_id',
            '{{%product_products}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    private function dropRelations()
    {
        $this->dropForeignKey('fk_product_reviews_product_id', '{{%product_reviews}}');
        $this->dropIndex('ix_product_reviews_product_id', '{{%product_reviews}}');
    }

    private function createOldRelations()
    {
        $this->createIndex('ix_product_reviews_product_variation_id', '{{%product_reviews}}', 'product_variation_id');
        $this->addForeignKey(
            'fk_product_reviews_product_variation_id',
            '{{%product_reviews}}',
            'product_variation_id',
            '{{%product_variations}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    private function dropOldRelations()
    {
        $this->dropForeignKey('fk_product_reviews_product_variation_id','{{%product_reviews}}');
        $this->dropIndex('ix_product_reviews_product_variation_id', '{{%product_reviews}}');
    }

}
