<?php

use yii\db\Migration;

/**
 * Class m180924_144206_create_column_admin_answer_to_product_review
 */
class m180924_144206_create_column_admin_answer_to_product_review extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        $this->addColumn('{{%product_reviews}}', 'admin_answer', $this->string(255)->null() . ' after text');
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        $this->dropColumn('{{%product_reviews}}', 'admin_answer');
    }
}
