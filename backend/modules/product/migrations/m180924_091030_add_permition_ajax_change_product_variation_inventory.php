<?php

use yii\db\Migration;

/**
 * Class m180924_091030_add_permition_ajax_change_product_variation_inventory
 */
class m180924_091030_add_permition_ajax_change_product_variation_inventory extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        $permitions = [
            [
                'name' => 'product/product-variation/change-count',
                'type' => 2,
                'description' => 'Module Product. Change product variation count',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
        ];

        $this->batchInsert(
            '{{%auth_item}}',
            ['name', 'type', 'description', 'created_at', 'updated_at'],
            $permitions
        );

        $data = [];

        foreach ($permitions as $permition) {
            $data[] = [
                'parent' => 'superadmin',
                'child' => $permition['name'],
            ];
        }

        if ($data) {
            $this->batchInsert(
                '{{%auth_item_child}}',
                ['parent', 'child'],
                $data
            );
        }
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function safeDown()
    {
        return true;
    }
}
