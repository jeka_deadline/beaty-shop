<?php

use yii\db\Migration;

/**
 * Class m180906_132852_swith_foreign_key_variations_to_products
 */
class m180906_132852_swith_foreign_key_variations_to_products extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk_product_free_recommendets_product_recommended_id','{{%product_free_recommendets}}');

        $this->addForeignKey(
            'fk_product_free_recommendets_product_recommended_id',
            '{{%product_free_recommendets}}',
            'product_recommended_id',
            '{{%product_products}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_product_free_recommendets_product_recommended_id','{{%product_free_recommendets}}');

        $this->addForeignKey(
            'fk_product_free_recommendets_product_recommended_id',
            '{{%product_free_recommendets}}',
            'product_recommended_id',
            '{{%product_variations}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        return true;
    }
}
