<?php

use yii\db\Migration;

/**
 * Class m180928_090101_add_colum_count_and_display_order_product_set_variations_table
 */
class m180928_090101_add_colum_count_and_display_order_product_set_variations_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('product_set_variations', 'count', $this->integer()->defaultValue(1));
        $this->addColumn('product_set_variations', 'display_order', $this->integer()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('product_set_variations', 'count');
        $this->dropColumn('product_set_variations', 'display_order');
    }
}
