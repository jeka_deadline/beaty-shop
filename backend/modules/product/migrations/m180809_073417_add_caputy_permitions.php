<?php

use yii\db\Migration;

/**
 * Class m180809_073417_add_caputy_permitions
 */
class m180809_073417_add_caputy_permitions extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
         $permitions=[
            [
                'name' => 'product/order/caputy',
                'type' => 2,
                'description' => 'Module Product. Permission caputy order.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
        ];

        $this->batchInsert(
            '{{%auth_item}}',
            ['name', 'type', 'description', 'created_at', 'updated_at'],
            $permitions
        );

        $data = [];

        foreach ($permitions as $permition) {
            $data[] = [
                'parent' => 'superadmin',
                'child' => $permition['name'],
            ];
        }

        if ($data) {
            $this->batchInsert(
                '{{%auth_item_child}}',
                ['parent', 'child'],
                $data
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180809_073417_add_caputy_permitions cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180809_073417_add_caputy_permitions cannot be reverted.\n";

        return false;
    }
    */
}
