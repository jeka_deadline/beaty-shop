<?php

use yii\db\Migration;

/**
 * Class m180914_064013_change_product_reviews_table
 */
class m180914_064013_change_product_reviews_table extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        $this->addColumn('{{%product_reviews}}', 'username', $this->string(255)->null() . ' after parent_id');
        $this->addColumn('{{%product_reviews}}', 'surname', $this->string(255)->null() . ' after username');
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        $this->dropColumn('{{%product_reviews}}', 'username');
        $this->dropColumn('{{%product_reviews}}', 'surname');
    }
}
