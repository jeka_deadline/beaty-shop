<?php

use yii\db\Migration;

/**
 * Class m180516_092358_add_colum_subject_product_ratings_table
 */
class m180516_092358_add_colum_subject_product_ratings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('product_reviews', 'subject', $this->string(255));
        $this->addColumn('product_reviews', 'rating_id', $this->integer()->null());

        $this->createRelations();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropRelations();

        $this->dropColumn('product_reviews', 'subject');
        $this->dropColumn('product_reviews', 'rating_id');
    }

    /**
     * Create relations between tables.
     *
     * @return void
     */
    private function createRelations()
    {
        $this->createIndex('ix_product_reviews_rating_id', '{{%product_reviews}}', 'rating_id');
        $this->addForeignKey(
            'fk_product_reviews_rating_id',
            '{{%product_reviews}}',
            'rating_id',
            '{{%product_ratings}}',
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    private function dropRelations()
    {
        $this->dropForeignKey('fk_product_reviews_rating_id', '{{%product_reviews}}');
        $this->dropIndex('ix_product_reviews_rating_id', '{{%product_reviews}}');
    }
}
