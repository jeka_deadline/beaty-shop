<?php

use yii\db\Migration;

/**
 * Class m181003_113229_add_permission_edit_sets
 */
class m181003_113229_add_permission_edit_sets extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        $permitions = [
            [
                'name' => 'product/edit-set/create',
                'type' => 2,
                'description' => 'Module Product. Permission to create a product edit set.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/edit-set/delete',
                'type' => 2,
                'description' => 'Module Product. Permission to delete a edit set.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/edit-set/index',
                'type' => 2,
                'description' => 'Module Product. Permission to view the list of edit sets.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/edit-set/update',
                'type' => 2,
                'description' => 'Module Product. Permission to edit the edit set.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/edit-set/view',
                'type' => 2,
                'description' => 'Module Product. Permission to view the edit set.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/edit-set/search-recommendet-products',
                'type' => 2,
                'description' => 'Module Product. Permission to search for products in the section recommended products for the edit set.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/edit-set/search-set-products',
                'type' => 2,
                'description' => 'Module Product. Permission to search for products to add them to the edit set.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/edit-set/validate-set-items',
                'type' => 2,
                'description' => 'Module Product. Permission to check the form product edit set.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
        ];

        $this->batchInsert(
            '{{%auth_item}}',
            ['name', 'type', 'description', 'created_at', 'updated_at'],
            $permitions
        );

        $data = [];

        foreach ($permitions as $permition) {
            $data[] = [
                'parent' => 'superadmin',
                'child' => $permition['name'],
            ];
        }

        if ($data) {
            $this->batchInsert(
                '{{%auth_item_child}}',
                ['parent', 'child'],
                $data
            );
        }
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function safeDown()
    {
        return true;
    }
}
