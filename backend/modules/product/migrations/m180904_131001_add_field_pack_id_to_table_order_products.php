<?php

use yii\db\Migration;

/**
 * Class m180904_131001_add_field_pack_id_to_table_order_products
 */
class m180904_131001_add_field_pack_id_to_table_order_products extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        $this->addColumn('{{%product_order_products}}', 'pack_id', $this->integer(10)->defaultValue(null));

        $this->createIndex('ix_product_order_products_pack_id', '{{%product_order_products}}', 'pack_id');
        $this->addForeignKey(
            'fk_product_order_products_pack_id',
            '{{%product_order_products}}',
            'pack_id',
            '{{%product_variation_packs}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_product_order_products_pack_id', '{{%product_order_products}}');
        $this->dropIndex('ix_product_order_products_pack_id', '{{%product_order_products}}');
        $this->dropColumn('{{%product_order_products}}', 'pack_id');
    }
}
