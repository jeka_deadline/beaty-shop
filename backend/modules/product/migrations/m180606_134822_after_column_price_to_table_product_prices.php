<?php

use yii\db\Migration;

/**
 * Class m180606_134822_after_column_price_to_table_product_prices
 */
class m180606_134822_after_column_price_to_table_product_prices extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%product_prices}}', 'price', 'REAL NULL DEFAULT 0');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('{{%product_prices}}', 'price', $this->float()->defaultValue(0));
    }
}
