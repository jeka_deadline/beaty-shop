<?php

use yii\db\Migration;

/**
 * Class m180516_140349_add_colum_default_variation_id_product_products_table
 */
class m180516_140349_add_colum_default_variation_id_product_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('product_products', 'default_variation_id', $this->integer()->null());

        $this->createRelations();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropRelations();

        $this->dropColumn('product_products', 'default_variation_id');
    }

    /**
     * Create relations between tables.
     *
     * @return void
     */
    private function createRelations()
    {
        $this->createIndex('ix_product_products_default_variation_id', '{{%product_products}}', 'default_variation_id');
        $this->addForeignKey(
            'fk_product_products_default_variation_id',
            '{{%product_products}}',
            'default_variation_id',
            '{{%product_variations}}',
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    private function dropRelations()
    {
        $this->dropForeignKey('fk_product_products_default_variation_id', '{{%product_products}}');
        $this->dropIndex('ix_product_products_default_variation_id', '{{%product_products}}');
    }
}
