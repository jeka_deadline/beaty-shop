<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product_free_recommendets`.
 */
class m180515_122046_create_product_free_recommendets_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('product_free_recommendets', [
            'id' => $this->primaryKey(),
            'product_variation_recommended_id' => $this->integer()->notNull(),
            'active' => $this->smallInteger(1)->defaultValue(0),
            'display_order'    => $this->integer()->defaultValue(0),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);

        $this->createRelations();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropRelations();
        $this->dropTable('product_free_recommendets');
    }

    private function createRelations()
    {
        $this->createIndex('ix_product_free_recommendets_product_variation_recommended_id', '{{%product_free_recommendets}}', 'product_variation_recommended_id');
        $this->addForeignKey(
            'fk_product_free_recommendets_product_variation_recommended_id',
            '{{%product_free_recommendets}}',
            'product_variation_recommended_id',
            '{{%product_variations}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    private function dropRelations()
    {
        $this->dropForeignKey('fk_product_free_recommendets_product_variation_recommended_id','{{%product_free_recommendets}}');
        $this->dropIndex('ix_product_free_recommendets_product_variation_recommended_id', '{{%product_free_recommendets}}');
    }
}
