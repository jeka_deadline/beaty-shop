<?php

use yii\db\Migration;

/**
 * Class m180525_145845_add_colum_display_order_menu_product_categories_table
 */
class m180525_145845_add_colum_display_order_menu_product_categories_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('product_categories', 'display_order_hmenu', $this->integer()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('product_categories', 'display_order_hmenu');
    }

}
