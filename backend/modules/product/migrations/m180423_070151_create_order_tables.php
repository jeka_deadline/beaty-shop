<?php

use yii\db\Migration;

/**
 * Class m180423_070151_create_order_tables
 */
class m180423_070151_create_order_tables extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        // create table `product_orders`
        $this->createTable('{{%product_orders}}', [
            'id'           => $this->primaryKey(),
            'order_number' => $this->string()->notNull(),
            'user_id'      => $this->integer()->null(),
            'type_payment' => $this->string(20)->notNull(),
            'status'       => $this->string(20)->notNull(),
            'total_sum'    => $this->float()->notNull(),
            'created_at'   => $this->timestamp()->null()->defaultValue(null),
            'updated_at'   => $this->timestamp()->null()->defaultValue(null),
        ]);

        // create table `product_order_products`
        $this->createTable('{{%product_order_products}}', [
            'id'                     => $this->primaryKey(),
            'order_id'               => $this->integer()->notNull(),
            'product_variation_id'   => $this->integer()->notNull(),
            'product_variation_name' => $this->string(255)->notNull(),
            'count'                  => $this->integer()->notNull(),
            'price'                  => $this->float()->notNull(),
            'created_at'             => $this->timestamp()->null()->defaultValue(null),
            'updated_at'             => $this->timestamp()->null()->defaultValue(null),
        ]);

        // create table `product_order_shipping_information`
        $this->createTable('{{%product_order_shipping_information}}', [
            'id'                       => $this->primaryKey(),
            'order_id'                 => $this->integer()->notNull(),
            'shipping_method'          => $this->string(20)->notNull(),
            'shipping_tracking_number' => $this->string(20)->null(),
            'surname'                  => $this->string(50)->notNull(),
            'name'                     => $this->string(50)->notNull(),
            'country_id'               => $this->integer()->notNull(),
            'city_id'                  => $this->integer()->notNull(),
            'address1'                 => $this->text()->notNull(),
            'address2'                 => $this->text()->null(),
            'zip'                      => $this->string(5)->notNull(),
            'phone'                    => $this->string(16)->notNull(),
            'email'                    => $this->string(255)->notNull(),
            'created_at'               => $this->timestamp()->null()->defaultValue(null),
            'updated_at'               => $this->timestamp()->null()->defaultValue(null),
        ]);

        // create table `product_order_billing_information`
        $this->createTable('{{%product_order_billing_information}}', [
            'id'         => $this->primaryKey(),
            'order_id'   => $this->integer()->notNull(),
            'surname'    => $this->string(50)->notNull(),
            'name'       => $this->string(50)->notNull(),
            'country_id' => $this->integer()->notNull(),
            'city_id'    => $this->integer()->notNull(),
            'address1'   => $this->text()->notNull(),
            'address2'   => $this->text()->null(),
            'zip'        => $this->string(5)->notNull(),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);

        $this->createRelations();
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        $this->dropRelations();

        $this->dropTable('{{%product_order_billing_information}}');
        $this->dropTable('{{%product_order_shipping_information}}');
        $this->dropTable('{{%product_order_products}}');
        $this->dropTable('{{%product_orders}}');
    }

    /**
     * Create relations between tables.
     *
     * @return void
     */
    private function createRelations()
    {
        // create relations between table `product_orders` and table `user_users`
        $this->createIndex('ix_product_orders_user_id', '{{%product_orders}}', 'user_id');
        $this->addForeignKey(
            'fk_product_orders_user_id',
            '{{%product_orders}}',
            'user_id',
            '{{%user_users}}',
            'id',
            'SET NULL',
            'CASCADE'
        );

        // ====================== Start block relations in table `product_order_products` =========================

        // create relations between table `product_order_products` and table `product_orders`
        $this->createIndex('ix_product_order_products_order_id', '{{%product_order_products}}', 'order_id');
        $this->addForeignKey(
            'fk_product_order_products_order_id',
            '{{%product_order_products}}',
            'order_id',
            '{{%product_orders}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        // create relations between table `product_order_products` and table `product_variations`
        $this->createIndex('ix_product_order_products_product_variation_id', '{{%product_order_products}}', 'product_variation_id');
        $this->addForeignKey(
            'fk_product_order_products_product_variation_id',
            '{{%product_order_products}}',
            'product_variation_id',
            '{{%product_variations}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        // ====================== End block relations in table `product_order_products` =======================================
        // ====================== Start block relations in table `product_order_shipping_information` =========================

        // create relations between table `product_order_shipping_information` and table `product_orders`
        $this->createIndex('ix_product_order_shipping_information_order_id', '{{%product_order_shipping_information}}', 'order_id');
        $this->addForeignKey(
            'fk_product_order_shipping_information_order_id',
            '{{%product_order_shipping_information}}',
            'order_id',
            '{{%product_orders}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        // create relations between table `product_order_shipping_information` and table `geo_countries`
        $this->createIndex('ix_product_order_shipping_information_country_id', '{{%product_order_shipping_information}}', 'country_id');
        $this->addForeignKey(
            'fk_product_order_shipping_information_country_id',
            '{{%product_order_shipping_information}}',
            'country_id',
            '{{%geo_countries}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        // create relations between table `product_order_shipping_information` and table `geo_cities`
        $this->createIndex('ix_product_order_shipping_information_city_id', '{{%product_order_shipping_information}}', 'city_id');
        $this->addForeignKey(
            'fk_product_order_shipping_information_city_id',
            '{{%product_order_shipping_information}}',
            'city_id',
            '{{%geo_cities}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        // ====================== End block relations in table `product_order_shipping_information` ===========================
        // ====================== Start block relations in table `product_order_billing_information` =========================

        // create relations between table `product_order_billing_information` and table `product_orders`
        $this->createIndex('ix_product_order_billing_information_order_id', '{{%product_order_billing_information}}', 'order_id');
        $this->addForeignKey(
            'fk_product_order_billing_information_order_id',
            '{{%product_order_billing_information}}',
            'order_id',
            '{{%product_orders}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        // create relations between table `product_order_billing_information` and table `geo_countries`
        $this->createIndex('ix_product_order_billing_information_country_id', '{{%product_order_billing_information}}', 'country_id');
        $this->addForeignKey(
            'fk_product_order_billing_information_country_id',
            '{{%product_order_billing_information}}',
            'country_id',
            '{{%geo_countries}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        // create relations between table `product_order_billing_information` and table `geo_cities`
        $this->createIndex('ix_product_order_billing_information_city_id', '{{%product_order_billing_information}}', 'city_id');
        $this->addForeignKey(
            'fk_product_order_billing_information_city_id',
            '{{%product_order_billing_information}}',
            'city_id',
            '{{%geo_cities}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        // ====================== End block relations in table `product_order_billing_information` =========================
    }

    /**
     * Drop relations between tables.
     *
     * @return void
     */
    private function dropRelations()
    {
        // ====================== Start block relations in table `product_order_billing_information` ======================

        // drop relations between table `product_order_billing_information` and table `geo_cities`
        $this->dropForeignKey('fk_product_order_billing_information_city_id', '{{%product_order_billing_information}}');
        $this->dropIndex('ix_product_order_billing_information_city_id', '{{%product_order_billing_information}}');

        // drop relations between table `product_order_billing_information` and table `geo_countries`
        $this->dropForeignKey('fk_product_order_billing_information_country_id', '{{%product_order_billing_information}}');
        $this->dropIndex('ix_product_order_billing_information_country_id', '{{%product_order_billing_information}}');

        // drop relations between table `product_order_billing_information` and table `product_orders`
        $this->dropForeignKey('fk_product_order_billing_information_order_id', '{{%product_order_billing_information}}');
        $this->dropIndex('ix_product_order_billing_information_order_id', '{{%product_order_billing_information}}');

        // ====================== End block relations in table `product_order_billing_information` =========================
        // ====================== Start block relations in table `product_order_shipping_information` ======================

        // drop relations between table `product_order_shipping_information` and table `geo_cities`
        $this->dropForeignKey('fk_product_order_shipping_information_city_id', '{{%product_order_shipping_information}}');
        $this->dropIndex('ix_product_order_shipping_information_city_id', '{{%product_order_shipping_information}}');

        // drop relations between table `product_order_shipping_information` and table `geo_countries`
        $this->dropForeignKey('fk_product_order_shipping_information_country_id', '{{%product_order_shipping_information}}');
        $this->dropIndex('ix_product_order_shipping_information_country_id', '{{%product_order_shipping_information}}');

        // drop relations between table `product_order_shipping_information` and table `product_orders`
        $this->dropForeignKey('fk_product_order_shipping_information_order_id', '{{%product_order_shipping_information}}');
        $this->dropIndex('ix_product_order_shipping_information_order_id', '{{%product_order_shipping_information}}');

        // ====================== End block relations in table `product_order_shipping_information` =========================
        // ====================== Start block relations in table `product_order_products` ===================================

        // drop relations between table `product_order_products` and table `product_variations`
        $this->dropForeignKey('fk_product_order_products_product_variation_id', '{{%product_order_products}}');
        $this->dropIndex('ix_product_order_products_product_variation_id', '{{%product_order_products}}');

        // drop relations between table `product_order_products` and table `product_orders`
        $this->dropForeignKey('fk_product_order_products_order_id', '{{%product_order_products}}');
        $this->dropIndex('ix_product_order_products_order_id', '{{%product_order_products}}');

        // ====================== End block relations in table `product_order_products` =====================================

        // drop relations between table `product_orders` and table `user_users`
        $this->dropForeignKey('fk_product_orders_user_id', '{{%product_orders}}');
        $this->dropIndex('ix_product_orders_user_id', '{{%product_orders}}');
    }
}
