<?php

use yii\db\Migration;

/**
 * Class m180517_082522_update_colum_product_variation_recommended_id_to_product_recommended_id_product_free_recommendets_table
 */
class m180517_082522_update_colum_product_variation_recommended_id_to_product_recommended_id_product_free_recommendets_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropOldRelations();
        $this->dropColumn('product_free_recommendets', 'product_variation_recommended_id');

        $this->addColumn('product_free_recommendets', 'product_recommended_id', $this->integer()->notNull());
        $this->createRelations();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropRelations();
        $this->dropColumn('product_free_recommendets', 'product_recommended_id');

        $this->addColumn('product_free_recommendets', 'product_variation_recommended_id', $this->integer()->notNull());
        $this->createOldRelations();
    }

    /**
     * Create relations between tables.
     *
     * @return void
     */
    private function createRelations()
    {
        $this->createIndex('ix_product_free_recommendets_product_recommended_id', '{{%product_free_recommendets}}', 'product_recommended_id');
        $this->addForeignKey(
            'fk_product_free_recommendets_product_recommended_id',
            '{{%product_free_recommendets}}',
            'product_recommended_id',
            '{{%product_variations}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    private function dropRelations()
    {
        $this->dropForeignKey('fk_product_free_recommendets_product_recommended_id', '{{%product_free_recommendets}}');
        $this->dropIndex('ix_product_free_recommendets_product_recommended_id', '{{%product_free_recommendets}}');
    }

    private function createOldRelations()
    {
        $this->createIndex('ix_product_free_recommendets_product_variation_recommended_id', '{{%product_free_recommendets}}', 'product_variation_recommended_id');
        $this->addForeignKey(
            'fk_product_free_recommendets_product_variation_recommended_id',
            '{{%product_free_recommendets}}',
            'product_variation_recommended_id',
            '{{%product_variations}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    private function dropOldRelations()
    {
        $this->dropForeignKey('fk_product_free_recommendets_product_variation_recommended_id','{{%product_free_recommendets}}');
        $this->dropIndex('ix_product_free_recommendets_product_variation_recommended_id', '{{%product_free_recommendets}}');
    }
}
