<?php

use yii\db\Migration;

/**
 * Class m180601_115742_add_colum_active_product_ratings_table
 */
class m180601_115742_add_colum_active_product_ratings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('product_ratings', 'active', $this->smallInteger(1)->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('product_ratings', 'active');
    }
}
