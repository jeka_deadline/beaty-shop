<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product`.
 */
class m180411_071520_create_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%product_products}}', [
            'id' => $this->primaryKey(),
            'slug' => $this->string(255)->notNull()->unique(),
            'meta_title' => $this->string(255)->null(),
            'meta_description' => $this->text()->null(),
            'meta_keywords' => $this->string(255)->null(),
            'active' => $this->smallInteger(1)->defaultValue(1),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);

        $this->createTable('{{%product_product_lang_fields}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'lang_id' => $this->integer()->notNull(),
            'meta_title' => $this->string(255)->null(),
            'meta_description' => $this->text()->null(),
            'meta_keywords' => $this->string(255)->null(),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);

        $this->createTable('{{%product_variations}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'name' => $this->string(255)->notNull(),
            'vendor_code' => $this->string(100)->notNull()->unique(),
            'description' => $this->text()->defaultValue(null),
            'short_description' => $this->string(1000)->null(),
            'active' => $this->smallInteger(1)->defaultValue(1),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);

        $this->createTable('{{%product_variation_lang_fields}}', [
            'id' => $this->primaryKey(),
            'product_variation_id' => $this->integer()->notNull(),
            'lang_id' => $this->integer()->notNull(),
            'name' => $this->string(255)->notNull(),
            'description' => $this->text()->defaultValue(null),
            'short_description' => $this->string(1000)->null(),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);

        $this->createTable('{{%product_relation_categories}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'product_category_id' => $this->integer()->notNull(),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);

        $this->createTable('{{%product_inventories}}', [
            'id' => $this->primaryKey(),
            'product_variation_id' => $this->integer()->notNull(),
            'count' => $this->integer()->defaultValue(0),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);

        $this->createTable('{{%product_prices}}', [
            'id' => $this->primaryKey(),
            'product_variation_id' => $this->integer()->notNull(),
            'price' => $this->float()->defaultValue(0),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);

        $this->createTable('{{%product_promotions}}', [
            'id' => $this->primaryKey(),
            'product_variation_id' => $this->integer()->notNull(),
            'price' => $this->float()->defaultValue(0),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);

        $this->createTable('{{%product_images}}', [
            'id' => $this->primaryKey(),
            'product_variation_id' => $this->integer()->notNull(),
            'filename' => $this->string(255)->notNull(),
            'sort' => $this->integer()->defaultValue(0),
            'preview' => $this->integer()->defaultValue(0),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);

        $this->createRelations();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropRelations();
        $this->dropTable('{{%product_products}}');
        $this->dropTable('{{%product_product_lang_fields}}');
        $this->dropTable('{{%product_variations}}');
        $this->dropTable('{{%product_variation_lang_fields}}');
        $this->dropTable('{{%product_relation_categories}}');
        $this->dropTable('{{%product_inventories}}');
        $this->dropTable('{{%product_prices}}');
        $this->dropTable('{{%product_promotions}}');
        $this->dropTable('{{%product_images}}');
    }

    private function createRelations()
    {
        $this->createIndex('ix_product_product_lang_fields_lang_id', '{{%product_product_lang_fields}}', 'lang_id');
        $this->addForeignKey(
            'fk_product_product_lang_fields_lang_id',
            '{{%product_product_lang_fields}}',
            'lang_id',
            '{{%core_languages}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->createIndex('ix_product_variation_lang_fields_product_id', '{{%product_product_lang_fields}}', 'product_id');
        $this->addForeignKey(
            'fk_product_product_lang_fields_product_id',
            '{{%product_product_lang_fields}}',
            'product_id',
            '{{%product_products}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex('ix_product_variation_product_id', '{{%product_variations}}', 'product_id');
        $this->addForeignKey(
            'fk_product_variation_product_id',
            '{{%product_variations}}',
            'product_id',
            '{{%product_products}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->createIndex('ix_product_variation_vendor_code', '{{%product_variations}}', 'vendor_code');

        $this->createIndex('ix_product_variation_lang_fields_lang_id', '{{%product_variation_lang_fields}}', 'lang_id');
        $this->addForeignKey(
            'fk_product_variation_lang_fields_lang_id',
            '{{%product_variation_lang_fields}}',
            'lang_id',
            '{{%core_languages}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->createIndex('ix_product_variation_lang_fields_product_variation_id', '{{%product_variation_lang_fields}}', 'product_variation_id');
        $this->addForeignKey(
            'fk_product_variation_lang_fields_product_variation_id',
            '{{%product_variation_lang_fields}}',
            'product_variation_id',
            '{{%product_variations}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex('ix_product_relation_categories_lang_id', '{{%product_relation_categories}}', 'product_id');
        $this->addForeignKey(
            'fk_product_relation_categories_product_id',
            '{{%product_relation_categories}}',
            'product_id',
            '{{%product_products}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->createIndex('ix_product_relation_categories_product_variation_id', '{{%product_relation_categories}}', 'product_category_id');
        $this->addForeignKey(
            'fk_product_relation_categories_product_variation_id',
            '{{%product_relation_categories}}',
            'product_category_id',
            '{{%product_categories}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex('ix_product_inventories_product_variation_id', '{{%product_inventories}}', 'product_variation_id');
        $this->addForeignKey(
            'fk_product_inventories_product_variation_id',
            '{{%product_inventories}}',
            'product_variation_id',
            '{{%product_variations}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex('ix_product_prices_product_variation_id', '{{%product_prices}}', 'product_variation_id');
        $this->addForeignKey(
            'fk_product_prices_product_variation_id',
            '{{%product_prices}}',
            'product_variation_id',
            '{{%product_variations}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex('ix_product_promotions_product_variation_id', '{{%product_promotions}}', 'product_variation_id');
        $this->addForeignKey(
            'fk_product_promotions_product_variation_id',
            '{{%product_promotions}}',
            'product_variation_id',
            '{{%product_variations}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex('ix_product_images_product_variation_id', '{{%product_images}}', 'product_variation_id');
        $this->addForeignKey(
            'fk_product_images_product_variation_id',
            '{{%product_images}}',
            'product_variation_id',
            '{{%product_variations}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    private function dropRelations()
    {
        $this->dropForeignKey('fk_product_images_product_variation_id','{{%product_images}}');
        $this->dropIndex('ix_product_images_product_variation_id', '{{%product_images}}');

        $this->dropForeignKey('fk_product_promotions_product_variation_id','{{%product_promotions}}');
        $this->dropIndex('ix_product_promotions_product_variation_id', '{{%product_promotions}}');

        $this->dropForeignKey('fk_product_prices_product_variation_id','{{%product_prices}}');
        $this->dropIndex('ix_product_prices_product_variation_id', '{{%product_prices}}');

        $this->dropForeignKey('fk_product_inventories_product_variation_id','{{%product_inventories}}');
        $this->dropIndex('ix_product_inventories_product_variation_id', '{{%product_inventories}}');

        $this->dropForeignKey('fk_product_relation_categories_product_id','{{%product_relation_categories}}');
        $this->dropIndex('ix_product_relation_categories_lang_id', '{{%product_relation_categories}}');
        $this->dropForeignKey('fk_product_relation_categories_product_variation_id','{{%product_relation_categories}}');
        $this->dropIndex('ix_product_relation_categories_product_variation_id', '{{%product_relation_categories}}');

        $this->dropForeignKey('fk_product_variation_lang_fields_lang_id','{{%product_variation_lang_fields}}');
        $this->dropIndex('ix_product_variation_lang_fields_lang_id', '{{%product_variation_lang_fields}}');
        $this->dropForeignKey('fk_product_variation_lang_fields_product_variation_id','{{%product_variation_lang_fields}}');
        $this->dropIndex('ix_product_variation_lang_fields_product_variation_id', '{{%product_variation_lang_fields}}');

        $this->dropForeignKey('fk_product_variation_product_id','{{%product_variations}}');
        $this->dropIndex('ix_product_variation_product_id', '{{%product_variations}}');
        $this->dropIndex('ix_product_variation_vendor_code', '{{%product_variations}}');

        $this->dropForeignKey('fk_product_product_lang_fields_lang_id','{{%product_product_lang_fields}}');
        $this->dropIndex('ix_product_product_lang_fields_lang_id', '{{%product_product_lang_fields}}');
        $this->dropForeignKey('fk_product_product_lang_fields_product_id','{{%product_product_lang_fields}}');
        $this->dropIndex('ix_product_variation_lang_fields_product_id', '{{%product_product_lang_fields}}');
    }
}
