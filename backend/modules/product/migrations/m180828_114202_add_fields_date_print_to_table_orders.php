<?php

use yii\db\Migration;

/**
 * Class m180828_114202_add_fields_date_print_to_table_orders
 */
class m180828_114202_add_fields_date_print_to_table_orders extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        $this->addColumn('{{%product_orders}}', 'print_shipping_date', $this->datetime()->null()->defaultValue(null));
        $this->addColumn('{{%product_orders}}', 'print_ls_date', $this->datetime()->null()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        $this->dropColumn('{{%product_orders}}', 'print_shipping_date');
        $this->dropColumn('{{%product_orders}}', 'print_ls_date');
    }
}
