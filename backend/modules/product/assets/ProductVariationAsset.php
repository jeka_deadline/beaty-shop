<?php

namespace backend\modules\product\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class ProductVariationAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [

    ];
    public $js = [
        'js/product-variation.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public $publishOptions = [
        'forceCopy' => YII_DEBUG?true:false,
    ];
}
