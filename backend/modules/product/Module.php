<?php

namespace backend\modules\product;

use Yii;

/**
 * Module definition for Products
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\product\controllers';

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function init()
    {
        parent::init();
    }
}