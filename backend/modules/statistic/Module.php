<?php

namespace backend\modules\statistic;

use Yii;

/**
 * Module definition for Products
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\statistic\controllers';

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function init()
    {
        parent::init();
    }
}