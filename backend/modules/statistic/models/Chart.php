<?php

namespace backend\modules\statistic\models;

use backend\modules\product\models\Order;
use yii\base\Model;
use yii\validators\DateValidator;

class Chart extends Model
{
    public $date_range = null;

    private $date_min;
    private $date_max;

    private $all_sum_sales = 0;
    private $all_count_orders = 0;
    private $all_average_cart = 0;

    public function init()
    {
        parent::init();
        $this->date_range = implode(' to ', $this->getRangeDate());
    }


    public function rules()
    {
        return [
            [['date_range'], 'string', 'max' => 255],
            [['date_range'], 'validateRangeDate'],
        ];
    }

    public function validateRangeDate($attribute, $params)
    {
        $range = explode(' to ', $this->$attribute);
        $dateValidator = new DateValidator();
        $dateValidator->format = 'php:Y-m-d';
        $error = null;
        if (
            is_array($range)
            && count($range)==2
            && $dateValidator->validate($range[0], $error)
            && $dateValidator->validate($range[1], $error)
        ) {
            $this->date_min = $range[0];
            $this->date_max = $range[1];
        } else {
            $this->addError($attribute, $error);
        }
    }

    public function getSumSales() {
        $date_range = $this->getRangeDate();
        $rows = (new \yii\db\Query())
            ->select(['DATE(created_at) date', 'CAST(SUM(full_sum) AS DECIMAL(20, 2)) sum'])
            ->from(Order::tableName())
            ->where(['between', 'DATE(created_at)', $date_range['min'], $date_range['max']])
            ->andWhere(['in', 'status', [Order::ORDER_STATUS_PAID, Order::ORDER_STATUS_PREAUTORIZATION, Order::ORDER_STATUS_DELIVERED]])
            ->groupBy('DATE(created_at)')
            ->orderBy(['created_at' => SORT_ASC])
            ->all();

        if (!$rows) return null;

        $rows = array_map(function ($el) {
            $el['sum'] = (float) $el['sum'];
            $this->all_sum_sales += $el['sum'];
            return array_values($el);
        }, $rows);

        array_unshift($rows, ["Date", "Sales"]);

        return json_encode($rows);
    }

    public function getCountOrders() {
        $date_range = $this->getRangeDate();

        //print_r($date_range);
        //exit;
        $rows = (new \yii\db\Query())
            ->select(['DATE(created_at) date', 'COUNT(*) count'])
            ->from(Order::tableName())
            ->where(['between', 'DATE(created_at)', $date_range['min'], $date_range['max']])
            ->andWhere(['in', 'status', [Order::ORDER_STATUS_PAID, Order::ORDER_STATUS_PREAUTORIZATION, Order::ORDER_STATUS_DELIVERED]])
            ->groupBy('DATE(created_at)')
            ->orderBy(['created_at' => SORT_ASC])
            ->all();

        if (!$rows) return null;

        $rows = array_map(function ($el) {
            $el['count'] = (int) $el['count'];
            $this->all_count_orders += $el['count'];
            return array_values($el);
        }, $rows);

        array_unshift($rows, ["Date", "Orders"]);

        return json_encode($rows);
    }

    public function getAverageCart() {
        $date_range = $this->getRangeDate();
        $rows = (new \yii\db\Query())
            ->select(['DATE(created_at) date', 'CAST(AVG(full_sum) AS DECIMAL(20, 2)) avg'])
            ->from(Order::tableName())
            ->where(['between', 'DATE(created_at)', $date_range['min'], $date_range['max']])
            ->andWhere(['in', 'status', [Order::ORDER_STATUS_PAID, Order::ORDER_STATUS_PREAUTORIZATION, Order::ORDER_STATUS_DELIVERED]])
            ->groupBy('DATE(created_at)')
            ->orderBy(['created_at' => SORT_ASC])
            ->all();

        if (!$rows) return null;

        $rows = array_map(function ($el) {
            $el['avg'] = (float) $el['avg'];
            $this->all_average_cart += $el['avg'];
            return array_values($el);
        }, $rows);

        array_unshift($rows, ["Date", "Orders"]);

        return json_encode($rows);
    }

    private function getRangeDate() {
        if ($this->date_range) {
            return ['min' => $this->date_min, 'max' => $this->date_max];
        }
        $this->date_min = date('Y-m-d', strtotime("-1 month"));
        $this->date_max = date('Y-m-d');
        $this->date_range = implode(' to ', [$this->date_min, $this->date_max]);
        return ['min' => $this->date_min, 'max' => $this->date_max];
    }

    public function getAllSumSales() {
        return $this->all_sum_sales;
    }

    public function getAllCountOrders() {
        return $this->all_count_orders;
    }

    public function getAllAverageCart() {
        return $this->all_average_cart;
    }

    public function getRangeDateDay() {
        return ['min' => date('Y-m-d', strtotime("-1 days")), 'max' => date('Y-m-d')];
    }

    public function getRangeDateMonth() {
        return ['min' => date('Y-m-d', strtotime("-1 month")), 'max' => date('Y-m-d')];
    }

    public function getRangeDateYaer() {
        return ['min' => date('Y-m-d', strtotime("-1 year")), 'max' => date('Y-m-d')];
    }

}