<?php

namespace backend\modules\statistic\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class ShopStatisticAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [

    ];
    public $js = [
        'https://www.gstatic.com/charts/loader.js',
        'js/shop-statistic.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public $publishOptions = [
        'forceCopy' => YII_DEBUG?true:false,
    ];
}
