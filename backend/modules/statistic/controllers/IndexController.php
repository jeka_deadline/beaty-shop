<?php

namespace backend\modules\statistic\controllers;

use backend\modules\product\models\Order;
use backend\modules\product\models\OrderProductVariation;
use backend\modules\statistic\models\Chart;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use common\models\LoginForm;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/** Core index controller class.
*/
class IndexController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \developeruz\db_rbac\behaviors\AccessBehavior::className(),
                'login_url' => Yii::$app->user->loginUrl,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    /**
     * Display index page.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $formChart = new Chart();

        $dataProviderRecentOrders = new ActiveDataProvider([
            'query' => Order::getRecentOrders(),
            'pagination' => false,
            'sort' => false,
        ]);

        $dataProviderTopProducts = new ActiveDataProvider([
            'query' => OrderProductVariation::getTopProductVariations(),
            'pagination' => false,
            'sort' => false,
        ]);

        return $this->render('index', [
            'dataProviderRecentOrders' => $dataProviderRecentOrders,
            'dataProviderTopProducts' => $dataProviderTopProducts,
            'formChart' => $formChart,
            'sumSales' => $formChart->getSumSales(),
            'countOrders' => $formChart->getCountOrders(),
            'averageCart' => $formChart->getAverageCart()
        ]);
    }

    public function actionGetDataChart() {
        if (!Yii::$app->request->isAjax) throw new NotFoundHttpException("Not found");

        Yii::$app->response->format = Response::FORMAT_JSON;

        $formChart = new Chart();

        if ($formChart->load(Yii::$app->request->post()) && $formChart->validate()) {
            return [
                'status' => 'ok',
                'sales' => [
                    'data' => $formChart->getSumSales(),
                    'sum' => $formChart->allSumSales
                ],
                'orders' => [
                    'data' => $formChart->getCountOrders(),
                    'count' => $formChart->allCountOrders
                ],
                'average' => [
                    'data' => $formChart->getAverageCart(),
                    'sum' => $formChart->allAverageCart
                ],
            ];
        }

        return [
            'status' => 'error',
            'error' => $formChart->errors
        ];
    }
}
