<?php
/** @var $this yii\web\View */

use backend\modules\academy\models\Register;
use backend\modules\core\models\Subscriber;
use backend\modules\core\models\SupportMessage;
use backend\modules\core\models\TrainerDistributor;
use backend\modules\product\models\Order;
use backend\modules\statistic\assets\ShopStatisticAsset;
use kartik\daterange\DateRangePicker;
use yii\grid\DataColumn;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

ShopStatisticAsset::register($this);

$this->title = 'Shop statistic';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop-statistic-index">

    <div class="row">
        <div class="col-lg-12 col-xs-12">
            <div class="box">
                <div class="box-body">
                    <?php $form = ActiveForm::begin([
                        'action' => Url::toRoute('/statistic/index/get-data-chart')
                    ]); ?>
                        <div class="btn-group pull-left col-lg-9 col-md-6 col-sm-12 col-xs-12">
                            <a href="#" class="btn btn-default" data-set-range-date="<?= implode(' to ', $formChart->getRangeDateDay()) ?>">Day</a>
                            <a href="#" class="btn btn-default active" data-set-range-date="<?= implode(' to ', $formChart->getRangeDateMonth()) ?>">Month</a>
                            <a href="#" class="btn btn-default" data-set-range-date="<?= implode(' to ', $formChart->getRangeDateYaer()) ?>">Year</a>
                        </div>
                        <div class="btn-group pull-right col-lg-3 col-md-6 col-sm-12 col-xs-12">

                                <?= $form->field($formChart, 'date_range', [
                                    'template' => "{label}\n{hint}\n<div class=\"input-group drp-container\">{input}".'<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>'."</div>\n{error}",
                                    'options'=>['class'=>'drp-container form-group']
                                ])->widget(DateRangePicker::classname(), [
                                    'convertFormat'=>true,
                                    'useWithAddon'=>true,
                                    'pluginOptions'=>[
                                        'locale'=>[
                                            'format'=>'Y-m-d',
                                            'separator'=>' to ',
                                        ],
                                        'opens'=>'left'
                                    ],
                                    'pluginEvents' => [
                                        "apply.daterangepicker" => "getAJAXDataChartShopStatistic",
                                    ]
                                ])->label(false) ?>
                        </div>
                    <?php ActiveForm::end(); ?>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Currently</h3>
                </div>
                <div class="box-body">
                    <ul class="nav nav-stacked">
                        <li><a href="<?= Url::to(['/product/order/index', 'OrderSearch[is_new]' => 1]) ?>">New orders <span class="pull-right badge bg-red"><?= Order::getCountNewOrders() ?></span></a></li>
                        <li><a href="<?= Url::to(['/product/order/index', 'OrderSearch[status]' => 'new']) ?>">Status new orders <span class="pull-right badge bg-red"><?= Order::getCountStatusNewOrders() ?></span></a></li>
                        <li><a href="<?= Url::to(['/product/order/index', 'OrderSearch[status]' => 'paid']) ?>">Status paid orders <span class="pull-right badge bg-aqua"><?= Order::getCountStatusPaidOrders() ?></span></a></li>
                        <li><a href="<?= Url::to(['/core/support-message/index', 'SupportMessageSearch[is_new]' => 1]) ?>">Support messages <span class="pull-right badge bg-red"><?= SupportMessage::getCountNewMessages() ?></span></a></li>
                        <li><a href="<?= Url::to(['/core/subscribe/index', 'SubscriberSearch[is_new]' => 1]) ?>">Subscribers <span class="pull-right badge  bg-green"><?= Subscriber::getCountNewSubscribers() ?></span></a></li>
                        <li><a href="<?= Url::to(['/academy/register/index', 'RegisterSearch[is_new]' => 1]) ?>">Registration course <span class="pull-right badge bg-blue"><?= Register::getCountNew() ?></span></a></li>
                        <li><a href="<?= Url::to(['/core/trainer-distributor/index', 'TrainerDistributorSearch[type]' => 'trainer', 'TrainerDistributorSearch[is_new]' => 1]) ?>">Trainer <span class="pull-right badge bg-blue"><?= TrainerDistributor::getCountNewTrainer() ?></span></a></li>
                        <li><a href="<?= Url::to(['/core/trainer-distributor/index', 'TrainerDistributorSearch[type]' => 'distrubutor', 'TrainerDistributorSearch[is_new]' => 1]) ?>">Distributor <span class="pull-right badge bg-blue"><?= TrainerDistributor::getCountNewDistributor() ?></span></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="box box-primary box-statistic-charts">
                <div class="box-header with-border">
                    <h3 class="box-title">Statistic</h3>
                </div>
                <div class="box-body">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active text-center">
                                <a href="#statistic-sales" data-toggle="tab" aria-expanded="true">
                                    <span class="info-box-text">
                                        Sales
                                    </span>
                                    <span class="info-box-number" data-model="sale-sum">
                                        €<?= $formChart->allSumSales ?>
                                    </span>
                                </a>
                            </li>
                            <li class="text-center">
                                <a href="#statistic-orders" data-toggle="tab" aria-expanded="false">
                                    <span class="info-box-text">
                                        Orders
                                    </span>
                                    <span class="info-box-number" data-model="count-orders">
                                        <?= $formChart->allCountOrders ?>
                                    </span>
                                </a>
                            </li>
                            <li class="text-center">
                                <a href="#statistic-cart-value" data-toggle="tab" aria-expanded="false">
                                    <span class="info-box-text">
                                        Cart Value
                                    </span>
                                    <span class="info-box-number" data-model="average-cart">
                                        €<?= $formChart->allAverageCart ?>
                                    </span>
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="statistic-sales" data-chart-data='<?= $sumSales ?>'>

                            </div>
                            <div class="tab-pane" id="statistic-orders" data-chart-data='<?= $countOrders ?>'>

                            </div>
                            <div class="tab-pane" id="statistic-cart-value" data-chart-data='<?= $averageCart ?>'>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Products and Sales</h3>
                </div>
                <div class="box-body">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active text-center">
                                <a href="#statistic-recent-orders" data-toggle="tab" aria-expanded="true">
                                    Recent Orders
                                </a>
                            </li>
                            <li class="text-center">
                                <a href="#statistic-top-products" data-toggle="tab" aria-expanded="false">
                                    Top Products
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="statistic-recent-orders">
                                <?= GridView::widget([
                                    'dataProvider' => $dataProviderRecentOrders,
                                    'summary' => false,
                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn'],

                                        'order_number',
                                        [
                                            'attribute' => 'status',
                                            'label' => 'Step payment',
                                            'value' => function($model){ return $model->getStepPayment(); },
                                            'filter' => false,
                                        ],
                                        [
                                            'attribute' => 'total_sum',
                                            'value' => function ($model) {
                                                return $model->total_sum . ' €';
                                            }
                                        ],
                                        [
                                            'attribute' => 'tax',
                                            'value' => function($model) {
                                                return $model->tax . ' €';
                                            }
                                        ],
                                        [
                                            'attribute' => 'shipping_price',
                                            'value' => function($model) {
                                                return $model->shipping_price . ' €';
                                            }
                                        ],
                                        [
                                            'attribute' => 'full_sum',
                                            'value' => function($model) {
                                                return $model->full_sum . ' €';
                                            }
                                        ],
                                        [
                                            'class' => 'yii\grid\ActionColumn',
                                            'template' => '{view}{update}',
                                            'urlCreator' => function ($action, $model, $key, $index) {
                                                $params = is_array($key) ? $key : ['id' => (string) $key];
                                                $params[0] =  '/product/order/' . $action;
                                                return Url::toRoute($params);
                                            }
                                        ],
                                    ],
                                ]); ?>
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="statistic-top-products">
                                <?= GridView::widget([
                                    'dataProvider' => $dataProviderTopProducts,
                                    'summary' => false,
                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn'],

                                        'productVariation.id',
                                        'productVariation.name',
                                        'productVariation.vendor_code',

                                        [
                                            'class' => 'yii\grid\ActionColumn',
                                            'template' => '{view}{update}',
                                            'urlCreator' => function ($action, $model, $key, $index) {
                                                $params = is_array($key) ? $key : ['id' => (string) $key];
                                                $params[0] =  '/product/product/' . $action;
                                                if ($model->product) $params['id'] = $model->product->id;
                                                return Url::toRoute($params);
                                            }
                                        ],
                                    ],
                                ]); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>