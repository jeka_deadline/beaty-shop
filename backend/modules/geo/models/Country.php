<?php

namespace backend\modules\geo\models;

use common\models\geo\Country as BaseCountry;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * Backend country model extends common country model.
 */
class Country extends BaseCountry
{
    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::className(), ['country_id' => 'id']);
    }
}
