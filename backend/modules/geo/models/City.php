<?php

namespace backend\modules\geo\models;

use common\models\geo\City as BaseCity;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class City extends BaseCity
{
    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function getListCountries()
    {
        return Country::getHashMap('id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }
}
