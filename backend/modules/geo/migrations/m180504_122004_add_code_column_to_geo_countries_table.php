<?php

use yii\db\Migration;

/**
 * Handles adding code to table `geo_countries`.
 */
class m180504_122004_add_code_column_to_geo_countries_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('geo_countries', 'code', $this->string(5));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('geo_countries', 'code');
    }
}
