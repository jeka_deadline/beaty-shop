<?php

use yii\db\Migration;

/**
 * Handles the creation of table `countries`.
 */
class m180423_074141_create_countries_table extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        // create table countries.
        $this->createTable('{{%geo_countries}}', [
            'id'         => $this->primaryKey(),
            'name'       => $this->string(100),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);

        $this->createTable('{{%geo_cities}}', [
            'id'         => $this->primaryKey(),
            'country_id' => $this->integer(),
            'name'       => $this->string(100),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);

        $this->createRelations();
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        $this->dropRelations();

        // drop table cities.
        $this->dropTable('{{%geo_cities}}');

        // drop table countries.
        $this->dropTable('{{%geo_countries}}');
    }

    /**
     * Create relations between tables.
     *
     * @return void
     */
    private function createRelations()
    {
        // create relations between table `geo_cities` and table `geo_countries`
        $this->createIndex('ix_geo_cities_country_id', '{{%geo_cities}}', 'country_id');
        $this->addForeignKey(
            'fk_geo_cities_country_id',
            '{{%geo_cities}}',
            'country_id',
            '{{%geo_countries}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * Drop relations between tables.
     *
     * @return void
     */
    private function dropRelations()
    {
        // drop relations between table `geo_cities` and table `geo_countries`
        $this->dropForeignKey('fk_geo_cities_country_id', '{{%geo_cities}}');
        $this->dropIndex('ix_geo_cities_country_id', '{{%geo_cities}}');
    }
}
