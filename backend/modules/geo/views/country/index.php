<?php

use yii\helpers\Html;
use yii\grid\GridView;

/** @var \yii\web\View $this */
/** @var \backend\modules\geo\models\searchModels\CountrySearch $searchModel */
/** @var \yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Countries';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="country-index">

    <p>

        <?= Html::a('Create Country', ['create'], ['class' => 'btn btn-success']); ?>

    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
