<?php

use yii\helpers\Html;

/** @var \yii\web\View $this */
/** @var \backend\modules\geo\models\Country $model */

$this->title = 'Update Country: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Countries', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="country-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]); ?>

</div>
