<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var \yii\web\View $this */
/** @var \backend\modules\geo\models\City $model */
/** @var \yii\widgets\ActiveForm $form */
?>

<div class="city-form">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'country_id')->dropDownList($model->getListCountries()); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]); ?>

        <div class="form-group">

            <?= Html::submitButton('Save', ['class' => 'btn btn-success']); ?>

        </div>

    <?php ActiveForm::end(); ?>

</div>
