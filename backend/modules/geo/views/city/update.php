<?php

use yii\helpers\Html;

/** @var \yii\web\View $this */
/** @var \backend\modules\geo\models\City $model */

$this->title = 'Update City: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Cities', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="city-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]); ?>

</div>
