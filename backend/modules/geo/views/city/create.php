<?php

use yii\helpers\Html;


/** @var \yii\web\View $this */
/** @var \backend\modules\geo\models\City $model */

$this->title = 'Create City';
$this->params['breadcrumbs'][] = ['label' => 'Cities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="city-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]); ?>

</div>
