<?php

use yii\helpers\Html;
use yii\grid\GridView;

/** @var \yii\web\View $this */
/** @var \backend\modules\geo\models\searchModels\CitySearch $searchModel */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var \backend\modules\geo\models\City $model */

$this->title = 'Cities';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="city-index">
    <p>

        <?= Html::a('Create City', ['create'], ['class' => 'btn btn-success']); ?>

    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'country_id',
                'value' => function($model){ return $model->country->name; },
                'filter' => false,
            ],
            'name',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
