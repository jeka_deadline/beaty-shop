<?php

namespace backend\modules\user\models;

use developeruz\db_rbac\interfaces\UserRbacInterface;
use backend\modules\core\models\TrainerDistributor;

/**
 * User class extends base user class
 */
class User extends \common\models\user\User implements UserRbacInterface
{
    /**
     * Unblock user.
     *
     * @return bool
     */
    public function unblock()
    {
        return (bool)$this->updateAttributes(['blocked_at' => null]);
    }

    /**
     * Block user.
     *
     * @return bool
     */
    public function block()
    {
        return (bool)$this->updateAttributes(['blocked_at' => date('Y-m-d H:i:s')]);
    }

    public function confirmEmail()
    {
        if ($this->updateAttributes(['confirm_email_at' => date('Y-m-d H:i:s')])) {
            UserEmailToken::deleteAll(['user_id' => $this->id]);

            return true;
        }

        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSocial()
    {
        return $this->hasMany(Social::className(), ['user_id' => 'id']);
    }

    public function getUserName()
    {
        return $this->email;
    }

    public function getDistributorFullName()
    {
        if ($this->distributor) {
            return $this->distributor->first_name . ' ' . $this->distributor->last_name;
        }

        return null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistributor()
    {
        return $this->hasOne(TrainerDistributor::className(), ['id' => 'distributor_id']);
    }
}
