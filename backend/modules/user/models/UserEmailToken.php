<?php

namespace backend\modules\user\models;

use Yii;
use common\models\user\UserEmailToken as BaseUserEmailToken;

/**
 * User email token class extends base user email token class
 */
class UserEmailToken extends BaseUserEmailToken
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
