<?php

namespace backend\modules\user\models;

/**
 * Profile class extends base user class
 */
class Profile extends \common\models\user\Profile
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
