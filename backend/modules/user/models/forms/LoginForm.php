<?php

namespace backend\modules\user\models\forms;

use Yii;
use yii\base\Model;
use backend\modules\user\models\User;
use common\models\user\UserAdmin;

/**
 * Class for login user.
 */
class LoginForm extends Model
{
    /**
     * User email.
     *
     * @var string
     */
    public $email;

    /**
     * User password.
     *
     * @var string
     */
    public $password;

    /**
     * Remember user auth.
     *
     * @var bool
     */
    public $rememberMe;

    /**
     * User model if user find by email.
     *
     * @var \backend\modules\user\models\User
     */
    private $user = false;

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['email', 'password'], 'required'],
            [['password'], 'validatePassword'],
            [['rememberMe'], 'boolean'],
            [['email'], 'filter', 'filter' => 'trim'],
        ];
    }


    /**
     * Validate user password.
     *
     * @return void
     */
    public function validatePassword()
    {
        $user = $this->getUser();

        if (!$user || !Yii::$app->security->validatePassword($this->password, $user->password_hash)) {
            $this->addError('email', 'Access danied');

            return false;
        }

        $isUserAdminGranted = UserAdmin::findOne(['user_id' => $user->id]);

        if (!$isUserAdminGranted) {
            $this->addError('email', 'Access danied');
        }
    }

    /**
     * Attempt auth user.
     *
     * @return bool
     */
    public function login()
    {
        if ($this->validate()) {
            $user = User::findByEmail($this->email);
            Yii::$app->user->login($user, $this->rememberMe ? 3600 * 24 * 30 : 0);

            return true;
        }

        return false;
    }

    /**
     * Find user by email.
     *
     * @return \common\models\user\User|null
     */
    private function getUser()
    {
        if ($this->user === false) {
          $this->user = User::findByEmail($this->email);
        }

        return $this->user;
    }
}