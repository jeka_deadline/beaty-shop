<?php

namespace backend\modules\user\models\forms;

use Yii;
use DateTime;
use yii\base\Model;
use backend\modules\user\models\User;
use backend\modules\user\models\Profile;
use yii\helpers\ArrayHelper;

/**
 * Class for create new user or update user information.
 */
class UserForm extends Model
{
    /**
     * User email.
     *
     * @var string $email
     */
    public $email;

    /**
     * User password.
     *
     * @var string $password
     */
    public $password;

    /**
     * Repeat password.
     *
     * @var string $repeatPassword
     */
    public $repeatPassword;

    /**
     * User title.
     *
     * @var string $title
     */
    public $title;

    /**
     * User surname.
     *
     * @var string $surname
     */
    public $surname;

    /**
     * User name.
     *
     * @var string $name
     */
    public $name;

    /**
     * User company
     *
     * @var string $company
     */
    public $company;

    /**
     * User phone.
     *
     * @var string $phone
     */
    public $phone;

    /**
     * User date birth.
     *
     * @var string $dateBirth
     */
    public $dateBirth;

    /**
     * If need user confirm email. Only create form.
     *
     * @var bool $confirmEmail
     */
    public $confirmEmail = false;

    /**
     * User model if form is update user information
     *
     * @var null|\backend\modules\user\models\User $user
     */
    private $user = null;

    /**
     * Scenarion create user
     *
     * @var string
     */
    const SCENARIO_CREATE = 'create';

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function __construct(User $user = null, $config = [])
    {
        parent::__construct($config);

        if ($user) {
            $this->user = $user;
            $this->email = $user->email;
            $this->surname = $user->profile->surname;
            $this->name = $user->profile->name;
            $this->phone = $user->profile->phone;
            $this->company = $user->profile->company;
            $this->title = $user->profile->title;
            $this->dateBirth = $user->profile->date_birth;
        }
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return [
            // email
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => User::className(), 'filter' => function($query) {
                if ($this->user) {
                    $query->andWHere(['<>', 'id', $this->user->id]);
                }
            }],

            // password
            ['password', 'string', 'min' => 6, 'on' => self::SCENARIO_CREATE],

            // repeat password
            ['repeatPassword', 'compare', 'compareAttribute' => 'password', 'operator' => '=='],

            // required on scenario default
            [['password', 'repeatPassword'], 'required', 'on' => self::SCENARIO_CREATE],

            // required fields
            [['email', 'surname', 'name', 'phone'], 'required'],

            // confirm email
            ['confirmEmail', 'boolean'],

            // string max length 50
            [['surname', 'name'], 'string', 'max' => '50'],

            // filter trim
            [['surname', 'name', 'email', 'phone', 'company'], 'filter', 'filter' => 'trim'],

            // company
            ['company', 'string', 'max' => 100],

            // phone
            ['phone', 'match', 'pattern' => '#^\+?\d{12}$#'],

            // user title
            ['title', 'in', 'range' => [User::USER_TITLE_MR, User::USER_TITLE_MRS]],

            ['dateBirth', 'safe'],
        ];
    }

    /**
     * Create new user.
     *
     * @return bool
     */
    public function createUser()
    {
        if (!$this->validate()) {
            return false;
        }

        $user                   = new User();
        $user->email            = $this->email;
        $user->register_ip      = '127.0.0.1';
        $user->auth_key         = time() . '_' . Yii::$app->getSecurity()->generateRandomString(20);
        $user->confirm_email_at = ($this->confirmEmail) ? date('Y-m-d H:i:s') : null;

        $user->setPassword($this->password);

        $transaction = Yii::$app->db->beginTransaction();

        if ($user->validate() && $user->save()) {
            $profile = new Profile();

            $profile->surname = $this->surname;
            $profile->name = $this->name;
            $profile->phone = $this->phone;
            $profile->company = $this->company;
            $profile->date_birth = (new DateTime($this->dateBirth))->format('Y-m-d');
            $profile->title = $this->title;

            $user->link('profile', $profile);
            $transaction->commit();

            return true;
        }

        $transaction->rollback();

        return false;
    }

    /**
     * Update user.
     *
     * @return bool
     */
    public function updateUser()
    {
        if (!$this->validate()) {
            return false;
        }

        $user = $this->user;
        $profile = $this->user->profile;
        $user->email = $this->email;
        $profile->surname = $this->surname;
        $profile->name = $this->name;
        $profile->phone = $this->phone;
        $profile->company = $this->company;
        $profile->date_birth = (new DateTime($this->dateBirth))->format('Y-m-d');
        $profile->title = $this->title;

        $transaction = Yii::$app->db->beginTransaction();

        if ($user->validate() && $profile->validate() && $user->save() && $profile->save()) {
            $transaction->commit();

            return true;
        }

        $transaction->rollback();

        return false;
    }

    /**
     * Reset password fields.
     *
     * @return void
     */
    public function resetPasswordFields()
    {
        $this->password       = null;
        $this->repeatPassword = null;
    }

    /**
     * Get user list titles
     *
     * @return array
     */
    public function getListUserTitles()
    {
        return User::getListUserTitles();
    }
}
