<?php

use yii\db\Migration;

/**
 * Class m180919_144229_change_length_ip
 */
class m180919_144229_change_length_ip extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%user_users}}', 'register_ip', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('{{%user_users}}', 'register_ip', $this->string(15));
    }
}
