<?php

use yii\db\Migration;

/**
 * Class m180521_094439_change_fields_user_shipping_address
 */
class m180521_094439_change_fields_user_shipping_address extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user_shipping_addresses}}', 'default', $this->smallInteger(1)->defaultValue(0) . ' after email');

        // drop relations between table `user_shipping_addresses` and table `geo_cities`
        $this->dropForeignKey('fk_user_shipping_addresses_city_id', '{{%user_shipping_addresses}}');
        $this->dropIndex('ix_user_shipping_addresses_city_id', '{{%user_shipping_addresses}}');

        $this->dropColumn('{{%user_shipping_addresses}}', 'city_id');

        $this->addColumn('{{%user_shipping_addresses}}', 'city', $this->string(255)->notNull() . ' after country_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user_shipping_addresses}}', 'city');
        $this->addColumn('{{%user_shipping_addresses}}', 'city_id', $this->integer()->defaultValue(null) . ' after country_id');

        // create relations between table `user_shipping_addresses` and table `geo_cities`
        $this->createIndex('ix_user_shipping_addresses_city_id', '{{%user_shipping_addresses}}', 'city_id');
        $this->addForeignKey(
            'fk_user_shipping_addresses_city_id',
            '{{%user_shipping_addresses}}',
            'city_id',
            '{{%geo_cities}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->dropColumn('{{%user_shipping_addresses}}', 'default');
    }
}
