<?php

use yii\db\Migration;

/**
 * Class m180602_140925_user_admins_table
 */
class m180602_140925_user_admins_table extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        $this->createTable('{{%user_admins}}', [
            'id'      => $this->primaryKey(),
            'user_id' => $this->integer(11)->notNull(),
        ]);

        $this->createRelations();
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        $this->dropRelations();

        $this->dropTable('{{%user_admins}}');
    }

    /**
     * Create relations.
     *
     * @return void
     */
    private function createRelations()
    {
        $this->createIndex('ix_user_admins_user_id', '{{%user_admins}}', 'user_id');
        $this->addForeignKey(
            'fk_user_admins_user_id',
            '{{%user_admins}}',
            'user_id',
            '{{%user_users}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * Drop relations.
     *
     * @return void
     */
    private function dropRelations()
    {
        $this->dropForeignKey('fk_user_admins_user_id', '{{%user_admins}}');
        $this->dropIndex('ix_user_admins_user_id', '{{%user_admins}}');
    }
}
