<?php

use yii\db\Migration;

/**
 * Class m180425_081528_create_user_shipping_save_addresses
 */
class m180425_081528_create_user_shipping_save_addresses extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        // create table `user_shipping_addresses`
        $this->createTable('{{%user_shipping_addresses}}', [
            'id'           => $this->primaryKey(),
            'name_address' => $this->string(50)->notNull(),
            'user_id'      => $this->integer()->notNull(),
            'surname'      => $this->string(50)->notNull(),
            'name'         => $this->string(50)->notNull(),
            'country_id'   => $this->integer()->notNull(),
            'city_id'      => $this->integer()->notNull(),
            'address1'     => $this->text()->notNull(),
            'address2'     => $this->text()->null(),
            'zip'          => $this->string(5)->notNull(),
            'phone'        => $this->string(16)->notNull(),
            'email'        => $this->string(255)->notNull(),
            'created_at'   => $this->timestamp()->null()->defaultValue(null),
            'updated_at'   => $this->timestamp()->null()->defaultValue(null),
        ]);

        $this->createRelations();
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        $this->dropRelations();

        $this->dropTable('{{%user_shipping_addresses}}');
    }

    /**
     * Create relations between tables.
     *
     * @return void
     */
    private function createRelations()
    {
        // create relations between table `user_shipping_addresses` and table `user_users`
        $this->createIndex('ix_user_shipping_addresses_user_id', '{{%user_shipping_addresses}}', 'user_id');
        $this->addForeignKey(
            'fk_user_shipping_addresses_user_id',
            '{{%user_shipping_addresses}}',
            'user_id',
            '{{%user_users}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        // create relations between table `user_shipping_addresses` and table `geo_countries`
        $this->createIndex('ix_user_shipping_addresses_country_id', '{{%user_shipping_addresses}}', 'country_id');
        $this->addForeignKey(
            'fk_user_shipping_addresses_country_id',
            '{{%user_shipping_addresses}}',
            'country_id',
            '{{%geo_countries}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        // create relations between table `user_shipping_addresses` and table `geo_cities`
        $this->createIndex('ix_user_shipping_addresses_city_id', '{{%user_shipping_addresses}}', 'city_id');
        $this->addForeignKey(
            'fk_user_shipping_addresses_city_id',
            '{{%user_shipping_addresses}}',
            'city_id',
            '{{%geo_cities}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * Drop relations between tables.
     *
     * @return void
     */
    private function dropRelations()
    {
        // drop relations between table `user_shipping_addresses` and table `geo_cities`
        $this->dropForeignKey('fk_user_shipping_addresses_city_id', '{{%user_shipping_addresses}}');
        $this->dropIndex('ix_user_shipping_addresses_city_id', '{{%user_shipping_addresses}}');

        // drop relations between table `user_shipping_addresses` and table `geo_countries`
        $this->dropForeignKey('fk_user_shipping_addresses_country_id', '{{%user_shipping_addresses}}');
        $this->dropIndex('ix_user_shipping_addresses_country_id', '{{%user_shipping_addresses}}');

        // drop relations between table `user_shipping_addresses` and table `user_users`
        $this->dropForeignKey('fk_user_shipping_addresses_user_id', '{{%user_shipping_addresses}}');
        $this->dropIndex('ix_user_shipping_addresses_user_id', '{{%user_shipping_addresses}}');
    }
}
