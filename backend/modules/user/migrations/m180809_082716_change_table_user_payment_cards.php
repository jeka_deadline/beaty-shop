<?php

use yii\db\Migration;

/**
 * Class m180809_082716_change_table_user_payment_cards
 */
class m180809_082716_change_table_user_payment_cards extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%user_payment_cards}}', 'month');
        $this->dropColumn('{{%user_payment_cards}}', 'year');
        $this->addColumn('{{%user_payment_cards}}', 'pseudocardpan', $this->string(255)->null() . ' after number');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%user_payment_cards}}', 'month', $this->integer() . ' after number');
        $this->addColumn('{{%user_payment_cards}}', 'year', $this->integer() . ' after month');
        $this->dropColumn('{{%user_payment_cards}}', 'pseudocardpan');

    }
}
