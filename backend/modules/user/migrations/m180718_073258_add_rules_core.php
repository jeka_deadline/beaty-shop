<?php

use yii\db\Migration;

/**
 * Class m180718_073258_add_rules_core
 */
class m180718_073258_add_rules_core extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $permitions=[
            [
                'name' => 'core/career/create',
                'type' => 2,
                'description' => 'Module Core. Permission to create a vacancy for a career page.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'core/career/delete',
                'type' => 2,
                'description' => 'Module Core. Permission to remove vacancies on the career page.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'core/career/index',
                'type' => 2,
                'description' => 'Module Core. Permission to view the list of vanases on the career page.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'core/career/update',
                'type' => 2,
                'description' => 'Module Core. Permission to edit a vacancy for a career page.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'core/career/view',
                'type' => 2,
                'description' => 'Module Core. Permission for the vacancy information from the career page.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],

            [
                'name' => 'core/contact-studio/create',
                'type' => 2,
                'description' => 'Module Core. Permission to create a contact for the "Find Studio" page.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'core/contact-studio/delete',
                'type' => 2,
                'description' => 'Module Core. Permission to delete a contact on the page "Find a studio".',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'core/contact-studio/index',
                'type' => 2,
                'description' => 'Module Core. Permission to view the contact list for the "Find Studio" page.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'core/contact-studio/update',
                'type' => 2,
                'description' => 'Module Core. Permission to edit contacts for the "Find Studio" page.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'core/contact-studio/view',
                'type' => 2,
                'description' => 'Module Core. Permission to view information about the contact from the page "Find a studio".',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],

            [
                'name' => 'core/email-template/delete',
                'type' => 2,
                'description' => 'Module Core. Permission to delete a letter template.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'core/email-template/index',
                'type' => 2,
                'description' => 'Module Core. Permission to view the list of email templates.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'core/email-template/update',
                'type' => 2,
                'description' => 'Module Core. Permission to edit a letter template.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'core/email-template/view',
                'type' => 2,
                'description' => 'Module Core. Permission for information about the template of the letter.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],

            [
                'name' => 'core/language/create',
                'type' => 2,
                'description' => 'Module Core. Permission to add language.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'core/language/delete',
                'type' => 2,
                'description' => 'Module Core. Permission to delete a language.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'core/language/index',
                'type' => 2,
                'description' => 'Module Core. Permission to view the list of languages.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'core/language/update',
                'type' => 2,
                'description' => 'Module Core. Permission to edit the language settings.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'core/language/view',
                'type' => 2,
                'description' => 'Module Core. Permission to view the language parameters.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],

            [
                'name' => 'core/setting/create',
                'type' => 2,
                'description' => 'Module Core. Permission to create a setting.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'core/setting/delete',
                'type' => 2,
                'description' => 'Module Core. Permission to delete the setting.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'core/setting/index',
                'type' => 2,
                'description' => 'Module Core. Permission to view the list of settings.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'core/setting/update',
                'type' => 2,
                'description' => 'Module Core. Permission to edit the setting.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'core/setting/view',
                'type' => 2,
                'description' => 'Module Core. Permission for the adjustment of the settings.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],

            [
                'name' => 'core/slogan/create',
                'type' => 2,
                'description' => 'Module Core. Permission to create a slogan.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'core/slogan/delete',
                'type' => 2,
                'description' => 'Module Core. Permission to remove the slogan.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'core/slogan/index',
                'type' => 2,
                'description' => 'Module Core. Permission to view the list of slogans.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'core/slogan/update',
                'type' => 2,
                'description' => 'Module Core. Permission to edit the slogan.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'core/subscribe/view',
                'type' => 2,
                'description' => 'Module Core. Permission to view the slogan.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],

            [
                'name' => 'core/subscribe/delete',
                'type' => 2,
                'description' => 'Module Core. Permission to delete a subscription.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'core/subscribe/index',
                'type' => 2,
                'description' => 'Module Core. Permission to view the list of subscribers.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],

            [
                'name' => 'core/support-message/delete',
                'type' => 2,
                'description' => 'Module Core. Permission to delete a message in support.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'core/support-message/index',
                'type' => 2,
                'description' => 'Module Core. Permission to view the list of messages in support.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'core/support-message/view',
                'type' => 2,
                'description' => 'Module Core. Permission to view the message in support.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],

            [
                'name' => 'core/text-block/create',
                'type' => 2,
                'description' => 'Module Core. Permission to create a text block.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'core/text-block/delete',
                'type' => 2,
                'description' => 'Module Core. Permission to delete a text block.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'core/text-block/index',
                'type' => 2,
                'description' => 'Module Core. Permission to view the list of text blocks.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'core/text-block/update',
                'type' => 2,
                'description' => 'Module Core. Permission to edit a text block.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'core/text-block/view',
                'type' => 2,
                'description' => 'Module Core. Permission to view information about a text block.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],

            [
                'name' => 'core/trainer-distributor/delete',
                'type' => 2,
                'description' => 'Module Core. Permission to delete a subscription to "Become a Trainer (Destrictor)".',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'core/trainer-distributor/index',
                'type' => 2,
                'description' => 'Module Core. Permission to view the list of subscriptions for "Become a Trainer (Destrictor)".',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'core/trainer-distributor/view',
                'type' => 2,
                'description' => 'Module Core. Permission to view information about the subscription to "Become a Trainer (Destrictor)".',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],

            [
                'name' => 'core/translation-strings/index',
                'type' => 2,
                'description' => 'Module Core. Permission to view the list of languages for which the translation of lines is indicated.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'core/translation-strings/update',
                'type' => 2,
                'description' => 'Module Core. Permission to edit translated strings for the language.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'core/translation-strings/view',
                'type' => 2,
                'description' => 'Module Core. Permission to view information about translated strings for the language.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
        ];

        $this->batchInsert(
            '{{%auth_item}}',
            ['name', 'type', 'description', 'created_at', 'updated_at'],
            $permitions
        );

        $data = [];

        foreach ($permitions as $permition) {
            $data[] = [
                'parent' => 'superadmin',
                'child' => $permition['name'],
            ];
        }

        if ($data) {
            $this->batchInsert(
                '{{%auth_item_child}}',
                ['parent', 'child'],
                $data
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return true;
    }
}
