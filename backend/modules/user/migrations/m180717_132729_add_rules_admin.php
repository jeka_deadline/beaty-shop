<?php

use yii\db\Migration;

/**
 * Class m180717_132729_add_rules_admin
 */
class m180717_132729_add_rules_admin extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert(
            '{{%auth_item}}',
            [
                'name',
                'type',
                'description',
                'created_at',
                'updated_at'
            ],
            [
                [
                    'name'        => 'superadmin',
                    'type'        => 1,
                    'description' => 'Супер Администратор',
                    'created_at'  => time(),
                    'updated_at'  => time(),
                ],
                [
                    'name'        => 'admin',
                    'type'        => 1,
                    'description' => 'Администратор',
                    'created_at'  => time(),
                    'updated_at'  => time(),
                ],
                [
                    'name'        => 'manager',
                    'type'        => 1,
                    'description' => 'Руководитель',
                    'created_at'  => time(),
                    'updated_at'  => time(),
                ],
            ]
        );

        $this->insert('{{%auth_assignment}}', [
            'item_name'   => 'superadmin',
            'user_id'     => 1,
            'created_at'  => time(),
        ]);

        $auth = Yii::$app->authManager;
        $data = [];

        foreach ($auth->getPermissions() as $permition) {
            $data[] = [
                'parent' => 'superadmin',
                'child' => $permition->name,
            ];
        }

        if ($data) {
            $this->batchInsert(
                '{{%auth_item_child}}',
                ['parent', 'child'],
                $data
            );
        }

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return true;
    }
}
