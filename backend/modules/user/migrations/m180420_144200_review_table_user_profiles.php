<?php

use yii\db\Migration;

/**
 * Class m180420_144200_review_table_user_profiles
 */
class m180420_144200_review_table_user_profiles extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        $this->dropColumn('{{%user_profiles}}', 'patronymic');
        $this->dropColumn('{{%user_profiles}}', 'address');
        $this->dropColumn('{{%user_profiles}}', 'zip');
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        $this->addColumn('{{%user_profiles}}', 'patronymic', $this->string(50)->defaultValue(null) . ' after name');
        $this->addColumn('{{%user_profiles}}', 'address', $this->text()->defaultValue(null) . ' after sex');
        $this->addColumn('{{%user_profiles}}', 'zip', $this->string(255)->defaultValue(null) . ' after address');
    }
}
