<?php

use yii\db\Migration;

/**
 * Class m180515_135309_add_column_default_payment_card
 */
class m180515_135309_add_column_default_payment_card extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        $this->addColumn('{{%user_payment_cards}}', 'default', $this->smallInteger(1)->defaultValue(0) . ' after year');
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user_payment_cards}}', 'default');
    }
}
