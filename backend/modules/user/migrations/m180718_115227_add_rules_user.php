<?php

use yii\db\Migration;

/**
 * Class m180718_115227_add_rules_user
 */
class m180718_115227_add_rules_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $permitions=[
            [
                'name' => 'user/admin/create-user',
                'type' => 2,
                'description' => 'Module User. Permission to create the user.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'user/admin/delete',
                'type' => 2,
                'description' => 'Module User. Permission to delete a user.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'user/admin/index',
                'type' => 2,
                'description' => 'Module User. Permission to view the list of users.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'user/admin/update',
                'type' => 2,
                'description' => 'Module User. Permission to edit the user.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'user/admin/view',
                'type' => 2,
                'description' => 'Module User. Permission to view information about the user.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],

            [
                'name' => 'user/admin/block',
                'type' => 2,
                'description' => 'Module User. Permission to block the user.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'user/admin/unblock',
                'type' => 2,
                'description' => 'Module User. Permission to unblock the user.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'user/admin/confirm-email',
                'type' => 2,
                'description' => 'Module User. Permission to indicate that the email has been confirmed.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],

        ];

        $this->batchInsert(
            '{{%auth_item}}',
            ['name', 'type', 'description', 'created_at', 'updated_at'],
            $permitions
        );

        $data = [];

        foreach ($permitions as $permition) {
            $data[] = [
                'parent' => 'superadmin',
                'child' => $permition['name'],
            ];
        }

        if ($data) {
            $this->batchInsert(
                '{{%auth_item_child}}',
                ['parent', 'child'],
                $data
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return true;
    }
}
