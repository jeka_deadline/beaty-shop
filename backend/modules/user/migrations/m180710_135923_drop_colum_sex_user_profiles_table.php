<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `colum_sex_user_profiles`.
 */
class m180710_135923_drop_colum_sex_user_profiles_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('user_profiles', 'sex');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('user_profiles', 'sex', $this->string(6)->defaultValue(null));
    }
}
