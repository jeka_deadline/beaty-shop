<?php

use yii\db\Migration;

/**
 * Class m180718_090925_add_rules_pages
 */
class m180718_090925_add_rules_pages extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $permitions=[
            [
                'name' => 'pages/page/create',
                'type' => 2,
                'description' => 'Module Pages. Permission to create a page.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'pages/page/delete',
                'type' => 2,
                'description' => 'Module Pages. Permission to delete a page.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'pages/page/index',
                'type' => 2,
                'description' => 'Module Pages. Permission to view the list of pages.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'pages/page/update',
                'type' => 2,
                'description' => 'Module Pages. Permission to edit the pages.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'pages/page/view',
                'type' => 2,
                'description' => 'Module Pages. Permission to view information about the page.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
        ];

        $this->batchInsert(
            '{{%auth_item}}',
            ['name', 'type', 'description', 'created_at', 'updated_at'],
            $permitions
        );

        $data = [];

        foreach ($permitions as $permition) {
            $data[] = [
                'parent' => 'superadmin',
                'child' => $permition['name'],
            ];
        }

        if ($data) {
            $this->batchInsert(
                '{{%auth_item_child}}',
                ['parent', 'child'],
                $data
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return true;
    }
}
