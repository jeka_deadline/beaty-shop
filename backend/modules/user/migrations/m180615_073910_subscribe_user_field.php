<?php

use yii\db\Migration;

/**
 * Class m180615_073910_subscribe_user_field
 */
class m180615_073910_subscribe_user_field extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        $this->addColumn('{{%core_subscribers}}', 'user_id', $this->integer()->defaultValue(null));

        $this->createIndex('ix_core_subscribers_user_id', '{{%core_subscribers}}', 'user_id');
        $this->addForeignKey(
            'fk_core_subscribers_user_id',
            '{{%core_subscribers}}',
            'user_id',
            '{{%user_users}}',
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_core_subscribers_user_id', '{{%core_subscribers}}');
        $this->dropIndex('ix_core_subscribers_user_id', '{{%core_subscribers}}');
        $this->dropColumn('{{%core_subscribers}}', 'user_id');
    }
}
