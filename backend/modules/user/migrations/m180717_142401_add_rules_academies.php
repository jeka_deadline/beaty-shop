<?php

use yii\db\Migration;

/**
 * Class m180717_142401_add_rules_academies
 */
class m180717_142401_add_rules_academies extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $permitions=[
            [
                'name' => 'academy/academy/create',
                'type' => 2,
                'description' => 'Module Academy. Permission to create a course.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'academy/academy/delete',
                'type' => 2,
                'description' => 'Module Academy. Permission to delete a course.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'academy/academy/index',
                'type' => 2,
                'description' => 'Module Academy. Permission to view the list of courses.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'academy/academy/update',
                'type' => 2,
                'description' => 'Module Academy. Permission to edit the course.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'academy/academy/view',
                'type' => 2,
                'description' => 'Module Academy. Permission to attend a course.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],

            [
                'name' => 'academy/info-block/create',
                'type' => 2,
                'description' => 'Module Academy. Permission to create an information block for the course.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'academy/info-block/delete',
                'type' => 2,
                'description' => 'Module Academy. Permission to remove an information block from a course.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'academy/info-block/index',
                'type' => 2,
                'description' => 'Module Academy. Permission to view the list of information blocks for the course.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'academy/info-block/update',
                'type' => 2,
                'description' => 'Module Academy. Permission to edit the information block of the course.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'academy/info-block/view',
                'type' => 2,
                'description' => 'Module Academy. Permission for prosomtr information blocks in the course.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],

            [
                'name' => 'academy/price/create',
                'type' => 2,
                'description' => 'Module Academy. Permission to create a price for the course.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'academy/price/delete',
                'type' => 2,
                'description' => 'Module Academy. Permission to remove prices from the course.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'academy/price/index',
                'type' => 2,
                'description' => 'Module Academy. Permission to view the price list for the course.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'academy/price/update',
                'type' => 2,
                'description' => 'Module Academy. Permission to edit the price of the course.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'academy/price/view',
                'type' => 2,
                'description' => 'Module Academy. Permission to price for the course.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],

            [
                'name' => 'academy/register/delete',
                'type' => 2,
                'description' => 'Module Academy. Permission to delete a registered user on the course.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'academy/register/index',
                'type' => 2,
                'description' => 'Module Academy. Permission to view the list of registered users for the course.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'academy/register/update',
                'type' => 2,
                'description' => 'Module Academy. Permission to edit registered user information for the course.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'academy/register/view',
                'type' => 2,
                'description' => 'Module Academy. Permission for information about the registered user on the course.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
        ];

        $this->batchInsert(
            '{{%auth_item}}',
            ['name', 'type', 'description', 'created_at', 'updated_at'],
            $permitions
        );

        $data = [];

        foreach ($permitions as $permition) {
            $data[] = [
                'parent' => 'superadmin',
                'child' => $permition['name'],
            ];
        }

        if ($data) {
            $this->batchInsert(
                '{{%auth_item_child}}',
                ['parent', 'child'],
                $data
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return true;
    }
}
