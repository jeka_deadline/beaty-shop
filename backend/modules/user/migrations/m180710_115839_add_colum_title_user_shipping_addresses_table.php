<?php

use yii\db\Migration;

/**
 * Class m180710_115839_add_colum_title_user_shipping_addresses_table
 */
class m180710_115839_add_colum_title_user_shipping_addresses_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user_shipping_addresses', 'title', $this->string(10)->null());
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        $this->dropColumn('user_shipping_addresses', 'title');
    }
}
