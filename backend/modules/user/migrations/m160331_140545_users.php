<?php

use yii\db\Migration;

class m160331_140545_users extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        // table users
        $this->createTable('{{%user_users}}', [
            'id'                    => $this->primaryKey(),
            'email'                 => $this->string(100)->defaultValue(null),
            'password_hash'         => $this->string(100)->notNull(),
            'auth_key'              => $this->string(32)->notNull(),
            'blocked_at'            => $this->timestamp()->null()->defaultValue(null),
            'confirm_email_at'      => $this->timestamp()->null()->defaultValue(null),
            'register_ip'           => $this->string(15)->notNull(),
            'created_at'            => $this->timestamp()->null()->defaultValue(null),
            'updated_at'            => $this->timestamp()->null()->defaultValue(null),
            'login_with_social'     => $this->smallInteger(1)->defaultValue(0),
        ]);

        // table user profiles
        $this->createTable('{{%user_profiles}}', [
            'id'          => $this->primaryKey(),
            'user_id'     => $this->integer(11)->notNull(),
            'phone'       => $this->string(20)->defaultValue(null),
            'surname'     => $this->string(50)->defaultValue(null),
            'name'        => $this->string(50)->defaultValue(null),
            'patronymic'  => $this->string(50)->defaultValue(null),
            'sex'         => $this->string(6)->defaultValue(null),
            'address'     => $this->text()->defaultValue(null),
            'zip'         => $this->string(255)->defaultValue(null),
        ]);

        // table user socials
        $this->createTable('{{%user_socials}}', [
            'id'          => $this->primaryKey(),
            'provider'    => $this->string(100)->notNull(),
            'client_id'   => $this->string(100)->notNull(),
            'created_at'  => $this->timestamp()->null()->defaultValue(null),
            'user_id'     => $this->integer(11)->notNull(),
        ]);

        // table reset user password tokens
        $this->createTable('{{%user_reset_password_tokens}}', [
            'id'         => $this->primaryKey(),
            'user_id'    => $this->integer()->notNull(),
            'token'      => $this->string(32)->unique(),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);

        // table user confirm email tokens
        $this->createTable('{{%user_confirm_email_tokens}}', [
            'id'         => $this->primaryKey(),
            'user_id'    => $this->integer()->notNull(),
            'token'      => $this->string(32)->unique(),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);

        $this->createRelations();
        $this->createAdmins();

    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        $this->dropRelations();

        $this->dropTable('{{%user_confirm_email_tokens}}');
        $this->dropTable('{{%user_reset_password_tokens}}');
        $this->dropTable('{{%user_socials}}');
        $this->dropTable('{{%user_profiles}}');
        $this->dropTable('{{%user_users}}');
    }

    /**
     * Create admin users.
     *
     * @return void
     */
    private function createAdmins()
    {
        $currentDate = date('Y-m-d H:i:s');

        $this->insert('{{%user_users}}', [
            'email'                => 'evgeniy.bublik1992@gmail.com',
            'password_hash'        => '$2y$13$83q0/cDAKEIimLATTYWmfevmvEO7kF7smpCxm0LPszMes0SIaEYl2',
            'auth_key'             => md5(time() . \Yii::$app->getSecurity()->generateRandomString(20)),
            'confirm_email_at'     => $currentDate,
            'register_ip'          => '127.0.0.1',
            'created_at'           => $currentDate,
            'updated_at'           => $currentDate,
        ]);

        $this->insert('{{%user_profiles}}', [
            'phone'   => '+380935994767',
            'address' => 'г. Чернигов',
            'surname' => 'Бублик',
            'name'    => 'Евгений',
            'user_id' => 1,
        ]);
    }

    /**
     * Create relations.
     *
     * @return void
     */
    private function createRelations()
    {
        // Create relation between table `user_profiles` and table `user_users`
        $this->createIndex(
            'idx_user_profiles_user_id',
            '{{%user_profiles}}',
            'user_id'
        );
        $this->addForeignKey(
            'fk_user_profiles_user_id',
            '{{%user_profiles}}',
            'user_id',
            '{{%user_users}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        // Create relation between table `user_socials` and table `user_users`
        $this->createIndex(
            'idx_user_socials_user_id',
            '{{%user_socials}}',
            'user_id'
        );
        $this->addForeignKey(
            'fk_user_socials_user_id',
            '{{%user_socials}}',
            'user_id',
            '{{%user_users}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        // Create relation between table `user_reset_password_tokens` and table `user_users`
        $this->createIndex(
            'idx_user_reset_password_tokens_user_id',
            '{{%user_reset_password_tokens}}',
            'user_id'
        );
        $this->addForeignKey(
            'fk_user_reset_password_tokens_user_id',
            '{{%user_reset_password_tokens}}',
            'user_id',
            '{{%user_users}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        // Create relation between table `user_confirm_email_tokens` and table `user_users`
        $this->createIndex(
            'idx_user_confirm_email_tokens_user_id',
            '{{%user_confirm_email_tokens}}',
            'user_id'
        );
        $this->addForeignKey(
            'fk_user_confirm_email_tokens_user_id',
            '{{%user_confirm_email_tokens}}',
            'user_id',
            '{{%user_users}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * Drop relations.
     *
     * @return void
     */
    private function dropRelations()
    {
        // Drop relation between table `user_socials` and table `user_users`
        $this->dropForeignKey('fk_user_socials_user_id', '{{%user_socials}}');
        $this->dropIndex('idx_user_socials_user_id', '{{%user_socials}}');

        // Drop relation between table `user_profiles` and table `user_users`
        $this->dropForeignKey('fk_user_profiles_user_id', '{{%user_profiles}}');
        $this->dropIndex('idx_user_profiles_user_id', '{{%user_profiles}}');

        // Drop elation between table `user_reset_password_tokens` and table `user_users`
        $this->dropForeignKey('fk_user_reset_password_tokens_user_id', '{{%user_reset_password_tokens}}');
        $this->dropIndex('idx_user_reset_password_tokens_user_id', '{{%user_reset_password_tokens}}');

        // Drop elation between table `user_reset_password_tokens` and table `user_users`
        $this->dropForeignKey('fk_user_confirm_email_tokens_user_id', '{{%user_confirm_email_tokens}}');
        $this->dropIndex('idx_user_confirm_email_tokens_user_id', '{{%user_confirm_email_tokens}}');
    }
}
