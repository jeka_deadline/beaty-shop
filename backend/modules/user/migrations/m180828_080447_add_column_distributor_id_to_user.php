<?php

use yii\db\Migration;

/**
 * Class m180828_080447_add_column_distributor_id_to_user
 */
class m180828_080447_add_column_distributor_id_to_user extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        $this->addColumn('{{%user_users}}', 'distributor_id', $this->integer()->defaultValue(null));
        $this->createRelations();
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user_users}}', 'distributor_id');
        $this->dropRelations();
    }

    /**
     * Create relations betwenn table user payments and users.
     *
     * @return void
     */
    private function createRelations()
    {
        // create relations between table `core trainer distributors` and table `users`
        $this->createIndex('ix_distributor_id', '{{%user_users}}', 'distributor_id');
        $this->addForeignKey(
            'fk_distributor_id',
            '{{%user_users}}',
            'distributor_id',
            '{{%core_trainer_distributors}}',
            'id',
            'CASCADE',
            'SET NULL'
        );
    }

    /**
     * Drop relations betwenn table user payments and users.
     *
     * @return void
     */
    private function dropRelations()
    {
        // drop relations between table `core trainer distributors` and table `users`
        $this->dropForeignKey('fk_distributor_id', '{{%user_users}}');
        $this->dropIndex('ix_distributor_id', '{{%user_users}}');
    }
}
