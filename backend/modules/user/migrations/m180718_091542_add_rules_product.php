<?php

use yii\db\Migration;

/**
 * Class m180718_091542_add_rules_product
 */
class m180718_091542_add_rules_product extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $permitions=[
            [
                'name' => 'product/best-seller/create',
                'type' => 2,
                'description' => 'Module Product. Permission to add a product to the best seller section.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/best-seller/delete',
                'type' => 2,
                'description' => 'Module Product. Permission to remove a product from the best seller section.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/best-seller/index',
                'type' => 2,
                'description' => 'Module Product. Permission to view the list of products in the best seller section.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/best-seller/update',
                'type' => 2,
                'description' => 'Module Product. Permission to edit the product in the section of the best seller.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/best-seller/view',
                'type' => 2,
                'description' => 'Module Product. Permission to view product information in the best seller section.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],

            [
                'name' => 'product/coupon/create',
                'type' => 2,
                'description' => 'Module Product. Permission to add a coupon.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/coupon/delete',
                'type' => 2,
                'description' => 'Module Product. Permission to delete a coupon.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/coupon/index',
                'type' => 2,
                'description' => 'Module Product. Permission to view the list of coupons.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/coupon/view',
                'type' => 2,
                'description' => 'Module Product. Permission to view information about the coupon.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],

            [
                'name' => 'product/free-recommendet/create',
                'type' => 2,
                'description' => 'Module Product. Permission to add an unrelated recommended product.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/free-recommendet/delete',
                'type' => 2,
                'description' => 'Module Product. Permission to remove an unrelated recommended product.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/free-recommendet/index',
                'type' => 2,
                'description' => 'Module Product. Permission to view a list of non-tied recommended products.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/free-recommendet/update',
                'type' => 2,
                'description' => 'Module Product. Permission to edit an unrelated recommended product.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/free-recommendet/view',
                'type' => 2,
                'description' => 'Module Product. Permission to view information about an unrelated recommended product.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],

            [
                'name' => 'product/order/create',
                'type' => 2,
                'description' => 'Module Product. Permission to create an order.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/order/delete',
                'type' => 2,
                'description' => 'Module Product. Permission to delete an order.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/order/index',
                'type' => 2,
                'description' => 'Module Product. Permission to view the list of orders.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/order/update',
                'type' => 2,
                'description' => 'Module Product. Permission to edit the order.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/order/view',
                'type' => 2,
                'description' => 'Module Product. Permission to view order information.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],

            [
                'name' => 'product/product-category/create',
                'type' => 2,
                'description' => 'Module Product. Permission to create categories.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/product-category/delete',
                'type' => 2,
                'description' => 'Module Product. Permission to remove the cathorium.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/product-category/index',
                'type' => 2,
                'description' => 'Module Product. Permission to view the list of categories.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/product-category/update',
                'type' => 2,
                'description' => 'Module Product. Permission to edit a category.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/product-category/view',
                'type' => 2,
                'description' => 'Module Product. Permission to view information about the category.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/product-category/export',
                'type' => 2,
                'description' => 'Module Product. Permission to export categories.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/product-category/import',
                'type' => 2,
                'description' => 'Module Product. Permission to import categories.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],

            [
                'name' => 'product/product/create',
                'type' => 2,
                'description' => 'Module Product. Permission to create the product.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/product/delete',
                'type' => 2,
                'description' => 'Module Product. Permission to remove the product.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/product/index',
                'type' => 2,
                'description' => 'Module Product. Permission to view the list of products.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/product/update',
                'type' => 2,
                'description' => 'Module Product. Permission to edit the product.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/product/view',
                'type' => 2,
                'description' => 'Module Product. Permission to view product information.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],

            [
                'name' => 'product/product/export-inventory',
                'type' => 2,
                'description' => 'Module Product. Permission to view export inventory.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/product/import-inventory',
                'type' => 2,
                'description' => 'Module Product. Permission to import inventory.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/product/export-products',
                'type' => 2,
                'description' => 'Module Product. Permission to export products.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/product/import-products',
                'type' => 2,
                'description' => 'Module Product. Permission to import products.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/product/export-sale-price',
                'type' => 2,
                'description' => 'Module Product. Permission to export sale price.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/product/import-sale-price',
                'type' => 2,
                'description' => 'Module Product. Permission to import sale price.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/product/export-promotion-price',
                'type' => 2,
                'description' => 'Module Product. Permission to export promotion price.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/product/import-promotion-price',
                'type' => 2,
                'description' => 'Module Product. Permission to import promotion price.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],

            [
                'name' => 'product/product-filter/create',
                'type' => 2,
                'description' => 'Module Product. Permission to reduce the filter.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/product-filter/delete',
                'type' => 2,
                'description' => 'Module Product. Permission to remove the filter.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/product-filter/index',
                'type' => 2,
                'description' => 'Module Product. Permission to view the list of filters.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/product-filter/update',
                'type' => 2,
                'description' => 'Module Product. Permission to edit the filter.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/product-filter/view',
                'type' => 2,
                'description' => 'Module Product. Permission to view information about the filter.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],

            [
                'name' => 'product/product-review/create',
                'type' => 2,
                'description' => 'Module Product. Permission to revoke the product review.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/product-review/delete',
                'type' => 2,
                'description' => 'Module Product. Permission to delete a review of the product.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/product-review/index',
                'type' => 2,
                'description' => 'Module Product. Permission to view a list of product reviews.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/product-review/update',
                'type' => 2,
                'description' => 'Module Product. Permission to edit a product review.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/product-review/view',
                'type' => 2,
                'description' => 'Module Product. Permission to view information about the review of the product.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],

            [
                'name' => 'product/product-variation/create',
                'type' => 2,
                'description' => 'Module Product. Permission to create product variations.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/product-variation/duplicate',
                'type' => 2,
                'description' => 'Module Product. Permission to duplicate the product variation.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/product-variation/delete',
                'type' => 2,
                'description' => 'Module Product. Permission to remove product variation.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/product-variation/index',
                'type' => 2,
                'description' => 'Module Product. Permission to view the list of product variations.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/product-variation/update',
                'type' => 2,
                'description' => 'Module Product. Permission to edit product variations.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/product-variation/view',
                'type' => 2,
                'description' => 'Module Product. Permission to view information about product variations.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/product-variation/image-upload',
                'type' => 2,
                'description' => 'Module Product. Permission to upload image product variation.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/product-variation/image-delete',
                'type' => 2,
                'description' => 'Module Product. Permission to remove image product variation.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'product/product-variation/image-sort',
                'type' => 2,
                'description' => 'Module Product. Permission to sort image product variation.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
        ];

        $this->batchInsert(
            '{{%auth_item}}',
            ['name', 'type', 'description', 'created_at', 'updated_at'],
            $permitions
        );

        $data = [];

        foreach ($permitions as $permition) {
            $data[] = [
                'parent' => 'superadmin',
                'child' => $permition['name'],
            ];
        }

        if ($data) {
            $this->batchInsert(
                '{{%auth_item_child}}',
                ['parent', 'child'],
                $data
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return true;
    }
}
