<?php

use yii\db\Migration;

/**
 * Class m180718_113929_add_rules_statistic
 */
class m180718_113929_add_rules_statistic extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $permitions=[
            [
                'name' => 'statistic/index/index',
                'type' => 2,
                'description' => 'Module Statistic. Permission to view statistics.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'statistic/index/get-data-chart',
                'type' => 2,
                'description' => 'Module Statistic. Permission to receive statistical data for a period of time.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
        ];

        $this->batchInsert(
            '{{%auth_item}}',
            ['name', 'type', 'description', 'created_at', 'updated_at'],
            $permitions
        );

        $data = [];

        foreach ($permitions as $permition) {
            $data[] = [
                'parent' => 'superadmin',
                'child' => $permition['name'],
            ];
        }

        if ($data) {
            $this->batchInsert(
                '{{%auth_item_child}}',
                ['parent', 'child'],
                $data
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return true;
    }
}
