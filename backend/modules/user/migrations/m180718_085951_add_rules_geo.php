<?php

use yii\db\Migration;

/**
 * Class m180718_085951_add_rules_geo
 */
class m180718_085951_add_rules_geo extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $permitions=[
            [
                'name' => 'geo/city/create',
                'type' => 2,
                'description' => 'Module Geo. Permission to add a city.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'geo/city/delete',
                'type' => 2,
                'description' => 'Module Geo. Permission to remove the city.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'geo/city/index',
                'type' => 2,
                'description' => 'Module Geo. Permission to view the list of cities.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'geo/city/update',
                'type' => 2,
                'description' => 'Module Geo. Permission to edit data about the city.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'geo/city/view',
                'type' => 2,
                'description' => 'Module Geo. Permission to view information about the city.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],

            [
                'name' => 'geo/country/create',
                'type' => 2,
                'description' => 'Module Geo. Permission to add a country.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'geo/country/delete',
                'type' => 2,
                'description' => 'Module Geo. Permission to remove a country.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'geo/country/index',
                'type' => 2,
                'description' => 'Module Geo. Permission to view the list of countries.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'geo/country/update',
                'type' => 2,
                'description' => 'Module Geo. Permission to edit the country data.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'geo/country/view',
                'type' => 2,
                'description' => 'Module Geo. Permission to view information about the country.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
        ];

        $this->batchInsert(
            '{{%auth_item}}',
            ['name', 'type', 'description', 'created_at', 'updated_at'],
            $permitions
        );

        $data = [];

        foreach ($permitions as $permition) {
            $data[] = [
                'parent' => 'superadmin',
                'child' => $permition['name'],
            ];
        }

        if ($data) {
            $this->batchInsert(
                '{{%auth_item_child}}',
                ['parent', 'child'],
                $data
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return true;
    }
}
