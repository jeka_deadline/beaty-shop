<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_payment_cards`.
 */
class m180419_141316_create_user_payments_table extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        $this->createTable('{{%user_payment_cards}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'name' => $this->string(255),
            'number' => $this->string(16),
            'month' => $this->integer(),
            'year' => $this->integer(),
            'created_at' => $this->timestamp()->null()->defaultValue(null),
            'updated_at' => $this->timestamp()->null()->defaultValue(null),
        ]);

        $this->createRelations();
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        $this->dropRelations();
        $this->dropTable('{{%user_payment_cards}}');
    }

    /**
     * Create relations betwenn table user payments and users.
     *
     * @return void
     */
    private function createRelations()
    {
        // create relations between table `user_payment_cards` and table `users`
        $this->createIndex('ix_user_payments_user_id', '{{%user_payment_cards}}', 'user_id');
        $this->addForeignKey(
            'fk_user_payments_user_id',
            '{{%user_payment_cards}}',
            'user_id',
            '{{%user_users}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * Drop relations betwenn table user payments and users.
     *
     * @return void
     */
    private function dropRelations()
    {
        // drop relations between table `user_payment_card` and table `users`
        $this->dropForeignKey('fk_user_payments_user_id', '{{%user_payment_cards}}');
        $this->dropIndex('ix_user_payments_user_id', '{{%user_payment_cards}}');
    }
}
