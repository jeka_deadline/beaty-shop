<?php

use yii\db\Migration;

/**
 * Class m180816_112504_add_rules_slogan_view
 */
class m180816_112504_add_rules_slogan_view extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $permitions=[
            [
                'name' => 'core/slogan/view',
                'type' => 2,
                'description' => 'Module Core. Permission to view the slogan.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
        ];

        $this->batchInsert(
            '{{%auth_item}}',
            ['name', 'type', 'description', 'created_at', 'updated_at'],
            $permitions
        );

        $data = [];

        foreach ($permitions as $permition) {
            $data[] = [
                'parent' => 'superadmin',
                'child' => $permition['name'],
            ];
        }

        if ($data) {
            $this->batchInsert(
                '{{%auth_item_child}}',
                ['parent', 'child'],
                $data
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
