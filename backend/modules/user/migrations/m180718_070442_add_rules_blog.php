<?php

use yii\db\Migration;

/**
 * Class m180718_070442_add_rules_blog
 */
class m180718_070442_add_rules_blog extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $permitions=[
            [
                'name' => 'blog/article/create',
                'type' => 2,
                'description' => 'Module Blog. Permission to create an article.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'blog/article/delete',
                'type' => 2,
                'description' => 'Module Blog. Permission to delete an article.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'blog/article/index',
                'type' => 2,
                'description' => 'Module Blog. Permission to view the list of articles.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'blog/article/update',
                'type' => 2,
                'description' => 'Module Blog. Permission to edit an article.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'blog/article/view',
                'type' => 2,
                'description' => 'Module Blog. Permission to view the article.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],

            [
                'name' => 'blog/slider-image/create',
                'type' => 2,
                'description' => 'Module Blog. Permission to create a slider.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'blog/slider-image/delete',
                'type' => 2,
                'description' => 'Module Blog. Permission to delete the slider.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'blog/slider-image/index',
                'type' => 2,
                'description' => 'Module Blog. Permission to view the list of sliders.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'blog/slider-image/update',
                'type' => 2,
                'description' => 'Module Blog. Permission to edit the slider.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'blog/slider-image/view',
                'type' => 2,
                'description' => 'Module Blog. Permission for the information about the slider.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
            [
                'name' => 'blog/slider-image/image-delete',
                'type' => 2,
                'description' => 'Module Blog. Permission to delete an image from the slider.',
                'created_at'  => time(),
                'updated_at'  => time(),
            ],
        ];

        $this->batchInsert(
            '{{%auth_item}}',
            ['name', 'type', 'description', 'created_at', 'updated_at'],
            $permitions
        );

        $data = [];

        foreach ($permitions as $permition) {
            $data[] = [
                'parent' => 'superadmin',
                'child' => $permition['name'],
            ];
        }

        if ($data) {
            $this->batchInsert(
                '{{%auth_item_child}}',
                ['parent', 'child'],
                $data
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return true;
    }
}
