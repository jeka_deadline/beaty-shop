<?php

use yii\db\Migration;

/**
 * Class m180511_131946_new_user_profile_fields
 */
class m180511_131946_new_user_profile_fields extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        $this->addColumn('{{%user_profiles}}', 'title', $this->string(10)->null());
        $this->addColumn('{{%user_profiles}}', 'company', $this->string(100)->null());
        $this->addColumn('{{%user_profiles}}', 'date_birth', $this->date()->null());
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user_profiles}}', 'date_birth');
        $this->dropColumn('{{%user_profiles}}', 'company');
        $this->dropColumn('{{%user_profiles}}', 'title');
    }
}
