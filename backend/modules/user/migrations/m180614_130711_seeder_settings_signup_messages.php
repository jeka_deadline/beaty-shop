<?php

use yii\db\Migration;

/**
 * Class m180614_130711_seeder_settings_signup_messages
 */
class m180614_130711_seeder_settings_signup_messages extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        $this->insert('{{%core_settings}}', [
            'key' => 'core.something.wrong.text',
            'name' => 'Modal message something wrong',
            'value' => 'Something went wrong, try again later',
            'type' => 'text',
        ]);

        $this->insert('{{%core_settings}}', [
            'key' => 'user.success.signup.text',
            'name' => 'Modal message for success user signup',
            'value' => 'Your success signup, detail instructions send to you email',
            'type' => 'text',
        ]);
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        return true;
    }
}
