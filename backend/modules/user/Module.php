<?php

namespace backend\modules\user;

/**
 * User class module extends yii base Module
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\user\controllers';

    /**
     * {@inheritdoc}
     *
     * @return viod
     */
    public function init()
    {
        parent::init();
    }
}
