<?php
/** @var \backend\modules\user\models\forms\UserForm $model */
/** @var \yii\web\View $this */
?>

<h2>Update user</h2>

<div class="page page-dashboard">

    <?= $this->render('user_form', compact('model')); ?>

</div>