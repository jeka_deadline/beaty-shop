<?php

use yii\widgets\DetailView;
use yii\helpers\Html;

/** @var \yii\web\View $this */
/** @var \backend\modules\user\models\User $user */
?>

<?= Html::a('Delete', ['delete', 'id' => $user->id], ['class' => 'btn btn-danger']); ?>

<?= Html::a('Update', ['update', 'id' => $user->id], ['class' => 'btn btn-primary']); ?>

<?= Html::a('Back', ['index'], ['class' => 'btn btn-default']); ?>

<h2>User information</h2>

<?= DetailView::widget([
    'model' => $user,
    'attributes' => [
        'id',
        'email',
        [
            'label' => 'Is email confirm?',
            'value' => ($user->confirm_email_at) ? 'Yes' : 'No',
        ],
        [
            'label' => 'Is user block?',
            'value' => ($user->blocked_at) ? 'Yes' : 'No',
        ],
        'created_at',
        'updated_at',
        'register_ip',
    ],
]); ?>

<h3>User profile</h3>

<?= DetailView::widget([
    'model' => $user,
    'attributes' => [
        'profile.title',
        'profile.surname',
        'profile.name',
        'profile.phone',
        'profile.date_birth',
        'profile.company',
    ],
]); ?>