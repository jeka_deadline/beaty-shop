<?php

/** @var \backend\modules\user\models\forms\UserForm $model */
/** @var \yii\web\View $this */
/** @var \yii\widgets\ActiveForm $form */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use trntv\yii\datetime\DateTimeWidget;

?>

<?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'email')->textInput(); ?>

    <?php if ($model->scenario === $model::SCENARIO_CREATE) : ?>

        <?= $form->field($model, 'password')->passwordInput(); ?>

        <?= $form->field($model, 'repeatPassword')->passwordInput(); ?>

    <?php endif; ?>

    <?= $form->field($model, 'title')->dropDownList($model->getListUserTitles()); ?>

    <?= $form->field($model, 'surname')->textInput(); ?>

    <?= $form->field($model, 'name')->textInput(); ?>

    <?= $form->field($model, 'company'); ?>

    <?= $form->field($model, 'phone')->textInput(); ?>

    <?= $form->field($model, 'dateBirth')->widget(DateTimeWidget::className(), [
        'phpDatetimeFormat' => 'yyyy-MM-dd',
    ]); ?>

    <?php if ($model->scenario === $model::SCENARIO_CREATE) : ?>

        <?= $form->field($model, 'confirmEmail')->checkbox(); ?>

    <?php endif; ?>

    <?= Html::submitButton(($model->scenario === $model::SCENARIO_CREATE) ? 'Create' : 'Update', ['class' => 'btn btn-primary']); ?>

    <?= Html::a('Back', ['index'], ['class' => 'btn btn-default']); ?>

<?php ActiveForm::end(); ?>