<?php

/** @var \backend\modules\user\models\forms\UserForm $model */
/** @var \yii\web\View $this */
/** @var \backend\modules\user\models\searcModels\UserSearch $searchModel */
/** @var \yii\data\ActiveDataProvider $dataProvider */

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\user\Role;
use yii\helpers\Url;
use common\widgets\Alert;

?>
<div class="page page-dashboard table-responsive">

    <?= Html::a('Create user', Url::toRoute(['/user/admin/create-user']), ['class' => 'btn btn-success']); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
    		    [
                'attribute' => 'email',
            ],
            [
              	'attribute' => 'confirm_email_at',
              	'content' => function($model) {
              		  if ($model->confirm_email_at) {
                        return 'Activated';
              		  } else {
                        return Html::a(
              				      'Activate',
              				      Url::toRoute([
                                '/user/admin/confirm-email',
                                'id' => $model->id
              				      ]),
              				      ['class' => 'btn btn-success btn-xs']
                        );
                    }
              	},
                'filter' => false,
            ],
            [
                'attribute' => 'blocked_at',
                'content' => function($model) {
            		    if ($model->blocked_at) {
                        $name = 'Unblock';
                        $class = ['class' => 'btn btn-success btn-xs'];
                        $action = 'unblock';
            		    } else {
            		        $name = 'Block';
            		        $class = ['class' => 'btn btn-danger btn-xs'];
                        $action = 'block';
            		    }

            		    return Html::a(
            				    $name,
            				    Url::toRoute([
                            $action,
                            'id' => $model->id
            				    ]),
            				    ['class' => $class]
            			   );
                },
                'filter' => false,
            ],
            'distributor_id',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}&nbsp;&nbsp;{update}&nbsp;&nbsp;{permit}&nbsp;&nbsp;{delete}',
                'buttons' => [
                    'permit' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-wrench"></span>', Url::to(['/permit/user/view', 'id' => $model->id]), [
                            'title' => Yii::t('yii', 'Change user role')
                        ]); },
                ]
            ],
    	],
    ]); ?>

</div>