<?php

namespace backend\modules\user\controllers;

use Yii;
use yii\web\Controller;
use backend\modules\user\models\User;
use backend\modules\user\models\searchModels\UserSearch;
use backend\modules\user\models\forms\UserForm;
use yii\web\NotFoundHttpException;

class AdminController extends Controller
{
    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \developeruz\db_rbac\behaviors\AccessBehavior::className(),
                'login_url' => Yii::$app->user->loginUrl,
            ],
        ];
    }

    /**
     * Display page with all users.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', compact('searchModel', 'dataProvider'));
    }

    /**
     * Block user.
     *
     * @param int $id User id
     * @return string
     */
    public function actionBlock($id)
    {
        $user = $this->findUserById($id);

        if (!$user->isBlocked()) {
            // Attempt block self user
            if (intval($id) === Yii::$app->user->id) {
                Yii::$app->session->setFlash('error', 'Вы не можете заблокировать самого себя');
            } else {
                // block user
                if ($user->block()) {
                    Yii::$app->session->setFlash('success', 'Пользователь заблокирован');
                } else {
                    Yii::$app->session->setFlash('error', 'Не удалось заблокировать пользователя');
                }
            }
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Unblock user.
     *
     * @param int $id User id
     * @return string
     */
    public function actionUnblock($id)
    {
        $user = $this->findUserById($id);

        if ($user->isBlocked()) {
            if ($user->unblock()) {
                Yii::$app->session->setFlash('success', 'Пользователь разблокирован');
            } else {
                Yii::$app->session->setFlash('error', 'Не удалось разблокировать пользователя');
            }
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Confirm user email.
     *
     * @param int $id User id
     * @return string
     */
    public function actionConfirmEmail($id)
    {
        $user = $this->findUserById($id);

        if (!$user->isConfirmEmail()) {
            if ($user->confirmEmail()) {
                Yii::$app->session->setFlash('success', '"Электронная почта пользователя подтверждена"');
            } else {
                Yii::$app->session->setFlash('error', 'Не удалось подтвердить почту пользователя');
            }
        } else {
            Yii::$app->session->setFlash('info', 'Электронная почта данного пользователя уже подтверждена');
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Delete user.
     *
     * @var int $id User id
     * @return string
     */
    public function actionDelete($id)
    {
        $user = $this->findUserById($id);

        if (intval($id) === Yii::$app->user->id) {
            Yii::$app->session->setFlash('error', 'You can\'t delete self');
        } else {
            if ($user->delete()) {
                Yii::$app->session->setFlash('success', 'User success delete');
            } else {
                Yii::$app->session->setFlash('error', 'User not delete');
            }
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Create new user.
     *
     * @return midex
     */
    public function actionCreateUser()
    {
        $model = new UserForm();
        $model->scenario = UserForm::SCENARIO_CREATE;

        if ($model->load(Yii::$app->request->post()) && $model->createUser()) {

            Yii::$app->session->setFlash('succes', 'Пользователь успешно создан');

            return $this->redirect(['index']);
        }

        $model->resetPasswordFields();

        return $this->render('create-user', compact('model'));
    }

    /**
     * View user information.
     *
     * @var int $id User id
     * @return midex
     */
    public function actionView($id)
    {
        $user = $this->findUserById($id);

        return $this->render('view', compact('user'));
    }

    /**
     * Update user information.
     *
     * @var int $id User id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $user = $this->findUserById($id);

        $model = new UserForm($user);

        if ($model->load(Yii::$app->request->post()) && $model->updateUser()) {
            return $this->redirect(['view', 'id' => $user->id]);
        }

        $model->resetPasswordFields();

        return $this->render('update-user', compact('model'));
    }

    /**
     * Find user by id.
     *
     * @throws NotFoundHttpException If user not found
     * @param int $id User id
     * @return User
     */
    private function findUserById($id)
    {
        $user = User::findIdentity($id);

        if (!$user) {
            throw new NotFoundHttpException('User not found');
        }

        return $user;
    }
}
