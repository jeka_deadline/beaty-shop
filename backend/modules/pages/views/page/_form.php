<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;

/* @var $this yii\web\View */
/* @var $model backend\modules\pages\models\Page */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="page-form">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'header')->textInput(['maxlength' => true]); ?>

        <?= $form->field($model, 'uri')->textInput(['maxlength' => true]); ?>

        <?= ''/*$form->field($model, 'content')->widget(CKEditor::className(), [
            'editorOptions' => ElFinder::ckeditorOptions([
                'elfinder',
                'path' => 'pages',
                'filter' => 'image'
            ]),
        ]);*/ ?>

        <?= $form->field($model, 'content')->textarea(['rows' => 10]); ?>

        <?= $form->field($model, 'body_class')->textInput(['maxlength' => true]); ?>

        <?= $form->field($model, 'display_order')->textInput(); ?>

        <?= $form->field($model, 'active')->checkbox(); ?>

        <?= $form->field($model, 'meta_title')->textInput(['maxlength' => true]); ?>

        <?= $form->field($model, 'meta_description')->textarea(['rows' => 6]); ?>

        <?= $form->field($model, 'meta_keywords')->textInput(['maxlength' => true]); ?>

        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>
