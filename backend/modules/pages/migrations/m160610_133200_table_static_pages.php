<?php

use yii\db\Migration;

class m160610_133200_table_static_pages extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        // таблица страниц
        $this->createTable('{{%pages_static_page}}', [
            'id'               => $this->primaryKey(),
            'uri'              => $this->string(100)->notNull(),
            'header'           => $this->string(255)->notNull(),
            'menu_class'       => $this->string(255)->defaultValue(null),
            'body_class'       => $this->string(255)->defaultValue(null),
            'content'          => $this->text()->defaultValue(null),
            'display_order'    => $this->integer()->defaultValue(0),
            'active'           => $this->smallInteger(1)->defaultValue(0),
            'meta_title'       => $this->string(255)->null(),
            'meta_description' => $this->text()->null(),
            'meta_keywords'    => $this->string(255)->null(),
        ]);

        $this->createIndex('uix_pages_static_page_uri', '{{%pages_static_page}}', 'uri', true);
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        $this->dropTable('{{%pages_static_page}}');
    }
}
