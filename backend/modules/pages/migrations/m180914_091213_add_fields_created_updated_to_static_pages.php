<?php

use yii\db\Migration;

/**
 * Class m180914_091213_add_fields_created_updated_to_static_pages
 */
class m180914_091213_add_fields_created_updated_to_static_pages extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeUp()
    {
        $this->addColumn('{{%pages_static_page}}', 'created_at', $this->timestamp()->null()->defaultValue(null));
        $this->addColumn('{{%pages_static_page}}', 'updated_at', $this->timestamp()->null()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function safeDown()
    {
        $this->dropColumn('{{%pages_static_page}}', 'created_at');
        $this->dropColumn('{{%pages_static_page}}', 'updated_at');
    }
}
