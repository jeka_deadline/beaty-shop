<?php

namespace backend\modules\pages\models;

use Yii;
use common\models\pages\Page as BasePage;
use yii\helpers\ArrayHelper;

/**
 * Backend model page extends common model Page.
 */
class Page extends BasePage
{
    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['display_order'], 'default', 'value' => 0],
                [['uri'], 'match', 'pattern' => '#^[a-z][a-z\d_-]*[a-z\d]$#'],
            ]
        );
    }
}
