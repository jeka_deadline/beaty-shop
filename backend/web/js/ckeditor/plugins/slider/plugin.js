CKEDITOR.plugins.add('slider', {
    requires: ['richcombo'],
    init: function (editor) {
        editor.ui.addRichCombo('slider', {
            label: 'Slider',
            title: 'Slider',
            voiceLabel: 'Slider',
            className: 'cke_format',
            multiSelect: false,
            panel: {
                css: [editor.config.contentsCss, CKEDITOR.skin.getPath('editor')],
                voiceLabel: editor.lang.panelVoiceLabel
            },
            init: function () {
                for (var i in listSlider) {
                    this.add(listSlider[i][0], listSlider[i][1], listSlider[i][2]);
                }
            },
            onClick: function (value) {
                editor.insertHtml( ' '+value+' ');
            }
        });
    }
});