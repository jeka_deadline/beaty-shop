function productVariationImagesSort(event, params) {
    $.ajax({
        type: "POST",
        url: '/admin/product/product-variation/image-sort',
        dataType: 'JSON',
        data: {
            "data": params.stack
        },
        success: function (data) {},
        error: function () {}
    });
}

function productRecommendetsWidgetSearchSelect(ev, suggestion) {
    var s = window.location.search;
    s = s.match(new RegExp('id=([^&=]+)'));
    var id = s ? s[1] : false;
    $(this).typeahead("val", "");
    if ($(this).parents('.product-recommendets-input-widget').find('select option[value="' + suggestion.product_id + '"]').length || suggestion.product_id == id) return;
    var template = '<div class="col-sm-3 col-md-3 product-item">' +
        '<div class="thumbnail" style="height: 300px">'+
            '<div class="text-right">'+
                '<a href="#" class="text-danger click-delete" data-id="{{product_id}}">'+
                    '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'+
                '</a>'+
            '</div>'+
            '<img src="{{image}}" alt="{{name}}" style="height: 182px">'+
            '<div class="caption">'+
                '<h4>{{name}}</h4>'+
            '</div>'+
        '</div>'+
    '</div>';
    var template = Handlebars.compile(template);
    $(this).parents('.product-recommendets-input-widget').find('.panel .panel-body').append(template(suggestion));
    $(this).parents('.product-recommendets-input-widget').find('select').append($(new Option(suggestion.value, suggestion.product_id)).attr('selected','selected'));
}

function productFreeRecommendetsWidgetSearchSelect(ev, suggestion) {
    $(this).parents('.product-free-recommendets-input-widget').find('input[type=hidden]:first').val(suggestion.product_id);
}

$("body").on("click", ".product-recommendets-input-widget .panel-body .click-delete", function() {
    var id = parseInt($(this).attr('data-id'));
    if (id) {
        $(this).parents('.product-recommendets-input-widget').find('select option[value="' + id + '"]').remove();
        $(this).parents('.product-item').remove();
    }
    return false;
});

function productFiltersSortUpdate(e) {
    var nameAttribute = $(e.target).parent().find('input[data-name-input]').attr('data-name-input');
    var filtersBlock = $(e.target).parents('.filters-block');
    var ids = filtersBlock.find('[name="v1-'+nameAttribute+'"]').val().split(',');
    var select = filtersBlock.find('select[name^="'+nameAttribute+'"]');
    select.find('option').remove();
    $.each(ids, function() {
        select.append($(new Option(this, this)).attr('selected','selected'));
    });
}