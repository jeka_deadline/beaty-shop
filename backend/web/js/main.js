$(function() {
    //Ajax запрос при выборе категории
    $('#crud-form').on("change", '#item-category', function(event){
      var value = $('#item-category').val();
      $.ajax({
          type: 'POST',
          url: 'chose-master-model',
          data: {'categoryId': value},
          dataType: 'json',
          success: function(data) {
             var select = $('#item');
             select.empty();
             var select1 = document.getElementById('item');
             var option = document.createElement('option');
             option.text = 'Choose masterProduct';
             option.value ='';
             select.append(option);
             data.forEach(function(item, i, arr) {
                var option = document.createElement('option');
                option.text = item.title;
                option.value = item.id;
                select.append(option);
             })
             select.removeAttr('disabled');
            },
            error:  function(xhr, str){}
        });
    });

    //Ajax запрос при выборе мастер продукта
    $('#crud-form').on("change", '#item', function(event){
       var value = $('#item').val();
       if(value){
           $.ajax({
               type: 'POST',
               url: 'chose-master-model',
               data: {'productId': value},
               dataType: 'json',
               success: function(data){
                   $('.myHide').closest('.form-group').addClass('hide');
                   $.each(data, function(key, val){
                       if(key!='id' && key!='preview'){
                           var elem = $('#item-'+key);
                           elem.val(val);
                       };

                   });
                   $('#description').text('Добавить товар');
               },
               error: function (xhr, str) {}
           });
       } else {
           $('.hide').removeClass('hide');
           $("#id_form").trigger('reset');
           $('#description').text('Добавить мастер товар');
       }
    });

    $('.updateHide').closest('.form-group').addClass('hide');

    //Ajax запрос при проверке IP
    $('#check_ip').on('click', function(){
        var regexp = /\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/;
        var ip = $('#ip').val();
        if(regexp.test(ip)){
            $.ajax({
                type: 'GET',
                url: 'http://www.freegeoip.net/json/'+ip,
                dataType: 'json',
                success: function(data){
                    var region;
                    if(data.region_name !=''){
                        region ='страна: '+data.country_name + ' регион: ' + data.region_name;
                        $('#regiongroup-region').attr("value", data.region_name);
                    }
                    else{
                        region='Неудалось определить регион';
                    }
                    $('#region').empty();
                    $('#region').append(region);
                },
                error: function(xhr, str){
                    alert('неудалось определить регион');
                }
            })
        }
        else{
            alert('Вееден неправильный IP адресс')
        }
    });

    $(document).on('keyup', '#input-promotion', function( e ) {
        var promotionVal = $(this).val();

        if (promotionVal.indexOf('%') > 0) {
            var priceVal = $('#input-price').val();
            var percentage = promotionVal.substring(0, promotionVal.indexOf('%'));

            $(this).val((priceVal - priceVal * (percentage / 100)).toFixed(2));
        }
    });

    $(document).on('submit', '#move-to-category', function( e ) {
        e.preventDefault();
        var data = {};
        data[ 'DuplicateCategoryForm[ids]' ] = $('#grid-with-select').yiiGridView('getSelectedRows');
        $(this).find('select, input').each(function(index, input) {
            data[ $(input).attr('name') ] = $(input).val();
        });

        $.post($(this).attr('action'), data);
    })
});