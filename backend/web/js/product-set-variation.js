function productSetWidgetSearchSelect(ev, suggestion) {
    $(this).typeahead("val", "");

    if ($(this).parents('.product-set-variations-input-widget').find('select option[value="' + suggestion.id + '"]').length) return;

    var ul = $(this).parents('.product-set-variations-input-widget').find('.panel .panel-body');
    ul.append(suggestion.template);
    $(this).parents('.product-set-variations-input-widget').find('select').append($(new Option(suggestion.value, suggestion.id)).attr('selected','selected'));
}

function productEditSetWidgetSearchSelect(ev, suggestion) {
    $(this).typeahead("val", "");

    if ($(this).parents('.product-set-variations-input-widget').find('select option[value="' + suggestion.product_id + '"]').length) return;

    var ul = $(this).parents('.product-set-variations-input-widget').find('.panel .panel-body');
    ul.append(suggestion.template);
    $(this).parents('.product-set-variations-input-widget').find('select').append($(new Option(suggestion.value, suggestion.product_id)).attr('selected','selected'));
}

$("body").on("click", ".product-set-variations-input-widget .panel-body .click-delete", function() {
    var id = parseInt($(this).attr('data-id'));
    if (id) {
        $(this).parents('.product-set-variations-input-widget').find('select option[value="' + id + '"]').remove();
        $(this).parents('.product-item').remove();
    }
    return false;
});

$("body").on("input", ".product-set-variations-input-widget .panel-body input[name$='[count]']", function() {
    var input = $(this);
    $.ajax({
        type: 'POST',
        url: 'validate-set-items',
        data: {
            'id': parseInt(input.attr('data-id')),
            'count': input.val()
        },
        dataType: 'json',
        success: function(data) {
            var block = input.parent('div.form-group');

            block.removeClass('has-error');
            block.find('.help-block').text('');

            if (data.status == 'error') {
                block.addClass('has-error');
                block.find('.help-block').text(data.errors.count);
            }
        },
        error:  function(xhr, str){}
    });

    return false;
});