function drawChartStatisticSales() {
    var statisticData = $('#statistic-sales').attr('data-chart-data');
    if (!statisticData) return;
    statisticData = JSON.parse(statisticData);

    var data = google.visualization.arrayToDataTable(statisticData);

    var options = {
        legend: { position: 'none' },
        width: $('.box-statistic-charts .tab-pane.active').width()
    };

    var chartSales = new google.visualization.LineChart(document.getElementById('statistic-sales'));

    chartSales.draw(data, options);
}

function drawChartStatisticOrders() {
    var statisticData = $('#statistic-orders').attr('data-chart-data');
    if (!statisticData) return;
    statisticData = JSON.parse(statisticData);

    var data = google.visualization.arrayToDataTable(statisticData);

    var options = {
        legend: { position: 'none' },
        width: $('.box-statistic-charts .tab-pane.active').width()
    };

    var chartSales = new google.visualization.LineChart(document.getElementById('statistic-orders'));

    chartSales.draw(data, options);
}

function drawChartAverageCart() {
    var statisticData = $('#statistic-cart-value').attr('data-chart-data');
    if (!statisticData) return;
    statisticData = JSON.parse(statisticData);

    var data = google.visualization.arrayToDataTable(statisticData);

    var options = {
        legend: { position: 'none' },
        width: $('.box-statistic-charts .tab-pane.active').width()
    };

    var chartSales = new google.visualization.LineChart(document.getElementById('statistic-cart-value'));

    chartSales.draw(data, options);
}

function getAJAXDataChartShopStatistic() {
    var form = $('#chart-date_range').parents('form');

    $.ajax({
        type: "POST",
        url: form.attr('action'),
        dataType: 'JSON',
        data: form.serializeArray(),
        success: function (data) {
            if (data.status != 'ok') return;

            $('[data-model="sale-sum"]').text('€'+data.sales.sum);
            $('#statistic-sales').attr('data-chart-data', data.sales.data);
            $('#statistic-sales').empty();
            drawChartStatisticSales();

            $('[data-model="count-orders"]').text(data.orders.count);
            $('#statistic-orders').attr('data-chart-data', data.orders.data);
            $('#statistic-orders').empty();
            drawChartStatisticOrders();

            $('[data-model="average-cart"]').text('€'+data.average.sum);
            $('#statistic-cart-value').attr('data-chart-data', data.average.data);
            $('#statistic-cart-value').empty();
            drawChartAverageCart();
        },
    });
}

$(function() {

    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChartStatisticSales);
    google.charts.setOnLoadCallback(drawChartStatisticOrders);
    google.charts.setOnLoadCallback(drawChartAverageCart);

    $("body").on("click", "[data-set-range-date]", function() {
        $('#chart-date_range').val($(this).data('set-range-date'));
        $("[data-set-range-date]").removeClass('active');
        $(this).addClass('active');
        getAJAXDataChartShopStatistic();
    });


});