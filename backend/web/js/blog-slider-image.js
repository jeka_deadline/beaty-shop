function sortBlogSliderImages(event, params) {
    var select = $('#sliderimage-data');
    select.empty();
    $.each(params.stack, function (key, stack) {
        var str = stack.key.substring(stack.key.search('/')+1,stack.key.length);
        select.append($(new Option(str, str)).attr('selected','selected'));
    });
}

function deleteBlogSliderImages(vKey, jqXHR) {
    var options = $('#sliderimage-data option');
    $.each(options, function (key, option) {
        var str = jqXHR.substring(jqXHR.search('/')+1,jqXHR.length);
        if ($(option).val() == str) {
            $(options[key]).remove();
        }
    });
}