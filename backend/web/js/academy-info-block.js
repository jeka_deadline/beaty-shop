function sortInfoBlockImages(event, params) {
    var select = $(this).parents('form').find('[name$="[images][]"]');
    select.empty();
    $.each(params.stack, function (key, stack) {
        select.append($(new Option(stack.key, stack.key)).attr('selected','selected'));
    });
}

function deleteInfoBlockImages(vKey, jqXHR) {
    var options = $(this).parents('form').find('[name$="[images][]"] option');
    $.each(options, function (key, option) {
        if ($(option).val() == jqXHR) {
            $(options[key]).remove();
        }
    });
}