<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=dbname',
            'username' => 'username',
            'password' => 'password',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp host',  // e.g. smtp.mandrillapp.com or smtp.gmail.com
                //'viewPath' => '@common/mail',
                'username' => 'smtp username',
                'password' => 'smtp password',
                'port' => 'smpt port', // Port 25 is a very common port too
                'encryption' => 'smtp encryption',
                // if smtp work with ssl and local machine
                'streamOptions' => [
                    'ssl' => [
                        'allow_self_signed' => true,
                        'verify_peer'       => false,
                        'verify_peer_name'  => false,
                    ],
                ]
            ],
        ],
    ],
];
