<?php

namespace common\helpers;

use yii\helpers\Url;
use common\components\Payone as BasePayone;

class Payone
{
    public static function sendCapture($order)
    {
        $parameters = [
            "request" => "capture",
            "txid" => $order->credit_txid,
            "amount" => str_replace('.', '', $order->full_sum),
            'currency' => 'EUR',
            "capturemode" => "completed",
            "sequencenumber" => "1"
        ];

        $request = array_merge(self::getDefaults(), $parameters);

        return BasePayone::sendRequest($request);
    }

    protected static function getDefaults()
    {
        return [
            "aid" => "41289",
            "mid" => "40937",
            "portalid" => "2029517",
            "key" => hash("md5", "9Z0IqKu17cs6XLR7"), // the key has to be hashed as md5
            "api_version" => "3.8",
            "mode" => (YII_DEBUG) ? "test" : "live", // can be "live" for actual transactions
            "encoding" => "UTF-8"
        ];
    }

    protected static function getUserPersonalData($shippingInfo)
    {
        return [
            "salutation" => ucfirst($shippingInfo->title),
            "firstname" => $shippingInfo->name,
            "lastname" => $shippingInfo->surname,
            "street" => $shippingInfo->address1,
            "zip" => $shippingInfo->zip,
            "city" => $shippingInfo->city,
            "country" => ($shippingInfo->country) ? strtoupper($shippingInfo->country->code) : 'EN',
            "email" => $shippingInfo->email,
            "language" => "en"
        ];
    }

    protected static function getItems($cart)
    {
        $items = [];

        $index = 1;

        foreach ($cart as $itemCart) {
            $items[ 'de[' . $index . ']' ] = $itemCart->productVariationName;
            $items[ 'it[' . $index . ']' ] = 'goods';
            $items[ 'id[' . $index . ']' ] = $itemCart->productVariationId;
            $items[ 'pr[' . $index . ']' ] = $itemCart->price;
            $items[ 'no[' . $index . ']' ] = $itemCart->count;

            $index++;
        }

        return $items;
    }
}