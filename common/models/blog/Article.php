<?php

namespace common\models\blog;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "blog_articles".
 *
 * @property int $id
 * @property string $slug
 * @property string $name
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $content
 * @property string $image
 * @property string $small_image
 * @property string $date
 * @property int $active
 * @property string $created_at
 * @property string $updated_at
 *
 * @property ArticleLangField[] $blogArticlesLangFields
 */
class Article extends \yii\db\ActiveRecord
{
    public static $path = 'files/blog/preview/';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'blog_articles';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new \yii\db\Expression('NOW()')
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['slug', 'name', 'date'], 'required'],
            [['meta_description', 'content'], 'string'],
            [['date', 'created_at', 'updated_at'], 'safe'],
            [['active'], 'integer'],
            [['slug', 'name', 'meta_title', 'meta_keywords', 'image', 'small_image'], 'string', 'max' => 255],
            [['slug'], 'unique'],
            [['slug'], 'match', 'pattern' => '#^[a-z][a-z\d-]*[a-z\d]$#'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'slug' => 'Slug',
            'name' => 'Name',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'meta_keywords' => 'Meta Keywords',
            'content' => 'Content',
            'image' => 'Image',
            'small_image' => 'Small Image',
            'date' => 'Date',
            'active' => 'Active',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlogArticlesLangFields()
    {
        return $this->hasMany(ArticleLangField::className(), ['article_id' => 'id']);
    }

    public function getUrlImageInput() {
        if (!$this->image) return [];

        return ['/'.self::$path.$this->image];
    }

    public function getUrlImage() {
        return '/'.self::$path.$this->image;
    }

    public function getUrlSmallImage() {
        if ($this->small_image && file_exists(Yii::getAlias('@frontend/web') . '/' . self::$path.$this->small_image)) {
            return '/'.self::$path.$this->small_image;
        }

        return '/'.self::$path.$this->image;
    }

}
