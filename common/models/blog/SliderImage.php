<?php

namespace common\models\blog;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "blog_slider_images".
 *
 * @property int $id
 * @property string $name
 * @property string $data
 * @property string $created_at
 * @property string $updated_at
 */
class SliderImage extends \yii\db\ActiveRecord
{

    public static $path = 'files/blog/sliders/';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'blog_slider_images';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new \yii\db\Expression('NOW()')
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['data'], 'safe'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'data' => 'Data',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function afterFind()
    {
        $data = unserialize($this->data);
        $this->data = $data?:'';

        parent::afterFind();
    }
}
