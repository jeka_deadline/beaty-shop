<?php

namespace common\models\product;

use frontend\modules\core\models\Language;
use Yii;
use common\models\user\User;

/**
 * This is the model class for table "{{%product_orders}}".
 *
 * @property int $id
 * @property string $order_number
 * @property int $user_id
 * @property string $type_payment
 * @property string $status
 * @property double $total_sum
 * @property double $shipping_price
 * @property double $tax
 * @property double $full_sum
 * @property int $is_new
 * @property string $created_at
 * @property string $updated_at
 */
class Order extends \yii\db\ActiveRecord
{
     /**
     * Order status new.
     *
     * @var string
     */
    const ORDER_STATUS_NEW = 'new';

    /**
     * Order status paid.
     *
     * @var string
     */
    const ORDER_STATUS_PAID = 'paid';

    /**
     * Order status cancel.
     *
     * @var string
     */
    const ORDER_STATUS_CANCEL = 'cancel';

    /**
     * Order status cancel.
     *
     * @var string
     */
    const ORDER_STATUS_DELIVERED = 'delivered';

    /**
     * Order status preautorization.
     *
     * @var string
     */
    const ORDER_STATUS_PREAUTORIZATION = 'preauthorization';

    /**
     * Payment paypal method.
     *
     * @var string
     */
    const PAYPAL_METHOD = 'paypal';

    /**
     * Payment credit card method.
     *
     * @var string
     */
    const CREDIT_METHOD = 'credit';

    /**
     * Payment klarna method.
     *
     * @var string
     */
    const KLARNA_METHOD = 'klarna';

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public static function tableName()
    {
        return '{{%product_orders}}';
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['order_number', 'type_payment', 'status', 'total_sum', 'full_sum'], 'required'],
            [['user_id', 'is_new'], 'integer'],
            [['total_sum', 'shipping_price', 'tax', 'full_sum'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['order_number', 'credit_txid'], 'string', 'max' => 255],
            [['type_payment', 'status'], 'string', 'max' => 20],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
     public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_number' => 'Order Number',
            'user_id' => 'User ID',
            'type_payment' => 'Type Payment',
            'status' => 'Status',
            'total_sum' => 'Total Sum',
            'shipping_price' => 'Shipping Price',
            'tax' => 'Tax',
            'full_sum' => 'Full Sum',
            'is_new' => 'Is New',
            'credit_txid' => 'Credit txid',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Get status for order.
     *
     * @return string
     */
    public function getStatus()
    {
        switch ($this->status) {
            case self::ORDER_STATUS_NEW:
                return 'New (wait paid)';
            case self::ORDER_STATUS_PAID:
                return 'Paid';
            case self::ORDER_STATUS_CANCEL:
                return 'Cancel';
            case self::ORDER_STATUS_DELIVERED:
                return 'Delivered';
            case self::ORDER_STATUS_PREAUTORIZATION:
                return 'Preautorization';
        }
    }

    /**
     * Get order date.
     *
     * @return string
     */
    public function getOrderDate()
    {
        $date = new \DateTime($this->created_at);

        return Language::setDateLocalMonth($date->format('d F Y, H:i'));
    }

    /**
     * Get human payment method.
     *
     * @return string
     */
    public function getHumanPaymentMethod()
    {
        switch ($this->type_payment) {
            case self::CREDIT_METHOD:
                return 'Kreditkarte';
            case self::PAYPAL_METHOD:
                return 'PayPal';
            case self::KLARNA_METHOD:
                return 'Sofortüberweisung';
        }
    }

    /**
     * Get tax for order.
     *
     * @return float
     */
    public function getTax()
    {
        return $this->total_sum / 100 * OrderShippingInformation::TAX_PERCENT;
    }

    public function getShippingPrice()
    {
        return $this->orderShippingInformation->getShippingPriceByMethod();
    }

    /**
     * Get full order price with shipping and tax.
     *
     * @return float
     */
    public function getFullTotalPrice()
    {
        $total = $this->total_sum;

        $tax = $total / 100 * OrderShippingInformation::TAX_PERCENT;

        $total += $this->orderShippingInformation->getShippingPriceByMethod();
        $total += $tax;

        return $total;

    }
}
