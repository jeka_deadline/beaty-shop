<?php

namespace common\models\product;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Html;

/**
 * This is the model class for table "product_variation_properties".
 *
 * @property int $id
 * @property int $product_variation_id
 * @property int $filter_id
 * @property string $value
 * @property string $created_at
 * @property string $updated_at
 *
 * @property ProductFilter $filter
 * @property ProductVariation $productVariation
 */
class ProductVariationPropertie extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_variation_properties';
    }

    public function behaviors()
    {
        return [
            [
                'class' =>TimestampBehavior::className(),
                'value' => new \yii\db\Expression('NOW()')
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_variation_id', 'filter_id', 'product_category_id'], 'required'],
            [['product_variation_id', 'filter_id', 'product_category_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['value'], 'string', 'max' => 255],
            [['filter_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductFilter::className(), 'targetAttribute' => ['filter_id' => 'id']],
            [['product_variation_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductVariation::className(), 'targetAttribute' => ['product_variation_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_variation_id' => 'Product Variation ID',
            'filter_id' => 'Filter ID',
            'product_category_id' => 'Product Category ID',
            'value' => 'Value',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilter()
    {
        return $this->hasOne(ProductFilter::className(), ['id' => 'filter_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductVariation()
    {
        return $this->hasOne(ProductVariation::className(), ['id' => 'product_variation_id']);
    }

    public function getLabel() {
        if (!$this->filter) return '';
        return $this->filter->name;
    }

    /**
     * Get human property value
     *
     * @return string
     */
    public function getHumanValue()
    {
        $filter = $this->filter;
        if ($this->filter->type === \common\models\product\ProductFilter::TYPE_COLORLIST) {
            return Html::tag('span', '&nbsp;&nbsp;&nbsp;&nbsp;', [
                'style' => 'background-color:' . $this->value,
            ]);
        }

        return $this->value;
    }
}
