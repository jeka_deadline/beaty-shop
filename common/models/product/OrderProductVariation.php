<?php

namespace common\models\product;

use Yii;

/**
 * This is the model class for table "{{%product_order_products}}".
 *
 * @property int $id
 * @property int $order_id
 * @property int $product_variation_id
 * @property string $product_variation_name
 * @property int $count
 * @property double $price
 * @property double $promotion
 * @property string $created_at
 * @property string $updated_at
 */
class OrderProductVariation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public static function tableName()
    {
        return '{{%product_order_products}}';
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['order_id', 'product_variation_id', 'product_variation_name', 'count', 'price'], 'required'],
            [['order_id', 'product_variation_id', 'count'], 'integer'],
            [['price', 'promotion'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['product_variation_name'], 'string', 'max' => 255],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['product_variation_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductVariation::className(), 'targetAttribute' => ['product_variation_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'product_variation_id' => 'Product Variation ID',
            'product_variation_name' => 'Product Variation Name',
            'count' => 'Count',
            'price' => 'Price',
            'promotion' => 'Promotion',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
