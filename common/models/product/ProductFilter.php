<?php

namespace common\models\product;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "product_filters".
 *
 * @property int $id
 * @property string $name
 * @property string $type
 * @property string $created_at
 * @property string $updated_at
 *
 * @property ProductCategorieRelationFilter[] $productCategorieRelationFilters
 * @property ProductVariationPropertie[] $productVariationProperties
 */
class ProductFilter extends \yii\db\ActiveRecord
{

    const TYPE_RANGE = 'range';
    const TYPE_SLIDER = 'slider';
    const TYPE_CHECKBOX = 'checkbox';
    const TYPE_DROPDOWN = 'dropdown';
    const TYPE_RADIOLIST = 'radiolist';
    const TYPE_COLORLIST = 'colorlist';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_filters';
    }

    public function behaviors()
    {
        return [
            [
                'class' =>TimestampBehavior::className(),
                'value' => new \yii\db\Expression('NOW()')
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'type' => 'Type',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductCategorieRelationFilters()
    {
        return $this->hasMany(ProductCategorieRelationFilter::className(), ['filter_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductVariationProperties()
    {
        return $this->hasMany(ProductVariationPropertie::className(), ['filter_id' => 'id']);
    }

    public static function typeLabels()
    {
        return [
//            self::TYPE_RANGE => 'Range',
//            self::TYPE_SLIDER => 'Slider',
//            self::TYPE_CHECKBOX => 'Check Box List',
//            self::TYPE_DROPDOWN => 'Drop Down List',
            self::TYPE_RADIOLIST => 'Radio List',
            self::TYPE_COLORLIST => 'Color List',
        ];
    }
}
