<?php

namespace common\models\product;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "product_images".
 *
 * @property int $id
 * @property int $product_variation_id
 * @property string $filename
 * @property int $sort
 * @property int $preview
 * @property string $created_at
 * @property string $updated_at
 *
 * @property ProductVariation $productVariation
 */
class ProductImage extends \yii\db\ActiveRecord
{

    public static $path = 'files/product/';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_images';
    }

    public function behaviors()
    {
        return [
            [
                'class' =>TimestampBehavior::className(),
                'value' => new \yii\db\Expression('NOW()')
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_variation_id', 'filename'], 'required'],
            [['product_variation_id', 'sort', 'preview'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['filename'], 'string', 'max' => 255],
            [['product_variation_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductVariation::className(), 'targetAttribute' => ['product_variation_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_variation_id' => 'Product Variation ID',
            'filename' => 'Filename',
            'sort' => 'Sort',
            'preview' => 'Preview',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductVariation()
    {
        return $this->hasOne(ProductVariation::className(), ['id' => 'product_variation_id']);
    }
}
