<?php

namespace common\models\product;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "product_products".
 *
 * @property int $id
 * @property string $slug
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property int $active
 * @property int $default_variation_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property ProductLangField[] $productProductLangField
 * @property ProductVariation[] $productVariation
 * @property ProductRelationCategorie[] $productRelationCategorie
 */
class Product extends \yii\db\ActiveRecord
{
    const TYPE_PRODUCT = 'product';
    const TYPE_PRODUCT_SET = 'set';
    const TYPE_PRODUCT_EDIT_SET = 'set-edit';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_products';
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new \yii\db\Expression('NOW()')
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['slug'], 'required'],
            [['meta_description'], 'string'],
            [['active', 'default_variation_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['slug', 'meta_title', 'meta_keywords'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 12],
            [['slug'], 'unique'],
            ['slug', 'match', 'pattern' => '#^[a-z][a-z\d-]*[a-z\d]$#'],
            [['default_variation_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductVariation::className(), 'targetAttribute' => ['default_variation_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'slug' => 'Slug',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'meta_keywords' => 'Meta Keywords',
            'active' => 'Active',
            'default_variation_id' => 'Default Variation ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Check if this product is an pack of another product.
     *
     * @return bool
     */
    public function isAnPack()
    {
        $count = Pack::find()
            ->where(['product_pack_id' => $this->id])
            ->count();

        return ($count > 0);
    }

    public function isProductSet() {
        return $this->type == self::TYPE_PRODUCT_SET || $this->type == self::TYPE_PRODUCT_EDIT_SET;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductProductLangFields()
    {
        return $this->hasMany(ProductLangField::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductVariations()
    {
        return $this->hasMany(ProductVariation::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDefaultProductVariation()
    {
        return $this->hasOne(ProductVariation::className(), ['id' => 'default_variation_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductRelationCategories()
    {
        return $this->hasMany(ProductRelationCategorie::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductCategories()
    {
        return $this->hasMany(ProductCategory::className(), ['id' => 'product_category_id'])
            ->viaTable(ProductRelationCategorie::tableName(), ['product_id' => 'id']);
    }

    /**
     * Update master product rating.
     *
     * @param int $product_id Product id
     * @return float Average product rating
     */
    public static function updateRating($product_id)
    {
        $average = ProductRating::find()
            ->where([
                'product_id' => $product_id,
                'active' => 1
            ])
            ->average('value');

        self::updateAll(['rating' => $average], ['id' => $product_id]);

        return $average;
    }
}
