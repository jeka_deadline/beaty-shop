<?php

namespace common\models\product;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "product_prices".
 *
 * @property int $id
 * @property int $product_variation_id
 * @property double $price
 * @property string $created_at
 * @property string $updated_at
 *
 * @property ProductVariation $productVariation
 */
class ProductPrice extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_prices';
    }

    public function behaviors()
    {
        return [
            [
                'class' =>TimestampBehavior::className(),
                'value' => new \yii\db\Expression('NOW()')
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_variation_id'], 'required'],
            [['product_variation_id'], 'integer'],
            [['price'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['product_variation_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductVariation::className(), 'targetAttribute' => ['product_variation_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_variation_id' => 'Product Variation ID',
            'price' => 'Price',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductVariation()
    {
        return $this->hasOne(ProductVariation::className(), ['id' => 'product_variation_id']);
    }
}
