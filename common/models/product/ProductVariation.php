<?php

namespace common\models\product;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "product_variations".
 *
 * @property int $id
 * @property int $product_id
 * @property string $name
 * @property string $vendor_code
 * @property string $description
 * @property string $short_description
 * @property int $active
 * @property string $created_at
 * @property string $updated_at
 * @property ProductImage[] $productImages
 * @property ProductInventorie[] $productInventories
 * @property ProductPrice[] $productPrices
 * @property ProductPromotion[] $productPromotions
 * @property ProductVariationLangField[] $productVariationLangFields
 * @property Product $product
 * @property Product $defaultProduct
 */
class ProductVariation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_variations}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' =>TimestampBehavior::className(),
                'value' => new \yii\db\Expression('NOW()')
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'name', 'vendor_code'], 'required'],
            [['product_id', 'active'], 'integer'],
            [['description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['vendor_code'], 'string', 'max' => 100],
            [['short_description'], 'string', 'max' => 1000],
            [['vendor_code'], 'unique'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'name' => 'Name',
            'vendor_code' => 'Vendor Code',
            'description' => 'Description',
            'short_description' => 'Short Description',
            'active' => 'Active',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImages()
    {
        return $this->hasMany(ProductImage::className(), ['product_variation_id' => 'id'])->orderBy(['sort' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPreview()
    {
        return $this->hasOne(ProductImage::className(), ['product_variation_id' => 'id'])->orderBy(['sort' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventorie()
    {
        return $this->hasOne(ProductInventorie::className(), ['product_variation_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrice()
    {
        return $this->hasOne(ProductPrice::className(), ['product_variation_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromotion()
    {
        return $this->hasOne(ProductPromotion::className(), ['product_variation_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVariationLangFields()
    {
        return $this->hasMany(ProductVariationLangField::className(), ['product_variation_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecommendets()
    {
        return $this->hasMany(ProductVariation::className(), ['id' => 'product_variation_recommended_id'])
            ->viaTable(ProductRecommendet::tableName(), ['product_variation_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDefaultProduct()
    {
        return $this->hasOne(Product::className(), ['default_variation_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductSetVariation()
    {
        return $this->hasOne(ProductSetVariation::className(), ['product_variation_id' => 'id']);
    }
}
