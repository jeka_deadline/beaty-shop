<?php

namespace common\models\product;

use Yii;

/**
 * This is the model class for table "{{%product_packs}}".
 *
 * @property int $id
 * @property int $product_id
 * @property int $product_pack_id
 * @property string $created_at
 * @property string $updated_at
 */
class Pack extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%product_packs}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'product_pack_id'], 'required'],
            [['product_id', 'product_pack_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['product_pack_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_pack_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'product_pack_id' => 'Product Pack ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
