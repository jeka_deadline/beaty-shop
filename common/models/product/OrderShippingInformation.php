<?php

namespace common\models\product;

use Yii;
use common\models\geo\City;
use common\models\geo\Country;

/**
 * This is the model class for table "{{%product_order_shipping_information}}".
 *
 * @property int $id
 * @property int $order_id
 * @property string $shipping_method
 * @property string $shipping_tracking_number
 * @property string $title
 * @property string $surname
 * @property string $name
 * @property string $company
 * @property int $country_id
 * @property string $city
 * @property string $address1
 * @property string $address2
 * @property string $zip
 * @property string $phone
 * @property string $email
 * @property string $created_at
 * @property string $updated_at
 */
class OrderShippingInformation extends \yii\db\ActiveRecord
{
    /**
     * Standart shipping method.
     *
     * @var string
     */
    const SHIPPING_STANDART_METHOD = 'standart';

    /**
     * Express shipping method.
     *
     * @var string
     */
    const SHIPPING_EXPRESS_METHOD = 'express';

    /**
     * Pickup shipping method
     *
     * @var string
     */
    const SHIPPING_PICKUP_METHOD = 'pickup';

    /**
     * Percen for tax.
     *
     * @var int
     */
    const TAX_PERCENT = 19;

    /**
     * Price for express shipping.
     *
     * @var float
     */
    const SHIPPING_PRICE_EXPRESS_METHOD = 11.9;

    /**
     * Price for standart shipping.
     *
     * @var float
     */
    const SHIPPING_PRICE_STANDART_METHOD = 4.99;

    /**
     * Price for pickup shipping.
     *
     * @var float
     */
    const SHIPPING_PRICE_PICKUP_METHOD = 0;

    /**
     * Price for standart shipping countries european alias.
     *
     * @var float
     */
    const SHIPPING_PRICE_EU_COUNTRIES_STANDART_METHOD = 13.99;

    /**
     * Price for standart shipping switzerland country.
     *
     * @var float
     */
    const SHIPPING_PRICE_SWITZERLAND_STANDART_METHOD = 26.90;

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public static function tableName()
    {
        return '{{%product_order_shipping_information}}';
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['order_id', 'shipping_method', 'surname', 'name', 'country_id', 'city', 'address1', 'zip', 'phone', 'email', 'title'], 'required'],
            [['order_id', 'country_id'], 'integer'],
            [['address1', 'address2'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['shipping_method', 'shipping_tracking_number'], 'string', 'max' => 20],
            [['surname', 'name'], 'string', 'max' => 50],
            [['zip'], 'string', 'max' => 5],
            [['title'], 'string', 'max' => 10],
            [['phone'], 'string', 'max' => 16],
            [['email', 'city', 'company'], 'string', 'max' => 255],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'shipping_method' => 'Shipping Method',
            'shipping_tracking_number' => 'Shipping Tracking Number',
            'title' => 'Title',
            'surname' => 'Surname',
            'name' => 'Name',
            'company' => 'Company',
            'country_id' => 'Country ID',
            'city' => 'City',
            'address1' => 'Address1',
            'address2' => 'Address2',
            'zip' => 'Zip',
            'phone' => 'Phone',
            'email' => 'Email',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Get shipping price by shipping method.
     *
     * @return float
     */
    public function getShippingPriceByMethod()
    {
        switch ($this->shipping_method) {
            case self::SHIPPING_STANDART_METHOD:
                return self::SHIPPING_PRICE_STANDART_METHOD;
            case self::SHIPPING_EXPRESS_METHOD:
                return self::SHIPPING_PRICE_EXPRESS_METHOD;
            case self::SHIPPING_PICKUP_METHOD:
                return self::SHIPPING_PRICE_PICKUP_METHOD;
            default:
                return self::SHIPPING_PRICE_STANDART_METHOD;
        }
    }
}
