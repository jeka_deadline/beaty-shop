<?php

namespace frontend\modules\product\helpers;

use Omnipay\Payone\Extend\Item;
use Omnipay\Payone\Extend\ItemInterface;
use Omnipay\Common\CreditCard;
use Omnipay\Omnipay;
use yii\helpers\Url;
use frontend\modules\product\components\Payone as BasePayone;

class Payone
{
    public static function sendPreautorization($order, $cart, $shippingInfo, $pseudocardpan)
    {
        $personalData = self::getUserPersonalData($shippingInfo);
        $creditCard = self::getCreditCard($order, $cart, $pseudocardpan);
        $items = self::getItems($cart);

        $request = array_merge(self::getDefaults(), $personalData, $creditCard, $items);

        return BasePayone::sendRequest($request);
    }

    public static function sendCapture($order, $txid)
    {
        $parameters = [
            "request" => "capture",
            "txid" => $txid,
            "amount" => str_replace('.', '', $order->full_sum),
            'currency' => 'EUR',
            "capturemode" => "completed",
            "sequencenumber" => "1"
        ];

        $request = array_merge(self::getDefaults(), $parameters);

        return BasePayone::sendRequest($request);
    }

    private static function getUserPersonalData($shippingInfo)
    {
        return [
            "salutation" => ucfirst($shippingInfo->title),
            "firstname" => $shippingInfo->name,
            "lastname" => $shippingInfo->surname,
            "street" => $shippingInfo->address1,
            "zip" => $shippingInfo->zip,
            "city" => $shippingInfo->city,
            "country" => ($shippingInfo->country) ? strtoupper($shippingInfo->country->code) : 'EN',
            "email" => $shippingInfo->email,
            "language" => "en"
        ];
    }

    private static function getCreditCard($order, $cart, $pseudocardpan)
    {
        return [
            "clearingtype" => "cc", // cc for credit card
            "reference" => $order->id,
            "amount" => str_replace('.', '', $order->full_sum),
            "currency" => "EUR",
            "request" => "preauthorization",
            "successurl" => Url::toRoute(['/product/checkout/success-payone', 'reference' => $order->id], true),
            "errorurl" => Url::toRoute(['/product/checkout/error-payone', 'reference' => $order->id], true),
            "backurl" => Url::toRoute(['/product/checkout/back-payone', 'reference' => $order->id], true),
            "pseudocardpan" => $pseudocardpan // pseudo card pan received from previous checkout steps, no other card details required
        ];
    }

    private static function getDefaults()
    {
        return [
            "aid" => "41289",
            "mid" => "40937",
            "portalid" => "2029517",
            "key" => hash("md5", "9Z0IqKu17cs6XLR7"), // the key has to be hashed as md5
            "api_version" => "3.8",
            "mode" => "test", // can be "live" for actual transactions
            "encoding" => "UTF-8"
        ];
    }

    private static function getItems($cart)
    {
        $items = [];

        $index = 1;

        foreach ($cart as $itemCart) {
            $items[ 'de[' . $index . ']' ] = $itemCart->productVariationName;
            $items[ 'it[' . $index . ']' ] = 'goods';
            $items[ 'id[' . $index . ']' ] = $itemCart->productVariationId;
            $items[ 'pr[' . $index . ']' ] = $itemCart->price;
            $items[ 'no[' . $index . ']' ] = $itemCart->count;

            $index++;
        }

        return $items;
    }
}