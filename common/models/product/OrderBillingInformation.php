<?php

namespace common\models\product;

use Yii;
use common\models\geo\Country;

/**
 * This is the model class for table "{{%product_order_billing_information}}".
 *
 * @property int $id
 * @property int $order_id
 * @property string $title
 * @property string $surname
 * @property string $name
 * @property string $company
 * @property int $country_id
 * @property string $city_id
 * @property string $address1
 * @property string $address2
 * @property string $zip
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Country $country
 * @property Order $order
 */
class OrderBillingInformation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public static function tableName()
    {
        return '{{%product_order_billing_information}}';
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['order_id', 'surname', 'name', 'country_id', 'city', 'address1', 'zip', 'title'], 'required'],
            [['order_id', 'country_id'], 'integer'],
            [['address1', 'address2'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['surname', 'name'], 'string', 'max' => 50],
            [['zip'], 'string', 'max' => 5],
            [['title'], 'string', 'max' => 10],
            [['city', 'company'], 'string', 'max' => 255],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'title' => 'Title',
            'surname' => 'Surname',
            'name' => 'Name',
            'company' => 'Company',
            'country_id' => 'Country ID',
            'city' => 'City',
            'address1' => 'Address1',
            'address2' => 'Address2',
            'zip' => 'Zip',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
