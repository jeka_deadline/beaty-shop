<?php

namespace common\models\product;

use Yii;

/**
 * This is the model class for table "{{%product_coupons}}".
 *
 * @property int $id
 * @property string $code
 * @property string $type
 * @property double $value
 * @property int $active
 * @property string $created_at
 * @property string $updated_at
 * @property string $finish_date
 */
class Coupon extends \yii\db\ActiveRecord
{
    /**
     * Coupon fix value type.
     *
     * @var string
     */
    const FIX_TYPE = 'fix';

    /**
     * Coupon percent value type.
     *
     * @var string
     */
    const PERCENT_TYPE = 'percent';

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public static function tableName()
    {
        return '{{%product_coupons}}';
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['code', 'value'], 'required'],
            [['type'], 'string'],
            [['value'], 'number'],
            [['active'], 'integer'],
            [['created_at', 'updated_at', 'finish_date'], 'safe'],
            [['code'], 'string', 'max' => 255],
            [['code'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'type' => 'Type',
            'value' => 'Value',
            'active' => 'Active',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Generate GUID for code coupon.
     *
    * @return string
    */
    public static function generateGUID()
    {
        mt_srand((double)microtime() * 10000);

        $charid = strtoupper(md5(uniqid(rand(), true)));
        $hyphen = '-';

        $uuid   = substr($charid, 0, 4) . $hyphen .
            substr($charid, 4, 4) . $hyphen .
            substr($charid, 8, 4) . $hyphen .
            substr($charid, 12, 4) . $hyphen .
            substr($charid, 16, 4);

        return $uuid;
    }
}
