<?php

namespace common\models\product;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%product_categories}}".
 *
 * @property int $id
 * @property int $parent_id
 * @property string $name
 * @property string $slug
 * @property string $description
 * @property int $display_order
 * @property int $display_order_hmenu
 * @property int $active
 * @property int $show_search
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $created_at
 * @property string $updated_at
 *
 * @property ProductCategoryLangField[] $langFields
 */
class ProductCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public static function tableName()
    {
        return '{{%product_categories}}';
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' =>TimestampBehavior::className(),
                'value' => new \yii\db\Expression('NOW()')
            ]
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['parent_id', 'display_order', 'display_order_hmenu', 'active', 'show_search'], 'integer'],
            [['name', 'slug'], 'required'],
            [['description', 'meta_description'], 'string'],
            [['name', 'slug', 'meta_title', 'meta_keywords'], 'string', 'max' => 255],
            [['slug'], 'unique'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * Create categories tree structure.
     *
     * @param \backend\modules\product\models\ProductCategory[] $categories
     * @return array
     */
    public static function createTree($categories)
    {
        $nested = [];

        foreach ( $categories as &$category ) {
            if (!$category[ 'parent_id' ]) {
                $nested[ $category[ 'id' ] ] = &$category;
            } else {
                $pid = $category[ 'parent_id' ];
                if (isset($categories[ $pid ])) {
                    if (!isset($categories[ $pid ][ 'children' ])) {
                        $categories[ $pid ][ 'children' ] = [];
                    }

                    $categories[ $pid ][ 'children' ][ $category[ 'id' ] ] = &$category;
                }
            }
        }

        return $nested;
    }

    public static function findNodeTree($nested, $nodeId) {
        if (!is_array($nested)) return false;
        foreach ($nested as $id => $node) {
            if ($id == $nodeId) return true;
            if (isset($node['children']) && self::findNodeTree($node['children'],$nodeId)) return true;
        }
        return false;
    }


    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent ID',
            'name' => 'Name',
            'slug' => 'Slug',
            'description' => 'Description',
            'display_order' => 'Display Order',
            'display_order_hmenu' => 'Display Order Header Menu',
            'active' => 'Active',
            'show_search' => 'Show Search',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'meta_keywords' => 'Meta Keywords',
            'created_at' => 'Created at',
            'updated_at' => 'Updated at',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(static::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildren()
    {
        return $this->hasMany(static::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLangFields()
    {
        return $this->hasMany(ProductCategoryLangField::className(), ['product_category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductFilters()
    {
        return $this->hasMany(ProductFilter::className(), ['id' => 'filter_id'])
            ->viaTable(ProductCategorieRelationFilter::tableName(), ['product_category_id' => 'id']);
    }
}
