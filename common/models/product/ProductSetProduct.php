<?php

namespace common\models\product;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "product_set_products".
 *
 * @property int $id
 * @property int $product_set_id
 * @property int $product_id
 * @property int $count
 * @property int $display_order
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Product $product
 * @property Product $productSet
 */
class ProductSetProduct extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_set_products';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_set_id', 'product_id'], 'required'],
            [['product_set_id', 'product_id', 'count', 'display_order'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['product_set_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_set_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' =>TimestampBehavior::className(),
                'value' => new \yii\db\Expression('NOW()')
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_set_id' => 'Product Set ID',
            'product_id' => 'Product ID',
            'count' => 'Count',
            'display_order' => 'Display Order',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductSet()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_set_id']);
    }
}
