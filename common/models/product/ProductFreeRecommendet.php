<?php

namespace common\models\product;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "product_free_recommendets".
 *
 * @property int $id
 * @property int $product_recommended_id
 * @property int $display_order
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Product $productRecommended
 */
class ProductFreeRecommendet extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_free_recommendets';
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' =>TimestampBehavior::className(),
                'value' => new Expression('NOW()')
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_recommended_id'], 'required'],
            [['product_recommended_id', 'display_order'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['product_recommended_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_recommended_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_recommended_id' => 'Product Recommended',
            'display_order' => 'Display Order',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_recommended_id']);
    }
}
