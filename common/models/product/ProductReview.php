<?php

namespace common\models\product;

use Yii;
use common\models\user\User;
use yii\behaviors\TimestampBehavior;
use \yii\db\Expression;

/**
 * This is the model class for table "{{%product_reviews}}".
 *
 * @property int $id
 * @property int $user_id
 * @property int $product_id
 * @property int $parent_id
 * @property string $username
 * @property string $surname
 * @property int $rating_id
 * @property string $subject
 * @property string $text
 * @property string admin_answer
 * @property int $active
 * @property string $created_at
 * @property string $updated_at
 *
 * @property ProductVariation $productVariation
 * @property User $user
 */
class ProductReview extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public static function tableName()
    {
        return '{{%product_reviews}}';
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['user_id', 'product_id'], 'required'],
            [['user_id', 'product_id', 'parent_id', 'rating_id', 'active'], 'integer'],
            [['subject', 'username', 'surname'], 'string', 'max' => 255],
            [['text', 'admin_answer'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['rating_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductRating::className(), 'targetAttribute' => ['rating_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' =>TimestampBehavior::className(),
                'value' => new Expression('NOW()')
            ]
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'product_id' => 'Product ID',
            'parent_id' => 'Parent ID',
            'surname' => 'Surname',
            'username' => 'Username',
            'subject' => 'Subject',
            'text' => 'Text',
            'admin_answer' => 'Admin answer',
            'active' => 'Active',
            'rating_id' => 'Rating ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildren()
    {
        return $this->hasMany(static::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(static::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRating()
    {
        return $this->hasOne(ProductRating::className(), ['id' => 'rating_id']);
    }

}
