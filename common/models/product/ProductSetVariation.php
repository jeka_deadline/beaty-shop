<?php

namespace common\models\product;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "product_set_variations".
 *
 * @property int $id
 * @property int $product_set_id
 * @property int $product_variation_id
 * @property int $display_order
 * @property int $count
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Product $productSet
 * @property ProductVariation $productVariation
 */
class ProductSetVariation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' =>TimestampBehavior::className(),
                'value' => new \yii\db\Expression('NOW()')
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_set_variations';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_set_id', 'product_variation_id'], 'required'],
            [['product_set_id', 'product_variation_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['display_order', 'count'], 'integer'],
            [['product_set_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_set_id' => 'id']],
            [['product_variation_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductVariation::className(), 'targetAttribute' => ['product_variation_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_set_id' => 'Product Set ID',
            'product_variation_id' => 'Product Variation ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductSet()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_set_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductVariation()
    {
        return $this->hasOne(ProductVariation::className(), ['id' => 'product_variation_id']);
    }
}
