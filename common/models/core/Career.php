<?php

namespace common\models\core;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "core_careers".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $display_order
 * @property int $active
 * @property string $created_at
 * @property string $updated_at
 *
 * @property CareerLangField[] $coreCareersLangFields
 */
class Career extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'core_careers';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new \yii\db\Expression('NOW()')
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description'], 'string'],
            [['display_order', 'active'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'display_order' => 'Display Order',
            'active' => 'Active',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoreCareersLangFields()
    {
        return $this->hasMany(CareerLangField::className(), ['career_id' => 'id']);
    }
}
