<?php

namespace common\models\core;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "core_slogans".
 *
 * @property int $id
 * @property string $slogan
 * @property int $display_order
 * @property int $active
 * @property string $created_at
 * @property string $updated_at
 *
 * @property SloganLangField[] $coreSlogansLangFields
 */
class Slogan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'core_slogans';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new \yii\db\Expression('NOW()')
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['slogan'], 'required'],
            [['display_order', 'active'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['slogan'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'slogan' => 'Slogan',
            'display_order' => 'Display Order',
            'active' => 'Active',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoreSlogansLangFields()
    {
        return $this->hasMany(SloganLangField::className(), ['slogan_id' => 'id']);
    }
}
