<?php

namespace common\models\core;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "core_slogans_lang_fields".
 *
 * @property int $id
 * @property int $slogan_id
 * @property int $lang_id
 * @property string $slogan
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Language $lang
 * @property Slogan $slogan0
 */
class SloganLangField extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'core_slogans_lang_fields';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new \yii\db\Expression('NOW()')
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['slogan_id', 'lang_id', 'slogan'], 'required'],
            [['slogan_id', 'lang_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['slogan'], 'string', 'max' => 255],
            [['lang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['lang_id' => 'id']],
            [['slogan_id'], 'exist', 'skipOnError' => true, 'targetClass' => Slogan::className(), 'targetAttribute' => ['slogan_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'slogan_id' => 'Slogan ID',
            'lang_id' => 'Lang ID',
            'slogan' => 'Slogan',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(Language::className(), ['id' => 'lang_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSlogan0()
    {
        return $this->hasOne(Slogan::className(), ['id' => 'slogan_id']);
    }
}
