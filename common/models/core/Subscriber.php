<?php

namespace common\models\core;

use Yii;
use common\models\user\User;

/**
 * This is the model class for table "{{%core_subscribers}}".
 *
 * @property int $id
 * @property string $email
 * @property int $is_new
 * @property string $created_at
 * @property int $user_id
 */
class Subscriber extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public static function tableName()
    {
        return '{{%core_subscribers}}';
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['is_new', 'user_id'], 'integer'],
            [['created_at'], 'safe'],
            [['email'], 'string', 'max' => 100],
            [['email'], 'unique'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'is_new' => 'Is New',
            'created_at' => 'Created At',
            'user_id' => 'User ID',
        ];
    }
}
