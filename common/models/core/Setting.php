<?php

namespace common\models\core;

use Yii;

/**
 * This is the model class for table "core_settings".
 *
 * @property int $id
 * @property string $key
 * @property string $name
 * @property string $value
 * @property string $type
 */
class Setting extends \yii\db\ActiveRecord
{
    private static $list = [];

    const TYPE_TEXT = 'text';
    const TYPE_IMAGE = 'image';
    const TYPE_CHECKBOX = 'checkbox';

    public static $path = 'files/settings/';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'core_settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['key'], 'required'],
            [['value', 'type'], 'string'],
            [['key', 'name'], 'string', 'max' => 255],
            [['key'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key' => 'Key',
            'name' => 'Name',
            'value' => 'Value',
            'type' => 'Type',
        ];
    }

    /**
     * @param string $index
     * @param null|var $default
     */
    public static function value($index) {
        if (isset(self::$list[$index])) {
            return self::$list[$index];
        } else {
            $modelSetting = Setting::findOne(['key' => $index]);
            if (!$modelSetting) return null;
            switch ($modelSetting->type) {
                case self::TYPE_IMAGE:
                    return self::$list[$index] = '/'.self::$path.$modelSetting->value;
                default:
                    return self::$list[$index] = $modelSetting->value;
            }
        }
        return null;
    }

    public static function typeLabels()
    {
        return [
            self::TYPE_TEXT => 'Text',
            self::TYPE_IMAGE => 'Image',
            self::TYPE_CHECKBOX => 'Checkbox',
        ];
    }
}
