<?php

namespace common\models\core;

use Yii;

/**
 * This is the model class for table "{{%core_email_templates}}".
 *
 * @property int $id
 * @property string $code
 * @property string $type
 * @property string $subject
 * @property string $from
 * @property string $text
 */
class EmailTemplate extends \yii\db\ActiveRecord
{
    const CONFIRM_EMAIL_TEMPLATE_CODE = 'confirm-email';

    const SUPPORT_MESSAGE_TEMPLATE_CODE = 'support-message';

    const RESET_PASSWORD_TEMPLATE_CODE = 'user-reset-password';

    const SUBSCRIBE_TEMPLATE_CODE = 'subscribe';

    const USER_REGISTER_TEMPLATE_CODE = 'user-register';

    const USER_ORDER_TEMPLATE_CODE = 'user-order';

    const NEW_USER_COUPON_CODE = 'new-user-coupon';

    const IMG_PATH = '/files/core/email';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%core_email_templates}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'subject', 'from', 'text'], 'required'],
            [['text'], 'string'],
            [['code', 'from'], 'string', 'max' => 100],
            [['type'], 'string', 'max' => 20],
            [['subject'], 'string', 'max' => 255],
            [['code'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'type' => 'Type',
            'subject' => 'Subject',
            'from' => 'From',
            'text' => 'Text',
        ];
    }
}
