<?php

namespace common\models\core;

use Yii;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%core_support_messages}}".
 *
 * @property int $id
 * @property string $title
 * @property string $surname
 * @property string $name
 * @property string $company
 * @property string $email
 * @property string $subject
 * @property string $text
 * @property int $is_new
 * @property string $created_at
 * @property string $updated_at
 */
class SupportMessage extends \yii\db\ActiveRecord
{
    /**
     * Saved path for support messages.
     *
     * @var string $filePath
     */
    private static $filePath = 'files/support';

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public static function tableName()
    {
        return '{{%core_support_messages}}';
    }

    /**
     * Get path saved files.
     *
     * @return string
     */
    public static function getFilePath()
    {
        return self::$filePath;
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * Get list support files for support message.
     *
     * @return array
     */
    public function getSupportFiles()
    {
        $frontendPath = self::getFilePath() . '/' . $this->id . '/';
        $pathFiles = Yii::getAlias("@frontend/web/{$frontendPath}");

        $files = [];
        $rii = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($pathFiles));

        foreach ($rii as $file) {
            if ($file->isDir()){
                continue;
            }

            $files[] = $file->getPathname();
        }

        return $files;
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function afterDelete()
    {
        $this->getSupportFiles();

        foreach ($this->getSupportFiles() as $file) {
            if (file_exists($file) && is_file($file) && is_writable($file)) {
                unlink($file);
            }
        }
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */    public function rules()
    {
        return [
            [['title', 'surname', 'name', 'email', 'subject', 'text'], 'required'],
            [['text'], 'string'],
            [['is_new'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['title'], 'string', 'max' => 10],
            [['surname', 'name'], 'string', 'max' => 50],
            [['email', 'subject', 'company'], 'string', 'max' => 100],
        ];
    }

    /**
     * Get list subjects for support messages.
     *
     * @return array
     */
    public static function getListSubjects()
    {
        return [
            'faq' => 'FAQ',
            'security' => Yii::t('core', 'Security'),
            'legal' => Yii::t('core', 'Legal'),
            'pay' => Yii::t('core', 'Pay'),
            'shipping' => Yii::t('core', 'Shipping'),
            'defective-delivery' => Yii::t('core', 'Defective delivery'),
            'defect' => Yii::t('core', 'Defect'),
            'others' => Yii::t('core', 'Others'),
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'surname' => 'Surname',
            'name' => 'Name',
            'email' => 'Email',
            'company' => 'Company',
            'subject' => 'Subject',
            'text' => 'Text',
            'is_new' => 'Is New',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
