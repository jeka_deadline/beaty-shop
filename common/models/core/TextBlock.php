<?php

namespace common\models\core;

use Yii;

/**
 * This is the model class for table "{{%core_text_blocks}}".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $content
 * @property string $code
 * @property int $active
 * @property string $created_at
 * @property string $updated_at
 */
class TextBlock extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%core_text_blocks}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'code'], 'required'],
            [['active'], 'integer'],
            [['description', 'content'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['code'], 'string', 'max' => 255],
            ['code', 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'content' => 'Content',
            'code' => 'Code',
            'active' => 'Active',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
