<?php

namespace common\models\core;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "core_trainer_distributors".
 *
 * @property int $id
 * @property string $sex
 * @property string $first_name
 * @property string $last_name
 * @property string $phone
 * @property string $email
 * @property string $country
 * @property string $state
 * @property string $city
 * @property string $address
 * @property string $type
 * @property string $created_at
 * @property string $updated_at
 * @property string $tax_number_1
 * @property string $tax_number_2
 * @property string $register_code
 */
class TrainerDistributor extends \yii\db\ActiveRecord
{
    const TYPE_TRAINER = 'trainer';
    const TYPE_DISTRUBUTOR = 'distrubutor';

    public static $path = 'files/trainer-distributor/';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'core_trainer_distributors';
    }

    public function behaviors()
    {
        return [
            [
                'class' =>TimestampBehavior::className(),
                'value' => new \yii\db\Expression('NOW()')
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'phone', 'email'], 'required'],
            [['address'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['sex', 'phone'], 'string', 'max' => 20],
            [['first_name', 'last_name'], 'string', 'max' => 50],
            [['company', 'email', 'country', 'state', 'city', 'tax_number_1', 'tax_number_2'], 'string', 'max' => 100],
            [['is_new'], 'integer'],
            [['register_code'], 'string', 'max' => 32],
            [['type'], 'string', 'max' => 15],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sex' => 'Sex',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'company' => 'Company',
            'phone' => 'Phone',
            'email' => 'Email',
            'country' => 'Country',
            'state' => 'State',
            'city' => 'City',
            'address' => 'Address',
            'type' => 'Type',
            'is_new' => 'Is New',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'tax_number_1' => 'Tax number 1',
            'tax_number_2' => 'Tax number 2',
            'register_code' => 'Register code',
        ];
    }

    public function getPath() {
        return Yii::getAlias('@frontend/web') . '/' . self::$path .'/' . $this->id . '/';
    }

    public function getUrlDir() {
        return '/'.self::$path.$this->id.'/';
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }

        $allPath = $this->getPath();
        if (is_dir($allPath)) {
            $files = scandir($allPath);
            foreach ($files as $file) {
                if (!($file=='.' || $file=='..')) {
                    unlink($allPath . $file);
                }
            }
            rmdir($allPath);
        }

        return true;
    }
}
