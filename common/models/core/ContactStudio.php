<?php

namespace common\models\core;

use Yii;

/**
 * This is the model class for table "core_contact_studios".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $address
 * @property double $lat
 * @property double $lng
 * @property string $type
 * @property int $display_order
 * @property int $active
 * @property string $created_at
 * @property string $updated_at
 *
 * @property ContactStudioLangField[] $contactStudioLangField
 */
class ContactStudio extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'core_contact_studios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'address', 'lat', 'lng'], 'required'],
            [['description'], 'string'],
            [['lat', 'lng'], 'number'],
            [['display_order', 'active'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 60],
            [['address'], 'string', 'max' => 80],
            [['type'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'address' => 'Address',
            'lat' => 'Lat',
            'lng' => 'Lng',
            'type' => 'Type',
            'display_order' => 'Display Order',
            'active' => 'Active',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContactStudioLangField()
    {
        return $this->hasMany(ContactStudioLangField::className(), ['contact_studio_id' => 'id']);
    }
}
