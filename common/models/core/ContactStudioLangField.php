<?php

namespace common\models\core;

use Yii;

/**
 * This is the model class for table "core_contact_studios_lang_fields".
 *
 * @property int $id
 * @property int $contact_studio_id
 * @property int $lang_id
 * @property string $name
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 *
 * @property ContactStudio $contactStudio
 * @property Language $lang
 */
class ContactStudioLangField extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'core_contact_studios_lang_fields';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['contact_studio_id', 'lang_id', 'name'], 'required'],
            [['contact_studio_id', 'lang_id'], 'integer'],
            [['description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 60],
            [['contact_studio_id'], 'exist', 'skipOnError' => true, 'targetClass' => ContactStudio::className(), 'targetAttribute' => ['contact_studio_id' => 'id']],
            [['lang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['lang_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'contact_studio_id' => 'Contact Studio ID',
            'lang_id' => 'Lang ID',
            'name' => 'Name',
            'description' => 'Description',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContactStudio()
    {
        return $this->hasOne(ContactStudio::className(), ['id' => 'contact_studio_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(Language::className(), ['id' => 'lang_id']);
    }
}
