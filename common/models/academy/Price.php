<?php

namespace common\models\academy;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "academy_prices".
 *
 * @property int $id
 * @property int $academy_id
 * @property string $name
 * @property string $description
 * @property double $price
 * @property int $best_offer
 * @property int $active
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Academy $academy
 * @property PriceLangField[] $academyPricesLangFields
 */
class Price extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'academy_prices';
    }

    public function behaviors()
    {
        return [
            [
                'class' =>TimestampBehavior::className(),
                'value' => new \yii\db\Expression('NOW()')
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['academy_id'], 'required'],
            [['academy_id', 'best_offer', 'active'], 'integer'],
            [['description'], 'string'],
            [['price'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['academy_id'], 'exist', 'skipOnError' => true, 'targetClass' => Academy::className(), 'targetAttribute' => ['academy_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'academy_id' => 'Academy',
            'name' => 'Name',
            'description' => 'Description',
            'price' => 'Price',
            'best_offer' => 'Best Offer',
            'active' => 'Active',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcademy()
    {
        return $this->hasOne(Academy::className(), ['id' => 'academy_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcademyPricesLangFields()
    {
        return $this->hasMany(PriceLangField::className(), ['academy_price_id' => 'id']);
    }
}
