<?php

namespace common\models\academy;

use common\models\academy\infoBlockModels\Attantion;
use common\models\academy\infoBlockModels\Description;
use common\models\academy\infoBlockModels\FinalInformation;
use common\models\academy\infoBlockModels\ImageSlider;
use common\models\academy\infoBlockModels\Schedule;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "academy_info_blocks".
 *
 * @property int $id
 * @property int $academy_id
 * @property string $name
 * @property string $type
 * @property string $data
 * @property int $display_order
 * @property int $active
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Academy $academy
 * @property InfoBlockLangField[] $academyInfoBlocksLangFields
 */
class InfoBlock extends \yii\db\ActiveRecord
{

    const TYPE_BLOCK_DESCRIPTION = 'description';
    const TYPE_BLOCK_ATTANTION = 'attantion';
    const TYPE_BLOCK_SCHEDULE = 'schedule';
    const TYPE_BLOCK_IMAGE_SLIDER = 'imageslider';
    const TYPE_BLOCK_FINAL_INFORMATION = 'finalinformation';

    public $blocks = null;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'academy_info_blocks';
    }

    public function behaviors()
    {
        return [
            [
                'class' =>TimestampBehavior::className(),
                'value' => new \yii\db\Expression('NOW()')
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['academy_id', 'name'], 'required'],
            [['academy_id', 'display_order', 'active'], 'integer'],
            [['data'], 'string'],
            [['created_at', 'updated_at', 'blocks'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 100],
            [['academy_id'], 'exist', 'skipOnError' => true, 'targetClass' => Academy::className(), 'targetAttribute' => ['academy_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'academy_id' => 'Academy ID',
            'name' => 'Name',
            'type' => 'Type',
            'data' => 'Data',
            'display_order' => 'Display Order',
            'active' => 'Active',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcademy()
    {
        return $this->hasOne(Academy::className(), ['id' => 'academy_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcademyInfoBlocksLangFields()
    {
        return $this->hasMany(InfoBlockLangField::className(), ['info_block_id' => 'id']);
    }

    public static function typeLabels()
    {
        return [
            self::TYPE_BLOCK_DESCRIPTION => 'Description',
            self::TYPE_BLOCK_ATTANTION => 'Attantion',
            self::TYPE_BLOCK_SCHEDULE => 'Schedule',
            self::TYPE_BLOCK_IMAGE_SLIDER => 'Image Slider',
            self::TYPE_BLOCK_FINAL_INFORMATION => 'Final Information',
        ];
    }

    public function getBlock()
    {
        return $this->getUnserializeData($this->data);
    }

    protected function getUnserializeData($data) {
        if ($data) {
            return unserialize($data);
        } else {
            return $this->factoryClassInfoBlock();
        }
    }

    protected function factoryClassInfoBlock() {
        switch ($this->type) {
            case self::TYPE_BLOCK_DESCRIPTION:
                return new Description();
            case self::TYPE_BLOCK_ATTANTION:
                return new Attantion();
            case self::TYPE_BLOCK_SCHEDULE:
                return new Schedule();
            case self::TYPE_BLOCK_FINAL_INFORMATION:
                return new FinalInformation();
            case self::TYPE_BLOCK_IMAGE_SLIDER:
                return new ImageSlider();
            default:
                return null;
        }
    }
}
