<?php

namespace common\models\academy\infoBlockModels;

use Serializable;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class Description
 * @property string $name
 * @property string $description
 * @package common\models\academy\infoBlockModels
 */
class ImageSlider extends Model implements Serializable, InfoBlockInterface
{
    public $images;
    public $files;

    public static $path = 'files/academy/infoblock/imageslider/';

    private $inputTemplates = [
        'images' => 'hidden-images',
        'files' => 'images',
    ];

    public function rules()
    {
        return [
            [['images'], 'each', 'rule' => ['string']],
            [['files'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg', 'maxFiles' => 0],
        ];
    }

    public function getLangAttributes($names = null, $except = [])
    {
        $except = array_merge($except, ['images','files']);
        return parent::getAttributes($names, $except);
    }

    public function attributeLabels()
    {
        return [
            'images' => 'Images',
            'files' => 'Images',
        ];
    }

    public function serialize()
    {
        if ($this->validate()) {
            $this->files = UploadedFile::getInstances($this, 'files');
            $this->uploadImages();
        }

        return serialize([
            $this->images
        ]);
    }

    public function unserialize($serialized)
    {
        list(
            $this->images
        ) = unserialize($serialized);
    }

    public function attributeTemplate($name)
    {
        return $this->inputTemplates[$name];
    }

    public function uploadImages()
    {
        if ($this->validate()) {
            $basePath = Yii::getAlias('@frontend/web');
            $allPath = $basePath . '/' . self::$path;
            if (!is_dir($allPath)) {
                mkdir($allPath, 0777, true);
            }
            foreach ($this->files as $key => $file) {
                $filename = md5(uniqid($file->baseName)).'.'.$file->extension;
                if ($file->saveAs($allPath . $filename)) {
                    chmod($allPath . $filename, 0777);
                    $this->images[] = $filename;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    public function getUrlImagesInput() {
        if (!$this->images) return [];

        $initialPreview = [];
        foreach ($this->images as $image) {
            $initialPreview[] = '/'.self::$path.$image;
        }
        return $initialPreview;
    }

    public function getImagesInputConfig() {
        if (!$this->images) return [];

        $initialPreviewConfig = [];
        foreach ($this->images as $image) {
            $initialPreviewConfig[] = [
                'url' => 'image-delete',
                'key' => $image,
            ];
        }
        return $initialPreviewConfig;
    }

    public function getTemplate() {
        return 'image-slider';
    }

    public static function deleteImage($filename) {
        $filePath = Yii::getAlias('@frontend/web') . '/' . self::$path . $filename;
        if (file_exists($filePath)) {
            unlink($filePath);
            return true;
        }
        return false;
    }
}