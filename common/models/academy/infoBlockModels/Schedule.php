<?php

namespace common\models\academy\infoBlockModels;

use Serializable;
use yii\base\Model;

/**
 * Class Schedule
 * @property string $name
 * @property string $schedule
 * @package common\models\academy\infoBlockModels
 */
class Schedule extends Model implements Serializable, InfoBlockInterface
{
    public $name;
    public $schedule;

    private $inputTemplates = [
        'name' => 'text-input',
        'schedule' => 'schedule',
    ];

    public function rules()
    {
        return [
            ['name', 'required'],
            [['name'], 'string', 'max' => 255],
            [['schedule'], 'string'],
        ];
    }

    public function getLangAttributes($names = null, $except = [])
    {
        return parent::getAttributes($names, $except);
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Title',
            'schedule' => 'Schedule',
        ];
    }

    public function serialize()
    {
        return serialize([
            $this->name,
            $this->schedule,
        ]);
    }

    public function unserialize($serialized)
    {
        list(
            $this->name,
            $this->schedule
        ) = unserialize($serialized);
    }

    public function attributeTemplate($name)
    {
        return $this->inputTemplates[$name];
    }

    public function getTemplate() {
        return 'schedule';
    }

    public function getDecodeSchedule(){
        return $this->schedule?json_decode($this->schedule):null;
    }
}