<?php
namespace common\models\academy\infoBlockModels;

interface InfoBlockInterface
{
    public function getLangAttributes($names = null, $except = []);
}