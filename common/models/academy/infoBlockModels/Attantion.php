<?php

namespace common\models\academy\infoBlockModels;

use Serializable;
use yii\base\Model;

/**
 * Class Attantion
 * @property string $name
 * @property string $attantion
 * @package common\models\academy\infoBlockModels
 */
class Attantion extends Model implements Serializable, InfoBlockInterface
{
    public $name;
    public $attantion;

    private $inputTemplates = [
        'name' => 'text-input',
        'attantion' => 'textarea',
    ];

    public function rules()
    {
        return [
            ['name', 'required'],
            [['name'], 'string', 'max' => 255],
            [['attantion'], 'string'],
        ];
    }

    public function getLangAttributes($names = null, $except = [])
    {
        return parent::getAttributes($names, $except);
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Title',
            'attantion' => 'Attantion',
        ];
    }

    public function serialize()
    {
        return serialize([
            $this->name,
            $this->attantion,
        ]);
    }

    public function unserialize($serialized)
    {
        list(
            $this->name,
            $this->attantion
        ) = unserialize($serialized);
    }

    public function attributeTemplate($name)
    {
        return $this->inputTemplates[$name];
    }

    public function getTemplate() {
        return 'attantion';
    }
}