<?php

namespace common\models\academy\infoBlockModels;

use Serializable;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class Description
 * @property string $name
 * @property string $image
 * @property string $date
 * @property string $location
 * @property string $trainer
 * @property string $seats
 * @property string $file
 * @package common\models\academy\infoBlockModels
 */
class FinalInformation extends Model implements Serializable, InfoBlockInterface
{
    public $name;
    public $image;
    public $date;
    public $location;
    public $trainer;
    public $seats;
    public $file;

    public static $path = 'files/academy/infoblock/finalinformation/';

    private $inputTemplates = [
        'name' => 'text-input',
        'date' => 'text-input',
        'location' => 'text-input',
        'trainer' => 'text-input',
        'seats' => 'text-input',
        'file' => 'one-image',
    ];

    public function rules()
    {
        return [
            ['name', 'required'],
            [['name', 'date', 'location', 'trainer', 'seats'], 'string', 'max' => 255],
            [['file'], 'file', 'extensions' => 'png, jpg, jpeg'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Title',
            'image' => 'Image',
            'file' => 'Image',
            'date' => 'Date',
            'location' => 'Location',
            'trainer' => 'Trainer',
            'seats' => 'Free seats',
        ];
    }

    public function getAttributes($names = null, $except = [])
    {
        $except[] = 'image';
        return parent::getAttributes($names, $except);
    }

    public function getLangAttributes($names = null, $except = [])
    {
        $except = array_merge($except, ['image','file','seats']);
        return parent::getAttributes($names, $except);
    }

    public function serialize()
    {
        if ($this->validate()) {
            $this->file = UploadedFile::getInstance($this, 'file');
            $this->uploadImage();
        }

        return serialize([
            $this->name,
            $this->image,
            $this->date,
            $this->location,
            $this->trainer,
            $this->seats,
        ]);
    }

    public function unserialize($serialized)
    {
        list(
            $this->name,
            $this->image,
            $this->date,
            $this->location,
            $this->trainer,
            $this->seats,
        ) = unserialize($serialized);
    }

    public function attributeTemplate($name)
    {
        return $this->inputTemplates[$name];
    }

    public function getTemplate() {
        return 'final-information';
    }

    public function getUrlImageInput() {
        if (!$this->image) return [];

        return ['/'.self::$path.$this->image];
    }

    public function uploadImage()
    {
        if ($this->file) {
            $basePath = Yii::getAlias('@frontend/web');
            $allPath = $basePath . '/' . self::$path;
            if (!is_dir($allPath)) {
                mkdir($allPath, 0777, true);
            }

            $file = $this->file;
            $filename = md5(uniqid($file->baseName)).'.'.$file->extension;
            if ($file->saveAs($allPath . $filename)) {
                chmod($allPath . $filename, 0777);
            }
            $this->image = $filename;

            return true;
        } else {
            return false;
        }
    }

    public function getUrlImage() {
        return '/'.self::$path.$this->image;
    }
}