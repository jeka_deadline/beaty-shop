<?php

namespace common\models\academy\infoBlockModels;

use Serializable;
use yii\base\Model;

/**
 * Class Description
 * @property string $name
 * @property string $description
 * @package common\models\academy\infoBlockModels
 */
class Description extends Model implements Serializable, InfoBlockInterface
{
    public $name;
    public $description;

    private $inputTemplates = [
        'name' => 'text-input',
        'description' => 'textarea',
    ];

    public function rules()
    {
        return [
            ['name', 'required'],
            [['name'], 'string', 'max' => 255],
            [['description'], 'string'],
        ];
    }

    public function getLangAttributes($names = null, $except = [])
    {
        return parent::getAttributes($names, $except);
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Title',
            'description' => 'Description',
        ];
    }

    public function serialize()
    {
        return serialize([
            $this->name,
            $this->description,
        ]);
    }

    public function unserialize($serialized)
    {
        list(
            $this->name,
            $this->description
        ) = unserialize($serialized);
    }

    public function attributeTemplate($name)
    {
        return $this->inputTemplates[$name];
    }

    public function getTemplate() {
        return 'description';
    }
}