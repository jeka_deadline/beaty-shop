<?php

namespace common\models\academy;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "academy_academys".
 *
 * @property int $id
 * @property string $image
 * @property string $small_image
 * @property string $name
 * @property string $slug
 * @property string $description
 * @property string $note
 * @property int $duration
 * @property double $price
 * @property int $display_order
 * @property int $active
 * @property string $created_at
 * @property string $updated_at
 *
 * @property AcademysLangField[] $academysLangField
 */
class Academy extends \yii\db\ActiveRecord
{

    public static $path = 'files/academy/';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'academy_academys';
    }

    public function behaviors()
    {
        return [
            [
                'class' =>TimestampBehavior::className(),
                'value' => new \yii\db\Expression('NOW()')
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['slug', 'name', 'date'], 'required'],
            [['description'], 'string'],
            [['duration', 'display_order', 'active'], 'integer'],
            [['price'], 'number'],
            [['created_at', 'updated_at', 'date'], 'safe'],
            [['slug', 'image', 'small_image', 'name'], 'string', 'max' => 255],
            [['note'], 'string', 'max' => 1000],
            [['slug'], 'unique'],
            ['slug', 'match', 'pattern' => '#^[a-z][a-z\d-]*[a-z\d]$#'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Image',
            'small_image' => 'Small Image',
            'name' => 'Name',
            'slug' => 'Slug',
            'description' => 'Description',
            'note' => 'Note',
            'duration' => 'Duration',
            'price' => 'Price',
            'date' => 'Date',
            'display_order' => 'Display Order',
            'active' => 'Active',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcademysLangFields()
    {
        return $this->hasMany(AcademysLangField::className(), ['academy_id' => 'id']);
    }

    public function getUrlImageInput() {
        if (!$this->image) return [];

        return ['/'.self::$path.$this->image];
    }

    public function getInfoBlocks()
    {
        return $this->hasMany(InfoBlock::className(), ['academy_id' => 'id']);
    }

    public function getUrlImage() {
        return '/'.self::$path.$this->image;
    }

    public function getUrlSmallImage() {
        if ($this->small_image && file_exists(Yii::getAlias('@frontend/web') . '/' . self::$path.$this->small_image)) {
            return '/'.self::$path.$this->small_image;
        }

        return '/'.self::$path.$this->image;
    }

    public function getPrices()
    {
        return $this->hasMany(Price::className(), ['academy_id' => 'id']);
    }

}
