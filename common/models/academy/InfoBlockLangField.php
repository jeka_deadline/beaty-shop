<?php

namespace common\models\academy;

use common\models\core\Language;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "academy_info_blocks_lang_fields".
 *
 * @property int $id
 * @property int $info_block_id
 * @property int $lang_id
 * @property string $name
 * @property string $data
 * @property string $created_at
 * @property string $updated_at
 *
 * @property InfoBlock $infoBlock
 * @property Language $lang
 */
class InfoBlockLangField extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'academy_info_blocks_lang_fields';
    }

    public function behaviors()
    {
        return [
            [
                'class' =>TimestampBehavior::className(),
                'value' => new \yii\db\Expression('NOW()')
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['info_block_id', 'lang_id', 'name'], 'required'],
            [['info_block_id', 'lang_id'], 'integer'],
            [['data'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['info_block_id'], 'exist', 'skipOnError' => true, 'targetClass' => InfoBlock::className(), 'targetAttribute' => ['info_block_id' => 'id']],
            [['lang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['lang_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'info_block_id' => 'Info Block ID',
            'lang_id' => 'Lang ID',
            'name' => 'Name',
            'data' => 'Data',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInfoBlock()
    {
        return $this->hasOne(InfoBlock::className(), ['id' => 'info_block_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(Language::className(), ['id' => 'lang_id']);
    }
}
