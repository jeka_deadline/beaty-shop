<?php

namespace common\models\academy;

use common\models\core\Language;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "academy_academys_lang_fields".
 *
 * @property int $id
 * @property int $academy_id
 * @property int $lang_id
 * @property string $name
 * @property string $description
 * @property string $note
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Academy $academy
 * @property Language $lang
 */
class AcademysLangField extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'academy_academys_lang_fields';
    }

    public function behaviors()
    {
        return [
            [
                'class' =>TimestampBehavior::className(),
                'value' => new \yii\db\Expression('NOW()')
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['academy_id', 'lang_id', 'name'], 'required'],
            [['academy_id', 'lang_id'], 'integer'],
            [['description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['note'], 'string', 'max' => 1000],
            [['academy_id'], 'exist', 'skipOnError' => true, 'targetClass' => Academy::className(), 'targetAttribute' => ['academy_id' => 'id']],
            [['lang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['lang_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'academy_id' => 'Academy ID',
            'lang_id' => 'Lang ID',
            'name' => 'Name',
            'description' => 'Description',
            'note' => 'Note',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcademy()
    {
        return $this->hasOne(Academy::className(), ['id' => 'academy_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(Language::className(), ['id' => 'lang_id']);
    }
}
