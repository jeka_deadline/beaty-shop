<?php

namespace common\models\academy;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "academy_registers".
 *
 * @property int $id
 * @property int $academy_id
 * @property int $price_id
 * @property string $sex
 * @property string $first_name
 * @property string $last_name
 * @property string $phone
 * @property string $company
 * @property string $email
 * @property int $is_new
 * @property int $seats
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Academy $academy
 */
class Register extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'academy_registers';
    }

    public function behaviors()
    {
        return [
            [
                'class' =>TimestampBehavior::className(),
                'value' => new \yii\db\Expression('NOW()')
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['academy_id', 'first_name', 'last_name', 'phone', 'email'], 'required'],
            [['academy_id', 'is_new', 'seats', 'price_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['sex', 'phone'], 'string', 'max' => 20],
            [['first_name', 'last_name'], 'string', 'max' => 50],
            [['email', 'company'], 'string', 'max' => 100],
            [['academy_id'], 'exist', 'skipOnError' => true, 'targetClass' => Academy::className(), 'targetAttribute' => ['academy_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'academy_id' => 'Academy',
            'price_id' => 'Price ID',
            'sex' => 'Sex',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'phone' => 'Phone',
            'email' => 'Email',
            'company' => 'Company',
            'seats' => 'Seats',
            'is_new' => 'Is New',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcademy()
    {
        return $this->hasOne(Academy::className(), ['id' => 'academy_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrice()
    {
        return $this->hasOne(Price::className(), ['id' => 'price_id']);
    }
}
