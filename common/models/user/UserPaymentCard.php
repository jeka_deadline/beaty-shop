<?php

namespace common\models\user;

use Yii;

/**
 * This is the model class for table "{{%user_payment_cards}}".
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $number
 * @property string $pseudocardpan
 * @property string $created_at
 * @property string $updated_at
 *
 * @property User $user
 */
class UserPaymentCard extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public static function tableName()
    {
        return '{{%user_payment_cards}}';
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['user_id', 'default'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'pseudocardpan'], 'string', 'max' => 255],
            [['number'], 'string', 'max' => 16],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'name' => 'Name',
            'number' => 'Number',
            'pseudocardpan' => 'Pseudocardpan',
            'default' => 'Default',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
