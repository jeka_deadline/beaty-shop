<?php

namespace common\models\user;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%user_users}}".
 *
 * @property int $id
 * @property string $email
 * @property string $password_hash
 * @property string $auth_key
 * @property string $blocked_at
 * @property string $confirm_email_at
 * @property string $register_ip
 * @property string $created_at
 * @property string $updated_at
 * @property int $login_with_social
 *
 * @property Profile profile
 * @property Social[] $socials
 * @property UserEmailToken $emailToken
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * User sex `male`.
     *
     * @var string
     */
    const USER_MALE = 'male';

    /**
     * User sex `female`.
     *
     * @var string
     */
    const USER_FEMALE = 'female';

    /**
     * User title missis
     *
     * @var string
     */
    const USER_TITLE_MRS = 'mrs';

    /**
     * User title mister
     *
     * @var string
     */
    const USER_TITLE_MR = 'mr';

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public static function tableName()
    {
        return '{{%user_users}}';
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['password_hash', 'auth_key', 'register_ip'], 'required'],
            [['blocked_at', 'confirm_email_at', 'created_at', 'updated_at'], 'safe'],
            [['login_with_social'], 'integer'],
            [['email', 'password_hash'], 'string', 'max' => 100],
            [['auth_key'], 'string', 'max' => 32],
            [['register_ip'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'password_hash' => 'Password Hash',
            'auth_key' => 'Auth Key',
            'blocked_at' => 'Blocked At',
            'confirm_email_at' => 'Confirm Email At',
            'register_ip' => 'Register Ip',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'login_with_social' => 'Login With Social',
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * Get human title.
     *
     * @param string $title Title.
     * @return string|null
     */
    public static function getHumanTitle($title)
    {
        switch ($title) {
            case self::USER_TITLE_MR:
                return 'Herr';
            case self::USER_TITLE_MRS:
                return 'Frau';
            default:
                return null;
        }
    }

    /**
     * {@inheritdoc}
     *
     * @return \common\models\user\User
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * {@inheritdoc}
     *
     * @param string $token
     * @param string $type
     * @return \common\models\user\User
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * {@inheritdoc}
     *
     * @return id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Generate user confirm email token.
     *
     * @return bool|\common\models\user\UserEmailToken
     */
    public function generateEmailToken()
    {
        $model          = new UserEmailToken();
        $model->token   = Yii::$app->security->generateRandomString();
        $model->user_id = $this->id;

        if ($model->validate() && $model->save()) {
            return $model;
        }

        return false;
    }

    /**
     * List user genders.
     *
     * @return array
     */
    public static function getListSex()
    {
        return [
            self::USER_MALE   => 'Male',
            self::USER_FEMALE => 'Female',
        ];
    }

    /**
     * List user titles.
     *
     * @return array
     */
    public static function getListUserTitles()
    {
        return [
            self::USER_TITLE_MRS => 'Mrs',
            self::USER_TITLE_MR  => 'Mr',
        ];
    }

    /**
     * Set user hash password.
     *
     * @param string $password Password string
     * @return void
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Set user auth key.
     *
     * @return void
     */
    public function generateAuthKey()
    {
        $this->auth_key = md5(time() . '_' . Yii::$app->security->generateRandomString(20));
    }

    /**
     * Find user by email.
     *
     * @param string $email User email
     * @return User
     */
    public static function findByEmail($email)
    {
        return static::find()
            ->where(['=', 'email', $email])
            ->one();
    }

    /**
     * Check if user is blocked.
     *
     * @return bool
     */
    public function isBlocked()
    {
        return ($this->blocked_at) ? true : false;
    }

    /**
     * Check if user is confirm email.
     *
     * @return bool
     */
    public function isConfirmEmail()
    {
        return ($this->confirm_email_at) ? true : false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSocial()
    {
        return $this->hasMany(Social::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmailToken()
    {
        return $this->hasOne(UserEmailToken::className(), ['user_id' => 'id']);
    }
}
