<?php

namespace common\models\user;

use Yii;

/**
 * This is the model class for table "{{%user_profiles}}".
 *
 * @property int $id
 * @property int $user_id
 * @property string $phone
 * @property string $surname
 * @property string $name
 * @property string $title
 * @property string $company
 * @property string $date_birth
 *
 * @property User $user
 */
class Profile extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public static function tableName()
    {
        return '{{%user_profiles}}';
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id'], 'integer'],
            [['date_birth'], 'safe'],
            [['phone'], 'string', 'max' => 20],
            [['surname', 'name'], 'string', 'max' => 50],
            [['title'], 'string', 'max' => 10],
            [['company'], 'string', 'max' => 100],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'phone' => 'Phone',
            'surname' => 'Surname',
            'name' => 'Name',
            'title' => 'Title',
            'company' => 'Company',
            'date_birth' => 'Date Birth',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
