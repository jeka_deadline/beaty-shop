<?php

namespace common\models\user;

use Yii;
use common\models\geo\Country;

/**
 * This is the model class for table "{{%user_shipping_addresses}}".
 *
 * @property int $id
 * @property string $name_address
 * @property string $name
 * @property string $title
 * @property int $user_id
 * @property string $surname
 * @property int $country_id
 * @property string $city
 * @property string $address1
 * @property string $address2
 * @property string $zip
 * @property string $phone
 * @property string $email
 * @property int $default
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Country $country
 * @property Use $user
 */
class UserShippingAddress extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public static function tableName()
    {
        return '{{%user_shipping_addresses}}';
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['name_address', 'city', 'title'], 'required'],
            [['user_id', 'country_id', 'default'], 'integer'],
            [['address1', 'address2'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name_address', 'name', 'surname'], 'string', 'max' => 50],
            [['city', 'email'], 'string', 'max' => 255],
            [['zip'], 'string', 'max' => 5],
            [['phone'], 'string', 'max' => 16],
            [['title'], 'string', 'max' => 10],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_address' => 'Name Address',
            'name' => 'Name',
            'user_id' => 'User ID',
            'surname' => 'Surname',
            'country_id' => 'Country ID',
            'city' => 'City',
            'address1' => 'Address1',
            'address2' => 'Address2',
            'zip' => 'Zip',
            'phone' => 'Phone',
            'email' => 'Email',
            'title' => 'Title',
            'default' => 'Default',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
