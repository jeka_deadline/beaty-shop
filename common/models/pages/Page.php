<?php

namespace common\models\pages;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "pages_static_page".
 *
 * @property integer $id
 * @property string $uri
 * @property string $header
 * @property string $menu_class
 * @property string $content
 * @property string $body_class
 * @property integer $display_order
 * @property integer $active
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $created_at
 * @property string $updated_at
 */
class Page extends \yii\db\ActiveRecord
{
    /**
     * @var string $filePath Path to save pages resources
     */
    const FILE_PATH = 'files/static-pages';

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public static function tableName()
    {
        return '{{%pages_static_page}}';
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new \yii\db\Expression('NOW()')
            ]
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['display_order', 'active'], 'integer'],
            [['uri', 'header'], 'required'],
            [['content'], 'string'],
            [['uri'], 'string', 'max' => 100],
            [['header', 'menu_class', 'body_class', 'meta_title', 'meta_keywords'], 'string', 'max' => 255],
            [['uri'], 'unique'],
            [['meta_description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id'               => 'ID',
            'uri'              => 'Uri',
            'header'           => 'Header',
            'menu_class'       => 'Menu Class',
            'body_class'       => 'Body Class',
            'content'          => 'Content',
            'display_order'    => 'Display order',
            'active'           => 'Active',
            'meta_title'       => 'Meta title',
            'meta_description' => 'Meta description',
            'meta_keywords'    => 'Meta_keywords',
            'created_at'       => 'Created at',
            'updated_at'       => 'Updated at',
        ];
    }
}
