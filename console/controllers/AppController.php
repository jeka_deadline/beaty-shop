<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use common\models\user\User;
use common\models\user\UserAdmin;

/**
 * Controller class for install|uninstall project.
 */
class AppController extends Controller
{
    /**
     * Up all migrations for project
     *
     * @return void
     */
    public function actionInstall()
    {
        Yii::$app->runAction('migrate', ['migrationPath' => 'backend/modules/core/migrations/', 'interactive' => false]);
        Yii::$app->runAction('migrate', ['migrationPath' => 'backend/modules/geo/migrations/', 'interactive' => false]);
        Yii::$app->runAction('migrate', ['migrationPath' => '@yii/rbac/migrations/', 'interactive' => false]);
        Yii::$app->runAction('migrate', ['migrationPath' => 'backend/modules/user/migrations/', 'interactive' => false]);
        Yii::$app->runAction('migrate', ['migrationPath' => 'backend/modules/product/migrations/', 'interactive' => false]);
        Yii::$app->runAction('migrate', ['migrationPath' => 'backend/modules/academy/migrations/', 'interactive' => false]);
        Yii::$app->runAction('migrate', ['migrationPath' => 'backend/modules/pages/migrations/', 'interactive' => false]);
        Yii::$app->runAction('migrate', ['migrationPath' => 'backend/modules/blog/migrations/', 'interactive' => false]);
    }

    /**
     * Down migrations for project
     *
     * @throws Exception
     */
    public function actionUninstall()
    {
        Yii::$app->runAction('migrate/down', ['migrationPath' => 'backend/modules/pages/migrations/', 'interactive' => false]);
        Yii::$app->runAction('migrate/down', ['migrationPath' => 'backend/modules/academy/migrations/', 'interactive' => false]);
        Yii::$app->runAction('migrate/down', ['migrationPath' => 'backend/modules/product/migrations/', 'interactive' => false]);
        Yii::$app->runAction('migrate/down', ['migrationPath' => '@yii/rbac/migrations/', 'interactive' => false]);
        Yii::$app->runAction('migrate/down', ['migrationPath' => 'backend/modules/user/migrations/', 'interactive' => false]);
        Yii::$app->runAction('migrate/down', ['migrationPath' => 'backend/modules/geo/migrations/', 'interactive' => false]);
        Yii::$app->runAction('migrate/down', ['migrationPath' => 'backend/modules/core/migrations/', 'interactive' => false]);
        Yii::$app->runAction('migrate/down', ['migrationPath' => 'backend/modules/blog/migrations/', 'interactive' => false]);
    }

    /**
     * Add admin rules for user.
     *
     * @param string $email User email
     * @return null|bool
     */
    public function actionCreateAdmin($email)
    {
        $user = User::findOne(['email' => $email]);

        if (!$user) {
            echo "User with email = {$email} not found" . PHP_EOL;
            return null;
        }

        if (UserAdmin::find()->where(['user_id' => $user->id])->count()) {
            echo "User with email = {$email} already have admin rules" . PHP_EOL;
            return null;
        }

        $admin = new UserAdmin();

        $admin->user_id = $user->id;

        $admin->save();

        echo "User with email = {$email} granted admin rules" . PHP_EOL;

        return true;;
    }

    /**
     * Generate password hash by string.
     *
     * @param string $password Password string
     * @return void
     */
    public function actionGeneratePasswordHash($password)
    {
        echo 'Password hash = ' . Yii::$app->security->generatePasswordHash($password) . PHP_EOL;
    }
}
