<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use yii\db\QueryBuilder;
use console\seeds\BaseSeeder;

/**
 * Controller class for seed rows to database.
 */
class SeedController extends Controller
{
    /**
     * Up all migrations for project
     *
     * @return void
     */
    public function actionInstall()
    {
        $this->seed(\console\seeds\EmailTemplateSeeder::className());
    }

    /**
     * Down migrations for project
     *
     * @throws Exception
     */
    public function actionUninstall()
    {
        Yii::$app->runAction('migrate/down', ['migrationPath' => 'backend/modules/product/migrations/', 'interactive' => false]);
        Yii::$app->runAction('migrate/down', ['migrationPath' => 'backend/modules/user/migrations/', 'interactive' => false]);
        Yii::$app->runAction('migrate/down', ['migrationPath' => 'backend/modules/geo/migrations/', 'interactive' => false]);
        Yii::$app->runAction('migrate/down', ['migrationPath' => 'backend/modules/core/migrations/', 'interactive' => false]);
    }

    private function seed($seederClassName)
    {
        $builder = new QueryBuilder();

        if (!$this->isExistSeederTable()) {
            $this->createSeederTable();
        }

        $objectClassSeeder = Yii::createObject($seederClassName);

        if (!$objectClassSeeder instanceof BaseSeeder || $this->seederCall($objectClassSeeder)) {
            return false;
        }

        $objectClassSeeder->call();

    }

    private function createSeederTable()
    {

    }

    private function isExistSeederTable()
    {
        $tableName = 'seeders';

        return true;
    }

    private function seederCall(BaseSeeder $objectClassSeeder)
    {

    }
}
