<?php
namespace console\modules\product\controllers;

use console\modules\product\models\ProductInventorie;
use yii\console\Controller;
use Yii;

class CronController extends Controller
{
    public function actionEnds()
    {
        echo "Start sending a product notice which ends.";
        $this->sendAdminEnds(ProductInventorie::getProductEnds());
        echo "Email sent. End.";
        return true;
    }

    /**
     * Send admin information Production Ends.
     *
     * @static
     * @return void
     */
    private function sendAdminEnds($productVariations)
    {
        $content = $this->renderPartial(
            'ends',
            [
                'productVariations' => $productVariations
            ]
        );

        $mailer = Yii::$app->mailer;

        $mailer->compose()
            ->setFrom([$mailer->transport->getUsername() => 'info'])
            ->setTo(Yii::$app->params[ 'adminEmail' ])
            ->setSubject('List production ends.')
            ->setTextBody($content)
            ->send();
    }
}
