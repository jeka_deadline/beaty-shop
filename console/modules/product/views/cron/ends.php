<?php
?>
The list of goods that ends:

<?php foreach ($productVariations as $variation): ?>
<?= $variation->name ?>

    Code: <?= $variation->vendor_code ?>

    Inventory: <?= $variation->inventorie?$variation->inventorie->count:0 ?>

<?php if ($variation->properties): ?>
<?php foreach ($variation->properties as $property) : ?>
<?php if ($property->filter): ?>
    <?= $property->filter->name; ?>: <?= $property->value; ?>

<?php endif; ?>
<?php endforeach; ?>

<?php endif; ?>
<?php endforeach; ?>