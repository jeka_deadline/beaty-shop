<?php

namespace console\modules\product\models;

use console\modules\core\models\Setting;

class ProductInventorie extends \common\models\product\ProductInventorie
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getProductEnds()
    {
        $quantity = Setting::value('product.send.email.limit.quantity');

        $productVariations = ProductVariation::find()
            ->joinWith('inventorie inventorie')
            ->with('properties')
            ->where(['<', 'inventorie.count', $quantity])
            ->andWhere(['>=', 'inventorie.count', 0])
            ->active()
            ->all();

        return $productVariations;
    }
}
