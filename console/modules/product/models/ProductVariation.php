<?php

namespace console\modules\product\models;

use frontend\queries\ActiveQuery;

/**
 * Frontend product variation model extends common product variation model.
 */
class ProductVariation extends \common\models\product\ProductVariation
{
    /**
     * {@inheritdoc}
     *
     * @return \frontend\queries\ActiveQuery
     */
    public static function find()
    {
        return new ActiveQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventorie()
    {
        return $this->hasOne(ProductInventorie::className(), ['product_variation_id' => 'id']);
    }

    public function getProperties()
    {
        return $this->hasMany(ProductVariationPropertie::className(), ['product_variation_id' => 'id']);
    }
}
