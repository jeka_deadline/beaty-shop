<?php
namespace console\modules\product;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'console\modules\product\controllers';

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function init()
    {
        return parent::init();
    }
}
