<?php

namespace console\seeds;

class EmailTemplateSeeder extends BaseSeeder
{
    public function call()
    {
        $this->insert('{{%core_email_templates}}', [
            [
                'type'    => 'core',
                'code'    => 'confirm-email',
                'text'    => 'Confirm you email by link {confirmEmailLink}',
                'subject' => 'Confirm email',
                'from'    => 'bot',
            ]
        ]);
    }
}