<?php

namespace console\seeds;

use yii\db\QueryBuilder;
use yii\helpers\ArrayHelper;

abstract class BaseSeeder
{
    abstract public function call();

    public function insert($tableName, $columns)
    {
        $builder = new QueryBuilder();

        if (ArrayHelper::isAssociative($columns)) {
            $columns = [$columns];
        }

        foreach ($columns as $itemData) {
            $builder->insert($tableName, $itemData);
        }
    }
}